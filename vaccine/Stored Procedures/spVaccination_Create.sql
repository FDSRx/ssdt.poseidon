﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/25/2014
-- Description:	Creates a new Vacination object.
-- SAMPLE CALL:
/*
DECLARE
	@VaccinationID BIGINT = NULL,
	@DateAdministered DATETIME = GETDATE()

EXEC vaccine.spVaccination_Create
	@PersonID = 135590,
	@DateAdministered = @DateAdministered,
	@CVXCodeID = 55,
	@AdministrationRouteID = 1,
	@AdministrationSiteID = 1,
	@Dosage = '100 cc',
	@MVXCodeID = 10,
	@FinancialClassID = 1,
	@AdministerTypeID = 1,
	@AdministerFirstName = 'Sarah',
	@AdministerLastName = 'Doe',
	@VaccineLotNumber = 'H107',
	@DateExpires = '1/19/2015',
	@CreatedBy = 'dhughes',
	@VaccinationID = @VaccinationID OUTPUT

SELECT @VaccinationID AS VaccinationID
	

*/

-- SELECT * FROM vaccine.Vaccination
-- SELECT * FROM phrm.vwPatient WHERE LastName LIKE '%Simmons%'
-- SELECT * FROM vaccine.CVXCode WHERE FullVaccineName LIKE '%infl%'
-- SELECT * FROM vaccine.MVXCode
-- SELECT * FROM vaccine.AdministrationRoute
-- SELECT * FROM vaccine.AdministrationSite
-- SELECT * FROM dbo.FinancialClass
-- =============================================
CREATE PROCEDURE [vaccine].[spVaccination_Create]
	@PersonID BIGINT,
	@DateAdministered DATETIME,
	@CVXCodeID INT,
	@AdministrationRouteID INT,
	@AdministrationSiteID INT,
	@Dosage VARCHAR(50),
	@MVXCodeID INT,
	@FinancialClassID INT,
	@AdministerTypeID INT = NULL,
	@AdministerFirstName VARCHAR(75) = NULL,
	@AdministerLastName VARCHAR(75) = NULL,
	@VaccineLotNumber VARCHAR(25) = NULL,
	@DateExpires DATETIME = NULL,
	@Notes VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256),
	@VaccinationID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	---------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------
	SET @VaccinationID = NULL;
	
	---------------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000) = NULL
		
	---------------------------------------------------------------------------
	-- Null argument validation.
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	---------------------------------------------------------------------------
	-- Person.
	IF @PersonID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Vaccination object. Object reference (@PersonID) is not set to an instance of an object. ' +
			'The @PersonID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- Date administered.
	IF @DateAdministered IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Vaccination object. Object reference (@DateAdministered) is not set to an instance of an object. ' +
			'The @DateAdministered parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- CVX code.
	IF @CVXCodeID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Vaccination object. Object reference (@CVXCodeID) is not set to an instance of an object. ' +
			'The @CVXCodeID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- Administration route.
	IF @AdministrationRouteID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Vaccination object. Object reference (@AdministrationRouteID) is not set to an instance of an object. ' +
			'The @AdministrationRouteID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	-- Administration site.
	IF @AdministrationSiteID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Vaccination object. Object reference (@AdministrationSiteID) is not set to an instance of an object. ' +
			'The @AdministrationSiteID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- Dosage
	IF dbo.fnIsNullOrWhiteSpace(@Dosage) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to create Vaccination object. Object reference (@Dosage) is not set to an instance of an object. ' +
			'The @Dosage parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- MVX Code
	IF @MVXCodeID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Vaccination object. Object reference (@MVXCodeID) is not set to an instance of an object. ' +
			'The @MVXCodeID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- Financial class code.
	IF @FinancialClassID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Vaccination object. Object reference (@FinancialClassID) is not set to an instance of an object. ' +
			'The @FinancialClassID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END		
	

	---------------------------------------------------------------------------
	-- Create new vaccination object.
	-- <Summary>
	-- Adds a new vaccincation object to a person's list of vaccinations.
	-- </Summary>
	---------------------------------------------------------------------------
	INSERT INTO vaccine.Vaccination (
		PersonID,
		DateAdministered,
		CVXCodeID,
		AdministrationRouteID,
		AdministrationSiteID,
		Dosage,
		MVXCodeID,
		FinancialClassID,
		AdministerTypeID,
		AdministerFirstName,
		AdministerLastName,
		VaccineLotNumber,
		DateExpires,
		Notes,
		CreatedBy
	)
	SELECT
		@PersonID,
		@DateAdministered,
		@CVXCodeID,
		@AdministrationRouteID,
		@AdministrationSiteID,
		@Dosage,
		@MVXCodeID,
		@FinancialClassID,
		@AdministerTypeID,
		@AdministerFirstName,
		@AdministerLastName,
		@VaccineLotNumber,
		@DateExpires,
		@Notes,
		@CreatedBy
	
	-- Retrieve record identity
	SET @VaccinationID = SCOPE_IDENTITY();
	
	
		
END
