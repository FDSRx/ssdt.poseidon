﻿
CREATE VIEW [vaccine].[vwVaccination]
AS
SELECT     vac.VaccinationID, vac.PersonID, psn.FirstName, psn.LastName, psn.BirthDate, vac.DateAdministered, vac.CVXCodeID, cvx.Code AS CVXCode, cvx.FullVaccineName, 
                      vac.AdministrationRouteID, ar.Code AS AdministrationRouteCode, ar.Description AS AdministrationRouteDescription, vac.AdministrationSiteID, 
                      st.Code AS AdministrationSiteCode, st.Description AS AdministrationSiteDescription, vac.Dosage, vac.MVXCodeID, mvx.Code AS MVXCode, mvx.Name AS MVXName, 
                      vac.FinancialClassID, fc.Code AS FinancialClassCode, fc.Description AS FinancialClassDescription, 
                      vac.AdministerTypeID, atyp.Code AS AdministerTypeCode, atyp.Name AS AdministerTypeName,
                      vac.AdministerFirstName, vac.AdministerLastName, vac.VaccineLotNumber, vac.DateExpires,
                      vac.DateCreated, vac.DateModified, vac.CreatedBy, 
                      vac.ModifiedBy
FROM         vaccine.Vaccination AS vac INNER JOIN
                      dbo.Person AS psn ON vac.PersonID = psn.BusinessEntityID LEFT OUTER JOIN
                      vaccine.CVXCode AS cvx ON vac.CVXCodeID = cvx.CVXCodeID LEFT OUTER JOIN
                      vaccine.AdministrationRoute AS ar ON vac.AdministrationRouteID = ar.AdministrationRouteID LEFT OUTER JOIN
                      vaccine.AdministrationSite AS st ON vac.AdministrationSiteID = st.AdministrationSiteID LEFT OUTER JOIN
                      vaccine.MVXCode AS mvx ON vac.MVXCodeID = mvx.MVXCodeID LEFT OUTER JOIN
                      dbo.FinancialClass AS fc ON vac.FinancialClassID = fc.FinancialClassID
                      LEFT JOIN medical.AdministerType atyp
						ON vac.AdministerTypeID = atyp.AdministerTypeID


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vac"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "psn"
            Begin Extent = 
               Top = 6
               Left = 273
               Bottom = 125
               Right = 442
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cvx"
            Begin Extent = 
               Top = 6
               Left = 480
               Bottom = 125
               Right = 653
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ar"
            Begin Extent = 
               Top = 6
               Left = 691
               Bottom = 125
               Right = 888
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "st"
            Begin Extent = 
               Top = 6
               Left = 926
               Bottom = 125
               Right = 1112
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mvx"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 211
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "fc"
            Begin Extent = 
               Top = 126
               Left = 249
               Bottom = 245
               Right = 415
            End
            DisplayFlags = 280
            TopColumn = 0
       ', @level0type = N'SCHEMA', @level0name = N'vaccine', @level1type = N'VIEW', @level1name = N'vwVaccination';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'  End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'vaccine', @level1type = N'VIEW', @level1name = N'vwVaccination';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'vaccine', @level1type = N'VIEW', @level1name = N'vwVaccination';

