﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/22/2014
-- Description:	Returns the MVXCodeID.
-- SAMPLE CALL: SELECT vaccine.fnGetMVXCodeID(1);
-- SAMPLE CALL: SELECT vaccine.fnGetMVXCodeID('AVB');
-- SELECT * FROM vaccine.MVXCode
-- =============================================
CREATE FUNCTION vaccine.fnGetMVXCodeID
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT MVXCodeID FROM vaccine.MVXCode WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT MVXCodeID FROM vaccine.MVXCode WHERE MVXCodeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
