﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/22/2014
-- Description:	Returns the AdministrationRouteID.
-- SAMPLE CALL: SELECT vaccine.fnGetAdministrationRouteID(1);
-- SAMPLE CALL: SELECT vaccine.fnGetAdministrationRouteID('IV');
-- SELECT * FROM vaccine.AdministrationRoute
-- =============================================
CREATE FUNCTION [vaccine].[fnGetAdministrationRouteID]
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT AdministrationRouteID FROM vaccine.AdministrationRoute WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT AdministrationRouteID FROM vaccine.AdministrationRoute WHERE AdministrationRouteID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
