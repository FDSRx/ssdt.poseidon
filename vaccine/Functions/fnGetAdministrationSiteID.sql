﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/22/2014
-- Description:	Returns the AdministrationSiteID.
-- SAMPLE CALL: SELECT vaccine.fnGetAdministrationSiteID(1);
-- SAMPLE CALL: SELECT vaccine.fnGetAdministrationSiteID('LT');
-- SELECT * FROM vaccine.AdministrationSite
-- =============================================
CREATE FUNCTION [vaccine].[fnGetAdministrationSiteID]
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT AdministrationSiteID FROM vaccine.AdministrationSite WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT AdministrationSiteID FROM vaccine.AdministrationSite WHERE AdministrationSiteID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
