﻿CREATE TABLE [vaccine].[CPTCVXMap] (
    [CPTCVXMapID]     INT              IDENTITY (1, 1) NOT NULL,
    [CPTCode]         VARCHAR (25)     NOT NULL,
    [CPTDescription]  VARCHAR (256)    NOT NULL,
    [CVXCode]         VARCHAR (25)     NOT NULL,
    [VaccineName]     VARCHAR (256)    NOT NULL,
    [CPTCodeStatus]   VARCHAR (50)     NULL,
    [Comments]        VARCHAR (MAX)    NULL,
    [DateLastUpdated] DATETIME         NOT NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_CPTCVXMap_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_CPTCVXMap_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_CPTCVXMap_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)    NULL,
    [ModifiedBy]      VARCHAR (256)    NULL,
    CONSTRAINT [PK_CPTCVXMap] PRIMARY KEY CLUSTERED ([CPTCVXMapID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CPTCVXMap_CPTCodeCVXCode]
    ON [vaccine].[CPTCVXMap]([CPTCode] ASC, [CVXCode] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CPTCVXMap_rowguid]
    ON [vaccine].[CPTCVXMap]([rowguid] ASC);

