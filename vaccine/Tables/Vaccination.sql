﻿CREATE TABLE [vaccine].[Vaccination] (
    [VaccinationID]         BIGINT           IDENTITY (1, 1) NOT NULL,
    [PersonID]              BIGINT           NOT NULL,
    [DateAdministered]      DATETIME         NOT NULL,
    [CVXCodeID]             INT              NOT NULL,
    [AdministrationRouteID] INT              NULL,
    [AdministrationSiteID]  INT              NULL,
    [Dosage]                VARCHAR (50)     NULL,
    [MVXCodeID]             INT              NULL,
    [FinancialClassID]      INT              NULL,
    [AdministerTypeID]      INT              NULL,
    [AdministerFirstName]   VARCHAR (75)     NULL,
    [AdministerLastName]    VARCHAR (75)     NULL,
    [VaccineLotNumber]      VARCHAR (50)     NULL,
    [DateExpires]           DATETIME         NULL,
    [Notes]                 VARCHAR (MAX)    NULL,
    [rowguid]               UNIQUEIDENTIFIER CONSTRAINT [DF_Vaccination_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]           DATETIME         CONSTRAINT [DF_Vaccination_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]          DATETIME         CONSTRAINT [DF_Vaccination_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             VARCHAR (256)    NULL,
    [ModifiedBy]            VARCHAR (256)    NULL,
    CONSTRAINT [PK_Vaccination] PRIMARY KEY CLUSTERED ([VaccinationID] ASC),
    CONSTRAINT [FK_Vaccination_AdministerType] FOREIGN KEY ([AdministerTypeID]) REFERENCES [medical].[AdministerType] ([AdministerTypeID]),
    CONSTRAINT [FK_Vaccination_AdministrationRoute] FOREIGN KEY ([AdministrationRouteID]) REFERENCES [vaccine].[AdministrationRoute] ([AdministrationRouteID]),
    CONSTRAINT [FK_Vaccination_AdministrationSite] FOREIGN KEY ([AdministrationSiteID]) REFERENCES [vaccine].[AdministrationSite] ([AdministrationSiteID]),
    CONSTRAINT [FK_Vaccination_CVXCode] FOREIGN KEY ([CVXCodeID]) REFERENCES [vaccine].[CVXCode] ([CVXCodeID]),
    CONSTRAINT [FK_Vaccination_FinancialClass] FOREIGN KEY ([FinancialClassID]) REFERENCES [dbo].[FinancialClass] ([FinancialClassID]),
    CONSTRAINT [FK_Vaccination_MVXCode] FOREIGN KEY ([MVXCodeID]) REFERENCES [vaccine].[MVXCode] ([MVXCodeID]),
    CONSTRAINT [FK_Vaccination_Person] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Vaccination_rowguid]
    ON [vaccine].[Vaccination]([rowguid] ASC);


GO
CREATE NONCLUSTERED INDEX [UIX_Vaccination_Person]
    ON [vaccine].[Vaccination]([PersonID] ASC);

