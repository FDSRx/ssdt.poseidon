﻿CREATE TABLE [vaccine].[AdministrationSite] (
    [AdministrationSiteID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]                 VARCHAR (25)     NOT NULL,
    [Description]          VARCHAR (256)    NOT NULL,
    [rowguid]              UNIQUEIDENTIFIER CONSTRAINT [DF_AdministrationSite_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]          DATETIME         CONSTRAINT [DF_AdministrationSite_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]         DATETIME         CONSTRAINT [DF_AdministrationSite_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]            VARCHAR (256)    NULL,
    [ModifiedBy]           VARCHAR (256)    NULL,
    CONSTRAINT [PK_AdministrationSite] PRIMARY KEY CLUSTERED ([AdministrationSiteID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AdministrationSite_Code]
    ON [vaccine].[AdministrationSite]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AdministrationSite_rowguid]
    ON [vaccine].[AdministrationSite]([rowguid] ASC);

