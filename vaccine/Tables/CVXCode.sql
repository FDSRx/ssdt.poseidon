﻿CREATE TABLE [vaccine].[CVXCode] (
    [CVXCodeID]        INT              IDENTITY (1, 1) NOT NULL,
    [Code]             VARCHAR (25)     NOT NULL,
    [FullVaccineName]  VARCHAR (256)    NOT NULL,
    [ShortDescription] VARCHAR (100)    NOT NULL,
    [VaccineStatus]    VARCHAR (50)     NOT NULL,
    [Notes]            VARCHAR (MAX)    NULL,
    [DateLastUpdated]  DATETIME         NOT NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_CVXCode_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_CVXCode_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_CVXCode_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_CVXCode] PRIMARY KEY CLUSTERED ([CVXCodeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CVXCode_Code]
    ON [vaccine].[CVXCode]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CVXCode_rowguid]
    ON [vaccine].[CVXCode]([rowguid] ASC);

