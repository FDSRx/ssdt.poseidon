﻿CREATE TABLE [vaccine].[AdministrationRoute] (
    [AdministrationRouteID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]                  VARCHAR (25)     NOT NULL,
    [Description]           VARCHAR (256)    NOT NULL,
    [Definition]            VARCHAR (4000)   NULL,
    [rowguid]               UNIQUEIDENTIFIER CONSTRAINT [DF_AdministrationRoute_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]           DATETIME         CONSTRAINT [DF_AdministrationRoute_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]          DATETIME         CONSTRAINT [DF_AdministrationRoute_DateModifiied] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             VARCHAR (256)    NULL,
    [ModifiedBy]            VARCHAR (256)    NULL,
    CONSTRAINT [PK_AdministrationRoute] PRIMARY KEY CLUSTERED ([AdministrationRouteID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AdministrationRoute_Code]
    ON [vaccine].[AdministrationRoute]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AdministrationRoute_rowguid]
    ON [vaccine].[AdministrationRoute]([rowguid] ASC);

