﻿CREATE TABLE [vaccine].[MVXCode] (
    [MVXCodeID]       INT              IDENTITY (1, 1) NOT NULL,
    [Code]            VARCHAR (25)     NOT NULL,
    [Name]            VARCHAR (256)    NOT NULL,
    [Status]          VARCHAR (25)     NOT NULL,
    [Notes]           VARCHAR (MAX)    NULL,
    [DateLastUpdated] DATETIME         NOT NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_MVXCode_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_MVXCode_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_MVXCode_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)    NULL,
    [ModifiedBy]      VARCHAR (256)    NULL,
    CONSTRAINT [PK_MVXCode] PRIMARY KEY CLUSTERED ([MVXCodeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MVXCode_Code]
    ON [vaccine].[MVXCode]([MVXCodeID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MVXCode_Name]
    ON [vaccine].[MVXCode]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MVXCode_rowguid]
    ON [vaccine].[MVXCode]([rowguid] ASC);

