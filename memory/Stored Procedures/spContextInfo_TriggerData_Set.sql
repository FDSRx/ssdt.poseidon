﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/5/2014
-- Description:	Stores a reference (pointer) to additional data that is required by the trigger.
-- SAMPLE CALL:
/*
DECLARE @TriggerToken UNIQUEIDENTIFIER

EXEC memory.spContextInfo_TriggerData_Set
	@ObjectName = 'TestObject',
	@DataString = 'dhughes',
	@TriggerToken = @TriggerToken OUTPUT
	
SELECT @TriggerToken AS TriggerToken

*/

-- SELECT * FROM memory.TriggerData
-- =============================================
CREATE PROCEDURE memory.spContextInfo_TriggerData_Set
	@ObjectName VARCHAR(256) = NULL,
	@DataXml XML = NULL,
	@DataString VARCHAR(MAX) = NULL,
	@DataBinary VARBINARY(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@TriggerToken UNIQUEIDENTIFIER = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------	
	SET @TriggerToken = NULL;
	
	
	-------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------	
	DECLARE
		@Context_Info VARBINARY(100)



	-------------------------------------------------------------------------------------------
	-- Set the CONTEXT_INFO sql variable
	-- <Summary>
	-- Sets the sql CONTEXT_INFO property.  The context info allows additional data to be passed
	-- aroun in the current session context. We use the context info property to store our
	-- pointer to the necessary data that is required for the trigger.
	-- </Summary>
	-------------------------------------------------------------------------------------------		
	BEGIN TRY
	
		EXEC memory.spTriggerData_Create
			@ObjectName = @ObjectName,
			@DataXml = @DataXml,
			@DataString = @DataString,
			@DataBinary = @DataBinary,
			@CreatedBy = @CreatedBy,
			@TriggerToken = @TriggerToken OUTPUT
		

		SET @Context_Info = CAST(@TriggerToken AS VARBINARY(100));
		
		-- Set the context info.
		SET CONTEXT_INFO @Context_Info;
	
	END TRY
	BEGIN CATCH
	
	END CATCH
	
	
	
	
	
	
	
	
	
	
END
