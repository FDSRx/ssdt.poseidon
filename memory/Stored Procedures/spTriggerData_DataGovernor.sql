﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/13/2015
-- Description:	Governs the trigger data table and prevents memory leaks from occurring. 
-- SAMPLE CALL:
/*

	EXEC memory.spTriggerData_DataGovernor
		@ExpiredDataMinutes = 10,
		@Debug = 1

*/

-- SELECT * FROM memory.TriggerData
-- =============================================
CREATE PROCEDURE [memory].[spTriggerData_DataGovernor]
	@ExpiredDataMinutes INT = NULL,
	@InitiatedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL


	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@ExpiredDataMinutes=' + dbo.fnToStringOrEmpty(@ExpiredDataMinutes) + ';' +
		'@InitiatedBy=' + dbo.fnToStringOrEmpty(@InitiatedBy) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @ExpiredDataMinutes = ISNULL(@ExpiredDataMinutes, 10);
	SET @Debug = ISNULL(@Debug, 0);

	--------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@DateThreshold DATETIME = DATEADD(MINUTE, ABS(@ExpiredDataMinutes) * -1, GETDATE());

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT @DateThreshold AS DateThreshold
	END

	--------------------------------------------------------------------------------------------------------
	-- Manage data.
	-- <Summary>
	-- Inspects the data and determines if a cleanup process needs to be performed to maintain "memory"
	-- leaks.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	DELETE obj
	--SELECT *
	FROM memory.TriggerData obj
	WHERE obj.DateCreated <= @DateThreshold

END
