﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/5/2014
-- Description:	Stores a reference (pointer) to additional data that is required by the trigger.
-- SAMPLE CALL:
/*
DECLARE @TriggerToken UNIQUEIDENTIFIER

EXEC memory.spContextInfo_TriggerData_Set
	@ObjectName = 'TestObject',
	@DataString = 'dhughes',
	@TriggerToken = @TriggerToken OUTPUT
	
SELECT @TriggerToken AS TriggerToken

DECLARE
	--@TriggerToken UNIQUEIDENTIFIER,
	@ObjectName VARCHAR(256),
	@DataXml XML,
	@DataString VARCHAR(MAX),
	@DataBinary VARBINARY(MAX)
	
EXEC memory.spContextInfo_TriggerData_Get
	@TriggerToken = @TriggerToken OUTPUT,
	@ObjectName = @ObjectName OUTPUT,
	@DataXml = @DataXml OUTPUT,
	@DataString = @DataString OUTPUT,
	@DataBinary = @DataBinary OUTPUT

SELECT @TriggerToken AS TriggerToken, @ObjectName AS ObjectName, @DataXml AS DataXml, @DataString AS DataString, @DataBinary AS DataBinary

*/

-- SELECT * FROM memory.TriggerData
-- =============================================
CREATE PROCEDURE [memory].[spContextInfo_TriggerData_Get]
	@ObjectName VARCHAR(256)= NULL OUTPUT,
	@DataXml XML = NULL OUTPUT,
	@DataString VARCHAR(MAX) = NULL OUTPUT,
	@DataBinary VARBINARY(MAX) = NULL OUTPUT,
	@TriggerToken UNIQUEIDENTIFIER = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------	
	SET @TriggerToken = NULL;
	
	--------------------------------------------------------------------------------------------------------
	-- Retrieve trigger data.
	-- <Summary>
	-- Retrieves the TriggerData's object unique identiifer from the 
	-- context data (sql type session).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------		
	BEGIN TRY
	
		--------------------------------------------------------------------------------------------------------
		-- Set variables.
		--------------------------------------------------------------------------------------------------------		
		SET @TriggerToken = CAST(CONTEXT_INFO() AS UNIQUEIDENTIFIER);
	
		--------------------------------------------------------------------------------------------------------
		-- Record request.
		--------------------------------------------------------------------------------------------------------
		-- Log incoming arguments
		/*
		DECLARE @Args VARCHAR(MAX) =
			'@ObjectName=' + dbo.fnToStringOrEmpty(@ObjectName) + ';' +
			'@DataXml=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @DataXml)) + ';' +
			'@DataString=' + dbo.fnToStringOrEmpty(@DataString) + ';' +
			'@DataBinary=' + dbo.fnToStringOrEmpty(@DataBinary) + ';' +
			'@TriggerToken=' + dbo.fnToStringOrEmpty(@TriggerToken) ;

		EXEC dbo.spLogInformation
			@InformationProcedure = @ProcedureName,
			@Message = 'The request was fired.',
			@Arguments = @Args
		*/

		--------------------------------------------------------------------------------------------------------
		-- Retrieve trigger data.
		--------------------------------------------------------------------------------------------------------
		EXEC memory.spTriggerData_Get
			@TriggerToken = @TriggerToken,
			@ObjectName = @ObjectName OUTPUT,
			@DataXml = @DataXml OUTPUT,
			@DataString = @DataString OUTPUT,
			@DataBinary = @DataBinary OUTPUT		

		--------------------------------------------------------------------------------------------------------
		-- Remove trigger data.
		--------------------------------------------------------------------------------------------------------
		EXEC memory.spTriggerData_Delete
			@TriggerToken = @TriggerToken
			
	END TRY
	BEGIN CATCH
	
	END CATCH
	
	
	
	
	
	
	
	
	
	
END
