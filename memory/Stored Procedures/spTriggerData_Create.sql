﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/5/2014
-- Description:	Creates a new TriggerData object.
-- SAMPLE CALL:
/*
DECLARE @TriggerToken UNIQUEIDENTIFIER

EXEC memory.spTriggerData_Create
	@ObjectName = 'TestObject',
	@DataString = 'dhughes',
	@TriggerToken = @TriggerToken OUTPUT
	
SELECT @TriggerToken AS TriggerToken

*/

-- SELECT * FROM memory.TriggerData
-- =============================================
CREATE PROCEDURE memory.spTriggerData_Create
	@ObjectName VARCHAR(256) = NULL,
	@DataXml XML = NULL,
	@DataString VARCHAR(MAX) = NULL,
	@DataBinary VARBINARY(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@TriggerToken UNIQUEIDENTIFIER = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------------------
	SET @TriggerToken = NEWID();

	BEGIN TRY
		
		INSERT INTO memory.TriggerData (
			TriggerToken,
			ObjectName,
			DataXml,
			DataString,
			DataBinary,
			CreatedBy
		)
		SELECT
			@TriggerToken AS TriggerToken,
			@ObjectName AS ObjectName,
			@DataXml AS DataXml,
			@DataString AS DataString,
			@DataBinary AS DataBinary,
			@CreatedBy AS CreatedBy
		
	
	END TRY
	BEGIN CATCH
	
	END CATCH
	
	
END
