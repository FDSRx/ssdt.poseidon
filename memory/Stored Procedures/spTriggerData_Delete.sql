﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/5/2014
-- Description:	Deletes a TriggerData object.
-- SAMPLE CALL:
/*
DECLARE @TriggerToken UNIQUEIDENTIFIER = 'AECCE8B2-C3EB-44C2-A938-2D6ABFEB47C8'

EXEC memory.spTriggerData_Delete
	@TriggerToken = @TriggerToken

*/

-- SELECT * FROM memory.TriggerData
-- =============================================
CREATE PROCEDURE [memory].[spTriggerData_Delete]
	@TriggerToken UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@TriggerToken=' + dbo.fnToStringOrEmpty(@TriggerToken) ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	----------------------------------------------------------------------------
	-- Deletes a TriggerData object.
	----------------------------------------------------------------------------
	BEGIN TRY
		
		DELETE
		FROM memory.TriggerData
		WHERE TriggerToken = @TriggerToken
		
	
	END TRY
	BEGIN CATCH
	
	END CATCH
	
	
END
