﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/5/2014
-- Description:	Retrieves a TriggerData object.
-- SAMPLE CALL:
/*

DECLARE 
	@TriggerToken UNIQUEIDENTIFIER = 'E3EEEC64-441A-46FF-99DC-A86902F94CBF',
	@DataXml XML = NULL,
	@DataString VARCHAR(MAX) = NULL,
	@DataBinary VARBINARY(MAX) = NULL

EXEC memory.spTriggerData_Get
	@TriggerToken = @TriggerToken,
	@DataXml = @DataXml OUTPUT,
	@DataString = @DataString OUTPUT,
	@DataBinary = @DataBinary OUTPUT

SELECT @DataXml AS DataXml, @DataString AS DataString, @DataBinary AS DataBinary

*/

-- SELECT * FROM memory.TriggerData
-- =============================================
CREATE PROCEDURE [memory].[spTriggerData_Get]
	@TriggerToken UNIQUEIDENTIFIER,
	@ObjectName VARCHAR(256) = NULL OUTPUT,
	@DataXml XML = NULL OUTPUT,
	@DataString VARCHAR(MAX) = NULL OUTPUT,
	@DataBinary VARBINARY(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------
	SET @ObjectName = NULL;
	SET @DataXml = NULL;
	SET @DataString = NULL;
	SET @DataBinary = NULL;
	
	
	----------------------------------------------------------------------------
	-- Retrieve a TriggerData object.
	----------------------------------------------------------------------------
	SELECT
		@DataXml = DataXml,
		@DataString = DataString,
		@DataBinary = DataBinary
	FROM memory.TriggerData
	WHERE TriggerToken = @TriggerToken
		
	
	
END
