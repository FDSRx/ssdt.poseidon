﻿CREATE TABLE [memory].[TriggerData] (
    [TriggerToken] UNIQUEIDENTIFIER CONSTRAINT [DF_TriggerData_TriggerToken] DEFAULT (newid()) NOT NULL,
    [ObjectName]   VARCHAR (256)    NULL,
    [DataXml]      XML              NULL,
    [DataString]   VARCHAR (MAX)    NULL,
    [DataBinary]   VARBINARY (MAX)  NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_TriggerData_DateCreated] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_TriggerData] PRIMARY KEY CLUSTERED ([TriggerToken] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_TriggerData_TriggerToken]
    ON [memory].[TriggerData]([TriggerToken] ASC);

