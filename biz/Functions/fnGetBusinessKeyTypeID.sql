﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/16/2015
-- Description:	Returns the BusinessKeyType object identifier.
-- SAMPLE CALL: SELECT biz.fnGetBusinessKeyTypeID('NABP');

-- SElECT * FROM biz.BusinessKeyType
-- =============================================
CREATE FUNCTION [biz].[fnGetBusinessKeyTypeID]
(
	@Key VARCHAR(25)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @ID INT;

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT TOP 1 BusinessKeyTypeID FROM biz.BusinessKeyType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @ID = (SELECT TOP 1 BusinessKeyTypeID FROM biz.BusinessKeyType WHERE BusinessKeyTypeID = @Key);
	END

	-- Return the result of the function
	RETURN @ID;

END

