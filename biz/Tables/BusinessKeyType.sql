﻿CREATE TABLE [biz].[BusinessKeyType] (
    [BusinessKeyTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]              VARCHAR (25)     NOT NULL,
    [Name]              VARCHAR (256)    NOT NULL,
    [Description]       VARCHAR (1000)   NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessKeyType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_BusinessKeyType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_BusinessKeyType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessKeyType] PRIMARY KEY CLUSTERED ([BusinessKeyTypeID] ASC) WITH (FILLFACTOR = 90)
);

