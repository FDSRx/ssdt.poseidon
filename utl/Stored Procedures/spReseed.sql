﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/19/2013
-- Description:	Reseeds table
-- SAMPLE CALL: utl.spReseed @Namespace = 'Athena.spt.Ticket'
-- SAMPLE CALL: utl.spReseed @Namespace = 'spt.Ticket'
-- SAMPLE CALL: utl.spReseed @Namespace = 'Ticket' -- Invalid sample call
-- SAMPLE CALL: utl.spReseed @Namespace = '' -- Invalid sample call (null or empty)
-- SAMPLE CALL: utl.spReseed @Namespace = 'spt.Ticket', @Seed = 0
/*
-- TO USE:
-- Type the namespace of the table you wish to reseed. If you do not pass a new seed it will seed the table to the last known
-- value of the identity column.  For example, if the table has an identity column with value 10, it will reseed to 10. If
-- the schema is dbo bound, then you do not need to pass the schema.  
-- SEE WORKING SAMPLES ABOVE.
*/
-- =============================================
CREATE PROCEDURE [utl].[spReseed]
	@Namespace VARCHAR(1000),
	@Seed INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------
	-- Local variables
	-----------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@Sql VARCHAR(MAX) = NULL,
		@DbName VARCHAR(255) = NULL,
		@SchemaName VARCHAR(255) = NULL,
		@TableName VARCHAR(255) = NULL,
		@ColumnName VARCHAR(255) = NULL
		
	DECLARE @tblSeed AS TABLE (RowCnt INT, Seed INT);
	
	-----------------------------------------------------
	-- Verify object reference
	-----------------------------------------------------
	-- Verifiy an object was passed.
	IF ISNULL(@Namespace, '') = ''
	BEGIN

		SET @ErrorMessage = 'Object reference (@Namespace) is not set to an instance of an object. The object cannot be null or empty.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	
	END
	
	-- Verify the object exists	
	IF OBJECT_ID(@Namespace) IS NULL
	BEGIN

		SET @ErrorMessage = 'Object reference (' + ISNULL(@Namespace, '@Namespace <Empty Object>') + ') does not exist.'
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	
	END	
	
	-- Check if the object has an identity column
	IF ((SELECT OBJECTPROPERTY( OBJECT_ID(@Namespace), 'TableHasIdentity')) = 0)
	BEGIN
		
		SET @ErrorMessage = 'Object reference (' + ISNULL(@Namespace, '@Namespace <Empty Object>') + ') does not have an identity column.'
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	-----------------------------------------------------
	-- Parse namespace
	-----------------------------------------------------
	EXEC utl.spParseNamespace
		@Namespace = @Namespace,
		@Database  = @DbName OUTPUT,
		@Schema = @SchemaName OUTPUT,
		@Table = @TableName OUTPUT
	
	-- debug
	-- SELECT @DbName AS DatabaseName, @SchemaName AS SchemaName, @TableName AS TableName
	
	IF @Seed IS NULL
	BEGIN
	
		EXEC utl.spGetObjectIdentityColumn
			@Namespace = @Namespace,
			@Column = @ColumnName OUTPUT
		
		-- Debug
		-- SELECT * FROM @tblColumn
		
		IF @ColumnName IS NULL
		BEGIN

			SET @ErrorMessage = 'Unable to locate identity column for object (' + ISNULL(@Namespace, '@Namespace <Empty Object>') + ').  Please supply a seed for the specified table.'
			
			RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			  
			 RETURN;
		END
			
	END


	-----------------------------------------------------
	-- Calculate new seed
	-----------------------------------------------------
	IF @Seed IS NULL AND ISNULL(@ColumnName, '') <> ''
	BEGIN
	
		SET @Sql = 'SELECT COUNT(*) AS RowCnt, MAX(' + @ColumnName + ') AS CurrentIdentity FROM ' + @Namespace;

		INSERT INTO @tblSeed (RowCnt, Seed)
		EXEC(@Sql);	
		
		-- Debug
		-- SELECT * FROM @tblSeed
		
	END
	
	
	------------------------------------------------------
	-- Seed logic
	-- 1. If a seed was supplied then reseed with supplied.
	-- 2. If a seed was not supplied and the table does not have any rows
	--		then reseed to 0.
	-- 3. Try and seed from the last max record of the identity column
	------------------------------------------------------
	
	-- 1. If a seed was supplied then reseed with supplied value.
	IF @Seed IS NOT NULL
	BEGIN
		
		SET @Sql = 'DBCC CHECKIDENT (' + '''' + @Namespace + '''' + ', reseed, ' + CONVERT(VARCHAR(MAX), @Seed) + ')';
		--PRINT @Sql
		EXEC(@Sql);
		
		RETURN;
	
	END	
	
	-- 2. If the table does not have any rows and no seed was supplied then reseed to 0.
	IF (SELECT TOP 1 ISNULL(RowCnt, -1) FROM @tblSeed) = 0 AND @Seed IS NULL
	BEGIN
		
		SET @Sql = 'DBCC CHECKIDENT (' + '''' + @Namespace + '''' + ', reseed, 0)';
		--PRINT @Sql
		EXEC(@Sql);
		
		RETURN;
	END
	
	-- 3. If a seed was not supplied and the table has rows, then reseed with the max value of the identity column.
	IF (SELECT TOP 1 Seed FROM @tblSeed) IS NOT NULL AND @Seed IS NULL
	BEGIN
		
		SET @Seed = (SELECT TOP 1 Seed FROM @tblSeed);
		
		SET @Sql = 'DBCC CHECKIDENT (' + '''' + @Namespace + '''' + ', reseed, ' + CONVERT(VARCHAR(MAX), @Seed) + ')';
		--PRINT @Sql
		EXEC(@Sql);	
		
		RETURN;
	END





	
	

	
	
END








