﻿

-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/13/2013
-- Description:	Searches stored procedures for specific text
-- SAMPLE CALL: 
/*

DECLARE
	@Keywords VARCHAR(MAX) = 'utl.',
	@Delimiter VARCHAR(10) = NULL,
	@ResultSet VARCHAR(50) = NULL,
	@Result VARCHAR(MAX) = NULL

EXEC [utl].[spFindObject] 
	@Keywords = @Keywords,
	@Delimiter = @Delimiter,
	@ResultSet = @ResultSet,
	@Result = @Result OUTPUT

SELECT @Result AS Result

*/
-- =============================================
CREATE PROCEDURE [utl].[spFindObject]
	@Keywords VARCHAR(MAX),
	@Delimiter VARCHAR(10) = NULL,
	@ResultSet VARCHAR(50) = NULL,
	@Result VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	IF dbo.fnTrim(ISNULL(@Keywords, '')) = ''
	BEGIN
		RETURN; -- Exit lookup if a NULL or empty string is passed to be searched
	END
	
	-------------------------------------------------------------------------------------------
	-- Sanitize input
	-------------------------------------------------------------------------------------------
	SET @Delimiter = ISNULL(@Delimiter, ',');
	SET @ResultSet = ISNULL(@ResultSet, 'FULL');
	
	-------------------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------------------
	DECLARE 
		@Sql VARCHAR(MAX),
		@IsInitialPass BIT = 1,
		@ProcedureName VARCHAR(300)= OBJECT_NAME(@@PROCID),
		@SchemaName VARCHAR(25) = OBJECT_SCHEMA_NAME(@@PROCID)
	
	
	-------------------------------------------------------------------------------------------
	-- Temporary resources
	-------------------------------------------------------------------------------------------

	-- Search results object
	IF OBJECT_ID('tempdb..#tmpSearchResults') IS NOT NULL
	BEGIN
		DROP TABLE #tmpSearchResults;
	END
	
	CREATE TABLE #tmpSearchResults (
		Idx INT IDENTITY(1,1),
		ObjectType VARCHAR(256),
		Name VARCHAR(500),
		Definition VARCHAR(MAX),
		SqlObject XML
	);

	-- Results object
	IF OBJECT_ID('tempdb..#tmpResults') IS NOT NULL
	BEGIN
		DROP TABLE #tmpResults;
	END
	
	CREATE TABLE #tmpResults (
		Idx INT IDENTITY(1,1),
		ObjectType VARCHAR(256),
		Name VARCHAR(500),
		Definition VARCHAR(MAX),
		Keyword VARCHAR(MAX),
		Match VARCHAR(MAX),
		Position INT,
		SqlObject XML
	);
	
	-- Keywords object
	IF OBJECT_ID('tempdb..#tmpKeywords') IS NOT NULL
	BEGIN
		DROP TABLE #tmpKeywords;
	END
	
	CREATE TABLE #tmpKeywords (
		Idx INT IDENTITY(1,1),
		Value VARCHAR(MAX)
	);
	
	-------------------------------------------------------------------------------------------
	-- Parse keywords
	-------------------------------------------------------------------------------------------
	INSERT INTO #tmpKeywords (
		Value
	)
	SELECT
		dbo.fnTrim(Value)
	FROM dbo.fnSplit(@Keywords, @Delimiter)
	WHERE dbo.fnTrim(ISNULL(Value, '')) <> ''
	
	
	-------------------------------------------------------------------------------------------
	-- Write SQL
	-------------------------------------------------------------------------------------------
	SET @Sql = 
		'SELECT' + CHAR(10) +
			'CASE' + CHAR(10) +
				'WHEN o.type IN (''FN'', ''IF'', ''TF'', ''FS'', ''FT'') THEN ''Function''' + CHAR(10) +
				'WHEN o.type IN (''U'') THEN ''Table''' + CHAR(10) +
				'WHEN o.type IN (''V'') THEN ''View''' + CHAR(10) +
				'WHEN o.type IN (''P'') THEN ''Stored Procedure''' + CHAR(10) +
				'WHEN o.type IN (''TR'') THEN ''Trigger''' + CHAR(10) +
			'END AS ObjectType,' + CHAR(10) +
			'ISNULL(s.name, ''<unknown schema>'') + ''.'' + ISNULL(o.name, ''<uknown object>'') AS Name,' + CHAR(10) +
			'OBJECT_DEFINITION(o.object_id) AS Definition,' + CHAR(10) +
			'CONVERT(XML, ''<SqlObject>'' + dbo.fnXmlWriteElementString(''Definition'', OBJECT_DEFINITION(o.object_id)) + ''</SqlObject>'') AS SqlObject' + CHAR(10) +
		'--SELECT *' + CHAR(10) +
		'FROM sys.objects o' + CHAR(10) +
			'JOIN sys.schemas s' + CHAR(10) +
				'ON o.schema_id = s.schema_id' + CHAR(10);
	

	-- Debug
	--SELECT @Sql AS SqlString
	PRINT @Sql

	-------------------------------------------------------------------------------------------
	-- Write object query for keywords
	-------------------------------------------------------------------------------------------
	DECLARE
		@CurIndex INT = (SELECT MIN(Idx) FROM #tmpKeywords),
		@EndIndex INT = (SELECT MAX(Idx) FROM #tmpKeywords)
	
	
	WHILE @CurIndex <= @EndIndex
	BEGIN
		
		DECLARE @Value VARCHAR(MAX) = NULL;
		
		SELECT
			@Value = dbo.fnEscapeSpecialChars(Value)
		FROM #tmpKeywords
		WHERE Idx = @CurIndex
		
		SET @Sql = ISNULL(@Sql, '') +
			CASE WHEN ISNULL(@IsInitialPass, 0) = 1 THEN 'WHERE (' + CHAR(10) ELSE 'OR ' END +
			'OBJECT_DEFINITION(o.object_id) LIKE ''%' + @Value + '%''' + CHAR(10);
		
		-- We have made our first pass; set to false for 'OR' to kick in
		SET @IsInitialPass = 0;
		
		-- Incriment current index
		SET @CurIndex = @CurIndex + 1;
	END
	
	
	SET @Sql = ISNULL(@Sql, '') + ')' + CHAR(10) +
		'AND o.type IN (''FN'', ''IF'', ''TF'', ''FS'', ''FT'', ''U'', ''V'', ''P'', ''TR'')' + CHAR(10) +
		'ORDER by ObjectType' + CHAR(10);
		
	-- Debug
	--PRINT @Sql
	
	-------------------------------------------------------------------------------------------
	-- Obtain search results.
	-------------------------------------------------------------------------------------------
	INSERT INTO #tmpSearchResults (
		ObjectType,
		Name,
		Definition,
		SqlObject
	)
	EXEC (@Sql);




	-------------------------------------------------------------------------------------------
	-- Create result list
	-------------------------------------------------------------------------------------------

	DECLARE
		@ObjectIndex INT = 0,
		@ObjectType VARCHAR(256) = NULL,
		@Name VARCHAR(256) = NULL,
		@Definition VARCHAR(MAX) = NULL,
		@SqlObject XML = NULL


	-------------------------------------------------------------------------------------------
	-- Loop through list of objects.
	-------------------------------------------------------------------------------------------
	SELECT TOP(1)
		@ObjectIndex = Idx,
		@ObjectType = ObjectType,
		@Name = Name,
		@Definition = Definition,
		@SqlObject = SqlObject
	FROM #tmpSearchResults
	WHERE Idx > @ObjectIndex
	ORDER BY Idx ASC

	WHILE @@ROWCOUNT = 1
	BEGIN

		DECLARE
			@KeywordIndex INT = 0,
			@Keyword VARCHAR(MAX) = NULL

		-------------------------------------------------------------------------------------------
		-- Loop through keyword list.
		-- <Summary>
		-- Iterate through the keyword listing and find all matches of the keyword within in the
		-- object definition.
		-- </Summary>
		-------------------------------------------------------------------------------------------
		SELECT TOP(1)
			@KeywordIndex = Idx,
			@Keyword = Value
		FROM #tmpKeywords
		WHERE Idx > @KeywordIndex
		ORDER BY Idx ASC

		WHILE @@ROWCOUNT = 1
		BEGIN

			--Debug
			--SELECT @Keyword AS Keyword

			DECLARE
				@Match VARCHAR(MAX) = NULL,
				@ContextLength INT = 30


			DECLARE
				@Position INT = CHARINDEX(@Keyword, @Definition)

			--Debug
			--SELECT @Position AS Position, @Definition AS Definition

			-------------------------------------------------------------------------------------------
			-- Find all search snippets.
			-------------------------------------------------------------------------------------------
			WHILE @Position != 0
			BEGIN
				
				SET @Match = 
					CASE 
						WHEN @Position > 0 AND ( @Position - @ContextLength ) > 0 AND ( @Position + LEN(@Keyword) + @ContextLength ) < LEN(@Definition)
							THEN SUBSTRING(@Definition, ( @Position- @ContextLength ), LEN(@Keyword) + 60  )
						WHEN @Position > 0 AND ( @Position - @ContextLength ) < 0 AND ( @Position + LEN(@Keyword) + @ContextLength  ) < LEN(@Definition)
							THEN CASE
								WHEN @Position > 0 AND ( @Position - 10 ) > 0 AND ( @Position + LEN(@Keyword) + @ContextLength  ) < LEN(@Definition)
									THEN SUBSTRING(@Definition, ( @Position - 10 ), LEN(@Keyword) + 60  )
								ELSE '<Unable to obtain match. A new interrogation definition is required.>' 
								END
						ELSE '<Unable to obtain match. A new interrogation defnition is required or this particular keyword from the keywords list does not apply.>'
					END


				--Debug
				--SELECT @Match AS Match

				-------------------------------------------------------------------------------------------
				-- Record match.
				-------------------------------------------------------------------------------------------
				INSERT INTO #tmpResults (
					ObjectType,
					Name,
					Definition,
					Keyword,
					Match,
					Position,
					SqlObject
				)
				SELECT
					@ObjectType,
					@Name,
					@Definition,
					@Keyword,
					@Match,
					@Position,
					@SqlObject


				-------------------------------------------------------------------------------------------
				-- Set the new position of the next match.
				-------------------------------------------------------------------------------------------
				SET @Position = CHARINDEX(@Keyword, @Definition, @Position + LEN(@Keyword));
				


			END


			-------------------------------------------------------------------------------------------
			-- Fetch next keyword from list of keywords.
			-- <Remarks>
			-- Do not place any code beyond this point.  It simulates a cursor method by
			-- determing if there is another row to process.
			-- </Remarks>
			-------------------------------------------------------------------------------------------
			SELECT TOP(1)
				@KeywordIndex = Idx,
				@Keyword = dbo.fnEscapeSpecialChars(Value)
			FROM #tmpKeywords
			WHERE Idx > @KeywordIndex
			ORDER BY Idx ASC

		END


		-------------------------------------------------------------------------------------------
		-- Fetch next object from list of objects.
		-- <Remarks>
		-- Do not place any code beyond this point.  It simulates a cursor method by
		-- determing if there is another row to process.
		-- </Remarks>
		-------------------------------------------------------------------------------------------
		SELECT TOP(1)
			@ObjectIndex = Idx,
			@ObjectType = ObjectType,
			@Name = Name,
			@Definition = Definition,
			@SqlObject = SqlObject
		FROM #tmpSearchResults
		WHERE Idx > @ObjectIndex
		ORDER BY Idx ASC

	END



	-------------------------------------------------------------------------------------------
	-- Return results
	-------------------------------------------------------------------------------------------
	SET @Result = (
		SELECT
			ObjectType,
			Name,
			Definition,
			Keyword,
			Match,
			Position,
			SqlObject
		FROM #tmpResults
		ORDER BY 
			ObjectType, 
			Name, 
			Keyword,
			Position
		FOR XML PATH('Result'), ROOT('Results')
	);

	IF @ResultSet IN ('XML')
	BEGIN

		SELECT @Result As Result

	END
	ELSE IF @ResultSet IN ('OUT', 'OUTPUT')
	BEGIN
		-- Place holder
		SET @Result = @Result;
	END
	ELSE
	BEGIN

		SELECT
			ObjectType,
			Name,
			Definition,
			Keyword,
			Match,
			Position,
			SqlObject
		FROM #tmpResults
		ORDER BY 
			ObjectType, 
			Name, 
			Keyword,
			Position

	END
	







	
	-------------------------------------------------------------------------------------------
	-- Dispose of objects.
	-------------------------------------------------------------------------------------------
	DROP TABLE #tmpKeywords;
	DROP TABLE #tmpSearchResults;
	DROP TABLE #tmpResults;


END