﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/16/2013
-- Description:	Parses the database namespace into its appropriate pecies
-- SAMPLE CALL:
/*
DECLARE 
	@Server VARCHAR(255) = NULL,
	@Database VARCHAR(255) = NULL,
	@Schema VARCHAR(255) = NULL,
	@Table VARCHAR(255) = NULL

EXEC utl.spParseNamespace
	@Namespace = 'Athena.dbo.Users',
	@Server = @Server OUTPUT,
	@Database  = @Database OUTPUT,
	@Schema = @Schema OUTPUT,
	@Table = @Table OUTPUT

SELECT @Server AS ServerName, @Database AS DatabaseName, @Schema AS SchemaName, @Table AS TableName
*/
-- =============================================
CREATE PROCEDURE [utl].[spParseNamespace]
	@Namespace VARCHAR(1000),
	@Server VARCHAR(255) = NULL OUTPUT,
	@Database VARCHAR(255) = NULL OUTPUT,
	@Schema VARCHAR(255) = NULL OUTPUT,
	@Table VARCHAR(255) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------
	SET @Server = NULL;
	SET @Database = NULL;
	SET @Schema = NULL;
	SET @Table = NULL;
	
	-----------------------------------------------------
	-- Local variables
	-----------------------------------------------------
	DECLARE @tblNamespace AS TABLE (Idx INT IDENTITY(1,1), Value VARCHAR(255));	
	
	-----------------------------------------------------
	-- Parse namespace
	-- <Summary>
	-- Splits a given namespace into its corresponding
	-- peices.  For example Athena.dbo.Users would equal
	-- Table = Users; Schema = dbo; Database = Athena.
	-- </Summary>
	-----------------------------------------------------
	INSERT INTO @tblNamespace (
		Value
	)
	SELECT Value
	FROM dbo.fnSplit(@Namespace, '.')
	
	DECLARE @RowCount INT = @@ROWCOUNT;
	
	-- Parse namespace
	IF @RowCount = 1
	BEGIN
		SET @Table = (SELECT TOP 1 Value FROM @tblNamespace WHERE Idx = 1);
	END
	ELSE IF @RowCount = 2
	BEGIN
		SET @Schema = (SELECT TOP 1 Value FROM @tblNamespace WHERE Idx = 1);
		SET @Table = (SELECT TOP 1 Value FROM @tblNamespace WHERE Idx = 2);
	END
	ELSE IF @RowCount = 3
	BEGIN
		SET @Database = (SELECT TOP 1 Value FROM @tblNamespace WHERE Idx = 1);
		SET @Schema = (SELECT TOP 1 Value FROM @tblNamespace WHERE Idx = 2);
		SET @Table = (SELECT TOP 1 Value FROM @tblNamespace WHERE Idx = 3);
	END
	ELSE IF @RowCount = 4
	BEGIN
		SET @Server = (SELECT TOP 1 Value FROM @tblNamespace WHERE Idx = 1);
		SET @Database = (SELECT TOP 1 Value FROM @tblNamespace WHERE Idx = 2);
		SET @Schema = (SELECT TOP 1 Value FROM @tblNamespace WHERE Idx = 3);
		SET @Table = (SELECT TOP 1 Value FROM @tblNamespace WHERE Idx = 4);
	END
	
	
	-- Set defaults
	SET @Server = ISNULL(@Server, @@SERVERNAME);
	SET @Database = ISNULL(@Database, DB_NAME());
	SET @Schema = ISNULL(@Schema, SCHEMA_NAME());
	
END
