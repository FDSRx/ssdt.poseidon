﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/16/2013
-- Description:	Determines if the table has an identity column
-- SAMPLE CALL:
/*
DECLARE @HasIdentity BIT, @Column VARCHAR(255)
EXEC utl.spGetObjectIdentityColumn
	@Namespace = 'spt.AuthorEntity',
	@HasIdentity = @HasIdentity OUTPUT,
	@Column = @Column OUTPUT
SELECT @HasIdentity AS HasIdentity, @Column AS ColumnName
*/
-- =============================================
CREATE PROCEDURE [utl].[spGetObjectIdentityColumn]
	@Namespace VARCHAR(1000),
	@HasIdentity BIT = NULL OUTPUT,
	@Column VARCHAR(255) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------
	SET @HasIdentity = 0;
	SET @Column = NULL;
	
	------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------
	DECLARE
		@Sql VARCHAR(MAX) = NULL,
		@ErrorMessage VARCHAR(4000) = NULL,
		@Server VARCHAR(255) = NULL,
		@Database VARCHAR(255) = NULL,
		@Schema VARCHAR(255) = NULL,
		@Table VARCHAR(255) = NULL
	
	DECLARE @tblColumn AS TABLE (ColumnName VARCHAR(255));	
	
	-----------------------------------------------------
	-- Verify object reference
	-----------------------------------------------------
	-- Verifiy an object was passed.
	IF ISNULL(@Namespace, '') = ''
	BEGIN

		SET @ErrorMessage = 'Object reference (' + ISNULL(@Namespace, '<Empty Object>') + ') is not set to an instance of an object.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	
	END
	
	-- Verify the object exists	
	IF OBJECT_ID(@Namespace) IS NULL
	BEGIN

		SET @ErrorMessage = 'Object reference (' + ISNULL(@Namespace, '<Empty Object>') + ') does not exist.'
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	
	END	
		
	------------------------------------------------------------
	-- Parse namespace
	------------------------------------------------------------		
	EXEC utl.spParseNamespace
		@Namespace = @Namespace,
		@Server = @Server OUTPUT,
		@Database  = @Database OUTPUT,
		@Schema = @Schema OUTPUT,
		@Table = @Table OUTPUT


	------------------------------------------------------------
	-- Inspect object listing for identity column
	------------------------------------------------------------			
	SET @Sql = 'SELECT MAX(i.name)' + CHAR(10) +
		'FROM  ' + @Database + '.sys.objects o' + CHAR(10) +
			'JOIN ' + @Database + '.sys.columns c' + CHAR(10) +
				'ON o.object_id = c.object_id' + CHAR(10) +
			'JOIN ' + @Database + '.sys.schemas s' + CHAR(10) +
				'ON o.schema_id = s.schema_id' + CHAR(10) +
			'JOIN ' + @Database + '.sys.identity_columns i' + CHAR(10) +
				'ON o.object_id = i.object_id' + CHAR(10) +	
		'WHERE s.name = ' + '''' + @Schema + '''' + CHAR(10) +
			'AND o.name = ' + '''' + @Table  + '''' + CHAR(10)
	

	-- Debug
	-- PRINT @Sql;
	
	-- Excute dynamic sql
	INSERT INTO @tblColumn (ColumnName)
	EXEC (@Sql);
	
	-- Debug
	-- SELECT * FROM @tblColumn
	
	------------------------------------------------------------
	-- Determine if the object has an identity.
	------------------------------------------------------------
	IF (SELECT COUNT(*) FROM @tblColumn WHERE ColumnName IS NOT NULL) <> 0
	BEGIN
		SET @Column = (SELECT TOP 1 ColumnName FROM @tblColumn WHERE ColumnName IS NOT NULL);
		SET @HasIdentity = 1;
	END

		
		
END
