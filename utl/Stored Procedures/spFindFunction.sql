﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/13/2013
-- Description:	Searches functions for specific text and returns matched functions
-- SAMPLE CALL: utl.spFindFunction @Keywords = 'PageID, Page, getPageID, ticket'
-- =============================================
CREATE PROCEDURE [utl].[spFindFunction]
	@Keywords VARCHAR(MAX),
	@Delimiter VARCHAR(10) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	IF dbo.fnTrim(ISNULL(@Keywords, '')) = ''
	BEGIN
		RETURN; -- Exit lookup if a NULL or empty string is passed to be searched
	END
	
	-------------------------------------------
	-- Default data
	-------------------------------------------
	SET @Delimiter = ISNULL(@Delimiter, ',');
	
	-------------------------------------------
	-- Local variables
	-------------------------------------------
	DECLARE 
		@Sql VARCHAR(MAX),
		@IsInitialPass BIT = 1,
		@ProcedureName VARCHAR(300)= OBJECT_NAME(@@PROCID),
		@SchemaName VARCHAR(25) = OBJECT_SCHEMA_NAME(@@PROCID)

	-------------------------------------------
	-- Temporary resources
	-------------------------------------------
	IF OBJECT_ID('tempdb..#tmpSearchResults') IS NOT NULL
	BEGIN
		DROP TABLE #tmpSearchResults;
	END
	
	CREATE TABLE #tmpSearchResults (
		Name VARCHAR(500),
		Definition VARCHAR(MAX),
		Snippet VARCHAR(MAX),
		Position INT
	)
	
	-------------------------------------------
	-- Build key words table
	-------------------------------------------
	IF OBJECT_ID('tempdb..#tmpKeywords') IS NOT NULL
	BEGIN
		DROP TABLE #tmpKeywords;
	END
	
	CREATE TABLE #tmpKeywords (
		Idx INT IDENTITY(1,1),
		Value VARCHAR(MAX)
	)
	
	-------------------------------------------
	-- Parse keywords
	-------------------------------------------
	INSERT INTO #tmpKeywords (
		Value
	)
	SELECT
		dbo.fnTrim(Value)
	FROM dbo.fnSplit(@Keywords, @Delimiter)
	WHERE dbo.fnTrim(ISNULL(Value, '')) <> ''
	
	
	-------------------------------------------
	-- Write SQL
	-------------------------------------------
	SET @Sql = 
		'SELECT ISNULL(s.name, ''<unknown>'') + ''.'' +  o.name AS name, m.definition' + CHAR(10) +
		'FROM sys.objects o' + CHAR(10) +
			'JOIN sys.sql_modules m' + CHAR(10) +
				'ON o.object_id = m.object_id' + CHAR(10) +
			'LEFT JOIN sys.schemas s' + CHAR(10) +
				'ON o.schema_id = s.schema_id' + CHAR(10)


	-- Debug
	-- PRINT @Sql

	-------------------------------------------
	-- Write object query for keywords
	-------------------------------------------
	DECLARE
		@CurIndex INT = (SELECT MIN(Idx) FROM #tmpKeywords),
		@EndIndex INT = (SELECT MAX(Idx) FROM #tmpKeywords)
	
	
	WHILE @CurIndex <= @EndIndex
	BEGIN
		
		DECLARE @Value VARCHAR(MAX) = NULL;
		
		SELECT
			@Value = Value
		FROM #tmpKeywords
		WHERE Idx = @CurIndex
		
		SET @Sql = ISNULL(@Sql, '') +
			CASE WHEN ISNULL(@IsInitialPass, 0) = 1 THEN 'WHERE (' + CHAR(10) ELSE 'OR ' END +
			'OBJECT_DEFINITION(m.object_id) LIKE ''%' + @Value + '%''' + CHAR(10);
		
		-- We have made our first pass; set to false for 'OR' to kick in
		SET @IsInitialPass = 0;
		
		-- Incriment current index
		SET @CurIndex = @CurIndex + 1;
	END	
	
	SET @Sql = ISNULL(@Sql, '') + ')' + CHAR(10) +
		'AND o.type IN (''FN'', ''IF'', ''TF'', ''FS'', ''FT'')'
	
	-- Debug
	--PRINT @Sql
	
	-------------------------------------------
	-- Fetch results
	-------------------------------------------
	INSERT INTO #tmpSearchResults (
		Name,
		Definition
	)
	EXEC (@Sql);


	-------------------------------------------
	-- Return results
	-------------------------------------------
	DECLARE
		@Word VARCHAR(MAX) = (SELECT TOP 1 Value FROM #tmpKeywords WHERE Idx = 1);

	SELECT
		Name,
		Definition,
		@Word AS KeywordSnipped,
		CASE 
			WHEN CHARINDEX(@Word, Definition) > 0 AND ( CHARINDEX(@Word, Definition) - 30 ) > 0 AND ( CHARINDEX(@Word, Definition) + LEN(@Word) + 30 ) < LEN(Definition)
				THEN SUBSTRING(Definition, ( CHARINDEX(@Word, Definition) - 30 ), LEN(@Word) + 60  )
			WHEN CHARINDEX(@Word, Definition) > 0 AND ( CHARINDEX(@Word, Definition) - 30 ) < 0 AND ( CHARINDEX(@Word, Definition) + LEN(@Word) + 30  ) < LEN(Definition)
				THEN CASE
					WHEN CHARINDEX(@Word, Definition) > 0 AND ( CHARINDEX(@Word, Definition) - 10 ) > 0 AND ( CHARINDEX(@Word, Definition) + LEN(@Word) + 30  ) < LEN(Definition)
						THEN SUBSTRING(Definition, ( CHARINDEX(@Word, Definition) - 10 ), LEN(@Word) + 60  )
					ELSE '<Unable to obtain snippet. A new interrogation defnition is required.>' 
					END
			ELSE '<Unable to obtain snippet. A new interrogation defnition is required or this particular keyword from the keywords list does not apply.>'
		END AS Snippet,
		CHARINDEX(@Word, Definition) AS Position		
	FROM #tmpSearchResults
	
	
	-------------------------------------------
	-- Release resources
	-------------------------------------------
	DROP TABLE #tmpKeywords;

END








