﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/6/2014
-- Description:	Gets the columns and their definitions of the specified table
-- SAMPLE CALL: 
/*
DECLARE @ColumnData XML

EXEC utl.spGetTableColumnInformation 
	@Namespace = 'IPP_Dev.dbo.Document,Athena.dbo.Business',
	@ColumnData = @ColumnData OUTPUT,
	@IsResultXml = 0

SELECT @ColumnData AS ColumnData

*/
-- =============================================
CREATE PROCEDURE utl.spGetTableColumnInformation
	@Namespace VARCHAR(1000),
	@IsResultXml BIT = 0,
	@IsXmlOutputOnly BIT = 0,
	@ColumnData XML = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @ColumnData = NULL;
	SET @IsResultXml = ISNULL(@IsResultXml, 0);
	SET @IsXmlOutputOnly = ISNULL(@IsXmlOutputOnly, 0);
	
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@Sql VARCHAR(MAX) = NULL,
		@Server varchar(255) = NULL,
		@Database VARCHAR(255) = NULL,
		@Schema VARCHAR(255) = NULL,
		@Table VARCHAR(255) = NULL
		
		
	----------------------------------------------------------------------
	-- Verify object reference
	----------------------------------------------------------------------
	-- Verifiy an object was passed.
	IF ISNULL(@Namespace, '') = ''
	BEGIN

		SET @ErrorMessage = 'Object reference (@Namespace) is not set to an instance of an object. The object cannot be null or empty.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	
	END
	
	
	/*
	-- Verify the object exists	
	IF OBJECT_ID(@Namespace) IS NULL
	BEGIN

		SET @ErrorMessage = 'Object reference (' + ISNULL(@Namespace, '@Namespace <Empty Object>') + ') does not exist.'
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	
	END	
	*/



	----------------------------------------------------------------------
	-- Create temporary resources
	----------------------------------------------------------------------
	CREATE TABLE #tmpColumnInfo (
		DatabaseName VARCHAR(255),
		SchemaName VARCHAR(255),
		TableName VARCHAR(255),
		ColumnName VARCHAR(255),
		DataType VARCHAR(255),
		DataSize INT,
		DataPrecision INT,
		IsIdentityColumn BIT
	);
	
	CREATE TABLE #tmpNamespaces (
		Idx INT IDENTITY(1,1),
		TableNamespace VARCHAR(1000)
	);
	
	
	----------------------------------------------------------------------
	-- Split multiple namespaces (if applicable)
	-- <Summary>
	-- If the caller provides a comma delimited list of namespaces
	-- then we need to split and process them all
	-- </Summary>
	----------------------------------------------------------------------
	INSERT INTO #tmpNamespaces (
		TableNamespace
	)
	SELECT
		Value
	FROM dbo.fnSplit(@Namespace, ',')
	WHERE LTRIM(RTRIM(ISNULL(Value, ''))) <> ''
	
	
	BEGIN TRY
	
		----------------------------------------------------------------------
		-- Process namespaces
		-- <Summary>
		-- Loop through each namespace and obtain table column infomration
		-- </Summary>
		----------------------------------------------------------------------
		-- Destroy existing namespace.
		SET @Namespace = NULL;
		
		DECLARE
			@Idx INT = 0
		
		----------------------------------------------------------------------
		-- Simulate cursor operation.
		----------------------------------------------------------------------
		SELECT TOP(1)
			@Idx = Idx,
			@Namespace = TableNamespace
		FROM #tmpNamespaces
		WHERE Idx > @Idx
		ORDER BY Idx ASC	
		
		WHILE @@ROWCOUNT = 1
		BEGIN
			----------------------------------------------------------------------
			-- Parse namespace
			----------------------------------------------------------------------		
			EXEC utl.spParseNamespace
				@Namespace = @Namespace,
				@Server = @Server OUTPUT,
				@Database  = @Database OUTPUT,
				@Schema = @Schema OUTPUT,
				@Table = @Table OUTPUT
				
					
			----------------------------------------------------------------------
			-- Build interrogation sql
			----------------------------------------------------------------------			
			SET @Sql = 
				'SELECT ' + CHAR(10) +
					'''' + @Database + '''' + ' AS DatabaseName ' + CHAR(10) +
					',' + '''' + @Schema + '''' + ' AS SchemaName ' + CHAR(10) +
					',o.Name AS TableName ' + CHAR(10) +
					', c.Name AS ColumnName '  + CHAR(10) +
					', t.Name AS DataType ' + CHAR(10) +
					', t.max_length AS DataSize ' + CHAR(10) +
					', t.precision AS DataPrecision ' + CHAR(10) +
					', CASE WHEN c.Name = i.Name THEN 1 ELSE 0 END AS IsIdentityColumn' + CHAR(10) +
				'FROM  ' + @Database + '.sys.objects o' + CHAR(10) +
					'JOIN ' + @Database + '.sys.columns c' + CHAR(10) +
						'ON o.object_id = c.object_id' + CHAR(10) +
					'JOIN ' + @Database + '.sys.schemas s' + CHAR(10) +
						'ON o.schema_id = s.schema_id' + CHAR(10) +
					'LEFT JOIN   ' + @Database + '.sys.types t ' + CHAR(10) +
						'ON t.system_type_id = c.system_type_id' + CHAR(10) +  
					'LEFT JOIN ' + @Database + '.sys.identity_columns i' + CHAR(10) +
						'ON o.object_id = i.object_id' + CHAR(10) +	
				'WHERE s.name = ' + '''' + @Schema + '''' + CHAR(10) +
					'AND o.name = ' + '''' + @Table  + '''' + CHAR(10)
			
			-- Debug
			--PRINT @Sql


			----------------------------------------------------------------------
			-- Get column info
			----------------------------------------------------------------------		
			INSERT INTO #tmpColumnInfo (
				DatabaseName,
				SchemaName,
				TableName,
				ColumnName,
				DataType,
				DataSize,
				DataPrecision,
				IsIdentityColumn 
			)
			EXEC (@Sql);
			
			----------------------------------------------------------------------
			-- Simulate cursor operation.
			----------------------------------------------------------------------
			SELECT TOP(1)
				@Idx = Idx,
				@Namespace = TableNamespace
			FROM #tmpNamespaces
			WHERE Idx > @Idx
			ORDER BY Idx ASC
			
			-- * NO CODE BELOW THIS POINT.  THE ABOVE SIMULATES A "CURSOR FETCH NEXT" OPERATION * --
		END
		
	END TRY
	BEGIN CATCH
		
		EXEC spPrintError
		
	END CATCH
	
	----------------------------------------------------------------------
	-- Return results
	----------------------------------------------------------------------		
	IF @IsResultXml = 0
	BEGIN
	
		SELECT
			DatabaseName,
			SchemaName,
			TableName,
			ColumnName,
			DataType,
			DataSize,
			DataPrecision,
			IsIdentityColumn
		FROM #tmpColumnInfo	
	
	END
	ELSE 
	BEGIN
	
		SET @ColumnData = (
			SELECT *
			FROM #tmpColumnInfo
			FOR XML PATH('Column'), ROOT('Columns')
		);
		
		IF @IsXmlOutputOnly = 0
		BEGIN
			SELECT @ColumnData AS Prescriptions
		END
		
	END

	
	----------------------------------------------------------------------
	-- Release resources
	----------------------------------------------------------------------	
	DROP TABLE #tmpColumnInfo;
	DROP TABLE #tmpNamespaces;
	
END
