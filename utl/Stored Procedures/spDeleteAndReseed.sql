﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/16/2013
-- Description:	Deletes data from the table and then reseeds
-- SAMPLE CALL: utl.spDeleteAndReseed 'spt.Ticket'
-- SELECT * FROM spt.Ticket
-- =============================================
CREATE PROCEDURE [utl].[spDeleteAndReseed]
	@Namespace VARCHAR(255),
	@Seed INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------
	-- Local variables
	-----------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@Sql VARCHAR(MAX) = NULL
		
	-----------------------------------------------------
	-- Verify object reference
	-----------------------------------------------------
	-- Verifiy an object was passed.
	IF ISNULL(@Namespace, '') = ''
	BEGIN

		SET @ErrorMessage = 'Object reference (' + ISNULL(@Namespace, '<Empty Object>') + ') is not set to an instance of an object.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	
	END

	-----------------------------------------------------
	-- Verify the object exists	
	-----------------------------------------------------
	IF OBJECT_ID(@Namespace) IS NULL
	BEGIN

		SET @ErrorMessage = 'Object reference (' + ISNULL(@Namespace, '<Empty Object>') + ') does not exist.'
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	
	END	
	

	
	-----------------------------------------------------
	-- Delete data from table
	-----------------------------------------------------
	SET @Sql = 'DELETE FROM ' + @Namespace
	
	EXEC (@Sql);
	
	-----------------------------------------------------
	-- Reseed table
	-- <Remarks>
	-- If the table does not have an identity column
	-- then let's silently exit the code vs. an error
	-- being thrown by going through the reseed process
	-- </Remarks>
	-----------------------------------------------------
	DECLARE
		@HasIdentity BIT = 0
	
	-- Check for identity
	EXEC utl.spGetObjectIdentityColumn
		@Namespace = @Namespace,
		@HasIdentity = @HasIdentity OUTPUT
	
	-- Reseed (if applicable)
	IF ISNULL(@HasIdentity, 0) = 1
	BEGIN
		EXEC utl.spReseed
			@Namespace = @Namespace,
			@Seed = @Seed
	END
	
	
	
END
