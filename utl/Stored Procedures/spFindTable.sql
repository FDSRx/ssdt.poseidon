﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/31/2015
-- Description:	Searches Table information for the specified criteria.
-- SAMPLE CALL: 
/*

EXEC utl.spFindTable
	@Keywords = 'SourcePatientKey',
	@Debug = 1

*/
-- =============================================
CREATE PROCEDURE [utl].[spFindTable]
	@Keywords VARCHAR(MAX),
	@Delimiter VARCHAR(10) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	IF dbo.fnTrim(ISNULL(@Keywords, '')) = ''
	BEGIN
		RETURN; -- Exit lookup if a NULL or empty string is passed to be searched
	END
	
	---------------------------------------------------------------------------------------------------------
	-- Sanitize data.
	---------------------------------------------------------------------------------------------------------
	SET @Delimiter = ISNULL(@Delimiter, ',');
	SET @Debug = ISNULL(@Debug, 0);
	
	---------------------------------------------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------------------------------------------
	DECLARE 
		@Sql VARCHAR(MAX),
		@IsInitialPass BIT = 1,
		@ProcedureName VARCHAR(300)= OBJECT_NAME(@@PROCID),
		@SchemaName VARCHAR(25) = OBJECT_SCHEMA_NAME(@@PROCID),
		@CurIndex INT,
		@EndIndex INT,
		@Value VARCHAR(MAX)
		
	
	
	---------------------------------------------------------------------------------------------------------
	-- Temporary resources
	---------------------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpSearchResults') IS NOT NULL
	BEGIN
		DROP TABLE #tmpSearchResults;
	END
	
	CREATE TABLE #tmpSearchResults (
		DatabaseName VARCHAR(256),
		SchemaName VARCHAR(25),
		TableName VARCHAR(256),
		ColumnName VARCHAR(256),
		DataType VARCHAR(25),
		MaxLength VARCHAR(25),
		Precision VARCHAR(25),
		IsIdentity BIT,
		IsNullable BIT,
		IsComputed BIT,
		IsUserDefined BIT,
		DateCreated DATETIME,
		DateModified DATETIME
	);
	
	
	---------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	---------------------------------------------------------------------------------------------------------
	-- Keywords
	IF OBJECT_ID('tempdb..#tmpKeywords') IS NOT NULL
	BEGIN
		DROP TABLE #tmpKeywords;
	END
	
	CREATE TABLE #tmpKeywords (
		Idx INT IDENTITY(1,1),
		Value VARCHAR(MAX)
	);

	-- Tables
	IF OBJECT_ID('tempdb..#tmpTables') IS NOT NULL
	BEGIN
		DROP TABLE #tmpTables;
	END

	CREATE TABLE #tmpTables (
		Idx INT IDENTITY(1,1),
		Value VARCHAR(MAX)
	);
	
	---------------------------------------------------------------------------------------------------------
	-- Parse keywords.
	---------------------------------------------------------------------------------------------------------
	INSERT INTO #tmpKeywords (
		Value
	)
	SELECT
		dbo.fnTrim(Value)
	FROM dbo.fnSplit(@Keywords, @Delimiter)
	WHERE dbo.fnTrim(ISNULL(Value, '')) <> ''
	
	
	---------------------------------------------------------------------------------------------------------
	-- Write Sql script to find table objects based on constraints.
	-- <Summary>
	-- Inspects the constraints information to determine if any of the keywords are looking for a
	-- particular constraint.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------
	SET @Sql = '
		SELECT
		   OBJECT_NAME(o.parent_object_id)' + CHAR(10) +
		'FROM sys.objects o' + CHAR(10) +
		'WHERE o.parent_object_id <> 0' + CHAR(10);

	-- Debug
	--SELECT @Sql AS SqlString

	---------------------------------------------------------------------------------------------------------
	-- Loop through keywords to create an "OR" construct when looking up constraints.
	---------------------------------------------------------------------------------------------------------
	SET @CurIndex = (SELECT MIN(Idx) FROM #tmpKeywords);
	SET @EndIndex = (SELECT MAX(Idx) FROM #tmpKeywords);	
	
	WHILE @CurIndex <= @EndIndex
	BEGIN
		
		SET @Value = NULL;
		
		SELECT
			@Value = Value
		FROM #tmpKeywords
		WHERE Idx = @CurIndex
		
		SET @Sql = ISNULL(@Sql, '') +
			CASE 
				WHEN ISNULL(@IsInitialPass, 0) = 1 THEN 'AND (' 
				ELSE 'OR ' 
			END +
			'o.name LIKE ''%' + @Value + '%''' + CHAR(10);
		
		-- We have made our first pass; set to false for 'OR' to kick in
		SET @IsInitialPass = 0;
		
		-- Incriment current index
		SET @CurIndex = @CurIndex + 1;
	END
	
	-- Set ending closure
	SET @Sql = ISNULL(@Sql,'') + ' )'
	
	IF @Debug = 1
	BEGIN
		-- Debug
		PRINT @Sql
	END

	---------------------------------------------------------------------------------------------------------
	-- Fetch table results.
	---------------------------------------------------------------------------------------------------------
	INSERT INTO #tmpTables (
		Value
	)
	EXEC (@Sql);
	
	-- Debug
	-- SELECT * FROM #tmpTables


	---------------------------------------------------------------------------------------------------------
	-- Write Sql script to find table objects based on column name.
	-- <Summary>
	-- Inspects the column information to determine if any table matches the keywords provided.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------
	SET @Sql = 
		'SELECT 
				DB_NAME() AS DatabaseName, 
				s.[name] AS SchemaName,
				t.[name] AS TableName,
				c.[name] AS ColumnName,
				d.[name] AS DataType,
				d.[max_length] AS MaxLength,
				d.[precision] AS Precesion,
				c.[is_identity] AS IsIdentity,
				c.[is_nullable] AS IsNullable,
				c.[is_computed] AS IsComputed,
				d.[is_user_defined] AS IsUserDefined,
				t.[modify_date] AS DateModified,
				t.[create_date] AS DateCreated
		FROM sys.schemas s' + CHAR(10) + 
			'INNER JOIN  sys.tables  t' + CHAR(10) + 
				'ON s.schema_id = t.schema_id' + CHAR(10) + 
			'INNER JOIN  sys.columns c' + CHAR(10) + 
				'ON t.object_id = c.object_id' + CHAR(10) + 
			'INNER JOIN  sys.types   d' + CHAR(10) + 
				'ON c.user_type_id = d.user_type_id' + CHAR(10)



	-- Debug
	--SELECT @Sql AS SqlString

	---------------------------------------------------------------------------------------------------------
	-- Write object query for keywords
	---------------------------------------------------------------------------------------------------------
	SET @CurIndex = (SELECT MIN(Idx) FROM #tmpKeywords);
	SET @EndIndex = (SELECT MAX(Idx) FROM #tmpKeywords);
	SET @IsInitialPass = 1;	
	
	WHILE @CurIndex <= @EndIndex
	BEGIN
		
		SET @Value = NULL;
		
		SELECT
			@Value = Value
		FROM #tmpKeywords
		WHERE Idx = @CurIndex
		
		SET @Sql = ISNULL(@Sql, '') +
			CASE 
				WHEN ISNULL(@IsInitialPass, 0) = 1 THEN 'WHERE ' 
				ELSE 'OR ' 
			END +
			'c.name LIKE ''%' + @Value + '%''' + CHAR(10);
		
		-- We have made our first pass; set to false for 'OR' to kick in
		SET @IsInitialPass = 0;
		
		-- Incriment current index
		SET @CurIndex = @CurIndex + 1;
	END

	---------------------------------------------------------------------------------------------------------
	-- Write object query for tables.
	---------------------------------------------------------------------------------------------------------
	SET @CurIndex = (SELECT MIN(Idx) FROM #tmpTables);
	SET @EndIndex = (SELECT MAX(Idx) FROM #tmpTables);
	SET @IsInitialPass = 1;	
	
	WHILE @CurIndex <= @EndIndex
	BEGIN
		
		SET @Value = NULL;
		
		SELECT
			@Value = Value
		FROM #tmpTables
		WHERE Idx = @CurIndex
		
		SET @Sql = ISNULL(@Sql, '') +
			'OR t.name LIKE ''%' + @Value + '%''' + CHAR(10);
		
		-- Incriment current index
		SET @CurIndex = @CurIndex + 1;
	END	
	
	IF @Debug = 1
	BEGIN
		-- Debug
		PRINT @Sql
	END
	
	---------------------------------------------------------------------------------------------------------
	-- Fetch results
	---------------------------------------------------------------------------------------------------------
	INSERT INTO #tmpSearchResults (
		DatabaseName,
		SchemaName,
		TableName,
		ColumnName,
		DataType,
		MaxLength,
		Precision,
		IsIdentity,
		IsNullable,
		IsComputed,
		IsUserDefined,
		DateCreated,
		DateModified
	)
	EXEC (@Sql);

	---------------------------------------------------------------------------------------------------------
	-- Return results
	---------------------------------------------------------------------------------------------------------
	SELECT
		DatabaseName,
		SchemaName,
		TableName,
		ColumnName,
		DataType,
		MaxLength,
		Precision,
		IsIdentity,
		IsNullable,
		IsComputed,
		IsUserDefined,
		DateCreated,
		DateModified	
	FROM #tmpSearchResults
	

	
	---------------------------------------------------------------------------------------------------------
	-- Dispose of resources.
	---------------------------------------------------------------------------------------------------------
	DROP TABLE #tmpKeywords;
	DROP TABLE #tmpSearchResults;
	DROP TABLE #tmpTables;

END








