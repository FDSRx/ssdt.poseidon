﻿CREATE TABLE [creds].[Workgroup] (
    [CredentialEntityID] BIGINT           NOT NULL,
    [Name]               VARCHAR (512)    NOT NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_Workgroup_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_Workgroup_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_Workgroup_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_Workgroup] PRIMARY KEY CLUSTERED ([CredentialEntityID] ASC),
    CONSTRAINT [FK_Workgroup_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Workgroup_Name]
    ON [creds].[Workgroup]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Workgroup_rowguid]
    ON [creds].[Workgroup]([rowguid] ASC);

