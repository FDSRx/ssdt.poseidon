﻿CREATE TABLE [creds].[SecurityQuestion] (
    [SecurityQuestionID] INT              IDENTITY (1, 1) NOT NULL,
    [Question]           VARCHAR (500)    NOT NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_SecurityQuestion_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_SecurityQuestion_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_SecurityQuestion_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SecurityQuestion] PRIMARY KEY CLUSTERED ([SecurityQuestionID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_SecurityQuestion_Question]
    ON [creds].[SecurityQuestion]([Question] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_SecurityQuestion_rowguid]
    ON [creds].[SecurityQuestion]([rowguid] ASC);

