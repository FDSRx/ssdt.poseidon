﻿CREATE TABLE [creds].[CredentialRequest] (
    [CredentialRequestID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]       INT              NULL,
    [BusinessEntityID]    INT              NULL,
    [CredentialEntityID]  BIGINT           NULL,
    [ApplicationKey]      VARCHAR (256)    NULL,
    [BusinessKey]         VARCHAR (256)    NULL,
    [BusinessKeyType]     VARCHAR (50)     NULL,
    [CredentialTypeKey]   VARCHAR (50)     NULL,
    [CredentialKey]       VARCHAR (256)    NULL,
    [PassKey]             VARCHAR (256)    NULL,
    [SessionKey]          VARCHAR (256)    NULL,
    [IsValid]             BIT              NOT NULL,
    [IPAddress]           VARCHAR (256)    NULL,
    [BrowserName]         VARCHAR (256)    NULL,
    [BrowserVersion]      VARCHAR (50)     NULL,
    [DeviceName]          VARCHAR (50)     NULL,
    [IsMobile]            BIT              CONSTRAINT [DF_CredentialRequest_IsMobile] DEFAULT ((0)) NOT NULL,
    [UserAgent]           VARCHAR (500)    NULL,
    [Comments]            VARCHAR (MAX)    NULL,
    [rowguid]             UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialRequest_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]         DATETIME         CONSTRAINT [DF_CredentialRequest_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]        DATETIME         CONSTRAINT [DF_CredentialRequest_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           VARCHAR (256)    NULL,
    [ModifiedBy]          VARCHAR (256)    NULL,
    [RequestTypeID]       INT              NULL,
    CONSTRAINT [PK_CredentialRequest] PRIMARY KEY CLUSTERED ([CredentialRequestID] ASC),
    CONSTRAINT [FK_CredentialRequest_RequestType] FOREIGN KEY ([RequestTypeID]) REFERENCES [dbo].[RequestType] ([RequestTypeID])
);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/1/2016
-- Description:	Audits a record change of the creds.CredentialRequest table

-- SELECT * FROM creds.CredentialRequest_History
-- SELECT * FROM creds.CredentialRequest WHERE CredentialEntityID = 801018

-- TRUNCATE TABLE creds.CredentialRequest_History
-- =============================================
CREATE TRIGGER [creds].[trigCredentialRequest_Audit]
   ON  [creds].CredentialRequest
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------
	-- Exit trigger when zero records affected.
	-------------------------------------------------------------------------------------------
	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) 
	BEGIN
	   RETURN;
	END;

	-------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------	
	DECLARE @AuditAction VARCHAR(10); -- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	-------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-------------------------------------------------------------------------------------------
	-- Retrieve session data
	-------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-------------------------------------------------------------------------------------------
	-- Audit transaction.
	-------------------------------------------------------------------------------------------
	-- Audit inserted records
	INSERT INTO creds.CredentialRequest_History (
		CredentialRequestID,
		ApplicationID,
		BusinessEntityID,
		CredentialEntityID,
		ApplicationKey,
		BusinessKey,
		BusinessKeyType,
		CredentialTypeKey,
		CredentialKey,
		PassKey,
		SessionKey,
		IsValid,
		IPAddress,
		BrowserName,
		BrowserVersion,
		DeviceName,
		IsMobile,
		UserAgent,
		Comments,
		RequestTypeID,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		d.CredentialRequestID,
		d.ApplicationID,
		d.BusinessEntityID,
		d.CredentialEntityID,
		d.ApplicationKey,
		d.BusinessKey,
		d.BusinessKeyType,
		d.CredentialTypeKey,
		d.CredentialKey,
		d.PassKey,
		d.SessionKey,
		d.IsValid,
		d.IPAddress,
		d.BrowserName,
		d.BrowserVersion,
		d.DeviceName,
		d.IsMobile,
		d.UserAgent,
		d.Comments,
		d.RequestTypeID,
		d.rowguid,	
		d.DateCreated,	
		d.DateModified,
		d.CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_NAME() 
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d

	
	--SELECT * FROM creds.CredentialRequest
	--SELECT * FROM creds.CredentialRequest_History
	
	

END
