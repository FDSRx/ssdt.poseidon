﻿CREATE TABLE [creds].[NetworkUser] (
    [CredentialEntityID] BIGINT           NOT NULL,
    [Domain]             VARCHAR (256)    NOT NULL,
    [Username]           VARCHAR (256)    NOT NULL,
    [DisplayName]        VARCHAR (256)    NULL,
    [IsDisabled]         BIT              CONSTRAINT [DF_NetworkUser_IsDisabled] DEFAULT ((0)) NOT NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_NetworkUser_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_NetworkUser_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_NetworkUser_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_NetworkUser] PRIMARY KEY CLUSTERED ([CredentialEntityID] ASC),
    CONSTRAINT [FK_NetworkUser_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_NetworkUser_DomainUsername]
    ON [creds].[NetworkUser]([Domain] ASC, [Username] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_NetworkUser_rowguid]
    ON [creds].[NetworkUser]([rowguid] ASC);

