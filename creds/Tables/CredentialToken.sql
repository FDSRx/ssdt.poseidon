﻿CREATE TABLE [creds].[CredentialToken] (
    [CredentialTokenID]  BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID] BIGINT           NOT NULL,
    [ApplicationID]      INT              NULL,
    [BusinessEntityID]   BIGINT           NULL,
    [Token]              UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialToken_Token] DEFAULT (newid()) NOT NULL,
    [TokenData]          XML              NULL,
    [DateExpires]        DATETIME         CONSTRAINT [DF_CredentialToken_DateExpires] DEFAULT (getdate()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_CredentialToken_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_CredentialToken_DateModified] DEFAULT (getdate()) NOT NULL,
    [TokenTypeID]        INT              NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_CredentialToken] PRIMARY KEY CLUSTERED ([CredentialTokenID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CredentialToken_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CredentialToken_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_CredentialToken_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID]),
    CONSTRAINT [FK_CredentialToken_TokenType] FOREIGN KEY ([TokenTypeID]) REFERENCES [dbo].[TokenType] ([TokenTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialToken_Token]
    ON [creds].[CredentialToken]([Token] ASC) WITH (FILLFACTOR = 90);

