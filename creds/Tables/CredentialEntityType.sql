﻿CREATE TABLE [creds].[CredentialEntityType] (
    [CredentialEntityTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]                   VARCHAR (25)     NOT NULL,
    [Name]                   VARCHAR (50)     NOT NULL,
    [Description]            VARCHAR (1000)   NULL,
    [rowguid]                UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntityType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_CredentialEntityType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]           DATETIME         CONSTRAINT [DF_CredentialEntityType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              VARCHAR (256)    NULL,
    [ModifiedBy]             VARCHAR (256)    NULL,
    CONSTRAINT [PK_CredentialEntityType] PRIMARY KEY CLUSTERED ([CredentialEntityTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityType_Code]
    ON [creds].[CredentialEntityType]([Code] ASC);


GO
CREATE NONCLUSTERED INDEX [UIX_CredentialEntityType_Name]
    ON [creds].[CredentialEntityType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityType_rowguid]
    ON [creds].[CredentialEntityType]([rowguid] ASC);

