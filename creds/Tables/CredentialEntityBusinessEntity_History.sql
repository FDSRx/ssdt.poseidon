﻿CREATE TABLE [creds].[CredentialEntityBusinessEntity_History] (
    [CredentialEntityBusinessEntityHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityBusinessEntityID]        BIGINT           NOT NULL,
    [CredentialEntityID]                      BIGINT           NOT NULL,
    [ApplicationID]                           INT              NULL,
    [BusinessEntityID]                        BIGINT           NOT NULL,
    [rowguid]                                 UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]                             DATETIME         NOT NULL,
    [DateModified]                            DATETIME         NOT NULL,
    [CreatedBy]                               VARCHAR (256)    NULL,
    [ModifiedBy]                              VARCHAR (256)    NULL,
    [AuditGuid]                               UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntityBusinessEntity_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                             VARCHAR (10)     NOT NULL,
    [DateAudited]                             DATETIME         CONSTRAINT [DF_CredentialEntityBusinessEntity_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CredentialEntityBusinessEntity_History] PRIMARY KEY CLUSTERED ([CredentialEntityBusinessEntityHistoryID] ASC)
);

