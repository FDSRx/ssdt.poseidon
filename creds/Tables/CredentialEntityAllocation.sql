﻿CREATE TABLE [creds].[CredentialEntityAllocation] (
    [CredentialEntityAllocationID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID]           BIGINT           NOT NULL,
    [ApplicationID]                INT              NOT NULL,
    [BusinessEntityID]             BIGINT           NULL,
    [rowguid]                      UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntityAllocation_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                  DATETIME         CONSTRAINT [DF_CredentialEntityAllocation_DateCreated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CredentialEntityAllocation] PRIMARY KEY CLUSTERED ([CredentialEntityAllocationID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CredentialEntityAllocation_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CredentialEntityAllocation_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_CredentialEntityAllocation_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_CredentialEntityAllocation_CredAppBiz]
    ON [creds].[CredentialEntityAllocation]([CredentialEntityID] ASC, [ApplicationID] ASC, [BusinessEntityID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_CredentialEntityAllocation_rowguid]
    ON [creds].[CredentialEntityAllocation]([rowguid] ASC) WITH (FILLFACTOR = 90);

