﻿CREATE TABLE [creds].[CredentialEntity] (
    [CredentialEntityID]     BIGINT           NOT NULL,
    [CredentialEntityTypeID] INT              NOT NULL,
    [BusinessEntityID]       BIGINT           NOT NULL,
    [PersonID]               BIGINT           NULL,
    [rowguid]                UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntity_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_CredentialEntity_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]           DATETIME         CONSTRAINT [DF_CredentialEntity_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              VARCHAR (256)    NULL,
    [ModifiedBy]             VARCHAR (256)    NULL,
    CONSTRAINT [PK_CredentialEntity] PRIMARY KEY CLUSTERED ([CredentialEntityID] ASC),
    CONSTRAINT [FK_CredentialEntity_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_CredentialEntity_CredentialEntityType] FOREIGN KEY ([CredentialEntityTypeID]) REFERENCES [creds].[CredentialEntityType] ([CredentialEntityTypeID]),
    CONSTRAINT [FK_CredentialEntity_Person] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntity_rowguid]
    ON [creds].[CredentialEntity]([rowguid] ASC);

