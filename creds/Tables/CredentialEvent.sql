﻿CREATE TABLE [creds].[CredentialEvent] (
    [EventID]      INT              NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEvent_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_CredentialEvent_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_CredentialEvent_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_CredentialEvent] PRIMARY KEY CLUSTERED ([EventID] ASC),
    CONSTRAINT [FK_CredentialEvent_Event] FOREIGN KEY ([EventID]) REFERENCES [dbo].[Event] ([EventID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEvent_Code]
    ON [creds].[CredentialEvent]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEvent_Name]
    ON [creds].[CredentialEvent]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEvent_rowguid]
    ON [creds].[CredentialEvent]([rowguid] ASC);

