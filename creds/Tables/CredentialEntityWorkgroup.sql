﻿CREATE TABLE [creds].[CredentialEntityWorkgroup] (
    [CredentialEntityWorkgroupID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID]          BIGINT           NOT NULL,
    [WorkgroupID]                 BIGINT           NOT NULL,
    [rowguid]                     UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntityWorkgroup_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                 DATETIME         CONSTRAINT [DF_CredentialEntityWorkgroup_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                DATETIME         CONSTRAINT [DF_CredentialEntityWorkgroup_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                   VARCHAR (256)    NULL,
    [ModifiedBy]                  VARCHAR (256)    NULL,
    CONSTRAINT [PK_CredentialEntityWorkgroup] PRIMARY KEY CLUSTERED ([CredentialEntityWorkgroupID] ASC),
    CONSTRAINT [FK_CredentialEntityWorkgroup_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID]),
    CONSTRAINT [FK_CredentialEntityWorkgroup_Workgroup] FOREIGN KEY ([WorkgroupID]) REFERENCES [creds].[Workgroup] ([CredentialEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityWorkgroup_CredWorkgroup]
    ON [creds].[CredentialEntityWorkgroup]([CredentialEntityID] ASC, [WorkgroupID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityWorkgroup_rowguid]
    ON [creds].[CredentialEntityWorkgroup]([rowguid] ASC);

