﻿CREATE TABLE [creds].[CredentialEntityApplication] (
    [CredentialEntityApplicationID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID]            BIGINT           NOT NULL,
    [ApplicationID]                 INT              NOT NULL,
    [rowguid]                       UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntityApplication_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                   DATETIME         CONSTRAINT [DF_CredentialEntityApplication_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModifieid]                 DATETIME         CONSTRAINT [DF_CredentialEntityApplication_DateModifieid] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                     VARCHAR (256)    NULL,
    [ModifiedBy]                    VARCHAR (256)    NULL,
    CONSTRAINT [PK_CredentialEntityApplication] PRIMARY KEY CLUSTERED ([CredentialEntityApplicationID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CredentialEntityApplication_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CredentialEntityApplication_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityApplication_CredApp]
    ON [creds].[CredentialEntityApplication]([CredentialEntityID] ASC, [ApplicationID] ASC) WITH (FILLFACTOR = 90);

