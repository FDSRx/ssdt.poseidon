﻿CREATE TABLE [creds].[LinkedProviderUser_History] (
    [LinkedProviderUserHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [LinkedProviderUserID]        BIGINT           NULL,
    [CredentialEntityID]          BIGINT           NULL,
    [CredentialProviderID]        INT              NULL,
    [UserKey]                     VARCHAR (256)    NULL,
    [PasswordUnencrypted]         NVARCHAR (256)   NULL,
    [PasswordEncrypted]           NVARCHAR (256)   NULL,
    [rowguid]                     UNIQUEIDENTIFIER NULL,
    [DateCreated]                 DATETIME         NULL,
    [DateModified]                DATETIME         NULL,
    [CreatedBy]                   VARCHAR (256)    NULL,
    [ModifiedBy]                  VARCHAR (256)    NULL,
    [AuditGuid]                   UNIQUEIDENTIFIER CONSTRAINT [DF_LinkedProviderUser_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                 VARCHAR (10)     NULL,
    [DateAudited]                 DATETIME         CONSTRAINT [DF_LinkedProviderUser_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_LinkedProviderUser_History] PRIMARY KEY CLUSTERED ([LinkedProviderUserHistoryID] ASC)
);

