﻿CREATE TABLE [creds].[ExtranetUser] (
    [CredentialEntityID] BIGINT           NOT NULL,
    [Username]           VARCHAR (256)    NOT NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_ExtranetUser_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_ExtranetUser_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_ExtranetUser_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_ExtranetUser] PRIMARY KEY CLUSTERED ([CredentialEntityID] ASC),
    CONSTRAINT [FK_ExtranetUser_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ExtranetUser_rowguid]
    ON [creds].[ExtranetUser]([rowguid] ASC);

