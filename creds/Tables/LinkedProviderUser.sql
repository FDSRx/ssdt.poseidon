﻿CREATE TABLE [creds].[LinkedProviderUser] (
    [LinkedProviderUserID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID]   BIGINT           NOT NULL,
    [CredentialProviderID] INT              NOT NULL,
    [UserKey]              VARCHAR (256)    NOT NULL,
    [PasswordUnencrypted]  NVARCHAR (256)   NULL,
    [PasswordEncrypted]    NVARCHAR (256)   NULL,
    [rowguid]              UNIQUEIDENTIFIER CONSTRAINT [DF_LinkedProviderUser_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]          DATETIME         CONSTRAINT [DF_LinkedProviderUser_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]         DATETIME         CONSTRAINT [DF_LinkedProviderUser_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]            VARCHAR (256)    NULL,
    [ModifiedBy]           VARCHAR (256)    NULL,
    CONSTRAINT [PK_LinkedProviderUser] PRIMARY KEY CLUSTERED ([LinkedProviderUserID] ASC),
    CONSTRAINT [FK_LinkedProviderUser_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID]),
    CONSTRAINT [FK_LinkedProviderUser_CredentialProvider] FOREIGN KEY ([CredentialProviderID]) REFERENCES [creds].[CredentialProvider] ([CredentialProviderID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_LinkedProviderUser_CredProvUser]
    ON [creds].[LinkedProviderUser]([CredentialEntityID] ASC, [CredentialProviderID] ASC, [UserKey] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_LinkedProviderUser_rowguid]
    ON [creds].[LinkedProviderUser]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/1/2016
-- Description:	Audits a record change of the creds.LinkedProviderUser table

-- SELECT * FROM creds.LinkedProviderUser_History
-- SELECT * FROM creds.LinkedProviderUser WHERE CredentialEntityID = 801018

-- TRUNCATE TABLE creds.LinkedProviderUser_History
-- =============================================
CREATE TRIGGER [creds].[trigLinkedProviderUser_Audit]
   ON  [creds].LinkedProviderUser
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------
	-- Exit trigger when zero records affected.
	-------------------------------------------------------------------------------------------
	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) 
	BEGIN
	   RETURN;
	END;

	-------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------	
	DECLARE @AuditAction VARCHAR(10); -- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	-------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-------------------------------------------------------------------------------------------
	-- Retrieve session data
	-------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-------------------------------------------------------------------------------------------
	-- Audit transaction.
	-------------------------------------------------------------------------------------------
	-- Audit inserted records
	INSERT INTO creds.LinkedProviderUser_History (
		LinkedProviderUserID,
		CredentialEntityID,
		CredentialProviderID,
		UserKey,
		PasswordUnencrypted,
		PasswordEncrypted,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		d.LinkedProviderUserID,
		d.CredentialEntityID,
		d.CredentialProviderID,
		d.UserKey,
		d.PasswordUnencrypted,
		d.PasswordEncrypted,
		d.rowguid,	
		d.DateCreated,	
		d.DateModified,
		d.CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_NAME() 
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d

	
	--SELECT * FROM creds.LinkedProviderUser
	--SELECT * FROM creds.LinkedProviderUser_History
	
	

END
