﻿CREATE TABLE [creds].[CredentialEntityBusinessEntity] (
    [CredentialEntityBusinessEntityID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID]               BIGINT           NOT NULL,
    [ApplicationID]                    INT              NULL,
    [BusinessEntityID]                 BIGINT           NOT NULL,
    [rowguid]                          UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntityBusinessEntity_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                      DATETIME         CONSTRAINT [DF_CredentialEntityBusinessEntity_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                     DATETIME         CONSTRAINT [DF_CredentialEntityBusinessEntity_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                        VARCHAR (256)    NULL,
    [ModifiedBy]                       VARCHAR (256)    NULL,
    CONSTRAINT [PK_CredentialEntityBusinessEntity] PRIMARY KEY CLUSTERED ([CredentialEntityBusinessEntityID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CredentialEntityBusinessEntity_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CredentialEntityBusinessEntity_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_CredentialEntityBusinessEntity_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityBusinessEntity_CredAppBiz]
    ON [creds].[CredentialEntityBusinessEntity]([CredentialEntityID] ASC, [ApplicationID] ASC, [BusinessEntityID] ASC) WITH (FILLFACTOR = 90);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/11/2014
-- Description:	Audits a record change of the creds.CredentialEntityBusinessEntity table
-- SELECT * FROM creds.CredentialEntityBusinessEntity_History
-- SELECT * FROM creds.CredentialEntityBusinessEntity WHERE CredentialEntityID = 801018
-- TRUNCATE TABLE creds.CredentialEntityBusinessEntity
-- =============================================
CREATE TRIGGER creds.[trigCredentialEntityBusinessEntity_Audit]
   ON  creds.CredentialEntityBusinessEntity
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10); -- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-----------------------------------------------------------------------------
	-- Audit transaction
	-----------------------------------------------------------------------------
	-- Audit inserted records
	INSERT INTO creds.CredentialEntityBusinessEntity_History (
		CredentialEntityBusinessEntityID,
		CredentialEntityID,
		ApplicationID,
		BusinessEntityID,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		d.CredentialEntityBusinessEntityID,
		d.CredentialEntityID,
		d.ApplicationID,
		d.BusinessEntityID,
		d.rowguid,	
		d.DateCreated,	
		d.DateModified,
		d.CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d

	
	--SELECT * FROM creds.CredentialEntityBusinessEntity
	
	

END
