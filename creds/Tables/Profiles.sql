﻿CREATE TABLE [creds].[Profiles] (
    [ProfileID]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID]  BIGINT           NOT NULL,
    [PropertyName]        VARCHAR (256)    NOT NULL,
    [PropertyValueString] NVARCHAR (MAX)   NULL,
    [PropertyValueBinary] VARBINARY (MAX)  NULL,
    [rowguid]             UNIQUEIDENTIFIER CONSTRAINT [DF_Profiles_rowguid] DEFAULT (newid()) NULL,
    [DateCreated]         DATETIME         CONSTRAINT [DF_Profiles_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]        DATETIME         CONSTRAINT [DF_Profiles_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           VARCHAR (256)    NULL,
    [ModifiedBy]          VARCHAR (256)    NULL,
    CONSTRAINT [PK_Profiles] PRIMARY KEY CLUSTERED ([ProfileID] ASC),
    CONSTRAINT [FK_Profiles_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Profiles_CredentialProperty]
    ON [creds].[Profiles]([CredentialEntityID] ASC, [PropertyName] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Profiles_rowguid]
    ON [creds].[Profiles]([rowguid] ASC);

