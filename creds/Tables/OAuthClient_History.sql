﻿CREATE TABLE [creds].[OAuthClient_History] (
    [OAuthClientHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID]   BIGINT           NULL,
    [ClientID]             NVARCHAR (256)   NULL,
    [SecretKey]            NVARCHAR (256)   NULL,
    [Name]                 VARCHAR (256)    NULL,
    [Description]          VARCHAR (1000)   NULL,
    [rowguid]              UNIQUEIDENTIFIER NULL,
    [DateCreated]          DATETIME         NULL,
    [DateModified]         DATETIME         NULL,
    [CreatedBy]            VARCHAR (256)    NULL,
    [ModifiedBy]           VARCHAR (256)    NULL,
    [AuditGuid]            UNIQUEIDENTIFIER CONSTRAINT [DF_OAuthClient_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]          VARCHAR (10)     NULL,
    [DateAudited]          DATETIME         CONSTRAINT [DF_OAuthClient_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OAuthClient_History] PRIMARY KEY CLUSTERED ([OAuthClientHistoryID] ASC)
);

