﻿CREATE TABLE [creds].[Membership] (
    [MembershipID]                     BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID]               BIGINT           NOT NULL,
    [EmailAddress]                     VARCHAR (256)    NULL,
    [PasswordHash]                     VARBINARY (128)  NULL,
    [PasswordSalt]                     VARBINARY (128)  NULL,
    [PasswordQuestionID]               INT              NULL,
    [PasswordAnswerHash]               VARBINARY (128)  NULL,
    [PasswordAnswerSalt]               VARBINARY (128)  NULL,
    [IsApproved]                       BIT              CONSTRAINT [DF_Membership_IsApproved] DEFAULT ((0)) NOT NULL,
    [IsLockedOut]                      BIT              CONSTRAINT [DF_Membership_IsLockedOut] DEFAULT ((0)) NOT NULL,
    [DateLastLoggedIn]                 DATETIME         NULL,
    [DateLastPasswordChanged]          DATETIME         NULL,
    [DatePasswordExpires]              DATETIME         NULL,
    [DateLastLockedOut]                DATETIME         NULL,
    [DateLockOutExpires]               DATETIME         NULL,
    [FailedPasswordAttemptCount]       INT              CONSTRAINT [DF_Membership_FailedPasswordAttemptCount] DEFAULT ((0)) NOT NULL,
    [FailedPasswordAnswerAttemptCount] INT              CONSTRAINT [DF_Membership_FailedPasswordAnswerAttemptCount] DEFAULT ((0)) NOT NULL,
    [IsDisabled]                       BIT              CONSTRAINT [DF_Membership_IsDisabled] DEFAULT ((0)) NOT NULL,
    [Comment]                          VARCHAR (1000)   NULL,
    [rowguid]                          UNIQUEIDENTIFIER CONSTRAINT [DF_Membership_rowguid] DEFAULT (newid()) NULL,
    [DateCreated]                      DATETIME         CONSTRAINT [DF_Membership_DateCreated] DEFAULT (getdate()) NULL,
    [DateModified]                     DATETIME         CONSTRAINT [DF_Membership_DateModified] DEFAULT (getdate()) NULL,
    [CreatedBy]                        VARCHAR (256)    NULL,
    [ModifiedBy]                       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Membership] PRIMARY KEY CLUSTERED ([MembershipID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Membership_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID]),
    CONSTRAINT [FK_Membership_SecurityQuestion] FOREIGN KEY ([PasswordQuestionID]) REFERENCES [creds].[SecurityQuestion] ([SecurityQuestionID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Membership_CredentialEntityID]
    ON [creds].[Membership]([CredentialEntityID] ASC) WITH (FILLFACTOR = 90);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/6/2013
-- Description:	Audits a record change of the creds.Membership table
-- SELECT * FROM creds.Membership_History
-- SELECT * FROM creds.Membership  WHERE CredentialEntityID = 801018
-- TRUNCATE TABLE creds.Membership_History
-- =============================================
CREATE TRIGGER [creds].[trigMembership_Audit]
   ON  [creds].[Membership]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-----------------------------------------------------------------------------
	-- Audit transaction
	-----------------------------------------------------------------------------
	
	IF @AuditActionCode IN ('U', 'D') AND ( 
		UPDATE(EmailAddress) OR UPDATE(PasswordHash) OR UPDATE(PasswordSalt) OR UPDATE(PasswordQuestionID) OR
		UPDATE(PasswordAnswerHash) OR UPDATE(PasswordAnswerSalt))
	BEGIN
		-- Audit inserted records
		INSERT INTO creds.Membership_History (
			MembershipID,	
			CredentialEntityID,	
			EmailAddress,
			PasswordHash,
			PasswordSalt,
			PasswordQuestionID,
			PasswordAnswerHash,
			PasswordAnswerSalt,
			IsApproved,
			IsLockedOut,
			DateLastLoggedIn,
			DateLastPasswordChanged,
			DateLastLockedOut,
			FailedPasswordAttemptCount,
			FailedPasswordAnswerAttemptCount,
			IsDisabled,
			Comment,
			rowguid,	
			DateCreated,	
			DateModified,
			CreatedBy,
			ModifiedBy,
			AuditGuid,
			AuditAction,
			DateAudited
		)
		SELECT
			d.MembershipID,	
			d.CredentialEntityID,	
			d.EmailAddress,
			d.PasswordHash,
			d.PasswordSalt,
			d.PasswordQuestionID,
			d.PasswordAnswerHash,
			d.PasswordAnswerSalt,
			d.IsApproved,
			d.IsLockedOut,
			d.DateLastLoggedIn,
			d.DateLastPasswordChanged,
			d.DateLastLockedOut,
			d.FailedPasswordAttemptCount,
			d.FailedPasswordAnswerAttemptCount,
			d.IsDisabled,
			d.Comment,
			d.rowguid,	
			d.DateCreated,	
			d.DateModified,
			d.CreatedBy,
			CASE 
				WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
				ELSE d.ModifiedBy 
			END AS ModifiedBy,
			@AuditGuid,
			@AuditAction,
			@DateAudited
		FROM deleted d
			JOIN inserted i
				ON d.CredentialEntityID = i.CredentialEntityID
		WHERE i.EmailAddress <> d.EmailAddress
			OR i.PasswordHash <> d.PasswordHash
			OR i.PasswordSalt <> d.PasswordSalt
			OR i.PasswordQuestionID <> d.PasswordQuestionID
			OR i.PasswordAnswerHash <> d.PasswordAnswerHash
			OR i.PasswordAnswerSalt <> d.PasswordAnswerSalt
			OR i.IsDisabled <> d.IsDisabled
	END
	
	--SELECT * FROM creds.Membership
	
	

END
