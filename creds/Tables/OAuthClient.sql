﻿CREATE TABLE [creds].[OAuthClient] (
    [CredentialEntityID] BIGINT           NOT NULL,
    [ClientID]           NVARCHAR (256)   CONSTRAINT [DF_OAuthClient_ClientID] DEFAULT (newid()) NOT NULL,
    [SecretKey]          NVARCHAR (256)   CONSTRAINT [DF_OAuthClient_SecretKey] DEFAULT (newid()) NOT NULL,
    [Name]               VARCHAR (256)    NULL,
    [Description]        VARCHAR (1000)   NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_OAuthClient_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_OAuthClient_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_OAuthClient_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_OAuthClient] PRIMARY KEY CLUSTERED ([CredentialEntityID] ASC),
    CONSTRAINT [FK_OAuthClient_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_OAuthClient_ClientID]
    ON [creds].[OAuthClient]([ClientID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_OAuthClient_rowguid]
    ON [creds].[OAuthClient]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/1/2016
-- Description:	Audits a record change of the creds.OAuthClient table

-- SELECT * FROM creds.OAuthClient_History
-- SELECT * FROM creds.OAuthClient WHERE CredentialEntityID = 801018

-- TRUNCATE TABLE creds.OAuthClient_History
-- =============================================
CREATE TRIGGER [creds].[trigOAuthClient_Audit]
   ON  [creds].[OAuthClient]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------
	-- Exit trigger when zero records affected.
	-------------------------------------------------------------------------------------------
	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) 
	BEGIN
	   RETURN;
	END;

	-------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------	
	DECLARE @AuditAction VARCHAR(10); -- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	-------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-------------------------------------------------------------------------------------------
	-- Retrieve session data
	-------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-------------------------------------------------------------------------------------------
	-- Audit transaction.
	-------------------------------------------------------------------------------------------
	-- Audit inserted records
	INSERT INTO creds.OAuthClient_History (
		CredentialEntityID,
		ClientID,
		SecretKey,
		Name,
		Description,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		d.CredentialEntityID,
		d.ClientID,
		d.SecretKey,
		d.Name,
		d.Description,
		d.rowguid,	
		d.DateCreated,	
		d.DateModified,
		d.CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_NAME() 
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d

	
	--SELECT * FROM creds.OAuthClient
	--SELECT * FROM creds.OAuthClient_History
	
	

END
