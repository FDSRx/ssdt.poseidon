﻿CREATE TABLE [creds].[SecurityLayer] (
    [SecurityLayerID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]            VARCHAR (25)     NOT NULL,
    [Name]            VARCHAR (50)     NOT NULL,
    [Description]     VARCHAR (1000)   NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_SecurityLayer_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_SecurityLayer_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_SecurityLayer_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)    NULL,
    [ModifiedBy]      VARCHAR (256)    NULL,
    CONSTRAINT [PK_SecurityLayer] PRIMARY KEY CLUSTERED ([SecurityLayerID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SecurityLayer_Code]
    ON [creds].[SecurityLayer]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SecurityLayer_Name]
    ON [creds].[SecurityLayer]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SecurityLayer_rowguid]
    ON [creds].[SecurityLayer]([rowguid] ASC);

