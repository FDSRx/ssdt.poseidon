﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/9/2014
-- Description:	Gets the CredentialEventID.
-- SAMPLE CALL: SELECT creds.fnGetCredentialEventID('LO')
-- SAMPLE CALL: SELECT creds.fnGetCredentialEventID(2)
-- SAMPLE CALL: SELECT creds.fnGetCredentialEventID('AUTHNF')

-- SELECT * FROM creds.CredentialEvent
-- =============================================
CREATE FUNCTION [creds].[fnGetCredentialEventID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT EventID FROM creds.CredentialEvent WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT EventID FROM creds.CredentialEvent WHERE EventID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

