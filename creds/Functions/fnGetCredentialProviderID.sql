﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/15/2015
-- Description:	Gets the CredentialProvider object identifier.
-- SAMPLE CALL: SELECT creds.fnGetCredentialProviderID('ENG')
-- SAMPLE CALL: SELECT creds.fnGetCredentialProviderID(2)
-- SAMPLE CALL: SELECT creds.fnGetCredentialProviderID('MRXA')

-- SELECT * FROM creds.CredentialProvider
-- =============================================
CREATE FUNCTION [creds].[fnGetCredentialProviderID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT CredentialProviderID FROM creds.CredentialProvider WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT CredentialProviderID FROM creds.CredentialProvider WHERE CredentialProviderID = @Key);
	END

	-- Return the result of the function
	RETURN @ID;

END

