﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Gets the Credentity Entity Type ID
-- SAMPLE CALL: SELECT creds.fnGetCredentialEntityTypeID('EXTRANET')
-- SAMPLE CALL: SELECT creds.fnGetCredentialEntityTypeID(2)
-- SAMPLE CALL: SELECT creds.fnGetCredentialEntityTypeID('INTRANET')
-- SELECT * FROM creds.CredentialEntityType
-- =============================================
CREATE FUNCTION [creds].[fnGetCredentialEntityTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT CredentialEntityTypeID FROM creds.CredentialEntityType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT CredentialEntityTypeID FROM creds.CredentialEntityType WHERE CredentialEntityTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

