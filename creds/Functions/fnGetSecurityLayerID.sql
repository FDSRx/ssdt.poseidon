﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2014
-- Description:	Gets the SecurityLayerID of the SecurityLayer object.
-- SAMPLE CALL: SELECT creds.fnGetSecurityLayerID('NONE')
-- SAMPLE CALL: SELECT creds.fnGetSecurityLayerID(2)
-- =============================================
CREATE FUNCTION [creds].[fnGetSecurityLayerID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Id VARCHAR(50)

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT SecurityLayerID FROM creds.SecurityLayer WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @Id = (SELECT SecurityLayerID FROM creds.SecurityLayer WHERE SecurityLayerID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @Id;

END

