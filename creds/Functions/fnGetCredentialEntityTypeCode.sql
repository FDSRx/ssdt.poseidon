﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Gets the Credentity Entity Type Code
-- SAMPLE CALL: SELECT creds.fnGetCredentialEntityTypeCode('EXTRANET')
-- SAMPLE CALL: SELECT creds.fnGetCredentialEntityTypeCode(2)
-- =============================================
CREATE FUNCTION [creds].[fnGetCredentialEntityTypeCode] 
(
	@Key VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Code VARCHAR(50)

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Code = (SELECT Code FROM creds.CredentialEntityType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @Code = (SELECT Code FROM creds.CredentialEntityType WHERE CredentialEntityTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @Code;

END

