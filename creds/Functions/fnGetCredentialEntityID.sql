﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/7/2014
-- Description:	Gets the CredentialEntityID from the unique identifier.
-- SAMPLE CALL: SELECT creds.fnGetCredentialEntityID('D1D88CF0-5CDE-4410-81CE-EBC92A0D0636')
-- SAMPLE CALL: SELECT creds.fnGetCredentialEntityID(801018)
-- SELECT * FROM creds.CredentialEntity
-- SELECT * FROM creds.CredentialToken
-- =============================================
CREATE FUNCTION creds.fnGetCredentialEntityID 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT CredentialEntityID FROM creds.CredentialToken WHERE Token = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT CredentialEntityID FROM creds.CredentialEntity WHERE CredentialEntityID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

