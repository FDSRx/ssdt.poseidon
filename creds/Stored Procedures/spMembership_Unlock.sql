﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/8/2014
-- Description:	Unlocks a single or all applicable locked records.
-- SAMPLE CALL:
/*

DECLARE 
	@IsLocked BIT = NULL
	
EXEC creds.spMembership_IsLockedOut
	@CredentialEntityID = 801018,
	@IsLockedOut = @IsLocked OUTPUT

SELECT @IsLocked AS IsLocked

EXEC creds.spMembership_Unlock
	@CredentialEntityID = 801018,
	@ForceUnlock = 1

*/
-- SELECT * FROM creds.Membership
-- SELECT * FROM creds.vwExtranetUser WHERE BusinessEntityID = 10684
-- =============================================
CREATE PROCEDURE [creds].[spMembership_Unlock]
	@MembershipID BIGINT = NULL,
	@CredentialEntityID BIGINT = NULL,
	@BusinessID BIGINT = NULL,
	@Username VARCHAR(256) = NULL,
	@CredentialEntityTypeID INT = NULL,
	@ForceUnlock BIT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@MembershipID=' + dbo.fnToStringOrEmpty(@MembershipID) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@CredentialEntityTypeID=' + dbo.fnToStringOrEmpty(@CredentialEntityTypeID) + ';'

	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	----------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------
	SET @ForceUnlock = ISNULL(@ForceUnlock, 0);
	
	----------------------------------------------------------------------------------
	-- Declare variables.
	----------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@TranDate DATETIME = GETDATE()
	
	/*
	----------------------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules.
	-- </Summary>
	----------------------------------------------------------------------------------
	-- A tag name is required to be created.
	IF @MembershipID IS NULL AND @CredentialEntityID IS NULL AND ( @BusinessID IS NULL OR @Username IS NULL OR @CredentialEntityID IS NULL )
	BEGIN
	
		SET @ErrorMessage = 'Unable to retrieve Membership entry.  Object references ' +
			'("@MembershipID" or "@CredentialEntityID" or "@BusinessID and @Username and @CredentialEntityID") are not set to an instance of an object. ' +
			'The "@MembershipID" or "@CredentialEntityID" or "@BusinessID and @Username and @CredentialEntityID" cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	*/

	----------------------------------------------------------------------------------
	-- Interrogate provided identifiers and determine the proper channel of
	-- identification.
	-- <Summary>
	-- Inspects credential indentification keys and determines how to access the
	-- membership record.
	-- </Summary>
	----------------------------------------------------------------------------------
	IF @MembershipID IS NULL AND @CredentialEntityID IS NULL
	BEGIN
		----------------------------------------------------------------------------------
		-- Fetch credential entity
		-- <Summary>
		-- Interrogates the credentials for the provided type
		-- to determine if the supplied credentials meets the required
		-- criteria to be authenticated.
		-- </Summary>
		----------------------------------------------------------------------------------
		EXEC creds.spCredentialEntity_Derived_Exists
			@CredentialEntityTypeID = @CredentialEntityTypeID,
			@BusinessEntityID = @BusinessID,
			@DerivedCredentialKey = @Username,
			@CredentialEntityID = @CredentialEntityID OUTPUT
	END
	
	----------------------------------------------------------------------------------
	-- Interrogate unique keys and determine winner.
	-- <Summary>
	-- If the primary record key is supplied (i.e. the surrogate key)
	-- then declare that key the winner and void the foreign key; 
	-- otherwise, keep the foreign unique key.
	-- </Summary>
	----------------------------------------------------------------------------------
	IF @MembershipID IS NOT NULL
	BEGIN
		SET @CredentialEntityID = NULL;
	END
	
	-- Debug
	--SELECT @MembershipID AS MembershipID, @CredentialEntityID AS CredentialEntityID, @ForceUnlock AS ForceUnlock
	
	
	----------------------------------------------------------------------------------
	-- Unlock record(s)
	-- <Summary>
	-- If a specific record was provided, then unlock the specific record; otherwise,
	-- attempt an unlock on all applicable records.
	-- </Summary>
	----------------------------------------------------------------------------------
	IF @MembershipID IS NOT NULL OR @CredentialEntityID IS NOT NULL
	BEGIN
		
		-- Specific record to unlock.
		UPDATE mem
		SET
			IsLockedOut = 0,
			FailedPasswordAttemptCount = 0,
			FailedPasswordAnswerAttemptCount = 0,
			DateLockOutExpires = NULL,
			DateModified = GETDATE(),
			ModifiedBy = @ModifiedBy
		--SELECT *
		FROM creds.Membership mem
		WHERE ( MembershipID = @MembershipID OR
			CredentialEntityID = @CredentialEntityID
		)
			AND (
				( IsLockedOut = 1
					AND DateLockOutExpires IS NOT NULL
					AND DateLockOutExpires < GETDATE() ) OR
					@ForceUnlock = 1					
			)
	
	END
	ELSE
	BEGIN
		
		-- Unlock all applicable locked records.
		UPDATE mem
		SET
			IsLockedOut = 0,
			FailedPasswordAttemptCount = 0,
			FailedPasswordAnswerAttemptCount = 0,
			DateLockOutExpires = NULL,
			DateModified = GETDATE(),
			ModifiedBy = @ModifiedBy
		--SELECT *
		FROM creds.Membership mem
		WHERE (
				( IsLockedOut = 1
					AND DateLockOutExpires IS NOT NULL
					AND DateLockOutExpires < GETDATE() ) OR
					@ForceUnlock = 1						
		)
		
	END
	

END
