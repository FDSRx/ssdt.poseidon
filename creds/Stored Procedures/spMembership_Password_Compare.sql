﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Compare passwords
-- SAMPLE CALL:
/*
DECLARE
	@IsValid BIT = NULL,
	@IsApproved BIT = NULL,
	@IsLockedOut BIT = NULL,
	@DateLastLoggedIn DATETIME = NULL,
	@DateLastPasswordChanged DATETIME = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@DateLastLockedOut DATETIME = NULL,
	@DateLockOutExpires DATETIME = NULL,
	@FailedPasswordAttemptCount INT = NULL,
	@FailedPasswordAnswerAttemptCount INT = NULL,
	@IsDisabled BIT = NULL
	
EXEC creds.spMembership_Password_Compare 
	@CredentialEntityID = 801050, 
	@Password = 'password', 
	@IsValid = @IsValid OUTPUT,
	@IsApproved = @IsApproved OUTPUT,
	@IsLockedOut = @IsLockedOut OUTPUT,
	@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
	@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
	@DatePasswordExpires = @DatePasswordExpires OUTPUT,
	@DateLastLockedOut = @DateLastLockedOut OUTPUT,
	@DateLockOutExpires = @DateLockOutExpires OUTPUT,
	@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
	@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
	@IsDisabled = @IsDisabled OUTPUT
	
SELECT @IsValid AS IsValid, @IsApproved AS IsApproved, @IsLockedOut AS IsLockedOut, @DateLastLoggedIn AS DateLastLoggedIn,
	@DateLastPasswordChanged AS DateLastPasswordChanged, @DatePasswordExpires AS DatePasswordExpires, @DateLastLockedOut AS DateLastLockedOut,
	@DateLockOutExpires AS DateLockOutExpires, @FailedPasswordAttemptCount AS FailedPasswordAttemptCount, 
	@FailedPasswordAnswerAttemptCount AS FailedPasswordAnswerAttemptCount, @IsDisabled AS IsDisabled
	
*/

-- SELECT * FROM creds.Membership
-- =============================================
CREATE PROCEDURE [creds].[spMembership_Password_Compare]
	@CredentialEntityID BIGINT,
	@Password VARCHAR(128),
	@IsValid BIT = NULL OUTPUT,
	-- Core membership properties ****************************
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT
	-- *******************************************************
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------------------------------
	SET @IsValid = 0;
	SET @IsApproved = 0;
	SET	@IsLockedOut = 0;
	SET	@DateLastLoggedIn = NULL;
	SET	@DateLastPasswordChanged = NULL;
	SET @DatePasswordExpires = NULL;
	SET	@DateLastLockedOut = NULL;
	SET @DateLockOutExpires = NULL;
	SET	@FailedPasswordAttemptCount = 0;
	SET	@FailedPasswordAnswerAttemptCount = 0;
	SET @IsDisabled = 0;
		
	-----------------------------------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------------------------------
	DECLARE
		@PasswordHash VARBINARY(128),
		@PasswordSalt VARBINARY(10)
	
	-----------------------------------------------------------------------------------------------
	-- Retrieve current password
	-- </Summary>
	-- Fetches current password information for comparison
	-- </Summary>
	-----------------------------------------------------------------------------------------------
	EXEC creds.spMembership_Password_Get	
		@CredentialEntityID = @CredentialEntityID,
		@PasswordHash = @PasswordHash OUTPUT,
		@PasswordSalt = @PasswordSalt OUTPUT,
		@IsApproved = @IsApproved OUTPUT,
		@IsLockedOut = @IsLockedOut OUTPUT,
		@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
		@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
		@DatePasswordExpires = @DatePasswordExpires OUTPUT,
		@DateLastLockedOut = @DateLastLockedOut OUTPUT,
		@DateLockOutExpires = @DateLockOutExpires OUTPUT,		
		@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
		@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
		@IsDisabled = @IsDisabled OUTPUT
	
	-----------------------------------------------------------------------------------------------
	-- Compare password data.
	-- <Summary>
	-- Hashes provided password to compare to the stored
	-- password in the membership repository.  If both hash elements match each other, then
	-- the user has successfully sent in the correct
	-- password.
	-- </Summary>
	-----------------------------------------------------------------------------------------------
	SET @IsValid = dbo.fnPasswordCompare(@Password + CONVERT(VARCHAR(MAX), @PasswordSalt), @PasswordHash);
	
	
END
