﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/26/2015
-- Description:	Interrogates the ClientID and SecretKey to determine if the OAuthClient object is valid.
-- Remarks: Very similar (if not currently the same) as the "_IsValid" procedure.  However, the is valid procedure
--				may later include filtering criteria that would not be the same as the "_Exists" method.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityID BIGINT = NULL,
	@Exists BIT = NULL

EXEC creds.spOAuthClient_Exists
	@ClientID = 'EAE24BAD-1ABF-4AD8-8473-DB5435FC6124',
	@SecretKey = 'EA712596-215A-46A9-9CD4-39C3AE01D739',
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@Exists = @Exists OUTPUT

SELECT @CredentialEntityID AS CredentialEntityID, @Exists AS IsValid 

*/

-- SELECT * FROM creds.OAuthClient
-- =============================================
CREATE PROCEDURE [creds].[spOAuthClient_Exists]
	@CredentialEntityID BIGINT = NULL OUTPUT, -- a.k.a. Client identifier
	@ClientID NVARCHAR(256) = NULL OUTPUT, -- a.k.a. Username
	@SecretKey NVARCHAR(256) = NULL OUTPUT, -- a.k.a. Password
	@Name VARCHAR(256) = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-----------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ClientID=' + dbo.fnToStringOrEmpty(@ClientID) + ';' +
		'@SecretKey=' + dbo.fnToStringOrEmpty(@SecretKey) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@Exists=' + dbo.fnToStringOrEmpty(@Exists) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' ;
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	-----------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------
	SET @Name = NULL;
	SET @Exists = 0;


	-----------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------
	-- No declaration.

	-----------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------
	-- Not applicable.


	-----------------------------------------------------------------------------------------------------
	-- Validate object.
	-----------------------------------------------------------------------------------------------------
	SELECT
		@CredentialEntityID = CredentialEntityID,
		@ClientID = ClientID,
		@SecretKey = @SecretKey,
		@Name = Name,
		@Exists = CASE WHEN CredentialEntityID IS NOT NULL THEN 1 ELSE 0 END
	FROM creds.OAuthClient
	WHERE ( @CredentialEntityID IS NOT NULL AND CredentialEntityID = @CredentialEntityID ) 
	OR ( 
		@CredentialEntityID IS NULL
		AND ClientID = @ClientID
		AND SecretKey = @SecretKey 
	)

	-- Void the output data if the object could not be validated.
	IF @@ROWCOUNT = 0
	BEGIN
		SET @CredentialEntityID = NULL;
		SET @ClientID = NULL;
		SET @SecretKey = NULL;
		SET @Name = NULL;
	END






END
