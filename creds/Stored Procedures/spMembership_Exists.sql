﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Checks if the credential already has a membership created
-- SAMPLE CALL:
/*

DECLARE
	@MembershipID BIGINT = 1,
	@CredentialEntityID BIGINT = 801026
	
EXEC creds.spMembership_Exists
	@MembershipID = @MembershipID OUTPUT,
	@CredentialEntityID = @CredentialEntityID OUTPUT

SELECT @MembershipID AS MembershipID, @CredentialEntityID AS CredentialEntityID

*/

-- SELECT * FROM creds.Membership
-- =============================================
CREATE PROCEDURE [creds].[spMembership_Exists]
	@MembershipID BIGINT = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,	
	@EmailAddress VARCHAR(256) = NULL OUTPUT,
	@PasswordHash VARBINARY(128) = NULL OUTPUT,
	@PasswordSalt VARBINARY(10) = NULL OUTPUT,
	@PasswordQuestionID INT = NULL OUTPUT,
	@PasswordAnswerHash VARBINARY(128) = NULL OUTPUT,
	@PasswordAnswerSalt VARBINARY(10) = NULL OUTPUT,
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	---------------------------------------------------------------
	-- Sanitize input
	-- <Summary>
	-- Cleans input variables and ensures the output variables are
	-- properly set upon retrieval
	-- </Summary>
	---------------------------------------------------------------
	SET @Exists = 0;	
	
	---------------------------------------------------------------
	-- Determine if the credential membership already exists
	---------------------------------------------------------------
	EXEC creds.spMembership_Get
		@MembershipID = @MembershipID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@EmailAddress = @EmailAddress OUTPUT,
		@PasswordHash = @PasswordHash OUTPUT,
		@PasswordSalt = @PasswordSalt OUTPUT,
		@PasswordQuestionID = @PasswordQuestionID OUTPUT,
		@PasswordAnswerHash = @PasswordAnswerHash OUTPUT,
		@PasswordAnswerSalt = @PasswordAnswerSalt OUTPUT,
		@IsApproved = @IsApproved OUTPUT,
		@IsLockedOut = @IsLockedOut OUTPUT,
		@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
		@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
		@DatePasswordExpires = @DatePasswordExpires OUTPUT,
		@DateLastLockedOut = @DateLastLockedOut OUTPUT,
		@DateLockOutExpires = @DateLockOutExpires OUTPUT,
		@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
		@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
		@IsDisabled = @IsDisabled OUTPUT
	
	
	-- Determine if the record exists by inspecting the returned MembershipID.
	-- If an ID was returned, then we have the record; otherwise, a record was
	-- not discovered.
	IF @MembershipID IS NOT NULL
	BEGIN
		SET @Exists = 1;
	END
	
	
END
