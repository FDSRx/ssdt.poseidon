﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/2/2015
-- Description:	Determines if CredentialEntityApplication object exists.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityApplicationID BIGINT = NULL,
	@Exists BIT = NULL
	
	
EXEC creds.spCredentialEntityApplication_Exists
	@CredentialEntityID = 959790,
	@ApplicationID = 8,
	@CredentialEntityApplicationID = @CredentialEntityApplicationID OUTPUT,
	@Exists = @Exists OUTPUT

SELECT @CredentialEntityApplicationID AS CredentialEntityApplicationID, @Exists AS IsFound

*/

-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntityApplication
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntityApplication_Exists]
	@CredentialEntityID BIGINT,
	@ApplicationID INT,
	@CreatedBy VARCHAR(256) = NULL,
	@CredentialEntityApplicationID BIGINT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------
	DECLARE @ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';'
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';'
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';'
		'@CredentialEntityApplicationID=' + dbo.fnToStringOrEmpty(@CredentialEntityApplicationID) + ';'
		'@Exists=' + dbo.fnToStringOrEmpty(@Exists) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CredentialEntityApplicationID = NULL;
	SET @Exists = 0;
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)

	/*
	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- CredentialEntityID
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- ApplicationID
	IF @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@RoleID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/
		
	-------------------------------------------------------------------------------
	-- Determine if CredentialEntityApplication object exists.
	-------------------------------------------------------------------------------
	SELECT TOP 1
		@CredentialEntityApplicationID = CredentialEntityApplicationID
	FROM creds.CredentialEntityApplication cea
	WHERE CredentialEntityID = @CredentialEntityID
		AND ApplicationID = @ApplicationID
	
	IF @@ROWCOUNT = 1
	BEGIN		
		SET @Exists = 1;		
	END
		
		

		
END
