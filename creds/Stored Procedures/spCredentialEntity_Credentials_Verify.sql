﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/7/2014
-- Description:	Verifies the provided credentials. No logging in/authenticating is actually performed.
/*

DECLARE 
	@IsValid BIT = 0,
	@CredentialEntityID BIGINT = NULL,
	@CredentialToken VARCHAR(50) = NULL,
	@IsLockedOut BIT = NULL,
	@IsDisabled BIT = NULL

EXEC creds.spCredentialEntity_Credentials_Verify
	@ApplicationID = NULL,
	@BusinessID = 10684,
	@Username = 'dhughes',
	@Password = 'password',
	@CredentialEntityTypeID = 1,
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@CredentialToken = @CredentialToken OUTPUT,
	@IsValid = @IsValid OUTPUT,
	@IsLockedOut = @IsLockedOut OUTPUT,
	@IsDisabled = @IsDisabled OUTPUT

SELECT @IsValid AS IsValid, @CredentialEntityID AS CredentialEntityID, @CredentialToken AS CredentialToken,
	@IsLockedOut AS IsLockedOut, @IsDisabled AS IsDisabled


EXEC creds.spMembership_Unlock
	@CredentialEntityID = 801050,
	@ForceUnlock = 1	
	
*/

-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
-- SELECT * FROM creds.CredentialEntity
-- SELECT * FROM creds.vwExtranetUser WHERE Username = 'dhughes'
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntity_Credentials_Verify]
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL OUTPUT,
	@Username VARCHAR(256) = NULL,
	@Password VARCHAR(50) = NULL,
	@CredentialEntityTypeID INT = NULL,
	@CredentialToken VARCHAR(50) = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@Roles XML = NULL OUTPUT,
	@IgnoreLockedOutValidation BIT = NULL,
	@IgnoreDisabledValidation BIT = NULL,
	@IgnoreChangePasswordValidation BIT = NULL,
	@IsValid BIT = 0 OUTPUT,
	-- Core membership properties ****************************
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT,
	-- *******************************************************
	@IsChangePasswordRequired BIT = NULL OUTPUT,
	@IsFirstLogin BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@Password=' + dbo.fnToStringOrEmpty(@Password) + ';' +
		'@CredentialEntityTypeID=' + dbo.fnToStringOrEmpty(@CredentialEntityTypeID) + ';' +
		'@CredentialToken=' + dbo.fnToStringOrEmpty(@CredentialToken) + ';' 

	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	---------------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------------
	SET @IsValid = 0;
	SET @Roles = NULL;
	SET @IsApproved = 0;
	SET	@IsLockedOut = 0;
	SET	@DateLastLoggedIn = NULL;
	SET	@DateLastPasswordChanged = NULL;
	SET @DatePasswordExpires = NULL;
	SET	@DateLastLockedOut = NULL;
	SET @DateLockOutExpires = NULL;
	SET	@FailedPasswordAttemptCount = 0;
	SET	@FailedPasswordAnswerAttemptCount = 0;
	SET @IsDisabled = 0;
	SET @IsChangePasswordRequired = 0;
	SET	@IsFirstLogin = 0;

	---------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------	
	DECLARE 
		@IsExpired BIT = 0

	---------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	---------------------------------------------------------------------------	
	IF @BusinessID IS NULL 
		OR (
			dbo.fnIsNullOrWhiteSpace(@Username) = 1 
				AND dbo.fnIsNullOrWhiteSpace(@Password) = 1
				AND dbo.fnIsNullOrWhiteSpace(@CredentialToken) = 1
		)
	BEGIN
		SET @IsValid = 0;
		RETURN;
	END
				
	
	-- debug
	--SELECT @ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @Username AS Username, @Password AS Password, @CredentialToken AS CredentialToken
	
	
	
	---------------------------------------------------------------------------
	-- Authenticate credentials
	-- <Summary>
	-- If the CredentialToken object is provided, then only use the token as it
	-- is an acquired object from a pre-authorization and negates the need to perform
	-- a username and password lookup.
	-- </Summary>
	---------------------------------------------------------------------------
	IF dbo.fnIsNullOrWhiteSpace(@CredentialToken) = 0
	BEGIN
	
		EXEC creds.spCredentialToken_Update
			@Token = @CredentialToken OUTPUT,
			@IsExpired = @IsExpired OUTPUT,
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessID OUTPUT,
			@CredentialEntityID = @CredentialEntityID OUTPUT,
			@InvalidateTokenOnExpired = 1
		
		SET @IsValid = CASE WHEN @IsExpired = 0 THEN 1 ELSE 0 END;
			
	END
	ELSE
	BEGIN

		EXEC creds.spCredentialEntity_Credentials_Authenticate
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessID,
			@Username = @Username,
			@Password = @Password,
			@CredentialEntityTypeID = @CredentialEntityTypeID,
			@IgnoreTokenCreation = 1,
			@IgnoreLockedOutValidation = @IgnoreLockedOutValidation,
			@IgnoreDisabledValidation = @IgnoreDisabledValidation,
			@IgnoreChangePasswordValidation = @IgnoreChangePasswordValidation,
			@CredentialEntityID = @CredentialEntityID OUTPUT,
			@CredentialToken = @CredentialToken OUTPUT,
			@IsValid = @IsValid OUTPUT,
			-- Core membership properties ****************************
			@IsApproved = @IsApproved OUTPUT,
			@IsLockedOut = @IsLockedOut OUTPUT,
			@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
			@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
			@DatePasswordExpires = @DatePasswordExpires OUTPUT,
			@DateLastLockedOut = @DateLastLockedOut OUTPUT,
			@DateLockOutExpires = @DateLockOutExpires OUTPUT,		
			@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
			@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
			@IsDisabled = @IsDisabled OUTPUT,
			-- *******************************************************
			@IsChangePasswordRequired = @IsChangePasswordRequired OUTPUT,
			@IsFirstLogin = @IsFirstLogin OUTPUT
			
	END
	
	
	
	
	
	
	
	
	
	
	
	
	
END
