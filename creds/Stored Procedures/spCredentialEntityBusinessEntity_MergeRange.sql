﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/2/2014
-- Description:	Creates a single or collection of CredentialEntityBusinessEntity objects.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityBusinessEntityIDs VARCHAR(MAX) = NULL
	
EXEC creds.spCredentialEntityBusinessEntity_MergeRange
	@CredentialEntityID = 959790,
	@ApplicationID = 8,
	@BusinessEntityID = '3759,44,256,261',
	@ModifiedBy = 'dhughes',	
	@CreatedBy = 'dhughes',
	@CredentialEntityBusinessEntityIDs = @CredentialEntityBusinessEntityIDs OUTPUT

SELECT @CredentialEntityBusinessEntityIDs AS CredentialEntityBusinessEntityIDs


*/

-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntityBusinessEntity
-- SELECT * FROM dbo.Store
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntityBusinessEntity_MergeRange]
	@CredentialEntityID BIGINT,
	@ApplicationID INT = NULL,
	@BusinessEntityID VARCHAR(MAX) = NULL,
	@BusinessEntityTypeID INT,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256),
	@DeleteUnspecified BIT = NULL,
	@CredentialEntityBusinessEntityIDs VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------
	DECLARE @Trancount INT = @@TRANCOUNT;
	
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CredentialEntityBusinessEntityIDs = NULL;
	SET @DeleteUnspecified = ISNULL(@DeleteUnspecified, 0);
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @CredentialEntityID
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntity association. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	-- @BusinessEntityTypeID
	IF @BusinessEntityTypeID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntity association. Object reference (@BusinessEntityTypeID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityTypeID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END		
	
	/*
	-- @BusinessEntityID
	IF dbo.fnIsNullOrWhiteSpace(@BusinessEntityID) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntity association. Object reference (@RoleID) is not set to an instance of an object. ' +
			'The parameter, @RoleID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/


	BEGIN TRY
	--------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		-------------------------------------------------------------------------------
		-- Temporary objects.
		-------------------------------------------------------------------------------
		DECLARE @tblOutput AS TABLE (
			Idx INT IDENTITY(1,1),
			CredentialEntityBusinessEntityID BIGINT
		);
		
		-------------------------------------------------------------------------------
		-- Store ModifiedBy property in "session" like object.
		-------------------------------------------------------------------------------
		EXEC memory.spContextInfo_TriggerData_Set
			@ObjectName = @ProcedureName,
			@DataString = @ModifiedBy
		
		
		-------------------------------------------------------------------------------
		-- Remove unspecified items (if applicable).
		-- <Summary>
		-- Removes items that are not specified in the range of items.
		-- </Summary>
		-------------------------------------------------------------------------------
		IF @DeleteUnspecified = 1
		BEGIN
			DELETE cebe
			FROM creds.CredentialEntityBusinessEntity cebe
				JOIN dbo.BusinessEntity be
					ON cebe.BusinessEntityID = be.BusinessEntityID
			WHERE be.BusinessEntityTypeID = @BusinessEntityTypeID
				AND cebe.CredentialEntityID = @CredentialEntityID
				AND ( @ApplicationID IS NULL OR cebe.ApplicationID = @ApplicationID )
				AND ( dbo.fnIsNullOrWhiteSpace(@BusinessEntityID) = 1 
					OR cebe.BusinessEntityID NOT IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@BusinessEntityID, ',')) )
		END
				
		-------------------------------------------------------------------------------
		-- Create new CredentialEntityBusinessEntity object(s).
		-------------------------------------------------------------------------------
		INSERT INTO creds.CredentialEntityBusinessEntity (
			CredentialEntityID,
			ApplicationID,
			BusinessEntityID,			
			CreatedBy
		)
		OUTPUT inserted.CredentialEntityBusinessEntityID INTO @tblOutput(CredentialEntityBusinessEntityID)
		SELECT
			@CredentialEntityID AS CredentialEntityID,
			@ApplicationID AS ApplicationID,			
			Value AS RoleID,
			@CreatedBy
		FROM dbo.fnSplit(@BusinessEntityID, ',') biz
			JOIN dbo.BusinessEntity be
				ON CASE WHEN ISNUMERIC(biz.Value) = 1 THEN biz.Value ELSE NULL END = be.BusinessEntityID
			LEFT JOIN creds.CredentialEntityBusinessEntity cebe
				ON cebe.CredentialEntityID = @CredentialEntityID
					AND ( @ApplicationID IS NULL OR cebe.ApplicationID = @ApplicationID )
					AND cebe.BusinessEntityID = CASE WHEN ISNUMERIC(biz.Value) = 1 THEN biz.Value ELSE NULL END
		WHERE be.BusinessEntityTypeID = @BusinessEntityTypeID
			AND ISNUMERIC(biz.Value) = 1
			AND cebe.CredentialEntityBusinessEntityID IS NULL
		
		-------------------------------------------------------------------------------
		-- Retrieve record identity value(s).
		-------------------------------------------------------------------------------
		SET @CredentialEntityBusinessEntityIDs = (
			SELECT 
				CONVERT(VARCHAR(25), CredentialEntityBusinessEntityID) + ','
			FROM @tblOutput
			FOR XML PATH('')
		);
		
		IF RIGHT(@CredentialEntityBusinessEntityIDs, 1) = ','
		BEGIN
			SET @CredentialEntityBusinessEntityIDs = LEFT(@CredentialEntityBusinessEntityIDs, LEN(@CredentialEntityBusinessEntityIDs) - 1);
		END

	--------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END		
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);	
	
	END CATCH
	
		
END
