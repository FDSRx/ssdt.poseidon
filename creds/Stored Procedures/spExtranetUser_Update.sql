﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/10/2014
-- Description:	Updates an Extranet user.
-- SAMPLE CALL:
/*

DECLARE
	-- Credential Identifier
	@CredentialEntityID BIGINT = '801050',
	-- Application
	@ApplicationID INT = NULL,
	@ApplicationIDList VARCHAR(MAX) = NULL,
	-- Business
	@BusinessEntityID BIGINT = NULL,
	-- Roles
	@RoleIDList VARCHAR(MAX) = NULL,
	@DeleteUnspecifiedRoles BIT = NULL,
	-- User
	@Username VARCHAR(256) = NULL,
	@Password VARCHAR(50) = 'password',
	@PasswordQuestionID INT = NULL,
	@PasswordAnswer VARCHAR(500) = NULL,
	-- Membership
	@IsApproved BIT = NULL,
	@IsChangePasswordRequired BIT = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@IsDisabled BIT = NULL,
	-- Person
	@PersonID BIGINT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderID INT = NULL,
	@GenderCode VARCHAR(50) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(50) = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = 1


-- Update demographics.
EXEC creds.spExtranetUser_Update
	@CredentialEntityID = @CredentialEntityID,
	@ApplicationID = @ApplicationID,
	@ApplicationIDList = @ApplicationIDList,
	@BusinessEntityID = @BusinessEntityID,
	@RoleIDList = @RoleIDList,
	@DeleteUnspecifiedRoles = @DeleteUnspecifiedRoles,
	@Username = @Username,
	@Password = @Password,
	@PasswordQuestionID = @PasswordQuestionID,
	@PasswordAnswer = @PasswordAnswer,
	@IsApproved = @IsApproved,
	@IsChangePasswordRequired = @IsChangePasswordRequired,
	@DatePasswordExpires = @DatePasswordExpires,
	@IsDisabled = @IsDisabled,
	@PersonID = @PersonID,
	@Title = @Title,
	@FirstName = @FirstName,
	@LastName = @LastName,
	@MiddleName = @MiddleName,
	@Suffix = @Suffix,
	@BirthDate = @BirthDate,
	@GenderID = @GenderID,
	@GenderCode = @GenderCode,
	@LanguageID = @LanguageID,
	@LanguageCode = @LanguageCode,
	@AddressLine1 = @AddressLine1,
	@AddressLine2 = @AddressLine2,
	@City = @City,
	@State = @State,
	@PostalCode = @PostalCode,
	@PhoneNumber_Home = @PhoneNumber_Home,
	@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
	@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
	@PhoneNumber_Mobile = @PhoneNumber_Mobile,
	@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
	@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
	@EmailAddress_Primary = @EmailAddress_Primary,
	@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
	@EmailAddress_Alternate = @EmailAddress_Alternate,
	@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

	
*/


-- SELECT * FROM creds.Membership WHERE CredentialEntityID = 801050
-- SELECT * FROM creds.CredentialEntity WHERE CredentialEntityID = 801053
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- SELECT * FROM dbo.Person WHERE BusinessEntityID = 801053
-- SELECT * FROM acl.CredentialEntityRole WHERE CredentialEntityID = 801053
-- SELECT * FROM dbo.Person_History WHERE BusinessEntityID = 801053

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [creds].[spExtranetUser_Update]
	-- Credential Identifier
	@CredentialEntityID BIGINT,
	-- Application
	@ApplicationID INT = NULL,
	@ApplicationIDList VARCHAR(MAX) = NULL,
	-- Business
	@BusinessEntityID BIGINT = NULL,
	-- Roles
	@RoleIDList VARCHAR(MAX) = NULL,
	@DeleteUnspecifiedRoles BIT = NULL,
	-- User
	@Username VARCHAR(256) = NULL,
	@Password VARCHAR(50) = NULL,
	@PasswordQuestionID INT = NULL,
	@PasswordAnswer VARCHAR(500) = NULL,
	-- Membership
	@IsApproved BIT = NULL,
	@IsChangePasswordRequired BIT = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@IsDisabled BIT = NULL,
	-- Person
	@PersonID BIGINT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderID INT = NULL,
	@GenderCode VARCHAR(50) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(50) = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	-- Incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationIDList=' + dbo.fnToStringOrEmpty(@ApplicationIDList) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@RoleIDList=' + dbo.fnToStringOrEmpty(@RoleIDList) + ';' +
		'@DeleteUnspecifiedRoles=' + dbo.fnToStringOrEmpty(@DeleteUnspecifiedRoles) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@Password=' + dbo.fnToStringOrEmpty(@Password) + ';' +
		'@PasswordQuestionID=' + dbo.fnToStringOrEmpty(@PasswordQuestionID) + ';' +
		'@PasswordAnswer=' + dbo.fnToStringOrEmpty(@PasswordAnswer) + ';' +
		'@IsApproved=' + dbo.fnToStringOrEmpty(@IsApproved) + ';' +
		'@IsChangePasswordRequired=' + dbo.fnToStringOrEmpty(@IsChangePasswordRequired) + ';' +
		'@DatePasswordExpires=' + dbo.fnToStringOrEmpty(@DatePasswordExpires) + ';' +
		'@IsDisabled=' + dbo.fnToStringOrEmpty(@IsDisabled) + ';' +
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName) + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName) + ';' +
		'@MiddleName=' + dbo.fnToStringOrEmpty(@MiddleName) + ';' +
		'@Suffix=' + dbo.fnToStringOrEmpty(@Suffix) + ';' +
		'@BirthDate=' + dbo.fnToStringOrEmpty(@BirthDate) + ';' +
		'@GenderID=' + dbo.fnToStringOrEmpty(@GenderID) + ';' +
		'@GenderCode=' + dbo.fnToStringOrEmpty(@GenderCode) + ';' +
		'@LanguageID=' + dbo.fnToStringOrEmpty(@LanguageID) + ';' +
		'@LanguageCode=' + dbo.fnToStringOrEmpty(@LanguageCode) + ';' +
		'@AddressLine1=' + dbo.fnToStringOrEmpty(@AddressLine1) + ';' +
		'@AddressLine2=' + dbo.fnToStringOrEmpty(@AddressLine2) + ';' +
		'@City=' + dbo.fnToStringOrEmpty(@City) + ';' +
		'@State=' + dbo.fnToStringOrEmpty(@State) + ';' +
		'@PostalCode=' + dbo.fnToStringOrEmpty(@PostalCode) + ';' +
		'@PhoneNumber_Home=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home) + ';' +
		'@PhoneNumber_Home_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsCallAllowed) + ';' +
		'@PhoneNumber_Home_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsTextAllowed) + ';' +
		'@PhoneNumber_Mobile=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile) + ';' +
		'@PhoneNumber_Mobile_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsCallAllowed) + ';' +
		'@PhoneNumber_Mobile_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsTextAllowed) + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary) + ';' +
		'@EmailAddress_Primary_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsEmailAllowed) + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate) + ';' +
		'@EmailAddress_Alternate_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsEmailAllowed) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;


	---------------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	---------------------------------------------------------------------------------------------------------------------------------
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/	


	---------------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------------------------------------------
	SET @ModifiedBy = COALESCE(@ModifiedBy, @CreatedBy, SUSER_NAME());
	SET @CreatedBy = ISNULL(@CreatedBy, @ModifiedBy);
	SET @DeleteUnspecifiedRoles = ISNULL(@DeleteUnspecifiedRoles, 0);	
	SET @Debug = ISNULL(@Debug, 0);

	
	---------------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@PersonTypeID INT = dbo.fnGetPersonTypeID('CRED'),
		@ErrorLogID BIGINT = NULL,
		@IsUpdated BIT = 0

	---------------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	---------------------------------------------------------------------------------------------------------------------------------
	-- No initialization necessary.
		
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@CredentialEntityID AS CredentialEntityID, @BusinessEntityID AS BusinessEntityID, @ApplicationID AS ApplicationID,
			@ModifiedBy AS ModifiedBy, @CreatedBy AS CreatedBy, @DeleteUnspecifiedRoles AS DeleteUnspecifiedRoles,
			@PersonTypeID AS PersonTypeID, @Username AS UserName, @Password AS Password, @Args AS Arguments
	END


		
	---------------------------------------------------------------------------------------------------------------------------------
	-- Check for null arguments
	-- <Summary>
	-- Checks critical inputs for null argument exceptions.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------------------------------		
	---------------------------------------------------------------------------------------------------------------------------------
	-- Argument is null exception: @CredentialEntityID
	---------------------------------------------------------------------------------------------------------------------------------
	IF @CredentialEntityID IS NULL
	BEGIN
			
		SET @ErrorMessage = 'Unable to modify Extranet User. Exception: Object reference (@CredentialEntityID) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END	
		
	---------------------------------------------------------------------------------------------------------------------------------
	-- Modify an extranet user.
	-- <Summary>
	-- Interrogates the Credential to determine if the user has existing
	-- demographics, or if demographics need to be added.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------------------------------	
	BEGIN TRY
	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	---------------------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	---------------------------------------------------------------------------------------------------------------------------------
	
		---------------------------------------------------------------------------------------------------------------------------------
		-- Overwrite Person object. (if applicable) 
		-- <Summary>
		-- If a Person identifier was provided, then let's swap the existing identifier with the provided
		-- identifier.
		-- </Summary>
		---------------------------------------------------------------------------------------------------------------------------------
		IF @PersonID IS NOT NULL AND @IsUpdated = 0
		BEGIN
		
			---------------------------------------------------------------------------------------------------------------------------------
			-- Overwrite existing Person identifier.
			---------------------------------------------------------------------------------------------------------------------------------
			UPDATE ce
			SET
				ce.PersonID = @PersonID,
				ce.DateModified = GETDATE(),
				ce.ModifiedBy = @ModifiedBy
			FROM creds.CredentialEntity ce
			WHERE CredentialEntityID = @CredentialEntityID
			
			SET @IsUpdated = 1;
			
		END
		
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Interrogate CredentialObject to determine how to update the attached Person object.
		---------------------------------------------------------------------------------------------------------------------------------
		DECLARE 
			@CredentialPersonID BIGINT = NULL,
			@IsCredentityEntityIDAPersonID BIGINT = NULL
		
		SET @CredentialPersonID = (SELECT PersonID FROM creds.CredentialEntity WHERE CredentialEntityID = @CredentialEntityID);
		SET @IsCredentityEntityIDAPersonID = CASE WHEN dbo.fnGetPersonID(@CredentialEntityID) IS NULL THEN 0 ELSE 1 END;
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Determine if a person currently exists. 
		-- <Summary>
		-- If a Person object is part of the credential, then let's update the Person object with the
		-- provided demographics.  Otherwise, let's see if we can use the CredentialEntityID as our
		-- Person object.
		-- </Summary>
		---------------------------------------------------------------------------------------------------------------------------------
		IF @CredentialPersonID IS NOT NULL AND @IsUpdated = 0
		BEGIN
		
			---------------------------------------------------------------------------------------------------------------------------------
			-- Update the existing Person object.
			-- <Summary>
			-- Updates the user's contact (demographic) information.
			-- </Summary>
			---------------------------------------------------------------------------------------------------------------------------------

			-- Update contact information.
			EXEC dbo.spPerson_ContactInformation_Update
				--Person
				@PersonID = @CredentialPersonID,
				@Title = @Title,
				@FirstName = @FirstName,
				@LastName = @LastName,
				@MiddleName = @MiddleName,
				@Suffix = @Suffix,
				@BirthDate = @BirthDate,
				@GenderID = @GenderID,
				@LanguageID = @LanguageID,
				--Person Address
				@AddressLine1 = @AddressLine1,
				@AddressLine2 = @AddressLine2,
				@City = @City,
				@State = @State,
				@PostalCode = @PostalCode,
				--Person Phone
				--	Home
				@PhoneNumber_Home = @PhoneNumber_Home,
				@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
				@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
				-- Mobile
				@PhoneNumber_Mobile = @PhoneNumber_Mobile,
				@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
				@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
				--Person Email
				--	Primary
				@EmailAddress_Primary = @EmailAddress_Primary,
				@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
				--	Alternate
				@EmailAddress_Alternate = @EmailAddress_Alternate,
				@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
				-- Caller
				@ModifiedBy = @ModifiedBy
			
			---------------------------------------------------------------------------------------------------------------------------------
			-- Stamp the CredentialEntity as being modified.
			-- <Summary>
			-- Marks the CredentialEntity object as having a modification perfomed.
			-- </Summary>
			---------------------------------------------------------------------------------------------------------------------------------
			UPDATE ce
			SET
				ce.DateModified = GETDATE(),
				ce.ModifiedBy = @ModifiedBy
			FROM creds.CredentialEntity ce
			WHERE CredentialEntityID = @CredentialEntityID
			
			SET @IsUpdated = 1;
		END
		
		---------------------------------------------------------------------------------------------------------------------------------
		-- Determine if the CredentialEntityID is allowed to be extended as a Person object.
		-- <Summary>
		-- If the CredentityEntityID does not exist as a person object, then we can use the 
		-- credential idenifier as the person identifier. If the CrednetialEntityID does exist,
		-- then we will need to create a new Person object as we cannot guarentee that they are one
		-- in the same.
		-- </Summary>
		---------------------------------------------------------------------------------------------------------------------------------
		IF @CredentialPersonID IS NULL AND @IsUpdated = 0
		BEGIN
		
			-- If the CredentialEntityID is not defined as a Person object, then extend the CredentialEntity object
			-- to be a person.
			SET @CredentialPersonID = CASE WHEN @IsCredentityEntityIDAPersonID = 0 THEN @CredentialEntityID ELSE NULL END;
			
			-- If a birth date was not supplied then use "1/1/1900" as the default birthday for the person record.
			SET @BirthDate = ISNULL(@BirthDate, '1/1/1900');
			
			---------------------------------------------------------------------------------------------------------------------------------
			-- Create a Person object.
			-- <Summary>
			-- If the CredentialEntity is not a person, then it will be extended as a Person object;
			-- otherwise, a new Person object will be created and attached to the CredentialEntity.
			-- </Summary>
			---------------------------------------------------------------------------------------------------------------------------------
			EXEC dbo.spPerson_Create
				@PersonID = @CredentialPersonID OUTPUT,
				@PersonTypeID = @PersonTypeID,
				@Title = @Title,
				@FirstName = @FirstName,
				@LastName = @LastName,
				@MiddleName = @MiddleName,
				@Suffix = @Suffix,
				@BirthDate = @BirthDate,
				@GenderID = @GenderID,
				@LanguageID = @LanguageID,
				--Person Address
				@AddressLine1 = @AddressLine1,
				@AddressLine2 = @AddressLine2,
				@City = @City,
				@State = @State,
				@PostalCode = @PostalCode,
				--Person Phone
				--	Home
				@PhoneNumber_Home = @PhoneNumber_Home,
				@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
				@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
				-- Mobile
				@PhoneNumber_Mobile = @PhoneNumber_Mobile,
				@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
				@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
				--Person Email
				--	Primary
				@EmailAddress_Primary = @EmailAddress_Primary,
				@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
				--	Alternate
				@EmailAddress_Alternate = @EmailAddress_Alternate,
				@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
				--Caller
				@CreatedBy = @CreatedBy,
				--Output
				@ErrorLogID = @ErrorLogID OUTPUT,
				@ThrowCaughtException = 1 -- throw the caught exception if encountered.	
		
			
			---------------------------------------------------------------------------------------------------------------------------------
			-- Attach the Person object to the CredentialEntity.
			-- <Summary>
			-- Attaches the returned Person object to the CredentialEntity.
			-- </Summary>
			---------------------------------------------------------------------------------------------------------------------------------
			UPDATE ce
			SET
				ce.PersonID = @CredentialPersonID,
				ce.DateModified = GETDATE(),
				ce.ModifiedBy = @ModifiedBy
			FROM creds.CredentialEntity ce
			WHERE CredentialEntityID = @CredentialEntityID
			
			
		END
	

		---------------------------------------------------------------------------------------------------------------------------------
		-- Update membership for the credential
		-- <Summary>
		-- Updates a credential membership that consists of 
		-- encrypted password information and security question
		-- information (if applicable).
		-- </Summary>
		---------------------------------------------------------------------------------------------------------------------------------
		IF @CredentialEntityID IS NOT NULL
		BEGIN
		
			EXEC creds.spMembership_Update
				@CredentialEntityID = @CredentialEntityID,
				@EmailAddress = @EmailAddress_Primary,
				@Password = @Password,
				@IsApproved = @IsApproved,
				@IsChangePasswordRequired = @IsChangePasswordRequired,
				@DatePasswordExpires = @DatePasswordExpires,
				@IsDisabled = @IsDisabled,
				@Debug = @Debug
				
		END
		

		---------------------------------------------------------------------------------------------------------------------------------
		-- Merge (add/update) roles for the specified application, business, and user.
		-- <Summary>
		-- Updates the credentials set of roles.
		-- </Summary>
		---------------------------------------------------------------------------------------------------------------------------------
		IF @CredentialEntityID IS NOT NULL 
			AND @ApplicationID IS NOT NULL 
			AND @BusinessEntityID IS NOT NULL
		BEGIN
		
			DECLARE 
				@RoleIDArray VARCHAR(MAX) = acl.fnRoleKeyTranslator(@RoleIDList, DEFAULT);
				
			EXEC acl.spCredentialEntityRole_MergeRange
				@CredentialEntityID = @CredentialEntityID,
				@ApplicationID = @ApplicationID,
				@BusinessEntityID = @BusinessEntityID,
				@RoleID = @RoleIDArray,
				@DeleteUnspecified = @DeleteUnspecifiedRoles,
				@CreatedBy = @CreatedBy,
				@ModifiedBy = @ModifiedBy
				
		END		
		
		
		
	---------------------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	---------------------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END

		-- Log and rethrow error
		EXECUTE [dbo].spLogException
			@Arguments = @Args
		
		-- re-throw error
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		
		---- Verifies that data is in a commitable state.  If not, then we need to
		---- re-throw the error so that it continues to bubble up the chain.
		--EXECUTE dbo.spTryThrowUncommitableWriteError @ErrorProcedure;
		
	END CATCH
	
END
