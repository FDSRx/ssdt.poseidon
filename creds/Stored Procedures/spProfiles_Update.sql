﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/21/2014
-- Description:	Updates a user profile record
-- SAMPLE CALL:
/*
DECLARE 
	@PropertyValueString VARCHAR(MAX), 
	@PropertyValueBinary VARBINARY(MAX)
	
EXEC creds.spProfiles_Update
	@ProfileID = NULL,
	@UserEntityID = 10,
	@PropertyName = 'PRFSTRE',
	@PropertyValueString = @PropertyValueString,
	@PropertyValueBinary = @PropertyValueBinary
*/
-- SELECT * FROM creds.Profiles
-- =============================================
CREATE PROCEDURE [creds].[spProfiles_Update]
	@ProfileID BIGINT = NULL,
	@CredentialEntityID BIGINT = NULL,
	@PropertyName VARCHAR(256) = NULL,
	@PropertyValueString VARCHAR(MAX) = NULL,
	@PropertyValueBinary VARBINARY(MAX) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------
	IF @ProfileID IS NOT NULL
	BEGIN
		SET @CredentialEntityID = NULL;
		SET @PropertyName = NULL;
	END
		
	----------------------------------------------------------------------
	-- Update profile entry
	----------------------------------------------------------------------
	IF @ProfileID IS NOT NULL OR (@CredentialEntityID IS NOT NULL AND @PropertyName IS NOT NULL)
	BEGIN
	
		UPDATE prf
		SET 
			prf.PropertyValueString = @PropertyValueString,
			prf.PropertyValueBinary = @PropertyValueBinary,
			prf.DateModified = GETDATE(),
			prf.ModifiedBy = @ModifiedBy
		--SELECT *
		FROM creds.Profiles prf
		WHERE ProfileID = @ProfileID
			OR (CredentialEntityID = @CredentialEntityID
				AND PropertyName = @PropertyName
			)
	
	END
		
	
	
END

