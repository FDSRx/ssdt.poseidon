﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/8/2014
-- Description:	Returns a membership record.
-- SAMPLE CALL:
/* 
DECLARE 
	@Password VARBINARY(128), 
	@Salt VARBINARY(10),
	@IsLocked BIT,
	@IsDisabled BIT

EXEC creds.spMembership_Get 
	@CredentialEntityID = 801018, 
	@PasswordHash = @Password OUTPUT, 
	@PasswordSalt = @Salt OUTPUT,
	@IsLockedOut = @IsLocked OUTPUT,
	@IsDisabled = @IsDisabled OUTPUT
	
SELECT @Password AS Password, @Salt AS Salt, @IsLocked AS IsLocked, @IsDisabled IsDisabled
 
*/

-- SELECT * FROM creds.Membership
-- =============================================
CREATE PROCEDURE [creds].[spMembership_Get]
	@MembershipID BIGINT = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@EmailAddress VARCHAR(256) = NULL OUTPUT,
	@PasswordHash VARBINARY(128) = NULL OUTPUT,
	@PasswordSalt VARBINARY(10) = NULL OUTPUT,
	@PasswordQuestionID INT = NULL OUTPUT,
	@PasswordAnswerHash VARBINARY(128) = NULL OUTPUT,
	@PasswordAnswerSalt VARBINARY(128) = NULL OUTPUT,
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT,
	@Comment VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------
	SET @EmailAddress = NULL;
	SET @PasswordHash = NULL;
	SET @PasswordSalt = NULL;
	SET @PasswordQuestionID = NULL;
	SET @PasswordAnswerHash = NULL;
	SET @PasswordAnswerSalt = NULL;
	SET @IsApproved = 0;
	SET @IsLockedOut = 0;
	SET @DateLastLoggedIn = NULL;
	SET @DateLastPasswordChanged = NULL;
	SET @DatePasswordExpires = NULL;
	SET @DateLastLockedOut = NULL;
	SET @FailedPasswordAttemptCount = 0;
	SET @FailedPasswordAnswerAttemptCount = 0;
	SET @IsDisabled = 0;
	SET @Comment = NULL;

	--------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)

	
	/*
	--------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules.
	-- </Summary>
	--------------------------------------------------------------
	-- A tag name is required to be created.
	IF @MembershipID IS NULL AND @CredentialEntityID IS NULL
	BEGIN
	
		SET @ErrorMessage = 'Unable to retrieve Membership entry.  Object references (@MembershipID or @CredentialEntityID) are not set to an instance of an object. ' +
			'The @MembershipID or @CredentialEntityID cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	*/
	
	--------------------------------------------------------------
	-- Interrogate unique keys and determine winner.
	-- <Summary>
	-- If the primary record key is supplied (i.e. the surrogate key)
	-- then declare that key the winner and void the foreign key; 
	-- otherwise, keep the foreign unique key.
	-- </Summary>
	--------------------------------------------------------------	
	IF @MembershipID IS NOT NULL
	BEGIN
		SET @CredentialEntityID = NULL;
	END
	
	-- Debug
	--SELECT @MembershipID AS MembershipID, @CredentialEntityID AS CredentialEntityID
		
	--------------------------------------------------------------
	-- Fetch membership data
	--------------------------------------------------------------
	SELECT TOP 1
		@MembershipID = MembershipID,
		@CredentialEntityID = CredentialEntityID,
		@EmailAddress = EmailAddress,
		@PasswordHash = PasswordHash,
		@PasswordSalt = PasswordSalt,
		@PasswordQuestionID = PasswordQuestionID,
		@PasswordAnswerHash = PasswordAnswerHash,
		@PasswordAnswerSalt = PasswordAnswerSalt,
		@IsApproved = IsApproved,
		@IsLockedOut = IsLockedOut,
		@DateLastLoggedIn = DateLastLoggedIn,
		@DateLastPasswordChanged = DateLastPasswordChanged,
		@DatePasswordExpires = DatePasswordExpires,
		@DateLastLockedOut = DateLastLockedOut,
		@DateLockOutExpires = DateLockOutExpires,
		@FailedPasswordAttemptCount = FailedPasswordAttemptCount,
		@FailedPasswordAnswerAttemptCount = FailedPasswordAnswerAttemptCount,
		@IsDisabled = IsDisabled,
		@Comment = Comment
	--SELECT *
	FROM creds.Membership
	WHERE MembershipID = @MembershipID
		OR CredentialEntityID = @CredentialEntityID
	
	-- If no records where discovered then void the primary keys.
	IF @@ROWCOUNT = 0
	BEGIN
		SET @MembershipID = NULL;
		SET @CredentialEntityID = NULL;
	END
	
	
	-- Debug
	--SELECT
	--	@MembershipID AS MembershipID,
	--	@CredentialEntityID AS CredentialEntityID,
	--	@EmailAddress AS EmailAddress,
	--	@PasswordHash AS PasswordHash,
	--	@PasswordSalt AS PasswordSalt,
	--	@PasswordQuestionID AS PasswordQuestionID,
	--	@PasswordAnswerHash AS PasswordAnswerHash,
	--	@PasswordAnswerSalt AS PasswordAnswerSalt,
	--	@IsApproved AS IsApproved,
	--	@IsLockedOut AS IsLockedOut,
	--	@DateLastLoggedIn AS DateLastLoggedIn,
	--	@DateLastPasswordChanged AS DateLastPasswordChanged,
	--	@DatePasswordExpires AS DatePasswordExpires,
	--	@DateLastLockedOut AS DateLastLockedOut,
	--	@DateLockOutExpires AS DateLockOutExpires,
	--	@FailedPasswordAttemptCount AS FailedPasswordAttemptCount,
	--	@FailedPasswordAnswerAttemptCount AS FailedPasswordAnswerAttemptCount,
	--	@IsDisabled AS IsDisabled,
	--	@Comment AS Comment
	
	
	
	
	
		
END
