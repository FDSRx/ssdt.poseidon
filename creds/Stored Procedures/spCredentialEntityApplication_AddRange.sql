﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/5/2015
-- Description:	Creates a new set of spCredentialEntityApplication_AddRange object(s).
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityApplicationIDList VARCHAR(MAX) = NULL
	
EXEC creds.spCredentialEntityApplication_AddRange
	@CredentialEntityID = 801050,
	@ApplicationID = 10,
	@CredentialEntityApplicationIDList = @CredentialEntityApplicationIDList OUTPUT

SELECT @CredentialEntityApplicationIDList AS CredentialEntityApplicationIDList

*/

-- SELECT * FROM acl.Roles
-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM acl.CredentialEntityApplication
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntityApplication_AddRange]
	@CredentialEntityID BIGINT,
	@ApplicationID VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CredentialEntityApplicationIDList VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ErrorMessage VARCHAR(4000),
		@ErrorSeverity INT = NULL,
		@ErrorState INT = NULL,
		@ErrorNumber INT = NULL,
		@ErrorLine INT = NULL,
		@ErrorProcedure  NVARCHAR(200) = NULL
	
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CredentialEntityApplicationIDList = NULL;
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
		

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @CredentialEntityID
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @ApplicationID
	IF dbo.fnIsNullOrWhiteSpace(@ApplicationID) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	


	BEGIN TRY
	--------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------	
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		-------------------------------------------------------------------------------
		-- Temporary objects.
		-------------------------------------------------------------------------------
		DECLARE @tblOutput AS TABLE (
			Idx INT IDENTITY(1,1),
			CredentialEntityApplicationID BIGINT
		);
		
				
		-------------------------------------------------------------------------------
		-- Create new object(s).
		-------------------------------------------------------------------------------
		INSERT INTO creds.CredentialEntityApplication (			
			CredentialEntityID,
			ApplicationID,
			CreatedBy
		)
		OUTPUT inserted.CredentialEntityApplicationID INTO @tblOutput(CredentialEntityApplicationID)
		SELECT
			@CredentialEntityID AS CredentialEntityID,
			Value AS ApplicationID,
			@CreatedBy
		FROM dbo.fnSplit(@ApplicationID, ',')
		WHERE ISNUMERIC(Value) = 1
		
		-------------------------------------------------------------------------------
		-- Retrieve record identity value(s).
		-------------------------------------------------------------------------------
		SET @CredentialEntityApplicationIDList = (
			SELECT 
				CONVERT(VARCHAR(25), CredentialEntityApplicationID) + ','
			FROM @tblOutput
			FOR XML PATH('')
		);
		
		IF RIGHT(@CredentialEntityApplicationIDList, 1) = ','
		BEGIN
			SET @CredentialEntityApplicationIDList = LEFT(@CredentialEntityApplicationIDList, LEN(@CredentialEntityApplicationIDList) - 1);
		END

	--------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END		
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>'),
			@ErrorMessage = ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);	
	
	END CATCH
	
		
END
