﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/9/2014
-- Description:	Creates a new Credential Event Log object.
-- SAMPLE CALL:
/*
	DECLARE 
		@EventLogID BIGINT
	
	EXEC creds.spCredentialEventLog_Create
		@EventID = 1,
		@EventSource = 'spCredentialEventLog_Create',
		@EventDataString = 'CredentialEntityID: 80108',
		@EventLogID = @EventLogID OUTPUT,
		@CreatedBy = 'dhughes'
	
	SELECT @EventLogID AS EventLogID
*/

-- SELECT * FROM dbo.EventLog
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEventLog_Create]
	@EventID INT,
	@EventSource VARCHAR(256) = NULL,
	@EventDataXml XML = NULL,
	@EventDataString VARCHAR(256) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@EventLogID BIGINT = NULL OUTPUT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Instance variables
	----------------------------------------------------------------------
	DECLARE @Trancount INT = @@TRANCOUNT;
	
	--------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------
	SET @EventLogID = NULL;
	
	--------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------
	DECLARE
		@EventLogTypeID INT = dbo.fnGetEventLogTypeID('CRED'),
		@ErrorMessage VARCHAR(4000)
	
	--------------------------------------------------------------------------------
	-- Create new Credential Event Log Object.
	--------------------------------------------------------------------------------
	BEGIN TRY
	--IF @Trancount = 0
	--BEGIN
	--	BEGIN TRANSACTION;
	--END	
	--------------------------------------------------------------------------------
	-- ** Begin transactional content
	--------------------------------------------------------------------------------
		
		------------------------------------------------------------------------------
		-- Create a new EventLog object.
		-- <Summary>
		-- Creates a super-type EventLog object that can be further defined
		-- by a sub-type EventLog object (where applicable).
		-- </Summary>
		------------------------------------------------------------------------------
		EXEC dbo.spEventLog_Create
			@EventLogTypeID = @EventLogTypeID,
			@EventID = @EventID,
			@EventSource = @EventSource,
			@EventDataXml = @EventDataXml,
			@EventDataString = @EventDataString,
			@CreatedBy = @CreatedBy,
			@EventLogID = @EventLogID OUTPUT
		
	
	--------------------------------------------------------------------------------
	-- ** End transactional content
	--------------------------------------------------------------------------------	
	--IF @Trancount = 0
	--BEGIN
	--	COMMIT TRANSACTION;
	--END
	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		--IF @Trancount = 0 AND @@TRANCOUNT > 0
		--BEGIN
		--	ROLLBACK TRANSACTION;
		--END
		
		-- re-throw error
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		
	END CATCH	
	
END
