﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Update person password
-- SAMPLE CALL:
/*

DECLARE
	@CredentialEntityID BIGINT = '801018',
	@Password VARCHAR(256) = 'password',
	@DatePasswordExpires DATETIME = NULL,
	@ModifiedBy VARCHAR(256) = 'dhughes', 
	@Debug BIT = 1,
	@IsValid BIT

EXEC creds.spMembership_Password_Compare 
	@CredentialEntityID = 801018, 
	@Password = 'password', 
	@IsValid = @IsValid OUTPUT
	
SELECT @IsValid AS ValidPassword

EXEC creds.spMembership_Password_Update 
	@CredentialEntityID = @CredentialEntityID, 
	@Password = @Password,
	@DatePasswordExpires = @DatePasswordExpires,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/

-- SELECT * FROM creds.Membership
-- SELECT * FROM dbo.EventLog
-- SELECT * FROM creds.CredentialEvent

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [creds].[spMembership_Password_Update]
	@CredentialEntityID BIGINT,
	@Password VARCHAR(256),
	@DatePasswordExpires DATETIME = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	-- Incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@Password=' + dbo.fnToStringOrEmpty(@Password) + ';' +
		'@DatePasswordExpires=' + dbo.fnToStringOrEmpty(@DatePasswordExpires) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' ;


	---------------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	---------------------------------------------------------------------------------------------------------------------------------
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/	

		---------------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------------------------------------------
	SET @ModifiedBy = COALESCE(@ModifiedBy, SUSER_NAME());
	SET @Debug = ISNULL(@Debug, 0);	

	---------------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@Hash VARBINARY(128),
		@Salt VARBINARY(10),
		@EventID INT

	---------------------------------------------------------------------------------------------------------------------------------
	-- Salt and Hash clear text
	---------------------------------------------------------------------------------------------------------------------------------
	SET @Salt = dbo.fnPasswordSalt(RAND());
	SET @Hash = dbo.fnPasswordHash(@Password + CONVERT(VARCHAR(MAX), @Salt));

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@CredentialEntityID AS CredentialEntityID, @Password AS Password, @Salt AS Salt, @Hash AS HashKey,
			@DatePasswordExpires AS DatePasswordExpires, @Args AS Arguments
	END	
	
	---------------------------------------------------------------------------------------------------------------------------------
	-- Update user password
	---------------------------------------------------------------------------------------------------------------------------------
	UPDATE pwd
	SET
		pwd.PasswordHash = @Hash,
		pwd.PasswordSalt = @Salt,
		pwd.DateLastPasswordChanged = GETDATE(),
		pwd.DatePasswordExpires = @DatePasswordExpires,
		pwd.DateModified = GETDATE(),
		pwd.ModifiedBy = @ModifiedBy
	FROM creds.Membership pwd
	WHERE CredentialEntityID = @CredentialEntityID
	

	---------------------------------------------------------------------------------------------------------------------------------
	-- Determine if a row was found to update;
	-- fail if we could not find person.
	---------------------------------------------------------------------------------------------------------------------------------
	IF @@ROWCOUNT > 0
	BEGIN
		---------------------------------------------------------------------------------------------------------------------------------
		-- Record event.  Do not fail the process if the event was
		-- unable to be recorded.
		---------------------------------------------------------------------------------------------------------------------------------
		BEGIN TRY
		
			SET @EventID = creds.fnGetCredentialEventID('CP');
			
			DECLARE @EventData XML = 
				'<CredentialEventData>' +
					dbo.fnXmlWriteElementString('CredentialEntityID', @CredentialEntityID) +
				'</CredentialEventData>';
			
			EXEC creds.spCredentialEventLog_Create
				@EventID = @EventID,
				@EventSource = @ProcedureName,
				@EventDataXml = @EventData,
				@CreatedBy = @ModifiedBy
		
		END TRY
		BEGIN CATCH		
		END CATCH
	
	END
	ELSE
	BEGIN
	
		SET @ErrorMessage = 'Unable to update membership password.  The CredentialEntityID, ' + CONVERT(VARCHAR, @CredentialEntityID) + 
			', does not have membership record.';
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
	
	END
	

	
END
