﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Get a credentials encrypted password
-- SAMPLE CALL:
/* 
DECLARE 
	@CredentialEntityID BIGINT = 2,
	@PasswordQuestionID INT,
	@PasswordAnswerHash VARBINARY(128), 
	@PasswordAnswerSalt VARBINARY(10)
	
EXEC creds.spMembership_Password_QuestionAnswer_Get 
	@CredentialEntityID = @CredentialEntityID, 
	@PasswordQuestionID = @PasswordQuestionID OUTPUT,
	@PasswordAnswerHash = @PasswordAnswerHash OUTPUT, 
	@PasswordAnswerSalt = @PasswordAnswerSalt OUTPUT
	
SELECT @PasswordQuestionID AS PasswordQuestionID, @PasswordAnswerHash AS AnswerHash, @PasswordAnswerSalt AS AnswerSalt
*/
-- =============================================
CREATE PROCEDURE [creds].[spMembership_Password_QuestionAnswer_Get]
	@CredentialEntityID BIGINT,
	@PasswordQuestionID INT = NULL OUTPUT,
	@PasswordAnswerHash VARBINARY(128) = NULL OUTPUT,
	@PasswordAnswerSalt VARBINARY(10) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------
	SET @PasswordAnswerHash = NULL;
	SET @PasswordAnswerSalt = NULL;
	SET @PasswordQuestionID = NULL;


	------------------------------------------------------------
	-- Fetch password data
	------------------------------------------------------------	
	SELECT
		@PasswordQuestionID = PasswordQuestionID,
		@PasswordAnswerHash = PasswordAnswerHash,
		@PasswordAnswerSalt = PasswordAnswerSalt
	--SELECT *
	FROM creds.Membership
	WHERE CredentialEntityID = @CredentialEntityID
		
END
