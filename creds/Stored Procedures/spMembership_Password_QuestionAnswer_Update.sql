﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Update person password
-- SAMPLE CALL:  
/*
 -- valid call
EXEC creds.spMembership_Password_QuestionAnswer_Update 
	@CredentialEntityID = 2, 
	@PasswordAnswer = 'test1'
	
-- SAMPLE CALL: 
-- invalid call (unknown membership)
EXEC creds.spMembership_Password_QuestionAnswer_Update 
	@CredentialEntityID = -100, 
	@PasswordAnswer = 'test1' 

DECLARE @IsValid BIT
EXEC creds.spMembership_Password_Compare 2, 'test1', @IsValid OUTPUT
SELECT @IsValid AS ValidPassword
*/
-- SELECT * FROM creds.Membership
-- =============================================
CREATE PROCEDURE [creds].[spMembership_Password_QuestionAnswer_Update]
	@CredentialEntityID BIGINT,
	@PasswordQuestionID INT = NULL,
	@PasswordAnswer VARCHAR(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------
	-- Local variables
	---------------------------------------------------
	DECLARE
		@Hash VARBINARY(128),
		@Salt VARBINARY(10),
		@HasQuestion BIT = 0,
		@ErrorMessage VARCHAR(4000)

	---------------------------------------------------------
	-- Salt and Hash clear text
	---------------------------------------------------------
	SET @Salt = dbo.fnPasswordSalt(RAND());
	SET @Hash = dbo.fnPasswordHash(@PasswordAnswer + CONVERT(VARCHAR(MAX), @Salt));
	
	
	---------------------------------------------------------
	-- Update user password
	---------------------------------------------------------
	UPDATE pwd
	SET
		pwd.PasswordQuestionID = CASE WHEN @PasswordQuestionID IS NOT NULL THEN @PasswordQuestionID ELSE PasswordQuestionID END,
		pwd.PasswordAnswerHash = CASE WHEN PasswordQuestionID IS NOT NULL THEN @Hash ELSE PasswordAnswerHash END,
		pwd.PasswordAnswerSalt = CASE WHEN PasswordQuestionID IS NOT NULL THEN @Salt ELSE PasswordAnswerSalt END,
		@HasQuestion = CASE WHEN PasswordQuestionID IS NULL THEN 0 ELSE 1 END
	--SELECT *
	FROM creds.Membership pwd
	WHERE CredentialEntityID = @CredentialEntityID
	

	---------------------------------------------------------
	-- Determine if a row was found to update
	-- fail if we could not find person
	---------------------------------------------------------
	IF @@ROWCOUNT = 0
	BEGIN
	
		SET @ErrorMessage = 'Unable to update the membership''s password security question answer.  The CredentialEntityID, ' + CONVERT(VARCHAR, @CredentialEntityID) + 
			', does not have a membership record.';
		
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;	
	END

	---------------------------------------------------------
	-- Determine if the membership record had a password
	-- security question
	---------------------------------------------------------
	IF @@ROWCOUNT = 0
	BEGIN
	
		SET @ErrorMessage = 'Unable to update the membership''s password security question answer.  The CredentialEntityID, ' + CONVERT(VARCHAR, @CredentialEntityID) + 
			', does not have a password security question assigned to the membership record.';
		
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );	
		
		RETURN;
	END

	
END
