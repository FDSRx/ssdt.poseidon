﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/29/31
-- Description:	Creates a new credential entity
-- SAMPLE CALL: 
/*
DECLARE 
	@CredentialEntityTypeID INT = 1, 
	@ApplicationID INT = 1, 
	@BusinessEntityID INT = -1, 
	@CredentialEntityID BIGINT
	
EXEC creds.spCredentialEntity_Create
	@CredentialEntityTypeID = @CredentialEntityTypeID, 
	@BusinessEntityID = @BusinessEntityID, 
	@CredentialEntityID = @CredentialEntityID OUTPUT
	
SELECT @CredentialEntityID
*/
-- SELECT * FROM creds.CredentialEntity
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntity_Create]
	@CredentialEntityTypeID INT,
	@BusinessEntityID INT = NULL, -- Store, Chain, etc.
	@PersonID BIGINT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CredentialEntityID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------
	-- Sanitize input
	-- <Summary>
	-- Cleans or resets arguments
	-- </Summary>
	---------------------------------------------------------------
	SET @CredentialEntityID = NULL;
	
	-----------------------------------------------------------------------
	-- Create a new Credential BusinessEntityID
	-- <Summary>
	-- Creates a new CredentialEntityID from the business entity create process
	-- </Summary>
	-----------------------------------------------------------------------
	DECLARE @BusinessEntityTypeID INT = dbo.fnGetBusinessEntityTypeID('CRED');
	
	SET @CredentialEntityID = @PersonID;
	
	IF @CredentialEntityID IS NULL
	BEGIN
	
		EXEC dbo.spBusinessEntity_Create
			@BusinessEntityTypeID = @BusinessEntityTypeID,
			@BusinessEntityID = @CredentialEntityID OUTPUT
	
	END
	
	---------------------------------------------------------------
	-- Create credential entity
	-- <Summary>
	-- Creates a new unique credential entity to be used for a 
	-- specified credential object.
	-- </Summary>
	---------------------------------------------------------------
	INSERT INTO creds.CredentialEntity (
		CredentialEntityID,
		CredentialEntityTypeID,
		BusinessEntityID,
		PersonID,
		CreatedBy
	)
	SELECT 
		@CredentialEntityID AS CredentialEntity,
		@CredentialEntityTypeID AS CredentialEntityTypeID,
		@BusinessEntityID AS BusinessEntityID,
		@PersonID AS PersonID,
		@CreatedBy
	
	
END
