﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/8/2014
-- Description:	Indicates if the provided membership identifier is locked.
-- SAMPLE CALL:
/*

DECLARE 
	@IsLocked BIT = NULL
	
EXEC creds.spMembership_IsLockedOut
	@CredentialEntityID = 801018,
	@IsLockedOut = @IsLocked OUTPUT

SELECT @IsLocked AS IsLocked

*/
-- SELECT * FROM creds.Membership
-- SELECT * FROM creds.vwExtranetUser WHERE BusinessEntityID = 10684
-- =============================================
CREATE PROCEDURE creds.spMembership_IsLockedOut
	@MembershipID BIGINT = NULL,
	@CredentialEntityID BIGINT = NULL,
	@BusinessID BIGINT = NULL,
	@Username VARCHAR(256) = NULL,
	@CredentialEntityTypeID INT = NULL,
	@RequestedBy VARCHAR(256) = NULL,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@MembershipID=' + dbo.fnToStringOrEmpty(@MembershipID) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@CredentialEntityTypeID=' + dbo.fnToStringOrEmpty(@CredentialEntityTypeID) + ';'

	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	----------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------
	SET @IsLockedOut = 0;
	SET @DateLockOutExpires = NULL;
	
	----------------------------------------------------------------------------------
	-- Declare variables.
	----------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@CredentialExists BIT = 0
	
	/*
	----------------------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules.
	-- </Summary>
	----------------------------------------------------------------------------------
	-- A tag name is required to be created.
	IF @MembershipID IS NULL AND @CredentialEntityID IS NULL AND ( @BusinessID IS NULL OR @Username IS NULL OR @CredentialEntityID IS NULL )
	BEGIN
	
		SET @ErrorMessage = 'Unable to retrieve Membership entry.  Object references ' +
			'("@MembershipID" or "@CredentialEntityID" or "@BusinessID and @Username and @CredentialEntityID") are not set to an instance of an object. ' +
			'The "@MembershipID" or "@CredentialEntityID" or "@BusinessID and @Username and @CredentialEntityID" cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	*/

	----------------------------------------------------------------------------------
	-- Interrogate provided identifiers and determine the proper channel of
	-- identification.
	-- <Summary>
	-- Inspects credential indentification keys and determines how to access the
	-- membership record.
	-- </Summary>
	----------------------------------------------------------------------------------
	IF @MembershipID IS NULL AND @CredentialEntityID IS NULL
	BEGIN
		----------------------------------------------------------------------------------
		-- Fetch credential entity
		-- <Summary>
		-- Interrogates the credentials for the provided type
		-- to determine if the supplied credentials meets the required
		-- criteria to be authenticated.
		-- </Summary>
		----------------------------------------------------------------------------------
		EXEC creds.spCredentialEntity_Derived_Exists
			@CredentialEntityTypeID = @CredentialEntityTypeID,
			@BusinessEntityID = @BusinessID,
			@DerivedCredentialKey = @Username,
			@CredentialEntityID = @CredentialEntityID OUTPUT,
			@Exists = @CredentialExists OUTPUT
	END
	
	----------------------------------------------------------------------------------
	-- Interrogate unique keys and determine winner.
	-- <Summary>
	-- If the primary record key is supplied (i.e. the surrogate key)
	-- then declare that key the winner and void the foreign key; 
	-- otherwise, keep the foreign unique key.
	-- </Summary>
	----------------------------------------------------------------------------------
	IF @MembershipID IS NOT NULL
	BEGIN
		SET @CredentialEntityID = NULL;
	END
	
	-- Debug
	--SELECT @MembershipID AS MembershipID, @CredentialEntityID AS CredentialEntityID
	
	----------------------------------------------------------------------------------
	-- Determine locked status.
	----------------------------------------------------------------------------------	
	IF @MembershipID IS NOT NULL OR @CredentialEntityID IS NOT NULL
	BEGIN
		
		-- Determines if the record can be unlocked. If so, the record is unlocked;
		-- otherwise, it remains locked.
		EXEC creds.spMembership_Unlock
			@MembershipID = @MembershipID,
			@CredentialEntityID = @CredentialEntityID,
			@ModifiedBy = @RequestedBy
		
		-- Determines if hte record can be locked. If so, the record is locked;
		-- otherwise, it remains unlocked.
		EXEC creds.spMembership_LockOut
			@MembershipID = @MembershipID,
			@CredentialEntityID = @CredentialEntityID,
			@ModifiedBy = @RequestedBy,
			@IsLockedOut = @IsLockedOut OUTPUT
		
		/*
		-- Returns the locked status of the membership.	
		EXEC creds.spMembership_Get
			@MembershipID = @MembershipID,
			@CredentialEntityID = @CredentialEntityID,
			@IsLockedOut = @IsLockedOut OUTPUT
		*/
		
	END
	

END
