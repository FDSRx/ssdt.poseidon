﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/27/2014
-- Description:	Interrogates credential data for the supplied type and determines if credentials
--				are valid.
-- Change Log:
-- 3/27/2015 - DRH - Change password required and first login workaround.  If the password is invalid, then
--					do no alert the user that a change password is required or that it is their first login. 
--					Technically, they were not authenticated to be privy to that information.	
-- 9/25/2015 - dhughes - Added the ability to inspect the application (when provided) and determine if the
--					specified user criteria belongs to the application.  If the application is not found as
--					part of the credentials authorized applications then the user is considered invalid.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityID BIGINT,
	@CredentialToken VARCHAR(50),
	@IsValid BIT,
	@IsApproved BIT = NULL,
	@IsLockedOut BIT = NULL,
	@DateLastLoggedIn DATETIME = NULL,
	@DateLastPasswordChanged DATETIME = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@DateLastLockedOut DATETIME = NULL,
	@DateLockOutExpires DATETIME = NULL,
	@FailedPasswordAttemptCount INT = NULL,
	@FailedPasswordAnswerAttemptCount INT = NULL,
	@IsDisabled BIT = NULL,
	@IsChangePasswordRequired BIT = NULL,
	@IsFirstLogin BIT = NULL

EXEC creds.spCredentialEntity_Credentials_Authenticate
	@ApplicationID = 1,
	@BusinessEntityID = 92,
	@Username = 'dhughes',
	@Password = 'passwords',
	@CredentialEntityTypeID = 1,
	@IgnoreTokenCreation = 1,
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@CredentialToken = @CredentialToken OUTPUT,
	@IsValid = @IsValid OUTPUT,
	@IsApproved = @IsApproved OUTPUT,
	@IsLockedOut = @IsLockedOut OUTPUT,
	@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
	@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
	@DatePasswordExpires = @DatePasswordExpires OUTPUT,
	@DateLastLockedOut = @DateLastLockedOut OUTPUT,
	@DateLockOutExpires = @DateLockOutExpires OUTPUT,
	@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
	@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
	@IsDisabled = @IsDisabled OUTPUT,
	@IsChangePasswordRequired = @IsChangePasswordRequired OUTPUT,
	@IsFirstLogin = @IsFirstLogin OUTPUT,
	@ModifiedBy = 'dhughes'
	
SELECT @CredentialEntityID AS CredentialEntityID, @CredentialToken AS CredentialToken,
	@IsValid AS IsValid, @IsApproved AS IsApproved, @IsLockedOut AS IsLockedOut, @DateLastLoggedIn AS DateLastLoggedIn,
	@DateLastPasswordChanged AS DateLastPasswordChanged, @DatePasswordExpires AS DatePasswordExpires, @DateLastLockedOut AS DateLastLockedOut,
	@DateLockOutExpires AS DateLockOutExpires, @FailedPasswordAttemptCount AS FailedPasswordAttemptCount, 
	@FailedPasswordAnswerAttemptCount AS FailedPasswordAnswerAttemptCount, @IsDisabled AS IsDisabled,
	@IsChangePasswordRequired AS IsChangePasswordRequired, @IsFirstLogin AS IsFirstLogin

EXEC creds.spMembership_Unlock
	@CredentialEntityID = 801018,
	@ForceUnlock = 1		
*/

-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
-- SELECT * FROM creds.CredentialEntityType 
-- SELECT * FROM creds.Membership
-- SELECT * FROM dbo.EventLog
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntity_Credentials_Authenticate]
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT,
	@Username VARCHAR(256),
	@Password VARCHAR(50),
	@CredentialEntityTypeID VARCHAR(25) = NULL,
	@IgnoreTokenCreation BIT = NULL,
	@IgnoreLockedOutValidation BIT = NULL,
	@IgnoreDisabledValidation BIT = NULL,
	@IgnoreChangePasswordValidation BIT = NULL,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@CredentialToken VARCHAR(50) = NULL OUTPUT,
	@IsValid BIT = 0 OUTPUT,
	-- Core membership properties ****************************
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT,
	-- *******************************************************
	@IsChangePasswordRequired BIT = NULL OUTPUT,
	@IsFirstLogin BIT = NULL OUTPUT,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------------------------------
	SET @CredentialEntityID = NULL;
	SET @CredentialToken = NULL;
	SET @IgnoreTokenCreation = ISNULL(@IgnoreTokenCreation, 0);
	SET @IgnoreLockedOutValidation = ISNULL(@IgnoreLockedOutValidation, 0);
	SET @IgnoreDisabledValidation = ISNULL(@IgnoreDisabledValidation, 0);
	SET @IgnoreChangePasswordValidation = ISNULL(@IgnoreChangePasswordValidation, 0);
	SET @IsValid = 0;
	SET @IsApproved = 0;
	SET	@IsLockedOut = 0;
	SET	@DateLastLoggedIn = NULL;
	SET	@DateLastPasswordChanged = NULL;
	SET @DatePasswordExpires = NULL;
	SET	@DateLastLockedOut = NULL;
	SET @DateLockOutExpires = NULL;
	SET	@FailedPasswordAttemptCount = 0;
	SET	@FailedPasswordAnswerAttemptCount = 0;
	SET @IsDisabled = 0;
	SET @IsChangePasswordRequired = 0;
	SET	@IsFirstLogin = 0;
		
	
	-----------------------------------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------------------------------
	DECLARE
		@Exists BIT = 0,
		@TranDate DATETIME = GETDATE(),
		@Source VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-----------------------------------------------------------------------------------------------
	-- Fetch credential entity
	-- <Summary>
	-- Interrogates the credentials for the provided type
	-- to determine if the supplied credentials meets the required
	-- criteria to be authenticated.
	-- </Summary>
	-----------------------------------------------------------------------------------------------
	EXEC creds.spCredentialEntity_Derived_Exists
		@CredentialEntityTypeID = @CredentialEntityTypeID,
		@BusinessEntityID = @BusinessEntityID,
		@DerivedCredentialKey = @Username,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@Exists = @Exists OUTPUT
	
	
	-----------------------------------------------------------------------------------------------
	-- Inspect results
	-- <Summary>
	-- If we do not have a CredentialEntityID then we need to flag
	-- as invalid and exit the procedure.  Otherwise, we need to
	-- validate password.
	-- </Summary>
	-----------------------------------------------------------------------------------------------
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @IsValid = 0;
		
		RETURN; -- Exit procedure.
	END

	-----------------------------------------------------------------------------------------------
	-- Determine if the CredentialEntity object has access to the provided application (if applicable).
	-- <Summary>
	-- If the caller specified an application then let's determine if the credential has
	-- access to said application.  If the user does not have access then consider the
	-- credentials invalid.
	-- </Summary>
	-- <Remarks>
	-- This technique was added to combat logging into applications even though the user was
	-- no properly setup for an application.  It should also remain backwards compatible to
	-- any existing logins.
	-- </Remarks>
	-----------------------------------------------------------------------------------------------
	IF @CredentialEntityID IS NOT NULL AND @ApplicationID IS NOT NULL
	BEGIN
		DECLARE 
			@CredentialEntityApplicationID BIGINT = NULL

		SELECT
			@CredentialEntityApplicationID = CredentialEntityApplicationID
		--SELECT *
		FROM creds.CredentialEntityApplication
		WHERE CredentialEntityID = @CredentialEntityID
			AND ApplicationID = @ApplicationID

		IF @CredentialEntityApplicationID IS NULL
		BEGIN
			SET @IsValid = 0;
		
			RETURN; -- Exit procedure.
		END			
	END
	
	-----------------------------------------------------------------------------------------------
	-- Unlock previously locked account (if applicable).
	-----------------------------------------------------------------------------------------------
	EXEC creds.spMembership_Unlock
		@CredentialEntityID = @CredentialEntityID,
		@ModifiedBy = @ModifiedBy
	
	-----------------------------------------------------------------------------------------------
	-- Validate password
	-- <Summary>
	-- Determine if supplied password matches the password in the
	-- membership repository.
	-- </Summary>
	-----------------------------------------------------------------------------------------------
	EXEC creds.spMembership_Password_Compare
		@CredentialEntityID = @CredentialEntityID,
		@Password = @Password,
		@IsValid = @IsValid OUTPUT,
		-- Core membership properties ****************************
		@IsApproved = @IsApproved OUTPUT,
		@IsLockedOut = @IsLockedOut OUTPUT,
		@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
		@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
		@DatePasswordExpires = @DatePasswordExpires OUTPUT,
		@DateLastLockedOut = @DateLastLockedOut OUTPUT,
		@DateLockOutExpires = @DateLockOutExpires OUTPUT,		
		@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
		@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
		@IsDisabled = @IsDisabled OUTPUT
		-- *******************************************************
	
	-- update validaiton variable
	SET @IsValid = ISNULL(@IsValid, 0);
	
	-----------------------------------------------------------------------------------------------
	-- If the provided credentials are invalid, then track
	-- the bad request.
	-----------------------------------------------------------------------------------------------
	IF @IsValid = 0
	BEGIN
	
		SET @FailedPasswordAttemptCount = ISNULL(@FailedPasswordAttemptCount, 0) + 1;
		
		EXEC creds.spMembership_Update
			@CredentialEntityID = @CredentialEntityID,
			@FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
			@ModifiedBy = @ModifiedBy
	END
	
	-----------------------------------------------------------------------------------------------
	-- Set lock (if applicable)
	-----------------------------------------------------------------------------------------------
	EXEC creds.spMembership_LockOut
		@CredentialEntityID = @CredentialEntityID,
		@ApplicationID = @ApplicationID,
		@ModifiedBy = @ModifiedBy,
		@IsLockedOut = @IsLockedOut OUTPUT
		

	-----------------------------------------------------------------------------------------------
	-- Analyze credential's account
	-- <Summary>
	-- Inspect the credentials account to determine if there are outside factors that could cause
	-- the authentication to be invalid.
	-- </Summary>
	-----------------------------------------------------------------------------------------------
	-- Determine if the password is required to be changed.
	SET @IsChangePasswordRequired =
		CASE
			WHEN @DatePasswordExpires IS NOT NULL AND @DatePasswordExpires <= GETDATE() THEN 1
			ELSE 0
		END;
	
	-- Determine if this is the credential's first time logging into his/her account.
	SET @IsFirstLogin = 
		CASE
			WHEN @DateLastLoggedIn IS NULL THEN 1
			ELSE 0
		END;
	
	-- 3/27/2015 - DRH - Change password required and first login workaround.  If the password is invalid, then
	-- do no alert the user that a change password is required or that it is their first login. 
	-- Technically, they were not authenticated to be privy to that information.	
	SET @IsChangePasswordRequired = CASE WHEN @IsValid = 1 THEN @IsChangePasswordRequired ELSE 0 END;
	SET @IsFirstLogin = CASE WHEN @IsValid = 1 THEN @IsFirstLogin ELSE 0 END;
	
	-----------------------------------------------------------------------------------------------
	-- Determine if the account analysis causes the validatity of the request to be invalid.
	-----------------------------------------------------------------------------------------------	
	SET @IsValid = 
		CASE
			WHEN @IsLockedOut = 1 AND @IgnoreLockedOutValidation = 0 THEN 0
			WHEN @IsChangePasswordRequired = 1 AND @IgnoreChangePasswordValidation = 0 THEN 0
			WHEN @IsDisabled = 1 AND @IgnoreDisabledValidation = 0 THEN 0
			--WHEN @IsApproved = 0 THEN 0
			ELSE @IsValid
		END;
	
	-----------------------------------------------------------------------------------------------
	-- If a valid response was received from the membership
	-- query then we need to create a new credential security token.
	-- Otherwise, the credentials that were passed were invalid.
	-----------------------------------------------------------------------------------------------
	IF @IsValid = 1 AND @IgnoreTokenCreation = 0
	BEGIN
	
		DECLARE 
			@TokenTypeID INT = dbo.fnGetTokenTypeID('LGN')
			
		-- Create credential token.
		EXEC creds.spCredentialToken_Create
			@CredentialEntityID = @CredentialEntityID,
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessEntityID,
			@TokenTypeID = @TokenTypeID,
			@Token = @CredentialToken OUTPUT
				
		-- Void password attempts.
		EXEC creds.spMembership_Update
			@CredentialEntityID = @CredentialEntityID,
			@FailedPasswordAttemptCount = 0,
			@DateLastLoggedIn = @TranDate,
			@ModifiedBy = @ModifiedBy
		
		-----------------------------------------------------------------------------------------------
		-- Record event.  Do not fail the process if the event was
		-- unable to be recorded.
		-----------------------------------------------------------------------------------------------
		BEGIN TRY
		
			DECLARE @EventID INT = creds.fnGetCredentialEventID('AUTHNS');
			
			DECLARE @EventData XML = 
				'<CredentialEventData>' +
					dbo.fnXmlWriteElementString('CredentialEntityID', @CredentialEntityID) +
					dbo.fnXmlWriteElementString('ApplicationID', @ApplicationID) +
					dbo.fnXmlWriteElementString('BusinessID', @BusinessEntityID) +
					dbo.fnXmlWriteElementString('Username', @Username) +
					dbo.fnXmlWriteElementString('CredentialEntityTypeID', @CredentialEntityTypeID) +
					dbo.fnXmlWriteElementString('CredentialToken', @CredentialToken) +
				'</CredentialEventData>';
			
			EXEC creds.spCredentialEventLog_Create
				@EventID = @EventID,
				@EventSource = @Source,
				@EventDataXml = @EventData,
				@CreatedBy = @ModifiedBy
		
		END TRY
		BEGIN CATCH		
		END CATCH	
			
	END
	
	
END
