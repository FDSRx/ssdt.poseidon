﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/11/2014
-- Description:	Creates a new CredentialEntityBusinessEntity object.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityBusinessEntityID BIGINT = NULL
	
EXEC creds.spCredentialEntityBusinessEntity_Create
	@CredentialEntityID = 959790,
	@ApplicationID = 8,
	@BusinessEntityID = 3759,
	@CredentialEntityBusinessEntityID = @CredentialEntityBusinessEntityID OUTPUT,
	@CreatedBy = 'dhughes'

SELECT @CredentialEntityBusinessEntityID AS CredentialEntityBusinessEntityID

*/

-- SELECT * FROM creds.CredentialEntityBusinessEntity
-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- =============================================
CREATE PROCEDURE creds.[spCredentialEntityBusinessEntity_Create]
	@CredentialEntityID BIGINT,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CredentialEntityBusinessEntityID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CredentialEntityBusinessEntityID = NULL;
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- CredentialEntityID
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntity association. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntity association. Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
		
	-------------------------------------------------------------------------------
	-- Create new CredentialEntityBusinessEntity object.
	-- <Summary>
	-- Attaches a BusinessEntity object to a CredentialEntity object.
	-- </Summary>
	-------------------------------------------------------------------------------
	INSERT INTO creds.CredentialEntityBusinessEntity (
		CredentialEntityID,
		ApplicationID,
		BusinessEntityID,
		CreatedBy
	)
	SELECT
		@CredentialEntityID AS CredentialEntityID,
		@ApplicationID AS ApplicationID,
		@BusinessEntityID AS BusinessEntityID,		
		@CreatedBy AS CreatedBy
	
	-- Retrieve record identity value.
	SET @CredentialEntityBusinessEntityID = SCOPE_IDENTITY();
		
END
