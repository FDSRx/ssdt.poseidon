﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/31/2013
-- Description:	Creates a credential token that is used as a pointer to a credential entity
-- SAMPLE CALL:
/*
DECLARE
	@CredentialEntityID BIGINT = 801050, 
	@CredentialTokenID BIGINT, @Token VARCHAR(50), @ErrorLogID INT
	
EXEC creds.spCredentialToken_Create
	@CredentialEntityID = @CredentialEntityID,
	@TokenData = NULL,
	@TokenDurationMinutes = NULL,
	@CredentialTokenID = @CredentialTokenID OUTPUT,
	@Token = @Token OUTPUT,
	@ErrorLogID = @ErrorLogID OUTPUT
	
SELECT @CredentialTokenID AS CredentialTokenID, @Token AS Token, @ErrorLogID AS ErrorLogID
*/

-- SELECT * FROM creds.CredentialEntity
-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
-- SELECT * FROM dbo.TokenType
-- SELECT * FROM creds.ExtranetUser
-- SELECT * FROM creds.Membership
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- =============================================
CREATE PROCEDURE [creds].[spCredentialToken_Create]
	@CredentialEntityID BIGINT,
	@ApplicationID INT = NULL,
	@BusinessEntityID INT = NULL,
	@TokenTypeID INT = NULL,
	@TokenData XML = NULL,
	@DateExpires DATETIME = NULL,
	@TokenDurationMinutes INT = NULL,
	@CredentialTokenID BIGINT = NULL OUTPUT,
	@Token VARCHAR(50) = NULL OUTPUT,
	@CreatedBy VARCHAR(256) = NULL,
	@ErrorLogID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------
	-- <Remarks>
	-- **
	-- A CredentialEntityID supplied by itself (i.e. no application
	-- or business) is technically enough information to create a token.
	-- The Credential is tied to a business and an applicaiton can be
	-- inferred if only one application exists for the user; otherwise,
	-- an application is required to be supplied.
	-- <Remarks>
	-- **
	----------------------------------------------------------------
	
	----------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------
	SET @CredentialTokenID = NULL;
	SET @Token = NULL;
	SET @ErrorLogID = NULL;

	----------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ApplicationData XML,
		@BusinessEntityID_Assigned INT
	

	----------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------
	SET @TokenTypeID = ISNULL(@TokenTypeID, dbo.fnGetTokenTypeID('OTR'));
	
	----------------------------------------------------------------
	-- Fetch credential entity base information
	-- <Summary>
	-- Retrieves the credential entities base information (i.e. application,
	-- business, type).
	-- </Summary>
	----------------------------------------------------------------	
	EXEC creds.spCredentialEntity_Get
		@CredentialEntityID = @CredentialEntityID,
		@ApplicationData = @ApplicationData OUTPUT,
		@BusinessEntityID = @BusinessEntityID_Assigned OUTPUT
	
	-- Set the preferred business.
	SET @BusinessEntityID = ISNULL(@BusinessEntityID, @BusinessEntityID_Assigned);
	
	----------------------------------------------------------------
	-- Infer application
	-- <Summary>
	-- If an ApplicaitonID was not supplied, then lets scan the user's
	-- application list and if only one application exists then let's
	-- set the existing application as the user's default. Otherwise,
	-- a preferred application needs to be supplied as we cannot infer
	-- an default applicaiton from multiple entries.
	-- </Summary>
	----------------------------------------------------------------
	DECLARE @x XML = @ApplicationData;
	
	IF @x IS NOT NULL
	BEGIN
	
		DECLARE @tblApplicationIDList AS TABLE (
			ApplicationID INT
		);
		
		INSERT INTO @tblApplicationIDList (
			ApplicationID
		)
		SELECT
			a.value('(ApplicationID)[1]','INT') AS ApplicationID
		FROM @x.nodes('Applications/Application') AS app(a)
		
		-- If we only have one row then lets set the user's default application to the return application.
		IF @@ROWCOUNT = 1
		BEGIN
		
			SET @ApplicationID = ISNULL(@ApplicationID, (SELECT TOP 1 ApplicationID FROM @tblApplicationIDList));
		END
		
	END
	
	----------------------------------------------------------------
	-- Create Token
	-- <Summary>
	-- Creates a security token that is used as a mapping device back
	-- to a credential entity
	-- </Summary>
	----------------------------------------------------------------
	BEGIN TRY
	
		------------------------------------------------------
		-- Token duration
		-- <Summary>
		-- Determines the amount of time the token is allowed
		-- to stay active
		-- </Summary>
		------------------------------------------------------
		IF @TokenDurationMinutes IS NULL
		BEGIN
			DECLARE @DurationKey VARCHAR(25) = 'CredTokenDurationMin';
			
			SET @TokenDurationMinutes = COALESCE(dbo.fnGetAppBusinessConfigValue(@ApplicationID, @BusinessEntityID, @DurationKey),
				dbo.fnGetAppConfigValue(@ApplicationID, @DurationKey),
				dbo.fnGetBusinessConfigValue(@BusinessEntityID, @DurationKey), 
				dbo.fnGetConfigValue(@DurationKey), 30);
					
			--Data Helper
			--SELECT * FROM dbo.Configuration		
		END
		
		-- Set expiration date
		SET @DateExpires = ISNULL(@DateExpires, DATEADD(mi, @TokenDurationMinutes, GETDATE())) ;
		
		------------------------------------------------------
		-- Create token
		-- <Summary>
		-- Creates a new credential token.
		-- </Summary>
		------------------------------------------------------
		DECLARE @tblOutput AS TABLE (
			CredentialTokenID BIGINT,
			Token UNIQUEIDENTIFIER
		);
		
		INSERT INTO creds.CredentialToken (
			CredentialEntityID,
			ApplicationID,
			BusinessEntityID,
			TokenTypeID,
			TokenData,
			DateExpires,
			CreatedBy
		)
		OUTPUT inserted.CredentialTokenID, inserted.Token INTO @tblOutput
		SELECT
			@CredentialEntityID,
			@ApplicationID,
			@BusinessEntityID,
			@TokenTypeID,
			@TokenData,
			@DateExpires,
			@CreatedBy
		
		SET @CredentialTokenID = SCOPE_IDENTITY();
		SET @Token = (SELECT TOP 1 Token FROM @tblOutput);
	
	END TRY
	BEGIN CATCH
	
		EXECUTE [dbo].[spLogError] @ErrorLogID OUTPUT;
		
		SET @ErrorMessage = ERROR_MESSAGE();
		
		-- Re-throw error
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
	END CATCH
	
	
	
	
END
