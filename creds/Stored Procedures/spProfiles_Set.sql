﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/8/2013
-- Description:	Create/Updates a user profile record
-- SAMPLE CALL:
/*
DECLARE
	@ProfileID BIGINT, 
	@PropertyValueString VARCHAR(MAX) = 'Larrys Demo', 
	@PropertyValueBinary VARBINARY(MAX)
	
EXEC creds.spProfiles_Set
	@ProfileID = @ProfileID OUTPUT,
	@CredentialEntityID = 801050,
	@PropertyName = 'PRFSTRE',
	@PropertyValueString = @PropertyValueString,
	@PropertyValueBinary = @PropertyValueBinary

SELECT @ProfileID AS ProfileID
*/
-- SELECT * FROM creds.Profiles
-- SELECT * FROM creds.vwExtranetUser WHERE Username = 'dhughes'
-- =============================================
CREATE PROCEDURE [creds].[spProfiles_Set]
	@ProfileID BIGINT = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL,
	@PropertyName VARCHAR(256) = NULL,
	@PropertyValueString VARCHAR(MAX) = NULL,
	@PropertyValueBinary VARBINARY(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	---------------------------------------------------------
	-- Fetch record
	-- <Summary>
	-- If the record exists then we need to perfrom an update;
	-- otherwise, we need to perform an insert
	-- </Summary>
	---------------------------------------------------------
	EXEC creds.spProfiles_Get
		@ProfileID = @ProfileID OUTPUT,
		@CredentialEntityID = @CredentialEntityID,
		@PropertyName = @PropertyName
	
	
	---------------------------------------------------------
	-- Dtermine if we need to update the record or create
	-- the record
	---------------------------------------------------------
	IF @ProfileID IS NOT NULL
	BEGIN
		EXEC creds.spProfiles_Update
			@ProfileID = @ProfileID,
			@PropertyValueString = @PropertyValueString,
			@PropertyValueBinary = @PropertyValueBinary,
			@ModifiedBy = @ModifiedBy
	END
	ELSE
	BEGIN
		EXEC creds.spProfiles_Create
			@CredentialEntityID = @CredentialEntityID,
			@PropertyName = @PropertyName,
			@PropertyValueString = @PropertyValueString,
			@PropertyValueBinary = @PropertyValueBinary,
			@CreatedBy = @CreatedBy,
			@ProfileID = @ProfileID OUTPUT
		
	END
		
	
	
END

