﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/5/2015
-- Description:	Deletes a CredentialEntityApplication object.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityApplicationID BIGINT = NULL
	
EXEC creds.spCredentialEntityApplication_Delete
	@CredentialEntityApplicationID = @CredentialEntityApplicationID,
	@CredentialEntityID = 959790,
	@ApplicationID = 8
	

SELECT @CredentialEntityApplicationID AS CredentialEntityApplicationID

*/

-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntityApplication
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntityApplication_Delete]
	@CredentialEntityApplicationID BIGINT = NULL,
	@CredentialEntityID BIGINT= NULL,
	@ApplicationID INT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID)
		
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------


	/*
	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- CredentialEntityID
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- ApplicationID
	IF @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/
	
	-------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy
		
	-------------------------------------------------------------------------------
	-- Delete CredentialEntityApplication object.
	-------------------------------------------------------------------------------
	DELETE ca
	FROM creds.CredentialEntityApplication ca
	WHERE ca.CredentialEntityApplicationID = @CredentialEntityApplicationID
		OR (
			ca.CredentialEntityID = @CredentialEntityID
			AND ca.ApplicationID = @ApplicationID
		)
		
		
END
