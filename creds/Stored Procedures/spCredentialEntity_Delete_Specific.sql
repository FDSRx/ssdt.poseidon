﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/10/2015
-- Description:	Deletes a credential entity.
-- SAMPLE CALL:
/*

EXEC creds.spCredentialEntity_Delete
	@CredentialEntityID = 961200
	
*/

-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntity
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntity_Delete_Specific]
	@CredentialEntityID BIGINT,
	@RetainCredential BIT = NULL,
	@ApplicationIDIsAnyOf VARCHAR(MAX) = NULL,
	@BusinessIDIsAnyOf VARCHAR(MAX) = NULL,
	@KeepCredentialEntityID BIT = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ErrorMessage VARCHAR(4000) = NULL


	-----------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------
	SET @RetainCredential = ISNULL(@RetainCredential, 0);
	SET @ApplicationIDIsAnyOf = NULLIF(@ApplicationIDIsAnyOf, '');
	SET @BusinessIDIsAnyOf = NULLIF(@BusinessIDIsAnyOf, '');
	SET @KeepCredentialEntityID = ISNULL(@KeepCredentialEntityID, 0);
	SET @Debug = ISNULL(@Debug, 0);

	-----------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------------------
	-- No declarations required.	


	-----------------------------------------------------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------
	IF @CredentialEntityID IS NULL 
	BEGIN
		SET @ErrorMessage = 'Unable to remove Credential/User from the system.  Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The @CredentialEntityID parameter cannot be null or empty.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END


	BEGIN TRY
	-----------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	-----------------------------------------------------------------------------------------------------------------
	SET @Trancount = @@TRANCOUNT;	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
		
		-----------------------------------------------------------------------------------------------------------------
		-- Remove Credential object data
		-- <Summary>
		-- Removes all applicable credential object components from the system (unless specified otherwise).
		-- </Summary>
		-----------------------------------------------------------------------------------------------------------------
		-- CredentialEntityRolePermission (Credential permissions)
		DELETE 
		--SELECT *
		FROM acl.CredentialEntityRolePermission 
		WHERE CredentialEntityID = @CredentialEntityID
			AND (@ApplicationIDIsAnyOf IS NULL OR (@ApplicationIDIsAnyOf IS NOT NULL AND 
				ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationIDIsAnyOf, ',') WHERE ISNUMERIC(Value) = 1 )))
			AND (@BusinessIDIsAnyOf IS NULL OR (@BusinessIDIsAnyOf IS NOT NULL AND 
				BusinessID IN (SELECT Value FROM dbo.fnSplit(@BusinessIDIsAnyOf, ',') WHERE ISNUMERIC(Value) = 1 )))

		-----------------------------------------------------------------------------------------------------------------
		-- Remove permission based objects.
		-----------------------------------------------------------------------------------------------------------------
		-- CredentialEntityGroup (Credential groups)
		DELETE 
		--SELECT *
		FROM acl.CredentialEntityGroup 
		WHERE CredentialEntityID = @CredentialEntityID
			AND (@ApplicationIDIsAnyOf IS NULL OR (@ApplicationIDIsAnyOf IS NOT NULL AND 
				ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationIDIsAnyOf, ',') WHERE ISNUMERIC(Value) = 1 )))
			AND (@BusinessIDIsAnyOf IS NULL OR (@BusinessIDIsAnyOf IS NOT NULL AND 
				BusinessEntityID IN (SELECT Value FROM dbo.fnSplit(@BusinessIDIsAnyOf, ',') WHERE ISNUMERIC(Value) = 1 )))

		-- CredentialEntityRole (Credential roles)
		DELETE 
		--SELECT *
		FROM acl.CredentialEntityRole
		WHERE CredentialEntityID = @CredentialEntityID
			AND (@ApplicationIDIsAnyOf IS NULL OR (@ApplicationIDIsAnyOf IS NOT NULL AND 
				ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationIDIsAnyOf, ',') WHERE ISNUMERIC(Value) = 1 )))
			AND (@BusinessIDIsAnyOf IS NULL OR (@BusinessIDIsAnyOf IS NOT NULL AND 
				BusinessEntityID IN (SELECT Value FROM dbo.fnSplit(@BusinessIDIsAnyOf, ',') WHERE ISNUMERIC(Value) = 1 )))

		-- CredentialEntityAllocation
		DELETE 
		--SELECT *
		FROM creds.CredentialEntityAllocation
		WHERE CredentialEntityID = @CredentialEntityID

		-----------------------------------------------------------------------------------------------------------------
		-- Remove application based objects.
		-----------------------------------------------------------------------------------------------------------------
		-- CredentialEntityApplication (Credential applications)
		DELETE 
		--SELECT *
		FROM creds.CredentialEntityApplication
		WHERE CredentialEntityID = @CredentialEntityID
			AND (@ApplicationIDIsAnyOf IS NULL OR (@ApplicationIDIsAnyOf IS NOT NULL AND 
				ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationIDIsAnyOf, ',') WHERE ISNUMERIC(Value) = 1 )))

		-----------------------------------------------------------------------------------------------------------------
		-- Remove business baesd objects.
		-----------------------------------------------------------------------------------------------------------------
		-- CredentialEntityBusinessEntity (Credential businesses)
		DELETE 
		--SELECT *
		FROM creds.CredentialEntityBusinessEntity
		WHERE CredentialEntityID = @CredentialEntityID
			AND (@ApplicationIDIsAnyOf IS NULL OR (@ApplicationIDIsAnyOf IS NOT NULL AND 
				ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationIDIsAnyOf, ',') WHERE ISNUMERIC(Value) = 1 )))
			AND (@BusinessIDIsAnyOf IS NULL OR (@BusinessIDIsAnyOf IS NOT NULL AND 
				BusinessEntityID IN (SELECT Value FROM dbo.fnSplit(@BusinessIDIsAnyOf, ',') WHERE ISNUMERIC(Value) = 1 )))

		-----------------------------------------------------------------------------------------------------------------
		-- Remove user based objects.
		-----------------------------------------------------------------------------------------------------------------
		-- CredentialEntityWorkgroups (Credential workgroups)
		DELETE 
		--SELECT *
		FROM creds.CredentialEntityWorkgroup
		WHERE CredentialEntityID = @CredentialEntityID

		-- User definitions
		-- ExtranetUser
		DELETE 
		--SELECT *
		FROM creds.ExtranetUser
		WHERE CredentialEntityID = @CredentialEntityID
			AND ISNULL(@RetainCredential, 0) = 0

		-- NetworkUser
		DELETE 
		--SELECT *
		FROM creds.NetworkUser
		WHERE CredentialEntityID = @CredentialEntityID
			AND ISNULL(@RetainCredential, 0) = 0

		-- Workgroup
		DELETE 
		--SELECT *
		FROM creds.Workgroup
		WHERE CredentialEntityID = @CredentialEntityID
			AND ISNULL(@RetainCredential, 0) = 0

		-- LinkedProviderUser
		DELETE 
		--SELECT *
		FROM creds.LinkedProviderUser
		WHERE CredentialEntityID = @CredentialEntityID

		-- OAuthClient
		DELETE 
		--SELECT *
		FROM creds.OAuthClient
		WHERE CredentialEntityID = @CredentialEntityID
			AND ISNULL(@RetainCredential, 0) = 0

		-----------------------------------------------------------------------------------------------------------------
		-- Remove primary object.
		-----------------------------------------------------------------------------------------------------------------
		-- Membership
		DELETE 
		FROM creds.Membership
		WHERE CredentialEntityID = @CredentialEntityID
			AND ISNULL(@RetainCredential, 0) = 0

		-- Profiles
		DELETE 
		--SELECT *
		FROM creds.Profiles
		WHERE CredentialEntityID = @CredentialEntityID
			AND ISNULL(@RetainCredential, 0) = 0

		-- CredentialTokens (Credential tokens)
		DELETE 
		FROM creds.CredentialToken
		WHERE CredentialEntityID = @CredentialEntityID
			AND ISNULL(@RetainCredential, 0) = 0

		-- CredentialRequest (Credential tokens)
		DELETE 
		--SELECT *
		FROM creds.CredentialRequest
		WHERE CredentialEntityID = @CredentialEntityID
			AND ISNULL(@RetainCredential, 0) = 0


		-- If the user does not want to keep the credental, then let's remove it permanently from the system.
		IF @KeepCredentialEntityID = 0
		BEGIN

			-- CredentialEntity (Main object)
			DELETE
			--SELECT *
			FROM creds.CredentialEntity
			WHERE CredentialEntityID = @CredentialEntityID
				AND ISNULL(@RetainCredential, 0) = 0

		END
	
	--------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);	
	
	END CATCH
	
	

END
