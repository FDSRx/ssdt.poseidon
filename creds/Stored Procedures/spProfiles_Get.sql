﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/21/2014
-- Description:	Retrieves a credential profile record
-- SAMPLE CALL:
/*
DECLARE @PropertyValueString VARCHAR(MAX), @PropertyValueBinary VARBINARY(MAX)
EXEC creds.spProfiles_Get
	@ProfileID = 1,
	@CredentialEntityID = NULL,
	@PropertyName = NULL,
	@PropertyValueString = @PropertyValueString OUTPUT,
	@PropertyValueBinary = @PropertyValueBinary OUTPUT
SELECT @PropertyValueString AS PropertyValueString, @PropertyValueBinary AS PropertyValueBinary
*/
-- SELECT * FROM creds.Profiles
-- =============================================
CREATE PROCEDURE [creds].[spProfiles_Get]
	@ProfileID BIGINT = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@PropertyName VARCHAR(256) = NULL OUTPUT,
	@PropertyValueString VARCHAR(MAX) = NULL OUTPUT,
	@PropertyValueBinary VARBINARY(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize Input
	----------------------------------------------------------------------
	SET @PropertyValueString = NULL;
	SET @PropertyValueBinary = NULL;
	
	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------
	IF @ProfileID IS NOT NULL
	BEGIN
		SET @CredentialEntityID = NULL;
		SET @PropertyName = NULL;
	END
	
	----------------------------------------------------------------------
	-- Fetch profile entry
	----------------------------------------------------------------------
	IF @ProfileID IS NOT NULL OR (@CredentialEntityID IS NOT NULL AND @PropertyName IS NOT NULL)
	BEGIN

		SELECT
			@ProfileID = ProfileID,
			@CredentialEntityID = CredentialEntityID,
			@PropertyName = PropertyName,
			@PropertyValueString = PropertyValueString,
			@PropertyValueBinary = PropertyValueBinary
		FROM creds.Profiles
		WHERE ProfileID = @ProfileID
			OR (CredentialEntityID = @CredentialEntityID
				AND PropertyName = @PropertyName
			)
			
	END
		
	
	
END

