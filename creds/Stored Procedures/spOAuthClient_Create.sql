﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/26/2015
-- Description:	Creates a new OAuthClient object.
-- SAMPLE CALL:
/*

DECLARE
	@ClientID NVARCHAR(256) = NULL,
	@SecretKey NVARCHAR(256) = NULL

EXEC creds.spOAuthClient_Create
	@CredentialEntityID = 801018,
	@ClientID = @ClientID OUTPUT,
	@SecretKey = @SecretKey OUTPUT,
	@CreatedBy = 'dhughes'

SELECT @ClientID AS ClientID, @SecretKey AS SecretKey

*/

-- SELECT * FROM creds.OAuthClient
-- SELECT * FROM creds.CredentialEntity
-- =============================================
CREATE PROCEDURE [creds].[spOAuthClient_Create]
	@CredentialEntityID BIGINT,
	@ClientID NVARCHAR(256) = NULL OUTPUT,
	@SecretKey NVARCHAR(256) = NULL OUTPUT,
	@Name VARCHAR(256) = NULL OUTPUT,
	@Description VARCHAR(1000) = NULL OUTPUT,
	@CreatedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@ClientID=' + dbo.fnToStringOrEmpty(@ClientID) + ';' +
		'@SecretKey=' + dbo.fnToStringOrEmpty(@SecretKey) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------------------------
	SET @clientID = ISNULL(@ClientID, NEWID());
	SET @SecretKey = ISNULL(@SecretKey, NEWID());

	-------------------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	-------------------------------------------------------------------------------------------------------------------------
	-- No resources defined.


	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Getter/Setter' AS ActionName,
			@CredentialEntityID AS CredentialEntityID, @ClientID AS ClientID, @SecretKey AS SecretKey,
			@Name AS Name, @Description AS Description, @CreatedBy AS CreatedBy
	END
		
	-------------------------------------------------------------------------------------------------------------------------
	-- Create object.
	-------------------------------------------------------------------------------------------------------------------------
	INSERT INTO creds.OAuthClient (
		CredentialEntityID,
		ClientID,
		SecretKey,
		Name,
		Description,
		CreatedBy
	)
	SELECT
		@CredentialEntityID AS CredentialEntityID,
		@ClientID AS ClientID,
		@SecretKey AS SecretKey,
		@Name AS Name,
		@Description AS Description,
		@CreatedBy AS CreatedBy




END
