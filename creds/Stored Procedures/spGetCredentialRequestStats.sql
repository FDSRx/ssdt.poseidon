﻿


CREATE PROCEDURE [creds].[spGetCredentialRequestStats]
 @CredentialEntityID BIGINT, 
 @ApplicationId INT = NULL 
AS
/*
 ==========================================================================================
 Author:		Alex Korolev
 Create date:	03/02/2016
 Description:	Returns User Login Stats based on @CredentialEntityID and @ApplicationId
 SAMPLE CALL:   creds.spGetCredentialRequestStats 1102528 ,10
 Modifications:
 ==========================================================================================
*/
BEGIN
	
	SET NOCOUNT ON;
	
WITH BaseStats AS(
 	SELECT 
	COUNT(cr.CredentialRequestID) AS [Count],
	YEAR(cr.DateCreated) as [Year],
	DATENAME(MONTH, cr.DateCreated) as [Month]
	FROM   creds.vwCredentialRequest cr
	WHERE cr.CredentialEntityID =  @CredentialEntityID
	AND (@ApplicationId IS NULL OR cr.ApplicationID = @ApplicationId )
	GROUP BY  year(DateCreated), DATENAME(MONTH,DateCreated)
	
)
SELECT [Year],
		COALESCE(January,0) AS Jan,
		COALESCE(February,0) AS Feb,
		COALESCE(March,0) AS Mar,
		COALESCE(April,0) AS APR,
		COALESCE(May,0) AS May,
		COALESCE(June,0) AS Jun,
		COALESCE(July,0) AS Jul,
		COALESCE(August,0) AS Aug,
		COALESCE(September,0) AS Sep,
		COALESCE(October,0) AS Oct,
		COALESCE(November,0) AS Nov,
		COALESCE(December, 0) AS Dec
FROM BaseStats
PIVOT(SUM([Count]) FOR [Month] 
IN (
	January,
	February,
	March,
	April,
    May,
	June,
	July,
	August,
	September,
	October,
	November,
    December))
AS PvtStats

END



