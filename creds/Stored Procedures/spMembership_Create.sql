﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Creates and/or updates a membership password
-- SAMPLE CALL:
/*
DECLARE 
	@CredentialEntityID BIGINT = 2,
	@EmailAddress VARCHAR(256) = 'dhughes@hcc-care.com',
	@Password VARCHAR(256) = 'test',
	@MembershipID BIGINT
	--,@PasswordQuestionID INT = 1
	
EXEC creds.spMembership_Create
	@CredentialEntityID = @CredentialEntityID,
	@EmailAddress = @EmailAddress,
	@Password = @Password,
	--@PasswordQuestionID = @PasswordQuestionID,
	@MembershipID = @MembershipID OUTPUT
	
SELECT @MembershipID AS MembershipID
*/
-- SELECT * FROM creds.Membership
-- =============================================
CREATE PROCEDURE [creds].[spMembership_Create]
	@CredentialEntityID BIGINT,
	@EmailAddress VARCHAR(256) = NULL,
	@Password VARCHAR(256) = NULL,
	@PasswordQuestionID VARCHAR(1000) = NULL,
	@PasswordAnswer VARCHAR(500) = NULL,
	@IsApproved BIT = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@IsDisabled BIT = NULL,
	@Comment VARCHAR(1000) = NULL,
	@IsChangePasswordRequired BIT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@MembershipID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	--------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-- <Summary>
	-- Cleans input variables and ensures the output variables are
	-- properly set upon retrieval
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	SET @IsApproved = ISNULL(@IsApproved, 0);
	SET @IsDisabled = ISNULL(@IsDisabled, 0);
	SET @IsChangePasswordRequired = ISNULL(@IsChangePasswordRequired, 0);
	SET @MembershipID = NULL;
	SET @Comment = CASE WHEN dbo.fnIsNullOrWhiteSpace(@Comment) = 1 THEN NULL ELSE @Comment END;
	
	--------------------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@MembershipExists BIT = 0,
		@ErrorMessage VARCHAR(4000),
		@PasswordHash VARBINARY(128),
		@PasswordSalt VARBINARY(10),
		@PasswordAnswerHash VARBINARY(128),
		@PasswordAnswerSalt VARBINARY(10)
	
	--------------------------------------------------------------------------------------------------------
	-- Ensure credential object was supplied.
	--------------------------------------------------------------------------------------------------------
	IF ISNULL(@CredentialEntityID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Unable to create a membership object.' +
			+ '. Exception: Object reference (@CredentialEntityID) is not set to an instance of an object. A CredentialEntityID is required.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;		
	END
	
	--------------------------------------------------------------------------------------------------------
	-- Verify membership does not already exist
	-- <Summary>
	-- Interrogates the membership data to see if the credential
	-- in question already has an existing membership.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	EXEC creds.spMembership_Exists
		@CredentialEntityID = @CredentialEntityID,
		@Exists = @MembershipExists OUTPUT
	
	-- If the membership exists, then let's fail with a meaningful response and exit the procedure.
	IF ISNULL(@MembershipExists, 0) = 1
	BEGIN
		
		SET @ErrorMessage = 'Unable to create a membership for CredentialEntityID:  ' + CONVERT(VARCHAR, @CredentialEntityID) 
			+ '. The membership already exists. Exception: Violation of primary key contstraint on table creds.Membership.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;		
	END
	
	
	--------------------------------------------------------------------------------------------------------
	-- Create encrypted passcodes (if applicable)
	-- <Summary>
	-- Verifies and encrypts clear text passcodes (i.e. Password and
	-- password security question).
	-- </Summary>
	-- <Remarks>
	-- If a security question was provided but an answer was not given
	-- then we need to fail the process.  This also holds true if a
	-- security answer was given, but no corresponding quesiton.
	-- </Remarks>
	--------------------------------------------------------------------------------------------------------
	-- If we have a password then lets encrypt.
	IF dbo.fnIsNullOrWhiteSpace(@Password) = 0
	BEGIN			
		--------------------------------------------------------------------------------------------------------
		-- Salt and Hash clear text
		--------------------------------------------------------------------------------------------------------
		SET @PasswordSalt = dbo.fnPasswordSalt(RAND());
		SET @PasswordHash = dbo.fnPasswordHash(@Password + CONVERT(VARCHAR(MAX), @PasswordSalt));
		
	END
	
	-- If we have a password question and answer then lets encrypt.
	IF ISNULL(@PasswordQuestionID, 0) <> 0 AND dbo.fnIsNullOrWhiteSpace(@PasswordAnswer) = 0
	BEGIN
				
		--------------------------------------------------------------------------------------------------------
		-- Salt and Hash clear text
		--------------------------------------------------------------------------------------------------------
		SET @PasswordAnswerSalt = dbo.fnPasswordSalt(RAND());
		SET @PasswordAnswerHash = dbo.fnPasswordHash(@Password + CONVERT(VARCHAR(MAX), @PasswordSalt));
		
	END
	
	-- *Fail procedure if a password question was provided but no answer was given
	IF ISNULL(@PasswordQuestionID, 0) <> 0 AND dbo.fnIsNullOrWhiteSpace(@PasswordAnswer) = 1
	BEGIN
			
		SET @ErrorMessage = 'Unable to create a membership for CredentialEntityID:  ' + CONVERT(VARCHAR, @CredentialEntityID) 
			+ '. A password question was provided but did not have a corresponding answer. ' + 
			'Exception: Violation of password security question constraint.  Both a password question and answer need to be provided.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;	
	END
	
	-- *Fail procedure if a password question answer was provided but no question was given
	IF ISNULL(@PasswordQuestionID, 0) = 0 AND dbo.fnIsNullOrWhiteSpace(@PasswordAnswer) = 0
	BEGIN
			
		SET @ErrorMessage = 'Unable to create a membership for CredentialEntityID:  ' + CONVERT(VARCHAR, @CredentialEntityID) 
			+ '. A password question answer was provided but did not have a corresponding question. ' + 
			'Exception: Violation of password security question constraint.  Both a password question and answer need to be provided.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;	
	END
	
	--------------------------------------------------------------------------------------------------------
	-- Determine if the password is required to be changed upon login.
	-- <Summary>
	-- Inspects the force password change flag to determine if the password needs to be set to
	-- expire immediately; otherwise, use the data from DatePasswordExpires parameter.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	IF @IsChangePasswordRequired = 1
	BEGIN
		SET @DatePasswordExpires = GETDATE();
	END
	
	--------------------------------------------------------------------------------------------------------
	-- Create credential membership record.
	-- <Summary>
	-- Creates a new crednetial membership record and returns the 
	-- record identity to the caller.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	INSERT INTO creds.Membership (
		CredentialEntityID,
		EmailAddress,
		PasswordHash,
		PasswordSalt,
		PasswordQuestionID,
		PasswordAnswerHash,
		PasswordAnswerSalt,
		IsApproved,
		DatePasswordExpires,
		IsDisabled,
		Comment,
		CreatedBy
	)
	SELECT
		@CredentialEntityID,
		@EmailAddress,
		@PasswordHash,
		@PasswordSalt,
		@PasswordQuestionID,
		@PasswordAnswerHash,
		@PasswordAnswerSalt,
		@IsApproved,
		@DatePasswordExpires,
		@IsDisabled,
		@Comment,
		@CreatedBy
	
	-- Retrieve record identity
	SET @MembershipID = SCOPE_IDENTITY();
	
	
END
