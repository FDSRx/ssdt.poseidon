﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/26/2015
-- Description:	Returns a list of Business object(s) that are applicable to the specified credential criteria.
/*

DECLARE
	@CredentialEntityID BIGINT = 960840,
	@BusinessData XML = NULL

EXEC creds.spCredentialEntityBusinessEntity_Get_List
	@CredentialEntityID = @CredentialEntityID,
	@BusinessData = @BusinessData OUTPUT,
	@IsOutputOnly = 1,
	@IsResultXml = 1

SELECT @BusinessData AS BusinessData

*/

-- SELECT * FROM creds.CredentialEntityBusinessEntity
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntityBusinessEntity_Get_List]
	@CredentialEntityID BIGINT,
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@IsResultXml BIT = 0,
	@IsOutputOnly BIT = 0,
	@BusinessData XML = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-----------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@BusinessData=' + dbo.fnToStringOrEmpty(@BusinessData) + ';' +
		'@IsResultXml=' + dbo.fnToStringOrEmpty(@IsResultXml) + ';' +
		'@IsOutputOnly=' + dbo.fnToStringOrEmpty(@IsOutputOnly) + ';' ;
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	----------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------------------------------
	SET @BusinessData = NULL;
	SET @IsResultXml = ISNULL(@IsResultXml, 0);
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);


	----------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	----------------------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpBusinesses') IS NOT NULL
	BEGIN
		DROP TABLE #tmpBusinesses;
	END

	CREATE TABLE #tmpBusinesses (
		BusinessID BIGINT,
		SourceStoreKey VARCHAR(50),
		Nabp VARCHAR(50),
		Name VARCHAR(256)
	);


	----------------------------------------------------------------------------------------------------------
	-- Fetch data.
	----------------------------------------------------------------------------------------------------------
	INSERT INTO #tmpBusinesses (
		BusinessID,
		SourceStoreKey,
		Nabp,
		Name
	)
	SELECT TOP 1
		ce.BusinessEntityID AS BusinessID,
		sto.SourceStoreID,
		sto.Nabp,
		sto.Name
	FROM creds.CredentialEntity ce
		JOIN dbo.Store sto
			ON ce.BusinessEntityID = sto.BusinessEntityID
	WHERE CredentialEntityID = @CredentialEntityID

	UNION

	SELECT
		cb.BusinessEntityID AS Id,
		sto.SourceStoreID,
		sto.Nabp,
		sto.Name
	--SELECT *
	--SElECT TOP 1 *
	FROM creds.CredentialEntityBusinessEntity cb (NOLOCK)
		JOIN dbo.Business biz (NOLOCK)
			ON cb.BusinessEntityID = biz.BusinessEntityID
		JOIN dbo.Store sto (NOLOCK)
			ON biz.BusinessEntityID = sto.BusinessEntityID
	WHERE cb.CredentialEntityID = @CredentialentityID
		AND ( @ApplicationID IS NULL OR ( @ApplicationID IS NOT NULL AND cb.ApplicationID = @ApplicationID) )



	----------------------------------------------------------------------------------------------------------
	-- Return data.
	----------------------------------------------------------------------------------------------------------
	-- Convert the data into a well-formatted xml string.
	SET @BusinessData = (
		SELECT
			BusinessID,
			SourceStoreKey,
			Nabp,
			Name
		--SELECT *
		--SElECT TOP 1 *
		FROM #tmpBusinesses
		FOR XML PATH('Business'), ROOT('Businesses')
	);

	-- If the data is to be a true result set, then return the data as it normally would be returned.
	IF ISNULL(@IsOutputOnly, 0) = 0
	BEGIN

		IF ISNULL(@IsResultXml, 0) = 0
		BEGIN
			SELECT
				BusinessID,
				SourceStoreKey,
				Nabp,
				Name
			--SELECT *
			--SElECT TOP 1 *
			FROM #tmpBusinesses
		END
		ELSE
		BEGIN
			SELECT @BusinessData AS BusinessData
		END

	END

	


END
