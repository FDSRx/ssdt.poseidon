﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/10/2015
-- Description:	Deletes a credential entity.
-- SAMPLE CALL:
/*

DECLARE
	@CredentialEntityID BIGINT = 961200,
	@ModifiedBy VARCHAR(256) = NULL

EXEC creds.spCredentialEntity_Delete
	@CredentialEntityID = @CredentialEntityID,
	@ModifiedBy = @ModifiedBy
	
*/

-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntity
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntity_Delete]
	@CredentialEntityID BIGINT,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT


	-----------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------------------
	-- No local declarations.


	-----------------------------------------------------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------
	IF @CredentialEntityID IS NULL 
	BEGIN
		SET @ErrorMessage = 'Unable to remove Credential/User from the system.  Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The @CredentialEntityID parameter cannot be null or empty.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END


	-----------------------------------------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-----------------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy



	BEGIN TRY
	-----------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	-----------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
		
		-----------------------------------------------------------------------------------------------------------------
		-- Remove Credential object data
		-----------------------------------------------------------------------------------------------------------------
		-- CredentialEntityGroup (Credential groups)
		DELETE 
		--SELECT *
		FROM acl.CredentialEntityRolePermission 
		WHERE CredentialEntityID = @CredentialEntityID

		-- CredentialEntityGroup (Credential groups)
		DELETE 
		--SELECT *
		FROM acl.CredentialEntityGroup 
		WHERE CredentialEntityID = @CredentialEntityID

		-- CredentialEntityRole (Credential roles)
		DELETE 
		--SELECT *
		FROM acl.CredentialEntityRole
		WHERE CredentialEntityID = @CredentialEntityID

		-- CredentialEntityAllocation
		DELETE 
		--SELECT *
		FROM creds.CredentialEntityAllocation
		WHERE CredentialEntityID = @CredentialEntityID

		-- CredentialEntityApplication (Credential applications)
		DELETE 
		--SELECT *
		FROM creds.CredentialEntityApplication
		WHERE CredentialEntityID = @CredentialEntityID

		-- CredentialEntityBusinessEntity (Credential businesses)
		DELETE 
		--SELECT *
		FROM creds.CredentialEntityBusinessEntity
		WHERE CredentialEntityID = @CredentialEntityID

		-- CredentialEntityWorkgroups (Credential workgroups)
		DELETE 
		--SELECT *
		FROM creds.CredentialEntityWorkgroup
		WHERE CredentialEntityID = @CredentialEntityID

		-- CredentialTokens (Credential tokens)
		DELETE 
		FROM creds.CredentialToken
		WHERE CredentialEntityID = @CredentialEntityID

		-- CredentialRequest (Credential tokens)
		DELETE 
		--SELECT *
		FROM creds.CredentialRequest
		WHERE CredentialEntityID = @CredentialEntityID

		-- Membership
		DELETE 
		FROM creds.Membership
		WHERE CredentialEntityID = @CredentialEntityID

		-- Profiles
		DELETE 
		--SELECT *
		FROM creds.Profiles
		WHERE CredentialEntityID = @CredentialEntityID

		-- User definitions
		-- ExtranetUser
		DELETE 
		FROM creds.ExtranetUser
		WHERE CredentialEntityID = @CredentialEntityID

		-- NetworkUser
		DELETE 
		--SELECT *
		FROM creds.NetworkUser
		WHERE CredentialEntityID = @CredentialEntityID

		-- Workgroup
		DELETE 
		--SELECT *
		FROM creds.Workgroup
		WHERE CredentialEntityID = @CredentialEntityID

		-- LinkedProviderUser
		DELETE 
		--SELECT *
		FROM creds.LinkedProviderUser
		WHERE CredentialEntityID = @CredentialEntityID

		-- OAuthClient
		DELETE 
		--SELECT *
		FROM creds.OAuthClient
		WHERE CredentialEntityID = @CredentialEntityID

		-- CredentialEntity (Main object)
		DELETE
		--SELECT *
		FROM creds.CredentialEntity
		WHERE CredentialEntityID = @CredentialEntityID
	
	-----------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	-----------------------------------------------------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);	
	
	END CATCH
	
	

END
