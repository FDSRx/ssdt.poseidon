﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Creates an extranet user
-- SAMPLE CALL:
/*
-- Good call
EXEC creds.spExtranetUser_Create
	@ApplicationIDList = 1,
	@BusinessEntityID = 10684,
	@Username = 'dhughes',
	@Password = 'password'
	--,@PersonID = 1
	--,@FirstName = 'Test Administrator'
	--,@LastName = 'hughes'
*/

-- SELECT * FROM acl.Roles
-- SELECT * FROM dbo.Application
-- SELECT * FROM creds.CredentialEntity
-- SELECT * FROM acl.CredentialEntityRole
-- SELECT * FROM creds.Membership
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- SELECT * FROM dbo.Person ORDER BY PersonID DESC
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [creds].[spExtranetUser_Create]
	--Application
	@ApplicationIDList VARCHAR(MAX),
	--Business
	@BusinessEntityID BIGINT,
	-- Roles
	@RoleIDList VARCHAR(MAX) = NULL,
	--User/Membership
	@Username VARCHAR(256),
	@Password VARCHAR(50),
	@PasswordQuestionID INT = NULL,
	@PasswordAnswer VARCHAR(500) = NULL,
	--Membership
	@IsApproved BIT = NULL,
	@IsChangePasswordRequired BIT = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@IsDisabled BIT = NULL,
	--Person
	@PersonID BIGINT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderID INT = NULL,
	@LanguageID INT = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Ouput
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@CredentialEntityExists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	--Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationIDList=' + dbo.fnToStringOrEmpty(@ApplicationIDList) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@RoleIDList=' + dbo.fnToStringOrEmpty(@RoleIDList) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID) + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName) + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName) + ';' +
		'@BirthDate=' + dbo.fnToStringOrEmpty(@BirthDate) + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary) + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate) + ';' +
		'@PhoneNumber_Home=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home) + ';' +
		'@PhoneNumber_Mobile=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile) + ';' 
	
	DECLARE @Source VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Source,
		@Message = 'The request was fired.',
		@Arguments = @Args	
	*/
		
	---------------------------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------------------------
	DECLARE @Trancount INT = @@TRANCOUNT;

	---------------------------------------------------------------------------------------------
	-- Sanitize Input
	---------------------------------------------------------------------------------------------
	SET @CredentialEntityID = NULL;
	SET @CredentialEntityExists = 0;
	
	---------------------------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@Exception VARCHAR(4000),
		@CredentialEntityTypeID INT = creds.fnGetCredentialEntityTypeID('EXTRANET'),
		@PersonTypeID INT = dbo.fnGetPersonTypeID('CRED'),
		@Arguments VARCHAR(MAX),
		@ErrorLogID INT,
		@IgnorePersonCreate BIT = 1
	
	---------------------------------------------------------------------------------------------
	-- Set variables
	---------------------------------------------------------------------------------------------
	SET @ApplicationIDList = dbo.fnApplicationKeyTranslator(@ApplicationIDList, DEFAULT);
	SET @RoleIDList = acl.fnRoleKeyTranslator(@RoleIDList, DEFAULT);
	
	---------------------------------------------------------------------------------------------
	-- Check for null arguments
	-- <Summary>
	-- Checks critical inputs for null argument exceptions.
	-- The following pieces are required to properly create
	-- an Extranet user
	-- </Summary>
	---------------------------------------------------------------------------------------------
		
		
	---------------------------------------------------------------------------------------------
	-- Argument is null exception: @ApplicationID
	---------------------------------------------------------------------------------------------
	IF dbo.fnIsNullOrWhiteSpace(@ApplicationIDList) IS NULL
	BEGIN
			
		SET @ErrorMessage = 'Unable to create Extranet User. Exception: Object reference (@ApplicationIDList) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END
	
	---------------------------------------------------------------------------------------------
	-- Argument is null exception: @BusinessEntityID
	---------------------------------------------------------------------------------------------
	IF @BusinessEntityID IS NULL
	BEGIN
			
		SET @ErrorMessage = 'Unable to create Extranet User. Exception: Object reference (@BusinessEntityID) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END
	
	---------------------------------------------------------------------------------------------
	-- Argument is null exception: @Username
	---------------------------------------------------------------------------------------------
	IF dbo.fnIsNullOrWhiteSpace(@Username) = 1
	BEGIN
			
		SET @ErrorMessage = 'Unable to create Extranet User. Exception: Object reference (@Username) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END
	
	---------------------------------------------------------------------------------------------
	-- Argument is null exception: @Password
	---------------------------------------------------------------------------------------------
	IF dbo.fnIsNullOrWhiteSpace(@Password) = 1
	BEGIN
			
		SET @ErrorMessage = 'Unable to create Extranet User. Exception: Object reference (@Password) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		
		RETURN;	
			
	END		
	
		
	---------------------------------------------------------------------------------------------
	-- Create an extranet user
	---------------------------------------------------------------------------------------------	
	BEGIN TRY
	
		---------------------------------------------------------------------------------------------
		-- Check for existing extranet user
		-- <Summary>
		-- Checks for an existing user.
		-- </Summary>
		---------------------------------------------------------------------------------------------
		EXEC creds.spCredentialEntity_Derived_Exists
			@CredentialEntityTypeID = @CredentialEntityTypeID,
			@BusinessEntityID = @BusinessEntityID,
			@DerivedCredentialKey = @Username,
			@CredentialEntityID = @CredentialEntityID OUTPUT
		
		IF @CredentialEntityID IS NOT NULL
		BEGIN
			
			SET @CredentialEntityExists = 1;
			
			SET @Arguments = 
				'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationIDList) + ';' +	
				'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +	
				'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';';
			

			SET @ErrorMessage = 'Unable to create Extranet User. Exception: Extranet CredentialEntityID already exists in the system. ' +
				'Arguments: ' + @Arguments;
				
			RAISERROR (
				@ErrorMessage, -- Message text.
				16, -- Severity.
				1 -- State.
			);	
			
			RETURN;
		END
		
	
		---------------------------------------------------------------------------------------------
		-- Validate person demographics
		-- <Summary>
		-- Determines if the supplied person data is sufficient enough to 
		-- create a person entity.
		-- </Summary>
		---------------------------------------------------------------------------------------------
		DECLARE
			@IsValid BIT = 0,
			@x XML, -- XML shortcut
			@IsAllRequiredPropertiesMissing BIT,
			@InvalidProperties VARCHAR(MAX)
			
		EXEC dbo.spPerson_IsValid
			--Person
			@PersonID = @PersonID,
			@Title = @Title,
			@FirstName = @FirstName,
			@LastName = @LastName,
			@MiddleName = @MiddleName,
			@Suffix = @Suffix,
			@BirthDate = @BirthDate,
			@GenderID = @GenderID,
			@LanguageID = @LanguageID,
			--Person Address
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			--Person Phone
			--	Home
			@PhoneNumber_Home = @PhoneNumber_Home,
			@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
			@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
			-- Mobile
			@PhoneNumber_Mobile = @PhoneNumber_Mobile,
			@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
			@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
			--Person Email
			--	Primary
			@EmailAddress_Primary = @EmailAddress_Primary,
			@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
			--	Alternate
			@EmailAddress_Alternate = @EmailAddress_Alternate,
			@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
			-- Output
			@IsValid = @IsValid OUTPUT,
			@ValidationSummary = @x OUTPUT
		
		SET @IsAllRequiredPropertiesMissing = @x.value('data(ValidationSummary/*:Exception/*:IsAllRequiredPropertiesMissing)[1]', 'BIT');
		SET @InvalidProperties = @x.value('data(ValidationSummary/*:Exception/*:InvalidProperties)[1]', 'VARCHAR(MAX)');
		
		-- If all properties are not missing then we need to make sure we have the First Name or the Person ID was correctly supplied.
		IF ISNULL(@IsAllRequiredPropertiesMissing, 0) = 0
		BEGIN
		
			-- If a PersonID was supplied and it is invalid, then we need to fail.
			IF @InvalidProperties LIKE '%PersonID%'
			BEGIN
				SET @ErrorMessage = 'Unable to create Extranet User. Exception: Invalid PersonID supplied.  The PersonID does not exist.';
					
				RAISERROR (
					@ErrorMessage, -- Message text.
					16, -- Severity.
					1 -- State.
				);
			END
			
			-- The a first name was not supplied, then we need to fail.
			IF @InvalidProperties LIKE '%FirstName%'
			BEGIN
				
				SET @ErrorMessage = 'Unable to create Extranet User. Exception: Invalid person demographics supplied to create a person entry. ' +
					'A minimum of First Name is required to create a person entry. ';
					
				RAISERROR (
					@ErrorMessage, -- Message text.
					16, -- Severity.
					1 -- State.
				);
			
			END
			ELSE
			BEGIN
				SET @LastName = ISNULL(@LastName, 'Person');
				SET @BirthDate = ISNULL(@BirthDate, '1/1/1900');
			END
		END
	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		---------------------------------------------------------------------------------------------
		-- Create a person entry (if applicable)
		-- <Summary>
		-- Adds extranet user demographics to the person repository
		-- </Summary>
		---------------------------------------------------------------------------------------------
		-- If a PersonID was not supplied and all the required person fields were not missing, then
		-- we must want to try and create a person.
		IF @PersonID IS NULL AND ISNULL(@IsAllRequiredPropertiesMissing, 0) = 0
		BEGIN
		
			-- Create person
			EXEC spPerson_Create
				@PersonTypeID = @PersonTypeID,
				@Title = @Title,
				@FirstName = @FirstName,
				@LastName = @LastName,
				@MiddleName = @MiddleName,
				@Suffix = @Suffix,
				@BirthDate = @BirthDate,
				@GenderID = @GenderID,
				@LanguageID = @LanguageID,
				--Person Address
				@AddressLine1 = @AddressLine1,
				@AddressLine2 = @AddressLine2,
				@City = @City,
				@State = @State,
				@PostalCode = @PostalCode,
				--Person Phone
				--	Home
				@PhoneNumber_Home = @PhoneNumber_Home,
				@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
				@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
				-- Mobile
				@PhoneNumber_Mobile = @PhoneNumber_Mobile,
				@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
				@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
				--Person Email
				--	Primary
				@EmailAddress_Primary = @EmailAddress_Primary,
				@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
				--	Alternate
				@EmailAddress_Alternate = @EmailAddress_Alternate,
				@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
				-- Caller
				@CreatedBy = @CreatedBy,
				--Output
				@PersonID = @PersonID OUTPUT,
				@ErrorLogID = @ErrorLogID OUTPUT
			
			-- If an error was encountered when creating a person then fail
			IF ISNULL(@ErrorLogID, 0) <> 0
			BEGIN
				--bubble error
				SELECT @ErrorMessage = ErrorMessage FROM ErrorLog (NOLOCK) WHERE ErrorLogID = @ErrorLogID
				
				-- grab exception
				SET @Exception = @ErrorMessage;
				-- create error message.
				SET @ErrorMessage = 'Unable to create person record for extranet user. Exception: ' + ISNULL(@ErrorMessage, '');
				
					
				RAISERROR (@ErrorMessage, -- Message text.
					   16, -- Severity.
					   1 -- State.
					   );		
			END			
		
		END
		
		---------------------------------------------------------------------------------------------
		-- Create credential entity
		-- <Summary>
		-- Creates a credential entity object to be applied
		-- to an extranet user.
		-- </Summary>
		---------------------------------------------------------------------------------------------
		EXEC creds.spCredentialEntity_Create
			@CredentialEntityTypeID = @CredentialEntityTypeID,
			@BusinessEntityID = @BusinessEntityID,
			@PersonID = @PersonID,
			@CreatedBy = @CreatedBy,
			@CredentialEntityID = @CredentialEntityID OUTPUT	

	
		---------------------------------------------------------------------------------------------
		-- Add the applications the user is allowed to have access to.
		-- <Remarks>
		-- There should be at least one application assigned to a user.
		-- </Remarks>
		---------------------------------------------------------------------------------------------		
		DECLARE @tblApplicationList AS TABLE (
			ApplicationID INT
		);
		
		INSERT INTO @tblApplicationList (
			ApplicationID
		)
		SELECT
			dbo.fnTrimAll(Value)
		FROM dbo.fnSplit(@ApplicationIDList, ',')
		WHERE ISNULL(Value, '') <> '' 
			AND ISNUMERIC(Value) = 1
		
		INSERT INTO creds.CredentialEntityApplication (
			CredentialEntityID,
			ApplicationID,
			CreatedBy
		)
		SELECT
			@CredentialEntityID,
			ApplicationID,
			@CreatedBy
		FROM @tblApplicationList
	
		---------------------------------------------------------------------------------------------
		-- Add the roles that define the user.
		---------------------------------------------------------------------------------------------
		DECLARE @tblRoles AS TABLE (
			RoleID INT
		);
		
		INSERT INTO @tblRoles (
			RoleID
		)
		SELECT
			dbo.fnTrimAll(Value)
		FROM dbo.fnSplit(@RoleIDList, ',')
		WHERE ISNULL(Value, '') <> '' 
			AND ISNUMERIC(Value) = 1
		
		INSERT INTO acl.CredentialEntityRole (
			CredentialEntityID,
			ApplicationID,
			BusinessEntityID,
			RoleID,
			CreatedBy
		)
		SELECT
			@CredentialEntityID,
			a.ApplicationID,
			@BusinessEntityID,
			r.RoleID,
			@CreatedBy
		FROM @tblRoles r
			CROSS APPLY @tblApplicationList a

		---------------------------------------------------------------------------------------------
		-- Create a membership for the credential
		-- <Summary>
		-- Creates a credential membership that consists of 
		-- encrypted password information and security question
		-- information (if applicable).
		-- </Summary>
		---------------------------------------------------------------------------------------------		
		EXEC creds.spMembership_Create
			@CredentialEntityID = @CredentialEntityID,
			@EmailAddress = @EmailAddress_Primary,
			@Password = @Password,
			@PasswordQuestionID = @PasswordQuestionID,
			@PasswordAnswer = @PasswordAnswer,
			@IsApproved = @IsApproved,			
			@DatePasswordExpires = @DatePasswordExpires,
			@IsDisabled = @IsDisabled,
			@IsChangePasswordRequired = @IsChangePasswordRequired,
			@CreatedBy = @CreatedBy		

		
		---------------------------------------------------------------------------------------------
		-- Create extranet entry
		-- <Summary>
		-- Finalize process by creating the extranet entry.
		-- </Summary>
		---------------------------------------------------------------------------------------------
		INSERT INTO creds.ExtranetUser (
			CredentialEntityID,
			Username,
			CreatedBy
		)
		SELECT
			@CredentialEntityID,
			@Username,
			@CreatedBy
			
	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END
	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END

		-- Log and rethrow error
		EXECUTE [dbo].spLogException
			@Exception = @Exception,
			@Arguments = @Arguments
		
		-- re-throw error
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		
		--DECLARE @ErrorProcedure VARCHAR(4000) = ERROR_PROCEDURE();
		
		---- Verifies that data is in a commitable state.  If not, then we need to
		---- re-throw the error so that it continues to bubble up the chain.
		--EXECUTE dbo.spTryThrowUncommitableWriteError @ErrorProcedure;
		
	END CATCH
	
END
