﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/31/2014
-- Description:	Expires (Invalidates) the token.
-- SAMPLE CALL:
/*

DECLARE @Token VARCHAR(50) = '3C99FB57-2C81-498B-B340-6F94808EF885'

EXEC creds.spCredentialToken_Expire
	@Token = @Token


*/

-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
-- =============================================
CREATE PROCEDURE creds.spCredentialToken_Expire
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@CredentialEntityID BIGINT = NULL,
	@CredentialTokenID BIGINT = NULL,
	@TokenTypeID INT = NULL,
	@Token VARCHAR(50) = NULL,
	@ModifiedBy VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------
	SET @ModifiedBy = ISNULL(@ModifiedBy, 'TokenExpirationManager');
	
	---------------------------------------------------------------------------------------
	-- Set variables
	---------------------------------------------------------------------------------------
	IF @CredentialTokenID IS NOT NULL
	BEGIN
		SET @Token = NULL;
	END


	---------------------------------------------------------------------------------------
	-- Expire object.
	---------------------------------------------------------------------------------------
	UPDATE tok
	SET 
		tok.DateExpires = GETDATE(),
		tok.ModifiedBy = @ModifiedBy
	--SELECT *
	FROM creds.CredentialToken tok
	WHERE CredentialTokenID = @CredentialTokenID
		OR Token = @Token
			AND ( @TokenTypeID IS NULL OR TokenTypeID = @TokenTypeID )
		AND DateExpires > GETDATE()
			
			
			
END
