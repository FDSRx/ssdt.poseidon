﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/31/2013
-- Description:	Returns credential token information
-- SAMPLE CALL:
/*
DECLARE 
	@CredentialTokenID BIGINT = NULL, 
	@Token VARCHAR(50) = 'D1D88CF0-5CDE-4410-81CE-EBC92A0D0636',
	@CredentialEntityID BIGINT,
	@TokenData XML,
	@IsExpired BIT,
	@DateExpires DATETIME,
	@ApplicationID INT,
	@BusinessEntityID INT,
	@InvalidateTokenOnExpired BIT = 1

EXEC creds.spCredentialToken_Get
	@CredentialTokenID = @CredentialTokenID OUTPUT,
	@Token = @Token OUTPUT,
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@TokenData = @TokenData OUTPUT,
	@IsExpired = @IsExpired OUTPUT,
	@DateExpires = @DateExpires OUTPUT,
	@ApplicationID = @ApplicationID OUTPUT,
	@BusinessEntityID = @BusinessEntityID OUTPUT,
	@InvalidateTokenOnExpired = @InvalidateTokenOnExpired

SELECT @CredentialTokenID AS CredentialTokenID, @CredentialEntityID AS CredentialEntityID,
	@Token AS Token, @TokenData AS TokenData,
	@ApplicationID AS ApplicationID, @BusinessEntityID AS BusinessEntityID,
	@DateExpires AS DateExpires, @IsExpired AS IsExpired	
*/
--SELECT * FROM creds.CredentialToken
-- =============================================
CREATE PROCEDURE [creds].[spCredentialToken_Get]
	@CredentialTokenID BIGINT = NULL OUTPUT,
	@Token VARCHAR(50) = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@TokenData XML = NULL OUTPUT,
	@IsExpired BIT = NULL OUTPUT,
	@DateExpires DATETIME = NULL OUTPUT,
	@ApplicationID INT = NULL OUTPUT,
	@BusinessEntityID INT = NULL OUTPUT,
	@InvalidateTokenOnExpired BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------
	SET @IsExpired = 0;
	SET @TokenData = NULL;
	SET @DateExpires = NULL;
	SET @InvalidateTokenOnExpired = ISNULL(@InvalidateTokenOnExpired, 0);
	--SET @ApplicationID = NULL;
	--SET @BusinessEntityID = NULL;
	
	--------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
	
	-----------------------------------------------------------------
	-- Argument is null exception
	-----------------------------------------------------------------
	IF @CredentialTokenID IS NULL AND ISNULL(@Token, '') = ''
	BEGIN
	
		SET @ErrorMessage = 'Unable to initiate credential token retrieval process. ' +
			'Exception: Object references (@CredentialTokenID and @Token) are not set to an instance ' +
			'of an object. A CredentialTokenID or Token needs to be referenced.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;
				
	END
	
	--------------------------------------------------------------
	-- Fetch credential token
	--------------------------------------------------------------
	-- Copy identities
	DECLARE 
		@RefCredentialTokenID BIGINT = @CredentialTokenID,
		@RefToken UNIQUEIDENTIFIER


	IF dbo.fnIsUniqueIdentifier(@Token) = 1 
		SET @RefToken = CAST(@Token AS UNIQUEIDENTIFIER)
	
	-- Destroy identities
	SET @CredentialTokenID = NULL;
	SET @Token = NULL;
	
	SELECT 
		@CredentialTokenID = tok.CredentialTokenID,
		@CredentialEntityID = tok.CredentialEntityID,
		@ApplicationID = tok.ApplicationID,
		@BusinessEntityID = tok.BusinessEntityID,
		@Token = tok.Token,
		@TokenData = tok.TokenData,
		@DateExpires = tok.DateExpires,
		@IsExpired = CASE WHEN tok.DateExpires >= GETDATE() THEN 0 ELSE 1 END
	--SELECT *
	FROM creds.CredentialToken tok
	WHERE 
		(tok.Token = @RefToken OR CredentialTokenID = @RefCredentialTokenID)
		--(CONVERT(VARCHAR(50), tok.Token) = @Token OR CredentialTokenID = @RefCredentialTokenID)
		AND ( @ApplicationID IS NULL OR ApplicationID = @ApplicationID )
		AND ( @BusinessEntityID IS NULL OR BusinessEntityID = @BusinessEntityID )


	-- If the caller does not want any information returned on an expired token
	-- then we need to destroy the token data.
	IF ISNULL(@InvalidateTokenOnExpired, 0) = 1 AND ISNULL(@IsExpired, 0) = 1
	BEGIN
		EXEC creds.spCredentialToken_Data_Invalidate
			@CredentialTokenID = @CredentialTokenID OUTPUT,
			@CredentialEntityID = @CredentialEntityID OUTPUT,
			@Token = @Token OUTPUT,
			@TokenData = @TokenData OUTPUT
	END

END
