﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/1/2013
-- Description:	Invalidates credential token (if applicable)
-- =============================================
CREATE PROCEDURE [creds].spCredentialToken_Data_Invalidate
	@CredentialTokenID BIGINT = NULL OUTPUT,
	@Token VARCHAR(50) = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@TokenData XML = NULL OUTPUT,
	@IsExpired BIT = NULL OUTPUT,
	@DateExpires DATETIME = NULL OUTPUT,
	@ApplicationID INT = NULL OUTPUT,
	@BusinessEntityID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	--------------------------------------------------------------
	-- Invalidate credential token data
	-- <Summary>
	-- Destroys all the appropriate token data so that non-sensitive
	-- information is returned back to the caller.
	-- </Summary>
	--------------------------------------------------------------
	SET @CredentialTokenID = NULL;
	SET @Token = NULL;
	SET @CredentialEntityID = NULL;
	SET @TokenData = NULL;
	
END