﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/11/2013
-- Description:	Indicates whether the credential entity workgoup already exists.
-- SAMPLE CALL:
/*
DECLARE 
	@CredentialEntityWorkgroupID BIGINT, 
	@Exists BIT
	
EXEC creds.spCredentialEntityWorkgroup_Exists
	@CredentialEntityID = 11,
	@WorkgoupID = 12,
	@CredentialEntityWorkgroupID = @CredentialEntityWorkgroupID OUTPUT,
	@Exists = @Exists OUTPUT
	
SELECT @CredentialEntityWorkgroupID AS CredentialEntityWorkgroupID, @Exists AS DoesExist
*/
-- SELECT * FROM creds.CredentialEntityWorkgroup
-- SELECT * FROM creds.NetworkUser
-- SELECT * FROM creds.Workgroup
-- =============================================
CREATE PROCEDURE creds.spCredentialEntityWorkgroup_Exists
	@CredentialEntityID BIGINT,
	@WorkgoupID BIGINT,
	@CredentialEntityWorkgroupID BIGINT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @CredentialEntityWorkgroupID = NULL;
	SET @Exists = 0;
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
	
	----------------------------------------------------------------------
	-- Check for null arguments
	-- <Summary>
	-- Checks critical inputs for null argument exceptions.
	-- The following pieces are required to properly interrogate credential
	-- workgroup collections.
	-- </Summary>
	----------------------------------------------------------------------
		
		
	----------------------------------------------------------------------
	-- Argument is null exception: @CredentialEntityID
	----------------------------------------------------------------------
	IF @CredentialEntityID IS NULL
	BEGIN
			
		SET @ErrorMessage = 'Unable to interrogate data. Exception: Object reference (@CredentialEntityID) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END
	
	----------------------------------------------------------------------
	-- Argument is null exception: @WorkgoupID
	----------------------------------------------------------------------
	IF @WorkgoupID IS NULL
	BEGIN
			
		SET @ErrorMessage = 'Unable to interrogate data. Exception: Object reference (@WorkgoupID) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END
	
	---------------------------------------------------------
	-- Determine if the record exists
	---------------------------------------------------------
	SET @CredentialEntityWorkgroupID = (
		SELECT
			CredentialEntityWorkgroupID
		FROM creds.CredentialEntityWorkgroup
		WHERE CredentialEntityID = @CredentialEntityID
			AND WorkgroupID = @WorkgoupID
	);
	
	-- Set flag
	SET @Exists = CASE WHEN @CredentialEntityWorkgroupID IS NOT NULL THEN 1 ELSE 0 END;
	
END
