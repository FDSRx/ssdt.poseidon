﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/1/2013
-- Description:	Gets the credential entity base information
-- SAMPLE CALL: 
/*
DECLARE
	@CredentialEntityID BIGINT = 1,
	@ApplicationData XML = NULL,
	@BusinessEntityID INT = NULL,
	@CredentialEntityTypeID INT = NULL
	
EXEC creds.spCredentialEntity_Get
	@CredentialEntityID = @CredentialEntityID,
	@ApplicationData = @ApplicationData OUTPUT,
	@BusinessEntityID = @BusinessEntityID OUTPUT,
	@CredentialEntityTypeID = @CredentialEntityTypeID OUTPUT

SELECT @ApplicationData AS ApplicationData, @BusinessEntityID AS BusinessEntityID, 
	@CredentialEntityTypeID AS CredentialEntityTypeID
*/
-- SELECT * FROM creds.CredentialEntity
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntity_Get]
	@CredentialEntityID BIGINT,
	@BusinessEntityID INT OUTPUT,
	@ApplicationData XML = NULL OUTPUT,
	@CredentialEntityTypeID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-------------------------------------------------------------------
	-- Sanitize input
	-------------------------------------------------------------------
	SET @BusinessEntityID = NULL;
	SET @CredentialEntityTypeID = NULL;
	
	-------------------------------------------------------------------
	-- Fetch data
	-------------------------------------------------------------------
	SELECT
		@CredentialEntityTypeID = CredentialEntityTypeID,
		@BusinessEntityID = BusinessEntityID
	--SELECT *
	FROM creds.CredentialEntity
	WHERE CredentialEntityID = @CredentialEntityID
	
	-- Build list of applications the user has access to.
	SET @ApplicationData = (
		SELECT
			app.ApplicationID,
			app.Code,
			app.Name
		FROM creds.CredentialEntityApplication capp
			JOIN dbo.Application app
				ON capp.ApplicationID = app.ApplicationID
		WHERE capp.CredentialEntityID = @CredentialEntityID
		FOR XML PATH('Application'), ROOT('Applications')
	);

	
END
