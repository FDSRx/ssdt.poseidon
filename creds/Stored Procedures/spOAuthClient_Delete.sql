﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/26/2015
-- Description:	Deletes an OAuthClient object.
-- SAMPLE CALL:
/*

DECLARE
	@CredentialEntityID BIGINT = 801018,
	@ClientID NVARCHAR(256) = NULL,
	@SecretKey NVARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = 'dhughes'

EXEC creds.spOAuthClient_Delete
	@CredentialEntityID = @CredentialEntityID,
	@ModifiedBy = @ModifiedBy

*/

-- SELECT * FROM creds.OAuthClient
-- SELECT * FROM creds.CredentialEntity
-- =============================================
CREATE PROCEDURE [creds].[spOAuthClient_Delete]
	@CredentialEntityID BIGINT = NULL,
	@ClientID NVARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT

	-----------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@ClientID=' + dbo.fnToStringOrEmpty(@ClientID) + ';' +
		'@SecretKey=' + dbo.fnToStringOrEmpty(@SecretKey) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------
	-- No sanitation required.

	-----------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	-----------------------------------------------------------------------------------------------------------------
	-- No resources defined.

	-----------------------------------------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-----------------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy


	BEGIN TRY
	-----------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	-----------------------------------------------------------------------------------------------------------------
	SET @Trancount = @@TRANCOUNT;	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
			
		-----------------------------------------------------------------------------------------------------------------
		-- Delete object.
		-----------------------------------------------------------------------------------------------------------------
		-- If the object is a pure OAuthClient then remove all traces of the object.
		IF NOT EXISTS(SELECT TOP 1 * FROM ExtranetUser WHERE CredentialEntityID = @CredentialEntityID)
			AND NOT EXISTS(SELECT TOP 1 * FROM NetworkUser WHERE CredentialEntityID = @CredentialEntityID)
			AND NOT EXISTS(SELECT TOP 1 * FROM LinkedProviderUser WHERE CredentialEntityID = @CredentialEntityID)
			AND NOT EXiSTS(SELECT TOP 1 * FROM Workgroup WHERE CredentialEntityID = @CredentialEntityID)
		BEGIN

			-- Remove the entire credential.
			EXEC creds.spCredentialEntity_Delete
				@CredentialEntityID,
				@ModifiedBy = @ModifiedBy

		END
		ELSE
		BEGIN

			-- Only remove the data service credential.
			DELETE obj
			FROM creds.OAuthClient obj
			WHERE ( @CredentialEntityID IS NOT NULL AND CredentialEntityID = @CredentialEntityID )
				--OR (
				--	@CredentialEntityID IS NULL
				--	AND ClientID = @ClientID
				--)
		END

	-----------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	-----------------------------------------------------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		SET @ErrorMessage = ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			16,
			1
		);	
	
	END CATCH



END
