﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/5/2014
-- Description:	Creates a CredentialEntityRequest object.
-- SAMPLE CALL:
/*
DECLARE @CredentialEntityRequestID BIGINT = NULL

EXEC creds.spCredentialRequest_Create
	@RequestTypeID = 2,
	@ApplicationID = 1,
	@BusinessEntityID = 92,
	@CredentialEntityID = 1,
	@ApplicationKey = 'ESMT',
	@BusinessKey = '70000',
	@CredentialKey = 'dhughes',
	@PassKey = 'test',
	@SessionKey = NULL,
	@IsValid = 0,
	@IpAddress = ':111',
	@BrowserName = 'FireFox',
	@BrowserVersion = '22',
	@DeviceName = NULL,
	@IsMobile = 0,
	@UserAgent = NULL,
	@Comments = 'Test entry',
	@CreatedBy = 'dhughes',
	@CredentialRequestID = @CredentialEntityRequestID OUTPUT

SELECT @CredentialEntityRequestID AS CredentialEntityRequestID

*/

-- SELECT * FROM creds.CredentialRequest
-- =============================================
CREATE PROCEDURE [creds].[spCredentialRequest_Create]
	@RequestTypeID INT = NULL,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@CredentialEntityID BIGINT = NULL,
	@ApplicationKey VARCHAR(256) = NULL,
	@BusinessKey VARCHAR(256) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@CredentialTypeKey VARCHAR(50) = NULL,
	@CredentialKey VARCHAR(256) = NULL,
	@PassKey VARCHAR(256) = NULL,
	@SessionKey VARCHAR(256) = NULL,
	@IsValid BIT = NULL,
	@IpAddress VARCHAR(256) = NULL,
	@BrowserName VARCHAR(256) = NULL,
	@BrowserVersion VARCHAR(50) = NULL,
	@DeviceName VARCHAR(50) = NULL,
	@IsMobile BIT = NULL,
	@UserAgent VARCHAR(500) = NULL,
	@Comments VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CredentialRequestID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------
	SET @CredentialRequestID = NULL;
	SET @IsMobile = ISNULL(@IsMobile, 0);
	SET @IsValid = ISNULL(@IsValid, 0);
	
	------------------------------------------------------------------------------------
	-- Create a new CredentialRequest record.
	------------------------------------------------------------------------------------
	INSERT INTO creds.CredentialRequest (
		RequestTypeID,
		ApplicationID,
		BusinessEntityID,
		CredentialEntityID,
		ApplicationKey,
		BusinessKey,
		BusinessKeyType,
		CredentialTypeKey,
		CredentialKey,
		PassKey,
		SessionKey,
		IsValid,
		IPAddress,
		BrowserName,
		BrowserVersion,
		DeviceName,
		IsMobile,
		UserAgent,
		Comments,
		CreatedBy
	)
	SELECT
		@RequestTypeID,
		@ApplicationID,
		@BusinessEntityID,
		@CredentialEntityID,
		@ApplicationKey,
		@BusinessKey,
		@BusinessKeyType,
		@CredentialTypeKey,
		@CredentialKey,
		@PassKey,
		@SessionKey,
		@IsValid,
		@IpAddress,
		@BrowserName,
		@BrowserVersion,
		@DeviceName,
		@IsMobile,
		@UserAgent,
		@Comments,
		@CreatedBy
		
	-- Retrieve record identity.
	SET @CredentialRequestID = SCOPE_IDENTITY();
END
