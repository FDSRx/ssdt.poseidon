﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/5/2015
-- Description:	Creates/updates a CredentialEntityApplication object.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityApplicationID BIGINT = NULL,
	@Exists BIT = NULL
	
	
EXEC creds.spCredentialEntityApplication_Merge
	@CredentialEntityID = 801050,
	@ApplicationID = 10,
	@CredentialEntityApplicationID = @CredentialEntityApplicationID OUTPUT,
	@Exists = @Exists OUTPUT

SELECT @CredentialEntityApplicationID AS CredentialEntityApplicationID, @Exists AS IsFound

*/

-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntityApplication
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntityApplication_Merge]
	@CredentialEntityID BIGINT,
	@ApplicationID INT,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@CredentialEntityApplicationID BIGINT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------
	DECLARE @ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@CredentialEntityApplicationID=' + dbo.fnToStringOrEmpty(@CredentialEntityApplicationID) + ';' +
		'@Exists=' + dbo.fnToStringOrEmpty(@Exists) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CredentialEntityApplicationID = NULL;
	SET @Exists = 0;
	SET @CreatedBy = ISNULL(@CreatedBy, @ModifiedBy);
	SET @ModifiedBy = ISNULL(@ModifiedBy, @CreatedBy);
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)

	/*
	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- CredentialEntityID
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- ApplicationID
	IF @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@RoleID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/
		
	-------------------------------------------------------------------------------
	-- Determine if CredentialEntityApplication object exists.
	-------------------------------------------------------------------------------
	EXEC creds.spCredentialEntityApplication_Exists
		@CredentialEntityID = @CredentialEntityID,
		@ApplicationID = @ApplicationID,
		@CredentialEntityApplicationID = @CredentialEntityApplicationID OUTPUT,
		@Exists = @Exists OUTPUT
	
	-------------------------------------------------------------------------------
	-- Merge record.
	-- <Summary>
	-- If the record does not exist, then add then create a new
	-- CredentialEntityApplication object; otherwise, do nothing.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- If a CredentialEntityApplicationID was not discovered, then create a new
	-- record.
	IF @CredentialEntityApplicationID IS NULL
	BEGIN
	
		-- Create new CredentialEntityApplicationID object.
		EXEC creds.spCredentialEntityApplication_Create
			@CredentialEntityID = @CredentialEntityID,
			@ApplicationID = @ApplicationID,
			@CreatedBy = @CreatedBy,
			@CredentialEntityApplicationID = @CredentialEntityApplicationID OUTPUT
	
	END
	-- If a record was discovered then do nothing.
	ELSE
	BEGIN
	
		SET @Exists = 1;
		
	END
	
	
		
		

		
END
