﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/15/2015
-- Description:	Returns the linked account that is associated with the credential and provider.
/*
DECLARE
	@LinkedProviderUserID BIGINT = 1,
	@CredentialEntityID BIGINT,
	@CredentialProviderID INT,
	@UserKey VARCHAR(256)
	
EXEC creds.spLinkedProviderUser_Get
	@LinkedProviderUserID = 1,
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@CredentialProviderID = @CredentialProviderID OUTPUT,
	@UserKey = @UserKey OUTPUT

SELECT @LinkedProviderUserID AS LinkedProviderUserID, @CredentialEntityID AS CredentialEntityID,
	@CredentialProviderID AS CredentialProviderID, @UserKey AS UserKey
*/
-- =============================================
CREATE PROCEDURE [creds].[spLinkedProviderUser_Get]
	@LinkedProviderUserID BIGINT = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@CredentialProviderID INT = NULL OUTPUT,
	@UserKey VARCHAR(256) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		
	
	-- Debug: Log request
	/*	
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@LinkedProviderUserID=' + dbo.fnToStringOrEmpty(@LinkedProviderUserID) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@CredentialProviderID=' + dbo.fnToStringOrEmpty(@CredentialProviderID) + ';' +
		'@UserKey=' + dbo.fnToStringOrEmpty(@UserKey) + ';'
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	
	*/

	-------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------
	SET @UserKey = NULL;
	
	-------------------------------------------------------------------------------------------------
	-- Set variables
	-------------------------------------------------------------------------------------------------
	IF @LinkedProviderUserID IS NOT NULL
	BEGIN
		SET @CredentialProviderID = NULL;
		SET @CredentialEntityID = NULL;
	END
		
	-------------------------------------------------------------------------------------------------
	-- Fetch results
	-------------------------------------------------------------------------------------------------
	SELECT TOP 1
		@LinkedProviderUserID = LinkedProviderUserID,
		@CredentialEntityID = CredentialEntityID,
		@CredentialProviderID = CredentialProviderID,
		@UserKey = UserKey 
	--SELECT *
	FROM creds.LinkedProviderUser
	WHERE LinkedProviderUserID = @LinkedProviderUserID
		OR ( CredentialEntityID = @CredentialEntityID
			AND CredentialProviderID = @CredentialProviderID )	

	-- Void data if nothing was returned.
	IF @@ROWCOUNT = 0
	BEGIN
		SET @CredentialEntityID = NULL;
		SET @CredentialProviderID = NULL;
		SET @LinkedProviderUserID = NULL;
		SET @UserKey = NULL;
	END

END
