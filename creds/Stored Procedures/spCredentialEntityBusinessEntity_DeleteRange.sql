﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/11/2014
-- Description:	Removes a single or collection of CredentialEntityBusinessEntity objects based on the specified criteria.
-- SAMPLE CALL:
/*


EXEC creds.spCredentialEntityBusinessEntity_DeleteRange
	@CredentialEntityID = 959790,
	@ApplicationID = 8,
	@BusinessEntityID = '3759,3760',
	@ModifiedBy = 'dhughes'

*/

-- SELECT * FROM creds.CredentialEntityBusinessEntity
-- SELECT * FROM creds.CredentialEntityBusinessEntity_History
-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntityBusinessEntity_DeleteRange]
	@CredentialEntityBusinessEntityID VARCHAR(MAX) = NULL,
	@CredentialEntityID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@BusinessEntityID VARCHAR(MAX) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID)

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @CredentialEntityBusinessEntityID, @CredentialEntityID, @BusinessEntityID
	IF dbo.fnIsNullOrWhiteSpace(@CredentialEntityBusinessEntityID) = 1 
		AND @CredentialEntityID IS NULL 
		AND dbo.fnIsNullOrWhiteSpace(@BusinessEntityID) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to delete BusinessEntity asocitation. Object references (@CredentialEntityBusinessEntityID or @CredentialEntityID and @BusinessEntityID) ' +
			'are not set to an instance of an object. ' +
			'The parameters, @CredentialEntityBusinessEntityID or @CredentialEntityID and @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @CredentialEntityBusinessEntityID AND @CredentialEntityID.
	IF dbo.fnIsNullOrWhiteSpace(@CredentialEntityBusinessEntityID) = 1  AND 
		@CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to delete BusinessEntity asocitation. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END		
	
	-- @CredentialEntityBusinessEntityID AND @BusinessEntityID.
	IF dbo.fnIsNullOrWhiteSpace(@CredentialEntityBusinessEntityID) = 1 
		AND @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to delete BusinessEntity Association. Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy
			
	-------------------------------------------------------------------------------
	-- Delete CredentialEntityBusinessEntity object(s).
	-- <Summary>
	-- Removes a single or collection of CredentialEntityBusinessEntity objects.
	-- </Summary>
	-------------------------------------------------------------------------------
	DELETE cebe
	FROM creds.CredentialEntityBusinessEntity cebe
	WHERE CredentialEntityBusinessEntityID IN (SElECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@CredentialEntityBusinessEntityID, ','))
		OR (
			CredentialEntityID = @CredentialEntityID
			AND ( @ApplicationID IS NULL OR ApplicationID = @ApplicationID )
			AND BusinessEntityID IN (SElECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@BusinessEntityID, ','))			
		)
		
END
