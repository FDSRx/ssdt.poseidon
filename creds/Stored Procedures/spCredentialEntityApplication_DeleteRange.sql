﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/5/2015
-- Description:	Removes a single or collection of CredentialEntityApplication objects based on the specified criteria.
-- SAMPLE CALL:
/*


EXEC creds.spCredentialEntityApplication_DeleteRange
	@CredentialEntityID = 959790,
	@ApplicationID = 8,
	@ModifiedBy = 'dhughes'

*/

-- SELECT * FROM creds.CredentialEntityApplication
-- SELECT * FROM creds.CredentialEntityApplication_History
-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntityApplication_DeleteRange]
	@CredentialEntityApplicationID VARCHAR(MAX) = NULL,
	@CredentialEntityID BIGINT = NULL,
	@ApplicationID VARCHAR(MAX) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID)

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @CredentialEntityBusinessEntityID, @CredentialEntityID, @BusinessEntityID
	IF dbo.fnIsNullOrWhiteSpace(@CredentialEntityApplicationID) = 1 
		AND @CredentialEntityID IS NULL
		AND dbo.fnISNullOrWhiteSpace(@ApplicationID) = 1 
	BEGIN
		SET @ErrorMessage = 'Unable to delete CredentialEntityApplication object. Object references ' +
			'(@CredentialEntityApplicationID or @CredentialEntityID and @ApplicationID) ' +
			'are not set to an instance of an object. ' +
			'The parameters, @CredentialEntityApplicationID or @CredentialEntityID and @ApplicationID, cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @CredentialEntityBusinessEntityID AND @CredentialEntityID.
	IF dbo.fnIsNullOrWhiteSpace(@CredentialEntityApplicationID) = 1  AND 
		@CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to delete CredentialEntityApplication object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END		
	
	-- @CredentialEntityBusinessEntityID AND @BusinessEntityID.
	IF dbo.fnIsNullOrWhiteSpace(@CredentialEntityApplicationID) = 1 
		AND @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to delete CredentialEntityApplication object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy
			
	-------------------------------------------------------------------------------
	-- Delete CredentialEntityApplication object(s).
	-- <Summary>
	-- Removes a single or collection of CredentialEntityApplication objects.
	-- </Summary>
	-------------------------------------------------------------------------------
	DELETE cea
	FROM creds.CredentialEntityApplication cea
	WHERE cea.CredentialEntityApplicationID IN (SElECT Value FROM dbo.fnSplit(@CredentialEntityApplicationID, ',') WHERE ISNUMERIC(Value) = 1)
		OR (
			cea.CredentialEntityID = @CredentialEntityID
			AND cea.ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationID, ',') WHERE ISNUMERIC(Value) = 1)	
		)
		
END
