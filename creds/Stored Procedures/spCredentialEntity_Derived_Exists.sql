﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Determines if the extranet user already exists.
-- SAMPLE CALL:
/*

DECLARE
	@CredentialEntityID BIGINT,
	@Exists BIT,
	@IsLockedOut BIT,
	@DateLockOutExpires DATETIME
	
EXEC creds.spCredentialEntity_Derived_Exists
	@CredentialEntityTypeID = 1,
	@BusinessEntityID = 92,
	@DerivedCredentialKey = 'dhughes',
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@IsLockedOut = @IsLockedOut OUTPUT,
	@DateLockOutExpires = @DateLockOutExpires OUTPUT

SELECT @CredentialEntityID AS CredentialEntityID, @IsLockedOut AS IsLockedOut, @DateLockOutExpires AS DateLockOutExpires

*/

-- SELECT * FROM creds.CredentialEntity
-- SELECT * FROM creds.CredentialEntityType
-- SELECT * FROM creds.ExtranetUser
-- SELECT * FROM creds.Membership
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntity_Derived_Exists]
	@CredentialEntityTypeID INT,
	@BusinessEntityID BIGINT,
	@Domain VARCHAR(256) = NULL,
	@DerivedCredentialKey VARCHAR(256),
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------
	SET @CredentialEntityID = NULL;
	SET @Exists = 0;
	SET @IsDisabled = 0;
	
	----------------------------------------------------------
	-- Local variables
	----------------------------------------------------------
	DECLARE
		@CredentialEntityTypeCode VARCHAR(50) = creds.fnGetCredentialEntityTypeCode(@CredentialEntityTypeID),
		@ErrorMessage VARCHAR(4000)
		
	
	----------------------------------------------------------------------
	-- Check for null arguments
	-- <Summary>
	-- Checks critical inputs for null argument exceptions.
	-- The following pieces are required to interrogate credential entities.
	-- </Summary>
	----------------------------------------------------------------------	
	----------------------------------------------------------------------
	-- Argument is null exception: @CredentialEntityTypeID
	----------------------------------------------------------------------
	IF @CredentialEntityTypeID IS NULL
	BEGIN
		
		SET @ErrorMessage = 'Unable to interrogate credential types. Exception: Object reference (@CredentialEntityTypeID) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END
	
	----------------------------------------------------------------------
	-- Argument is null exception: @BusinessEntityID
	----------------------------------------------------------------------
	IF @BusinessEntityID IS NULL
	BEGIN
			
		SET @ErrorMessage = 'Unable to interrogate credential types. Exception: Object reference (@BusinessEntityID) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END
	
	
	----------------------------------------------------------
	-- Interrogate credential entity type code and determine
	-- which lookup needs to be performed.
	----------------------------------------------------------
	
	----------------------------------------------------------
	-- If the credential belongs to the extranet group
	-- then we need to ensure we do not add the same user
	----------------------------------------------------------
	IF @CredentialEntityTypeCode = 'EXTRANET'
	BEGIN
	
		SELECT TOP 1
			@CredentialEntityID = ent.CredentialEntityID,
			@IsLockedOut = mem.IsLockedOut,
			@DateLockOutExpires = mem.DateLockOutExpires,
			@IsDisabled = mem.IsDisabled
		--SELECT *
		FROM creds.CredentialEntity ent
			JOIN creds.ExtranetUser usr
				ON ent.CredentialEntityID = usr.CredentialEntityID
			LEFT JOIN creds.Membership mem
				ON ent.CredentialEntityID = mem.CredentialEntityID
		WHERE ent.BusinessEntityID = @BusinessEntityID
			AND ent.CredentialEntityTypeID = @CredentialEntityTypeID
			AND usr.Username = @DerivedCredentialKey
		
		
		IF @CredentialEntityID IS NOT NULL
		BEGIN
			SET @Exists = 1;
		END			
	
	
		RETURN;
	
	END
	
	----------------------------------------------------------
	-- If the credential belongs to the network group
	-- then we need to ensure we do not add the same user
	----------------------------------------------------------
	IF @CredentialEntityTypeCode = 'NETWORK'
	BEGIN
	
		SELECT TOP 1 
			@CredentialEntityID = ent.CredentialEntityID,
			@IsLockedOut = mem.IsLockedOut,
			@DateLockOutExpires = mem.DateLockOutExpires
		--SELECT *
		FROM creds.CredentialEntity ent
			JOIN creds.NetworkUser usr
				ON ent.CredentialEntityID = usr.CredentialEntityID
			LEFT JOIN creds.Membership mem
				ON ent.CredentialEntityID = mem.CredentialEntityID
		WHERE ent.BusinessEntityID = @BusinessEntityID
			AND ent.CredentialEntityTypeID = @CredentialEntityTypeID
			AND usr.Domain = @Domain
			AND usr.Username = @DerivedCredentialKey
		
		
		IF @CredentialEntityID IS NOT NULL
		BEGIN
			SET @Exists = 1;
		END			
	
	
		RETURN;
		
	END
	
	----------------------------------------------------------
	-- If the credential belongs to the workgoup
	-- then we need to ensure we do not add the same user
	----------------------------------------------------------
	IF @CredentialEntityTypeCode = 'WORKGROUP'
	BEGIN
	
		SELECT TOP 1 
			@CredentialEntityID = ent.CredentialEntityID,
			@IsLockedOut = mem.IsLockedOut,
			@DateLockOutExpires = mem.DateLockOutExpires
		--SELECT *
		FROM creds.CredentialEntity ent
			JOIN creds.Workgroup usr
				ON ent.CredentialEntityID = usr.CredentialEntityID
			LEFT JOIN creds.Membership mem
				ON ent.CredentialEntityID = mem.CredentialEntityID
		WHERE ent.BusinessEntityID = @BusinessEntityID
			AND ent.CredentialEntityTypeID = @CredentialEntityTypeID
			AND usr.Name = @DerivedCredentialKey
		
		
		IF @CredentialEntityID IS NOT NULL
		BEGIN
			SET @Exists = 1;
		END			
	
	
		RETURN;
		
	END	
	
	
END
