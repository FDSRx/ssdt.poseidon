﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/8/2014
-- Description:	Creates a lock on a single or all applicable records.
-- SAMPLE CALL:
/*

DECLARE 
	@IsLocked BIT = NULL
	
EXEC creds.spMembership_IsLockedOut
	@CredentialEntityID = 801018,
	@IsLockedOut = @IsLocked OUTPUT

SELECT @IsLocked AS IsLocked

EXEC creds.spMembership_LockOut
	@CredentialEntityID = 801018,
	@ModifiedBy = 'dhughes'

EXEC creds.spMembership_Unlock
	@CredentialEntityID = 801018,
	@ForceUnlock = 1,
	@ModifiedBy = 'dhughes'
	
*/

-- SELECT * FROM creds.Membership
-- SELECT * FROM creds.vwExtranetUser WHERE BusinessEntityID = 10684
-- SELECT * FROM dbo.Configuration
-- SELECT * FROM dbo.EventLog
-- =============================================
CREATE PROCEDURE [creds].[spMembership_LockOut]
	@MembershipID BIGINT = NULL,
	@CredentialEntityID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@Username VARCHAR(256) = NULL,
	@CredentialEntityTypeID INT = NULL,
	@ForceLock BIT = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@IsLockedOut BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@MembershipID=' + dbo.fnToStringOrEmpty(@MembershipID) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@CredentialEntityTypeID=' + dbo.fnToStringOrEmpty(@CredentialEntityTypeID) + ';'

	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	----------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------
	SET @ForceLock = ISNULL(@ForceLock, 0);
	SET @IsLockedOut = 0;
	
	----------------------------------------------------------------------------------
	-- Declare variables.
	----------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@TranDate DATETIME = GETDATE(),
		@LockOutTimeMin INT = ISNULL(dbo.fnGetFirstOrDefaultConfigValue(@ApplicationID, @BusinessID, 'CredentialLockOutTimeMin'), 20),
		@MaxAttempts INT = ISNULL(dbo.fnGetFirstOrDefaultConfigValue(@ApplicationID, @BusinessID, 'CredentialLockOutMaxAttempts'), 3),
		@DateLockOutExpires DATETIME,
		@IsPreExistingLockOut BIT = 0,
		@Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	
	/*
	----------------------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules.
	-- </Summary>
	----------------------------------------------------------------------------------
	-- A tag name is required to be created.
	IF @MembershipID IS NULL AND @CredentialEntityID IS NULL AND ( @BusinessID IS NULL OR @Username IS NULL OR @CredentialEntityID IS NULL )
	BEGIN
	
		SET @ErrorMessage = 'Unable to retrieve Membership entry.  Object references ' +
			'("@MembershipID" or "@CredentialEntityID" or "@BusinessID and @Username and @CredentialEntityID") are not set to an instance of an object. ' +
			'The "@MembershipID" or "@CredentialEntityID" or "@BusinessID and @Username and @CredentialEntityID" cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	*/

	----------------------------------------------------------------------------------
	-- Set variables.
	----------------------------------------------------------------------------------
	SET @DateLockOutExpires = DATEADD(MI, @LockOutTimeMin, GETDATE());
	
	----------------------------------------------------------------------------------
	-- Interrogate provided identifiers and determine the proper channel of
	-- identification.
	-- <Summary>
	-- Inspects credential indentification keys and determines how to access the
	-- membership record.
	-- </Summary>
	----------------------------------------------------------------------------------
	IF @MembershipID IS NULL AND @CredentialEntityID IS NULL
	BEGIN
		----------------------------------------------------------------------------------
		-- Fetch credential entity
		-- <Summary>
		-- Interrogates the credentials for the provided type
		-- to determine if the supplied credentials meets the required
		-- criteria to be authenticated.
		-- </Summary>
		----------------------------------------------------------------------------------
		EXEC creds.spCredentialEntity_Derived_Exists
			@CredentialEntityTypeID = @CredentialEntityTypeID,
			@BusinessEntityID = @BusinessID,
			@DerivedCredentialKey = @Username,
			@CredentialEntityID = @CredentialEntityID OUTPUT
	END
	
	----------------------------------------------------------------------------------
	-- Interrogate unique keys and determine winner.
	-- <Summary>
	-- If the primary record key is supplied (i.e. the surrogate key)
	-- then declare that key the winner and void the foreign key; 
	-- otherwise, keep the foreign unique key.
	-- </Summary>
	----------------------------------------------------------------------------------
	IF @MembershipID IS NOT NULL
	BEGIN
		SET @CredentialEntityID = NULL;
	END
	
	-- Debug
	--SELECT @MembershipID AS MembershipID, @CredentialEntityID AS CredentialEntityID, @ApplicationID AS ApplicationID, @BusinessID AS BusinessID,
	--	@Username AS Username, @ForceLock AS ForceLock, @MaxAttempts AS MaxAttempts, @DateLockOutExpires AS DateLockOutExpires,
	--	@IsPreExistingLockOut AS IsPrexistingLockOut
	
	
	----------------------------------------------------------------------------------
	-- Lock record(s)
	-- <Summary>
	-- If a specific record was provided, then lock the specific record; otherwise,
	-- attempt a lock on all applicable records.
	-- </Summary>
	----------------------------------------------------------------------------------
	IF @MembershipID IS NOT NULL OR @CredentialEntityID IS NOT NULL
	BEGIN
		
		DECLARE @tblOutput AS TABLE (
			MembershipID BIGINT,
			CredentialEntityID BIGINT,
			IsLockedOut BIT
		);
		
		-- Specific record lock.
		UPDATE mem
		SET
			IsLockedOut = 1,
			DateLastLockedOut = GETDATE(),
			DateLockOutExpires = @DateLockOutExpires,
			DateModified = GETDATE(),
			ModifiedBy = @ModifiedBy
		--SELECT *
		OUTPUT deleted.MembershipID, deleted.CredentialEntityID, deleted.IsLockedOut 
			INTO @tblOutput(MembershipID, CredentialEntityID, IsLockedOut)
		FROM creds.Membership mem
		WHERE ( MembershipID = @MembershipID OR
			CredentialEntityID = @CredentialEntityID
		)
			AND ( FailedPasswordAttemptCount > @MaxAttempts
				OR @ForceLock = 1 )
		
		IF @@ROWCOUNT > 0
		BEGIN
			SET @IsLockedOut = 1;
			
			SET @IsPreExistingLockOut = ISNULL((SELECT TOP 1 IsLockedOut FROM @tblOutput), 0);
			
			----------------------------------------------------------------------------------
			-- Record lockout event if it is the first occurrence
			-- <Summary>
			-- A lockout can occur multiple times due to a person continuing to try
			-- and validate their credentials after a lockout has been initiated. Only
			-- record the first instance of the lockout so that continuing lockouts are not
			-- tracked. Lockouts will keep being updated, as the lockout expiry time needs to
			-- be refreshed for every attempt.
			-- </Summary>
			----------------------------------------------------------------------------------
			IF @IsPreExistingLockOut = 0
			BEGIN
			
				---------------------------------------------------------
				-- Record event.  Do not fail the process if the event was
				-- unable to be recorded.
				---------------------------------------------------------
				BEGIN TRY
				
					DECLARE @EventID INT = creds.fnGetCredentialEventID('LO');
					
					DECLARE @EventData XML = 
						'<CredentialEventData>' +
							dbo.fnXmlWriteElementString('MembershipID', @MembershipID) +
							dbo.fnXmlWriteElementString('CredentialEntityID', @CredentialEntityID) +
							dbo.fnXmlWriteElementString('CredentialEntityTypeID', @CredentialEntityTypeID) +
							dbo.fnXmlWriteElementString('ApplicationID', @ApplicationID) +
							dbo.fnXmlWriteElementString('BusinessID', @BusinessID) +
							dbo.fnXmlWriteElementString('Username', @Username) +
							dbo.fnXmlWriteElementString('ForceLock', @ForceLock) +
						'</CredentialEventData>';
					
					EXEC creds.spCredentialEventLog_Create
						@EventID = @EventID,
						@EventSource = @Procedure,
						@EventDataXml = @EventData,
						@CreatedBy = @ModifiedBy
				
				END TRY
				BEGIN CATCH		
				END CATCH
				
			END		
				
		END
	
	END
	ELSE
	BEGIN
		
		-- Lock all applicable records.
		UPDATE mem
		SET
			IsLockedOut = 1,
			DateLastLockedOut = GETDATE(),
			DateLockOutExpires = @DateLockOutExpires,
			DateModified = GETDATE(),
			ModifiedBy = @ModifiedBy
		--SELECT *
		FROM creds.Membership mem
		WHERE ( FailedPasswordAttemptCount > @MaxAttempts
				OR @ForceLock = 1 )
		
		
	END
	

END
