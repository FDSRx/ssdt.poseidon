﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/4/2014
-- Description:	Updates a membership record
-- SAMPLE CALL:
/*

DECLARE 
	@MembershipID BIGINT = NULL,
	@CredentialEntityID BIGINT = 801050,
	@EmailAddress VARCHAR(256) = 'dhughes@rxlps.com',
	@Password VARCHAR(256) = NULL,
	@PasswordQuestionID INT = NULL,
	@IsApproved BIT = NULL,
	@IsLockedOut BIT = NULL,
	@DateLastLoggedIn DATETIME = NULL,
	@DateLastPasswordChanged DATETIME = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@DateLastLockedOut DATETIME = NULL,
	@DateLockOutExpires DATETIME = NULL,
	@FailedPasswordAttemptCount INT = NULL,
	@FailedPasswordAnswerAttemptCount INT = NULL,
	@IsDisabled BIT = NULL,
	@Comment VARCHAR(MAX) = NULL,
	@IsChangePasswordRequired BIT = NULL,
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@Debug BIT = 1
	
EXEC creds.spMembership_Update
	@MembershipID = @MembershipID,
	@CredentialEntityID = @CredentialEntityID,
	@EmailAddress = @EmailAddress,
	@Password = @Password,
	@PasswordQuestionID = @PasswordQuestionID,
	@IsApproved = @IsApproved,
	@IsLockedOut = @IsLockedOut,
	@DateLastLoggedIn = @DateLastLoggedIn,
	@DateLastPasswordChanged = @DateLastPasswordChanged,
	@DatePasswordExpires = @DatePasswordExpires,
	@DateLastLockedOut = @DateLastLockedOut,
	@DateLockOutExpires = @DateLockOutExpires,
	@FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
	@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
	@IsDisabled = @IsDisabled,
	@Comment = @Comment,
	@IsChangePasswordRequired = @IsChangePasswordRequired,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

	

*/

-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.Membership WHERE CredentialEntityID = 801050

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [creds].[spMembership_Update]
	@MembershipID BIGINT = NULL,
	@CredentialEntityID BIGINT = NULL,
	@EmailAddress VARCHAR(256) = NULL,
	@Password VARCHAR(256) = NULL,
	@PasswordQuestionID INT = NULL,
	@IsApproved BIT = NULL,
	@IsLockedOut BIT = NULL,
	@DateLastLoggedIn DATETIME = NULL,
	@DateLastPasswordChanged DATETIME = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@DateLastLockedOut DATETIME = NULL,
	@DateLockOutExpires DATETIME = NULL,
	@FailedPasswordAttemptCount INT = NULL,
	@FailedPasswordAnswerAttemptCount INT = NULL,
	@IsDisabled BIT = NULL,
	@Comment VARCHAR(MAX) = NULL,
	@IsChangePasswordRequired BIT = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	---------------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()
	
	DECLARE @Args VARCHAR(MAX) =	
		'@MembershipID=' + dbo.fnToStringOrEmpty(@MembershipID) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@EmailAddress=' + dbo.fnToStringOrEmpty(@EmailAddress) + ';' +
		'@Password=' + dbo.fnToStringOrEmpty(@Password) + ';' +
		'@PasswordQuestionID=' + dbo.fnToStringOrEmpty(@PasswordQuestionID) + ';' +
		'@IsApproved=' + dbo.fnToStringOrEmpty(@IsApproved) + ';' +
		'@IsLockedOut=' + dbo.fnToStringOrEmpty(@IsLockedOut) + ';' +
		'@DateLastLoggedIn=' + dbo.fnToStringOrEmpty(@DateLastLoggedIn) + ';' +
		'@DateLastPasswordChanged=' + dbo.fnToStringOrEmpty(@DateLastPasswordChanged) + ';' +
		'@DatePasswordExpires=' + dbo.fnToStringOrEmpty(@DatePasswordExpires) + ';' +
		'@DateLastLockedOut=' + dbo.fnToStringOrEmpty(@DateLastLockedOut) + ';' +
		'@DateLockOutExpires=' + dbo.fnToStringOrEmpty(@DateLockOutExpires) + ';' +
		'@FailedPasswordAttemptCount=' + dbo.fnToStringOrEmpty(@FailedPasswordAttemptCount) + ';' +
		'@FailedPasswordAnswerAttemptCount=' + dbo.fnToStringOrEmpty(@FailedPasswordAnswerAttemptCount) + ';' +
		'@IsDisabled=' + dbo.fnToStringOrEmpty(@IsDisabled) + ';' +
		'@Comment=' + dbo.fnToStringOrEmpty(@Comment) + ';' +
		'@IsChangePasswordRequired=' + dbo.fnToStringOrEmpty(@IsChangePasswordRequired) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	---------------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	---------------------------------------------------------------------------------------------------------------------------------
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/	
				
	---------------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-- <Summary>
	-- Cleans input variables and ensures the output variables are
	-- properly set upon retrieval.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------------------------------
	SET @Comment = CASE WHEN dbo.fnIsNullOrWhiteSpace(@Comment) = 1 THEN NULL ELSE @Comment END;
	SET @IsChangePasswordRequired = ISNULL(@IsChangePasswordRequired, 0);
	SET @ModifiedBy = ISNULL(@ModifiedBy, SUSER_NAME());
	SET @Debug = ISNULL(@Debug, 0);
	
	---------------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------------------------------------------------------------------
	-- No local variables required.

	---------------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	---------------------------------------------------------------------------------------------------------------------------------
	-- No initialization necessary.
		
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@CredentialEntityID AS CredentialEntityID, @Password AS Password, @Args AS Arguments, @Comment AS Comment,
			@IsChangePasswordRequired AS IsChangePasswordRequired
	END
		
	---------------------------------------------------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------------------------------
	IF @CredentialEntityID IS NULL AND @MembershipID IS NULL
	BEGIN
		
		SET @ErrorMessage = 'Unable to update the Membership object. ' +
			'Object reference (@CredentialEntityID or @MembershipID) are not set to an instance of an object. ' +
			'A CredentialEntityID or MembershipID is required.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
	END
	
	---------------------------------------------------------------------------------------------------------------------------------
	-- Void CredentialEntityID if the MembershipID was provided.
	---------------------------------------------------------------------------------------------------------------------------------
	IF @MembershipID IS NOT NULL
	BEGIN
		SET @CredentialEntityID = NULL;
	END
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Record key validation.' AS ActionMethod,
			@MembershipID AS MembershipID, @CredentialEntityID AS CredentialEntityID, @EmailAddress AS EmailAddress
	END

	---------------------------------------------------------------------------------------------------------------------------------
	-- Determine if the password is required to be changed upon login.
	-- <Summary>
	-- Inspects the force password change flag to determine if the password needs to be set to
	-- expire immediately; otherwise, use the data from DatePasswordExpires parameter.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------------------------------
	IF @IsChangePasswordRequired = 1 AND dbo.fnIsNullOrWhitespace(@Password) = 1
	BEGIN
		SET @DatePasswordExpires = GETDATE();
	END

	---------------------------------------------------------------------------------------------------------------------------------
	-- Change password.
	-- <Summary>
	-- If a password is provided then let's change the password.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------------------------------
	IF dbo.fnIsNullOrWhitespace(@Password) = 0
	BEGIN
		EXEC creds.spMembership_Password_Update
			@CredentialEntityID = @CredentialEntityID,
			@Password = @Password,
			@DatePasswordExpires = @DatePasswordExpires,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug
	END
			
	---------------------------------------------------------------------------------------------------------------------------------
	-- Update credential membership record.
	-- <Summary>
	-- Updates the Membership record with the provided values.  If any of the provided
	-- values are null, then the existing data will remain intact.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------------------------------
	UPDATE mem
	SET 
		mem.EmailAddress = CASE WHEN @EmailAddress IS NULL THEN mem.EmailAddress ELSE @EmailAddress END,
		mem.IsApproved = CASE WHEN @IsApproved IS NULL THEN mem.IsApproved ELSE @IsApproved END,
		mem.IsLockedOut = CASE WHEN @IsLockedOut IS NULL THEN mem.IsLockedOut ELSE @IsLockedOut END,
		mem.DateLastLoggedIn = CASE WHEN @DateLastLoggedIn IS NULL THEN mem.DateLastLoggedIn ELSE @DateLastLoggedIn END,
		mem.DateLastPasswordChanged = CASE WHEN @DateLastPasswordChanged IS NULL THEN mem.DateLastPasswordChanged ELSE @DateLastPasswordChanged END,
		mem.DatePasswordExpires = CASE WHEN @DatePasswordExpires IS NULL THEN mem.DatePasswordExpires ELSE @DatePasswordExpires END,
		mem.DateLastLockedOut = CASE WHEN @DateLastLockedOut IS NULL THEN mem.DateLastLockedOut ELSE @DateLastLockedOut END,
		mem.FailedPasswordAttemptCount = CASE WHEN @FailedPasswordAttemptCount IS NULL THEN mem.FailedPasswordAttemptCount ELSE @FailedPasswordAttemptCount END,
		mem.FailedPasswordAnswerAttemptCount = CASE WHEN @FailedPasswordAnswerAttemptCount IS NULL THEN mem.FailedPasswordAnswerAttemptCount ELSE @FailedPasswordAnswerAttemptCount END,
		mem.IsDisabled = CASE WHEN @IsDisabled IS NULL THEN mem.IsDisabled ELSE @IsDisabled END,
		mem.Comment = CASE WHEN @Comment IS NULL THEN mem.Comment ELSE @Comment END,
		mem.DateModified = GETDATE(),
		mem.ModifiedBy = CASE WHEN @ModifiedBy IS NULL THEN mem.ModifiedBy ELSE @ModifiedBy END
	--SELECT *
	FROM creds.Membership mem
	WHERE ( @MembershipID IS NOT NULL AND MembershipID = @MembershipID )
		 OR ( @MembershipID IS NULL AND @CredentialEntityID IS NOT NULL AND CredentialEntityID = @CredentialEntityID )
	

	
	
END
