﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/31/2014
-- Description:	Indicates whether the specified token is active or expired.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityID BIGINT = NULL,
	@Token VARCHAR(50) = '02A53E12-0494-460B-824B-32107B6CF348',
	@IsExpired BIT,
	@DateExpires DATETIME,
	@Exists BIT = NULL
	
EXEC creds.spCredentialToken_IsActive
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@Token = @Token OUTPUT,
	@IsExpired = @IsExpired OUTPUT,
	@DateExpires = @DateExpires OUTPUT,
	@Exists = @Exists OUTPUT
	,@InvalidateDataOnExpired = 1

SELECT @CredentialEntityID AS CredentialEntityID, @Token AS Token, @IsExpired AS IsExpired, @DateExpires AS DateExpires, @Exists AS IsFound

*/

-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
-- SELECT * FROM dbo.TokenType
-- =============================================
CREATE PROCEDURE [creds].[spCredentialToken_IsActive]
	@ApplicationID INT = NULL,
	@BusinessID INT = NULL,
	@TokenTypeID INT = NULL,
	@Token VARCHAR(50) OUTPUT,
	@InvalidateDataOnExpired BIT = NULL,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@IsExpired BIT = NULL OUTPUT,
	@DateExpires DATETIME = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------
	SET @IsExpired = 0;	
	SET @Exists = 0;
	SET @InvalidateDataOnExpired = ISNULL(@InvalidateDataOnExpired, 0);

	---------------------------------------------------------------------------------------
	-- Determine token validity.
	---------------------------------------------------------------------------------------
	SELECT
		@CredentialEntityID = CredentialEntityID,
		@Token = Token,
		@IsExpired = CASE WHEN DateExpires > GETDATE() THEN 0 ELSE 1 END,
		@DateExpires = DateExpires,
		@Exists = 1
	FROM creds.CredentialToken
	WHERE Token = @Token
		AND ( @ApplicationID IS NULL OR ApplicationID = @ApplicationID )
		AND ( @BusinessID IS NULL OR BusinessEntityID = @BusinessID )
		AND ( @TokenTypeID IS NULL OR TokenTypeID = @TokenTypeID )
	
	-- If the token was unable to be discovered, then void it and show that it is expired.
	IF @@ROWCOUNT = 0
	BEGIN
		SET @IsExpired = 1;	
		SET @DateExpires = GETDATE();		
	END
	
	-- Void token data if it is expired.
	IF @CredentialEntityID IS NOT NULL AND @IsExpired = 1 AND @InvalidateDataOnExpired = 1
	BEGIN
		SET @Token = NULL;	
		SET @CredentialEntityID = NULL;
	END
	
		
END
