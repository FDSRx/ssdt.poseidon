﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Returns the credential/user Membership password and account settings.
-- SAMPLE CALL:
/* 
DECLARE 
	@Password VARBINARY(128), 
	@Salt VARBINARY(10),
	@PasswordQuestionID INT,
	@PasswordAnswerHash VARBINARY(128),
	@PasswordAnswerSalt VARBINARY(10),
	@IsApproved BIT = NULL,
	@IsLockedOut BIT = NULL,
	@DateLastLoggedIn DATETIME = NULL,
	@DateLastPasswordChanged DATETIME = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@DateLastLockedOut DATETIME = NULL,
	@DateLockOutExpires DATETIME = NULL,
	@FailedPasswordAttemptCount INT = NULL,
	@FailedPasswordAnswerAttemptCount INT = NULL,
	@IsDisabled BIT = NULL

EXEC creds.spMembership_Password_Get 
	@CredentialEntityID = 801050, 
	@PasswordHash = @Password OUTPUT, 
	@PasswordSalt = @Salt OUTPUT,
	@PasswordAnswerHash = @PasswordAnswerHash OUTPUT,
	@PasswordAnswerSalt = @PasswordAnswerSalt OUTPUT,
	@IsApproved = @IsApproved OUTPUT,
	@IsLockedOut = @IsLockedOut OUTPUT,
	@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
	@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
	@DatePasswordExpires = @DatePasswordExpires OUTPUT,
	@DateLastLockedOut = @DateLastLockedOut OUTPUT,
	@DateLockOutExpires = @DateLockOutExpires OUTPUT,
	@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
	@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
	@IsDisabled = @IsDisabled OUTPUT
	
SELECT @Password AS PasswordHash, @Salt AS PasswordSalt, @PasswordQuestionID AS PasswordQuestionID,
	@PasswordAnswerHash AS PasswordAnswerHash, @PasswordAnswerSalt AS PasswordAnswerHash,
	@IsApproved AS IsApproved, @IsLockedOut AS IsLockedOut, @DateLastLoggedIn AS DateLastLoggedIn,
	@DateLastPasswordChanged AS DateLastPasswordChanged, @DatePasswordExpires AS DatePasswordExpires, @DateLastLockedOut AS DateLastLockedOut,
	@DateLockOutExpires AS DateLockOutExpires, @FailedPasswordAttemptCount AS FailedPasswordAttemptCount, 
	@FailedPasswordAnswerAttemptCount AS FailedPasswordAnswerAttemptCount, @IsDisabled AS IsDisabled
	
*/

-- SELECT * FROM creds.Membership
-- =============================================
CREATE PROCEDURE [creds].[spMembership_Password_Get]
	@MembershipID BIGINT = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@EmailAddress VARCHAR(256) = NULL OUTPUT,
	@PasswordHash VARBINARY(128) = NULL OUTPUT,
	@PasswordSalt VARBINARY(10) = NULL OUTPUT,
	@PasswordQuestionID INT = NULL OUTPUT,
	@PasswordAnswerHash VARBINARY(128) = NULL OUTPUT,
	@PasswordAnswerSalt VARBINARY(10) = NULL OUTPUT,
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------
	SET @EmailAddress = NULL;
	SET @PasswordHash = NULL;
	SET @PasswordSalt = NULL;
	SET @PasswordQuestionID = NULL;
	SET @PasswordAnswerHash = NULL;
	SET @IsApproved = 0;
	SET	@IsLockedOut = 0;
	SET	@DateLastLoggedIn = NULL;
	SET	@DateLastPasswordChanged = NULL;
	SET @DatePasswordExpires = NULL;
	SET	@DateLastLockedOut = NULL;
	SET @DateLockOutExpires = NULL;
	SET	@FailedPasswordAttemptCount = 0;
	SET	@FailedPasswordAnswerAttemptCount = 0;
	SET @IsDisabled = 0;

	------------------------------------------------------------------------------------------
	-- If a MembershipID was provided, then do not use the CredentialEntityID.
	------------------------------------------------------------------------------------------	
	IF @MembershipID IS NOT NULL
	BEGIN
		SET @CredentialEntityID = NULL;
	END
	
	------------------------------------------------------------------------------------------
	-- Fetch password data.
	------------------------------------------------------------------------------------------
	SELECT TOP 1
		@MembershipID = MembershipID,
		@CredentialEntityID = CredentialEntityID,
		@PasswordHash = PasswordHash,
		@PasswordSalt = PasswordSalt,
		@PasswordQuestionID = PasswordQuestionID,
		@PasswordAnswerHash = PasswordAnswerHash,
		@PasswordAnswerSalt = PasswordAnswerSalt,
		@IsApproved = IsApproved,
		@IsLockedOut = IsLockedOut,
		@DateLastLoggedIn = DateLastLoggedIn,
		@DateLastPasswordChanged = DateLastPasswordChanged,
		@DatePasswordExpires = DatePasswordExpires,
		@DateLastLockedOut = DateLastLockedOut,
		@DateLockOutExpires = DateLockOutExpires,
		@FailedPasswordAttemptCount = FailedPasswordAttemptCount,
		@FailedPasswordAnswerAttemptCount = FailedPasswordAnswerAttemptCount,
		@IsDisabled = IsDisabled
	--SELECT *
	FROM creds.Membership
	WHERE MembershipID = @MembershipID
		OR CredentialEntityID = @CredentialEntityID
		
	-- If no records where discovered then void the primary keys.
	IF @@ROWCOUNT = 0
	BEGIN
		SET @MembershipID = NULL;
		SET @CredentialEntityID = NULL;
	END
		
END
