﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/26/2015
-- Description:	Translates the CredentialToken object into an OAuthClient object.
/*

DECLARE
	@Token VARCHAR(50) = '96E1F568-287E-4A61-B48B-B3D8231B0A6E', --'D7A33909-9B7D-4EB0-AE38-D2F6B489CBC9',
	@ValidateTokenExpiration BIT = 0,
	@ClientID NVARCHAR(256) = NULL,
	@SecretKey NVARCHAR(256) = NULL,
	@IsValid BIT = NULL

EXEC creds.spCredentialToken_TranslateToOAuthClient
	@Token = @Token,
	@ValidateTokenExpiration = @ValidateTokenExpiration,
	@ClientID = @ClientID OUTPUT,
	@SecretKey = @SecretKey OUTPUT,
	@IsValid = @IsValid OUTPUT

SELECT @IsValid AS IsValid, @ClientID AS ClientID, @SecretKey AS SecretKey

-- EXEC creds.spOAuthClient_Delete @ClientID = 'B2F974ED-7979-464F-925C-13B7ABB203F5'
		

*/

-- SELECT * FROM creds.CredentialToken WHERE CredentialEntityID = 960840
-- SELECT * FROM creds.CredentialToken ORDER BY 1 DESC
-- SELECT * FROM creds.OAuthClient
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE creds.spCredentialToken_TranslateToOAuthClient
	@CredentialTokenID BIGINT = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@Token VARCHAR(50) = NULL OUTPUT,
	@TokenData VARCHAR(MAX) = NULL OUTPUT,
	@ClientID NVARCHAR(256) = NULL OUTPUT,
	@SecretKey NVARCHAR(256) = NULL OUTPUT,
	@Name VARCHAR(256) = NULL OUTPUT,
	@ValidateTokenExpiration BIT = NULL,
	@IsValid BIT = NULL OUTPUT,
	@IsOutputOnly BIT = NULL -- Will default to always output.
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CredentialTokenID=' + dbo.fnToStringOrEmpty(@CredentialTokenID) + ';' +
		'@Token=' + dbo.fnToStringOrEmpty(@Token) + ';' +
		'@ClientID=' + dbo.fnToStringOrEmpty(@ClientID) + ';' +
		'@SecretKey=' + dbo.fnToStringOrEmpty(@SecretKey) + ';' +
		'@IsOutputOnly=' + dbo.fnToStringOrEmpty(@IsOutputOnly) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	-------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 1);
	SET @ValidateTokenExpiration = ISNULL(@ValidateTokenExpiration, 0);
	SET @IsValid = 0;

	-------------------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE
		@PersonID BIGINT

	-------------------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------------------
	-- No instantiation.

	-- Debug
	--SELECT @CredentialTokenID AS CredentialTokenID, @CredentialEntityID AS CredentialEntityID, @ApplicationID AS ApplicationID,
	--	@BusinessEntityID AS BusinessEntityID, @Token AS Token

	-------------------------------------------------------------------------------------------------------
	-- Fetch token data.
	-------------------------------------------------------------------------------------------------------
	SELECT TOP 1
		@CredentialTokenID = CredentialTokenID,
		@CredentialEntityID = CredentialEntityID,
		@ApplicationID = ApplicationID,
		@BusinessEntityID = BusinessEntityID,
		@Token = Token,
		@TokenData = CONVERT(VARCHAR(MAX), TokenData),
		@IsValid = CASE WHEN CredentialTokenID IS NOT NULL THEN 1 ELSE 0 END
	--SELECT *
	--SELECT TOP 1 *
	FROM creds.CredentialToken
	WHERE ( @CredentialTokenID IS NOT NULL AND CredentialTokenID = @CredentialTokenID )
		OR (
			@CredentialTokenID IS NULL
			AND Token = @Token
			AND ( @CredentialEntityID IS NULL OR ( @CredentialEntityID IS NOT NULL AND CredentialEntityID = @CredentialEntityID ) )
			AND ( @ApplicationID IS NULL OR ( @ApplicationID IS NOT NULL AND ApplicationID = @ApplicationID ) )
			AND ( @BusinessEntityID IS NULL OR ( @BusinessEntityID IS NOT NULL AND BusinessEntityID = @BusinessEntityID ) )
		)
		-- Validate expiration (if applicable).
		AND ( ( @ValidateTokenExpiration IS NULL OR @ValidateTokenExpiration = 0 ) OR
			DateExpires >= GETDATE() )

	IF @@ROWCOUNT = 0
	BEGIN
		SET @IsValid = 0;
		SET @CredentialTokenID = NULL;
		SET @CredentialEntityID = NULL;
		SET @ApplicationID = NULL;
		SET @BusinessEntityID = NULL;
		SET @Token = NULL;
		SET @TokenData = NULL;
		SET @ClientID = NULL;
		SET @SecretKey = NULL;
	END
	ELSE
	BEGIN
		-- Update the token the way the legacy construct normally would.
		-- NOTE: This ensures backwards compatibility and potentially does not require the use
		-- of the Authorization server JWT (or whichever token construct is being created) for determining
		-- the expiration date of the token.
		EXEC creds.spCredentialToken_Update
			@CredentialTokenID = @CredentialTokenID
	END


	-------------------------------------------------------------------------------------------------------
	-- Retrieve OAuthClient object.
	-------------------------------------------------------------------------------------------------------
	SELECT
		@PersonID = ce.PersonID,
		@ClientID = oa.ClientID,
		@SecretKey = oa.SecretKey,
		@Name = oa.Name
	--SELECT *
	--SELECT TOP 1 *
	FROM creds.OAuthClient oa
		JOIN creds.CredentialEntity ce
			ON oa.CredentialEntityID = ce.CredentialEntityID
	WHERE oa.CredentialEntityID = @CredentialEntityID

	-------------------------------------------------------------------------------------------------------
	-- Create OAuthClient object (if applicable).
	-------------------------------------------------------------------------------------------------------
	IF ISNULL(@IsValid, 0) = 1 
		AND @CredentialEntityID IS NOT NULL
		AND ( @ClientID IS NULL OR @ClientID = '' )
	BEGIN
		-- Create OAuthClient object.
		BEGIN TRY

			-- Try and identifiy the client.
			SET @Name = (
				SELECT TOP 1
					FirstName + 
						CASE 
							WHEN LastName IS NOT NULL THEN ' ' + LastName
							ELSE ''
						END
				FROM dbo.Person
				WHERE BusinessEntityID = @PersonID
			);

			SET @Name = CASE WHEN LEN(@Name) = 0 THEN NULL ELSE @Name END;

			-- Create new OAuthClient object.
			EXEC creds.spOAuthClient_Create
				@CredentialEntityID = @CredentialEntityID,
				@ClientID = @ClientID OUTPUT,
				@SecretKey = @SecretKey OUTPUT,
				@Name = @Name

		END TRY
		BEGIN CATCH

			EXEC dbo.spLogError;

		END CATCH		
	END


	-------------------------------------------------------------------------------------------------------
	-- Return data.
	-------------------------------------------------------------------------------------------------------
	IF @IsOutputOnly = 0
	BEGIN

		SELECT
			@IsValid AS IsValid,
			@CredentialTokenID AS CredentialTokenID,
			@CredentialEntityID AS CredentialEntityID,
			@ApplicationID AS ApplicationID,
			@BusinessEntityID AS BusinessEntityID,
			@Token AS Token,
			@TokenData AS TokenData,
			@ClientID AS ClientID,
			@SecretKey AS SecretKey
	END

END
