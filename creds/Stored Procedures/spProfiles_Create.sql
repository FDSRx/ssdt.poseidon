﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/21/2014
-- Description:	Adds a record to the profiles table
-- SELECT * FROM creds.Profiles
-- =============================================
CREATE PROCEDURE [creds].[spProfiles_Create] 
	@CredentialEntityID BIGINT,
	@PropertyName VARCHAR(256),
	@PropertyValueString VARCHAR(MAX) = NULL,
	@PropertyValueBinary VARBINARY(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ProfileID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize Input
	----------------------------------------------------------------------
	SET @ProfileID = NULL;
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@Arguments VARCHAR(MAX)
		
	----------------------------------------------------------------------
	-- Check for null arguments
	-- <Summary>
	-- Checks critical inputs for null argument exceptions.
	-- The following pieces are required to properly create
	-- a profile setting.
	-- </Summary>
	----------------------------------------------------------------------		
		
	----------------------------------------------------------------------
	-- Argument is null exception: @ApplicationID
	----------------------------------------------------------------------
	IF @CredentialEntityID IS NULL
	BEGIN
			
		SET @ErrorMessage = 'Unable to create profile entry. Exception: Object reference (@CredentialEntityID) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END
	
	IF dbo.fnIsNullOrWhiteSpace(@PropertyName) = 1
	BEGIN
			
		SET @ErrorMessage = 'Unable to create profile entry. Exception: Object reference (@PropertyName) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END
	
	----------------------------------------------------------------------
	-- Create profile entry
	----------------------------------------------------------------------
	INSERT INTO creds.Profiles (
		CredentialEntityID,
		PropertyName,
		PropertyValueString,
		PropertyValueBinary,
		CreatedBy
	)
	SELECT
		@CredentialEntityID,
		@PropertyName,
		@PropertyValueString,
		@PropertyValueBinary,
		@CreatedBy
		
	--Retrieve identity
	SET @ProfileID = SCOPE_IDENTITY();

	
END

