﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/25/2015
-- Description:	Returns a list of Application object(s) that are applicable to the specified credential criteria.
/*

DECLARE
	@CredentialEntityID BIGINT = 960840,
	@ApplicationData XML = NULL

EXEC creds.spCredentialEntityApplication_Get_List
	@CredentialEntityID = @CredentialEntityID,
	@ApplicationData = @ApplicationData OUTPUT,
	@IsOutputOnly = 1,
	@IsResultXml = 0

SELECT @ApplicationData AS ApplicationData

*/

-- SELECT * FROM creds.CredentialEntityApplication
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntityApplication_Get_List]
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@CredentialEntityID BIGINT,
	@IsResultXml BIT = 0,
	@IsOutputOnly BIT = 0,
	@ApplicationData XML = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-----------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@ApplicationData=' + dbo.fnToStringOrEmpty(@ApplicationData) + ';' ;
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	----------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------------------------------
	SET @ApplicationData = NULL;
	SET @IsResultXml = ISNULL(@IsResultXml, 0);
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);


	----------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	----------------------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpApplications') IS NOT NULL
	BEGIN
		DROP TABLE #tmpApplications;
	END

	CREATE TABLE #tmpApplications (
		ApplicationID INT,
		Code VARCHAR(50),
		Name VARCHAR(256),
		Description VARCHAR(1000)
	);


	----------------------------------------------------------------------------------------------------------
	-- Fetch data.
	----------------------------------------------------------------------------------------------------------
	INSERT INTO #tmpApplications (
		ApplicationID,
		Code,
		Name,
		Description
	)
	SELECT
		ca.ApplicationID AS Id,
		a.Code,
		a.Name,
		a.Description
	--SELECT *
	--SElECT TOP 1 *
	FROM creds.CredentialEntityApplication ca (NOLOCK)
		JOIN dbo.Application a (NOLOCK)
			ON ca.ApplicationID = a.ApplicationID
	WHERE ca.CredentialEntityID = @CredentialentityID



	----------------------------------------------------------------------------------------------------------
	-- Return data.
	----------------------------------------------------------------------------------------------------------
	-- Convert the data into a well-formatted xml string.
	SET @ApplicationData = (
		SELECT
			ApplicationID AS Id,
			Code,
			Name,
			Description
		--SELECT *
		--SElECT TOP 1 *
		FROM #tmpApplications
		FOR XML PATH('Application'), ROOT('Applications')
	);

	-- If the data is to be a true result set, then return the data as it normally would be returned.
	IF ISNULL(@IsOutputOnly, 0) = 0
	BEGIN

		IF ISNULL(@IsResultXml, 0) = 0
		BEGIN
			SELECT
				ApplicationID AS Id,
				Code,
				Name,
				Description
			--SELECT *
			--SElECT TOP 1 *
			FROM #tmpApplications
		END
		ELSE
		BEGIN
			SELECT @ApplicationData AS ApplicationData
		END

	END	


END
