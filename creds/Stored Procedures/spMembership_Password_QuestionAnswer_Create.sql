﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Update person password
-- SAMPLE CALL: 
/*

creds.spMembership_Password_QuestionAnswer_Create 
	@CredentialEntityID = 2, 
	@PasswordQuestionID = 1,
	@PasswordAnswer = 'test1' -- valid call
	
*/
-- SELECT * FROM creds.Membership
-- =============================================
CREATE PROCEDURE [creds].[spMembership_Password_QuestionAnswer_Create]
	@CredentialEntityID BIGINT,
	@PasswordQuestionID INT,
	@PasswordAnswer VARCHAR(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------
	-- Local variables
	---------------------------------------------------
	DECLARE
		@Hash VARBINARY(128),
		@Salt VARBINARY(10),
		@HasQuestion BIT = 0,
		@MembershipExists BIT = 0,
		@MembershipID INT,
		@ErrorMessage VARCHAR(4000)
		

	---------------------------------------------------------
	-- Salt and Hash clear text
	---------------------------------------------------------
	SET @Salt = dbo.fnPasswordSalt(RAND());
	SET @Hash = dbo.fnPasswordHash(@PasswordAnswer + CONVERT(VARCHAR(MAX), @Salt));	
	
	---------------------------------------------------------
	-- Verify a membership reocrd exists
	-- <Summary>
	-- Determines if a membership record exists for the specified
	-- credential.  If the record does not exist,
	-- then a new record needs to be created with the requested
	-- security context.
	-- </Summary>
	---------------------------------------------------------
	EXEC creds.spMembership_Exists
		@CredentialEntityID = @CredentialEntityID,
		@MembershipID = @MembershipID,
		@Exists = @MembershipExists OUTPUT
	
	
	---------------------------------------------------------
	-- A membership record does not exist for the supplied 
	-- credential, so let's create one.
	---------------------------------------------------------
	IF ISNULL(@MembershipExists, 0) = 0
	BEGIN
	
		EXEC creds.spMembership_Create
			@CredentialEntityID = @CredentialEntityID,
			@PasswordQuestionID = @PasswordQuestionID,
			@PasswordAnswer = @PasswordAnswer
		
		
		RETURN;
		
	END
	ELSE
	BEGIN		
		---------------------------------------------------------
		-- A membership record exists; let's interrogate and determine 
		-- action.
		---------------------------------------------------------
		DECLARE 
			@CurrentPasswordQuestionID INT
		
		EXEC creds.spMembership_Password_QuestionAnswer_Get
			@CredentialEntityID = @CredentialEntityID,
			@PasswordQuestionID = @CurrentPasswordQuestionID OUTPUT
		
		IF @CurrentPasswordQuestionID IS NULL
		BEGIN
		
			---------------------------------------------------------
			-- Update (Create) user password security question
			---------------------------------------------------------
			UPDATE pwd
			SET
				pwd.PasswordQuestionID = @PasswordQuestionID,
				pwd.PasswordAnswerHash = @Hash,
				pwd.PasswordAnswerSalt = @Salt
			--SELECT *
			FROM creds.Membership pwd
			WHERE CredentialEntityID = @CredentialEntityID
			
		END
		ELSE
		BEGIN
		
			---------------------------------------------------------------
			-- Security question already exists. Do not update.
			-- Fail procedure to caller of attempted data modification.
			---------------------------------------------------------------			
			SET @ErrorMessage = 'Unable to create a membership password security question and answer for CredentialEntityID:  ' + ISNULL(CONVERT(VARCHAR, @CredentialEntityID), '<Empty Object>') 
				+ '. Exception: The membership has an existing password security question associated with the account.'
				
			RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
			
			RETURN;		

		
		END
		

		



	END
	
END
