﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/31/2013
-- Description:	Validates the credentials that are necessary to be considered an
--	authenticated extranet user.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityID BIGINT,
	@CredentialToken VARCHAR(50),
	@IsValid BIT = NULL,
	@IsApproved BIT = NULL,
	@IsLockedOut BIT = NULL,
	@DateLastLoggedIn DATETIME = NULL,
	@DateLastPasswordChanged DATETIME = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@DateLastLockedOut DATETIME = NULL,
	@DateLockOutExpires DATETIME = NULL,
	@FailedPasswordAttemptCount INT = NULL,
	@FailedPasswordAnswerAttemptCount INT = NULL,
	@IsDisabled BIT = NULL,
	@IsChangePasswordRequired BIT = NULL,
	@IsFirstLogin BIT = NULL

EXEC creds.spExtranetUser_Credentials_Validate
	@ApplicationID = 1,
	@BusinessEntityID = 92,
	@Username = 'dhughes',
	@Password = 'password',
	--@IgnoreTokenCreation = 1,
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@CredentialToken = @CredentialToken OUTPUT,
	@IsValid = @IsValid OUTPUT,
	@IsLockedOut = @IsLockedOut OUTPUT,
	@RequestedBy = 'dhughes'
	
SELECT @CredentialEntityID AS CredentialEntityID, @CredentialToken AS CredentialToken,
	@IsValid AS IsValid, @IsApproved AS IsApproved, @IsLockedOut AS IsLockedOut, @DateLastLoggedIn AS DateLastLoggedIn,
	@DateLastPasswordChanged AS DateLastPasswordChanged, @DatePasswordExpires AS DatePasswordExpires, @DateLastLockedOut AS DateLastLockedOut,
	@DateLockOutExpires AS DateLockOutExpires, @FailedPasswordAttemptCount AS FailedPasswordAttemptCount, 
	@FailedPasswordAnswerAttemptCount AS FailedPasswordAnswerAttemptCount, @IsDisabled AS IsDisabled,
	@IsChangePasswordRequired AS IsChangePasswordRequired, @IsFirstLogin AS IsFirstLogin
	
*/

-- SELECT * FROM creds.Membership
-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
-- SELECT * FROM dbo.EventLog ORDER BY EventLogID DESC
-- SELECT * FROM dbo.RequestTracker
-- TRUNCATE TABLE dbo.RequestTracker
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [creds].[spExtranetUser_Credentials_Validate]
	@ApplicationID INT = NULL,
	@BusinessEntityID INT,
	@Username VARCHAR(256),
	@Password VARCHAR(50),
	@IgnoreTokenCreation BIT = NULL,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@CredentialToken VARCHAR(50) = NULL OUTPUT,
	@IsValid BIT = NULL OUTPUT,
	-- Core membership properties ****************************
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT,
	-- *******************************************************
	@IsChangePasswordRequired BIT = NULL OUTPUT,
	@IsFirstLogin BIT = NULL OUTPUT,
	@RequestedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------------------------------------------
	-- Instance variables.
	---------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	



	---------------------------------------------------------------------------------------------------
	-- Log request.
	---------------------------------------------------------------------------------------------------	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@Password=' + dbo.fnToStringOrEmpty(@Password) + ';' +
		'@IgnoreTokenCreation=' + dbo.fnToStringOrEmpty(@IgnoreTokenCreation) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@CredentialToken=' + dbo.fnToStringOrEmpty(@CredentialToken) + ';' +
		'@IsValid=' + dbo.fnToStringOrEmpty(@IsValid) + ';' +
		'@IsApproved=' + dbo.fnToStringOrEmpty(@IsApproved) + ';' +
		'@IsLockedOut=' + dbo.fnToStringOrEmpty(@IsLockedOut) + ';' +
		'@DateLastLoggedIn=' + dbo.fnToStringOrEmpty(@DateLastLoggedIn) + ';' +
		'@DateLastPasswordChanged=' + dbo.fnToStringOrEmpty(@DateLastPasswordChanged) + ';' +
		'@DatePasswordExpires=' + dbo.fnToStringOrEmpty(@DatePasswordExpires) + ';' +
		'@DateLastLockedOut=' + dbo.fnToStringOrEmpty(@DateLastLockedOut) + ';' +
		'@DateLockOutExpires=' + dbo.fnToStringOrEmpty(@DateLockOutExpires) + ';' +
		'@FailedPasswordAttemptCount=' + dbo.fnToStringOrEmpty(@FailedPasswordAttemptCount) + ';' +
		'@IsDisabled=' + dbo.fnToStringOrEmpty(@IsDisabled) + ';' +
		'@IsChangePasswordRequired=' + dbo.fnToStringOrEmpty(@IsChangePasswordRequired) + ';' +
		'@IsFirstLogin=' + dbo.fnToStringOrEmpty(@IsFirstLogin) + ';' +
		'@RequestedBy=' + dbo.fnToStringOrEmpty(@RequestedBy) + ';' ;
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	---------------------------------------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------------------------------------
	SET @CredentialEntityID = NULL;
	SET @CredentialToken = NULL;
	SET @IsValid = 0;
	SET @IgnoreTokenCreation = ISNULL(@IgnoreTokenCreation, 0);
	SET @IsApproved = 0;
	SET	@IsLockedOut = 0;
	SET	@DateLastLoggedIn = NULL;
	SET	@DateLastPasswordChanged = NULL;
	SET @DatePasswordExpires = NULL;
	SET	@DateLastLockedOut = NULL;
	SET @DateLockOutExpires = NULL;
	SET	@FailedPasswordAttemptCount = 0;
	SET	@FailedPasswordAnswerAttemptCount = 0;
	SET @IsDisabled = 0;
	SET @IsChangePasswordRequired = 0;
	SET	@IsFirstLogin = 0;
	
	---------------------------------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------------------------------
	DECLARE
		@CredentialEntityTypeID INT = creds.fnGetCredentialEntityTypeID('EXTRANET'),
		@Exists BIT = 0,
		@Source VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@RequestKey VARCHAR(256) = 
			dbo.fnIfNullOrEmpty(@ApplicationID, '<EmptyApplicationKey>') + '-' + 
			dbo.fnIfNullOrEmpty(@BusinessEntityID, '<EmptyBusinessKey>') + '-' +
			dbo.fnIfNullOrEmpty(@Username, '<EmptyUsername>')

	---------------------------------------------------------------------------------------------------
	-- Fetch credential entity
	-- <Summary>
	-- Interrogates the credentials data for extranet users
	-- to determine if the supplied credentials meets the required
	-- criteria to be authenticated.
	-- </Summary>
	---------------------------------------------------------------------------------------------------
	EXEC creds.spCredentialEntity_Derived_Exists
		@CredentialEntityTypeID = @CredentialEntityTypeID,
		@BusinessEntityID = @BusinessEntityID,
		@DerivedCredentialKey = @Username,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@Exists = @Exists OUTPUT
	
	
	
	---------------------------------------------------------------------------------------------------
	-- Inspect results
	-- <Summary>
	-- If we do not have a CredentialEntityID then we need to flag
	-- as invalid and exit the procedure.  Otherwise, we need to
	-- validate password.
	-- </Summary>
	---------------------------------------------------------------------------------------------------
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @IsValid = 0;
		
		--RETURN; -- Exit procedure.
	END
	
	---------------------------------------------------------------------------------------------------
	-- Validate password
	-- <Summary>
	-- Determine if supplied password matches the password in the
	-- membership repository.
	-- </Summary>
	---------------------------------------------------------------------------------------------------
	EXEC creds.spMembership_Password_Compare
		@CredentialEntityID = @CredentialEntityID,
		@Password = @Password,
		@IsValid = @IsValid OUTPUT,
		-- Core membership properties ****************************
		@IsApproved = @IsApproved OUTPUT,
		@IsLockedOut = @IsLockedOut OUTPUT,
		@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
		@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
		@DatePasswordExpires = @DatePasswordExpires OUTPUT,
		@DateLastLockedOut = @DateLastLockedOut OUTPUT,
		@DateLockOutExpires = @DateLockOutExpires OUTPUT,		
		@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
		@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
		@IsDisabled = @IsDisabled OUTPUT
		-- *******************************************************
	
	-- update validaiton variable
	SET @IsValid = ISNULL(@IsValid, 0);
	
	---------------------------------------------------------------------------------------------------
	-- Track the request (regardless of being valid).
	-- <Summary>
	-- Tracks the attempt.  If the request is valid and not locked out, then it will
	-- be removed from the tracking table. Otherwise, the attempt will be recorded
	-- for possible further inspection.
	-- </Summary>
	---------------------------------------------------------------------------------------------------	
	DECLARE 
		@RequestTypeID INT = dbo.fnGetRequestTypeID('AUTH'),
		@RequestTrackerID BIGINT = NULL,
		@MaxRequestsAllowed INT = NULL,
		@RequestCount INT = NULL
	
	EXEC dbo.spRequestTracker_TrackWithLockOut
		@RequestTypeID = @RequestTypeID,
		@SourceName = @Source,
		@ApplicationKey = @ApplicationID,
		@BusinessKey = @BusinessEntityID,
		@RequesterKey = @Username,
		@RequestKey = @RequestKey,
		@ApplicationID = @ApplicationID,
		@BusinessEntityID = @BusinessEntityID,
		@RequesterID = @CredentialEntityID,
		@CreatedBy = @RequestedBy,
		@ModifiedBy = @RequestedBy,
		@MaxRequestsAllowed = @MaxRequestsAllowed OUTPUT,
		@RequestCount = @RequestCount OUTPUT,
		@IsLocked = @IsLockedOut OUTPUT,
		@RequestTrackerID = @RequestTrackerID OUTPUT
	
	-- Debug
	--SELECT @RequestTrackerID AS RequestTrackerID, @MaxRequestsAllowed AS MaxRequestsAllowed, @RequestCount AS RequestCount,
	--	@IsLocked AS IsLocked
			

	---------------------------------------------------------------------------------------------------
	-- Validate the lock status of the account and determine if that affects
	-- the validity of the user/password verification.
	---------------------------------------------------------------------------------------------------	
	IF @IsLockedOut = 1
	BEGIN
	
		IF @RequestCount = @MaxRequestsAllowed AND @IsValid = 0
		BEGIN
			SET @IsValid = 0;
			SET @IsLockedOut = 1
		END
		ELSE IF @RequestCount >= @MaxRequestsAllowed AND @IsValid = 0
		BEGIN
			SET @IsValid = 0;
			SET @IsLockedOut = 1
		END
		ELSE IF @RequestCount <= @MaxRequestsAllowed AND @IsValid = 1
		BEGIN
			SET @IsValid = 1;
			SET @IsLockedOut = 0 -- Unlock request as it is not really locked.
		END
		ELSE
		BEGIN
			SET @IsValid = 0;
		END
		
		---------------------------------------------------------------------------------------------------
		-- Record  locked event.  
		-- <Summary>
		-- Records the locked event, but only during its first encounter.
		-- </Summary>
		-- <Remarks>
		-- Do not fail the process if the event was
		-- unable to be recorded.
		-- </Remarks>
		---------------------------------------------------------------------------------------------------
		IF @RequestCount = @MaxRequestsAllowed AND @IsValid = 0
		BEGIN
		
			BEGIN TRY
			
				DECLARE @LockOutEventID INT = creds.fnGetCredentialEventID('LO');
				
				DECLARE @LockOutEventData XML = 
					'<CredentialEventData>' +
						dbo.fnXmlWriteElementString('ApplicationID', @ApplicationID) +
						dbo.fnXmlWriteElementString('BusinessID', @BusinessEntityID) +
						dbo.fnXmlWriteElementString('CredentialEntityID', @CredentialEntityID) +
						dbo.fnXmlWriteElementString('Username', @Username) +
						dbo.fnXmlWriteElementString('CredentialEntityTypeID', @CredentialEntityTypeID) +
						dbo.fnXmlWriteElementString('CredentialToken', @CredentialToken) +
						dbo.fnXmlWriteElementString('MaxRequestsAllowed', @MaxRequestsAllowed) +
					'</CredentialEventData>';
				
				EXEC creds.spCredentialEventLog_Create
					@EventID = @LockOutEventID,
					@EventSource = @Source,
					@EventDataXml = @LockOutEventData,
					@CreatedBy = @RequestedBy
			
			END TRY
			BEGIN CATCH		
			END CATCH
		
		END	
		
	END
	
	
	
	---------------------------------------------------------------------------------------------------
	-- If a valid response was received from the membership
	-- query then we need to delete the tracked request as it was successful.
	---------------------------------------------------------------------------------------------------
	IF @IsValid = 1
	BEGIN
	
		EXEC dbo.spRequestTracker_Delete
			@RequestTrackerID = @RequestTrackerID,
			@ApplicationKey = @ApplicationID,
			@BusinessKey = @BusinessEntityID,
			@RequesterKey = @CredentialEntityID,
			@RequestKey = @RequestKey
			
	END	
		
	---------------------------------------------------------------------------------------------------
	-- If a valid response was received from the membership
	-- query then we need to create a new credential security token.
	-- Otherwise, the credentials that were passed were invalid.
	---------------------------------------------------------------------------------------------------
	IF @IsValid = 1 AND @IgnoreTokenCreation = 0
	BEGIN
	
		DECLARE 
			@TokenTypeID INT = dbo.fnGetTokenTypeID('LGN')
			
		EXEC creds.spCredentialToken_Create
			@CredentialEntityID = @CredentialEntityID,
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessEntityID,
			@TokenTypeID = @TokenTypeID,
			@Token = @CredentialToken OUTPUT				
			
		---------------------------------------------------------------------------------------------------
		-- Record event.  Do not fail the process if the event was
		-- unable to be recorded.
		---------------------------------------------------------------------------------------------------
		BEGIN TRY
		
			DECLARE @AuthEventID INT = creds.fnGetCredentialEventID('AUTHNS');
			
			DECLARE @AuthEventData XML = 
				'<CredentialEventData>' +
					dbo.fnXmlWriteElementString('ApplicationID', @ApplicationID) +
					dbo.fnXmlWriteElementString('BusinessID', @BusinessEntityID) +
					dbo.fnXmlWriteElementString('CredentialEntityID', @CredentialEntityID) +
					dbo.fnXmlWriteElementString('Username', @Username) +
					dbo.fnXmlWriteElementString('CredentialEntityTypeID', @CredentialEntityTypeID) +
					dbo.fnXmlWriteElementString('CredentialToken', @CredentialToken) +
				'</CredentialEventData>';
			
			EXEC creds.spCredentialEventLog_Create
				@EventID = @AuthEventID,
				@EventSource = @Source,
				@EventDataXml = @AuthEventData,
				@CreatedBy = @RequestedBy
		
		END TRY
		BEGIN CATCH		
		END CATCH
	
	END
	
END
