﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/5/2015
-- Description:	Creates a new CredentialEntityApplication object.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityApplicationID BIGINT = NULL
	
EXEC creds.spCredentialEntityApplication_Create
	@CredentialEntityID = 959790,
	@ApplicationID = 8,
	@CredentialEntityApplicationID = @CredentialEntityApplicationID OUTPUT

SELECT @CredentialEntityApplicationID AS CredentialEntityApplicationID

*/

-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntityApplication
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntityApplication_Create]
	@CredentialEntityID BIGINT,
	@ApplicationID INT,
	@CreatedBy VARCHAR(256) = NULL,
	@CredentialEntityApplicationID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CredentialEntityApplicationID = NULL;
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- CredentialEntityID
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- ApplicationID
	IF @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
		
	-------------------------------------------------------------------------------
	-- Create new CredentialEntityRole object.
	-------------------------------------------------------------------------------
	INSERT INTO creds.CredentialEntityApplication (		
		CredentialEntityID,
		ApplicationID,
		CreatedBy
	)
	SELECT
		@CredentialEntityID AS CredentialEntityID,
		@ApplicationID AS ApplicationID,
		@CreatedBy AS CreatedBy
	
	-- Retrieve record identity value.
	SET @CredentialEntityApplicationID = SCOPE_IDENTITY();
		
END
