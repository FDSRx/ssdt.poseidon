﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/11/2013
-- Description:	Creates a credential entity workgroup relationship
-- SAMPLE CALL:
/*
DECLARE @CredentialEntityWorkgroupID BIGINT
EXEC creds.spCredentialEntityWorkgroup_Create
	@CredentialEntityID = 11,
	@WorkgoupID = 12,
	@CredentialEntityWorkgroupID = @CredentialEntityWorkgroupID OUTPUT
SELECT @CredentialEntityWorkgroupID AS CredentialEntityWorkgroupID
*/
-- SELECT * FROM creds.CredentialEntityWorkgroup
-- SELECT * FROM creds.NetworkUser
-- SELECT * FROM creds.Workgroup
-- =============================================
CREATE PROCEDURE creds.spCredentialEntityWorkgroup_Create
	@CredentialEntityID BIGINT,
	@WorkgoupID BIGINT,
	@CredentialEntityWorkgroupID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------
	SET @CredentialEntityWorkgroupID = NULL;
	
	---------------------------------------------------------
	-- Create new entry
	---------------------------------------------------------
	INSERT INTO creds.CredentialEntityWorkgroup (
		CredentialEntityID,
		WorkgroupID
	)
	SELECT
		@CredentialEntityID,
		@WorkgoupID
	
	
	-- Retrieve identity
	SET @CredentialEntityWorkgroupID = SCOPE_IDENTITY();
	
END
