﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/31/2013
-- Description:	Updates a credential token
-- SAMPLE CALL:
/*
DECLARE 
	@CredentialTokenID BIGINT = NULL, 
	@ApplicationID INT = NULL,
	@BusinessEntityID INT,
	@Token VARCHAR(50) = 'D1D88CF0-5CDE-4410-81CE-EBC92A0D0636',
	@CredentialEntityID BIGINT,
	@TokenData XML,
	@IsExpired BIT,
	@DateExpires DATETIME,
	@InvalidateTokenOnExpired BIT = 1

EXEC creds.spCredentialToken_Update
	@CredentialTokenID = @CredentialTokenID OUTPUT,
	@Token = @Token OUTPUT,
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	--@ForceExpire = 1,
	@ForceUpdate = 1,
	@TokenData = @TokenData,
	@IsExpired = @IsExpired OUTPUT,
	@DateExpires = @DateExpires OUTPUT,
	@ApplicationID = @ApplicationID OUTPUT,
	@BusinessEntityID = @BusinessEntityID OUTPUT,
	@InvalidateTokenOnExpired = @InvalidateTokenOnExpired
	--,@SetBusinessEntityIDAsNull = 1
	--,@SetApplicationIDASNull = 1

SELECT @CredentialTokenID AS CredentialTokenID, @CredentialEntityID AS CredentialEntityID,
	@Token AS Token, @TokenData AS TokenData,
	@ApplicationID AS ApplicationID, @BusinessEntityID AS BusinessEntityID,
	@DateExpires AS DateExpires, @IsExpired AS IsExpired	
*/
--SELECT * FROM creds.CredentialToken WHERE Token = 'D1D88CF0-5CDE-4410-81CE-EBC92A0D0636'
--SELECT * FROM dbo.Configuration
-- =============================================
CREATE PROCEDURE [creds].[spCredentialToken_Update]
	@CredentialTokenID BIGINT = NULL OUTPUT,
	@Token VARCHAR(50) = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@TokenData XML = NULL,
	@TokenDurationMinutes INT = NULL,
	@ForceUpdate BIT = 0,
	@ForceExpire BIT = 0,
	@IsExpired BIT = 0 OUTPUT,
	@DateExpires DATETIME = NULL OUTPUT,
	@ApplicationID INT = NULL OUTPUT,
	@SetApplicationIDAsNull BIT = 0,
	@BusinessEntityID INT = NULL OUTPUT,
	@SetBusinessEntityIDAsNull BIT = 0,
	@InvalidateTokenOnExpired BIT = NULL,
	@ErrorLogID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------
	SET @ForceUpdate = ISNULL(@ForceUpdate, 0);
	SET @ForceExpire = ISNULL(@ForceExpire, 0);
	SET @InvalidateTokenOnExpired = ISNULL(@InvalidateTokenOnExpired, 1);
	SET @IsExpired = 0;
	SET @DateExpires = NULL;
	SET @SetApplicationIDAsNull = ISNULL(@SetApplicationIDAsNull, 0);
	SET @SetBusinessEntityIDAsNull = ISNULL(@SetBusinessEntityIDAsNull, 0);
	
	-----------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
	
	-----------------------------------------------------------------
	-- Set variables
	-----------------------------------------------------------------
	-- If a credetial id was referenced then it supersedes any token that may have been passed.
	SET @Token = CASE WHEN @CredentialTokenID IS NOT NULL THEN NULL ELSE @Token END;

	-----------------------------------------------------------------
	-- Argument is null exception
	-----------------------------------------------------------------
	IF @CredentialTokenID IS NULL AND ISNULL(@Token, '') = ''
	BEGIN
	
		SET @ErrorMessage = 'Unable to update credential token. Exception: Object references (@CredentialTokenID and @Token) are not set to an instance ' +
			'of an object. A CredentialTokenID or Token needs to be referenced.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;
				
	END
	
	BEGIN TRY
		
		--------------------------------------------------------------
		-- Fetch credential token
		--------------------------------------------------------------	
		EXEC creds.spCredentialToken_Get
			@CredentialTokenID = @CredentialTokenID OUTPUT,
			@Token = @Token OUTPUT,
			@CredentialEntityID = @CredentialEntityID OUTPUT,
			@IsExpired = @IsExpired OUTPUT,
			@DateExpires = @DateExpires OUTPUT,
			@ApplicationID = @ApplicationID OUTPUT,
			@BusinessEntityID = @BusinessEntityID OUTPUT
		
		
		------------------------------------------------------
		-- Token duration
		-- <Summary>
		-- Determines the amount of time the token is allowed
		-- to stay active
		-- </Summary>
		------------------------------------------------------
		DECLARE @DurationKey VARCHAR(25) = 'CredTokenDurationMin';
		
		SET @TokenDurationMinutes = COALESCE(dbo.fnGetAppBusinessConfigValue(@ApplicationID, @BusinessEntityID, @DurationKey),
			dbo.fnGetAppConfigValue(@ApplicationID, @DurationKey),
			dbo.fnGetBusinessConfigValue(@BusinessEntityID, @DurationKey), dbo.fnGetConfigValue(@DurationKey), 30);
			
	
		--Data Helper
		--SELECT * FROM dbo.Configuration	
		
		
		--------------------------------------------------------------
		-- Interrogate token response
		--------------------------------------------------------------
		DECLARE @tblOutput AS TABLE (
			ApplicationID INT,
			BusinessEntityID INT,
			DateExpires DATETIME
		);
		
		-- If I have a valid token returned, then process token accordingly
		IF @CredentialTokenID IS NOT NULL
		BEGIN
		
			-- if the token is not expired, or it is forced to be updated, or it is forced to be expired
			-- then update the token data.
			IF @IsExpired = 0 OR @ForceExpire = 1 OR @ForceUpdate = 1
			BEGIN
			
				UPDATE tok
				SET DateExpires = 
						CASE 
							WHEN @ForceExpire = 0 THEN DATEADD(mi, @TokenDurationMinutes, GETDATE()) 
							WHEN @ForceExpire = 1 AND @IsExpired = 0 THEN GETDATE() --DATEADD(mi, @TokenDurationMinutes * -1, GETDATE())
							ELSE DateExpires 
						END,
					TokenData = CASE WHEN @TokenData IS NULL THEN TokenData ELSE @TokenData END,
					BusinessEntityID = 
						CASE
							WHEN @SetBusinessEntityIDAsNull = 1 THEN NULL
							WHEN @BusinessEntityID IS NOT NULL THEN @BusinessEntityID
							ELSE BusinessEntityID
						END,
					ApplicationID = 
						CASE
							WHEN @SetApplicationIDAsNull = 1 THEN NULL
							WHEN @ApplicationID IS NOT NULL THEN @ApplicationID
							ELSE ApplicationID
						END,
					DateModified = GETDATE()
				--SELECT *
				OUTPUT inserted.ApplicationID, inserted.BusinessEntityID, inserted.DateExpires 
				INTO @tblOutput(ApplicationID, BusinessEntityID,DateExpires)
				FROM creds.CredentialToken tok
				WHERE CredentialTokenID = @CredentialTokenID
					AND ( @BusinessEntityID IS NULL OR BusinessEntityID = @BusinessEntityID )
					AND ( @ApplicationID IS NULL OR ApplicationID = @ApplicationID )
				
				
				-- Retrieve updated row data from output values.
				SELECT TOP(1)
					@ApplicationID = ApplicationID,
					@BusinessEntityID = BusinessEntityID,					
					@DateExpires = DateExpires
				FROM @tblOutput
				
				-- Send the caller a friendly bit to let them know the token is alive or dead.
				SET @IsExpired = 
					CASE
						WHEN @ForceUpdate = 1 THEN 0
						WHEN @ForceExpire = 1 OR @IsExpired = 1 THEN 1
						ELSE 0 
					END;
			
			END
		
		END
		ELSE
		BEGIN
			-- Credential token does not exist, so we should mark as expired so caller does not attempt
			-- to just use the expired. flag.
			SET @IsExpired = 1;
			SET @DateExpires = '1/1/1900';
		END
	
		-- If the caller does not want any information returned on an expired token
		-- then we need to destroy the token data.	
		IF ISNULL(@InvalidateTokenOnExpired, 0) = 1 AND ISNULL(@IsExpired, 0) = 1
		BEGIN
			EXEC creds.spCredentialToken_Data_Invalidate
				@CredentialTokenID = @CredentialTokenID OUTPUT,
				@CredentialEntityID = @CredentialEntityID OUTPUT,
				@Token = @Token OUTPUT,
				@TokenData = @TokenData OUTPUT
		END
	
	END TRY
	BEGIN CATCH
	
		EXECUTE [dbo].[spLogError] @ErrorLogID OUTPUT;
		
		SET @ErrorMessage = ERROR_MESSAGE();
		
		-- Re-throw error
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
	
	END CATCH
	
	
END
