﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/25/2015
-- Description:	Interrogates the ClientID and SecretKey to determine if the OAuthClient object is valid.
-- SAMPLE CALL:
/*

DECLARE
	@ApplicationID INT = 10,
	@CredentialEntityID BIGINT = NULL,
	@IsValid BIT = NULL,
	@Applications VARCHAR(MAX) = NULL,
	@ApplicationData XML = NULL,
	@Roles VARCHAR(MAX) = NULL,
	@RoleData XML = NULL,
	@Businesses VARCHAR(MAX) = NULL,
	@BusinessData XML = NULL,
	@RolePrefixKey VARCHAR(50) = 'APPCODE'

EXEC creds.spOAuthClient_SecurityContext_Get
	@ApplicationID = @ApplicationID,
	@ClientID = 'EAE24BAD-1ABF-4AD8-8473-DB5435FC6124',
	@SecretKey = 'EA712596-215A-46A9-9CD4-39C3AE01D739',
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@IsValid = @IsValid OUTPUT,
	@Applications = @Applications OUTPUT,
	@ApplicationData = @ApplicationData OUTPUT,
	@Roles = @Roles OUTPUT,
	@RoleData = @RoleData OUTPUT,
	@Businesses = @Businesses OUTPUT,
	@BusinessData = @BusinessData OUTPUT,
	@RolePrefixKey = @RolePrefixKey

SELECT @CredentialEntityID AS CredentialEntityID, @IsValid AS IsValid,
	@Applications AS Applications, @ApplicationData AS ApplicationData, @Roles AS Roles, 
	@RoleData AS RoleData, @Businesses AS Businesses, @BusinessData AS BusinessData

*/

-- SELECT * FROM creds.OAuthClient
-- =============================================
CREATE PROCEDURE [creds].[spOAuthClient_SecurityContext_Get]
	@ApplicationID INT = NULL, -- For future use.
	@BusinessID BIGINT = NULL, -- For future use.
	@ClientID NVARCHAR(256), -- a.k.a. Username
	@SecretKey NVARCHAR(256), -- a.k.a. Password
	@CredentialEntityID BIGINT = NULL OUTPUT, -- a.k.a. Client identifier
	@Name VARCHAR(256) = NULL OUTPUT,
	@IsValid BIT = NULL OUTPUT,
	@Roles VARCHAR(MAX) = NULL OUTPUT,
	@RoleProperty VARCHAR(50) = NULL,
	@RolePrefixKey VARCHAR(50) = NULL,
	@RolePrefixCustomName VARCHAR(50) = NULL,
	@RoleData XML = NULL OUTPUT,
	@Applications VARCHAR(MAX) = NULL OUTPUT,
	@ApplicationProperty VARCHAR(50) = NULL,
	@ApplicationData XML = NULL OUTPUT,
	@Businesses VARCHAR(MAX) = NULL OUTPUT,
	@BusinessProperty VARCHAR(50) = NULL,
	@BusinessData XML = NULL OUTPUT,
	@ListDelimiter VARCHAR(10) = NULL,
	@IsOutputOnly BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-----------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-----------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@ClientID=' + dbo.fnToStringOrEmpty(@ClientID) + ';' +
		'@SecretKey=' + dbo.fnToStringOrEmpty(@SecretKey) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@IsValid=' + dbo.fnToStringOrEmpty(@IsValid) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@Roles=' + dbo.fnToStringOrEmpty(@Roles) + ';' +
		'@Applications=' + dbo.fnToStringOrEmpty(@Applications) + ';' +
		'@BusinessKeys=' + dbo.fnToStringOrEmpty(@BusinessKeys) + ';' ;
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	-----------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------
	SET @IsValid = 0;
	SET @Name = NULL;
	SET @Roles = NULL;
	SET @Applications = NULL;
	SET @Businesses = NULL;
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	SET @ListDelimiter = ISNULL(@ListDelimiter, ',');


	-----------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationCount INT = 0,
		@ApplicationName VARCHAR(256) = NULL,
		@ApplicationCode VARCHAR(50) = NULL

	-----------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------
	-- Not applicable.

	-----------------------------------------------------------------------------------------------------
	-- Validate object.
	-----------------------------------------------------------------------------------------------------
	EXEC creds.spOAuthClient_IsValid
		@ClientID = @ClientID,
		@SecretKey = @SecretKey,
		@IsValid = @IsValid OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT
		
	-- If I did not find a Credential object then exit the procedure.
	IF @CredentialEntityID IS NULL
	BEGIN
		RETURN;
	END
	
	-----------------------------------------------------------------------------------------------------
	-- Temporary resources.
	-----------------------------------------------------------------------------------------------------
	-- Create temporary credential/application table.
	IF OBJECT_ID('tempdb..#tmpCredApplications') IS NOT NULL
	BEGIN
		DROP TABLE #tmpCredApplications;
	END

	CREATE TABLE #tmpCredApplications (
		ApplicationID INT,
		Code VARCHAR(50),
		Name VARCHAR(50),
		Description VARCHAR(1000)
	);

	-----------------------------------------------------------------------------------------------------
	-- Retrieve the object's business.
	-- <Summary>
	-- Retrieves the business that the object is bound to.
	-- </Summary>
	-----------------------------------------------------------------------------------------------------
	SET @BusinessID = ISNULL(@BusinessID, (
		SELECT TOP 1
			BusinessEntityID
		FROM creds.CredentialEntity
		WHERE CredentialEntityID = @CredentialEntityID
	));

	-- Debug
	-- SELECT @CredentialEntityID AS CredentialEntityID, @IsValid AS IsValid, @ApplicationID AS ApplicationID, @BusinessID AS BusinessID

	-----------------------------------------------------------------------------------------------------
	-- Retrieve object properties.
	-- <Summary>
	-- Retrieves properties that are applicable to the client (e.g. roles, businesses, applications, etc.)
	-- </Summary>
	-----------------------------------------------------------------------------------------------------
	IF @CredentialEntityID IS NOT NULL
	BEGIN

		-----------------------------------------------------------------------------------------------------
		-- Retrieve application data.
		-- <Summary>
		-- Retrieve the application data for the client object.
		-- </Summary>
		-----------------------------------------------------------------------------------------------------
		-- Data Review
		-- SELECT * FROM acl.CredentialEntityRole		
		-- SELECT * FROM acl.RoleAllocation	

		-- Retrieve list of applications.
		EXEC creds.spCredentialEntityApplication_Get_List
			@CredentialEntityID = @CredentialEntityID,
			@ApplicationID = @ApplicationID,
			@BusinessID = @BusinessID,
			@IsResultXml = 1,
			@IsOutputOnly = 1,
			@ApplicationData = @ApplicationData OUTPUT


		INSERT INTO #tmpCredApplications (
			ApplicationID,
			Code,
			Name,
			Description
		)
		SELECT
			Id,
			Code,
			Name,
			Description
		FROM dbo.fnGetApplicationsFromXml(@ApplicationData)

		-- Retrieve Application object count.
		SET @ApplicationCount = @@ROWCOUNT;

		-- Transform to identifier string.
		SET @Applications = (
			SELECT
				CASE
					WHEN @ApplicationProperty IS NULL OR @ApplicationProperty = '' THEN Code
					WHEN @ApplicationProperty = 'Name' THEN Name
					WHEN @ApplicationProperty = 'Code' THEN Code
					ELSE CONVERT(VARCHAR(50), ApplicationID)
				END + @ListDelimiter
			FROM #tmpCredApplications
			FOR XML PATH('')
		);

		-- Remove trailing comma.
		SET @Applications = 
			CASE
				WHEN @Applications IS NULL OR @Applications = '' THEN @Applications
				ELSE LEFT(@Applications, LEN(@Applications) - 1)
			END;

		SET @ApplicationData = (
			SELECT
				ApplicationID AS Id,
				Code,
				Name,
				Description,
				CONVERT(XML, (
					SELECT
						cer.RoleID AS Id,
						rle.Code,
						rle.Name,
						NULL AS Description
					FROM acl.CredentialEntityRole cer
						JOIN acl.Roles rle
							ON cer.RoleID = rle.RoleID
					WHERE CredentialEntityID = @CredentialEntityID
						AND ApplicationID = tmp.ApplicationID
						AND BusinessEntityID = @BusinessID
					FOR XML PATH('Role')
				)) AS Roles
			FROM #tmpCredApplications tmp
			FOR XML PATH('Application'), ROOT('Applications')
		);

		-----------------------------------------------------------------------------------------------------
		-- Retrieve business data.
		-- <Summary>
		-- Retrieve the business data for the client object.
		-- </Summary>
		-----------------------------------------------------------------------------------------------------
		EXEC creds.spCredentialEntityBusinessEntity_Get_List
			@CredentialEntityID = @CredentialEntityID,
			@ApplicationID = @ApplicationID,
			@BusinessID = @BusinessID,
			@IsResultXml = 1,
			@IsOutputOnly = 1,
			@BusinessData = @BusinessData OUTPUT

		-- Transform to identifier string.
		SET @Businesses = (
			SELECT
				CASE
					WHEN @BusinessProperty IS NULL OR @BusinessProperty = '' THEN Nabp
					WHEN @BusinessProperty = 'Nabp' THEN Name
					WHEN @BusinessProperty = 'SourceStoreKey' THEN SourceStoreKey
					WHEN @BusinessProperty = 'Name' THEN Name
					ELSE CONVERT(VARCHAR(50), BusinessID)
				END + @ListDelimiter
			FROM dbo.fnGetBusinessesFromXml(@BusinessData)
			FOR XML PATH('')
		);

		-- Remove trailing comma.
		SET @Businesses = 
			CASE
				WHEN @Businesses IS NULL OR @Businesses = '' THEN @Businesses
				ELSE LEFT(@Businesses, LEN(@Businesses) - 1)
			END;


		-----------------------------------------------------------------------------------------------------
		-- Retrieve role data.
		-- <Summary>
		-- Retrieve the role data for the client object.
		-- </Summary>
		-----------------------------------------------------------------------------------------------------
		EXEC acl.spCredentialEntityRole_Get_List
			@CredentialEntityID = @CredentialEntityID,
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessID,
			@IsResultXml = 1,
			@IsResultXmlOutputOnly = 1,
			@RoleData = @RoleData OUTPUT


		IF @ApplicationCount > 0
		BEGIN
			SELECT
				@ApplicationCode = Code,
				@ApplicationName = Name
			FROM #tmpCredApplications
			WHERE ApplicationID = @ApplicationID
		END

		-- Transform to identifier string.
		SET @Roles = (
			SELECT 
				CASE
					WHEN @ApplicationCount = 0 THEN ''
					WHEN @RolePrefixCustomName IS NOT NULL OR @RolePrefixCustomName <> '' THEN @RolePrefixCustomName + '.'
					WHEN @RolePrefixKey IN ('APPCODE', 'APPLICATIONCODE', 'ACODE') THEN @ApplicationCode + '.'
					WHEN @RolePrefixKey IN ('APPNAME', 'APPLICATIONNAME', 'ANAME') THEN @ApplicationName + '.'
					WHEN @RolePrefixKey IN ('APPID', 'APPLICATIONID', 'AID') THEN CONVERT(VARCHAR(50), @ApplicationID) + '.'
					ELSE ''
				END +
				CASE
					WHEN @RoleProperty IS NULL OR @RoleProperty = '' THEN Code
					WHEN @RoleProperty = 'Name' THEN Name
					WHEN @RoleProperty = 'Code' THEN Code
					ELSE CONVERT(VARCHAR(50), Id)
				END + @ListDelimiter
			FROM acl.fnGetRolesFromXml(@RoleData)
			FOR XML PATH('')
		);

		-- Remove trailing comma.
		SET @Roles = 
			CASE
				WHEN @Roles IS NULL OR @Roles = '' THEN @Roles
				ELSE LEFT(@Roles, LEN(@Roles) - 1)
			END;

	END

	
	-----------------------------------------------------------------------------------------------------
	-- Return data.
	-----------------------------------------------------------------------------------------------------
	IF ISNULL(@IsOutputOnly, 0) = 0
	BEGIN
		SELECT
			@IsValid AS IsValid,
			@CredentialEntityID AS CredentialEntityID,
			@Applications AS Applications,
			@ApplicationData AS ApplicationData,
			@Businesses AS Businesses,
			@BusinessData AS BusinessData,
			@Roles AS Roles,
			@RoleData AS RoleData
	END
	

	-----------------------------------------------------------------------------------------------------
	-- Dispose of resources.
	-----------------------------------------------------------------------------------------------------
	DROP TABLE #tmpCredApplications;


END
