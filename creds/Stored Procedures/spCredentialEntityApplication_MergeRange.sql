﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/2/2014
-- Description:	Creates a single or collection of CredentialEntityBusinessEntity objects.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityApplicationIDList VARCHAR(MAX) = NULL
	
EXEC creds.spCredentialEntityApplication_MergeRange
	@CredentialEntityID = 801050,
	@ApplicationID = '10,8',
	@DeleteUnspecified = 1,
	@ModifiedBy = 'dhughes',	
	@CreatedBy = 'dhughes',
	@CredentialEntityApplicationIDList = @CredentialEntityApplicationIDList OUTPUT

SELECT @CredentialEntityApplicationIDList AS CredentialEntityApplicationIDList


*/

-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntityApplication
-- =============================================
CREATE PROCEDURE [creds].[spCredentialEntityApplication_MergeRange]
	@CredentialEntityID BIGINT,
	@ApplicationID VARCHAR(MAX) = NULL,
	@DeleteUnspecified BIT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256),
	@CredentialEntityApplicationIDList VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------
	DECLARE @Trancount INT = @@TRANCOUNT;
	
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CredentialEntityApplicationIDList = NULL;
	SET @DeleteUnspecified = ISNULL(@DeleteUnspecified, 0);
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorSeverity INT = NULL,
		@ErrorState INT = NULL,
		@ErrorNumber INT = NULL,
		@ErrorLine INT = NULL,
		@ErrorProcedure  NVARCHAR(200) = NULL,
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)


	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @CredentialEntityID
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	-- @AppliationID
	IF @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CredentialEntityApplication object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END		
	


	BEGIN TRY
	--------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------	
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		-------------------------------------------------------------------------------
		-- Temporary objects.
		-------------------------------------------------------------------------------
		DECLARE @tblOutput AS TABLE (
			Idx INT IDENTITY(1,1),
			CredentialEntityApplicationID BIGINT
		);
		
		-------------------------------------------------------------------------------
		-- Store ModifiedBy property in "session" like object.
		-------------------------------------------------------------------------------
		EXEC memory.spContextInfo_TriggerData_Set
			@ObjectName = @ProcedureName,
			@DataString = @ModifiedBy
		
		
		-------------------------------------------------------------------------------
		-- Remove unspecified items (if applicable).
		-- <Summary>
		-- Removes items that are not specified in the range of items.
		-- </Summary>
		-------------------------------------------------------------------------------
		IF @DeleteUnspecified = 1
		BEGIN
			DELETE cea
			FROM creds.CredentialEntityApplication cea
			WHERE cea.CredentialEntityID = @CredentialEntityID
				AND ( @ApplicationID IS NULL OR 
					cea.ApplicationID NOT IN (SELECT Value FROM dbo.fnSplit(@ApplicationID, ',') WHERE ISNUMERIC(Value) = 1)
				)

		END
				
		-------------------------------------------------------------------------------
		-- Create new CredentialEntityApplication object(s).
		-------------------------------------------------------------------------------
		INSERT INTO creds.CredentialEntityApplication (
			CredentialEntityID,
			ApplicationID,	
			CreatedBy
		)
		OUTPUT inserted.CredentialEntityApplicationID INTO @tblOutput(CredentialEntityApplicationID)
		SELECT
			@CredentialEntityID AS CredentialEntityID,
			Value AS ApplicationID,			
			@CreatedBy
		FROM dbo.fnSplit(@ApplicationID, ',') app
			LEFT JOIN creds.CredentialEntityApplication cea
				ON cea.CredentialEntityID = @CredentialEntityID
					AND cea.ApplicationID = CASE WHEN ISNUMERIC(app.Value) = 1 THEN app.Value ELSE NULL END
		WHERE ISNUMERIC(app.Value) = 1
			AND cea.CredentialEntityApplicationID IS NULL
		
		-------------------------------------------------------------------------------
		-- Retrieve record identity value(s).
		-------------------------------------------------------------------------------
		SET @CredentialEntityApplicationIDList = (
			SELECT 
				CONVERT(VARCHAR(25), CredentialEntityApplicationID) + ','
			FROM @tblOutput
			FOR XML PATH('')
		);
		
		IF RIGHT(@CredentialEntityApplicationIDList, 1) = ','
		BEGIN
			SET @CredentialEntityApplicationIDList = LEFT(@CredentialEntityApplicationIDList, LEN(@CredentialEntityApplicationIDList) - 1);
		END

	--------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END		
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];
		
		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>'),
			@ErrorMessage = ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);	
	
	END CATCH
	
		
END
