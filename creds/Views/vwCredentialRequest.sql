﻿



CREATE VIEW creds.vwCredentialRequest AS
SELECT
	cr.CredentialRequestID,
	cr.RequestTypeID,
	rt.Code AS RequestTypeCode,
	rt.Name AS RequestTypeName,
	cr.ApplicationID,
	app.Code AS ApplicationCode,
	app.Name AS ApplicationName,
	sto.BusinessEntityID AS BusinessID,
	sto.Nabp,
	sto.Name AS StoreName,
	cr.CredentialEntityID,
	ce.CredentialEntityTypeID,
	cet.Code AS CredentialEntityTypeCode,
	cet.Name AS CredentialEntityTypeName,
	cr.ApplicationKey,
	cr.BusinessKey,
	cr.BusinessKeyType,
	cr.CredentialTypeKey,
	cr.CredentialKey,
	cr.PassKey,
	cr.SessionKey,
	cr.IsValid,
	cr.IPAddress,
	cr.BrowserName,
	cr.BrowserVersion,
	cr.DeviceName,
	cr.IsMobile,
	cr.UserAgent,
	cr.Comments,
	cr.rowguid,
	cr.DateCreated,
	cr.DateModified,
	cr.CreatedBy,
	cr.ModifiedBy
--SELECT *	
FROM creds.CredentialRequest cr
	LEFT JOIN dbo.RequestType rt
		ON rt.RequestTypeID = cr.RequestTypeID
	LEFT JOIN dbo.Application app
		ON app.ApplicationID = cr.ApplicationID
	LEFT JOIN dbo.Store sto
		ON sto.BusinessEntityID = cr.BusinessEntityID
	LEFT JOIN creds.CredentialEntity ce -- SELECT * FROM creds.CredentialEntity
		ON ce.CredentialEntityID = cr.CredentialEntityID
	LEFT JOIN creds.CredentialEntityType cet -- SELECT * FROM creds.CredentialEntityType
		ON cet.CredentialEntityTypeID = ce.CredentialEntityTypeID
	LEFT JOIN dbo.Person psn
		ON psn.BusinessEntityID = ce.PersonID
