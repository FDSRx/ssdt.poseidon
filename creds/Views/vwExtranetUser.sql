﻿

CREATE VIEW [creds].[vwExtranetUser]
AS
SELECT     ent.CredentialEntityID, biz.BusinessEntityID,sto.Nabp, COALESCE (sto.Name, chn.Name) AS BusinessName, 

						CONVERT(XML,
                          (SELECT     ApplicationID, Name, Code
                            FROM          creds.vwCredentialEntityApplication tmp
                            WHERE      tmp.CredentialEntityID = ent.CredentialEntityID FOR XML PATH('Application'), ROOT('Applications'))) AS ApplicationData, 
                            usr.Username, psn.PersonID AS PersonID, psn.FirstName, 
                      psn.MiddleName, psn.LastName, 
                      psn.BirthDate, 
                      psn.Gender, 
                      psn.AddressLine1, 
                      psn.AddressLine2, 
                      psn.City, 
                      psn.State, 
                      psn.PostalCode, 
                      psn.PrimaryEmail, 
                      psn.AlternateEmail, 
                      psn.HomePhone, 
                      psn.MobilePhone,
                      ent.DateCreated,
                      ent.DateModified,
                      ent.CreatedBy,
                      ent.ModifiedBy
/*select **/ FROM creds.CredentialEntity ent JOIN
                      creds.ExtranetUser usr ON ent.CredentialEntityID = usr.CredentialEntityID LEFT JOIN
                      dbo.BusinessEntity biz ON ent.BusinessEntityID = biz.BusinessEntityID LEFT JOIN
                      dbo.vwPerson psn ON ent.PersonID = psn.PersonID LEFT JOIN
                      dbo.Store sto ON biz.BusinessEntityID = sto.BusinessEntityID LEFT JOIN
                      dbo.Chain chn ON biz.BusinessEntityID = chn.BusinessEntityID



GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'creds', @level1type = N'VIEW', @level1name = N'vwExtranetUser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'creds', @level1type = N'VIEW', @level1name = N'vwExtranetUser';

