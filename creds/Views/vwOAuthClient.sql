﻿


CREATE ViEW [creds].[vwOAuthClient] AS
SELECT
	oauth.CredentialEntityID,
	credapp.ApplicationID,
	app.Code AS ApplicationCode,
	app.Name AS ApplicationName,
	sto.BusinessEntityID AS BusinessID,
	sto.Nabp,
	sto.Name AS BusinessName,
	psn.FirstName AS FirstName,
	psn.LastName AS LastName,
	oauth.ClientID,
	oauth.SecretKey,
	oauth.Name AS Name,
	oauth.Description,
	--CONVERT(XML, (
	--	SELECT
	--		ca.ApplicationID,
	--		a.Code,
	--		a.Name
	--	FROM creds.CredentialEntityApplication ca
	--		LEFT JOIN Application a
	--			ON a.ApplicationID = ca.ApplicationID
	--	WHERE ca.CredentialEntityID = oauth.CredentialEntityID
	--	FOR XML PATH('Application'), ROOT('Applications')
	--)) AS Applications,
	oauth.rowguid,
	oauth.DateCreated,
	oauth.DateModified,
	oauth.CreatedBy,
	oauth.ModifiedBy
--SELECT *
FROM creds.OAuthClient oauth
	JOIN creds.CredentialEntity ent -- SELECT * FROM creds.CredentialEntity
		ON ent.CredentialEntityID = oauth.CredentialEntityID
	LEFT JOIN dbo.Store sto
		ON sto.BusinessEntityID = ent.BusinessEntityID
	LEFT JOIN dbo.Person psn
		ON psn.BusinessEntityID = ent.PersonID
	LEFT JOIN creds.CredentialEntityApplication credapp
		ON credapp.CredentialEntityID = oauth.CredentialEntityID
	LEFT JOIN dbo.Application app
		ON app.ApplicationID = credapp.ApplicationID

