﻿

CREATE VIEW creds.vwLinkedProviderUser AS
SELECT
	lnkusr.LinkedProviderUserID,
	sto.BusinessEntityID AS BusinessID,
	sto.NABP,
	sto.Name AS BusinessName,
	lnkusr.CredentialEntityID,
	lnkusr.CredentialProviderID,
	prov.Code AS CredentialProviderCode,
	prov.Name AS CredentialProviderName,
	lnkusr.UserKey,
	extusr.Username AS ExtranetUserName,
	lnkusr.rowguid,
	lnkusr.DateCreated,
	lnkusr.DateModified,
	lnkusr.CreatedBy,
	lnkusr.ModifiedBy
--SELECT *
FROM creds.LinkedProviderUser lnkusr
	LEFT JOIN creds.CredentialEntity ent
		ON ent.CredentialEntityID = lnkusr.CredentialEntityID
	LEFT JOIN creds.CredentialProvider prov
		ON lnkusr.CredentialProviderID = prov.CredentialProviderID
	LEFT JOIN creds.ExtranetUser extusr
		ON extusr.CredentialEntityID = lnkusr.CredentialEntityID
	LEFT JOIN dbo.vwStore sto
		ON sto.BusinessEntityID = ent.BusinessEntityID