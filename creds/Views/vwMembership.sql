﻿
CREATE VIEW creds.vwMembership AS
SELECT 
	mem.MembershipID,
	mem.CredentialEntityID,
	ce.CredentialEntityTypeID,
	typ.Code AS CredentialEntityTypeCode,
	typ.Name AS CredentialEntityTypeName,
	ce.BusinessEntityID AS BusinessID,
	biz.Name AS BusinessName,
	ce.PersonID AS PersonID,
	psn.FirstName,
	psn.LastName,
	mem.EmailAddress,
	mem.PasswordHash,
	mem.PasswordSalt,
	mem.PasswordQuestionID,
	sq.Question AS PasswordQuestion,
	mem.PasswordAnswerHash,
	mem.PasswordAnswerSalt,
	mem.IsApproved,
	mem.IsLockedOut,
	mem.DateLastLoggedIn,
	mem.DateLastPasswordChanged,
	mem.DatePasswordExpires,
	mem.DateLastLockedOut,
	mem.DateLockOutExpires,
	mem.FailedPasswordAttemptCount,
	mem.FailedPasswordAnswerAttemptCount,
	mem.IsDisabled,
	mem.Comment,
	mem.rowguid,
	mem.DateCreated,
	mem.DateModified,
	mem.CreatedBy,
	mem.ModifiedBy
FROM creds.CredentialEntity ce -- SELECT * FROM creds.CredentialEntity
	JOIN creds.Membership mem -- SELECT * FROM creds.Membership
		ON mem.CredentialEntityID = ce.CredentialEntityID
	LEFT JOIN creds.CredentialEntityType typ
		ON typ.CredentialEntityTypeID = ce.CredentialEntityTypeID
	LEFT JOIN dbo.Business biz
		ON biz.BusinessEntityID = ce.BusinessEntityID
	LEFT JOIN dbo.Person psn
		ON psn.BusinessEntityID = ce.PersonID
	LEFT JOIN creds.SecurityQuestion sq -- SELECT * FROM creds.SecurityQuestion
		ON sq.SecurityQuestionID = mem.PasswordQuestionID