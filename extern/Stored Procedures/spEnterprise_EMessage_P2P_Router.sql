﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/20/2013
-- Description:	Routes messages to their appropriate recipients.
-- Change log:
-- 9/8/2016 - Nullified the code as it is currently not in a working state. The procedure has pointers
--		to LPS servers that are no longer within the FDS domain.

--	NOTE: Currently, this mechanism is a intermediate solution to having a full blown
--			messaging engine application that will handle all the business rules and message
--			traffic.
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- SELECT * FROM Athena.spt.Ticket ORDER BY TicketID DESC
-- =============================================
CREATE PROCEDURE [extern].[spEnterprise_EMessage_P2P_Router]
	@EMessage XML
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/*	
	
	/*
	-- Debug
	DECLARE @EMessageRaw VARCHAR(MAX) = CONVERT(VARCHAR(MAX), @EMessage);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = 'spEnterprise_EMessage_P2P_Router',
		@Message = 'spEnterprise_EMessage_P2P_Router fired by caller.',
		@Arguments = @EMessageRaw
	*/	

	----------------------------------------------------------
	-- If an EMessage is null, then do not bother to send
	----------------------------------------------------------
	IF @EMessage IS NULL
	BEGIN
		RETURN; 
	END
	
	-- Debug
	-- SELECT @EMessage AS EMessage
	
	----------------------------------------------------------
	-- Local variables
	----------------------------------------------------------
	DECLARE
		@msg XML = @EMessage,
		@SenderID INT,
		@SenderKey VARCHAR(255),
		@SourceKey VARCHAR(255),
		@ServiceCode VARCHAR(25),
		@ServiceID INT,
		@UserDefinedData XML,
		@RoutingData XML,
		@MessageTypeCode VARCHAR(25),
		@MessageTypeID INT,
		@MessageData XML,
		@FromAddress VARCHAR(255),
		@ToAddress VARCHAR(MAX),
		@Subject VARCHAR(1000),
		@Body VARChAR(MAX),
		@IsImportant BIT,
		@IsRead BIT,
		@IsConversation BIT,
		@CanRespond BIT,
		@ThreadKey UNIQUEIDENTIFIER,
		@MessageStatusID INT,
		@LanguageCode VARCHAR(10),
		@LanguageID INT,
		@DateCreated VARCHAR(255),
		@DateExpires VARCHAR(255),
		@DateDelay VARCHAR(255),
		@MinuteDelay INT,
		@MinuteExpires INT,		
		-- Routing information (destination route)
		@Route_Destination_IsConversation BIT,
		@Route_Destination_CanRespond BIT,
		@Route_Destination_ThreadKey UNIQUEIDENTIFIER,
		@Route_Destination_ServiceCode VARCHAR(25),
		@Route_Destination_MessageTypeCode VARCHAR(25),
		@Route_Destination_Address VARCHAR(256),
		@Route_Destination_ServiceID INT,
		@Route_Destination_MessageTypeID INT,
		@Route_Destination_Name VARCHAR(256),
		-- Routing information (source route)
		@Route_Source_IsConversation BIT,
		@Route_Source_CanRespond BIT,
		@Route_Source_ThreadKey UNIQUEIDENTIFIER,
		@Route_Source_ServiceCode VARCHAR(25),
		@Route_Source_MessageTypeCode VARCHAR(25),
		@Route_Source_Address VARCHAR(256),
		@Route_Source_ServiceID INT,
		@Route_Source_MessageTypeID INT,
		@Route_Source_Name VARCHAR(256),
		-- error
		@ErrorMessage NVARCHAR(4000)
			
	
	BEGIN TRY
	
			----------------------------------------------------------
			-- Parse EMessage.
			-- <Summary>
			-- Parses the EMessage into logical data elements.  Uses
			-- the information from the EMessage to correctly route the
			-- message to the intendend recipient.
			-- </Summary>
			----------------------------------------------------------
	
			-- EMessage high Level Data
			SET @SourceKey = @msg.value('(/EMessage/SourceKey)[1]', 'VARCHAR(255)');
			SET @SenderKey = @msg.value('(/EMessage/Sender/SenderKey)[1]', 'VARCHAR(255)');
			SET @LanguageCode = @msg.value('(/EMessage/LanguageCode)[1]', 'VARCHAR(255)');	
			SET @ServiceCode = @msg.value('(/EMessage/Sender/ServiceCode)[1]', 'VARCHAR(255)');	
			SET @MessageTypeCode = @msg.value('(/EMessage/MessageTypeCode)[1]', 'VARCHAR(255)');
			
			-- EMessage lower Level Data
			SET @FromAddress = @msg.value('(/EMessage/Sender/Address)[1]', 'VARCHAR(255)');
			SET @ToAddress = @msg.value('(/EMessage/Recipient/Address)[1]', 'VARCHAR(255)');
			
			-- EMessage message data
			SET @Subject = @msg.value('(/EMessage/Message/Subject)[1]', 'VARCHAR(1000)');
			SET @Body = @msg.value('(/EMessage/Message/Body/Text)[1]', 'VARCHAR(MAX)');
			SET @IsImportant = @msg.value('(/EMessage/Message/IsImportant)[1]', 'BIT');
			

			-- EMessage attributes
			SET @DateExpires = @msg.value('(/EMessage/DateExpires)[1]', 'VARCHAR(255)');
			SET @DateCreated = @msg.value('(/EMessage/DateCreated)[1]', 'VARCHAR(255)');
			SET @DateDelay =  @msg.value('(/EMessage/DateDelay)[1]', 'VARCHAR(255)');
			SET @MinuteDelay = @msg.value('(/EMessage/MinuteDelay)[1]', 'INT');
			SET @MinuteExpires = @msg.value('(/EMessage/MinuteExpires)[1]', 'INT');
			SET @RoutingData = 
				CASE
					WHEN @msg IS NOT NULL THEN ( SELECT e.query('.') FROM @msg.nodes('(EMessage/Routing)') AS EM(e) )
					ELSE @RoutingData
				END;
			SET @UserDefinedData = 
				CASE
					WHEN @msg IS NOT NULL THEN ( SELECT e.query('.') FROM @msg.nodes('(EMessage/UserDefinedData)') AS EM(e) )
					ELSE @UserDefinedData
				END;

			-- EMessage routing data or supplied routing data will always truncate parameter routing data.
			IF @RoutingData IS NOT NULL AND dbo.fnIsNullOrWhiteSpace(CONVERT(VARCHAR(MAX), @RoutingData)) = 0
			BEGIN
				DECLARE @r XML = @RoutingData;				
				
				-- Routing information (destination route)
				SET @Route_Destination_IsConversation = @r.value('data(Routing/DestinationRoute/Thread/IsConversation)[1]', 'BIT');
				SET @Route_Destination_CanRespond = @r.value('data(Routing/DestinationRoute/Thread/CanRespond)[1]', 'BIT');
				SET @Route_Destination_ThreadKey = dbo.fnIfNullOrEmpty(@r.value('data(Routing/DestinationRoute/Thread/ThreadKey)[1]', 'VARCHAR(50)'), NULL);
				SET @Route_Destination_MessageTypeCode = @r.value('data(Routing/DestinationRoute/MessageTypeCode)[1]', 'VARCHAR(25)');
				SET @Route_Destination_ServiceCode = @r.value('data(Routing/DestinationRoute/ServiceCode)[1]', 'VARCHAR(25)');
				SET @Route_Destination_Address = @r.value('data(Routing/DestinationRoute/Address)[1]', 'VARCHAR(256)');
				SET @Route_Destination_Name = @r.value('data(Routing/DestinationRoute/Name)[1]', 'VARCHAR(256)');

				-- Routing information (source route)
				SET @Route_Source_IsConversation = @r.value('data(Routing/SourceRoute/Thread/IsConversation)[1]', 'BIT');
				SET @Route_Source_CanRespond = @r.value('data(Routing/SourceRoute/Thread/CanRespond)[1]', 'BIT');
				SET @Route_Source_ThreadKey = dbo.fnIfNullOrEmpty(@r.value('data(Routing/SourceRoute/Thread/ThreadKey)[1]', 'VARCHAR(50)'), NULL);
				SET @Route_Source_MessageTypeCode = @r.value('data(Routing/SourceRoute/MessageTypeCode)[1]', 'VARCHAR(25)');
				SET @Route_Source_ServiceCode = @r.value('data(Routing/SourceRoute/ServiceCode)[1]', 'VARCHAR(25)');
				SET @Route_Source_Address = @r.value('data(Routing/SourceRoute/Address)[1]', 'VARCHAR(256)');
				SET @Route_Source_Name = @r.value('data(Routing/SourceRoute/Name)[1]', 'VARCHAR(256)');
			END


			---------------------------------------------------
			-- Look up codes
			-- <Summary>
			-- Translates EMessage codes back into their
			-- Surrogate keys
			-- </Summary>
			---------------------------------------------------
			SET @ServiceID = dbo.fnGetServiceID(@ServiceCode);
			SET @LanguageID = ISNULL(dbo.fnGetLanguageID(@LanguageCode), dbo.fnGetLanguageDefaultID());
			SET @MessageStatusID = msg.fnGetMessageStatusID('PNDNG');
			SET @MessageTypeID = dbo.fnGetMessageTypeID(@MessageTypeCode);
			
			-- Routing information (destination route)
			SET @Route_Destination_ServiceID = dbo.fnGetServiceID(@Route_Destination_ServiceCode);
			SET @Route_Destination_MessageTypeID = dbo.fnGetMessageTypeID(@Route_Destination_MessageTypeCode);
			
			-- Routing information (source route)
			SET @Route_Source_ServiceID = dbo.fnGetServiceID(@Route_Source_ServiceCode);
			SET @Route_Source_MessageTypeID = dbo.fnGetMessageTypeID(@Route_Source_MessageTypeCode);
			
			---------------------------------------------------
			-- Clean data
			---------------------------------------------------
			SELECT @DateCreated = CASE WHEN ISNULL(@DateCreated, '') = '' THEN NULL ELSE @DateCreated END;
			SELECT @DateExpires = CASE WHEN ISNULL(@DateExpires, '') = '' THEN NULL ELSE @DateExpires END;
			SELECT @DateDelay = CASE WHEN ISNULL(@DateExpires, '') = '' THEN NULL ELSE @DateDelay END;
			
			---------------------------------------------------
			-- Validate sender
			-- <Summary>
			-- Indicates whether the sender still has a vaild
			-- contract with our messaging service
			-- </Summary>
			---------------------------------------------------
			SELECT @SenderID = SenderID
			FROM msg.Sender 
			WHERE CONVERT(VARCHAR(255), SenderKey) = @SenderKey 
				AND ContractDateStart <= GETDATE() 
				AND ContractDateEnd > GETDATE()
				
			
			---------------------------------------------------------------------------------
			-- Route message
			-- <Summary>
			-- Interrogates the parsed data and determines the end point of the message.
			-- </Summary>
			---------------------------------------------------------------------------------
			-- Message Type Reference
			-- SELECT * FROM msg.MessageType
			
			-- IPP (v1.x) pharmacy system patient message.
			IF @MessageTypeCode = 'PATIENT'
			BEGIN
				
				-- Inbox Source and Reference Type
				-- SELECT * FROM IPP.dbo.InboxReferenceType
				-- SELECT * FROM IPP.dbo.InboxSource
				
				DECLARE 
					@PatientID INT = NULL,
					@PatientStoreID INT = NULL,
					@StoreID INT = NULL,
					@InboxReferenceTypeCode VARCHAR(25),
					@InboxSourceCode VARCHAR(25),
					@InboxReferenceTypeID INT = NULL,
					@InboxSourceID INT = NULL,
					@PatientEventTypeID INT,
					@PatientEventLogID INT
				
				SET @InboxReferenceTypeID = @msg.value('(EMessage/UserDefinedData/InboxReferenceTypeID)[1]', 'INT');
				
				SET	@InboxSourceID = dbo.fnIfNullOrWhiteSpace(@msg.value('(EMessage/UserDefinedData/InboxSourceID)[1]', 'INT'),
												IPP.dbo.fnGetInboxSourceID('PHRM'));
												
				SET @PatientID = @msg.value('(EMessage/UserDefinedData/PatientID)[1]', 'INT');
				SET @PatientStoreID = @msg.value('(EMessage/UserDefinedData/PatientStoreID)[1]', 'INT');
				SET @StoreID = @msg.value('(EMessage/UserDefinedData/StoreID)[1]', 'INT');
				
				SET @PatientEventTypeID = IPP.dbo.fnGetPatientEventTypeID('SECMSGRCVD');
												
				EXEC IPP.dbo.spPatientInbox_Send
					@FromID = NULL,
					@ToAddress = @Route_Destination_Address,
					@Subject = @Subject,
					@Body = @Body,
					@InboxSourceID = @InboxSourceID,
					@InboxReferenceTypeID = @InboxReferenceTypeID,
					@IsImportant = @IsImportant,
					@IsConversation = @Route_Destination_IsConversation,
					@CanRespond = @Route_Destination_CanRespond,
					@ThreadKey = @Route_Source_ThreadKey,
					@RoutingData = @RoutingData,
					@MessageData = @EMessage
				
				-- Attempt to trigger a secure message event.	
				BEGIN TRY
					-- Trigger patient secure message event.
					EXEC IPP.dbo.spPatientEventLog_Insert 
							@StoreID = @StoreID, 
							@Event = @PatientEventTypeID, 
							@PatientID = @PatientID, 
							@SourcePatientID = NULL, 
							@AdditionalData = NULL, 
							@PatientEventTokenID = NULL,
							@PatientEventLogID = @PatientEventLogID OUTPUT
				END TRY
				BEGIN CATCH
					EXEC spLogError
				END CATCH
				
			END
			-- Support ticket messaging system conversation item.
			ELSE IF @MessageTypeCode = 'TCKTCNVSN'
			BEGIN

				EXEC Athena.spt.spTicketConversation_Send
					@TicketKey = @Route_Destination_ThreadKey,
					@Message = @Body,
					@MessageData = @EMessage
			
			END
			ELSE IF @MessageTypeCode = 'EMAIL'
			BEGIN			

				-- Declare email specific variables
				DECLARE
					@EmailSubject VARCHAR(1000) = NULL,
					@EmailBody VARCHAR(8000) = NULL,
					@FromName VARCHAR(256),
					@UserDefinedFromAddress VARCHAR(256),
					@FromAddress_Default VARCHAR(256) = 'noreply@myPharmacyConnect.com',
					@FromName_Default VARCHAR(256) = 'myPharmacyConnect',
					@EmailDisclaimer_Footer VARCHAR(MAX) = msg.fnGetMessagePart('Common_Email_Footer_Disclaimer', DEFAULT);
					
				SET @EmailSubject = @msg.value('(EMessage/UserDefinedData/EmailSubject)[1]', 'VARCHAR(1000)');
				SET @EmailBody = @msg.value('(EMessage/UserDefinedData/EmailBody)[1]', 'VARCHAR(8000)');
				SET @UserDefinedFromAddress = @msg.value('(EMessage/UserDefinedData/EmailFromAddress)[1]', 'VARCHAR(256)');
				SET @FromName = @msg.value('(EMessage/UserDefinedData/EmailFromName)[1]', 'VARCHAR(256)');
				
				-- Generic from address.			
				SET @FromAddress = CASE WHEN dbo.fnIsNullOrEmpty(@UserDefinedFromAddress) = 0 THEN @UserDefinedFromAddress ELSE @FromAddress END;
				SET @FromAddress = CASE WHEN dbo.fnIsNullOrEmpty(@FromAddress) = 0 THEN @FromAddress ELSE @FromAddress_Default END;
				SET @FromAddress = CASE WHEN dbo.fnIsEmailValid(@FromAddress) = 1 THEN @FromAddress ELSE @FromAddress_Default END;
				-- Gerneric from name. 
				SET @FromName = CASE WHEN dbo.fnIsNullOrEmpty(@FromName) = 0 THEN @FromName ELSE @FromName_Default END;
				
				-- Verify we have a recipient email address.
				IF dbo.fnIsNullOrEmpty(@Route_Destination_Address) = 1
				BEGIN				
					RAISERROR(
						'Object reference is not set to an instance of an object.  An email address is required to send an email.',
						16,
						1
					);					
				END
				
				-- Verify we have a recipient email address.
				IF dbo.fnIsNullOrWhiteSpace(@EmailBody) = 1
				BEGIN				
					RAISERROR(
						'Object reference is not set to an instance of an object.  An email body is required to send an email.',
						16,
						1
					);					
				END
				
				-- Debug
				-- SELECT @EmailSubject AS EmailSubject, @EmailBody AS EmailBody, @FromAddress AS FromAddress, @FromName AS FromName
				
				
				----------------------------------------------------------------------
				-- Append email disclaimer
				----------------------------------------------------------------------
				SET @EmailBody = @EmailBody + '<br/><br/><br/><br/><br/><hr/>' + ISNULL(@EmailDisclaimer_Footer, '');
				
				
				----------------------------------------------------------------------
				-- Send email
				----------------------------------------------------------------------
				EXEC Athena.utl.spSendMail
					@Recipients = @Route_Destination_Address,
					@Subject = @EmailSubject,
					@Message = @EmailBody,
					@From_Address = @FromAddress,
					@From_Name = @FromName

					
			END

	
	
	END TRY
	BEGIN CATCH

		SET @ErrorMessage = ERROR_MESSAGE();
		
		EXEC dbo.spLogError
		
		RAISERROR(
			@ErrorMessage,
			16,
			1
		);
	
	END CATCH

	*/
END
