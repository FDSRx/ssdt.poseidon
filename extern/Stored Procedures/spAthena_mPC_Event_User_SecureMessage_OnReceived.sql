﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/6/2013
-- Description:	Calls athena event engine to trigger message is received.
-- Change log:
-- 9/8/2016 - Nullified the code as it is currently not in a working state. The procedure has pointers
--		to LPS servers that are no longer within the FDS domain.

-- SAMPLE CALL: extern.spAthena_mPC_Event_User_SecureMessage_OnReceived @PersonID = 59201
-- SELECT * FROM Athena.dbo.Application
-- SELECT * FROM Athena.dbo.Service
-- SELECT * FROM Athena.creds.vwInternetUser
-- SELECT TOP(10) * FROM Athena.dbo.UserEventLog ORDER BY UserEventLogID DESC
-- =============================================
CREATE PROCEDURE [extern].[spAthena_mPC_Event_User_SecureMessage_OnReceived]
	@PersonID BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*

	----------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------
	DECLARE 
		@ApplicationID INT = Athena.dbo.fnGetApplicationID('MPC'),
		@ServiceID INT = dbo.fnGetServiceID('MPC'),
		@EventID INT = Athena.dbo.fnGetUserEventID('SECMSGRCVD'),
		@PersonServiceID INT
		
	----------------------------------------------------------------
	-- Temporary resources
	----------------------------------------------------------------
	DECLARE @tblUserEntityIDList AS TABLE (
		Idx INT IDENTITY(1,1),
		UserEntityID BIGINT,
		BusinessEntityID INT
	);

	----------------------------------------------------------------
	-- Verify person has mPC service 
	----------------------------------------------------------------
	SELECT
		@PersonServiceID = BusinessEntityServiceID
	FROM dbo.BusinessEntityService
	WHERE BusinessEntityID = @PersonID
		AND ServiceID = @ServiceID
	
	
	IF @PersonServiceID IS NULL
	BEGIN		
		RETURN; -- Person does not have mPC service.  Do not fire event.		
	END
	
	
	----------------------------------------------------------------
	-- Get user accounts associated with person message box.
	----------------------------------------------------------------	
	INSERT INTO @tblUserEntityIDList (
		UserEntityID,
		BusinessEntityID
	)
	SELECT
		UserEntityID,
		BusinessEntityID
	--SELECT *
	FROM Athena.creds.vwInternetUser
	WHERE ApplicationID = @ApplicationID
		AND PersonID = @PersonID
	
	
	----------------------------------------------------------------
	-- Simulate cursor operation
	----------------------------------------------------------------	
	DECLARE
		@Idx INT = 0,
		@UserEntityID BIGINT,
		@BusinessEntityID INT
		
	SELECT TOP(1)
		@Idx = Idx,
		@UserEntityID = UserEntityID,
		@BusinessEntityID = BusinessEntityID
	FROM @tblUserEntityIDList
	WHERE Idx > @Idx
	ORDER BY Idx ASC
	
	-- Loop through records.
	WHILE @@ROWCOUNT = 1
	BEGIN
	
		----------------------------------------------------------------
		-- Trigger the event
		----------------------------------------------------------------			
		-- Create an event
		EXEC Athena.dbo.spUser_EventLog_Create
			@UserEntityID = @UserEntityID,
			@BusinessEntityID = @BusinessEntityID,
			@EventID = @EventID
		
		------------------------------------------------------
		-- ** No code beyound this point.  Simulates fetch next.
		------------------------------------------------------
			SELECT TOP(1)
				@Idx = Idx,
				@UserEntityID = UserEntityID,
				@BusinessEntityID = BusinessEntityID
			FROM @tblUserEntityIDList
			WHERE Idx > @Idx
			ORDER BY Idx ASC
	END
		

	*/

END
