﻿CREATE TABLE [media].[MediaType] (
    [MediaTypeID]  INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_MediaType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_MediaType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_MediaType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_MediaType] PRIMARY KEY CLUSTERED ([MediaTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MediaType_Code]
    ON [media].[MediaType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MediaType_Name]
    ON [media].[MediaType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MediaType_rowguid]
    ON [media].[MediaType]([rowguid] ASC);

