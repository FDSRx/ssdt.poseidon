﻿CREATE TABLE [config].[ConfigurationKey] (
    [ConfigurationKeyID] INT              IDENTITY (1, 1) NOT NULL,
    [Name]               VARCHAR (256)    NOT NULL,
    [Description]        VARCHAR (1000)   NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_ConfigurationKey_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_ConfigurationKey_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_ConfigurationKey_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_ConfigurationKey] PRIMARY KEY CLUSTERED ([ConfigurationKeyID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ConfigurationKey_Name]
    ON [config].[ConfigurationKey]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ConfigurationKey_rowguid]
    ON [config].[ConfigurationKey]([rowguid] ASC);

