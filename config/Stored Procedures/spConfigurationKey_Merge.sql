﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/10/2015
-- Description:	Creates/Updates a ConfigurationKey object.
-- SAMPLE CALL:
/*
DECLARE
	@ConfigurationKeyID INT = NULL,
	@Name VARCHAR(256) = 'CredentialLockOutMaxAttempts',
	@Description VARCHAR(1000) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = NULL

EXEC config.spConfigurationKey_Merge
	@ConfigurationKeyID = @ConfigurationKeyID OUTPUT,
	@Name = @Name OUTPUT,
	@Description = @Description,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@Exists = @Exists OUTPUT

SELECT @ConfigurationKeyID AS ConfigurationKeyID, @Name AS Name, @Exists AS IsFound

*/

-- SELECT * FROM config.ConfigurationKey
-- =============================================
CREATE PROCEDURE [config].[spConfigurationKey_Merge]
	@ConfigurationKeyID INT = NULL OUTPUT,
	@Name VARCHAR(256) OUTPUT,
	@Description VARCHAR(1000) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------
	SET @Exists = 0;

	-----------------------------------------------------------------------------------------
	-- Determine if the object exists.
	-----------------------------------------------------------------------------------------
	EXEC config.spConfigurationKey_Exists
		@ConfigurationKeyID = @ConfigurationKeyID OUTPUT,
		@Name = @Name OUTPUT,
		@Exists = @Exists OUTPUT
	
	IF @ConfigurationKeyID IS NULL
	BEGIN
		-----------------------------------------------------------------------------------------
		-- Create new object.
		-----------------------------------------------------------------------------------------
		EXEC config.spConfigurationKey_Create
			@Name = @Name,
			@Description = @Description,
			@CreatedBy = @CreatedBy,
			@ConfigurationKeyID = @ConfigurationKeyID OUTPUT	
	END
	ELSE
	BEGIN
		-----------------------------------------------------------------------------------------
		-- Update object.
		-----------------------------------------------------------------------------------------
		-- Do nothing for now as there is no update procedure in place.
		SET @Exists = 1; -- dummy assignment for IF placeholder.
	END
		

	
	
END
