﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/10/2015
-- Description:	Creates a new ConfigurationKey object.

-- SELECT * FROM config.ConfigurationKey
-- =============================================
CREATE PROCEDURE config.spConfigurationKey_Create
	@Name VARCHAR(256),
	@Description VARCHAR(1000) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ConfigurationKeyID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------
	SET @ConfigurationKeyID = NULL;
	
	-----------------------------------------------------------------------------------------
	-- Create new object.
	-----------------------------------------------------------------------------------------
	INSERT INTO config.ConfigurationKey (
		Name,
		Description,
		CreatedBy
	)
	SELECT
		@Name AS Name,
		@Description AS Description,
		@CreatedBy AS CreatedBy
	
	-- Retrieve record identity.
	SET @ConfigurationKeyID = SCOPE_IDENTITY();
	
	
END
