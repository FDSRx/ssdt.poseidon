﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/10/2015
-- Description:	Determines if the ConfigurationKey object exists.

-- SELECT * FROM config.ConfigurationKey
-- =============================================
CREATE PROCEDURE [config].[spConfigurationKey_Exists]
	@ConfigurationKeyID INT = NULL OUTPUT,
	@Name VARCHAR(256) = NULL OUTPUT,	
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------
	SET @Exists = 0;
	
	-----------------------------------------------------------------------------------------
	-- Create new object.
	-----------------------------------------------------------------------------------------
	SELECT 
		@ConfigurationKeyID = ConfigurationKeyID,
		@Name = Name
	FROM config.ConfigurationKey
	WHERE ConfigurationKeyID = @ConfigurationKeyID
		OR Name = @Name
		
	-- Set the existence flag.
	IF @@ROWCOUNT > 0
	BEGIN
		SET @Exists = 1;
	END
	
	
END
