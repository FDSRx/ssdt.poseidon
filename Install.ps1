param($installPath, $toolsPath, $package, $project)

$rootDir = (Get-Item $installPath).parent.parent.fullname

$deployTarget = "$rootDir\Databases\"

$deploySource = join-path $installPath 'content'

if (!(test-path $deployTarget)) {
	mkdir $deployTarget
}

Copy-Item "$deploySource/*" $deployTarget -Recurse -Force

$solution = Get-Interface $dte.Solution ([EnvDTE80.Solution2])


$deployFolder = $solution.Projects | where-object { $_.ProjectName -eq "Databases" } | select -first 1
if(!$deployFolder) {
	$deployFolder = $solution.AddSolutionFolder("Databases")
}

$folderItems = Get-Interface $deployFolder.ProjectItems ([EnvDTE.ProjectItems])
ls $deployTarget | foreach-object { 
	$folderItems.AddFromFile($_.FullName) > $null
	$item = $project.ProjectItems.Item($_.Name)
	$copyToOutput = $item.Properties.Item("CopyToOutputDirectory")
	$copyToOutput.Value = 1 #Copy always
	$buildAction = $item.Properties.Item("BuildAction")
	$buildAction.Value = 2 #Content
} > $null
