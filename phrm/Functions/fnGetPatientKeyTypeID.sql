﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/16/2015
-- Description:	Returns the PatientKeyType object identifier.
-- SAMPLE CALL: SELECT phrm.fnGetPatientKeyTypeID('SRC');

-- SElECT * FROM phrm.PatientKeyType
-- =============================================
CREATE FUNCTION phrm.[fnGetPatientKeyTypeID]
(
	@Key VARCHAR(25)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @ID INT;

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT TOP 1 PatientKeyTypeID FROM phrm.PatientKeyType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @ID = (SELECT TOP 1 PatientKeyTypeID FROM phrm.PatientKeyType WHERE PatientKeyTypeID = @Key);
	END

	-- Return the result of the function
	RETURN @ID;

END

