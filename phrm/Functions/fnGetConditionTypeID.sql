﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/7/2014
-- Description:	Get ConditionTypeID from Condition object.
-- SAMPLE CALL: SELECT [phrm].[fnGetConditionTypeID]('OTR');
-- SAMPLE CALL: SELECT [phrm].[fnGetConditionTypeID](1);
-- =============================================
CREATE FUNCTION [phrm].fnGetConditionTypeID
(
	@Key VARCHAR(15)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT ConditionTypeID FROM phrm.ConditionType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT ConditionTypeID FROM phrm.ConditionType WHERE ConditionTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
