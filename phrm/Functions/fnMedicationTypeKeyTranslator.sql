﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description: Gets the translated MedicationType key or keys.
-- References: dbo.fnSlice
-- SAMPLE CALL: SELECT phrm.fnMedicationTypeKeyTranslator(1, 'MedicationTypeID')
-- SAMPLE CALL: SELECT phrm.fnMedicationTypeKeyTranslator('OTC', 'Code')
-- SAMPLE CALL: SELECT phrm.fnMedicationTypeKeyTranslator('RX', DEFAULT)
-- SAMPLE CALL: SELECT phrm.fnMedicationTypeKeyTranslator(1, DEFAULT)
-- SAMPLE CALL: SELECT phrm.fnMedicationTypeKeyTranslator('1,2,3', DEFAULT)
-- SAMPLE CALL: SELECT phrm.fnMedicationTypeKeyTranslator('RX,OTC', DEFAULT)
-- SAMPLE CALL: SELECT phrm.fnMedicationTypeKeyTranslator('Over the counter,Prescription', 'NameList')
-- SELECT * FROM phrm.MedicationType
-- =============================================
CREATE FUNCTION [phrm].[fnMedicationTypeKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	-- Remove trailing comma (if applicable)
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END
	
	-- Do not perform the lookup if no value was supplied.
	IF LTRIM(RTRIM(ISNULL(@Key, ''))) = ''
	BEGIN
		RETURN NULL;
	END		
	
	-- Perform conditional lookup
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'MedicationTypeID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, MedicationTypeID) 
				FROM phrm.MedicationType
				WHERE MedicationTypeID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, MedicationTypeID) 
				FROM phrm.MedicationType
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, MedicationTypeID) 
				FROM phrm.MedicationType WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, MedicationTypeID) 
				FROM phrm.MedicationType
				WHERE MedicationTypeID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, MedicationTypeID) 
				FROM phrm.MedicationType
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'MedicationTypeIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, MedicationTypeID), '') + ','
				FROM phrm.MedicationType
				WHERE MedicationTypeID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'MedicationTypeCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, MedicationTypeID), '') + ','
				FROM phrm.MedicationType
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'MedicationTypeNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, MedicationTypeID), '') + ','
				FROM phrm.MedicationType
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, MedicationTypeID), '') + ','
				FROM phrm.MedicationType
				WHERE MedicationTypeID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, MedicationTypeID), '') + ','
				FROM phrm.MedicationType
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	-- Remove trailing comma (if applicable)
	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
