﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description:	Returns the PrescriberTypeID.
-- SAMPLE CALL: SELECT phrm.fnGetPrescriberTypeID(1);
-- SAMPLE CALL: SELECT phrm.fnGetPrescriberTypeID('PA');
-- SELECT * FROM phrm.PrescriberType
-- =============================================
CREATE FUNCTION phrm.fnGetPrescriberTypeID
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT PrescriberTypeID FROM phrm.PrescriberType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT PrescriberTypeID FROM phrm.PrescriberType WHERE PrescriberTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
