﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description:	Returns the MedicationTypeID.
-- SAMPLE CALL: SELECT phrm.fnGetMedicationTypeID(1);
-- SAMPLE CALL: SELECT phrm.fnGetMedicationTypeID('NONRX');
-- SELECT * FROM phrm.MedicationType
-- =============================================
CREATE FUNCTION phrm.fnGetMedicationTypeID
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT MedicationTypeID FROM phrm.MedicationType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT MedicationTypeID FROM phrm.MedicationType WHERE MedicationTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
