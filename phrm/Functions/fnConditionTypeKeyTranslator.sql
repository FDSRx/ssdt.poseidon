﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description: Gets the translated ConditionType key or keys.
-- References: dbo.fnSlice
-- SAMPLE CALL: SELECT phrm.fnConditionTypeKeyTranslator(1, 'ConditionTypeID')
-- SAMPLE CALL: SELECT phrm.fnConditionTypeKeyTranslator('ALG', 'Code')
-- SAMPLE CALL: SELECT phrm.fnConditionTypeKeyTranslator('DIS', DEFAULT)
-- SAMPLE CALL: SELECT phrm.fnConditionTypeKeyTranslator(1, DEFAULT)
-- SAMPLE CALL: SELECT phrm.fnConditionTypeKeyTranslator('1,2,3', DEFAULT)
-- SAMPLE CALL: SELECT phrm.fnConditionTypeKeyTranslator('ALG,DIS', DEFAULT)
-- SAMPLE CALL: SELECT phrm.fnConditionTypeKeyTranslator('Disease,Allergy', 'NameList')
-- SELECT * FROM phrm.ConditionType
-- =============================================
CREATE FUNCTION phrm.[fnConditionTypeKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END		
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'ConditionTypeID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ConditionTypeID) 
				FROM phrm.ConditionType
				WHERE ConditionTypeID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ConditionTypeID) 
				FROM phrm.ConditionType
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ConditionTypeID) 
				FROM phrm.ConditionType WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ConditionTypeID) 
				FROM phrm.ConditionType
				WHERE ConditionTypeID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ConditionTypeID) 
				FROM phrm.ConditionType
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'ConditionTypeIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ConditionTypeID), '') + ','
				FROM phrm.ConditionType
				WHERE ConditionTypeID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'ConditionTypeCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ConditionTypeID), '') + ','
				FROM phrm.ConditionType
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'ConditionTypeNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ConditionTypeID), '') + ','
				FROM phrm.ConditionType
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ConditionTypeID), '') + ','
				FROM phrm.ConditionType
				WHERE ConditionTypeID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ConditionTypeID), '') + ','
				FROM phrm.ConditionType
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
