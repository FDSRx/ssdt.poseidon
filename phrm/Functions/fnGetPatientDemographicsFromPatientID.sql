﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/17/2015
-- Description: Gets a standardized set of patient data based on the provided Patient object identifier.
-- Change Log:
-- 12/22/2015 - dhughes - Modified the "Store" join to become a subquery that brings back a distinct list of NABPs to combat
--					a potentional duplicate issue that needs to be resolved.
-- SAMPLE CALL: 
/*
	SELECT * FROM phrm.fnGetPatientDemographicsFromPatientID(30)
*/

-- SELECT * FROM phrm.Patient
-- SELECT * FROM phrm.vwPatient
-- =============================================
CREATE FUNCTION [phrm].[fnGetPatientDemographicsFromPatientID](
	@PatientID BIGINT
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT  
		pat.PatientID, 
		pat.BusinessEntityID AS StoreID, 
		sto.NABP, 
		sto.SourceStoreID, 
		sto.Name AS StoreName, 
		pat.PersonID, 
		pat.SourcePatientKey,
		psn.Title, 
		psn.FirstName, 
		psn.LastName, 
		psn.MiddleName, 
		psn.Suffix, 
		psn.BirthDate, 
		psn.GenderID,
		gdr.Code AS GenderCode,
		gdr.Name AS GenderName, 
		psn.LanguageID,
		lang.Code AS LanguageCode,
		lang.Name AS LanguageName,
		padr.AddressLine1, 
		padr.AddressLine2, 
		padr.City, 
		psp.StateProvinceID,
		psp.StateProvinceCode AS State, 
		padr.PostalCode, 
		pp.PhoneNumber AS HomePhone, 
		ISNULL(pp.IsCallAllowed, 0) AS IsCallAllowedToHomePhone, 
		ISNULL(pp.IsTextAllowed, 0) AS IsTextAllowedToHomePhone, 
		ISNULL(pp.IsPreferred, 0) AS IsHomePhonePreferred, 
		mp.PhoneNumber AS MobilePhone, 
		ISNULL(mp.IsCallAllowed, 0) AS IsCallAllowedToMobilePhone, 
		ISNULL(mp.IsTextAllowed, 0) AS IsTextAllowedToMobilePhone, 
		ISNULL(mp.IsPreferred, 0) AS IsMobilePhonePreferred, 
		pe.EmailAddress AS PrimaryEmail, 
		ISNULL(pe.IsEmailAllowed, 0) AS IsEmailAllowedToPrimaryEmail, 
		ISNULL(pe.IsPreferred, 0) AS IsPrimaryEmailPreferred, 
		ae.EmailAddress AS AlternateEmail, 
		ISNULL(ae.IsEmailAllowed, 0) AS IsEmailAllowedToAlternateEmail, 
		ISNULL(ae.IsPreferred, 0) AS IsAlternateEmailPreferred, 
		pat.rowguid, 
		pat.DateCreated, 
		pat.DateModified, 
		pat.CreatedBy, 
		pat.ModifiedBy
	FROM phrm.Patient AS pat    
		INNER JOIN dbo.Person AS psn WITH (NOLOCK) 
			ON pat.PersonID = psn.BusinessEntityID
		INNER JOIN (
			SELECT
				Nabp,
				MAX(SourceStoreID) AS SourceStoreID,
				MAX(BusinessEntityID) AS BusinessEntityID,
				MAX(Name) AS Name
			FROM dbo.Store (NOLOCK)
			GROUP BY Nabp
		) AS sto 
			ON pat.BusinessEntityID = sto.BusinessEntityID
		LEFT JOIN dbo.Gender AS gdr WITH (NOLOCK) 
			ON psn.GenderID = gdr.GenderID 
		LEFT JOIN dbo.Language AS lang WITH (NOLOCK) 
			ON psn.LanguageID = lang.LanguageID 
		LEFT JOIN dbo.BusinessEntityAddress adr WITH (NOLOCK) 
			ON psn.BusinessEntityID = adr.BusinessEntityID 
				AND adr.AddressTypeID = (SELECT TOP 1 AddressTypeID FROM dbo.AddressType WHERE Code = 'HME') -- SELECT * FROM dbo.AddressType
		LEFT JOIN dbo.Address AS padr WITH (NOLOCK) 
			ON padr.AddressID = adr.AddressID
		LEFT JOIN dbo.StateProvince AS psp WITH (NOLOCK) 
			ON psp.StateProvinceID = padr.StateProvinceID	
		LEFT JOIN dbo.BusinessEntityPhone AS pp WITH (NOLOCK) 
			ON psn.BusinessEntityID = pp.BusinessEntityID 
				AND pp.PhoneNumberTypeID = (SELECT TOP 1 PhoneNumberTypeID FROM dbo.PhoneNumberType WHERE Code = 'HME') -- SELECT * FROM dbo.PhoneNumberType
		LEFT JOIN dbo.BusinessEntityPhone AS mp WITH (NOLOCK) 
			ON psn.BusinessEntityID = mp.BusinessEntityID 
				AND mp.PhoneNumberTypeID = (SELECT TOP 1 PhoneNumberTypeID FROM dbo.PhoneNumberType WHERE Code = 'MBL') -- SELECT * FROM dbo.PhoneNumberType
		LEFT JOIN dbo.BusinessEntityEmailAddress AS pe WITH (NOLOCK) 
			ON psn.BusinessEntityID = pe.BusinessEntityID 
				AND pe.EmailAddressTypeID = (SELECT TOP 1 EmailAddressTypeID FROM dbo.EmailAddressType WHERE Code = 'PRIM') -- SELECT * FROM dbo.EmailAddressType
		LEFT JOIN dbo.BusinessEntityEmailAddress AS ae WITH (NOLOCK) -- SELECT * FROM dbo.BusinessEntityEmailAddress
			ON  psn.BusinessEntityID = ae.BusinessEntityID 
				AND ae.EmailAddressTypeID = (SELECT TOP 1 EmailAddressTypeID FROM dbo.EmailAddressType WHERE Code = 'ALT') -- SELECT * FROM dbo.EmailAddressType										  
	WHERE pat.PatientID = @PatientID

)