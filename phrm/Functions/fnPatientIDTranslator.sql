﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description: Gets the patient ID
-- SAMPLE CALL:
/*

SELECT phrm.fnPatientIDTranslator(10684, NULL, 30138, 'SourcePatientKey')
SELECT phrm.fnPatientIDTranslator('7871787', 'NABP', 30138, 'SourcePatientKey')

*/

-- SELECT * FROM dbo.Store WHERE BusinessEntityID = 10684
-- SELECT * FROM phrm.Patient
-- SELECT * FROM phrm.PatientDenormalized
-- =============================================
CREATE FUNCTION [phrm].[fnPatientIDTranslator]
(
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50)
)
RETURNS BIGINT
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Id BIGINT,
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType)
		
	
	SET @Id = 
		CASE
			WHEN @PatientKeyType = 'PatientID' AND ISNUMERIC(@PatientKey) = 1 THEN (
				SELECT TOP 1 PatientID 
				--SELECT TOP 1 *
				FROM phrm.Patient 
				WHERE PatientID = CONVERT(BIGINT, @PatientKey) 
					AND (@BusinessID IS NOT NULL AND BusinessEntityID = @BusinessID)
			)
			WHEN @PatientKeyType IN ('SourcePatientKey', 'SourcePatientID', 'SRC', 'SPK', 'SPID') THEN (
				SELECT TOP 1 PatientID 
				FROM phrm.Patient 
				WHERE BusinessEntityID = @BusinessID 
					AND SourcePatientKey = @PatientKey
			)
			WHEN @PatientKeyType = 'PersonID' AND ISNUMERIC(@PatientKey) = 1 THEN (
				SELECT TOP 1 PatientID 
				FROM phrm.Patient 
				WHERE BusinessEntityID = @BusinessID
					AND PersonID = CONVERT(BIGINT, @PatientKey)
			)
			WHEN @PatientKeyType IN ('rowguid', 'Token') AND dbo.fnIsUniqueIdentifier(@PatientKey) = 1 THEN (
				SELECT TOP 1 PatientID 
				FROM phrm.Patient 
				WHERE BusinessEntityID = ISNULL(@BusinessID, BusinessEntityID) 
					AND rowguid = CONVERT(UNIQUEIDENTIFIER, @PatientKey)
			)
			ELSE NULL
		END	

	-- Return the result of the function
	RETURN @Id;

END
