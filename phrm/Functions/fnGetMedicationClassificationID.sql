﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description:	Returns the MedicationClassificationID.
-- SAMPLE CALL: SELECT phrm.fnGetMedicationClassificationID(1);
-- SAMPLE CALL: SELECT phrm.fnGetMedicationClassificationID('NONRX');
-- SELECT * FROM phrm.MedicationClassification
-- =============================================
CREATE FUNCTION phrm.fnGetMedicationClassificationID
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT MedicationClassificationID FROM phrm.MedicationClassification WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT MedicationClassificationID FROM phrm.MedicationClassification WHERE MedicationClassificationID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
