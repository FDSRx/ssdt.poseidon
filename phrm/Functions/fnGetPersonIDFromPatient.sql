﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/19/2015
-- Description: Gets the PersonID from the patient criteria.
-- SAMPLE CALL: SELECT phrm.fnGetPersonIDFromPatient(10684, NULL, 30138, 'SourcePatientKey')

-- SELECT * FROM dbo.Store WHERE BusinessEntityID = 10684
-- SELECT * FROM phrm.Patient
-- =============================================
CREATE FUNCTION [phrm].[fnGetPersonIDFromPatient]
(
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50)
)
RETURNS BIGINT
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Id BIGINT,
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType)
		
	
	SET @Id = 
		CASE
			WHEN @PatientKeyType = 'PatientID' AND ISNUMERIC(@PatientKey) = 1 THEN (
				SELECT TOP 1
					PersonID 
				FROM phrm.Patient 
				WHERE BusinessEntityID = ISNULL(@BusinessID, BusinessEntityID) 
					AND PatientID = @PatientKey
			)
			WHEN @PatientKeyType IN ('SourcePatientKey', 'SourcePatientID', 'SPK', 'SK', 'SRC') THEN (
				SELECT TOP 1 
					PersonID 
				FROM phrm.Patient 
				WHERE BusinessEntityID = @BusinessID 
					AND SourcePatientKey = @PatientKey
			)
			WHEN @PatientKeyType = 'PersonID' AND ISNUMERIC(@PatientKey) = 1 THEN (
				SELECT TOP 1 
					PersonID 
				FROM phrm.Patient 
				WHERE BusinessEntityID = @BusinessID
					AND PersonID = @PatientKey
			)
			WHEN @PatientKeyType IN ('rowguid', 'Token') THEN (
				SELECT TOP 1 
					PersonID 
				FROM phrm.Patient 
				WHERE BusinessEntityID = ISNULL(@BusinessID, BusinessEntityID) 
					AND rowguid = @PatientKey
			)
			ELSE NULL
		END	

	-- Return the result of the function
	RETURN @Id;

END
