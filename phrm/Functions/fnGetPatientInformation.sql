﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/11/2014
-- Description:	Gets the standard patient data for the provided id
-- SAMPLE CALL: SELECT * FROM phrm.fnGetPatientInformation(135590)

-- SELECT * FROM phrm.Patient
-- SELECT * FROM phrm.vwPatient
-- =============================================
CREATE FUNCTION [phrm].[fnGetPatientInformation]
(	
	@PatientKey VARCHAR(50) = NULL
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT     
		pat.PatientID, 
		pat.BusinessEntityID AS StoreID, 
		sto.NABP, sto.SourceStoreID, 
		sto.Name AS StoreName, 
		pat.PersonID, 
		pat.SourcePatientKey, 
		psn.FirstName, 
        psn.LastName, 
        psn.MiddleName, 
        psn.Title, 
        psn.Suffix, 
        psn.BirthDate, 
        psn.Gender, 
        psn.Language, 
        psn.AddressLine1, 
        psn.AddressLine2, 
        psn.City, psn.State, 
        psn.PostalCode, 
        psn.HomePhone, 
        psn.IsCallAllowedToHomePhone,
        psn.IsTextAllowedToHomePhone,
        psn.IsHomePhonePreferred,
        psn.MobilePhone, 
        psn.IsCallAllowedToMobilePhone,
        psn.IsTextAllowedToMobilePhone,
		psn.IsMobilePhonePreferred,
        psn.PrimaryEmail, 
        psn.IsEmailAllowedToPrimaryEmail,
        psn.IsPrimaryEmailPreferred,
        psn.AlternateEmail, 
        psn.IsEmailAllowedToAlternateEmail,
        psn.IsAlternateEmailPreferred,
        pat.rowguid, 
        pat.DateCreated, 
        pat.DateModified
	FROM phrm.Patient AS pat 
		INNER JOIN dbo.vwPerson AS psn 
			ON pat.PersonID = psn.PersonID 
		INNER JOIN dbo.Store AS sto 
			ON pat.BusinessEntityID = sto.BusinessEntityID
	WHERE pat.PatientID = @PatientKey
	
)
