﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description: Gets the patient ID
-- SAMPLE CALL: SELECT phrm.fnPatientKeyTranslator(10684, NULL, 30138, 'SourcePatientKey')
-- SAMPLE CALL: SELECT phrm.fnPatientKeyTranslator('7871787', 'NABP', '21656', 'SourcePatientKeyList')

-- SELECT * FROM dbo.Store WHERE BusinessEntityID = 10684
-- SELECT * FROM phrm.Patient
-- =============================================
CREATE FUNCTION [phrm].[fnPatientKeyTranslator]
(
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(MAX),
	@PatientKeyType VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType)		

	IF RIGHT(@PatientKey, 1) = ','
	BEGIN
		SET @PatientKey = NULLIF(LEFT(@PatientKey, LEN(@PatientKey) - 1), '');
	END	
		
		
	SET @Ids = 
		CASE
			WHEN @PatientKeyType = 'PatientID' AND ISNUMERIC(@PatientKey) = 1 AND CHARINDEX(',', @PatientKey) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, PatientID) 
				FROM phrm.Patient 
				WHERE BusinessEntityID = ISNULL(@BusinessID, BusinessEntityID) 
					AND PatientID = CONVERT(BIGINT, @PatientKey)
			)
			WHEN @PatientKeyType IN ('SourcePatientKey', 'SourcePatientID') AND CHARINDEX(',', @PatientKey) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, PatientID)
				FROM phrm.Patient 
				WHERE BusinessEntityID = @BusinessID 
					AND SourcePatientKey = @PatientKey
			)
			WHEN @PatientKeyType = 'PersonID' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, PatientID)
				FROM phrm.Patient 
				WHERE BusinessEntityID = @BusinessID
					AND PersonID = @PatientKey
			)
			WHEN @PatientKeyType = 'PatientIDList' THEN (
				SELECT ISNULL(CONVERT(VARCHAR, PatientID), '') + ','
				FROM phrm.Patient 
				WHERE BusinessEntityID = @BusinessID
					AND PatientID IN (SELECT Value FROM dbo.fnSplit(@PatientKey, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @PatientKeyType IN ('PersonIDList', 'PersonKeyList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, PatientID), '') + ','
				FROM phrm.Patient 
				WHERE BusinessEntityID = @BusinessID
					AND PersonID IN (SELECT Value FROM dbo.fnSplit(@PatientKey, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @PatientKeyType IN ('SourcePatientKeyList', 'SourcePatientIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, PatientID), '') + ','
				FROM phrm.Patient 
				WHERE BusinessEntityID = @BusinessID
					AND SourcePatientKey IN (SELECT Value FROM dbo.fnSplit(@PatientKey, ',') )
				FOR XML PATH('')
			)
			WHEN @PatientKeyType IN ('rowguid', 'Token') THEN (
				SELECT TOP 1 CONVERT(VARCHAR, PatientID)
				FROM phrm.Patient 
				WHERE BusinessEntityID = ISNULL(@BusinessID, BusinessEntityID) 
					AND rowguid = @PatientKey
			)
			ELSE NULL
		END	



	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
