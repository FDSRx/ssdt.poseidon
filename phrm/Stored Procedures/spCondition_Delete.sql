﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/2/2015
-- Description:	Deletes a Condition object.
-- SAMPLE CALL:

/*

DECLARE
	@ConditionID BIGINT

EXEC phrm.spCondition_Delete
	@ConditionID = -1,
	@BusinessEntityID = 135590

SELECT @ConditionID AS ConditionID

*/

-- SELECT * FROM phrm.Condition
-- SELECT * FROM dbo.CodeQualifier
-- SELECT * FROM phrm.ConditionType
-- SELECT * FROM dbo.Origin
-- SELECT * From phrm.vwPatient WHERE LastName LIKE '%Simmons%'
-- =============================================
CREATE PROCEDURE [phrm].[spCondition_Delete]
	@ConditionID BIGINT,
	@BusinessEntityID BIGINT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------------
	-- Sanitation code goes here.
	
	---------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
	
	---------------------------------------------------------------------------
	-- Set variables
	---------------------------------------------------------------------------
	-- Variable setting code goes here.
	
	---------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	---------------------------------------------------------------------------
	-- Argument validation code goes here.

	
	
	---------------------------------------------------------------------------
	-- Remove condition record.
	-- <Summary>
	-- Deletes the Condition object from the condition repository.
	-- </Summary>
	---------------------------------------------------------------------------
	DELETE c
	FROM phrm.Condition c
	WHERE ( c.ConditionID = @ConditionID AND @BusinessEntityID IS NULL )
		OR ( c.ConditionID = @ConditionID AND c.BusinessEntityID = @BusinessEntityID )
	
	
	
	
	
	
	
	
			
		
END
