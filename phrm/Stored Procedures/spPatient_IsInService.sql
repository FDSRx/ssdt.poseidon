﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/12/2014
-- Description:	Indicates whether a patient exists and has the specified service.
-- SAMPLE CALL: 
/*
DECLARE @Exists BIT, @HasService BIT, @IsSelfOperated BIT

EXEC phrm.spPatient_IsInService
	@BusinessEntityID = 1, 
	@SourcePatientKey = 36782,
	@ServiceID = 1,
	@PatientExists = @Exists OUTPUT,
	@HasService = @HasService OUTPUT,
	@IsSelfOperated = @IsSelfOperated OUTPUT

SELECT @Exists AS IsFound, @HasService AS HasService, @IsSelfOperated AS IsSelfOperated
*/
--SELECT * FROM phrm.Patient
--SELECT * FROM phrm.vwPatient WHERE PersonID = 135849
--SELECT * FROM dbo.BusinessEntityService
-- =============================================
CREATE PROCEDURE [phrm].[spPatient_IsInService]
	@BusinessEntityID BIGINT,
	@PersonID BIGINT = NULL OUTPUT,
	@PatientID BIGINT = NULL OUTPUT,
	@SourcePatientKey VARCHAR(256) = NULL OUTPUT,
	@ServiceID INT,
	@PatientExists BIT = NULL OUTPUT,
	@HasService BIT = NULL OUTPUT,
	@IsSelfOperated BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------
	SET @PatientExists = 0;
	SET	@HasService = 0;
	SET @IsSelfOperated = 0;
	
	--------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
	
	--------------------------------------------------------------------
	-- Service Validation (Arguemnent Null Exception).
	-- <Summary>
	-- Ensure that a service was provided for inspection.
	-- </Summary>
	--------------------------------------------------------------------
	IF ISNULL(@ServiceID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Cannot determine if patient has the provided service. Object reference (@ServiceID) is not set to an instance of an object. ' +
		'@ServiceID cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
	END

	--------------------------------------------------------------------
	-- Service Validation (A known service was provided).
	-- <Summary>
	-- Ensure that a known service of the system was provided.
	-- </Summary>
	--------------------------------------------------------------------	
	SET @ServiceID = dbo.fnGetServiceID(@ServiceID);
	
	IF ISNULL(@ServiceID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Cannot determine if patient has the provided service. Cannot verify service.';
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
	END	

	
	-------------------------------------------------
	-- Verify the patient's existence
	-------------------------------------------------
	EXEC phrm.spPatient_Exists
		@BusinessEntityID = @BusinessEntityID, 
		@PersonID = @PersonID OUTPUT,
		@PatientID = @PatientID OUTPUT,		
		@SourcePatientKey = @SourcePatientKey,
		@Exists = @PatientExists OUTPUT	
		

	-------------------------------------------------
	-- Determine if the patient has the service.
	-------------------------------------------------	
	SELECT
		@HasService = CASE WHEN srv.ServiceID IS NOT NULL THEN 1 ELSE 0 END,
		@IsSelfOperated = conn.IsSelfOperated
	FROM dbo.BusinessEntityService srv
		LEFT JOIN dbo.ConnectionStatus conn
			ON srv.ConnectionStatusID = conn.ConnectionStatusID
	WHERE BusinessEntityID = @PersonID
		AND ServiceID = @ServiceID
	
		
END
