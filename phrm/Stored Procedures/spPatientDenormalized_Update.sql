﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/17/2015
-- Description:	Updates a PatientDenormalized object.
-- Change Log:
-- 7/22/2015 - dhughes - Modified the procedure to peform the update join on the "NABP" and "SourcePatientKey" fields instead
--				of the "PatientID" field.
-- SAMPLE CALL:
/*

DECLARE
	@PatientID BIGINT = 30,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@Debug BIT = 1

EXEC phrm.spPatientDenormalized_Update
	@PatientID = @PatientID OUTPUT,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@Debug = @Debug

SELECT @PatientID AS PatientID


*/

-- SELECT * FROM phrm.Patient
-- SELECT * FROM phrm.PatientDenormalized
-- =============================================
CREATE PROCEDURE [phrm].[spPatientDenormalized_Update]
	@PatientID BIGINT = NULL OUTPUT,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@PatientID=' + dbo.fnToStringOrEmpty(@PatientID)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey)  + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	-------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	-------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------------------
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @PatientKeyType = ISNULL(@PatientKeyType, 'SRC');
	SET @Debug = ISNULL(@Debug, 0);


	-------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------------------------------
	SET @PatientID = 
		CASE 
			WHEN @PatientID IS NOT NULL THEN @PatientID 
			ELSE phrm.fnPatientIDTranslator(@BusinessKey, @BusinessKeyType, @PatientKey, @PatientKeyType) 
		END;

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@PatientID AS PatientID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@PatientKey AS PatientKey, @PatientKeyType AS PatientKeyType, @Args AS Arguments
	END

	-------------------------------------------------------------------------------------------------------------------
	-- Create PatientDenormalized object.
	-------------------------------------------------------------------------------------------------------------------
	UPDATE trgt
	SET
		trgt.PatientID = src.PatientID,
		trgt.StoreID = src.StoreID,
		trgt.Nabp = src.Nabp,
		trgt.SourceStoreID = src.SourceStoreID,
		trgt.StoreName = src.StoreName,
		trgt.PersonID = src.PersonID,
		trgt.SourcePatientKey = src.SourcePatientKey,
		trgt.Title = src.Title,
		trgt.FirstName = src.FirstName,
		trgt.MiddleName = src.MiddleName,
		trgt.LastName = src.LastName,
		trgt.Suffix = src.Suffix,
		trgt.BirthDate = src.BirthDate,
		trgt.GenderID = src.GenderID,
		trgt.GenderCode = src.GenderCode,
		trgt.GenderName = src.GenderName,
		trgt.LanguageID = src.LanguageID,
		trgt.LanguageCode = src.LanguageCode,
		trgt.LanguageName = src.LanguageName,
		trgt.AddressLine1 = src.AddressLine1,
		trgt.AddressLine2 = src.AddressLine2,
		trgt.City = src.City,
		trgt.StateProvinceID = src.StateProvinceID,
		trgt.State = src.State,
		trgt.PostalCode = src.PostalCode,
		trgt.HomePhone = src.HomePhone,
		trgt.IsCallAllowedToHomePhone = src.IsCallAllowedToHomePhone,
		trgt.IsTextAllowedToHomePhone = src.IsTextAllowedToHomePhone,
		trgt.IsHomePhonePreferred = src.IsHomePhonePreferred,
		trgt.MobilePhone = src.MobilePhone,
		trgt.IsCallAllowedToMobilePhone = src.IsCallAllowedToMobilePhone,
		trgt.IsTextAllowedToMobilePhone = src.IsTextAllowedToMobilePhone,
		trgt.IsMobilePHonePreferred = src.IsMobilePHonePreferred,
		trgt.PrimaryEmail = src.PrimaryEmail,
		trgt.IsEmailAllowedToPrimaryEmail = src.IsEmailAllowedToPrimaryEmail,
		trgt.IsPrimaryEmailPreferred = src.IsPrimaryEmailPreferred,
		trgt.AlternateEmail = src.AlternateEmail,
		trgt.IsEmailAllowedToAlternateEmail = src.IsEmailAllowedToAlternateEmail,
		trgt.IsAlternateEmailPreferred = src.IsAlternateEmailPreferred,
		trgt.DateCreated = src.DateCreated,
		trgt.DateModified = src.DateModified,
		trgt.CreatedBy = src.CreatedBy,
		trgt.ModifiedBy = src.ModifiedBy
	--SELECT *
	--SELECT TOP 1 *
	FROM phrm.PatientDenormalized trgt
		JOIN phrm.fnGetPatientDemographicsFromPatientID(@PatientID) src
			ON trgt.Nabp = src.SourcePatientKey
				AND trgt.SourcePatientKEy = src.SourcePatientKey



END
