﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/6/2014
-- Description:	Creates a new pharmacy patient.
-- Change Log:
-- 11/17/2015 - dhughes - Modified the method to include a create new PatientDenormalized object after the process has sucessfully completed
--					creating a patient record.
-- SAMPLE CALL:
/*

DECLARE
	--Business
	@BusinessEntityID BIGINT,
	--Patient
	@SourcePatientKey VARCHAR(50),
	--Person
	@PersonID BIGINT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75), -- Required to try and create a unique person
	@LastName VARCHAR(75), -- Required to try and create a unique person
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE, -- Required to try and create a unique person
	@GenderID INT, -- Required to try and create a unique person
	@LanguageID INT = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15), -- Required to try and create a unique person
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Alternate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	-- MPC Pharmacy Service
	@Service_Mpc_ConnectionStatusID INT = NULL,
	@Service_Mpc_AccountKey VARCHAR(256) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Error
	@ErrorLogID INT = NULL,
	-- User output 
	@PatientID BIGINT = NULL,
	@PatientExists BIT = 0,
	@Debug BIT = NULL


EXEC phrm.spPatient_Create
	@BusinessEntityID = @BusinessEntityID,
	@SourcePatientKey = @SourcePatientKey,
	@PersonID = @PersonID,
	@Title = @Title,
	@FirstName = @FirstName,
	@LastName = @LastName,
	@MiddleName = @MiddleName,
	@Suffix = @Suffix,
	@BirthDate = @BirthDate,
	@GenderID = @GenderID,
	@LanguageID = @LanguageID,
	@AddressLine1 = @AddressLine1,
	@AddressLine2 = @AddressLine2,
	@City = @City,
	@State = @State,
	@PostalCode = @PostalCode,
	@PhoneNumber_Home = @PhoneNumber_Home,
	@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
	@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
	@PhoneNumber_Home_IsPreferred = @PhoneNumber_Home_IsPreferred,
	@PhoneNumber_Mobile = @PhoneNumber_Mobile,
	@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
	@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
	@PhoneNumber_Mobile_IsPreferred = @PhoneNumber_Mobile_IsPreferred,
	@EmailAddress_Primary = @EmailAddress_Primary,
	@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
	@EmailAddress_Primary_IsPreferred = @EmailAddress_Primary_IsPreferred,
	@EmailAddress_Alternate = @EmailAddress_Alternate,
	@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
	@EmailAddress_Alternate_IsPreferred = @EmailAddress_Alternate_IsPreferred,
	@Service_Mpc_ConnectionStatusID = @Service_Mpc_ConnectionStatusID,
	@Service_Mpc_AccountKey = @Service_Mpc_AccountKey,
	@CreatedBy = @CreatedBy,
	@ErrorLogID = @ErrorLogID OUTPUT,
	@PatientID = @PatientID OUTPUT,
	@PatientExists = @PatientExists OUTPUT,
	@Debug = @Debug


SELECT @PatientID AS PatientID, @PatientExists AS PatientExists, @ErrorLogID AS ErrorLogID

*/


-- SELECT * FROM phrm.Patient
-- SELECT * FROM phrm.vwPatient
-- SELECT * FROM phrm.PatientDenormalized

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [phrm].[spPatient_Create]
	--Business
	@BusinessEntityID BIGINT,
	--Patient
	@SourcePatientKey VARCHAR(50),
	--Person
	@PersonID BIGINT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75), -- Required to try and create a unique person
	@LastName VARCHAR(75), -- Required to try and create a unique person
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE, -- Required to try and create a unique person
	@GenderID INT, -- Required to try and create a unique person
	@LanguageID INT = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15), -- Required to try and create a unique person
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Alternate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	-- MPC Pharmacy Service
	@Service_Mpc_ConnectionStatusID INT = NULL,
	@Service_Mpc_AccountKey VARCHAR(256) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Error
	@ErrorLogID INT = NULL OUTPUT,
	-- User output 
	@PatientID BIGINT = NULL OUTPUT,
	@PatientExists BIT = 0 OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID)  + ';' +
		'@SourcePatientKey=' + dbo.fnToStringOrEmpty(@SourcePatientKey)  + ';' +
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID)  + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title)  + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName)  + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName)  + ';' +
		'@MiddleName=' + dbo.fnToStringOrEmpty(@MiddleName)  + ';' +
		'@Suffix=' + dbo.fnToStringOrEmpty(@Suffix)  + ';' +
		'@BirthDate=' + dbo.fnToStringOrEmpty(@BirthDate)  + ';' +
		'@GenderID=' + dbo.fnToStringOrEmpty(@GenderID)  + ';' +
		'@LanguageID=' + dbo.fnToStringOrEmpty(@LanguageID)  + ';' +
		'@AddressLine1=' + dbo.fnToStringOrEmpty(@AddressLine1)  + ';' +
		'@AddressLine2=' + dbo.fnToStringOrEmpty(@AddressLine2)  + ';' +
		'@City=' + dbo.fnToStringOrEmpty(@City)  + ';' +
		'@State=' + dbo.fnToStringOrEmpty(@State)  + ';' +
		'@PostalCode=' + dbo.fnToStringOrEmpty(@PostalCode)  + ';' +
		'@PhoneNumber_Home=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home)  + ';' +
		'@PhoneNumber_Home_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsCallAllowed)  + ';' +
		'@PhoneNumber_Home_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsTextAllowed)  + ';' +
		'@PhoneNumber_Home_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsPreferred)  + ';' +
		'@PhoneNumber_Mobile=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile)  + ';' +
		'@PhoneNumber_Mobile_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsCallAllowed)  + ';' +
		'@PhoneNumber_Mobile_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsTextAllowed)  + ';' +
		'@PhoneNumber_Mobile_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsPreferred)  + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary)  + ';' +
		'@EmailAddress_Primary_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsEmailAllowed)  + ';' +
		'@EmailAddress_Primary_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsPreferred)  + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate)  + ';' +
		'@EmailAddress_Alternate_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsEmailAllowed)  + ';' +
		'@EmailAddress_Alternate_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsPreferred)  + ';' +
		'@Service_Mpc_ConnectionStatusID=' + dbo.fnToStringOrEmpty(@Service_Mpc_ConnectionStatusID)  + ';' +
		'@Service_Mpc_AccountKey=' + dbo.fnToStringOrEmpty(@Service_Mpc_AccountKey)  + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy)  + ';' +
		'@ErrorLogID=' + dbo.fnToStringOrEmpty(@ErrorLogID)  + ';' +
		'@PatientID=' + dbo.fnToStringOrEmpty(@PatientID)  + ';' +
		'@PatientExists=' + dbo.fnToStringOrEmpty(@PatientExists)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;


	-------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	-------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-------------------------------------------------------------------------------------------------------------------------
	SET @ErrorLogID = NULL; -- Never have a user pass an error log ID.  They should only have one returned.
	SET @PatientID = NULL;
	SET @PatientExists = 0;	
	SET @Debug = ISNULL(@Debug, 0);
	

	-------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ServiceID INT = dbo.fnGetServiceID('PHRM'), -- SELECT * FROM Poseidon.dbo.Service
		@PersonTypeID INT = dbo.fnGetPersonTypeID('PAT')
	

	
	BEGIN TRY
	
		-------------------------------------------------------------------------------------------------------------------------
		-- Business Entity Validation
		-- <Remarks>
		-- Before we can even create a patient, make sure a non-null business entity ID
		-- was passed.
		-- </Remarks>
		-------------------------------------------------------------------------------------------------------------------------
		IF ISNULL(@BusinessEntityID, 0) = 0
		BEGIN
			
			SET @ErrorMessage = 'Cannot initialize PATIENT create process. A BusinessEntityID (BusinessID) was not provided.'
				
			RAISERROR (
				@ErrorMessage, -- Message text.
				16, -- Severity.
				1 -- State.
			);
				
		END
		
		-------------------------------------------------------------------------------------------------------------------------
		-- Source Patient Key Valdiation (vchrPatientID from pharmacy systems)
		-- <Remarks>
		-- Before we can even create a patient, make sure a non-null SourcePatientKey
		-- was passed
		-- </Remarks>
		-------------------------------------------------------------------------------------------------------------------------
		IF ISNULL(@SourcePatientKey, '') = ''
		BEGIN
			
			SET @ErrorMessage = 'Cannot initialize PATIENT create process. A SourcePatientKey was not provided.'
				
			RAISERROR (
				@ErrorMessage, -- Message text.
				16, -- Severity.
				1 -- State.
			);
				
		END
		
		-------------------------------------------------------------------------------------------------------------------------
		-- Determine if a patient already exists.
		-- <Remarks>
		-- Use the SourcePatientKey (pharmacy vchrPatientID) to identify if a 
		-- patient already exists.
		-- </Remarks>
		-------------------------------------------------------------------------------------------------------------------------			
		EXEC phrm.spPatient_Exists
			@BusinessEntityID = @BusinessEntityID,
			@SourcePatientKey = @SourcePatientKey,
			@PatientID = @PatientID OUTPUT,
			@Exists = @PatientExists OUTPUT
			
			
		-- Verify the patient does not already exist.
		IF ISNULL(@PatientExists, 0) > 0
		BEGIN		
			-- The patient already exists!  Since we have the PatientKey and the @Exists flag 
			-- alerting the caller that the person is an existing patient, we can exit the code gracefully.			  
			RETURN;			
		END
		
	-------------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	-------------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		--------------------------------------------------------------------
		-- If a person does not already exist and a specific person was passed
		-- then let's add the person ID as the patient instead of creating a new person.
		--------------------------------------------------------------------
		IF ISNULL(@PersonID, 0) > 0
		BEGIN

			DECLARE @FoundPersonID INT = dbo.fnGetPersonID(@PersonID);
			
			-- Validate person ID.
			IF ISNULL(@FoundPersonID, 0) = 0
			BEGIN
				
				SET @ErrorMessage = 'Cannot create a PATIENT for BusinessEntityID: ' + ISNULL(CONVERT(VARCHAR, @BusinessEntityID), '<EMPTY>') + '. Reason: '
					+ 'The PersonID, ' + CONVERT(VARCHAR, @PersonID) + ', passed to attach to a patient does not exist.';
				
				RAISERROR (
					@ErrorMessage, -- Message text.
					16, -- Severity.
					1 -- State.
				);

			END			
			
			-- If we do have a valid person, then skip to the create patient method.
			GOTO CREATEPATIENT;
				
		END
		
		
		-------------------------------------------------------------------------------------------------------------------------
		-- Create person entity
		-------------------------------------------------------------------------------------------------------------------------
		SET @PersonID = NULL; -- Reset person ID to ensure proper output from person create
		
		EXEC dbo.spPerson_Create
			@PersonTypeID = @PersonTypeID,
			@Title = @Title,
			@FirstName = @FirstName,
			@LastName = @LastName,
			@MiddleName = @MiddleName,
			@Suffix = @Suffix,
			@BirthDate = @BirthDate,
			@GenderID = @GenderID,
			@LanguageID = @LanguageID,
			--Person Address
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			--Person Phone
			--	Home
			@PhoneNumber_Home = @PhoneNumber_Home,
			@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
			@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
			@PhoneNumber_Home_IsPreferred = @PhoneNumber_Home_IsPreferred,
			-- Mobile
			@PhoneNumber_Mobile = @PhoneNumber_Mobile,
			@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
			@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
			@PhoneNumber_Mobile_IsPreferred = @PhoneNumber_Mobile_IsPreferred,
			--Person Email
			--	Primary
			@EmailAddress_Primary = @EmailAddress_Primary,
			@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
			@EmailAddress_Primary_IsPreferred = @EmailAddress_Primary_IsPreferred,
			--	Alternate
			@EmailAddress_Alternate = @EmailAddress_Alternate,
			@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
			@EmailAddress_Alternate_IsPreferred = @EmailAddress_Alternate_IsPreferred,
			--Caller
			@CreatedBy = @CreatedBy,
			--Output
			@PersonID = @PersonID OUTPUT,
			@ErrorLogID = @ErrorLogID OUTPUT	
		
		-- Debug
		--PRINT 'PERSON: ' + CONVERT(VARCHAR, ISNULL(@PersonID, 0)) + '; ERROR ' + CONVERT(VARCHAR, ISNULL(@ErrorLogID, 0))
		
		IF ISNULL(@ErrorLogID, 0) > 0
		BEGIN
			
			SELECT @ErrorMessage = ErrorMessage FROM dbo.ErrorLog (NOLOCK) WHERE ErrorLogID = @ErrorLogID
			
			SET @ErrorMessage = 'Cannot create a PATIENT for BusinessEntityID: ' + ISNULL(CONVERT(VARCHAR, @BusinessEntityID), '<EMPTY>') + '. Reason: '
				+ ISNULL(@ErrorMessage, '');
				
			RAISERROR (
				@ErrorMessage, -- Message text.
				16, -- Severity.
				1 -- State.
			);
					
		END
		
		
		CREATEPATIENT:
		-------------------------------------------------------------------------------------------------------------------------
		-- Create Patient
		-------------------------------------------------------------------------------------------------------------------------
		-- If no errors were encountered when creating a person then add the person as a patient.
		IF ISNULL(@PersonID, 0) > 0 AND ISNULL(@ErrorLogID, 0) = 0
		BEGIN				
			INSERT INTO phrm.Patient (
				BusinessEntityID,			
				PersonID,
				SourcePatientKey,
				CreatedBy
			)
			SELECT 
				@BusinessEntityID,
				@PersonID,
				@SourcePatientKey,
				@CreatedBy
				
			SET @PatientID = SCOPE_IDENTITY();				
		END	
	
	
		-------------------------------------------------------------------------------------------------------------------------
		-- Attach pharmacy service to the person/patient.
		-------------------------------------------------------------------------------------------------------------------------
		EXEC dbo.spBusinessEntityService_Create
			@BusinessEntityID = @PersonID,
			@ServiceID = @ServiceID,
			@CreatedBy = @CreatedBy
		
		-------------------------------------------------------------------------------------------------------------------------
		-- Attach MPC service (if applicable)
		-------------------------------------------------------------------------------------------------------------------------
		IF ISNULL(dbo.fnGetConnectionStatusCode(@Service_Mpc_ConnectionStatusID), 'NONE') NOT IN ('NONE', 'UNKNOWN')
		BEGIN
		
			SET @ServiceID = dbo.fnGetServiceID('MPC');
			
			EXEC dbo.spBusinessEntityService_Create
				@BusinessEntityID = @PersonID,
				@ServiceID = @ServiceID,
				@AccountKey = @Service_Mpc_AccountKey,
				@ConnectionStatusId = @Service_Mpc_ConnectionStatusID,
				@CreatedBy = @CreatedBy
		END	


		-------------------------------------------------------------------------------------------------------------------------
		-- Create customer record.
		-- <Summary>
		-- Adds the person as a customer of the business.
		-- </Summary>
		-------------------------------------------------------------------------------------------------------------------------	
		DECLARE
			@CustomerID BIGINT,
			@CustomerExists BIT
				
		EXEC Poseidon.dbo.spCustomer_Create
			@BusinessEntityID = @BusinessEntityID,
			@PersonID = @PersonID,
			@Title = @Title,
			@FirstName = @FirstName,
			@LastName = @LastName,
			@MiddleName = @MiddleName,
			@Suffix = @Suffix,
			@BirthDate = @BirthDate,
			@GenderID = @GenderID,
			@LanguageID = @LanguageID,
			--Person Address
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			--Person Phone
			--	Home
			@PhoneNumber_Home = @PhoneNumber_Home,
			@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
			@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
			@PhoneNumber_Home_IsPreferred = @PhoneNumber_Home_IsPreferred,
			-- Mobile
			@PhoneNumber_Mobile = @PhoneNumber_Mobile,
			@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
			@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
			@PhoneNumber_Mobile_IsPreferred = @PhoneNumber_Mobile_IsPreferred,
			--Person Email
			--	Primary
			@EmailAddress_Primary = @EmailAddress_Primary,
			@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
			@EmailAddress_Primary_IsPreferred = @EmailAddress_Primary_IsPreferred,
			--	Alternate
			@EmailAddress_Alternate = @EmailAddress_Alternate,
			@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
			@EmailAddress_Alternate_IsPreferred = @EmailAddress_Alternate_IsPreferred,
			--Output
			@CustomerID = @CustomerID OUTPUT,
			@ErrorLogID = @ErrorLogID OUTPUT,
			@CustomerExists = @CustomerExists OUTPUT
	
	
		-------------------------------------------------------------------------------------------------------------------------
		-- Create PatientDenormalized object.
		-- <Summary>
		-- Adds a flattened record to the PatientDenormalized table.
		-- </Summary>
		-------------------------------------------------------------------------------------------------------------------------	
		BEGIN TRY
			
			-- If a patient was created, then let's create a denormalized record.
			IF @PatientID IS NOT NULL
			BEGIN
				EXEC phrm.spPatientDenormalized_Merge
					@PatientID = @PatientID,
					@Debug = @Debug
			END

		END TRY
		BEGIN CATCH
			
			SET @ErrorMessage = 'Unable to create a denormalized patient object ' +
				'(PatientID: ' + dbo.fnToStringOrEmpty(@PatientID) + '). Error: ' + ERROR_MESSAGE(); 

			EXEC dbo.spLogException
				@Arguments = @Args,
				@Exception = @ErrorMessage;

		END CATCH
	
	-------------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	-------------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args

		SET @ErrorMessage = ERROR_MESSAGE();
	
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		
	END CATCH
	
END
