﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description:	Updates a Patient object.
-- Change Log:
-- 11/17/2015 - dhughes - Modified the method to include an update PatientDenormalized object after the process has sucessfully completed
--					updating a patient record.
-- SAMPLE CALL:
/*

DECLARE
	--Business
	@BusinessEntityID INT = NULL,
	--Patient
	@PatientID BIGINT = NULL,
	@SourcePatientKey VARCHAR(50) = NULL,
	--Person
	@PersonID INT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75), -- Required to try and create a unique person
	@LastName VARCHAR(75), -- Required to try and create a unique person
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE, -- Required to try and create a unique person
	@GenderID INT, -- Required to try and create a unique person
	@LanguageID INT = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15), -- Required to try and create a unique person
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Alternate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	-- MPC Pharmacy Service
	@Service_Mpc_ConnectionStatusID INT = NULL,
	@Service_Mpc_AccountKey VARCHAR(256) = NULL,
	-- Error
	@ErrorLogID INT = NULL,
	-- User output 
	@PatientExists BIT = 0,
	@Debug BIT = NULL

EXEC phrm.spPatient_Update
	@BusinessEntityID = @BusinessEntityID,
	@PatientID = @PatientID OUTPUT,
	@SourcePatientKey = @SourcePatientKey,
	@PersonID = @PersonID,
	@Title = @Title,
	@FirstName = @FirstName,
	@LastName = @LastName,
	@MiddleName = @MiddleName,
	@Suffix = @Suffix,
	@BirthDate = @BirthDate,
	@GenderID = @GenderID,
	@LanguageID = @LanguageID,
	@AddressLine1 = @AddressLine1,
	@AddressLine2 = @AddressLine2,
	@City = @City,
	@State = @State,
	@PostalCode = @PostalCode,
	@PhoneNumber_Home = @PhoneNumber_Home,
	@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
	@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
	@PhoneNumber_Home_IsPreferred = @PhoneNumber_Home_IsPreferred,
	@PhoneNumber_Mobile = @PhoneNumber_Mobile,
	@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
	@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
	@PhoneNumber_Mobile_IsPreferred = @PhoneNumber_Mobile_IsPreferred,
	@EmailAddress_Primary = @EmailAddress_Primary,
	@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
	@EmailAddress_Primary_IsPreferred = @EmailAddress_Primary_IsPreferred,
	@EmailAddress_Alternate = @EmailAddress_Alternate,
	@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
	@EmailAddress_Alternate_IsPreferred = @EmailAddress_Alternate_IsPreferred,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@Service_Mpc_ConnectionStatusID = @Service_Mpc_ConnectionStatusID,
	@Service_Mpc_AccountKey = @Service_Mpc_AccountKey,
	@ErrorLogID = @ErrorLogID OUTPUT,
	@PatientExists = @PatientExists OUTPUT,
	@Debug = @Debug


SELECT @PatientID AS PatientID, @PatientExists AS PatientExists, @ErrorLogID AS ErrorLogID

*/

-- SELECT * FROM phrm.Patient
-- SELECT * FROM phrm.vwPatient
-- SELECT * FROM phrm.PatientDenormalized

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ApplicationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [phrm].[spPatient_Update]
	--Business
	@BusinessEntityID INT = NULL,
	--Patient
	@PatientID BIGINT = NULL OUTPUT,
	@SourcePatientKey VARCHAR(50) = NULL,
	--Person
	@PersonID INT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75), -- Required to try and create a unique person
	@LastName VARCHAR(75), -- Required to try and create a unique person
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE, -- Required to try and create a unique person
	@GenderID INT, -- Required to try and create a unique person
	@LanguageID INT = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15), -- Required to try and create a unique person
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Alternate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	-- MPC Pharmacy Service
	@Service_Mpc_ConnectionStatusID INT = NULL,
	@Service_Mpc_AccountKey VARCHAR(256) = NULL,
	-- Error
	@ErrorLogID INT = NULL OUTPUT,
	-- User output 
	@PatientExists BIT = 0 OUTPUT,
	@Debug BIT = NULL
AS
BEGIN


	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID)  + ';' +
		'@PatientID=' + dbo.fnToStringOrEmpty(@PatientID)  + ';' +
		'@SourcePatientKey=' + dbo.fnToStringOrEmpty(@SourcePatientKey)  + ';' +
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID)  + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title)  + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName)  + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName)  + ';' +
		'@MiddleName=' + dbo.fnToStringOrEmpty(@MiddleName)  + ';' +
		'@Suffix=' + dbo.fnToStringOrEmpty(@Suffix)  + ';' +
		'@BirthDate=' + dbo.fnToStringOrEmpty(@BirthDate)  + ';' +
		'@GenderID=' + dbo.fnToStringOrEmpty(@GenderID)  + ';' +
		'@LanguageID=' + dbo.fnToStringOrEmpty(@LanguageID)  + ';' +
		'@AddressLine1=' + dbo.fnToStringOrEmpty(@AddressLine1)  + ';' +
		'@AddressLine2=' + dbo.fnToStringOrEmpty(@AddressLine2)  + ';' +
		'@City=' + dbo.fnToStringOrEmpty(@City)  + ';' +
		'@State=' + dbo.fnToStringOrEmpty(@State)  + ';' +
		'@PostalCode=' + dbo.fnToStringOrEmpty(@PostalCode)  + ';' +
		'@PhoneNumber_Home=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home)  + ';' +
		'@PhoneNumber_Home_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsCallAllowed)  + ';' +
		'@PhoneNumber_Home_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsTextAllowed)  + ';' +
		'@PhoneNumber_Home_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsPreferred)  + ';' +
		'@PhoneNumber_Mobile=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile)  + ';' +
		'@PhoneNumber_Mobile_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsCallAllowed)  + ';' +
		'@PhoneNumber_Mobile_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsTextAllowed)  + ';' +
		'@PhoneNumber_Mobile_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsPreferred)  + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary)  + ';' +
		'@EmailAddress_Primary_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsEmailAllowed)  + ';' +
		'@EmailAddress_Primary_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsPreferred)  + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate)  + ';' +
		'@EmailAddress_Alternate_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsEmailAllowed)  + ';' +
		'@EmailAddress_Alternate_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsPreferred)  + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy)  + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy)  + ';' +
		'@Service_Mpc_ConnectionStatusID=' + dbo.fnToStringOrEmpty(@Service_Mpc_ConnectionStatusID)  + ';' +
		'@Service_Mpc_AccountKey=' + dbo.fnToStringOrEmpty(@Service_Mpc_AccountKey)  + ';' +
		'@ErrorLogID=' + dbo.fnToStringOrEmpty(@ErrorLogID)  + ';' +
		'@PatientExists=' + dbo.fnToStringOrEmpty(@PatientExists)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;


	-------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	-------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-------------------------------------------------------------------------------------------------------------------------
	SET @ErrorLogID = NULL; -- never have a user pass an error log ID.  They should only have one returned.
	SET @PatientExists = 0;	
	SET @Debug = ISNULL(@Debug, 0);
	
	-------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ServiceID INT = dbo.fnGetServiceID('PHRM'), -- SELECT * FROM Poseidon.dbo.Service
		@PersonTypeID INT = dbo.fnGetPersonTypeID('PAT'),
		@AddressID BIGINT = NULL
	
	
	BEGIN TRY
	
		-------------------------------------------------------------------------------------------------------------------------
		-- Business Entity Validation
		-- <Remarks>
		-- Before we can even create a patient, make sure a non-null business entity ID
		-- was passed.
		-- </Remarks>
		-------------------------------------------------------------------------------------------------------------------------
		IF ISNULL(@BusinessEntityID, 0) = 0
		BEGIN
			
			SET @ErrorMessage = 'Cannot initialize PATIENT update process. A BusinessEntityID (BusinessID) was not provided.'
				
			RAISERROR (
				@ErrorMessage, -- Message text.
				16, -- Severity.
				1 -- State.
			);

		END
		
		-------------------------------------------------------------------------------------------------------------------------
		-- Patient Key Validation
		-- <Summary>
		-- A patient key of some variety must be provided to perform a lookup (i.e.
		-- either a PatientID, SourcePatientKey, or PersonID must be present.)
		-- </Summary>
		-------------------------------------------------------------------------------------------------------------------------
		IF ISNULL(@PatientID, 0) = 0 AND ISNULL(@SourcePatientKey, '') = '' AND ISNULL(@PersonID, 0) = 0
		BEGIN
			
			SET @ErrorMessage = 'Cannot initialize PATIENT update process. Object reference not set to an instance of an object. ' +
				'@PatientID or @SourcePatientKey or @PersonID cannot be null.';
				
				
			RAISERROR (
				@ErrorMessage, -- Message text.
				16, -- Severity.
				1 -- State.
			);
				
		END
		
		-------------------------------------------------------------------------------------------------------------------------
		-- Determine if a patient already exists.
		-- <Remarks>
		-- Use the SourcePatientKey (pharmacy vchrPatientID) to identify if a 
		-- member already exists.
		-- </Remarks>
		-------------------------------------------------------------------------------------------------------------------------			
		EXEC phrm.spPatient_Exists
			@BusinessEntityID = @BusinessEntityID,
			@SourcePatientKey = @SourcePatientKey,
			@PatientID = @PatientID OUTPUT,
			@PersonID = @PersonID OUTPUT,
			@Exists = @PatientExists OUTPUT
			
			
		-- Determine if the patient exists. If the patient already exists, then let's exit the code as create
		-- statement should be called.
		IF ISNULL(@PatientExists, 0) = 0
		BEGIN				  
			RETURN;			
		END	
	
		-------------------------------------------------------------------------------------------------------------------------
		-- Validate PersonID.
		-------------------------------------------------------------------------------------------------------------------------	
		IF ISNULL(@PersonID, 0) = 0
		BEGIN
			
			SET @ErrorMessage = 'Cannot initialize PATIENT update process. Unable to locate person record.';
				
				
			RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		END	
		
		
	-------------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	-------------------------------------------------------------------------------------------------------------------------	
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	
		-- Update contact information.
		EXEC dbo.spPerson_ContactInformation_Update
			--Person
			@PersonID = @PersonID,
			@Title = @Title,
			@FirstName = @FirstName,
			@LastName = @LastName,
			@MiddleName = @MiddleName,
			@Suffix = @Suffix,
			@BirthDate = @BirthDate,
			@GenderID = @GenderID,
			@LanguageID = @LanguageID,
			--Person Address
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			--Person Phone
			--	Home
			@PhoneNumber_Home = @PhoneNumber_Home,
			@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
			@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
			@PhoneNumber_Home_IsPreferred = @PhoneNumber_Home_IsPreferred,
			-- Mobile
			@PhoneNumber_Mobile = @PhoneNumber_Mobile,
			@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
			@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
			@PhoneNumber_Mobile_IsPreferred = @PhoneNumber_Mobile_IsPreferred,
			--Person Email
			--	Primary
			@EmailAddress_Primary = @EmailAddress_Primary,
			@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
			@EmailAddress_Primary_IsPreferred = @EmailAddress_Primary_IsPreferred,
			--	Alternate
			@EmailAddress_Alternate = @EmailAddress_Alternate,
			@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
			@EmailAddress_Alternate_IsPreferred = @EmailAddress_Alternate_IsPreferred,
			-- Caller
			@ModifiedBy = @ModifiedBy
		

		-------------------------------------------------------------------------------------------------------------------------
		-- Modify/Create pharmacy service to the person/patient.
		-------------------------------------------------------------------------------------------------------------------------
		EXEC dbo.spBusinessEntityService_Merge
			@BusinessEntityID = @PersonID,
			@ServiceID = @ServiceID,
			@CreatedBy = @CreatedBy,
			@ModifiedBy = @ModifiedBy
		
		-------------------------------------------------------------------------------------------------------------------------
		-- Modify/Create MPC service (if applicable)
		-------------------------------------------------------------------------------------------------------------------------
		IF ISNULL(dbo.fnGetConnectionStatusCode(@Service_Mpc_ConnectionStatusID), 'NONE') NOT IN ('NONE', 'UNKNOWN')
		BEGIN
		
			SET @ServiceID = dbo.fnGetServiceID('MPC');
			
			EXEC dbo.spBusinessEntityService_Merge
				@BusinessEntityID = @PersonID,
				@ServiceID = @ServiceID,
				@AccountKey = @Service_Mpc_AccountKey,
				@ConnectionStatusId = @Service_Mpc_ConnectionStatusID,
				@CreatedBy = @CreatedBy,
				@ModifiedBy = @ModifiedBy
		END	

		-------------------------------------------------------------------------------------------------------------------------
		-- Update PatientDenormalized object.
		-- <Summary>
		-- Updates a flattened record in the PatientDenormalized table.
		-- </Summary>
		-------------------------------------------------------------------------------------------------------------------------	
		BEGIN TRY
			
			-- If a patient was created, then let's create a denormalized record.
			IF @PatientID IS NOT NULL
			BEGIN
				EXEC phrm.spPatientDenormalized_Merge
					@PatientID = @PatientID,
					@Debug = @Debug
			END

		END TRY
		BEGIN CATCH
			
			SET @ErrorMessage = 'Unable to update a denormalized patient object ' +
				'(PatientID: ' + dbo.fnToStringOrEmpty(@PatientID) + '). Error: ' + ERROR_MESSAGE(); 

			EXEC dbo.spLogException
				@Arguments = @Args,
				@Exception = @ErrorMessage;

		END CATCH
		
	-------------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	-------------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args

		SET @ErrorMessage = ERROR_MESSAGE();
	
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		
	END CATCH
	
	

END
