﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/13/2016
-- Description:	Deletes the pateint population by business.

-- SAMPLE CALL
/*
DECLARE
	@BusinessID VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL

EXEC phrm.spPatient_Delete_ByBusiness
	@BusinessID = @BusinessID,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug
*/

-- SELECT * FROM phrm.Patient
-- SELECT * FROM phrm.PatientDenormalized

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC

-- =============================================
CREATE PROCEDURE [phrm].[spPatient_Delete_ByBusiness]
	@BusinessID VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID)

	DECLARE @Args VARCHAR(MAX) =
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	-------------------------------------------------------------------------------------------------------
	-- Record request.
	-------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @ModifiedBy = ISNULL(@ModifiedBy, SUSER_NAME());
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');

	-------------------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------------------


	-------------------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------------------

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@BusinessID AS BusinessID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@ModifiedBy AS ModifiedBy
	END

	-------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	-------------------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpBusinesses') IS NOT NULL
	BEGIN
		DROP TABLE #tmpBusinesses;
	END

	CREATE TABLE #tmpBusinesses (
		Idx BIGINT IDENTITY(1,1),
		BusinessID BIGINT
	);

	-------------------------------------------------------------------------------------------------------
	-- Parse data.
	-------------------------------------------------------------------------------------------------------
	-- Attempt business identifiers first.
	INSERT INTO #tmpBusinesses (
		BusinessID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@BusinessID, ',')
	WHERE ISNUMERIC(Value) = 1

	-- If no identifiers where able to be parsed, then attempt to translate the business keys.
	IF @@ROWCOUNT = 0 AND @BusinessID IS NULL
	BEGIN
		INSERT INTO #tmpBusinesses (
			BusinessID
		)
		SELECT
			dbo.fnBusinessIDTranslator(Value, @BusinessKeyType)
		FROM dbo.fnSplit(@BusinessKey, ',')
	END
	

	-------------------------------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy



	-------------------------------------------------------------------------------------------------------
	-- Delete objects.
	-------------------------------------------------------------------------------------------------------	
	DELETE trgt
	--SELECT *
	FROM phrm.Patient trgt
		JOIN #tmpBusinesses tmp
			ON tmp.BusinessID = trgt.BusinessEntityID

	-------------------------------------------------------------------------------------------------------
	-- Delete denormalized objects.
	-------------------------------------------------------------------------------------------------------	
	BEGIN TRY

		EXEC phrm.spPatientDenormalized_Delete_ByBusiness
			@BusinessID = @BusinessID,
			@BusinessKey = @BusinessKey,
			@BusinessKeyType = @BusinessKeyType,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug

	END TRY
	BEGIN CATCH

		EXEC dbo.spLogError;

	END CATCH

END