﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/30/2015
-- Description:	Deletes a patient object(s).
-- SAMPLE CALL:
/*

DECLARE
	@PatientID VARCHAR(MAX) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@SourcePatientKey VARCHAR(MAX) = 'FGA0012620000113,FGA0017360000514,FGA0022700000315',
	@BusinessKey VARCHAR(50) = '1101194',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@Debug BIT = 1

EXEC phrm.spPatient_Delete
	@PatientID = @PatientID,
	@BusinessEntityID = @BusinessEntityID,
	@SourcePatientKey = @SourcePatientKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/

-- SELECT * FROM phrm.Patient

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [phrm].[spPatient_Delete]
	@PatientID VARCHAR(MAX) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@SourcePatientKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID)

	------------------------------------------------------------------------------------------------------------------
	-- Record request.
	------------------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@PatientID=' + dbo.fnToStringOrEmpty(@PatientID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@SourcePatientKey=' + dbo.fnToStringOrEmpty(@SourcePatientKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);


	------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------------
	-- No declarations necessary.

	------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------------
	SET @BusinessEntityID = 
		CASE
			WHEN @BusinessEntityID IS NOT NULL THEN @BusinessEntityID
			ELSE dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType)
		END;

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Getter/Setter methods' AS ActionMethod,
			@PatientID AS PatientID, @BusinessEntityID AS BusinessEntityID, @SourcePatientKey AS SourcePatientKey,
			@BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType
	END


	------------------------------------------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	------------------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy


	------------------------------------------------------------------------------------------------------------------
	-- Remove object(s)
	------------------------------------------------------------------------------------------------------------------
	-- If the primary identifier was provided, then remove all primary entries.
	IF @PatientID IS NOT NULL OR @PatientID <> ''
	BEGIN
		DELETE
		FROM phrm.Patient
		WHERE PatientID IN (SELECT Value FROM dbo.fnSplit(@PatientID, ',') WHERE ISNUMERIC(Value) = 1)
	END

	-- If no primary identifier was provider, but a unique identifier was provided, then remove all unique identifiers.
	IF ( @PatientID IS NULL OR @PatientID = '' ) AND 
		( @BusinessEntityID IS NOT NULL AND ( @SourcePatientKey IS NOT NULL OR @SourcePatientKey <> ''))
	BEGIN
		DELETE
		FROM phrm.Patient
		WHERE BusinessEntityID = @BusinessEntityID 
			AND SourcePatientKey IN (SELECT Value FROM dbo.fnSplit(@SourcePatientKey, ','))	
	END


	------------------------------------------------------------------------------------------------------------------
	-- Remove denormalized object(s)
	------------------------------------------------------------------------------------------------------------------
	BEGIN TRY

		EXEC phrm.spPatientDenormalized_Delete
			@PatientID = @PatientID,
			@StoreID = @BusinessEntityID,
			@SourcePatientKey = @SourcePatientKey,
			@BusinessKey = @BusinessKey,
			@BusinessKeyType = @BusinessKeyType,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug

	END TRY
	BEGIN CATCH

		EXEC dbo.spLogError;

	END CATCH

END
