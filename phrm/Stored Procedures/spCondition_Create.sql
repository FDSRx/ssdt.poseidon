﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/8/2014
-- Description:	Creates a new Condition object.
-- SAMPLE CALL:
/*
DECLARE
	@ConditionID BIGINT

EXEC phrm.spCondition_Create
	@BusinessEntityID = 135590,
	@Name = 'Bird Allergies',
	@CreatedBy = 'dhughes',
	@ConditionID = @ConditionID OUTPUT

SELECT @ConditionID AS ConditionID
*/
-- SELECT * FROM phrm.Condition
-- SELECT * FROM dbo.CodeQualifier
-- SELECT * FROM phrm.ConditionType
-- SELECT * FROM dbo.Origin
-- SELECT * From phrm.vwPatient WHERE LastName LIKE '%Simmons%'
-- =============================================
CREATE PROCEDURE [phrm].[spCondition_Create]
	@BusinessEntityID BIGINT,
	@ConditionTypeID INT = NULL,
	@CodeQualifierID INT = NULL,
	@Code VARCHAR(25) = NULL,
	@Name VARCHAR(512),
	@Description VARCHAR(1000) = NULL,
	@OriginID INT = NULL,
	@DateInactive DATETIME = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ConditionID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------------
	SET @ConditionID = NULL;
	SET @ConditionTypeID = ISNULL(@ConditionTypeID, phrm.fnGetConditionTypeID('OTR'));
	SET @CodeQualifierID = ISNULL(@CodeQualifierID, dbo.fnGetCodeQualifierID('UNKN'));
	SET @OriginID = ISNULL(@OriginID, dbo.fnGetOriginID('UNKN'));
	
	---------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
	
	---------------------------------------------------------------------------
	-- Set variables
	---------------------------------------------------------------------------

	---------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	---------------------------------------------------------------------------
	-- @BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Condition object. Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The @BusinessEntityID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @Name
	IF dbo.fnIsNullOrWhiteSpace(@Name) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to create Condition object. Object reference (@Name) is not set to an instance of an object. ' +
			'The @Name parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	
	---------------------------------------------------------------------------
	-- Create hash key.
	-- <Summary>
	-- To help identify record changes, a hash key can be used to do a row
	-- comparison.
	-- </Summary>
	---------------------------------------------------------------------------
	DECLARE
		@HashKey VARCHAR(256) = dbo.fnCreateRecordHashKey(
			dbo.fnToHashableString(@ConditionTypeID) +
			dbo.fnToHashableString(@CodeQualifierID) +
			dbo.fnToHashableString(@Code) +
			dbo.fnToHashableString(@Name) +
			dbo.fnToHashableString(@Description) +
			dbo.fnToHashableString(@OriginID) +
			dbo.fnToHashableString(@DateInactive)
		);
	
	---------------------------------------------------------------------------
	-- Create condition record.
	-- <Summary>
	-- Creates a new Condition object and returns the object's identity.
	-- </Summary>
	---------------------------------------------------------------------------
	INSERT INTO phrm.Condition (
		BusinessEntityID,
		ConditionTypeID,
		CodeQualifierID,
		Code,
		Name,
		Description,
		OriginID,
		DateInactive,
		HashKey,
		CreatedBy
	)
	SELECT
		@BusinessEntityID,
		@ConditionTypeID,
		@CodeQualifierID,
		@Code,
		@Name,
		@Description,
		@OriginID,
		@DateInactive,
		@HashKey,
		@CreatedBy
	
	-- Retrieve record identity
	SET @ConditionID = SCOPE_IDENTITY();
	
	
	
	
	
	
	
			
		
END
