﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/21/2014
-- Description: Deletes a medication object from the person's list of medications.
-- SAMPLE CALL:
/*

EXEC phrm.spMedication_Delete
	@MedicationID = 54,
	@PersonID = NULL

*/
-- SELECT * FROM phrm.Medication
-- =============================================
CREATE PROCEDURE [phrm].[spMedication_Delete]
	@MedicationID BIGINT,
	@PersonID BIGINT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-------------------------------------------------------------------------
	-- Delete Medication object
	-------------------------------------------------------------------------
	DELETE
	FROM phrm.Medication
	WHERE MedicationID = @MedicationID
		AND ( @PersonID IS NULL OR PersonID = @PersonID )
	
END
