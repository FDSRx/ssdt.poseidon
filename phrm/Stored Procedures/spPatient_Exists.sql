﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/6/2014
-- Description:	Indicates whether a patient exists.
-- SAMPLE CALL: 
/*
DECLARE @Exists BIT

EXEC phrm.spPatient_Exists
	@BusinessEntityID = 10684, 
	@SourcePatientKey = 30138,
	@Exists = @Exists OUTPUT

SELECT @Exists AS IsFound
*/
--SELECT * FROM phrm.Patient
-- =============================================
CREATE PROCEDURE [phrm].[spPatient_Exists]
	@BusinessEntityID BIGINT,
	@PersonID BIGINT = NULL OUTPUT,
	@PatientID BIGINT = NULL OUTPUT,
	@SourcePatientKey VARCHAR(256) = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------
	SET @Exists = 0;
	
	--------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@StopSearch BIT = 0
	
	
	--------------------------------------------------------------------
	-- Business Entity Validation
	-- <Remarks>
	-- Before we can even create a patient, make sure a non-null BusinessEntityID
	-- was passed.
	-- </Remarks>
	--------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Cannot determine patient existence. Object reference not set to an instance of an object. ' +
		'@BusinessEntityID cannot be null';
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
	END
	
	--------------------------------------------------------------------
	-- Patient Key Validation
	-- <Summary>
	-- A patient key of some variety must be provided to perform a lookup (i.e.
	-- either a PatientID, SourcePatientKey, or PersonID must be present.)
	-- </Summary>
	--------------------------------------------------------------------
	IF ISNULL(@PatientID, 0) = 0 AND ISNULL(@SourcePatientKey, '') = '' AND ISNULL(@PersonID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Cannot determine patient existence. Object reference not set to an instance of an object. ' +
			'@PatientID or @SourcePatientKey or @PersonID cannot be null.';
			
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
	END

	
	-- If @PatientID was provided then perform PatientID lookup.
	IF @PatientID IS NOT NULL
	BEGIN
	
		SET @PersonID = NULL;
		SET @SourcePatientKey = NULL;
		
		-------------------------------------------------
		-- Fetch patient info by PatientID.
		-------------------------------------------------
		SET	@PatientID = (
			SELECT PatientID
			FROM phrm.Patient
			WHERE BusinessEntityID = @BusinessEntityID
				AND PatientID = @PatientID
		);
		
		SET @StopSearch = 1;
	END
	
	-- If @SourcePatientKey was provided and @PatientID has not been populatated, then perform
	-- SourcePatientKey lookup.
	IF @SourcePatientKey IS NOT NULL AND @StopSearch = 0
	BEGIN
		
		SET @PatientID = NULL;
		SET @PersonID = NULL;
		
		-------------------------------------------------
		-- Fetch patient info by SourcePatientKey.
		-------------------------------------------------
		SET	@PatientID = (
			SELECT PatientID
			FROM phrm.Patient
			WHERE BusinessEntityID = @BusinessEntityID
				AND SourcePatientKey = @SourcePatientKey
		);
		
		SET @StopSearch = 1;
	END
	
	-- If @PersonID was provided and @PatientID has not been populated, then perform PersonID lookup.
	IF @PersonID IS NOT NULL AND @StopSearch = 0
	BEGIN
	
		SET @PatientID = NULL;
		SET @SourcePatientKey = NULL;		
	
		-------------------------------------------------
		-- Fetch patient info by PersonID.
		-------------------------------------------------
		SET	@PatientID = (
			SELECT PatientID
			FROM phrm.Patient
			WHERE BusinessEntityID = @BusinessEntityID
				AND PersonID = @PersonID
		);
		
		SET @StopSearch = 1;
	END

	-------------------------------------------------
	-- Retrieve return data.
	-------------------------------------------------	
	SELECT 
		@PatientID = PatientID,
		@PersonID = PersonID,
		@SourcePatientKey = SourcePatientKey
	FROM phrm.Patient
	WHERE PatientID = @PatientID
	
	-------------------------------------------------
	-- Determine the patient's existence.
	-------------------------------------------------
	IF ISNULL(@PatientID, 0) <> 0
	BEGIN		
		SET @Exists = 1;			
	END	
	
		
END
