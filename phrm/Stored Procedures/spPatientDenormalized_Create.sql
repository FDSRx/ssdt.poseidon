﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/17/2015
-- Description:	Creates a new PatientDenormalized object.
-- SAMPLE CALL:
/*

DECLARE
	@PatientID BIGINT = 30,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@Debug BIT = 1

EXEC phrm.spPatientDenormalized_Create
	@PatientID = @PatientID OUTPUT,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@Debug = @Debug

SELECT @PatientID AS PatientID


*/

-- SELECT * FROM phrm.Patient
-- SELECT * FROM phrm.PatientDenormalized
-- =============================================
CREATE PROCEDURE [phrm].[spPatientDenormalized_Create]
	@PatientID BIGINT = NULL OUTPUT,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@PatientID=' + dbo.fnToStringOrEmpty(@PatientID)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey)  + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;


	-------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	-------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------------------
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @PatientKeyType = ISNULL(@PatientKeyType, 'SRC');
	SET @Debug = ISNULL(@Debug, 0);


	-------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------------------------------
	SET @PatientID = 
		CASE 
			WHEN @PatientID IS NOT NULL THEN @PatientID 
			ELSE phrm.fnPatientIDTranslator(@BusinessKey, @BusinessKeyType, @PatientKey, @PatientKeyType) 
		END;

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@PatientID AS PatientID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@PatientKey AS PatientKey, @PatientKeyType AS PatientKeyType, @Args AS Arguments
	END

	-------------------------------------------------------------------------------------------------------------------
	-- Create PatientDenormalized object.
	-------------------------------------------------------------------------------------------------------------------
	INSERT INTO phrm.PatientDenormalized (
		PatientID,
		StoreID,
		Nabp,
		SourceStoreID,
		StoreName,
		PersonID,
		SourcePatientKey,
		Title,
		FirstName,
		MiddleName,
		LastName,
		Suffix,
		BirthDate,
		GenderID,
		GenderCode,
		GenderName,
		LanguageID,
		LanguageCode,
		LanguageName,
		AddressLine1,
		AddressLine2,
		City,
		StateProvinceID,
		State,
		PostalCode,
		HomePhone,
		IsCallAllowedToHomePhone,
		IsTextAllowedToHomePhone,
		IsHomePhonePreferred,
		MobilePhone,
		IsCallAllowedToMobilePhone,
		IsTextAllowedToMobilePhone,
		IsMobilePHonePreferred,
		PrimaryEmail,
		IsEmailAllowedToPrimaryEmail,
		IsPrimaryEmailPreferred,
		AlternateEmail,
		IsEmailAllowedToAlternateEmail,
		IsAlternateEmailPreferred,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	)
	SELECT
		PatientID,
		StoreID,
		Nabp,
		SourceStoreID,
		StoreName,
		PersonID,
		SourcePatientKey,
		Title,
		FirstName,
		MiddleName,
		LastName,
		Suffix,
		BirthDate,
		GenderID,
		GenderCode,
		GenderName,
		LanguageID,
		LanguageCode,
		LanguageName,
		AddressLine1,
		AddressLine2,
		City,
		StateProvinceID,
		State,
		PostalCode,
		HomePhone,
		IsCallAllowedToHomePhone,
		IsTextAllowedToHomePhone,
		IsHomePhonePreferred,
		MobilePhone,
		IsCallAllowedToMobilePhone,
		IsTextAllowedToMobilePhone,
		IsMobilePHonePreferred,
		PrimaryEmail,
		IsEmailAllowedToPrimaryEmail,
		IsPrimaryEmailPreferred,
		AlternateEmail,
		IsEmailAllowedToAlternateEmail,
		IsAlternateEmailPreferred,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	FROM phrm.fnGetPatientDemographicsFromPatientID(@PatientID)



END
