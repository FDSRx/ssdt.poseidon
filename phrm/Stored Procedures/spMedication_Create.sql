﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description:	Creates a new Medication object entry.
-- SAMPLE CALL:
/*
DECLARE
	@MedicationID BIGINT = NULL

EXEC phrm.spMedication_Create
	@PersonID = 135590,
	@MedicationTypeID = 1,
	@Name = 'Advil',
	@Strength = '500 Mg',
	@CreatedBy = 'dhughes',
	@MedicationID = @MedicationID OUTPUT

SELECT @MedicationID AS MedicationID
	
*/
-- SELECT * FROM phrm.Medication
-- SELECT * FROM phrm.MedicationType
-- SELECT * FROM phrm.PrescriberType
-- SELECT * FROM phrm.vwPatient
-- =============================================
CREATE PROCEDURE [phrm].[spMedication_Create]
	@PersonID BIGINT,
	@MedicationTypeID INT,
	@Name VARCHAR(256),
	@NDC VARCHAR(25) = NULL,
	@Quantity DECIMAL(9,2) = NULL,
	@Strength VARCHAR(50) = NULL,
	@Directions VARCHAR(1000) = NULL,
	@Indication VARCHAR(1000) = NULL,
	@DateFilled DATETIME = NULL,
	@PrescriberTypeID INT = NULL,
	@PrescriberFirstName VARCHAR(75) = NULL,
	@PrescriberLastName VARCHAR(75) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@MedicationID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------------
	SET @MedicationID = NULL;
	
	---------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------	
	DECLARE
		@ErrorMessage VARCHAR(4000) = NULL
			
	
	---------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	---------------------------------------------------------------------------
	-- PersonID
	IF @PersonID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Medication object. Object reference (@PersonID) is not set to an instance of an object. ' +
			'The @PersonID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- MedicationTypeID
	IF @MedicationTypeID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Medication object. Object reference (@MedicationTypeID) is not set to an instance of an object. ' +
			'The @MedicationTypeID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- Medication name
	IF dbo.fnIsNullOrWhiteSpace(@Name) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to create Medication object. Object reference (@Name) is not set to an instance of an object. ' +
			'The @Name parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END		
	
	
	---------------------------------------------------------------------------
	-- Create new Medication object entry.
	---------------------------------------------------------------------------	
	INSERT INTO phrm.Medication (
		PersonID,
		MedicationTypeID,
		Name,
		NDC,
		Quantity,
		Strength,
		Directions,
		Indication,
		DateFilled,
		PrescriberTypeID,
		PrescriberFirstName,
		PrescriberLastName,
		CreatedBy
	)
	SELECT
		@PersonID,
		@MedicationTypeID,
		@Name,
		@NDC,
		@Quantity,
		@Strength,
		@Directions,
		@Indication,
		@DateFilled,
		@PrescriberTypeID,
		@PrescriberFirstName,
		@PrescriberLastName,
		@CreatedBy
	
	-- Retrieve record identifier
	SET @MedicationID = SCOPE_IDENTITY();
	
	
END
