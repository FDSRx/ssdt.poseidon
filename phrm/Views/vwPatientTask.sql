﻿








CREATE VIEW phrm.[vwPatientTask]
AS
SELECT
  tsk.NoteID,
  n.BusinessEntityID,
  enttyp.Name AS BusinessEntityTypeName,
  pat.StoreID AS BusinessID,
  pat.Nabp,
  pat.StoreName AS BusinessName,
  pat.PatientID,
  pat.SourcePatientKey,
  pat.PersonID,
  pat.FirstName,
  pat.LastName,
  pat.BirthDate,
  n.ScopeID,
  s.Code AS ScopeCode,
  s.Name AS ScopeName,
  n.NoteTypeID,
  typ.Code AS NoteTypeCode,
  typ.Name AS NoteTypeName,
  n.Title,
  n.Memo,
  n.NotebookID,
  nb.Code AS NotebookCode,
  nb.Name AS NotebookName,
  n.PriorityID,
  p.Code AS PriorityCode,
  p.Name AS PriorityName,
  tsk.AssignedToID,
  tsk.AssignedToString,
  tsk.AssignedByID,
  tsk.AssignedByString,
  tsk.DateDue,
  tsk.CompletedByID,
  tsk.CompletedByString,
  tsk.DateCompleted,
  o.OriginID,
  o.Code AS OriginCode,
  o.Name AS OriginName,
  n.OriginDataKey,
  n.AdditionalData,
  n.CorrelationKey,
  tsk.TaskStatusID,
  ts.Code AS TaskStatusCode,
  ts.Name AS TaskStatusName,
  tsk.ParentTaskID,
  tsk.rowguid,
  tsk.DateCreated,
  tsk.DateModified,
  tsk.CreatedBy,
  tsk.ModifiedBy
FROM dbo.Task AS tsk
INNER JOIN dbo.Note AS n -- SELECT * FROM dbo.Note
  ON tsk.NoteID = n.NoteID
JOIN phrm.vwPatient pat -- SELECT * FROM phrm.vwPatient
	ON pat.PersonID = n.BusinessEntityID
LEFT OUTER JOIN dbo.Priority AS p
  ON n.PriorityID = p.PriorityID
LEFT OUTER JOIN dbo.Scope AS s
  ON n.ScopeID = s.ScopeID
LEFT OUTER JOIN dbo.Origin AS o
  ON n.OriginID = o.OriginID
LEFT OUTER JOIN dbo.Notebook AS nb
  ON n.NotebookID = nb.NotebookID
LEFT OUTER JOIN dbo.NoteType AS typ
  ON n.NoteTypeID = typ.NoteTypeID
INNER JOIN dbo.BusinessEntity AS ent
  ON n.BusinessEntityID = ent.BusinessEntityID
INNER JOIN dbo.BusinessEntityType AS enttyp
  ON ent.BusinessEntityTypeID = enttyp.BusinessEntityTypeID
LEFT JOIN dbo.TaskStatus ts
  ON tsk.TaskStatusID = ts.TaskStatusID









