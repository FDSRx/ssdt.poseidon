﻿
CREATE VIEW [phrm].[vwPatient]
AS
SELECT     pat.PatientID, pat.BusinessEntityID AS StoreID, sto.NABP, sto.SourceStoreID, sto.Name AS StoreName, pat.PersonID, pat.SourcePatientKey, psn.FirstName, 
                      psn.LastName, psn.MiddleName, psn.Title, psn.Suffix, psn.BirthDate, psn.Gender, psn.Language, psn.AddressLine1, psn.AddressLine2, psn.City, psn.State, 
                      psn.PostalCode, 
                      psn.HomePhone,
                      psn.IsCallAllowedToHomePhone,
                      psn.IsTextAllowedToHomePhone,
                      psn.IsHomePhonePreferred,
                      psn.MobilePhone, 
                      psn.IsCallAllowedToMobilePhone,
                      psn.IsTextAllowedToMobilePhone,
                      psn.IsMobilePhonePreferred,
                      psn.PrimaryEmail,
                      psn.IsEmailAllowedToPrimaryEmail,
                      psn.IsPrimaryEmailPreferred, 
                      psn.AlternateEmail, 
                      psn.IsEmailAllowedToAlternateEmail,
                      psn.IsAlternateEmailPreferred,
                      pat.rowguid, pat.DateCreated, pat.DateModified, pat.CreatedBy, pat.ModifiedBy
FROM         phrm.Patient AS pat INNER JOIN
                      dbo.vwPerson AS psn ON pat.PersonID = psn.PersonID INNER JOIN
                      dbo.Store AS sto ON pat.BusinessEntityID = sto.BusinessEntityID


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "pat"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "psn"
            Begin Extent = 
               Top = 6
               Left = 250
               Bottom = 125
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sto"
            Begin Extent = 
               Top = 6
               Left = 448
               Bottom = 125
               Right = 617
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 39
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
 ', @level0type = N'SCHEMA', @level0name = N'phrm', @level1type = N'VIEW', @level1name = N'vwPatient';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'        Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'phrm', @level1type = N'VIEW', @level1name = N'vwPatient';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'phrm', @level1type = N'VIEW', @level1name = N'vwPatient';

