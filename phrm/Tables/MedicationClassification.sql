﻿CREATE TABLE [phrm].[MedicationClassification] (
    [MedicationClassificationID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]                       VARCHAR (25)     NOT NULL,
    [Name]                       VARCHAR (50)     NOT NULL,
    [Description]                VARCHAR (1000)   NULL,
    [rowguid]                    UNIQUEIDENTIFIER CONSTRAINT [DF_MedicationClassification_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                DATETIME         CONSTRAINT [DF_MedicationClassification_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]               DATETIME         CONSTRAINT [DF_MedicationClassification_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                  VARCHAR (256)    NULL,
    [ModifiedBy]                 VARCHAR (256)    NULL,
    CONSTRAINT [PK_MedicationClassification] PRIMARY KEY CLUSTERED ([MedicationClassificationID] ASC)
);

