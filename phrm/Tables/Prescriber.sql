﻿CREATE TABLE [phrm].[Prescriber] (
    [BusinessEntityID] BIGINT           NOT NULL,
    [PrescriberTypeID] INT              NOT NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Prescriber_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_Prescriber_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_Prescriber_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Prescriber] PRIMARY KEY CLUSTERED ([BusinessEntityID] ASC),
    CONSTRAINT [FK_Prescriber_Person] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[Person] ([BusinessEntityID]),
    CONSTRAINT [FK_Prescriber_PrescriberType] FOREIGN KEY ([PrescriberTypeID]) REFERENCES [phrm].[PrescriberType] ([PrescriberTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Prescriber_rowguid]
    ON [phrm].[Prescriber]([rowguid] ASC);

