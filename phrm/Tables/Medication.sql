﻿CREATE TABLE [phrm].[Medication] (
    [MedicationID]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [PersonID]            BIGINT           NOT NULL,
    [MedicationTypeID]    INT              NOT NULL,
    [Name]                VARCHAR (256)    NOT NULL,
    [NDC]                 VARCHAR (25)     NULL,
    [Quantity]            DECIMAL (9, 2)   NULL,
    [Strength]            VARCHAR (50)     NULL,
    [Directions]          VARCHAR (1000)   NULL,
    [Indication]          VARCHAR (1000)   NULL,
    [DateFilled]          DATETIME         NULL,
    [PrescriberTypeID]    INT              NULL,
    [PrescriberFirstName] VARCHAR (75)     NULL,
    [PrescriberLastName]  VARCHAR (75)     NULL,
    [rowguid]             UNIQUEIDENTIFIER CONSTRAINT [DF_Medication_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]         DATETIME         CONSTRAINT [DF_Medication_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]        DATETIME         CONSTRAINT [DF_Medication_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           VARCHAR (256)    NULL,
    [ModifiedBy]          VARCHAR (256)    NULL,
    CONSTRAINT [PK_Medication] PRIMARY KEY CLUSTERED ([MedicationID] ASC),
    CONSTRAINT [FK_Medication_MedicationType] FOREIGN KEY ([MedicationTypeID]) REFERENCES [phrm].[MedicationType] ([MedicationTypeID]),
    CONSTRAINT [FK_Medication_Person] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([BusinessEntityID]),
    CONSTRAINT [FK_Medication_PrescriberType] FOREIGN KEY ([PrescriberTypeID]) REFERENCES [phrm].[PrescriberType] ([PrescriberTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Medication_rowguid]
    ON [phrm].[Medication]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description:	Audits a record change of the Medication table
-- SELECT * FROM dbo.Medication_History
-- TRUNCATE TABLE dbo.Medication_History
-- =============================================
CREATE TRIGGER phrm.[trigMedication_Audit]
   ON  phrm.Medication
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-- Audit inserted records (updated or newly created)
	INSERT INTO phrm.Medication_History (
		MedicationID,
		PersonID,
		MedicationTypeID,
		Name,
		Quantity,
		Strength,
		Directions,
		DateFilled,
		PrescriberTypeID,
		PrescriberFirstName,
		PrescriberLastName,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		MedicationID,
		PersonID,
		MedicationTypeID,
		Name,
		Quantity,
		Strength,
		Directions,
		DateFilled,
		PrescriberTypeID,
		PrescriberFirstName,
		PrescriberLastName,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	--SELECT * FROM dbo.Medication
	
	

END
