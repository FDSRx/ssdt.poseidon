﻿CREATE TABLE [phrm].[Condition] (
    [ConditionID]      BIGINT           IDENTITY (1, 1) NOT NULL,
    [ConditionTypeID]  INT              NOT NULL,
    [BusinessEntityID] BIGINT           NOT NULL,
    [CodeQualifierID]  INT              NOT NULL,
    [Code]             VARCHAR (25)     NULL,
    [Name]             VARCHAR (512)    NOT NULL,
    [Description]      VARCHAR (1000)   NULL,
    [OriginID]         INT              NOT NULL,
    [DateInactive]     DATETIME         NULL,
    [HashKey]          VARCHAR (256)    NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Condition_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_Condition_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_Condition_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Condition] PRIMARY KEY CLUSTERED ([ConditionID] ASC),
    CONSTRAINT [FK_Condition_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Condition_CodeQualifier] FOREIGN KEY ([CodeQualifierID]) REFERENCES [dbo].[CodeQualifier] ([CodeQualifierID]),
    CONSTRAINT [FK_Condition_ConditionType] FOREIGN KEY ([ConditionTypeID]) REFERENCES [phrm].[ConditionType] ([ConditionTypeID]),
    CONSTRAINT [FK_Condition_DataSource] FOREIGN KEY ([OriginID]) REFERENCES [dbo].[Origin] ([OriginID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Condition_rowguid]
    ON [phrm].[Condition]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Condition_UniqueRow]
    ON [phrm].[Condition]([ConditionTypeID] ASC, [CodeQualifierID] ASC, [BusinessEntityID] ASC, [Name] ASC);

