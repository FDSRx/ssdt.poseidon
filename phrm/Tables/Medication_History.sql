﻿CREATE TABLE [phrm].[Medication_History] (
    [MedicationHIstoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [MedicationID]        BIGINT           NOT NULL,
    [PersonID]            BIGINT           NOT NULL,
    [MedicationTypeID]    INT              NOT NULL,
    [Name]                VARCHAR (256)    NOT NULL,
    [Quantity]            DECIMAL (9, 2)   NULL,
    [Strength]            VARCHAR (50)     NULL,
    [Directions]          VARCHAR (1000)   NULL,
    [DateFilled]          DATETIME         NULL,
    [PrescriberTypeID]    INT              NULL,
    [PrescriberFirstName] VARCHAR (75)     NULL,
    [PrescriberLastName]  VARCHAR (75)     NULL,
    [rowguid]             UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]         DATETIME         NOT NULL,
    [DateModified]        DATETIME         NOT NULL,
    [CreatedBy]           VARCHAR (256)    NULL,
    [ModifiedBy]          VARCHAR (256)    NULL,
    [AuditGuid]           UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [AuditAction]         VARCHAR (10)     NOT NULL,
    [DateAudited]         DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Medication_History] PRIMARY KEY CLUSTERED ([MedicationHIstoryID] ASC)
);

