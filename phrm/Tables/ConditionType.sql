﻿CREATE TABLE [phrm].[ConditionType] (
    [ConditionTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]            VARCHAR (25)     NOT NULL,
    [Name]            VARCHAR (25)     NOT NULL,
    [Description]     VARCHAR (1000)   NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_ConditionType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_ConditionType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_ConditionType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)    NULL,
    [ModifiedBy]      VARCHAR (256)    NULL,
    CONSTRAINT [PK_ConditionType] PRIMARY KEY CLUSTERED ([ConditionTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ConditionType_Code]
    ON [phrm].[ConditionType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ConditionType_Name]
    ON [phrm].[ConditionType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ConditionType_rowguid]
    ON [phrm].[ConditionType]([rowguid] ASC);

