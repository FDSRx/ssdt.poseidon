﻿CREATE TABLE [phrm].[Patient] (
    [PatientID]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [PersonID]         BIGINT           NOT NULL,
    [BusinessEntityID] BIGINT           NOT NULL,
    [SourcePatientKey] VARCHAR (25)     NOT NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Patient_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_Patient_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_Patient_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED ([PatientID] ASC),
    CONSTRAINT [FK_Patient_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Patient_Person] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Patient_BizPatientKey]
    ON [phrm].[Patient]([BusinessEntityID] ASC, [SourcePatientKey] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Patient_rowguid]
    ON [phrm].[Patient]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Patient_BizPerson]
    ON [phrm].[Patient]([BusinessEntityID] ASC, [PersonID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_Patient_PersonID_AK1]
    ON [phrm].[Patient]([PersonID] ASC)
    INCLUDE([BusinessEntityID]);

