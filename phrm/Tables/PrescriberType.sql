﻿CREATE TABLE [phrm].[PrescriberType] (
    [PrescriberTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]             VARCHAR (25)     NOT NULL,
    [Name]             VARCHAR (50)     NOT NULL,
    [Description]      VARCHAR (1000)   NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_PrescriberType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_PrescriberType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_PrescriberType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_PrescriberType] PRIMARY KEY CLUSTERED ([PrescriberTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PrescriberType_Code]
    ON [phrm].[PrescriberType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PrescriberType_Name]
    ON [phrm].[PrescriberType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PrescriberType_rowguid]
    ON [phrm].[PrescriberType]([rowguid] ASC);

