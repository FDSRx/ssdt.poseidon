﻿CREATE TABLE [phrm].[MedicationType] (
    [MedicationTypeID]           INT              IDENTITY (1, 1) NOT NULL,
    [MedicationClassificationID] INT              NOT NULL,
    [Code]                       VARCHAR (25)     NOT NULL,
    [Name]                       VARCHAR (50)     NOT NULL,
    [Description]                VARCHAR (1000)   NULL,
    [rowguid]                    UNIQUEIDENTIFIER CONSTRAINT [DF_MedicationType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                DATETIME         CONSTRAINT [DF_MedicationType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]               DATETIME         CONSTRAINT [DF_MedicationType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                  VARCHAR (256)    NULL,
    [ModifiedBy]                 VARCHAR (256)    NULL,
    CONSTRAINT [PK_MedicationType] PRIMARY KEY CLUSTERED ([MedicationTypeID] ASC),
    CONSTRAINT [FK_MedicationType_MedicationClassification] FOREIGN KEY ([MedicationClassificationID]) REFERENCES [phrm].[MedicationClassification] ([MedicationClassificationID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MedicationType_Code]
    ON [phrm].[MedicationType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MedicationType_Name]
    ON [phrm].[MedicationType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MedicationType_rowguid]
    ON [phrm].[MedicationType]([rowguid] ASC);

