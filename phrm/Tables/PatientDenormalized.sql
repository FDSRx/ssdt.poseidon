﻿CREATE TABLE [phrm].[PatientDenormalized] (
    [PatientID]                      BIGINT        NOT NULL,
    [StoreID]                        BIGINT        NULL,
    [Nabp]                           VARCHAR (50)  NULL,
    [SourceStoreID]                  VARCHAR (50)  NULL,
    [StoreName]                      VARCHAR (256) NULL,
    [PersonID]                       BIGINT        NULL,
    [SourcePatientKey]               VARCHAR (50)  NULL,
    [Title]                          VARCHAR (25)  NULL,
    [FirstName]                      VARCHAR (75)  NULL,
    [MiddleName]                     VARCHAR (75)  NULL,
    [LastName]                       VARCHAR (75)  NULL,
    [Suffix]                         VARCHAR (10)  NULL,
    [BirthDate]                      DATE          NULL,
    [GenderID]                       INT           NULL,
    [GenderCode]                     VARCHAR (25)  NULL,
    [GenderName]                     VARCHAR (50)  NULL,
    [LanguageID]                     INT           NULL,
    [LanguageCode]                   VARCHAR (25)  NULL,
    [LanguageName]                   VARCHAR (50)  NULL,
    [AddressLine1]                   VARCHAR (150) NULL,
    [AddressLine2]                   VARCHAR (150) NULL,
    [City]                           VARCHAR (50)  NULL,
    [StateProvinceID]                INT           NULL,
    [State]                          VARCHAR (25)  NULL,
    [PostalCode]                     VARCHAR (25)  NULL,
    [HomePhone]                      VARCHAR (25)  NULL,
    [IsCallAllowedToHomePhone]       BIT           CONSTRAINT [DF_PatientDenormalized_IsCallAllowedToHomePhone] DEFAULT ((0)) NOT NULL,
    [IsTextAllowedToHomePhone]       BIT           CONSTRAINT [DF_PatientDenormalized_IsTextAllowedToHomePhone] DEFAULT ((0)) NOT NULL,
    [IsHomePhonePreferred]           BIT           CONSTRAINT [DF_PatientDenormalized_IsHomePhonePreferred] DEFAULT ((0)) NOT NULL,
    [MobilePhone]                    VARCHAR (25)  NULL,
    [IsCallAllowedToMobilePhone]     BIT           CONSTRAINT [DF_PatientDenormalized_IsCallAllowedToMobilePhone] DEFAULT ((0)) NOT NULL,
    [IsTextAllowedToMobilePhone]     BIT           CONSTRAINT [DF_PatientDenormalized_IsTextAllowedToMobilePhone] DEFAULT ((0)) NOT NULL,
    [IsMobilePhonePreferred]         BIT           CONSTRAINT [DF_PatientDenormalized_IsMobilePHonePreferred] DEFAULT ((0)) NOT NULL,
    [PrimaryEmail]                   VARCHAR (256) NULL,
    [IsEmailAllowedToPrimaryEmail]   BIT           CONSTRAINT [DF_PatientDenormalized_IsEmailAllowedToPrimaryEmail] DEFAULT ((0)) NOT NULL,
    [IsPrimaryEmailPreferred]        BIT           CONSTRAINT [DF_PatientDenormalized_IsPrimaryEmailPreferred] DEFAULT ((0)) NOT NULL,
    [AlternateEmail]                 VARCHAR (256) NULL,
    [IsEmailAllowedToAlternateEmail] BIT           CONSTRAINT [DF_PatientDenormalized_IsEmailAllowedToAlternateEmail] DEFAULT ((0)) NOT NULL,
    [IsAlternateEmailPreferred]      BIT           CONSTRAINT [DF_PatientDenormalized_IsAlternateEmailPreferred] DEFAULT ((0)) NOT NULL,
    [DateCreated]                    DATETIME      CONSTRAINT [DF_PatientDenormalized_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                   DATETIME      CONSTRAINT [DF_PatientDenormalized_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                      VARCHAR (256) NULL,
    [ModifiedBy]                     VARCHAR (256) NULL,
    CONSTRAINT [PK_PatientDenormalized] PRIMARY KEY CLUSTERED ([PatientID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PatientDenormalized_NabpSourcePatientKey]
    ON [phrm].[PatientDenormalized]([Nabp] ASC, [SourcePatientKey] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PatientDenormalized_PersonID]
    ON [phrm].[PatientDenormalized]([PersonID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PatientDenormalized_StorePerson]
    ON [phrm].[PatientDenormalized]([StoreID] ASC, [PersonID] ASC);

