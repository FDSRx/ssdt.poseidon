﻿CREATE TABLE [phrm].[PatientKeyType] (
    [PatientKeyTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]             VARCHAR (25)     NOT NULL,
    [Name]             VARCHAR (256)    NOT NULL,
    [Description]      VARCHAR (1000)   NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_PatientKeyType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_PatientKeyType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_PatientKeyType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_PatientKeyType] PRIMARY KEY CLUSTERED ([PatientKeyTypeID] ASC) WITH (FILLFACTOR = 90)
);

