﻿


CREATE VIEW [news].[vwArticleHelp] AS 
/*
 =======================================================================
 Author:		Alex Korolev
 Create date:	06/10/2016
 Description:	Logical Unit of Help Article
 SAMPLE CALL:   Select * from [news].[vwArticleHelp]
 Modifications:
 =======================================================================
*/

SELECT
	ROW_NUMBER() OVER (ORDER BY art.ArticleID) as RecordID,     
	art.ArticleID,
	appl.Name AS ApplicationName,
	appl.ApplicationId,
	bus.Name AS BusinessName,
	hlp.BusinessID,
	hlp.HelpID,
	art.ArticleTypeID,
	art.ArticleTypeCode,
	art.ArticleTypeName,
	art.ArticleFormatID,
	art.ArticleFormatCode,
	art.ArticleFormatName,
	art.CategoryID,
	art.CategoryCode,
	art.CategoryName,
	art.SectionID,
	art.SectionCode,
	art.SectionName,
	art.Title,
	art.ArticleContent,
	art.Description,
	art.AuthorID,
	art.FirstName,
	art.LastName,
	art.FilePath,
	art.IsInactive,
	art.SortOrder,
	art.rowguid,
	art.DateCreated,
	art.DateModified,
	art.CreatedBy,
	art.ModifiedBy
FROM news.vwArticle art
LEFT OUTER JOIN help.Help hlp
ON art.ArticleID = hlp.ArticleID
LEFT OUTER JOIN dbo.Application appl
ON hlp.ApplicationID = appl.ApplicationID
LEFT OUTER JOIN dbo.Business bus
ON hlp.BusinessID = bus.BusinessEntityID
