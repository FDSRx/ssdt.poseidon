﻿





CREATE VIEW news.vwArticle AS 

SELECT
	art.ArticleID,
	art.ArticleTypeID,
	typ.Code AS ArticleTypeCode,
	typ.Name AS ArticleTypeName,
	art.ArticleFormatID,
	frmt.Code AS ArticleFormatCode,
	frmt.Name AS ArticleFormatName,
	art.CategoryID,
	cat.Code AS CategoryCode,
	cat.Name AS CategoryName,
	art.SectionID,
	sec.Code AS SectionCode,
	sec.Name AS SectionName,
	art.Title,
	art.ArticleContent,
	art.Description,
	art.AuthorID,
	psn.FirstName,
	psn.LastName,
	art.FilePath,
	art.IsInactive,
	art.SortOrder,
	art.rowguid,
	art.DateCreated,
	art.DateModified,
	art.CreatedBy,
	art.ModifiedBy
FROM news.Article art
	LEFT JOIN news.ArticleType typ
		ON typ.ArticleTypeID = art.ArticleTypeID
	LEFT JOIN news.ArticleFormat frmt
		ON frmt.ArticleFormatID = art.ArticleFormatID
	LEFT JOIN news.Category cat
		ON cat.CategoryID = art.CategoryID
	LEFT JOIN news.Section sec
		ON sec.SectionID = art.SectionID
	LEFT JOIN dbo.Person psn
		ON psn.BusinessEntityID = art.AuthorID