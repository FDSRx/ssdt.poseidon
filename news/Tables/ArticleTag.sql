﻿CREATE TABLE [news].[ArticleTag] (
    [ArticleTagID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ArticleID]    BIGINT           NOT NULL,
    [TagID]        BIGINT           NOT NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_ArticleTag_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_ArticleTag_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_ArticleTag_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_ArticleTag] PRIMARY KEY CLUSTERED ([ArticleTagID] ASC),
    CONSTRAINT [FK_ArticleTag_Article] FOREIGN KEY ([ArticleID]) REFERENCES [news].[Article] ([ArticleID]),
    CONSTRAINT [FK_ArticleTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ArticleTag_ArticleTag]
    ON [news].[ArticleTag]([ArticleID] ASC, [TagID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ArticleTag_rowguid]
    ON [news].[ArticleTag]([rowguid] ASC);

