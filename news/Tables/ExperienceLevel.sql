﻿CREATE TABLE [news].[ExperienceLevel] (
    [ExperienceLevelID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]              VARCHAR (25)     NOT NULL,
    [Name]              VARCHAR (50)     NOT NULL,
    [Description]       VARCHAR (1000)   NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_ExperienceLevel_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_ExperienceLevel_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_ExperienceLevel_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_ExperienceLevel] PRIMARY KEY CLUSTERED ([ExperienceLevelID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ExperienceLevel_Code]
    ON [news].[ExperienceLevel]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ExperienceLevel_Name]
    ON [news].[ExperienceLevel]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ExperienceLevel_rowguid]
    ON [news].[ExperienceLevel]([rowguid] ASC);

