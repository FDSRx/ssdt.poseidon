﻿CREATE TABLE [news].[ArticleFormat] (
    [ArticleFormatID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]            VARCHAR (25)     NOT NULL,
    [Name]            VARCHAR (50)     NOT NULL,
    [Description]     VARCHAR (1000)   NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_ArticleFormat_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_ArticleFormat_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_ArticleFormat_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)    NULL,
    [ModifiedBy]      VARCHAR (256)    NULL,
    CONSTRAINT [PK_ArticleFormat] PRIMARY KEY CLUSTERED ([ArticleFormatID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ArticleFormat_Code]
    ON [news].[ArticleFormat]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ArticleFormat_Name]
    ON [news].[ArticleFormat]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ArticleFormat_rowguid]
    ON [news].[ArticleFormat]([rowguid] ASC);

