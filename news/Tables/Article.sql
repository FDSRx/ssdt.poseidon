﻿CREATE TABLE [news].[Article] (
    [ArticleID]       BIGINT           IDENTITY (1, 1) NOT NULL,
    [ArticleTypeID]   INT              NOT NULL,
    [ArticleFormatID] INT              NOT NULL,
    [CategoryID]      INT              NOT NULL,
    [SectionID]       INT              NULL,
    [Title]           VARCHAR (1000)   NOT NULL,
    [ArticleContent]  VARCHAR (MAX)    NULL,
    [Description]     VARCHAR (2000)   NULL,
    [AuthorID]        BIGINT           NULL,
    [FilePath]        VARCHAR (256)    NULL,
    [IsInactive]      BIT              CONSTRAINT [DF_Table_1_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]       DECIMAL (9, 2)   CONSTRAINT [DF_Article_SortOrder] DEFAULT ((0)) NOT NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_Article_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_Article_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_Article_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)    NULL,
    [ModifiedBy]      VARCHAR (256)    NULL,
    CONSTRAINT [PK_Article] PRIMARY KEY CLUSTERED ([ArticleID] ASC),
    CONSTRAINT [FK_Article_ArticleFormat] FOREIGN KEY ([ArticleFormatID]) REFERENCES [news].[ArticleFormat] ([ArticleFormatID]),
    CONSTRAINT [FK_Article_ArticleType] FOREIGN KEY ([ArticleTypeID]) REFERENCES [news].[ArticleType] ([ArticleTypeID]),
    CONSTRAINT [FK_Article_Category] FOREIGN KEY ([CategoryID]) REFERENCES [news].[Category] ([CategoryID]),
    CONSTRAINT [FK_Article_Person] FOREIGN KEY ([AuthorID]) REFERENCES [dbo].[Person] ([BusinessEntityID]),
    CONSTRAINT [FK_Article_Section] FOREIGN KEY ([SectionID]) REFERENCES [news].[Section] ([SectionID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Article_rowguid]
    ON [news].[Article]([rowguid] ASC);

