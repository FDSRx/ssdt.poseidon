﻿CREATE TABLE [news].[Category] (
    [CategoryID]   INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Category_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Category_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Category_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([CategoryID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Category_Code]
    ON [news].[Category]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Category_Name]
    ON [news].[Category]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Category_rowguid]
    ON [news].[Category]([rowguid] ASC);

