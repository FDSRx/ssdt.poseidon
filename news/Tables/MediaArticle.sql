﻿CREATE TABLE [news].[MediaArticle] (
    [ArticleID]       BIGINT           NOT NULL,
    [MediaTypeID]     INT              NOT NULL,
    [DurationString]  VARCHAR (25)     NULL,
    [DurationMinutes] DECIMAL (9, 2)   NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_MediaArticle_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_MediaArticle_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_MediaArticle_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)    NULL,
    [ModifiedBy]      VARCHAR (256)    NULL,
    CONSTRAINT [PK_MediaArticle] PRIMARY KEY CLUSTERED ([ArticleID] ASC),
    CONSTRAINT [FK_MediaArticle_Article] FOREIGN KEY ([ArticleID]) REFERENCES [news].[Article] ([ArticleID]),
    CONSTRAINT [FK_MediaArticle_MediaType] FOREIGN KEY ([MediaTypeID]) REFERENCES [media].[MediaType] ([MediaTypeID])
);

