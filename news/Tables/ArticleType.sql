﻿CREATE TABLE [news].[ArticleType] (
    [ArticleTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]          VARCHAR (25)     NOT NULL,
    [Name]          VARCHAR (50)     NOT NULL,
    [Description]   VARCHAR (1000)   NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_ArticleType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_ArticleType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_ArticleType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_ArticleType] PRIMARY KEY CLUSTERED ([ArticleTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ArticleType_Code]
    ON [news].[ArticleType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ArticleType_Name]
    ON [news].[ArticleType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ArticleType_rowguid]
    ON [news].[ArticleType]([rowguid] ASC);

