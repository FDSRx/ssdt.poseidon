﻿CREATE TABLE [news].[Section] (
    [SectionID]    INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Section_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Section_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Section_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Section] PRIMARY KEY CLUSTERED ([SectionID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Section_Code]
    ON [news].[Section]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Section_Name]
    ON [news].[Section]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Section_rowguid]
    ON [news].[Section]([rowguid] ASC);

