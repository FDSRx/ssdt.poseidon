﻿CREATE TABLE [doc].[FileType] (
    [FileTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [Code]         NVARCHAR (10) NOT NULL,
    [Name]         NVARCHAR (50) NOT NULL,
    [DateModified] DATETIME      CONSTRAINT [DF_FileType_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FileType] PRIMARY KEY CLUSTERED ([FileTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FileType_Code]
    ON [doc].[FileType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FileType_Name]
    ON [doc].[FileType]([Name] ASC);

