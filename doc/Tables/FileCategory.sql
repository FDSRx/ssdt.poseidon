﻿CREATE TABLE [doc].[FileCategory] (
    [FileCategoryID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]           NVARCHAR (15)    NOT NULL,
    [Name]           NVARCHAR (50)    NOT NULL,
    [Description]    NVARCHAR (255)   NULL,
    [rowguid]        UNIQUEIDENTIFIER CONSTRAINT [DF_FileCategory_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]    DATETIME         CONSTRAINT [DF_FileCategory_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]   DATETIME         CONSTRAINT [DF_FileCategory_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FileCategory] PRIMARY KEY CLUSTERED ([FileCategoryID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FileCategory_Code]
    ON [doc].[FileCategory]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FileCategory_Name]
    ON [doc].[FileCategory]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FileCategory_rowguid]
    ON [doc].[FileCategory]([rowguid] ASC);

