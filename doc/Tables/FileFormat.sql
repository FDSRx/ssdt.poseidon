﻿CREATE TABLE [doc].[FileFormat] (
    [FileFormatID] INT            IDENTITY (1, 1) NOT NULL,
    [Code]         NVARCHAR (10)  NOT NULL,
    [Name]         NVARCHAR (50)  NOT NULL,
    [Description]  NVARCHAR (255) NULL,
    [DateCreated]  DATETIME       CONSTRAINT [DF_FileFormat_DateModified] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME       CONSTRAINT [DF_FileFormat_DateModified_1] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FileFormat] PRIMARY KEY CLUSTERED ([FileFormatID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FileFormat_Code]
    ON [doc].[FileFormat]([Code] ASC);


GO
CREATE NONCLUSTERED INDEX [UIX_FileFormat_Name]
    ON [doc].[FileFormat]([Name] ASC);

