﻿CREATE TABLE [doc].[Files] (
    [FileID]         BIGINT           IDENTITY (1, 1) NOT NULL,
    [OwnerID]        INT              NULL,
    [FileCategoryID] INT              NOT NULL,
    [Title]          VARCHAR (255)    NULL,
    [FileName]       VARCHAR (255)    NULL,
    [FileTypeID]     INT              NOT NULL,
    [FileFormatID]   INT              NULL,
    [FileExtension]  NVARCHAR (8)     NULL,
    [Revision]       NVARCHAR (5)     CONSTRAINT [DF_Files_Revision] DEFAULT ((0)) NULL,
    [ChangeNumber]   INT              CONSTRAINT [DF_Files_ChangeNumber] DEFAULT ((0)) NOT NULL,
    [FileSummary]    NVARCHAR (MAX)   NULL,
    [FilePath]       VARCHAR (1000)   NULL,
    [FileContent]    NVARCHAR (MAX)   NULL,
    [FileBinary]     VARBINARY (MAX)  NULL,
    [LanguageID]     INT              NOT NULL,
    [rowguid]        UNIQUEIDENTIFIER CONSTRAINT [DF_Files_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]    DATETIME         CONSTRAINT [DF_Files_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]   DATETIME         CONSTRAINT [DF_Files_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED ([FileID] ASC),
    CONSTRAINT [FK_Files_FileCategory] FOREIGN KEY ([FileCategoryID]) REFERENCES [doc].[FileCategory] ([FileCategoryID]),
    CONSTRAINT [FK_Files_FileFormat] FOREIGN KEY ([FileFormatID]) REFERENCES [doc].[FileFormat] ([FileFormatID]),
    CONSTRAINT [FK_Files_FileType] FOREIGN KEY ([FileTypeID]) REFERENCES [doc].[FileType] ([FileTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_File_rowguid]
    ON [doc].[Files]([rowguid] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revision number of the document. ', @level0type = N'SCHEMA', @level0name = N'doc', @level1type = N'TABLE', @level1name = N'Files', @level2type = N'COLUMN', @level2name = N'Revision';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'File path or URL pointer to file', @level0type = N'SCHEMA', @level0name = N'doc', @level1type = N'TABLE', @level1name = N'Files', @level2type = N'COLUMN', @level2name = N'FilePath';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Document content stored in text format.', @level0type = N'SCHEMA', @level0name = N'doc', @level1type = N'TABLE', @level1name = N'Files', @level2type = N'COLUMN', @level2name = N'FileContent';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Document file stored as binary format.', @level0type = N'SCHEMA', @level0name = N'doc', @level1type = N'TABLE', @level1name = N'Files', @level2type = N'COLUMN', @level2name = N'FileBinary';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The language of the document (i.e. English, Spanish, French, etc.)', @level0type = N'SCHEMA', @level0name = N'doc', @level1type = N'TABLE', @level1name = N'Files', @level2type = N'COLUMN', @level2name = N'LanguageID';

