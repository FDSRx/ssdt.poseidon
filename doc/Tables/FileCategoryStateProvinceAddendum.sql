﻿CREATE TABLE [doc].[FileCategoryStateProvinceAddendum] (
    [FileCategoryStateProvinceAddendumID] INT              IDENTITY (1, 1) NOT NULL,
    [FileCategoryID]                      INT              NOT NULL,
    [StateProvinceID]                     INT              NOT NULL,
    [FileID]                              BIGINT           NOT NULL,
    [rowguid]                             UNIQUEIDENTIFIER CONSTRAINT [DF_FileCategoryStateAddendum_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                         DATETIME         CONSTRAINT [DF_FileCategoryStateAddendum_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                        DATETIME         CONSTRAINT [DF_FileCategoryStateAddendum_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FileCategoryStateProvinceAddendum] PRIMARY KEY CLUSTERED ([FileCategoryStateProvinceAddendumID] ASC),
    CONSTRAINT [FK_FileCategoryStateAddendum_FileCategory] FOREIGN KEY ([FileCategoryID]) REFERENCES [doc].[FileCategory] ([FileCategoryID]),
    CONSTRAINT [FK_FileCategoryStateAddendum_Files] FOREIGN KEY ([FileID]) REFERENCES [doc].[Files] ([FileID]),
    CONSTRAINT [FK_FileCategoryStateAddendum_StateProvince] FOREIGN KEY ([StateProvinceID]) REFERENCES [dbo].[StateProvince] ([StateProvinceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FileCategoryStateAddendum_CategoryStateFile]
    ON [doc].[FileCategoryStateProvinceAddendum]([FileCategoryID] ASC, [StateProvinceID] ASC, [FileID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FileCategoryStateAddendum_rowguid]
    ON [doc].[FileCategoryStateProvinceAddendum]([rowguid] ASC);

