﻿CREATE TABLE [msg].[MessageVendor] (
    [MessageVendorID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]            NVARCHAR (15)    NOT NULL,
    [Name]            NVARCHAR (50)    NOT NULL,
    [Description]     NVARCHAR (255)   NOT NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_MessageVendor_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_MessageVendor_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_MessageVendor_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MessageVendor] PRIMARY KEY CLUSTERED ([MessageVendorID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageVendor_Code]
    ON [msg].[MessageVendor]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageVendor_Name]
    ON [msg].[MessageVendor]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageVendor_rowguid]
    ON [msg].[MessageVendor]([rowguid] ASC);

