﻿CREATE TABLE [msg].[MessageQueue] (
    [MessageQueueID]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [MessageOutboxID]    BIGINT           NULL,
    [MessageKey]         VARCHAR (256)    NULL,
    [MessageData]        XML              NULL,
    [BrokerMessageData]  XML              NULL,
    [IsInBrokerQueue]    BIT              CONSTRAINT [DF_MessageQueue_IsRequeued] DEFAULT ((0)) NOT NULL,
    [NumberOfTries]      INT              CONSTRAINT [DF_MessageQueue_NumberOfTries] DEFAULT ((0)) NOT NULL,
    [RequeueDate]        DATETIME         CONSTRAINT [DF_MessageQueue_RequeueDate] DEFAULT (getdate()) NOT NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_MessageQueue_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_MessageQueue_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_MessageQueue_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    [DoNotDeliverBefore] DATETIME         NULL,
    [ProcessingNotes]    NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_MessageQueue] PRIMARY KEY CLUSTERED ([MessageQueueID] ASC),
    CONSTRAINT [FK_MessageQueue_MessageOutbox] FOREIGN KEY ([MessageOutboxID]) REFERENCES [msg].[MessageOutbox] ([MessageOutboxID])
);

