﻿CREATE TABLE [msg].[MessageErrorLog] (
    [MessageErrorLogID] INT             IDENTITY (1, 1) NOT NULL,
    [ErrorTime]         DATETIME        CONSTRAINT [DF_MessageErrorLog_ErrorTime] DEFAULT (getdate()) NOT NULL,
    [UserName]          NVARCHAR (255)  NOT NULL,
    [ErrorNumber]       INT             NULL,
    [ErrorSeverity]     INT             NULL,
    [ErrorState]        INT             NULL,
    [ErrorProcedure]    NVARCHAR (126)  NULL,
    [ErrorLine]         INT             NULL,
    [ErrorMessage]      NVARCHAR (4000) NOT NULL,
    [MessageOutboxID]   INT             NULL,
    [Message]           NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_ErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED ([MessageErrorLogID] ASC)
);

