﻿CREATE TABLE [msg].[Sender] (
    [SenderID]          BIGINT           NOT NULL,
    [SenderTypeID]      INT              NOT NULL,
    [SenderKey]         UNIQUEIDENTIFIER CONSTRAINT [DF_Sender_SenderKey] DEFAULT (newid()) NOT NULL,
    [Name]              VARCHAR (255)    NULL,
    [Demographics]      XML              NULL,
    [ContractDateStart] DATETIME         CONSTRAINT [DF_Sender_ContractDateStart] DEFAULT (getdate()) NOT NULL,
    [ContractDateEnd]   DATETIME         CONSTRAINT [DF_Sender_ContractDateEnd] DEFAULT (CONVERT([datetime],'12/31/9999',(0))) NOT NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_Sender_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_Sender_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_Sender_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Sender] PRIMARY KEY CLUSTERED ([SenderID] ASC),
    CONSTRAINT [FK_Sender_BusinessEntity] FOREIGN KEY ([SenderID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Sender_SenderType] FOREIGN KEY ([SenderTypeID]) REFERENCES [msg].[SenderType] ([SenderTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Sender_SenderKey]
    ON [msg].[Sender]([SenderKey] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Sender_rowguid]
    ON [msg].[Sender]([rowguid] ASC);

