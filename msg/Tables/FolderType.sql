﻿CREATE TABLE [msg].[FolderType] (
    [FolderTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (25)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_FolderType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_FolderType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_FolderType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_FolderType] PRIMARY KEY CLUSTERED ([FolderTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FolderType_Code]
    ON [msg].[FolderType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FolderType_Name]
    ON [msg].[FolderType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FolderType_rowguid]
    ON [msg].[FolderType]([rowguid] ASC);

