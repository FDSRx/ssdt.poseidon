﻿CREATE TABLE [msg].[PredefinedMessage] (
    [PredefinedMessageID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]       INT              NULL,
    [BusinessEntityID]    BIGINT           NULL,
    [Name]                VARCHAR (256)    NOT NULL,
    [Header]              VARCHAR (MAX)    NULL,
    [HeaderFilePath]      VARCHAR (256)    NULL,
    [HeaderFileName]      VARCHAR (256)    NULL,
    [Body]                VARCHAR (MAX)    NULL,
    [BodyFilePath]        VARCHAR (256)    NULL,
    [BodyFileName]        VARCHAR (256)    NULL,
    [Footer]              VARCHAR (MAX)    NULL,
    [FooterFilePath]      VARCHAR (256)    NULL,
    [FooterFileName]      VARCHAR (256)    NULL,
    [LanguageID]          INT              NULL,
    [rowguid]             UNIQUEIDENTIFIER CONSTRAINT [DF_PredefinedMessage_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]         DATETIME         CONSTRAINT [DF_PredefinedMessage_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]        DATETIME         CONSTRAINT [DF_PredefinedMessage_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           VARCHAR (256)    NULL,
    [ModifiedBy]          VARCHAR (256)    NULL,
    CONSTRAINT [PK_PredefinedMessage] PRIMARY KEY CLUSTERED ([PredefinedMessageID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PredefinedMessage_AppBizName]
    ON [msg].[PredefinedMessage]([ApplicationID] ASC, [BusinessEntityID] ASC, [Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PredefinedMessage_rowguid]
    ON [msg].[PredefinedMessage]([rowguid] ASC);

