﻿CREATE TABLE [msg].[MessagePart] (
    [MessagePartID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID] INT              NULL,
    [BusinessID]    BIGINT           NULL,
    [KeyName]       VARCHAR (256)    NOT NULL,
    [Name]          VARCHAR (256)    NULL,
    [ContentTypeID] INT              NOT NULL,
    [Message]       VARCHAR (MAX)    NOT NULL,
    [LanguageID]    INT              NOT NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_MessagePart_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_MessagePart_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_MessagePart_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MessagePart] PRIMARY KEY CLUSTERED ([MessagePartID] ASC),
    CONSTRAINT [FK_MessagePart_ContentType] FOREIGN KEY ([ContentTypeID]) REFERENCES [msg].[ContentType] ([ContentTypeID]),
    CONSTRAINT [FK_MessagePart_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_MessagePart]
    ON [msg].[MessagePart]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_MessagePart_AppBizKeyLang]
    ON [msg].[MessagePart]([ApplicationID] ASC, [BusinessID] ASC, [KeyName] ASC, [LanguageID] ASC);

