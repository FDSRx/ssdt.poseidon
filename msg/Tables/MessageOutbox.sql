﻿CREATE TABLE [msg].[MessageOutbox] (
    [MessageOutboxID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [SenderID]        BIGINT           NOT NULL,
    [ServiceID]       INT              NOT NULL,
    [SourceKey]       VARCHAR (256)    NULL,
    [AdditionalData]  XML              NULL,
    [MessageTypeID]   INT              NOT NULL,
    [MessageData]     XML              NOT NULL,
    [FromAddress]     VARCHAR (256)    NULL,
    [ToAddress]       VARCHAR (MAX)    NULL,
    [MessageStatusID] INT              NOT NULL,
    [LanguageID]      INT              NOT NULL,
    [RetryCount]      INT              CONSTRAINT [DF_MessageOutbox_RetryCount] DEFAULT ((0)) NOT NULL,
    [DateExpires]     DATETIME         CONSTRAINT [DF_MessageOutbox_DateExpires] DEFAULT (CONVERT([datetime],'12/31/9999',(0))) NOT NULL,
    [DateDelay]       INT              NULL,
    [MinuteExpires]   INT              CONSTRAINT [DF_MessageOutbox_MinuteExpires] DEFAULT ((0)) NOT NULL,
    [MinuteDelay]     INT              CONSTRAINT [DF_MessageOutbox_MinuteDelay] DEFAULT ((0)) NOT NULL,
    [DateSent]        DATETIME         NULL,
    [ConfirmationKey] VARCHAR (500)    NULL,
    [TrackingNumber]  UNIQUEIDENTIFIER CONSTRAINT [DF_MessageOutbox_TrackingNumber] DEFAULT (newid()) NOT NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_MessageOutbox_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_MessageOutbox_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_MessageOutbox_DateModified] DEFAULT (getdate()) NOT NULL,
    [Note]            NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_MessageOutbox] PRIMARY KEY CLUSTERED ([MessageOutboxID] ASC),
    CONSTRAINT [FK_MessageOutbox_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_MessageOutbox_MessageStatus] FOREIGN KEY ([MessageStatusID]) REFERENCES [msg].[MessageStatus] ([MessageStatusID]),
    CONSTRAINT [FK_MessageOutbox_MessageType] FOREIGN KEY ([MessageTypeID]) REFERENCES [msg].[MessageType] ([MessageTypeID]),
    CONSTRAINT [FK_MessageOutbox_Sender] FOREIGN KEY ([SenderID]) REFERENCES [msg].[Sender] ([SenderID]),
    CONSTRAINT [FK_MessageOutbox_Service] FOREIGN KEY ([ServiceID]) REFERENCES [dbo].[Service] ([ServiceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageOutbox_rowguid]
    ON [msg].[MessageOutbox]([rowguid] ASC);


GO
CREATE NONCLUSTERED INDEX [UIX_MessageOutbox_TrackingNumber]
    ON [msg].[MessageOutbox]([TrackingNumber] ASC);

