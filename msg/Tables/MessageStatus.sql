﻿CREATE TABLE [msg].[MessageStatus] (
    [MessageStatusID] INT           IDENTITY (1, 1) NOT NULL,
    [Code]            NVARCHAR (10) NOT NULL,
    [Name]            NVARCHAR (50) NOT NULL,
    [DateModified]    DATETIME      CONSTRAINT [DF_MessageStatus_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MessageStatus] PRIMARY KEY CLUSTERED ([MessageStatusID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageStatus_Code]
    ON [msg].[MessageStatus]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageStatus_Name]
    ON [msg].[MessageStatus]([Name] ASC);

