﻿CREATE TABLE [msg].[SenderType] (
    [SenderTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_SenderType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_SenderType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_SenderType_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SenderType] PRIMARY KEY CLUSTERED ([SenderTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_SenderType_Code]
    ON [msg].[SenderType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_SenderType_Name]
    ON [msg].[SenderType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_SenderType_rowguid]
    ON [msg].[SenderType]([rowguid] ASC);

