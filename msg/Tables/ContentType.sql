﻿CREATE TABLE [msg].[ContentType] (
    [ContentTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]          VARCHAR (10)     NOT NULL,
    [Name]          VARCHAR (50)     NOT NULL,
    [Description]   VARCHAR (1000)   NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_ContentType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_ContentType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_ContentType_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ContentType] PRIMARY KEY CLUSTERED ([ContentTypeID] ASC)
);

