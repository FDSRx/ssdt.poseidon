﻿CREATE TABLE [msg].[MessageType] (
    [MessageTypeID]    INT              IDENTITY (1, 1) NOT NULL,
    [Code]             NVARCHAR (10)    NOT NULL,
    [Name]             NVARCHAR (50)    NOT NULL,
    [Description]      NVARCHAR (255)   NULL,
    [MessageXmlSchema] XML              NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_MessageType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_MessageType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_MessageType_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MessageType] PRIMARY KEY CLUSTERED ([MessageTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageType_Code]
    ON [msg].[MessageType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageType_Name]
    ON [msg].[MessageType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageType_rowguid]
    ON [msg].[MessageType]([rowguid] ASC);

