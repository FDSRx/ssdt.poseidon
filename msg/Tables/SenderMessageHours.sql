﻿CREATE TABLE [msg].[SenderMessageHours] (
    [SenderMessageHourID] INT              IDENTITY (1, 1) NOT NULL,
    [SenderID]            BIGINT           NOT NULL,
    [ServiceID]           INT              NULL,
    [MessageTypeID]       INT              NOT NULL,
    [HourTypeID]          INT              NOT NULL,
    [DayID]               INT              NOT NULL,
    [TimeStart]           TIME (7)         NULL,
    [TimeEnd]             TIME (7)         NULL,
    [TimeText]            VARCHAR (255)    NULL,
    [rowguid]             UNIQUEIDENTIFIER CONSTRAINT [DF_SenderMessageHours_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]         DATETIME         CONSTRAINT [DF_SenderMessageHours_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]        DATETIME         CONSTRAINT [DF_SenderMessageHours_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SenderMessageHours] PRIMARY KEY CLUSTERED ([SenderMessageHourID] ASC),
    CONSTRAINT [FK_SenderMessageHours_Days] FOREIGN KEY ([DayID]) REFERENCES [dbo].[Days] ([DayID]),
    CONSTRAINT [FK_SenderMessageHours_HourType] FOREIGN KEY ([HourTypeID]) REFERENCES [dbo].[HourType] ([HourTypeID]),
    CONSTRAINT [FK_SenderMessageHours_MessageType] FOREIGN KEY ([MessageTypeID]) REFERENCES [msg].[MessageType] ([MessageTypeID]),
    CONSTRAINT [FK_SenderMessageHours_Sender] FOREIGN KEY ([SenderID]) REFERENCES [msg].[Sender] ([SenderID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_SenderMessageHours_rowguid]
    ON [msg].[SenderMessageHours]([rowguid] ASC);

