﻿CREATE TABLE [msg].[Message] (
    [MessageID]    BIGINT           IDENTITY (1, 1) NOT NULL,
    [AuthorID]     BIGINT           NULL,
    [Subject]      VARCHAR (1000)   NULL,
    [Body]         VARCHAR (MAX)    NOT NULL,
    [IsImportant]  BIT              CONSTRAINT [DF_Message_IsImportant] DEFAULT ((0)) NOT NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Message_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Message_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Message_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED ([MessageID] ASC),
    CONSTRAINT [FK_Message_Person] FOREIGN KEY ([AuthorID]) REFERENCES [dbo].[Person] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Message_rowguid]
    ON [msg].[Message]([rowguid] ASC);

