﻿CREATE TABLE [msg].[SenderMessageVendor] (
    [SenderMessageVendorID] INT              IDENTITY (1, 1) NOT NULL,
    [SenderID]              BIGINT           NOT NULL,
    [MessageTypeID]         INT              NOT NULL,
    [MessageVendorID]       INT              NOT NULL,
    [SenderAddress]         VARCHAR (255)    NULL,
    [rowguid]               UNIQUEIDENTIFIER CONSTRAINT [DF_SenderMessageVendor_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]           DATETIME         CONSTRAINT [DF_SenderMessageVendor_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]          DATETIME         CONSTRAINT [DF_SenderMessageVendor_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SenderMessageVendor] PRIMARY KEY CLUSTERED ([SenderMessageVendorID] ASC),
    CONSTRAINT [FK_SenderMessageVendor_MessageType] FOREIGN KEY ([MessageTypeID]) REFERENCES [msg].[MessageType] ([MessageTypeID]),
    CONSTRAINT [FK_SenderMessageVendor_MessageVendor] FOREIGN KEY ([MessageVendorID]) REFERENCES [msg].[MessageVendor] ([MessageVendorID]),
    CONSTRAINT [FK_SenderMessageVendor_Sender] FOREIGN KEY ([SenderID]) REFERENCES [msg].[Sender] ([SenderID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_SenderMessageVendor_SenderMessageType]
    ON [msg].[SenderMessageVendor]([SenderID] ASC, [MessageTypeID] ASC);


GO
CREATE NONCLUSTERED INDEX [UIX_SenderMessageVendor_rowguid]
    ON [msg].[SenderMessageVendor]([rowguid] ASC);

