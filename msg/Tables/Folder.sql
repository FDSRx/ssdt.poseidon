﻿CREATE TABLE [msg].[Folder] (
    [FolderID]     INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (10)     NULL,
    [Name]         VARCHAR (255)    NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Folder_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Folder_DateModified] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Folder_DateModified_1] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED ([FolderID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Folder_Name]
    ON [msg].[Folder]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Folder_rowguid]
    ON [msg].[Folder]([rowguid] ASC);

