﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/10/2013
-- Description:	Send a message to person(s)
-- SELECT * FROM dbo.vwPerson WHERE LastName = 'TESTPERSON'
-- =============================================
CREATE PROCEDURE [msg].[spPersonMessage_Send]
	@FromID INT,
	@ToAddress VARCHAR(MAX), -- semi-colon delimited list of persons (similar to email)
	@ServiceID INT,
	@Subject VARCHAR(1000),
	@Body VARCHAR(MAX),
	@IsImportant BIT = 0,
	@IsRead BIT = 0,
	@IsConversation BIT = 0,
	@CanRespond BIT = 0,
	@ThreadKey VARCHAR(50) = NULL,
	@RoutingData XML = NULL,
	@MessageData XML = NULL,
	@ThreadData XML = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------
	-- Sanitize input
	--------------------------------------------------
	SET @IsImportant = ISNULL(@IsImportant, 0);
	SET @IsConversation = ISNULL(@IsConversation, 0);
	SET @CanRespond = ISNULL(@CanRespond, 0);
	SET @IsRead = ISNULL(@IsRead, 0);
	
	--------------------------------------------------
	-- Local variables
	--------------------------------------------------
	DECLARE
		@MessageID BIGINT = NULL,
		@FolderID_Inbox INT = (SELECT FolderID FROM msg.Folder WHERE CODE = 'INBOX'), -- get default folder
		@FolderID_Sent INT = (SELECT FolderID FROM msg.Folder WHERE CODE = 'SENT'), -- get default folder
		@ThreadDataRaw VARCHAR(MAX),
		@PersonMessageID BIGINT,
		@MessageTypeCode VARCHAR(25)

	--------------------------------------------------
	-- Temporary resources
	--------------------------------------------------
	DECLARE @TblPersonList AS TABLE (
		Idx INT IDENTITY(1,1),
		PersonID BIGINT
	)
	
	--------------------------------------------------
	-- Get person list
	--------------------------------------------------
	INSERT INTO @TblPersonList (
		PersonID
	)
	SELECT DISTINCT
		Value
	FROM dbo.fnSplit(@ToAddress, ';')
	WHERE Value IS NOT NULL
	
	--------------------------------------------------
	-- Create Message
	--------------------------------------------------
	EXEC msg.spMessage_Create
		@AuthorID = @FromID,
		@Subject = @Subject,
		@Body = @Body,
		@IsImportant = @IsImportant,
		@MessageID = @MessageID OUTPUT		
	
	--------------------------------------------------
	-- Create sent message
	--------------------------------------------------
	-- TODO: Determine if we need to store sent messages
	
	
	--------------------------------------------------
	-- If the message was sent via an EMessage request
	-- then we need to retrieve the routing data that was supplied
	-- and overwrite any thread data that was already set. 
	-- <Summary>
	-- Retrieves the EMessage route data and sets the
	-- needed thread information
	-- </Summary>
	-- <Remarks>
	-- We overwrite any supplied thread data with the EMessage data
	-- because the EMessage is supposed to be a well formed xml document
	-- with the properly supplied credentials for sending and receiving a
	-- message. If no emessage was supplied then we proceed as normal.
	-- <Remarks>
	--------------------------------------------------
	-- Use EMessage data before we use routing data.
	SET @RoutingData = 
		CASE
			WHEN @MessageData IS NOT NULL THEN ( SELECT e.query('.') FROM @MessageData.nodes('(EMessage/Routing)') AS EM(e) )
			ELSE @RoutingData
		END;
	
	-- EMessage routing data or supplied routing data will always truncate parameter routing data.
	IF @RoutingData IS NOT NULL AND dbo.fnIsNullOrWhiteSpace(CONVERT(VARCHAR(MAX), @RoutingData)) = 0
	BEGIN
		DECLARE @r XML = @RoutingData;
		
		SET @IsConversation = @r.value('data(Routing/DestinationRoute/Thread/IsConversation)[1]', 'BIT');
		SET @CanRespond = @r.value('data(Routing/DestinationRoute/Thread/CanRespond)[1]', 'BIT');
		SET @ThreadKey = @r.value('data(Routing/DestinationRoute/Thread/ThreadKey)[1]', 'VARCHAR(50)');
		SET @MessageTypeCode = @r.value('data(Routing/SourceRoute/MessageTypeCode)[1]', 'VARCHAR(25)');
	END
	
	-- If we do not have a thread key or it is not a unique identifier, 
	-- then we cannot respond and it is technically not considered
	-- a conversation at this point.
	IF dbo.fnIsNullOrEmpty(@ThreadKey) = 1 OR dbo.fnIsUniqueIdentifier(@ThreadKey) = 0
	BEGIN
		SET @IsConversation = 0;
		SET @CanRespond = 0;
		SET @ThreadKey = NULL;
	END
	
	
	--------------------------------------------------
	-- Beginning building thread data xml
	-- <Summary>
	-- Begins the process of building thread data based on the 
	-- messages that were delivered.
	-- </Summary>
	--------------------------------------------------
	SET @ThreadDataRaw = ISNULL(@ThreadDataRaw, '') +
		'<ThreadData>' +
			dbo.fnXmlWriteElementString('MessageID', @MessageID) +
			'<Threads>';
	
	--------------------------------------------------
	-- Send message to users
	--------------------------------------------------
	DECLARE
		@CurIndex INT = 1,
		@LastIndex INT = (SELECT MAX(Idx) FROM @TblPersonList)
	
	WHILE @CurIndex <= @LastIndex
	BEGIN
	
		DECLARE @PersonID INT = (SELECT PersonID FROM @TblPersonList WHERE Idx = @CurIndex)
	
		EXEC dbo.spPersonMessage_Create
			@PersonID = @PersonID,
			@ServiceID = @ServiceID,
			@FolderID = @FolderID_Inbox,
			@MessageID = @MessageID,
			@IsRead = @IsRead,
			@IsConversation = @IsConversation,
			@CanRespond = @CanRespond,
			@RoutingData = @RoutingData,
			@ThreadKey = @ThreadKey OUTPUT,
			@PersonMessageID = @PersonMessageID OUTPUT			
		
		-- Append thread data to the thread data xml structure.
		SET @ThreadDataRaw = ISNULL(@ThreadDataRaw, '') +
			'<Thread>' +
				dbo.fnXmlWriteElementString('ThreadKey', @ThreadKey) +
				dbo.fnXmlWriteElementString('PersonID', @PersonID) +
				dbo.fnXmlWriteElementString('PersonMessageID', @PersonMessageID) +
			'</Thread>';
				
				
		-- Go to next person
		SET @CurIndex = @CurIndex + 1;
	END
	
	-- Write the end tags to our thread data records
	SET @ThreadDataRaw = ISNULL(@ThreadDataRaw, '') +
			'</Threads>' +
		'</ThreadData>'
		
	--------------------------------------------------
	-- Convert thread data into xml
	-- <Summary>
	-- Trys to convert the raw xml string into xml
	-- </Summary>
	--------------------------------------------------	
	BEGIN TRY
		SET @ThreadData = CONVERT(XML, @ThreadDataRaw)
	END TRY
	BEGIN CATCH
		-- Do nothing
	END CATCH
	
	
	--------------------------------------------------
	-- Trigger user specified events
	-- <Summary>
	-- Triggers any events that are required after the message
	-- has been sent.  This can be both internal events or events
	-- that are required by external applications
	-- </Summary>
	-- <Remarks>
	-- Calling external methods is not ideal.
	-- </Remarks>
	--------------------------------------------------
	BEGIN TRY
		-- If the sender's message type is a ticket conversation message then we need to fire
		-- the on received event for athena's secure messaging notification.
		IF @MessageTypeCode = 'TCKTCNVSN'
		BEGIN
			EXEC extern.spAthena_mPC_Event_User_SecureMessage_OnReceived
				@PersonID = @PersonID
		END
			
	END TRY
	BEGIN CATCH
	
		EXEC spLogError
		
	END CATCH
	

	
	
	
END
