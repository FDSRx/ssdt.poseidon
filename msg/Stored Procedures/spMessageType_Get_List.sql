﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/14/2013
-- Description:	Returns a list of all message types.
-- SAMPLE CALL: msg.spMessageType_Get_List
-- =============================================
CREATE PROCEDURE msg.spMessageType_Get_List
	@MessageTypeID INT = NULL,
	@Code VARCHAR(25) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		MessageTypeID,
		Code,
		Name,
		DateCreated,
		DateModified
	--SELECT *
	FROM msg.MessageType
	WHERE MessageTypeID = ISNULL(@MessageTypeID, MessageTypeID)
	
END
