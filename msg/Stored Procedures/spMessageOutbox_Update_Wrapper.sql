﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/8/2103
-- Description:	Update message outbox item
-- =============================================
CREATE PROCEDURE [msg].[spMessageOutbox_Update_Wrapper]
	@MessageOutboxID BIGINT,
	@MessageStatusCode VARCHAR(25),
	@IsRetry BIT = NULL,
	@ConfirmationKey VARCHAR(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;    

	-----------------------------------------
	-- Local variables
	-----------------------------------------
	DECLARE @MessageStatusID INT
	
	SET @MessageStatusID = (SELECT MessageStatusID FROM msg.MessageStatus (NOLOCK) WHERE Code = @MessageStatusCode);
	
	EXEC msg.spMessageOutbox_Update
		@MessageOutboxID = @MessageOutboxID,
		@MessageStatusID = @MessageStatusID,
		@IsRetry = @IsRetry,
		@ConfirmationKey = @ConfirmationKey
    
    
END
