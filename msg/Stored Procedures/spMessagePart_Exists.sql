﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/30/2014
-- Description:	Indicates whether a message part item exists.
-- SAMPLE CALL: 
/*
DECLARE @Exists BIT, @MessagePartID BIGINT
EXEC msg.spMessagePart_Exists
	@KeyName = 'test',
	@MessagePartID = @MessagePartID OUTPUT,
	@Exists = @Exists OUTPUT
SELECT @MessagePartID AS MessagePartID, @Exists AS Found
*/
-- =============================================
CREATE PROCEDURE [msg].[spMessagePart_Exists]
	@KeyName VARCHAR(256),
	@LanguageID INT = NULL,
	@MessagePartID BIGINT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------
	SET @MessagePartID = NULL;
	SET @Exists = 0;

	---------------------------------------------------------------
	-- Set variables
	---------------------------------------------------------------
	SET @LanguageID = ISNULL(@LanguageID, dbo.fnGetLanguageDefaultID());
	
	---------------------------------------------------------------
	-- Determine if message part exists
	---------------------------------------------------------------
	SET @MessagePartID = (
		SELECT TOP 1
			MessagePartID
		FROM msg.MessagePart
		WHERE KeyName = @KeyName
			AND LanguageID = @LanguageID
	);
	
	-- Set existence flag
	SET @Exists = CASE WHEN @MessagePartID IS NOT NULL THEN 1 ELSE 0 END;
	
END
