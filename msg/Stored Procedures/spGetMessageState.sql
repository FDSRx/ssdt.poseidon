﻿
/*
 =============================================
 Author:		Alex Korolev
 Create date:	01/16/2015
 Description:	Returns Message Status
 SAMPLE CALL:   msg.spGetMessageState '85068842-F480-E211-9565-0015174A63BB'
 Modifications:
 =============================================
*/
CREATE PROCEDURE [msg].[spGetMessageState]
	@TrackingNumber uniqueidentifier 	
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT TOP 1
		ob.[MessageOutboxID]		,
		ob.[SenderID]				,
		ob.[ServiceID]				,
		ob.[SourceKey]				,
		ob.[AdditionalData]			,
		ob.[MessageTypeID]			,
		ob.[MessageData]			,
		ob.[FromAddress]			,
		ob.[ToAddress]				,
		ob.[MessageStatusID]		,
		ob.[LanguageID]				,
		ob.[RetryCount]				,
		ob.[DateExpires]			,
		ob.[DateDelay]				,
		ob.[MinuteExpires]			,
		ob.[MinuteDelay]			,
		ob.[DateSent]				,
		ob.[ConfirmationKey]		,
		ob.[TrackingNumber]			,
		ob.[rowguid]				,
		ob.[DateCreated]			,
		ob.[DateModified]			,
		ms.[Code]	as StausCode	,
		ms.[Name]	as StausDesc	
	FROM msg.MessageOutbox ob
	INNER JOIN msg.MessageStatus ms
	ON ob.MessageStatusID = ms.MessageStatusID  
		
	WHERE ob.[TrackingNumber] = @TrackingNumber
		 OR ob.[ConfirmationKey] = @TrackingNumber	
	
	

END

