﻿

CREATE PROCEDURE [msg].[spSrvBroker_EMessage_DequeueBatch]
	
AS
/*
==================================================================================
Author:		 Alex Korolev
Create date: 01/20/2015
Description: Dequeue message in batch mode and returns count messages processed
Example:	 spSrvBroker_EMessage_DequeueBatch	
==================================================================================
*/
BEGIN
	DECLARE @MessageCnt INT = 0;
SET NOCOUNT ON;
WHILE exists (SELECT 1 FROM dbo.EMessageQueue)
	BEGIN
		EXEC msg.spServiceBroker_EMessage_DequeueV2;
		SET @MessageCnt = @MessageCnt + 1; 
	END	
	SELECT @MessageCnt as MesageDequeueCount
END
