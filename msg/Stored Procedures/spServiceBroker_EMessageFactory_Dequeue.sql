﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/12/2014
-- Description:	Initiator process of EMessages.
-- =============================================
CREATE PROCEDURE [msg].[spServiceBroker_EMessageFactory_Dequeue]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------
	-- Local variables
	---------------------------------------------
	DECLARE 
		@MessageTypeName NVARCHAR(255),
		@ConversationKey UNIQUEIDENTIFIER,
		@OutboxMessageXml XML,
		@OutboxMessageBinary VARBINARY(MAX)			
			
	---------------------------------------------
	-- Pop item from queue
	---------------------------------------------
	;WAITFOR
	(
		RECEIVE TOP (1) 
			@OutboxMessageBinary = message_body, 
			@ConversationKey = conversation_handle, 
			@MessageTypeName = message_type_name, 
			@OutboxMessageXml = CASE message_type_name WHEN 'X' 
								  THEN CAST(message_body AS NVARCHAR(MAX)) 
								  ELSE message_body 
								END
		--SELECT *
		FROM EMessageFactoryQueue	
	), TIMEOUT 2000;
	
	-- If i had a record then end the conversation
	IF @ConversationKey IS NOT NULL AND dbo.fnReverse(ISNULL(@MessageTypeName, ''), '/') = 'EndDialog'
	BEGIN
		END CONVERSATION @ConversationKey;
	END
	ELSE IF @ConversationKey IS NOT NULL AND dbo.fnReverse(ISNULL(@MessageTypeName, ''), '/') = 'Error'
	BEGIN
		END CONVERSATION @ConversationKey;
	END
	ELSE IF @ConversationKey IS NOT NULL AND @MessageTypeName = '//Poseidon/EMessage/ItemAdded'
	BEGIN		
		END CONVERSATION @ConversationKey;
	END
	
END
