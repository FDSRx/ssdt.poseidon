﻿
/* 
==============================================================
 Author:		Daniel Hughes
 Create date: 2/8/2013
 Description:	Queue message
 Modifications:
 01/20/2015 : Akorolev added DoNotDeliverBefore param NULL
 ==============================================================
*/
CREATE PROCEDURE [msg].[spMessageQueue_Enqueue]
	@MessageOutboxID INT = NULL,
	@MessageData XML = NULL,
	@BrokerMessageData XML = NULL, 
	@DoNotDeliverBefore DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	BEGIN TRY
	
		-- Only requeue uknown outbox items or an item that is not already in the queue
		IF (SELECT COUNT(*) FROM msg.MessageQueue WHERE MessageOutboxID = @MessageOutboxID) = 0 
		BEGIN
		
			INSERT INTO msg.MessageQueue (
				MessageOutboxID,
				MessageData,
				BrokerMessageData,
				DoNotDeliverBefore
			)
			SELECT
				@MessageOutboxID,
				@MessageData,
				@BrokerMessageData,
				@DoNotDeliverBefore
				
		END
	END TRY
		
	BEGIN CATCH
	
		EXECUTE spLogError;
		
	END CATCH
		
END
