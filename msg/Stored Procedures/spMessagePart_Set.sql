﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/30/2014
-- Description:	Creates/Updates a message part.
-- SELECT * FROM msg.MessagePart
-- =============================================
CREATE PROCEDURE [msg].[spMessagePart_Set]
	@MessagePartID BIGINT = NULL OUTPUT,
	@KeyName VARCHAR(256) = NULL,
	@Name VARCHAR(256) = NULL,
	@ContentTypeID INT = NULL,
	@Message VARCHAR(MAX),
	@LanguageID INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------
	DECLARE 
		@Exists BIT = 0
	
	---------------------------------------------------------------
	-- Set variables
	---------------------------------------------------------------
	SET @LanguageID = ISNULL(@LanguageID, dbo.fnGetLanguageDefaultID());
	SET @ContentTypeID = ISNULL(@ContentTypeID, msg.fnGetContentTypeDefaultID());
	
	---------------------------------------------------------------
	-- Determine if message part exists.
	-- <Summary>
	-- If a MessagePartID was not supplied then we need to
	-- determine if the key/language pair already exists as a message
	-- part.  The response will provide us with a MessagePartID
	-- to perform an update
	-- </Summary>
	---------------------------------------------------------------
	IF @MessagePartID IS NULL
	BEGIN
		EXEC msg.spMessagePart_Exists
			@KeyName = @keyName,
			@LanguageID = @LanguageID,
			@MessagePartID = @MessagePartID OUTPUT,
			@Exists = @Exists OUTPUT
	END

	---------------------------------------------------------------
	-- Create/Update message part record.
	-- <Summary>
	-- If a MessagePartID was found then we will update; otherwise
	-- we will create a new message part.
	-- </Summary>
	---------------------------------------------------------------
	-- If no message part was found then let's create one.
	IF @MessagePartID IS NULL
	BEGIN
		EXEC msg.spMessagePart_Create
			@KeyName = @KeyName,
			@Name = @Name,
			@ContentTypeID = @ContentTypeID,
			@Message = @Message,
			@LanguageID = @LanguageID
	END
	ELSE
	-- If a message part was found then let's go ahead and update the part.
	BEGIN
	
		EXEC msg.spMessagePart_Update
			@MessagePartID = @MessagePartID,
			@Name = @Name,
			@ContentTypeID = @ContentTypeID,
			@Message = @Message
	END
	
	
	
	
	
	
	
	
	
END
