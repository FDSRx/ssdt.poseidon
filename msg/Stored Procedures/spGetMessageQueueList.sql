﻿


CREATE PROCEDURE [msg].[spGetMessageQueueList]

AS
/*
 =======================================================================
 Author:		Alex Korolev
 Create date:	01/20/2015
 Description:	Returns Message list from the queue avail for delivery
 SAMPLE CALL:   msg.spGetMessageQueueList 
 Modifications:
 =======================================================================
*/
BEGIN
	
	SET NOCOUNT ON;
	
SELECT
	 mq.MessageQueueId	,
	 mq.MessageData		,
	 mq.RowGuid			, 
	 mq.DateCreated   
FROM msg.MessageQueue mq
WHERE	
	(DoNotDeliverBefore IS NULL 
	OR  DoNotDeliverBefore < GETDATE())
END
