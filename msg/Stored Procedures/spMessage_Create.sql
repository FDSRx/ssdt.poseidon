﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/10/2013
-- Description:	Create an internal message
-- SELECT * FROM dbo.Person
-- SELECT * FROM dbo.PersonType
-- =============================================
CREATE PROCEDURE [msg].[spMessage_Create]
	@AuthorID INT,
	@Subject VARCHAR(1000) = NULL,
	@Body VARCHAR(MAX),
	@IsImportant BIT = 0,
	@MessageID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------
	-- Set defaults
	------------------------------------------------
	SET @IsImportant = ISNULL(@IsImportant, 0);
	SET @AuthorID = ISNULL(@AuthorID, dbo.fnGetConfigValue('UnknownAuthorID'));
	
	------------------------------------------------
	-- Create message
	------------------------------------------------
	INSERT INTO msg.Message (
		AuthorID,
		Subject,
		Body,
		IsImportant
	)
	SELECT
		@AuthorID,
		@Subject,
		@Body,
		@IsImportant
	
	-- Retrieve ID	
	SET @MessageID = SCOPE_IDENTITY();
	
END
