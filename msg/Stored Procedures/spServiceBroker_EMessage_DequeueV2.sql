﻿/*
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/8/2013
-- Description:	Dequeue message from messenger queue and add to outbox
-- SAMPLE CALL: msg.spServiceBroker_EMessage_Dequeue
-- SELECT * FROM msg.MessageOutbox ORDER BY MessageOutboxID DESC
-- SELECT TOP 5 * FROM msg.MessageOutbox ORDER BY MessageOutboxID DESC
-- SELECT * FROM msg.MessageErrorLog ORDER BY MessageErrorLogID DESC
-- SELECT * FROM msg.MessageQueue
-- DELETE FROM msg.MessageErrorLog
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- =============================================
Modifications:
  01/20/2015 Changed Queue Destination
*/
CREATE PROCEDURE [msg].[spServiceBroker_EMessage_DequeueV2]
	@MessageOutboxID BIGINT = NULL OUTPUT,
	@ErrorLogID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--------------------------------------------
	-- Local variables
	---------------------------------------------
	DECLARE 
		@MessageTypeName NVARCHAR(255),
		@ConversationKey UNIQUEIDENTIFIER,
		@OutboxMessageXml XML,
		@OutboxMessageBinary VARBINARY(MAX),
		@Response XML
		
	    	
    ---------------------------------------------
	-- Pop item from queue
	---------------------------------------------
	;WAITFOR
	(
		RECEIVE TOP (1) 
			@OutboxMessageBinary = message_body, 
			@ConversationKey = conversation_handle, 
			@MessageTypeName = message_type_name, 
			@OutboxMessageXml = CASE message_type_name WHEN 'X' 
								  THEN CAST(message_body AS NVARCHAR(MAX)) 
								  ELSE message_body 
								END		
		FROM EMessageQueue		
	), TIMEOUT 2000;

	
	-- If the item is a real message then let's put in our inbox
	--SELECT @MessageTypeName, @ConversationKey, @OutboxMessageXml, @OutboxMessageBinary

	IF @ConversationKey IS NOT NULL AND dbo.fnReverse(ISNULL(@MessageTypeName, ''), '/') = 'EndDialog'
	BEGIN
		END CONVERSATION @ConversationKey;
	END
	ELSE IF @ConversationKey IS NOT NULL AND dbo.fnReverse(ISNULL(@MessageTypeName, ''), '/') = 'Error'
	BEGIN
		END CONVERSATION @ConversationKey;
	END
	ELSE IF @ConversationKey IS NOT NULL AND @MessageTypeName = '//Poseidon/EMessage/AddItem'
	BEGIN
			
		BEGIN TRY
		
			DECLARE
				@x XML = @OutboxMessageXml,
				@msg XML,
				@SenderID INT,
				@SenderKey VARCHAR(255),
				@SourceKey VARCHAR(255),
				@ServiceCode VARCHAR(25),
				@ServiceID INT,
				@AdditionalData XML,
				@MessageTypeCode VARCHAR(25),
				@MessageTypeID INT,
				@MessageData XML,
				@FromAddress VARCHAR(255),
				@ToAddress VARCHAR(MAX),
				@MessageStatusID INT,
				@LanguageCode VARCHAR(10),
				@LanguageID INT,
				@DateCreated VARCHAR(255),
				@DateExpires VARCHAR(255),
				@DateDelay VARCHAR(255),
				@MinuteDelay INT,
				@MinuteExpires INT,
				@TrackingNumber UNIQUEIDENTIFIER = @ConversationKey,
				@ErrorMessage NVARCHAR(4000)
			
			-- Outbox ID
			SET @MessageOutboxID = @x.value('(/OutboxMessage/MessageOutboxID)[1]', 'INT')
			
			---------------------------------------------------
			-- Check for retry
			---------------------------------------------------
			IF ISNULL(@MessageOutboxID, 0) <> 0
			BEGIN
				-- I am retrying something.  Add retry logic here
				GOTO EOF;
			END
			ELSE
			BEGIN
				-- Set to null to be more clear that we did not recieve an outbox item
				SET @MessageOutboxID = NULL;
			END
			
			---------------------------------------------------
			-- Handle new message
			---------------------------------------------------
			
			-- Get the EMessage
			SET @msg = @x.value('(/OutboxMessage/Envelope)[1]', 'VARCHAR(MAX)')
			SET @MessageData = @msg;

			-- EMessage high Level Data
			SET @SourceKey = @msg.value('(/EMessage/SourceKey)[1]', 'VARCHAR(255)')
			SET @SenderKey = @msg.value('(/EMessage/Sender/SenderKey)[1]', 'VARCHAR(255)')
			SET @LanguageCode = @msg.value('(/EMessage/LanguageCode)[1]', 'VARCHAR(255)')		
			SET @ServiceCode = @msg.value('(/EMessage/Sender/ServiceCode)[1]', 'VARCHAR(255)')		
			SET @MessageTypeCode = @msg.value('(/EMessage/MessageTypeCode)[1]', 'VARCHAR(255)')
			
			-- EMessage lower Level Data
			SET @FromAddress = @msg.value('(/EMessage/Sender/Address)[1]', 'VARCHAR(255)')
			SET @ToAddress = @msg.value('(/EMessage/Recipient/Address)[1]', 'VARCHAR(255)')
			SET @DateExpires = @msg.value('(/EMessage/DateExpires)[1]', 'VARCHAR(255)')
			SET @DateCreated = @msg.value('(/EMessage/DateCreated)[1]', 'VARCHAR(255)')
			SET @DateDelay =  @msg.value('(/EMessage/DateDelay)[1]', 'VARCHAR(255)')
			SET @MinuteDelay = @msg.value('(/EMessage/MinuteDelay)[1]', 'INT')
			SET @MinuteExpires = @msg.value('(/EMessage/MinuteExpires)[1]', 'INT')
			SET @AdditionalData = 
				CASE
					WHEN @msg IS NOT NULL THEN ( SELECT e.query('.') FROM @msg.nodes('(EMessage/UserDefinedData)') AS EM(e) )
					ELSE @AdditionalData
				END;

			
			---------------------------------------------------
			-- Look up codes
			---------------------------------------------------
			SET @ServiceID = dbo.fnGetServiceID(@ServiceCode);
			SET @LanguageID = ISNULL(dbo.fnGetLanguageID(@LanguageCode), dbo.fnGetLanguageDefaultID());
			SET @MessageStatusID = (SELECT MessageStatusID FROM msg.MessageStatus WHERE Code = 'PNDNG');
			SET @MessageTypeID = (SELECT MessageTypeID FROM msg.MessageType WHERE Code = @MessageTypeCode);
			
			---------------------------------------------------
			-- Clean data
			---------------------------------------------------
			SELECT @DateCreated = CASE WHEN ISNULL(@DateCreated, '') = '' THEN NULL ELSE @DateCreated END;
			SELECT @DateExpires = CASE WHEN ISNULL(@DateExpires, '') = '' THEN NULL ELSE @DateExpires END;
			SELECT @DateDelay = CASE WHEN ISNULL(@DateExpires, '') = '' THEN NULL ELSE @DateDelay END;
			
			---------------------------------------------------
			-- Validate sender
			---------------------------------------------------
			
			SELECT @SenderID = SenderID
			FROM msg.Sender 
			WHERE CONVERT(VARCHAR(255), SenderKey) = @SenderKey 
				AND ContractDateStart <= GETDATE() 
				AND ContractDateEnd > GETDATE()
				
			IF @SenderID IS NULL
			BEGIN
				SET @ErrorMessage = 'Unable to create an outbox item.  The SENDER KEY is not valid.  The key does not exist '
					+ 'or the sender contract has expired. Sender Key: ' + ISNULL(@SenderKey, '<Empty Key>');
				
				RAISERROR (@ErrorMessage, -- Message text.
					   16, -- Severity.
					   1 -- State.
					   );
			END
			


			---------------------------------------------------
			-- Create outbox item
			---------------------------------------------------
			EXEC msg.spMessageOutbox_Create
				@SenderID = @SenderID,
				@ServiceID = @ServiceID,				
				@SourceKey = @SourceKey,
				@AdditionalData = @AdditionalData,
				@MessageTypeID = @MessageTypeID,
				@MessageData = @MessageData,
				@FromAddress = @FromAddress,
				@ToAddress = @ToAddress,
				@MessageStatusID = @MessageStatusID,
				@DateExpires = @DateExpires,
				@DateDelay = @DateDelay,
				@MinuteExpires = @MinuteExpires,
				@MinuteDelay = @MinuteDelay,
				@MessageOutboxID = @MessageOutboxID OUTPUT,
				@TrackingNumber = @TrackingNumber OUTPUT	
						
			--SET @DoNotSendBefore  = msg.fnGetNextSendDateTime (@SenderKey,@MessageTypeID)		
			
			-- Drop Message Into Queue Table:
			EXEC msg.spMessageQueue_Enqueue 
				@MessageOutboxID = @MessageOutboxID,
				@MessageData = @MessageData,
				@BrokerMessageData = @OutboxMessageXml,
				@DoNotDeliverBefore = NULL -- @DoNotSendBefore			
			
		
		END TRY
		BEGIN CATCH
		
			DECLARE @MessageErrorLogID INT = NULL;
			
			-- record message error
			DECLARE
				@Message NVARCHAR(MAX) = CONVERT(NVARCHAR(MAX), @MessageData)           
	            
			EXECUTE msg.spLogMessageError
				@MessageOutboxID = @MessageOutboxID,
				@Message = @Message,
				@MessageErrorLogID = @MessageErrorLogID OUTPUT
	        
			-- Assign the message error log to the standard error log variable    
			SET @ErrorLogID = @MessageErrorLogID
	        
	        
			-- record normal error    
			EXECUTE spLogError;
			
			
			
		END CATCH;
		  
		  

		EOF:
		
		-- Set and send response back to the initiator
		SET @Response = 'EMessage item was added successfully to the messaging queue.';
		
		SEND ON CONVERSATION @ConversationKey
		MESSAGE TYPE [//Poseidon/EMessage/ItemAdded] (@Response);
		
		END CONVERSATION @ConversationKey;

		
	
	END
	
END
