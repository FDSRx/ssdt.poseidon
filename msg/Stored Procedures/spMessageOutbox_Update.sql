﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/8/2103
-- Description:	Update message outbox item
-- =============================================
CREATE PROCEDURE [msg].[spMessageOutbox_Update]
	@MessageOutboxID BIGINT,
	@MessageStatusID INT,
	@IsRetry BIT = NULL,
	@ConfirmationKey VARCHAR(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;    

	UPDATE box
	SET
		box.MessageStatusID = CASE WHEN @MessageStatusID IS NULL THEN box.MessageStatusID ELSE @MessageStatusID END,
		box.RetryCount = CASE WHEN ISNULL(@IsRetry, 0) = 1 THEN box.RetryCount + 1 ELSE box.RetryCount END,
		box.ConfirmationKey = @ConfirmationKey,
		box.DateSent = GETDATE(),
		box.DateModified = GETDATE()
	--SELECT *
	FROM msg.MessageOutbox box
	WHERE box.MessageOutboxID = @MessageOutboxID


    
    
END
