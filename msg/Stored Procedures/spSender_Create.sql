﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/12/2103
-- Description:	Creates a new sender

-- SELECT * FROM msg.Sender
-- =============================================
CREATE PROCEDURE [msg].[spSender_Create]
	@SenderID BIGINT = NULL OUTPUT,
	@SenderTypeID INT,
	@Name VARCHAR(255) = NULL,
	@Demographics XML = NULL,
	@ContractDateStart DATETIME = NULL,
	@ContractDateEnd DATETIME = NULL,
	@SenderKey VARCHAR(255) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------
	-- Sanitize input.
	---------------------------------------------
	SET @SenderKey = NULL
	
	
	-----------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------
	SELECT @ContractDateStart = CASE WHEN @ContractDateStart IS NULL THEN '1/1/1900' ELSE @ContractDateStart END;
	SELECT @ContractDateEnd = CASE WHEN @ContractDateEnd IS NULL THEN '12/31/9999' ELSE @ContractDateEnd END;	

	-----------------------------------------------------------------------
	-- Create a new Sender BusinessEnittyID.
	-- <Summary>
	-- Creates a new SenderID from the business entity create process.
	-- </Summary>
	-----------------------------------------------------------------------
	DECLARE @BusinessEntityTypeID INT = dbo.fnGetBusinessEntityTypeID('SNDR');
		
	IF @SenderID IS NULL
	BEGIN
	
		EXEC dbo.spBusinessEntity_Create
			@BusinessEntityTypeID = @BusinessEntityTypeID,
			@BusinessEntityID = @SenderID OUTPUT
	
	END

	-----------------------------------------------------------------------
	-- Temporary resources.
	-----------------------------------------------------------------------
	DECLARE @tblOutput AS TABLE (
		Idx INT IDENTITY(1,1),
		SenderID BIGINT,
		SenderKey UNIQUEIDENTIFIER
	);
		
		
	-----------------------------------------------------------------------
	-- Create Sender.
	-----------------------------------------------------------------------
	INSERT INTO msg.Sender (
		SenderID,
		SenderTypeID,
		Name,
		Demographics,
		ContractDateStart,
		ContractDateEnd
	)
	OUTPUT inserted.SenderID, inserted.SenderKey INTO @tblOutput(SenderID, SenderKey)
	SELECT
		@SenderID,
		@SenderTypeID,
		@Name,	
		@Demographics,
		@ContractDateStart,
		@ContractDateEnd
	
	-- Retrieve identities.
	SELECT TOP 1
		@SenderID = SenderID,
		@SenderKey = SenderKey
	FROM @tblOutput
	
	
END
