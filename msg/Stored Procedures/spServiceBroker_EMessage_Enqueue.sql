﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/9/2013
-- Description:	Adds EMessages to the queue
-- =============================================
CREATE PROCEDURE [msg].[spServiceBroker_EMessage_Enqueue]
	@MessageOutboxID BIGINT = NULL,
	@MessageData XML = NULL,
	@TrackingNumber VARCHAR(255) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------
	-- Local variables
	---------------------------------------
	DECLARE
		@DialogHandle UniqueIdentifier,
		@OutboxMessage XML


	BEGIN TRY
	
		---------------------------------------
		-- Build queue message
		---------------------------------------	
		SET @OutboxMessage = N'<?xml version="1.0"?>
			<OutboxMessage>
			<MessageOutboxID>' + ISNULL(CONVERT(VARCHAR, @MessageOutboxID), '') + '</MessageOutboxID>' +
			'<Envelope><![CDATA[' + ISNULL(CONVERT(VARCHAR(MAX), @MessageData), '') + ']]></Envelope>
			</OutboxMessage>  
			'

		-- Send conversation to EMessage queue for processing.
		BEGIN DIALOG CONVERSATION @DialogHandle
		FROM SERVICE 
			[//Poseidon/EMessageFactoryService]
		TO SERVICE
			'//Poseidon/EMessageService'
		ON CONTRACT
			[//Poseidon/EMessage/AddItemContract];

		SEND ON CONVERSATION @DialogHandle
		MESSAGE TYPE [//Poseidon/EMessage/AddItem] (@OutboxMessage);
		--END CONVERSATION @DialogHandle; -- Do not end the conversation.  Let the process that picks up the message end the conversation.
		
		--Set tracking number
		SET @TrackingNumber = @DialogHandle;
	  
	END TRY
	BEGIN CATCH
            
		EXECUTE msg.spLogMessageError
            @MessageOutboxID = @MessageOutboxID,
            @Message = @MessageData
            
		EXECUTE spLogError;
		
	END CATCH
END
