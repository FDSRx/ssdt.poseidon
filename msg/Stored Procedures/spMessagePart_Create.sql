﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/30/2014
-- Description:	Creates a new message part.
-- =============================================
CREATE PROCEDURE [msg].[spMessagePart_Create]
	@KeyName VARCHAR(256),
	@Name VARCHAR(256) = NULL,
	@ContentTypeID INT = NULL,
	@Message VARCHAR(MAX),
	@LanguageID INT = NULL,
	@MessagePartID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------
	SET @MessagePartID = NULL;

	---------------------------------------------------------------
	-- Set variables
	---------------------------------------------------------------
	SET @LanguageID = ISNULL(@LanguageID, dbo.fnGetLanguageDefaultID());
	SET @ContentTypeID = ISNULL(@ContentTypeID, msg.fnGetContentTypeDefaultID());
	
	---------------------------------------------------------------
	-- Create new message part.
	---------------------------------------------------------------	
	INSERT INTO msg.MessagePart (
		KeyName,
		Name,
		ContentTypeID,
		Message,
		LanguageID
	)
	SELECT
		@KeyName,
		@Name,
		@ContentTypeID,
		@Message,
		@LanguageID
	
	-- Obtain record identity id.
	SET @MessagePartID = SCOPE_IDENTITY();
	
END
