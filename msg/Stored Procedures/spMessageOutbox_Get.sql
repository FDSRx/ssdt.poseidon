﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/7/2013
-- Description:	Create an outbox message
-- SAMPLE CALL: msg.spMessageOutbox_Get 4
-- =============================================
CREATE PROCEDURE [msg].[spMessageOutbox_Get]
	@MessageOutboxID BIGINT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT TOP 1
		box.MessageOutboxID,
		sdr.SenderID,
		sdr.SenderKey,
		srv.ServiceID,
		srv.Code AS ServiceCode,
		srv.Name AS ServiceName,
		vdr.MessageVendorID AS MessageVendorID,
		vdr.Code AS MessageVendorCode,
		vdr.Name AS MessageVendorName,
		box.SourceKey,
		box.AdditionalData,
		typ.MessageTypeID,
		typ.Code AS MessageTypeCode,
		typ.Name AS MessageTypeName,
		box.MessageData,
		CASE -- If no sender address was supplied then use the known sender address of the sender
			WHEN LTRIM(RTRIM(ISNULL(box.FromAddress, ''))) <> '' THEN box.FromAddress
			ELSE sdrvdr.SenderAddress
		END AS FromAddress,
		box.ToAddress,
		stat.MessageStatusID,
		stat.Code AS MessageStatusCode,
		stat.Name AS MessageStatusName,
		lang.LanguageID,
		lang.Code AS LanguageCode,
		lang.Name AS LanguageName,
		box.RetryCount,
		box.DateExpires,
		box.DateSent,
		box.TrackingNumber	
	--SELECT *
	FROM msg.MessageOutbox box
		LEFT JOIN msg.Sender sdr
			ON box.SenderID = sdr.SenderID
		LEFT JOIN msg.SenderMessageVendor sdrvdr
			ON sdr.SenderID = sdrvdr.SenderID
				AND box.MessageTypeID = sdrvdr.MessageTypeID
		LEFT JOIN msg.MessageVendor vdr
			ON sdrvdr.MessageVendorID = vdr.MessageVendorID
		JOIN dbo.Service srv
			ON box.ServiceID = srv.ServiceID
		JOIN msg.MessageType typ
			ON box.MessageTypeID = typ.MessageTypeID
		JOIN msg.MessageStatus stat
			ON box.MessageStatusID = stat.MessageStatusID
		JOIN dbo.Language lang
			ON box.LanguageID = lang.LanguageID
	WHERE MessageOutboxID = @MessageOutboxID
	
	

END
