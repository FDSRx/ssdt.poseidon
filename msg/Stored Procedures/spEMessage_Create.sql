﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/18/2013
-- Description:	Creates an EMessage entity
-- SAMPLE CALL:
/*
DECLARE @EMessage XML
EXEC msg.spEMessage_Create
	@EMessage = @EMessage OUTPUT
SELECT @EMessage AS EMessage
*/
--SELECT TOP 10 * FROM msg.MessageOutbox ORDER BY MessageOutboxID DESC
-- =============================================
CREATE PROCEDURE [msg].[spEMessage_Create] 
	@MessageTypeCode VARCHAR(25) = NULL,
	@MessageTypeID INT = NULL,
	@SourceKey VARCHAR(256) = NULL,
	@LanguageCode VARCHAR(25) = NULL,
	@LanguageID INT = NULL,
	@SenderServiceCode VARCHAR(25) = NULL,
	@SenderServiceID INT = NULL,
	@SenderMessageTypeCode VARCHAR(25) = NULL,
	@SenderMessageTypeID INT = NULL,
	@SenderKey VARCHAR(256) = NULL,
	@SenderName VARCHAR(256) = NULL,
	@SenderAddress VARCHAR(256) = NULL,
	@Sender_Thread_Key VARCHAR(256) = NULL,
	@Sender_Thread_IsConversation BIT = 0,
	@Sender_Thread_CanRespond BIT = 0,
	@RecipientServiceCode VARCHAR(25) = NULL,
	@RecipientServiceID INT = NULL,
	@RecipientMessageTypeCode VARCHAR(25) = NULL,
	@RecipientMessageTypeID INT = NULL,
	@RecipientName VARCHAR(256) = NULL,
	@RecipientAddress VARCHAR(MAX) = NULL,
	@Recipient_Thread_Key VARCHAR(256) = NULL,
	@Recipient_Thread_IsConversation BIT = 0,
	@Recipient_Thread_CanRespond BIT = 0,
	@Subject VARCHAR(1000) = NULL,
	@Header_Text VARCHAR(MAX) = NULL,
	@Header_IsFilePath BIT = 0,
	@Body_Text VARCHAR(MAX) = NULL,
	@Body_IsFilePath BIT = 0,
	@Footer_Text VARCHAR(MAX) = NULL,
	@Footer_IsFilePath BIT = 0,
	@IsImportant BIT = 0,
	@RoutingData XML = NULL,
	@UserDefinedData XML = NULL,
	@SpecialInstructions VARCHAR(MAX) = NULL,
	@DateCreated DATETIME = NULL,
	@DateExpires DATETIME = NULL,
	@DateDelay DATETIME = NULL,
	@MinuteExpires INT = NULL,
	@MinuteDelay INT = NULL,
	@EMessage XML = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------
	SET @Sender_Thread_CanRespond = ISNULL(@Sender_Thread_CanRespond, 0);
	SET @Sender_Thread_IsConversation = ISNULL(@Sender_Thread_IsConversation, 0);
	SET @Recipient_Thread_CanRespond = ISNULL(@Recipient_Thread_CanRespond, 0);
	SET @Recipient_Thread_IsConversation = ISNULL(@Recipient_Thread_IsConversation, 0);
	SET @Header_IsFilePath = ISNULL(@Header_IsFilePath, 0);
	SET @Body_IsFilePath = ISNULL(@Body_IsFilePath, 0);
	SET @Footer_IsFilePath = ISNULL(@Footer_IsFilePath, 0);
	SET @IsImportant = ISNULL(@IsImportant, 0);
	SET @DateCreated = ISNULL(@DateCreated, GETDATE());
	SET @DateExpires = ISNULL(@DateExpires, '12/31/9999');
	SET @EMessage = NULL;
	
	-----------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------

	-----------------------------------------------------------
	-- Set variables
	-----------------------------------------------------------
	SET @MessageTypeCode = ISNULL(dbo.fnGetMessageTypeCode(@MessageTypeID), @MessageTypeCode);
	SET @LanguageCode = ISNULL(dbo.fnGetLanguageCode(@LanguageID), @LanguageCode);
	SET @SenderServiceCode = ISNULL(dbo.fnGetServiceCode(@SenderServiceID), @SenderServiceCode);
	SET @RecipientServiceCode = ISNULL(dbo.fnGetServiceCode(@RecipientServiceID), @RecipientServiceCode);
	SET @SenderMessageTypeCode = ISNULL(dbo.fnGetMessageTypeCode(@SenderMessageTypeID), @SenderMessageTypeCode);
	SET @RecipientMessageTypeCode = ISNULL(dbo.fnGetMessageTypeCode(@RecipientMessageTypeID), @RecipientMessageTypeCode);
	
	SET  @EMessage =
		'<EMessage>' +
			dbo.fnXmlWriteElementString('MessageTypeCode', @MessageTypeCode) +
			dbo.fnXmlWriteElementString('SourceKey', @SourceKey) +  
			dbo.fnXmlWriteElementString('LanguageCode', @LanguageCode) +
			'<Sender>' +
				dbo.fnXmlWriteElementString('SenderKey', @SenderKey) +
				dbo.fnXmlWriteElementString('ServiceCode', @SenderServiceCode) +
				dbo.fnXmlWriteElementString('MessageTypeCode', @SenderMessageTypeCode) +
				dbo.fnXmlWriteElementString('Name', @SenderName) +
				dbo.fnXmlWriteElementString('Address', @SenderAddress) +
				'<Thread>' +
					dbo.fnXmlWriteElementString('ThreadKey', @Sender_Thread_Key) +
					dbo.fnXmlWriteElementString('IsConversation', @Sender_Thread_IsConversation) +
					dbo.fnXmlWriteElementString('CanRespond', @Sender_Thread_CanRespond) +
				'</Thread>' +
			'</Sender>' +
			'<Recipient>' +
				dbo.fnXmlWriteElementString('ServiceCode', @RecipientServiceCode) +
				dbo.fnXmlWriteElementString('MessageTypeCode', @RecipientMessageTypeCode) +
				dbo.fnXmlWriteElementString('Name', @RecipientName) +
				dbo.fnXmlWriteElementString('Address', @RecipientAddress) +
				'<Thread>' +
					dbo.fnXmlWriteElementString('ThreadKey', @Recipient_Thread_Key) +
					dbo.fnXmlWriteElementString('IsConversation', @Recipient_Thread_IsConversation) +
					dbo.fnXmlWriteElementString('CanRespond', @Recipient_Thread_CanRespond) +
				'</Thread>' +
			'</Recipient>' +
			'<Message>' +    
				dbo.fnXmlWriteElementString('Subject', @Subject) +
				'<Header>' +
				  dbo.fnXmlWriteElementString('FilePath', CASE WHEN @Header_IsFilePath = 1 THEN @Header_Text ELSE '' END) +
				  dbo.fnXmlWriteElementString('Text', CASE WHEN @Header_IsFilePath = 0 THEN @Header_Text ELSE '' END) +
				'</Header>' +
				'<Body>' +
					dbo.fnXmlWriteElementString('FilePath', CASE WHEN @Body_IsFilePath = 1 THEN @Body_Text ELSE '' END) +
					dbo.fnXmlWriteElementString('Text', CASE WHEN @Body_IsFilePath = 0 THEN @Body_Text ELSE '' END) +
				'</Body>' +
				'<Footer>' +
					dbo.fnXmlWriteElementString('FilePath', CASE WHEN @Footer_IsFilePath = 1 THEN @Footer_Text ELSE '' END) +
					dbo.fnXmlWriteElementString('Text', CASE WHEN @Footer_IsFilePath = 0 THEN @Footer_Text ELSE '' END) +
				'</Footer>' +
				dbo.fnXmlWriteElementString('IsImportant', @IsImportant) +
			'</Message>' +
			CASE 
				WHEN @RoutingData IS NOT NULL THEN CONVERT(VARCHAR(MAX), @RoutingData) 
				ELSE '<Routing>' +
				'<DestinationRoute>' +
					dbo.fnXmlWriteElementString('ServiceCode', @RecipientServiceCode) +
					dbo.fnXmlWriteElementString('MessageTypeCode', @RecipientMessageTypeCode) +
					dbo.fnXmlWriteElementString('Address', @RecipientAddress) +
					'<Thread>' +
						dbo.fnXmlWriteElementString('ThreadKey', @Recipient_Thread_Key) + 
						dbo.fnXmlWriteElementString('IsConversation', @Recipient_Thread_IsConversation) +
						dbo.fnXmlWriteElementString('CanRespond', @Recipient_Thread_CanRespond) +
					'</Thread>' +
				'</DestinationRoute>' +
				'<SourceRoute>' +
					dbo.fnXmlWriteElementString('ServiceCode', @SenderServiceCode) +
					dbo.fnXmlWriteElementString('MessageTypeCode', @SenderMessageTypeCode) +
					dbo.fnXmlWriteElementString('Address', @SenderAddress) +
					'<Thread>' +
						dbo.fnXmlWriteElementString('ThreadKey', @Sender_Thread_Key) + 
						dbo.fnXmlWriteElementString('IsConversation', @Sender_Thread_IsConversation) +
						dbo.fnXmlWriteElementString('CanRespond', @Sender_Thread_CanRespond) +
					'</Thread>' + 
				'</SourceRoute>' +
			'</Routing>' END +
			CASE
				WHEN @UserDefinedData IS NOT NULL THEN CONVERT(VARCHAR(MAX), @UserDefinedData)
				ELSE ''
			END +
			'<SpecialInstructions />' +
			dbo.fnXmlWriteElementString('DateCreated', @DateCreated) +
			dbo.fnXmlWriteElementString('DateExpires', @DateExpires) +
			dbo.fnXmlWriteElementString('DateDelay', @DateDelay) +
			dbo.fnXmlWriteElementString('MinuteExpires', @MinuteExpires) +
			dbo.fnXmlWriteElementString('MinuteDelay', @MinuteDelay) +
		'</EMessage>'
END
