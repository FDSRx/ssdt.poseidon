﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/30/2014
-- Description:	Updates a message part.
-- =============================================
CREATE PROCEDURE [msg].[spMessagePart_Update]
	@MessagePartID BIGINT = NULL OUTPUT,
	@KeyName VARCHAR(256) = NULL,
	@Name VARCHAR(256) = NULL,
	@ContentTypeID INT = NULL,
	@Message VARCHAR(MAX),
	@LanguageID INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------
	-- Set variables
	---------------------------------------------------------------
	SET @LanguageID = ISNULL(@LanguageID, dbo.fnGetLanguageDefaultID());
	SET @ContentTypeID = ISNULL(@ContentTypeID, msg.fnGetContentTypeDefaultID());
	
	---------------------------------------------------------------
	-- Obtain message part id if not supplied.
	-- <Summary>
	-- If a MessagePartID was not supplied then the unique key lookup
	-- will be performed to obtain an ID
	-- </Summary>
	---------------------------------------------------------------
	IF @MessagePartID IS NULL
	BEGIN
		SET @MessagePartID = (
			SELECT TOP 1
				MessagePartID
			FROM msg.MessagePart
			WHERE KeyName = @KeyName
				AND LanguageID = @LanguageID
		);
	END

	---------------------------------------------------------------
	-- Update message part record.
	---------------------------------------------------------------
	UPDATE part
	SET 
		part.Name = @Name,
		part.ContentTypeID = @ContentTypeID,
		part.Message = @Message,
		part.DateModified = GETDATE()
	FROM msg.MessagePart part
	WHERE MessagePartID = @MessagePartID
	
END
