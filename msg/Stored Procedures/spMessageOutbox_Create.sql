﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/7/2013
-- Description:	Create an outbox message
-- =============================================
CREATE PROCEDURE [msg].[spMessageOutbox_Create]
	@SenderID VARCHAR(255),
	@ServiceID INT,	
	@SourceKey VARCHAR(255),
	@AdditionalData XML = NULL,
	@MessageTypeID INT,
	@MessageData XML,
	@FromAddress VARCHAR(255),
	@ToAddress VARCHAR(MAX),
	@MessageStatusID INT,
	@DateExpires DATETIME = NULL,
	@LanguageID INT = NULL,
	@DateDelay INT = NULL,
	@MinuteExpires	INT = NULL,
	@MinuteDelay INT = NULL,
	@MessageOutboxID INT = NULL OUTPUT,
	@TrackingNumber VARCHAR(255) = NULL OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Set the language default if one was not provided
	IF ISNULL(@LanguageID, '') = ''
	BEGIN
		SET @LanguageID = dbo.fnGetLanguageDefaultID();
	END
	
	-- Preset intervals to 0 if null
	SET @DateDelay = ISNULL(@DateDelay,0);
	SET @MinuteDelay = ISNULL(@MinuteDelay, 0);
	SET @MinuteExpires = ISNULL(@MinuteExpires, 0);
	
	-- Fix date expires if it is not provided
	SELECT @DateExpires = CASE WHEN @DateExpires IS NULL THEN '12/31/9999' ELSE @DateExpires END;
	
	-- TODO: This may need an exist clause; however, this function should be the primary method for setting up outbox items
	-- Create a tracking ticket
	SET @TrackingNumber = ISNULL(@TrackingNumber, NEWID());

	-- Insert the message to the outbox for delivery
	INSERT INTO msg.MessageOutbox (
		SenderID,
		ServiceID,	
		SourceKey,
		AdditionalData,
		MessageTypeID,
		MessageData,
		FromAddress,
		ToAddress,
		MessageStatusID,
		DateExpires,
		LanguageID,
		DateDelay,
		MinuteDelay,
		MinuteExpires,
		TrackingNumber	
	)
	SELECT 
		@SenderID,
		@ServiceID,		
		@SourceKey,
		@AdditionalData,
		@MessageTypeID,
		@MessageData,
		@FromAddress,
		@ToAddress,
		@MessageStatusID,
		@DateExpires,
		@LanguageID,
		@DateDelay,
		@MinuteDelay,
		@MinuteExpires,
		@TrackingNumber
	
	-- return row id	
	SET @MessageOutboxID = SCOPE_IDENTITY();
	
	
END
