﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/8/2013
-- Description:	Creates a message error log
-- SAMPLE CALL: EXEC msg.spLogMessageError @UserName = 'Hermes', @ErrorProcedure = 'spLogMessageErrror', @ErrorMessage = 'Test failure'
-- =============================================
CREATE PROCEDURE [msg].[spLogMessageError]
	@UserName NVARCHAR(255) = NULL,
	@ErrorNumber INT = NULL,
	@ErrorSeverity INT = NULL,
	@ErrorState INT = NULL,
	@ErrorProcedure NVARCHAR(126) = NULL,
	@ErrorLine INT = NULL,
	@ErrorMessage NVARCHAR(MAX) = NULL,
	@MessageOutboxID INT = NULL,
	@Message NVARCHAR(MAX) = NULL,
	@MessageErrorLogID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	BEGIN TRY
	
		------------------------------------------------------
		-- Assign defaults
		------------------------------------------------------
		SELECT 
			@UserName = CASE WHEN @UserName IS NULL THEN CONVERT(sysname, CURRENT_USER) ELSE @UserName END, 
            @ErrorNumber = CASE WHEN @ErrorNumber IS NULL THEN  ERROR_NUMBER() ELSE @ErrorNumber END,
            @ErrorSeverity = CASE WHEN @ErrorSeverity IS NULL THEN ERROR_SEVERITY() ELSE @ErrorSeverity END,
            @ErrorState = CASE WHEN @ErrorState IS NULL THEN ERROR_STATE() ELSE @ErrorState END,
            @ErrorProcedure = CASE WHEN @ErrorProcedure IS NULL THEN ERROR_PROCEDURE() ELSE @ErrorProcedure END,
            @ErrorLine = CASE WHEN @ErrorLine IS NULL THEN ERROR_LINE() ELSE @ErrorLine END,
            @ErrorMessage = CASE WHEN @ErrorMessage IS NULL THEN ERROR_MESSAGE() ELSE @ErrorMessage END
	
		------------------------------------------------------
		-- Create message error
		------------------------------------------------------
		INSERT INTO msg.MessageErrorLog (
			UserName,
			ErrorNumber,
			ErrorSeverity,
			ErrorState,
			ErrorProcedure,
			ErrorLine,
			ErrorMessage,
			MessageOutboxID,
			Message
		)
		SELECT
			@UserName,
			@ErrorNumber,
			@ErrorSeverity,
			@ErrorState,
			@ErrorProcedure,
			@ErrorLine,
			@ErrorMessage,
			@MessageOutboxID,
			@Message
			
		SET @MessageErrorLogID = SCOPE_IDENTITY();
			
		
	END TRY
	BEGIN CATCH
	
		EXECUTE [dbo].[spLogError];
		
	END CATCH
	
END
