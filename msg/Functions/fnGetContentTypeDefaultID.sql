﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/20/2013
-- Description:	Gets the default content type id
-- SAMPLE CALL: SELECT msg.fnGetContentTypeDefaultID()
-- SELECT * FROM msg.ContentType
-- =============================================
CREATE FUNCTION [msg].[fnGetContentTypeDefaultID]
(
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT;

	SET @ID = msg.fnGetContentTypeID('TXT');

	-- Return the result of the function
	RETURN @ID;

END

