﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/20/2013
-- Description:	Gets the message status id
-- SAMPLE CALL: SELECT msg.fnGetMessageStatusID('PNDNG')
-- SAMPLE CALL: SELECT msg.fnGetMessageStatusID(3)
-- SELECT * FROM msg.MessageStatus
-- =============================================
CREATE FUNCTION [msg].[fnGetMessageStatusID]
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT;

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT MessageStatusID FROM msg.MessageStatus WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @ID = (SELECT MessageStatusID FROM msg.MessageStatus  WHERE MessageStatusID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @ID;

END

