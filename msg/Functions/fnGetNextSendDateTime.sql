﻿
/*
 ==================================================================
 Author:	  Alex Korolev
 Create date: 01/20/2015
 Description:	Returns Next alowed time for message to be send
 SAMPLE CALL: SELECT msg.fnGetNextSendDateTime ('EBDC5ED1-87DC-449B-BE34-DF07F68B7891',3)
 
==================================================================
*/
CREATE FUNCTION [msg].[fnGetNextSendDateTime]
(
	@SenderKey UNIQUEIDENTIFIER,
	@MessageTypeID INT
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here
	DECLARE @NEXDATE DATETIME;


	--- GET NEXT DAY
DECLARE @TimeStart TIME,
		@TimeEnd TIME,
		@DayID INT
		
		
				

SELECT 
	@TimeStart  = sh.TimeStart,
	@TimeEnd    = sh.TimeEnd,
	@DayID		= sh.DayID
FROM
	msg.Sender s
	INNER JOIN msg.SenderMessageHours sh
	ON s.SenderID = sh.SenderID
WHERE s.SenderKey = @SenderKey
AND  sh.DayID = DATEPART (dw , GETDATE()) -1
AND  sh.MessageTypeID = @MessageTypeID


IF(@TimeStart IS NOT NULL OR @TimeEnd IS NOT NULL)
	BEGIN
    
		
		IF(CAST(GETDATE() AS TIME) >= @TimeStart OR CAST(GETDATE() AS TIME) < @TimeEnd)
		BEGIN		  

			  IF(@DayID = 7)
				  BEGIN
					 SET @DayID = 1
				  END
			  ELSE
				  BEGIN
					 SET @DayID = @DayID + 1		     
				  END	
				  
				  
			SELECT 
			@NEXDATE =	CAST(CONVERT(DATETIME, CONVERT(CHAR(8), 
						DATEADD (day , 1 , GETDATE()), 112) 
						 + ' ' + 
						CONVERT(CHAR(8), sh.TimeEnd, 108)) as DATETIME)
			FROM
				msg.Sender s
				INNER JOIN msg.SenderMessageHours sh
				ON s.SenderID = sh.SenderID
			WHERE s.SenderKey = @SenderKey
			AND  sh.DayID = @DayID
			AND  sh.MessageTypeID = @MessageTypeID		
			END

			

		END
	RETURN @NEXDATE;

END


