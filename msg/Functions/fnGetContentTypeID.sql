﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/20/2013
-- Description:	Gets the content type ID
-- SAMPLE CALL: SELECT msg.fnGetContentTypeID('HTML')
-- SAMPLE CALL: SELECT msg.fnGetContentTypeID(1)
-- SELECT * FROM msg.ContentType
-- =============================================
CREATE FUNCTION [msg].[fnGetContentTypeID]
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT;

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT ContentTypeID FROM msg.ContentType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @ID = (SELECT ContentTypeID FROM msg.ContentType  WHERE ContentTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @ID;

END

