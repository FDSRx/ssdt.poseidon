﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/30/2014
-- Description:	Gets a message part.
-- SAMPLE CALL: SELECT msg.fnGetMessagePart('CommonEmailFooter', DEFAULT)
-- SAMPLE CALL: SELECT msg.fnGetMessagePart('CommonEmailFooter', 'en')
-- SAMPLE CALL: SELECT msg.fnGetMessagePart('CommonEmailFooter', 'es')
-- SELECT * FROM msg.MessagePart
-- =============================================
CREATE FUNCTION [msg].[fnGetMessagePart]
(
	@KeyName VARCHAR(256),
	@LanguageKey VARCHAR(5) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Message VARCHAR(MAX),
		@LanguageID INT,
		@DefaultLanguageID INT
	
	-- Get language id
	SET @DefaultLanguageID = dbo.fnGetLanguageDefaultID();
	SET @LanguageID = ISNULL(dbo.fnGetLanguageID(@LanguageKey), dbo.fnGetLanguageDefaultID());


	----------------------------------------------------------------------
	-- Use a heirarchy pattern to obtain message data.
	-- <Summary>
	-- The data will first be queried with the specified language code.
	-- If the language code is unable to be found then the data will
	-- be queried with its default langauge code (english).
	-- This ensures that a record for a key should come back.
	-- </Summary>
	----------------------------------------------------------------------
	
	----------------------------------------------------------------------
	-- Step 1: Query specified langauge
	----------------------------------------------------------------------
	SELECT
		@Message = Message
	FROM msg.MessagePart
	WHERE KeyName = @KeyName
		AND LanguageID = @LanguageID

	----------------------------------------------------------------------
	-- Step 2: If the specified did not return any results then query
	--			on the default language
	----------------------------------------------------------------------	
	IF @Message IS NULL
	BEGIN
		SELECT
			@Message = Message
		FROM msg.MessagePart
		WHERE KeyName = @KeyName
			AND LanguageID = @DefaultLanguageID
	END

	-- Return the result of the function
	RETURN @Message;

END

