﻿CREATE TABLE [medical].[AdministerType] (
    [AdministerTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]             VARCHAR (25)     NOT NULL,
    [Name]             VARCHAR (50)     NOT NULL,
    [Description]      VARCHAR (1000)   NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_AdministerType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_AdministerType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_AdministerType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_AdministerType] PRIMARY KEY CLUSTERED ([AdministerTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AdministerType_Code]
    ON [medical].[AdministerType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AdministerType_Name]
    ON [medical].[AdministerType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AdministerType_rowguid]
    ON [medical].[AdministerType]([rowguid] ASC);

