﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/25/2014
-- Description:	Returns the AdministerTypeID.
-- SAMPLE CALL: SELECT medical.fnGetAdministerTypeID(1);
-- SAMPLE CALL: SELECT medical.fnGetAdministerTypeID('RPH');
-- SELECT * FROM medical.AdministerType
-- =============================================
CREATE FUNCTION medical.[fnGetAdministerTypeID]
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT AdministerTypeID FROM medical.AdministerType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT AdministerTypeID FROM medical.AdministerType WHERE AdministerTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
