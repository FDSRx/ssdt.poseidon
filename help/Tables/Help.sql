﻿CREATE TABLE [help].[Help] (
    [HelpID]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID] INT              NULL,
    [BusinessID]    BIGINT           NULL,
    [ArticleID]     BIGINT           NOT NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_Help_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_Help_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_Help_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_Help] PRIMARY KEY CLUSTERED ([HelpID] ASC),
    CONSTRAINT [FK_Help_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_Help_Article] FOREIGN KEY ([ArticleID]) REFERENCES [news].[Article] ([ArticleID]),
    CONSTRAINT [FK_Help_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Help_AppBizArticle]
    ON [help].[Help]([ApplicationID] ASC, [BusinessID] ASC, [ArticleID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Help_rowguid]
    ON [help].[Help]([rowguid] ASC);

