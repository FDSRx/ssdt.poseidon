﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/13/2015
-- Description:	Returns a list of all known stores that is SSIS friendly.
-- SAMPLE CALL: EXEC ssis.spApi_Store_Get_List
-- SAMPLE CALL: EXEC ssis.spApi_Store_Get_List @ChainKey = '3685'
-- SAMPLE CALL: EXEC ssis.spApi_Store_Get_List @Name = 'ACADIA'
-- SAMPLE CALL: EXEC ssis.spApi_Store_Get_List @Nabp = '4512287'
-- SAMPLE CALL: EXEC ssis.spApi_Store_Get_List @ServiceKey = 'ENG'

-- SELECT * FROM dbo.vwStore
-- SELECT * FROM dbo.ChainStore
-- SELECT * FROM dbo.Service
-- =============================================
CREATE PROCEDURE [ssis].[spApi_Store_Get_List]
	@BusinessKey VARCHAR(MAX) = NULL,
	@SourceStoreKey VARCHAR(MAX) = NULL,
	@ChainKey VARCHAR(MAX) = NULL,
	@Nabp VARCHAR(MAX) = NULL,
	@Name VARCHAR(256) = NULL,
	@ServiceKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(MAX) = NULL,
	@MessageTypeKey VARCHAR(MAX) = NULL,
	@CommunicationMethodKey VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------------------------	
	-- SSIS compatibility.
	-- <Summary>
	-- Allows an SSIS package to recognize the result schema as it is unable to determine
	-- the schema of the temporary table.
	-- </Summary>
	----------------------------------------------------------------------------------------	
	IF 1 = 0 
	BEGIN
	
		DECLARE @Property VARCHAR(10) = '';
		
		SELECT TOP 1
			CAST(@Property AS VARCHAR(50)) AS BusinessEntityID,
			CAST(@Property AS VARCHAR(256)) AS Name,
			CAST(@Property AS VARCHAR(75)) AS SourceStoreID,
			CAST(@Property AS VARCHAR(75)) AS NABP,
			CAST(@Property AS VARCHAR(50)) AS BusinessToken,
			CAST(@Property AS VARCHAR(50)) AS BusinessNumber,
			CAST(@Property AS VARCHAR(50)) AS ChainID,
			CAST(@Property AS VARCHAR(50)) AS ChainToken,
			CAST(@Property AS VARCHAR(256)) AS AddressLine1,
			CAST(@Property AS VARCHAR(256)) AS AddressLine2,
			CAST(@Property AS VARCHAR(256)) AS City,
			CAST(@Property AS VARCHAR(256)) AS State,
			CAST(@Property AS VARCHAR(256)) AS PostalCode,
			CAST(@Property AS VARCHAR(256)) AS Latitude,
			CAST(@Property AS VARCHAR(256)) AS Longitude,
			CAST(@Property AS VARCHAR(256)) AS Website,
			CAST(@Property AS VARCHAR(256)) AS PhoneNumber,
			CAST(@Property AS VARCHAR(256)) AS EmailAddress,
			CAST(@Property AS VARCHAR(256)) AS FaxNumber,
			CAST(@Property AS VARCHAR(256)) AS TimeZoneCode,
			CAST(@Property AS VARCHAR(256)) AS TimeZoneLocation,
			CAST(@Property AS INT) AS TimeZoneOffSet,
			CAST(@Property AS BIT) AS SupportDST
				
	END	
	
	--------------------------------------------------------------------------------------------
	-- Fetch Results
	--------------------------------------------------------------------------------------------
	EXEC api.spDbo_Store_Get_List			
		@BusinessKey = @BusinessKey,
		@SourceStoreKey = @SourceStoreKey,
		@ChainKey = @ChainKey,
		@Nabp = @Nabp,
		@Name = @Name,
		@ServiceKey = @ServiceKey,
		@ApplicationKey = @ApplicationKey,
		@MessageTypeKey = @MessageTypeKey,
		@CommunicationMethodKey = @CommunicationMethodKey,
		@Skip = @Skip,
		@Take = @Take,
		@SortCollection = @SortCollection
	


		
END
