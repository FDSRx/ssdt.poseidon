﻿CREATE QUEUE [dbo].[ClaimFactoryQueue]
    WITH POISON_MESSAGE_HANDLING(STATUS = OFF), ACTIVATION (STATUS = ON, PROCEDURE_NAME = [serviceBroker].[spPhrm_ClaimFactoryQueue_Dequeue], MAX_QUEUE_READERS = 3, EXECUTE AS N'dbo');

