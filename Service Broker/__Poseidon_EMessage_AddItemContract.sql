﻿CREATE CONTRACT [//Poseidon/EMessage/AddItemContract]
    AUTHORIZATION [dbo]
    ([//Poseidon/EMessage/AddItem] SENT BY INITIATOR, [//Poseidon/EMessage/ItemAdded] SENT BY TARGET);

