﻿CREATE QUEUE [dbo].[EMessageFactoryQueue]
    WITH POISON_MESSAGE_HANDLING(STATUS = OFF), ACTIVATION (STATUS = ON, PROCEDURE_NAME = [msg].[spServiceBroker_EMessageFactory_Dequeue], MAX_QUEUE_READERS = 3, EXECUTE AS N'dbo');

