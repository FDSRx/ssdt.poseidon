﻿CREATE CONTRACT [//Poseidon/Claim/AddItemContract]
    AUTHORIZATION [dbo]
    ([//Poseidon/Claim/AddItem] SENT BY INITIATOR, [//Poseidon/Claim/ItemAdded] SENT BY TARGET);

