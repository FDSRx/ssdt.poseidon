﻿CREATE TABLE [acl].[CredentialEntityRole] (
    [CredentialEntityRoleID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID]     BIGINT           NOT NULL,
    [ApplicationID]          INT              NULL,
    [BusinessEntityID]       BIGINT           NULL,
    [RoleID]                 INT              NOT NULL,
    [rowguid]                UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntityRole_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_CredentialEntityRole_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]           DATETIME         CONSTRAINT [DF_CredentialEntityRole_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              VARCHAR (256)    NULL,
    [ModifiedBy]             VARCHAR (256)    NULL,
    CONSTRAINT [PK_CredentialEntityRole] PRIMARY KEY CLUSTERED ([CredentialEntityRoleID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CredentialEntityRole_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CredentialEntityRole_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_CredentialEntityRole_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID]),
    CONSTRAINT [FK_CredentialEntityRole_Roles] FOREIGN KEY ([RoleID]) REFERENCES [acl].[Roles] ([RoleID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityRole_CredAppBizRole]
    ON [acl].[CredentialEntityRole]([CredentialEntityID] ASC, [ApplicationID] ASC, [BusinessEntityID] ASC, [RoleID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityRole_rowguid]
    ON [acl].[CredentialEntityRole]([rowguid] ASC) WITH (FILLFACTOR = 90);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/8/2014
-- Description:	Audits a record change of the acl.CredentialEntityRole table
-- SELECT * FROM creds.Membership_History
-- SELECT * FROM creds.Membership  WHERE CredentialEntityID = 801018
-- TRUNCATE TABLE creds.Membership_History
-- =============================================
CREATE TRIGGER acl.[trigCredentialEntityRole_Audit]
   ON  acl.CredentialEntityRole
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10); -- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-----------------------------------------------------------------------------
	-- Audit transaction
	-----------------------------------------------------------------------------
	-- Audit inserted records
	INSERT INTO acl.CredentialEntityRole_History (
		CredentialEntityRoleID,
		CredentialEntityID,
		ApplicationID,
		BusinessEntityID,
		RoleID,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		d.CredentialEntityRoleID,
		d.CredentialEntityID,
		d.ApplicationID,
		d.BusinessEntityID,
		d.RoleID,
		d.rowguid,	
		d.DateCreated,	
		d.DateModified,
		d.CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d

	
	--SELECT * FROM creds.Membership
	
	

END
