﻿CREATE TABLE [acl].[AccessDirective] (
    [AccessDirectiveID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]              VARCHAR (25)     NOT NULL,
    [Name]              VARCHAR (50)     NOT NULL,
    [Description]       VARCHAR (1000)   NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_AccessDirective_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_AccessDirective_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_AccessDirective_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_AccessDirective] PRIMARY KEY CLUSTERED ([AccessDirectiveID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AccessDirective_Code]
    ON [acl].[AccessDirective]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AccessDirective_Name]
    ON [acl].[AccessDirective]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AccessDirective_rowguid]
    ON [acl].[AccessDirective]([rowguid] ASC);

