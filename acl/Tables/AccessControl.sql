﻿CREATE TABLE [acl].[AccessControl] (
    [AccessControlID]        INT              IDENTITY (1, 1) NOT NULL,
    [ApplicationID]          INT              NOT NULL,
    [BusinessEntityID]       BIGINT           NULL,
    [AccessInspectionTypeID] INT              NOT NULL,
    [AccessFilterTypeID]     INT              NOT NULL,
    [Filter]                 VARCHAR (500)    NOT NULL,
    [AccessDirectiveID]      INT              NOT NULL,
    [SortOrder]              INT              CONSTRAINT [DF_AccessControl_SortOrder] DEFAULT ((0)) NOT NULL,
    [rowguid]                UNIQUEIDENTIFIER CONSTRAINT [DF_AccessControl_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_AccessControl_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]           DATETIME         CONSTRAINT [DF_AccessControl_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              VARCHAR (256)    NULL,
    [ModifiedBy]             VARCHAR (256)    NULL,
    CONSTRAINT [PK_AccessControl] PRIMARY KEY CLUSTERED ([AccessControlID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AccessControl_AccessDirective] FOREIGN KEY ([AccessDirectiveID]) REFERENCES [acl].[AccessDirective] ([AccessDirectiveID]),
    CONSTRAINT [FK_AccessControl_AccessFilterType] FOREIGN KEY ([AccessFilterTypeID]) REFERENCES [acl].[AccessFilterType] ([AccessFilterTypeID]),
    CONSTRAINT [FK_AccessControl_AccessInspectionType] FOREIGN KEY ([AccessInspectionTypeID]) REFERENCES [acl].[AccessInspectionType] ([AccessInspectionTypeID]),
    CONSTRAINT [FK_AccessControl_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_AccessControl_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AccessControl_rowguid]
    ON [acl].[AccessControl]([rowguid] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AccessControl_AppBizInspectFilter]
    ON [acl].[AccessControl]([ApplicationID] ASC, [BusinessEntityID] ASC, [AccessInspectionTypeID] ASC, [AccessFilterTypeID] ASC, [Filter] ASC) WITH (FILLFACTOR = 90);

