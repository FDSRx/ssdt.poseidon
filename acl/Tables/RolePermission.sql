﻿CREATE TABLE [acl].[RolePermission] (
    [RolePermissionID] INT              IDENTITY (1, 1) NOT NULL,
    [ApplicationID]    INT              NULL,
    [BusinessEntityID] BIGINT           NULL,
    [RoleID]           INT              NOT NULL,
    [PermissionID]     INT              NOT NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_RolePermission_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_RolePermission_DateCreated] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_RolePermission] PRIMARY KEY CLUSTERED ([RolePermissionID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_RolePermission_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_RolePermission_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_RolePermission_Permission] FOREIGN KEY ([PermissionID]) REFERENCES [acl].[Permission] ([PermissionID]),
    CONSTRAINT [FK_RolePermission_Roles] FOREIGN KEY ([RoleID]) REFERENCES [acl].[Roles] ([RoleID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_RolePermission_AppBizRolePerm]
    ON [acl].[RolePermission]([ApplicationID] ASC, [BusinessEntityID] ASC, [RoleID] ASC, [PermissionID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_RolePermission_rowguid]
    ON [acl].[RolePermission]([rowguid] ASC) WITH (FILLFACTOR = 90);

