﻿CREATE TABLE [acl].[CredentialEntityRolePermission_History] (
    [CredentialEntityRolePermissionHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityRolePermissionID]        BIGINT           NOT NULL,
    [CredentialEntityID]                      BIGINT           NOT NULL,
    [ApplicationID]                           INT              NULL,
    [BusinessID]                              BIGINT           NULL,
    [RoleID]                                  INT              NOT NULL,
    [PermissionID]                            INT              NOT NULL,
    [rowguid]                                 UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]                             DATETIME         NOT NULL,
    [DateModified]                            DATETIME         NOT NULL,
    [CreatedBy]                               VARCHAR (256)    NULL,
    [ModifiedBy]                              VARCHAR (256)    NULL,
    [AuditGuid]                               UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntityRolePermission_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                             VARCHAR (10)     NULL,
    [DateAudited]                             DATETIME         CONSTRAINT [DF_CredentialEntityRolePermission_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CredentialEntityRolePermission_History] PRIMARY KEY CLUSTERED ([CredentialEntityRolePermissionHistoryID] ASC)
);

