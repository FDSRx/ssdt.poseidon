﻿CREATE TABLE [acl].[PermissionAllocation] (
    [PermissionAllocationID] INT              IDENTITY (1, 1) NOT NULL,
    [ApplicationID]          INT              NOT NULL,
    [BusinessEntityID]       BIGINT           NULL,
    [PermissionID]           INT              NOT NULL,
    [Description]            VARCHAR (1000)   NULL,
    [rowguid]                UNIQUEIDENTIFIER CONSTRAINT [DF_PermissionAllocation_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_PermissionAllocation_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]           DATETIME         CONSTRAINT [DF_PermissionAllocation_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              VARCHAR (256)    NULL,
    [ModifiedBy]             VARCHAR (256)    NULL,
    CONSTRAINT [PK_PermissionAllocation] PRIMARY KEY CLUSTERED ([PermissionAllocationID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_PermissionAllocation_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_PermissionAllocation_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_PermissionAllocation_Permission] FOREIGN KEY ([PermissionID]) REFERENCES [acl].[Permission] ([PermissionID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PermissionAllocation_AppBizPerm]
    ON [acl].[PermissionAllocation]([ApplicationID] ASC, [BusinessEntityID] ASC, [PermissionID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PermissionAllocation_rowguid]
    ON [acl].[PermissionAllocation]([rowguid] ASC) WITH (FILLFACTOR = 90);

