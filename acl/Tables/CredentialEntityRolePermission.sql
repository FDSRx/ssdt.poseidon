﻿CREATE TABLE [acl].[CredentialEntityRolePermission] (
    [CredentialEntityRolePermissionID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID]               BIGINT           NOT NULL,
    [ApplicationID]                    INT              NULL,
    [BusinessID]                       BIGINT           NULL,
    [RoleID]                           INT              NOT NULL,
    [PermissionID]                     INT              NOT NULL,
    [rowguid]                          UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntityRolePermission_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                      DATETIME         CONSTRAINT [DF_CredentialEntityRolePermission_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                     DATETIME         CONSTRAINT [DF_CredentialEntityRolePermission_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                        VARCHAR (256)    NULL,
    [ModifiedBy]                       VARCHAR (256)    NULL,
    CONSTRAINT [PK_CredentialEntityRolePermission] PRIMARY KEY CLUSTERED ([CredentialEntityRolePermissionID] ASC),
    CONSTRAINT [FK_CredentialEntityRolePermission_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CredentialEntityRolePermission_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_CredentialEntityRolePermission_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID]),
    CONSTRAINT [FK_CredentialEntityRolePermission_Permission] FOREIGN KEY ([PermissionID]) REFERENCES [acl].[Permission] ([PermissionID]),
    CONSTRAINT [FK_CredentialEntityRolePermission_Roles] FOREIGN KEY ([RoleID]) REFERENCES [acl].[Roles] ([RoleID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityRolePermission_rowguid]
    ON [acl].[CredentialEntityRolePermission]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityRolePermission_CredAppBizRolePerm]
    ON [acl].[CredentialEntityRolePermission]([CredentialEntityID] ASC, [ApplicationID] ASC, [BusinessID] ASC, [RoleID] ASC, [PermissionID] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/21/2016
-- Description:	Audits a record change of the acl.CredentialEntityRolePermission table
-- SELECT * FROM acl.CredentialEntityRolePermission_History
-- SELECT * FROM acl.CredentialEntityRolePermission  WHERE CredentialEntityID = 801018
-- TRUNCATE TABLE acl.CredentialEntityRolePermission_History
-- =============================================
CREATE TRIGGER [acl].[trigCredentialEntityRolePermission_Audit]
   ON  acl.CredentialEntityRolePermission
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10); -- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-----------------------------------------------------------------------------
	-- Audit transaction
	-----------------------------------------------------------------------------
	-- Audit inserted records
	INSERT INTO acl.CredentialEntityRolePermission_History (
		CredentialEntityRolePermissionID,
		CredentialEntityID,
		ApplicationID,
		BusinessID,
		RoleID,
		PermissionID,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		d.CredentialEntityRolePermissionID,
		d.CredentialEntityID,
		d.ApplicationID,
		d.BusinessID,
		d.RoleID,
		d.PermissionID,
		d.rowguid,	
		d.DateCreated,	
		d.DateModified,
		d.CreatedBy,
		CASE 
			WHEN NULLIF(@DataString, '') IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE SUSER_NAME()
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d

	
	--SELECT * FROM acl.CredentialEntityRolePermission_History
	
	

END