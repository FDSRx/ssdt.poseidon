﻿CREATE TABLE [acl].[CredentialEntityGroup] (
    [CredentialEntityGroupID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityID]      BIGINT           NOT NULL,
    [ApplicationID]           INT              NULL,
    [BusinessEntityID]        BIGINT           NULL,
    [GroupID]                 INT              NOT NULL,
    [rowguid]                 UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntityGroup_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]             DATETIME         CONSTRAINT [DF_CredentialEntityGroup_DateCreated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CredentialEntityGroup] PRIMARY KEY CLUSTERED ([CredentialEntityGroupID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CredentialEntityGroup_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CredentialEntityGroup_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_CredentialEntityGroup_CredentialEntity] FOREIGN KEY ([CredentialEntityID]) REFERENCES [creds].[CredentialEntity] ([CredentialEntityID]),
    CONSTRAINT [FK_CredentialEntityGroup_Groups] FOREIGN KEY ([GroupID]) REFERENCES [acl].[Groups] ([GroupID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityGroup_CredentialEntityGroup]
    ON [acl].[CredentialEntityGroup]([CredentialEntityID] ASC, [GroupID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CredentialEntityGroup_rowguid]
    ON [acl].[CredentialEntityGroup]([rowguid] ASC) WITH (FILLFACTOR = 90);

