﻿CREATE TABLE [acl].[GroupRole] (
    [GroupRoleID]      INT              IDENTITY (1, 1) NOT NULL,
    [ApplicationID]    INT              NOT NULL,
    [BusinessEntityID] BIGINT           NULL,
    [GroupID]          INT              NOT NULL,
    [RoleID]           INT              NOT NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_GroupRole_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_GroupRole_DateCreated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_GroupRole] PRIMARY KEY CLUSTERED ([GroupRoleID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_GroupRole_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_GroupRole_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_GroupRole_Groups] FOREIGN KEY ([GroupID]) REFERENCES [acl].[Groups] ([GroupID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_GroupRole_AppBizGroupRole]
    ON [acl].[GroupRole]([ApplicationID] ASC, [BusinessEntityID] ASC, [GroupID] ASC, [RoleID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [UIX_GroupRole_rowguid]
    ON [acl].[GroupRole]([rowguid] ASC) WITH (FILLFACTOR = 90);

