﻿CREATE TABLE [acl].[AccessFilterType] (
    [AccessFilterTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]               VARCHAR (25)     NOT NULL,
    [Name]               VARCHAR (50)     NOT NULL,
    [Description]        VARCHAR (1000)   NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_AccessFilterType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_AccessFilterType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_AccessFilterType_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_AccessFilterType] PRIMARY KEY CLUSTERED ([AccessFilterTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AccessFilterType_Code]
    ON [acl].[AccessFilterType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AccessFilterType_Name]
    ON [acl].[AccessFilterType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AccessFilterType_rowguid]
    ON [acl].[AccessFilterType]([rowguid] ASC);

