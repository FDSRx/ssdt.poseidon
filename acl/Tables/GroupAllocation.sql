﻿CREATE TABLE [acl].[GroupAllocation] (
    [GroupAllocationID] INT              IDENTITY (1, 1) NOT NULL,
    [ApplicationID]     INT              NOT NULL,
    [BusinessEntityID]  BIGINT           NULL,
    [GroupID]           INT              NOT NULL,
    [Description]       VARCHAR (1000)   NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_GroupAllocation_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_GroupAllocation_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_GroupAllocation_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_GroupAllocation] PRIMARY KEY CLUSTERED ([GroupAllocationID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_GroupAllocation_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_GroupAllocation_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_GroupAllocation_Groups] FOREIGN KEY ([GroupID]) REFERENCES [acl].[Groups] ([GroupID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_GroupAllocation_AppBizGroup]
    ON [acl].[GroupAllocation]([ApplicationID] ASC, [BusinessEntityID] ASC, [GroupID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_GroupAllocation_rowguid]
    ON [acl].[GroupAllocation]([rowguid] ASC) WITH (FILLFACTOR = 90);

