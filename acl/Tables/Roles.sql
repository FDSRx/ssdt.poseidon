﻿CREATE TABLE [acl].[Roles] (
    [RoleID]       INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Roles_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Roles_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Roles_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([RoleID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Roles_Code]
    ON [acl].[Roles]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Roles_Name]
    ON [acl].[Roles]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Roles_rowguid]
    ON [acl].[Roles]([rowguid] ASC);

