﻿CREATE TABLE [acl].[AccessInspectionType] (
    [AccessInspectionTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]                   VARCHAR (25)     NOT NULL,
    [Name]                   VARCHAR (50)     NOT NULL,
    [Description]            VARCHAR (1000)   NULL,
    [rowguid]                UNIQUEIDENTIFIER CONSTRAINT [DF_AccessInspectionType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_AccessInspectionType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]           DATETIME         CONSTRAINT [DF_AccessInspectionType_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_AccessInspectionType] PRIMARY KEY CLUSTERED ([AccessInspectionTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AccessInspectionType_Coe]
    ON [acl].[AccessInspectionType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AccessInspectionType_Name]
    ON [acl].[AccessInspectionType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AccessInspectionType_rowguid]
    ON [acl].[AccessInspectionType]([rowguid] ASC);

