﻿CREATE TABLE [acl].[Groups] (
    [GroupID]      INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Groups_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Groups_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Groups_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED ([GroupID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Groups_Code]
    ON [acl].[Groups]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Groups_Name]
    ON [acl].[Groups]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Groups_rowguid]
    ON [acl].[Groups]([rowguid] ASC);

