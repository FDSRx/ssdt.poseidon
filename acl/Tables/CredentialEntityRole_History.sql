﻿CREATE TABLE [acl].[CredentialEntityRole_History] (
    [CredentialEntityRoleHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CredentialEntityRoleID]        BIGINT           NOT NULL,
    [CredentialEntityID]            BIGINT           NOT NULL,
    [ApplicationID]                 INT              NULL,
    [BusinessEntityID]              BIGINT           NULL,
    [RoleID]                        INT              NOT NULL,
    [rowguid]                       UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]                   DATETIME         NOT NULL,
    [DateModified]                  DATETIME         CONSTRAINT [DF_CredentialEntityRole_History_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                     VARCHAR (256)    NULL,
    [ModifiedBy]                    VARCHAR (256)    NULL,
    [AuditGuid]                     UNIQUEIDENTIFIER CONSTRAINT [DF_CredentialEntityRole_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                   VARCHAR (10)     NOT NULL,
    [DateAudited]                   DATETIME         CONSTRAINT [DF_CredentialEntityRole_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CredentialEntityRole_History] PRIMARY KEY CLUSTERED ([CredentialEntityRoleHistoryID] ASC)
);

