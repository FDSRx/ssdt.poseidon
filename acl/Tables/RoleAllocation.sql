﻿CREATE TABLE [acl].[RoleAllocation] (
    [RoleAllocationID] INT              IDENTITY (1, 1) NOT NULL,
    [ApplicationID]    INT              NOT NULL,
    [BusinessEntityID] BIGINT           NULL,
    [RoleID]           INT              NOT NULL,
    [Description]      VARCHAR (1000)   NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_RoleAllocation_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_RoleAllocation_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_RoleAllocation_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_RoleAllocation] PRIMARY KEY CLUSTERED ([RoleAllocationID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_RoleAllocation_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_RoleAllocation_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_RoleAllocation_Roles] FOREIGN KEY ([RoleID]) REFERENCES [acl].[Roles] ([RoleID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_RoleAllocation_AppBizRole]
    ON [acl].[RoleAllocation]([ApplicationID] ASC, [BusinessEntityID] ASC, [RoleID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_RoleAllocation_rowguid]
    ON [acl].[RoleAllocation]([rowguid] ASC) WITH (FILLFACTOR = 90);

