﻿CREATE TABLE [acl].[Permission] (
    [PermissionID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Permissions_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Permissions_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Permissions_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED ([PermissionID] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Permissions_Code]
    ON [acl].[Permission]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Permissions_Name]
    ON [acl].[Permission]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Permissions_rowguid]
    ON [acl].[Permission]([rowguid] ASC);

