﻿CREATE VIEW [acl].[vwAccessControl]
AS
SELECT     ctrl.AccessControlID, ctrl.ApplicationID, app.Code AS ApplicationCode, app.Name AS ApplicationName, ctrl.BusinessEntityID, 
                      bizenttyp.Code AS BusinessEntityTypeCode, bizenttyp.Name AS BusinessEntityTypeName, ctrl.AccessInspectionTypeID, ins.Code AS AccessInspectionTypeCode, 
                      ins.Name AS AccessInspectionTypeName, ctrl.AccessFilterTypeID, filtyp.Code AS AccessFilterTypeCode, filtyp.Name AS AccessFilterTypeName, ctrl.Filter, 
                      ctrl.AccessDirectiveID, dir.Code AS AccessDirectiveCode, dir.Name AS AccessDirectiveName, ctrl.SortOrder, ctrl.rowguid, ctrl.DateCreated, ctrl.DateModified, 
                      ctrl.CreatedBy, ctrl.ModifiedBy
FROM         acl.AccessControl AS ctrl LEFT OUTER JOIN
                      dbo.Application AS app ON ctrl.ApplicationID = app.ApplicationID LEFT OUTER JOIN
                      dbo.BusinessEntity AS bizent ON ctrl.BusinessEntityID = bizent.BusinessEntityID LEFT OUTER JOIN
                      dbo.BusinessEntityType AS bizenttyp ON bizent.BusinessEntityTypeID = bizenttyp.BusinessEntityTypeID LEFT OUTER JOIN
                      acl.AccessInspectionType AS ins ON ctrl.AccessInspectionTypeID = ins.AccessInspectionTypeID LEFT OUTER JOIN
                      acl.AccessFilterType AS filtyp ON ctrl.AccessFilterTypeID = filtyp.AccessFilterTypeID LEFT OUTER JOIN
                      acl.AccessDirective AS dir ON ctrl.AccessDirectiveID = dir.AccessDirectiveID

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ctrl"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 245
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "app"
            Begin Extent = 
               Top = 6
               Left = 283
               Bottom = 125
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "bizent"
            Begin Extent = 
               Top = 6
               Left = 497
               Bottom = 125
               Right = 690
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "bizenttyp"
            Begin Extent = 
               Top = 6
               Left = 728
               Bottom = 125
               Right = 921
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ins"
            Begin Extent = 
               Top = 6
               Left = 959
               Bottom = 125
               Right = 1166
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "filtyp"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dir"
            Begin Extent = 
               Top = 126
               Left = 257
               Bottom = 245
               Right = 432
            End
            DisplayFlags = 280
            TopCol', @level0type = N'SCHEMA', @level0name = N'acl', @level1type = N'VIEW', @level1name = N'vwAccessControl';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'umn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'acl', @level1type = N'VIEW', @level1name = N'vwAccessControl';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'acl', @level1type = N'VIEW', @level1name = N'vwAccessControl';

