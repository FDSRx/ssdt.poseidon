﻿CREATE VIEW [acl].vwRolePermission
AS
SELECT     rp.RolePermissionID, app.ApplicationID, app.Name AS ApplicationName, biz.BusinessEntityID, stre.Name AS BusinessName, rle.RoleID, rle.Code AS RoleCode, 
                      rle.Name AS RoleName, rle.Description AS RoleDescription, p.PermissionID, p.Code AS PermissionCode, p.Name AS PermissionName, 
                      p.Description AS PermissionDescription
FROM         acl.RolePermission AS rp INNER JOIN
                      dbo.Application AS app ON rp.ApplicationID = app.ApplicationID LEFT OUTER JOIN
                      dbo.BusinessEntity AS biz ON rp.BusinessEntityID = biz.BusinessEntityID LEFT OUTER JOIN
                      dbo.Store AS stre ON biz.BusinessEntityID = stre.BusinessEntityID INNER JOIN
                      acl.Roles AS rle ON rp.RoleID = rle.RoleID INNER JOIN
                      acl.Permission AS p ON rp.PermissionID = p.PermissionID

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "rp"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "app"
            Begin Extent = 
               Top = 6
               Left = 247
               Bottom = 125
               Right = 423
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "biz"
            Begin Extent = 
               Top = 6
               Left = 461
               Bottom = 125
               Right = 654
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "stre"
            Begin Extent = 
               Top = 6
               Left = 692
               Bottom = 125
               Right = 861
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "rle"
            Begin Extent = 
               Top = 6
               Left = 899
               Bottom = 125
               Right = 1059
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 1097
               Bottom = 125
               Right = 1257
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Wid', @level0type = N'SCHEMA', @level0name = N'acl', @level1type = N'VIEW', @level1name = N'vwRolePermission';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'th = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'acl', @level1type = N'VIEW', @level1name = N'vwRolePermission';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'acl', @level1type = N'VIEW', @level1name = N'vwRolePermission';

