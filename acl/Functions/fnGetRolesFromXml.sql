﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/25/2015
-- Description:	Parses Role objects from the provided XML data.
-- SAMPLE CALL:
/*
DECLARE @Xml XML = '
<Roles>
  <Role>
    <RoleID>1</RoleID>
    <Code>ADMIN</Code>
    <Name>Administrator</Name>
    <Description>Complete access to the application and all its settings</Description>
    <Permisssions>
      <Permission>
        <PermissionID>2</PermissionID>
        <Code>R</Code>
        <Name>Read</Name>
      </Permission>
      <Permission>
        <PermissionID>3</PermissionID>
        <Code>W</Code>
        <Name>Write</Name>
      </Permission>
      <Permission>
        <PermissionID>4</PermissionID>
        <Code>D</Code>
        <Name>Delete</Name>
      </Permission>
      <Permission>
        <PermissionID>5</PermissionID>
        <Code>U</Code>
        <Name>Update</Name>
      </Permission>
    </Permisssions>
  </Role>
  <Role>
    <RoleID>5</RoleID>
    <Code>MPCS</Code>
    <Name>mPC Support</Name>
    <Description>User can access mPC specific functionality within the application</Description>
  </Role>
  <Role>
    <RoleID>6</RoleID>
    <Code>TIXTECH</Code>
    <Name>Ticket Technician</Name>
    <Description>User can access ticketing support records as a tech within the application</Description>
  </Role>
  <Role>
    <RoleID>7</RoleID>
    <Code>TIXADM</Code>
    <Name>Ticket Administrator</Name>
    <Description>User can access ticketing support records as an admin within the application</Description>
  </Role>
</Roles>'

SELECT *
FROM acl.fnGetRolesFromXml(@Xml);

*/

-- SELECT * FROM acl.CredentialEntityRole
-- =============================================
CREATE FUNCTION [acl].[fnGetRolesFromXml]
(	
	@Xml XML
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT
		ISNULL(objs.value('data(RoleID)[1]', 'INT'), objs.value('data(Id)[1]', 'INT')) AS Id,
		objs.value('data(Code)[1]', 'VARCHAR(50)') AS Code,
		objs.value('data(Name)[1]', 'VARCHAR(256)') AS Name,
		objs.value('data(Description)[1]', 'VARCHAR(1000)') AS Description
	FROM @Xml.nodes('*:Roles/*:Role') AS obj(objs)
)
