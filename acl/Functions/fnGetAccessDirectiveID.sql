﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/13/2014
-- Description:	Gets the access directive ID
-- SAMPLE CALL: SELECT acl.fnGetAccessDirectiveID('A')
-- SAMPLE CALL: SELECT acl.fnGetAccessDirectiveID(1)
-- =============================================
CREATE FUNCTION acl.[fnGetAccessDirectiveID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT AccessDirectiveID FROM acl.AccessDirective WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT AccessDirectiveID FROM acl.AccessDirective WHERE AccessDirectiveID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

