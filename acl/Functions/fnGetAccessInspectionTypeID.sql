﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/13/2014
-- Description:	Gets the access inspection type ID
-- SAMPLE CALL: SELECT acl.fnGetAccessInspectionTypeID('HOST')
-- SAMPLE CALL: SELECT acl.fnGetAccessInspectionTypeID(1)
-- =============================================
CREATE FUNCTION acl.[fnGetAccessInspectionTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT AccessInspectionTypeID FROM acl.AccessInspectionType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT AccessInspectionTypeID FROM acl.AccessInspectionType WHERE AccessInspectionTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

