﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/10/2014
-- Description: Gets the translated Role key or keys.
-- References: dbo.fnSlice
-- References: dbo.fnSplit

-- SAMPLE CALL: SELECT acl.fnRoleKeyTranslator(1, 'RoleID')
-- SAMPLE CALL: SELECT acl.fnRoleKeyTranslator('DEV', DEFAULT)
-- SAMPLE CALL: SELECT acl.fnRoleKeyTranslator(3, DEFAULT)
-- SAMPLE CALL: SELECT acl.fnRoleKeyTranslator('1,2,3', 'RoleIDList')
-- SAMPLE CALL: SELECT acl.fnRoleKeyTranslator('TIXTECH,LOYADM,MPCR', 'CodeList')
-- SAMPLE CALL: SELECT acl.fnRoleKeyTranslator('MPCR,ADD,DELETE', DEFAULT)
-- SAMPLE CALL: SELECT acl.fnRoleKeyTranslator('1,2,3', DEFAULT)

-- SELECT * FROM acl.Roles
-- =============================================
CREATE FUNCTION acl.[fnRoleKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END		
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'RoleID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, RoleID) 
				FROM acl.Roles 
				WHERE RoleID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, RoleID) 
				FROM acl.Roles 
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, RoleID) 
				FROM acl.Roles 
				WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, RoleID) 
				FROM acl.Roles 
				WHERE RoleID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, RoleID) 
				FROM acl.Roles 
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'RoleIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, RoleID), '') + ','
				FROM acl.Roles
				WHERE RoleID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'RoleCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, RoleID), '') + ','
				FROM acl.Roles
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'RoleNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, RoleID), '') + ','
				FROM acl.Roles
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, RoleID), '') + ','
				FROM acl.Roles
				WHERE RoleID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, RoleID), '') + ','
				FROM acl.Roles
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
