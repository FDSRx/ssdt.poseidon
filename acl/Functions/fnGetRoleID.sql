﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/2/2014
-- Description:	Gets the RoleID
-- SAMPLE CALL: SELECT acl.fnGetRoleID('ADMIN')
-- SAMPLE CALL: SELECT acl.fnGetRoleID(7)

-- SELECT * FROM acl.Roles
-- =============================================
CREATE FUNCTION [acl].fnGetRoleID 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT RoleID FROM acl.Roles WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT RoleID FROM acl.Roles WHERE RoleID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

