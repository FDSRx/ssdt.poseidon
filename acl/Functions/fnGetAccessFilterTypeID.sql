﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/13/2014
-- Description:	Gets the access filter type ID
-- SAMPLE CALL: SELECT acl.fnGetAccessFilterTypeID('IPADDR')
-- SAMPLE CALL: SELECT acl.fnGetAccessFilterTypeID(1)
-- =============================================
CREATE FUNCTION acl.[fnGetAccessFilterTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT AccessFilterTypeID FROM acl.AccessFilterType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT AccessFilterTypeID FROM acl.AccessFilterType WHERE AccessFilterTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

