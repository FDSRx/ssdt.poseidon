﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/13/2014
-- Description:	Creates a new Access Control entry.
-- =============================================
CREATE PROCEDURE acl.spAccessControl_Create
	@ApplicationID INT,
	@BusinessEntityID BIGINT = NULL,
	@AccessInspectionTypeID INT,
	@AccessFilterTypeID INT,
	@Filter VARCHAR(500),
	@AccessDirectiveID INT,
	@SortOrder INT = 0,
	@CreatedBy VARCHAR(256) = NULL,
	@AccessControlID INT = NULL	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --------------------------------------------------------
    -- Sanitize input
    --------------------------------------------------------
    SET @AccessControlID = NULL;
    SET @CreatedBy = ISNULL(@CreatedBy, 'System\Administrator');
    SET @SortOrder = ISNULL(@SortOrder, 0);
    
    --------------------------------------------------------
    -- Create entry
    -- <Summary>
    -- Creates a new access control entry.
    -- </Summary>
    --------------------------------------------------------
    INSERT INTO acl.AccessControl (
		ApplicationID,
		BusinessEntityID,
		AccessInspectionTypeID,
		AccessFilterTypeID,
		Filter,
		AccessDirectiveID,
		SortOrder,
		CreatedBy
	)
	SELECT
		@ApplicationID,
		@BusinessEntityID,
		@AccessInspectionTypeID,
		@AccessFilterTypeID,
		@Filter,
		@AccessDirectiveID,
		@SortOrder,
		@CreatedBy
	
	
	-- Return record identity
	SET @AccessControlID = SCOPE_IDENTITY();

END
