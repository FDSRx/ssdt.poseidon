﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/2/2014
-- Description:	Creates a new set of CredentialEntityRole objects.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityRoleIDs VARCHAR(MAX) = NULL
	
EXEC acl.spCredentialEntityRole_AddRange
	@CredentialEntityID = 959790,
	@BusinessEntityID = 3759,
	@ApplicationID = 8,
	@RoleID = '10,11,12',
	@CredentialEntityRoleIDs = @CredentialEntityRoleIDs OUTPUT

SELECT @CredentialEntityRoleIDs AS CredentialEntityRoleIDs

*/

-- SELECT * FROM acl.Roles
-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM acl.CredentialEntityRole
-- =============================================
CREATE PROCEDURE [acl].[spCredentialEntityRole_AddRange]
	@CredentialEntityID BIGINT,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@RoleID VARCHAR(MAX),
	@CreatedBy VARCHAR(256) = NULL,
	@CredentialEntityRoleIDs VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------
	DECLARE @Trancount INT = @@TRANCOUNT;
	
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CredentialEntityRoleIDs = NULL;
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- CredentialEntityID
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Role object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- RoleIDs
	IF dbo.fnIsNullOrWhiteSpace(@RoleID) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to create Role object. Object reference (@RoleID) is not set to an instance of an object. ' +
			'The parameter, @RoleID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	


	BEGIN TRY
	--------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		-------------------------------------------------------------------------------
		-- Temporary objects.
		-------------------------------------------------------------------------------
		DECLARE @tblOutput AS TABLE (
			Idx INT IDENTITY(1,1),
			CredentialEntityRoleID BIGINT
		);
		
				
		-------------------------------------------------------------------------------
		-- Create new CredentialEntityRole object.
		-------------------------------------------------------------------------------
		INSERT INTO acl.CredentialEntityRole (
			ApplicationID,
			BusinessEntityID,
			CredentialEntityID,
			RoleID,
			CreatedBy
		)
		OUTPUT inserted.CredentialEntityRoleID INTO @tblOutput(CredentialEntityRoleID)
		SELECT
			@ApplicationID AS ApplicationID,
			@BusinessEntityID AS BusinessEntityID,
			@CredentialEntityID AS CredentialEntityID,
			Value AS RoleID,
			@CreatedBy
		FROM dbo.fnSplit(@RoleID, ',')
		WHERE ISNUMERIC(Value) = 1
		
		-------------------------------------------------------------------------------
		-- Retrieve record identity value(s).
		-------------------------------------------------------------------------------
		SET @CredentialEntityRoleIDs = (
			SELECT 
				CONVERT(VARCHAR(25), CredentialEntityRoleID) + ','
			FROM @tblOutput
			FOR XML PATH('')
		);
		
		IF RIGHT(@CredentialEntityRoleIDs, 1) = ','
		BEGIN
			SET @CredentialEntityRoleIDs = LEFT(@CredentialEntityRoleIDs, LEN(@CredentialEntityRoleIDs) - 1);
		END

	--------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END		
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);	
	
	END CATCH
	
		
END
