﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/8/2013
-- Description:	Gets the roles that are applicable to a user.
-- SAMPLE CALL:
/*
-- Test result set	
EXEC acl.spCredentialEntityRole_Get_List
	@CredentialEntityID = 801018,
	@ApplicationID = 2,
	@BusinessEntityID = 92

-- Test xml result set	
EXEC acl.spCredentialEntityRole_Get_List
	@CredentialEntityID = 801018,
	@ApplicationID = 2,
	@BusinessEntityID = 92,
	@IsResultXml = 1

-- Test xml output	
DECLARE @RoleData XML
EXEC acl.spCredentialEntityRole_Get_List
	@CredentialEntityID = 801018,
	@ApplicationID = 2,
	@BusinessEntityID = 92,
	@IsResultXml = 1,
	@IsResultXmlOutputOnly = 1,
	@RoleData = @RoleData OUTPUT
SELECT @RoleData AS RoleData

*/

-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM acl.Roles
-- SELECT * FROM acl.CredentialEntityRole WHERE CredentialEntityID = '959773'
-- =============================================
CREATE PROCEDURE [acl].[spCredentialEntityRole_Get_List]
	@CredentialEntityID BIGINT,
	@ApplicationID INT = NULL,
	@BusinessEntityID INT = NULL,
	@IsResultXml BIT = 0,
	@IsResultXmlOutputOnly BIT = 0,
	@RoleData XML = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------
	-- Sanitize input
	----------------------------------------------------
	SET @IsResultXml = ISNULL(@IsResultXml, 0);
	SET @IsResultXmlOutputOnly = ISNULL(@IsResultXmlOutputOnly, 0);
	SET @RoleData = NULL;

	----------------------------------------------------
	-- Local variables
	----------------------------------------------------
	DECLARE
		@SentinelInt INT = -1999999999
	
	----------------------------------------------------
	-- Create temporary resources
	----------------------------------------------------
	-- Create credential role master listing
	IF OBJECT_ID('tempdb..#tmpCredentialRole') IS NOT NULL
	BEGIN
		DROP TABLE #tmpCredentialRole;
	END
	
	CREATE TABLE #tmpCredentialRole (
		CredentialEntityID BIGINT,
		RoleID INT,
		Code VARCHAR(25),
		Name VARCHAR(50),
		Description VARCHAR(1000),
		PermissionData XML,
		GroupData XML
	)
	
	-- Create role listing
	IF OBJECT_ID('tempdb..#tmpRoleIDList') IS NOT NULL
	BEGIN
		DROP TABLE #tmpRoleIDList;
	END
	
	CREATE TABLE #tmpRoleIDList (
		Idx INT IDENTITY(1,1),
		ApplicationID INT,
		BusinessEntityID INT,
		RoleID INT
	)
	
	----------------------------------------------------
	-- Fetch list of applicable roles
	-- <Summary>
	-- Obtains a list of applicable RoleIDs from both a 
	-- credentials groups and roles listing
	-- </Summary>	
	----------------------------------------------------
	
	----------------------------------------------------
	-- Obtain roles that have been applied to groups.
	-- <Summary>
	-- Uses a bottom-up pattern to find all applicable roles.
	-- 1. Credential, Business, Application
	-- 2. Credential, Business
	-- 3. Credential, Application
	-- 4. Credential
	-- </Summary>
	----------------------------------------------------
	-- Credential/Business/Application
	INSERT INTO #tmpRoleIDList (		
		ApplicationID,
		BusinessEntityID,
		RoleID
	)
	SELECT DISTINCT
		credgrp.ApplicationID,
		credgrp.BusinessEntityID,
		grprole.RoleID
	--SELECT *
	FROM acl.vwCredentialEntityGroup credgrp
		JOIN acl.vwGroupRole grprole
			ON credgrp.GroupID = grprole.GroupID
	WHERE credgrp.CredentialEntityID = @CredentialEntityID
		AND credgrp.ApplicationID = @ApplicationID
		AND credgrp.BusinessEntityID = @BusinessEntityID
	
	-- Credential/Application
	INSERT INTO #tmpRoleIDList (		
		ApplicationID,
		BusinessEntityID,
		RoleID
	)
	SELECT DISTINCT
		credgrp.ApplicationID,
		credgrp.BusinessEntityID,
		grprole.RoleID
	--SELECT *
	FROM acl.vwCredentialEntityGroup credgrp
		JOIN acl.vwGroupRole grprole
			ON credgrp.GroupID = grprole.GroupID
	WHERE credgrp.CredentialEntityID = @CredentialEntityID
		AND credgrp.ApplicationID = @ApplicationID
		AND credgrp.BusinessEntityID IS NULL
		AND NOT EXISTS (
			SELECT TOP(1) RoleID
			FROM #tmpRoleIDList virt
			WHERE virt.RoleID = grprole.RoleID
				AND ISNULL(virt.ApplicationID, @SentinelInt)  = ISNULL(grprole.ApplicationID, @SentinelInt)
				AND ISNULL(virt.BusinessEntityID, @SentinelInt) = ISNULL(grprole.BusinessEntityID, @SentinelInt)
		)
	
	-- Credential/Business
	INSERT INTO #tmpRoleIDList (		
		ApplicationID,
		BusinessEntityID,
		RoleID
	)
	SELECT DISTINCT
		credgrp.ApplicationID,
		credgrp.BusinessEntityID,
		grprole.RoleID
	--SELECT *
	FROM acl.vwCredentialEntityGroup credgrp
		JOIN acl.vwGroupRole grprole
			ON credgrp.GroupID = grprole.GroupID
	WHERE credgrp.CredentialEntityID = @CredentialEntityID
		AND credgrp.ApplicationID IS NULL
		AND credgrp.BusinessEntityID = @BusinessEntityID
		AND NOT EXISTS (
			SELECT TOP(1) RoleID
			FROM #tmpRoleIDList virt
			WHERE virt.RoleID = grprole.RoleID
				AND ISNULL(virt.ApplicationID, @SentinelInt)  = ISNULL(grprole.ApplicationID, @SentinelInt)
				AND ISNULL(virt.BusinessEntityID, @SentinelInt) = ISNULL(grprole.BusinessEntityID, @SentinelInt)
		)
	
	-- Credential
	INSERT INTO #tmpRoleIDList (		
		ApplicationID,
		BusinessEntityID,
		RoleID
	)
	SELECT DISTINCT
		credgrp.ApplicationID,
		credgrp.BusinessEntityID,
		grprole.RoleID
	--SELECT *
	FROM acl.vwCredentialEntityGroup credgrp
		JOIN acl.vwGroupRole grprole
			ON credgrp.GroupID = grprole.GroupID
	WHERE credgrp.CredentialEntityID = @CredentialEntityID
		AND credgrp.ApplicationID IS NULL
		AND credgrp.BusinessEntityID IS NULL
		AND NOT EXISTS (
			SELECT TOP(1) RoleID
			FROM #tmpRoleIDList virt
			WHERE virt.RoleID = grprole.RoleID
				AND ISNULL(virt.ApplicationID, @SentinelInt)  = ISNULL(grprole.ApplicationID, @SentinelInt)
				AND ISNULL(virt.BusinessEntityID, @SentinelInt) = ISNULL(grprole.BusinessEntityID, @SentinelInt)
		)	

	----------------------------------------------------	
	-- Obtain roles directly from the credential role listing.	
	-- <Summary>
	-- Uses a bottom-up pattern to find all applicable roles.
	-- 1. Credential, Business, Application
	-- 2. Credential, Business
	-- 3. Credential, Application
	-- 4. Credential
	-- </Summary>
	----------------------------------------------------	
	-- Credential/Application/Business
	INSERT INTO #tmpRoleIDList (
		ApplicationID,
		BusinessEntityID,
		RoleID
	)
	SELECT DISTINCT
		r.ApplicationID,
		r.BusinessEntityID,
		r.RoleID
	--SELECT *
	FROM acl.vwCredentialEntityRole r
	WHERE r.CredentialEntityID = @CredentialEntityID
		AND r.ApplicationID = @ApplicationID
		AND r.BusinessEntityID = @BusinessEntityID
		AND NOT EXISTS (
			SELECT TOP(1) RoleID
			FROM #tmpRoleIDList virt
			WHERE virt.RoleID = r.RoleID
				AND ISNULL(virt.ApplicationID, @SentinelInt)  = ISNULL(r.ApplicationID, @SentinelInt)
				AND ISNULL(virt.BusinessEntityID, @SentinelInt) = ISNULL(r.BusinessEntityID, @SentinelInt)
		)	
	
	-- Credential/Application
	INSERT INTO #tmpRoleIDList (
		ApplicationID,
		BusinessEntityID,
		RoleID
	)
	SELECT DISTINCT
		r.ApplicationID,
		r.BusinessEntityID,
		r.RoleID
	--SELECT *
	FROM acl.vwCredentialEntityRole r
	WHERE r.CredentialEntityID = @CredentialEntityID
		AND r.ApplicationID = @ApplicationID
		AND r.BusinessEntityID IS NULL
		AND NOT EXISTS (
			SELECT TOP(1) RoleID
			FROM #tmpRoleIDList virt
			WHERE virt.RoleID = r.RoleID
				AND ISNULL(virt.ApplicationID, @SentinelInt)  = ISNULL(r.ApplicationID, @SentinelInt)
				AND ISNULL(virt.BusinessEntityID, @SentinelInt) = ISNULL(r.BusinessEntityID, @SentinelInt)
		)
	
	-- Credential/Business
	INSERT INTO #tmpRoleIDList (
		ApplicationID,
		BusinessEntityID,
		RoleID
	)
	SELECT DISTINCT
		r.ApplicationID,
		r.BusinessEntityID,
		r.RoleID
	--SELECT *
	FROM acl.vwCredentialEntityRole r
	WHERE r.CredentialEntityID = @CredentialEntityID
		AND r.ApplicationID IS NULL
		AND r.BusinessEntityID = @BusinessEntityID
		AND NOT EXISTS (
			SELECT TOP(1) RoleID
			FROM #tmpRoleIDList virt
			WHERE virt.RoleID = r.RoleID
				AND ISNULL(virt.ApplicationID, @SentinelInt)  = ISNULL(r.ApplicationID, @SentinelInt)
				AND ISNULL(virt.BusinessEntityID, @SentinelInt) = ISNULL(r.BusinessEntityID, @SentinelInt)
		)	
	
	-- Credential
	INSERT INTO #tmpRoleIDList (
		ApplicationID,
		BusinessEntityID,
		RoleID
	)
	SELECT DISTINCT
		r.ApplicationID,
		r.BusinessEntityID,
		r.RoleID
	--SELECT *
	FROM acl.vwCredentialEntityRole r
	WHERE r.CredentialEntityID = @CredentialEntityID
		AND r.ApplicationID IS NULL
		AND r.BusinessEntityID IS NULL
		AND NOT EXISTS (
			SELECT TOP(1) RoleID
			FROM #tmpRoleIDList virt
			WHERE virt.RoleID = r.RoleID
				AND ISNULL(virt.ApplicationID, @SentinelInt)  = ISNULL(r.ApplicationID, @SentinelInt)
				AND ISNULL(virt.BusinessEntityID, @SentinelInt) = ISNULL(r.BusinessEntityID, @SentinelInt)
		)		
	
	
	
	----------------------------------------------------
	-- Fetch data
	-- <Summary>
	-- Fetches data from both the groups and roles table
	-- based on the supplied credential entity id.
	-- </Summary>
	----------------------------------------------------
	INSERT INTO #tmpCredentialRole (
		CredentialEntityID,
		RoleID,
		Code,
		Name,
		Description,
		PermissionData,
		GroupData
	)
	SELECT
		@CredentialEntityID AS CredentialEntityID,
		rle.RoleID,
		rle.Code AS Code,
		rle.Name AS Name,
		rle.Description AS Description, 
		(
			SELECT
				rp.PermissionID,
				rp.PermissionCode AS Code,
				rp.PermissionName AS Name,
				rp.PermissionDescription AS Description
			FROM acl.vwRolePermission rp
			WHERE rp.ApplicationID = tmp.ApplicationID
						AND rp.BusinessEntityID = tmp.BusinessEntityID
						AND rp.RoleID = rle.RoleID
			FOR XML PATH ('Permission')
		) AS PermissionData,
		(
			SELECT DISTINCT
				gr.GroupID,
				gr.GroupCode AS Code,
				gr.GroupName AS Name,
				gr.GroupDescription AS Description
			FROM acl.vwCredentialEntityGroup credgrp
				JOIN acl.vwGroupRole gr
					ON credgrp.ApplicationID = gr.ApplicationID
						AND credgrp.BusinessEntityID = gr.BusinessEntityID
						AND credgrp.GroupID = gr.GroupID
			WHERE credgrp.CredentialEntityID = @CredentialEntityID
				AND gr.RoleID = tmp.RoleID
			FOR XML PATH ('Group')
		) AS GroupData
	--SELECT *
	FROM #tmpRoleIDList tmp
		JOIN acl.Roles rle
			ON tmp.RoleID = rle.RoleID
	
	
	----------------------------------------------------
	-- Determine if data needs to be returned as XML format
	-- <Summary>
	-- Returns or outputs the data in XML format
	-- </Summary>
	----------------------------------------------------
	IF @IsResultXml = 1 OR @IsResultXmlOutputOnly = 1
	BEGIN
		SET @RoleData = (SELECT CONVERT(XML, (
			SELECT
				RoleID,
				Code,
				Name,
				Description,
				PermissionData AS [Permisssions],
				GroupData AS Groups
			FROM #tmpCredentialRole
			FOR XML PATH('Role'), ROOT('Roles')
		)));
		
		IF @IsResultXmlOutputOnly = 0
		BEGIN
			SELECT @RoleData AS Roles
		END
	END
	ELSE
	BEGIN
		
		----------------------------------------------------
		-- Return the roles result set
		----------------------------------------------------
		SELECT
			RoleID,
			Code,
			Name,
			Description
		FROM #tmpCredentialRole
	
	END
	
	
	----------------------------------------------------
	-- Release resources
	----------------------------------------------------
	DROP TABLE #tmpCredentialRole;
	DROP TABLE #tmpRoleIDList;
	
	
END
