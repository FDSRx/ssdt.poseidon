﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/2/2014
-- Description:	Creates a new set of CredentialEntityRole objects. The process will not fail if an object already exists.
--		The process will ignore the existing object and add the objects that do not exist.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityRoleIDs VARCHAR(MAX) = NULL
	
EXEC acl.spCredentialEntityRole_MergeRange
	@CredentialEntityID = 959790,
	@BusinessEntityID = 3759,
	@ApplicationID = 8,
	@RoleID = '10,11,12,13',
	@CredentialEntityRoleIDs = @CredentialEntityRoleIDs OUTPUT,
	@CreatedBy = 'dhughes',
	@ModifiedBy = 'dhughes',
	@DeleteUnspecified = 1

SELECT @CredentialEntityRoleIDs AS CredentialEntityRoleIDs

*/

-- SELECT * FROM acl.Roles
-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM acl.CredentialEntityRole
-- =============================================
CREATE PROCEDURE [acl].[spCredentialEntityRole_MergeRange]
	@CredentialEntityID BIGINT,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@RoleID VARCHAR(MAX),
	@CredentialEntityRoleIDs VARCHAR(MAX) = NULL OUTPUT,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@DeleteUnspecified BIT = 0 -- Removes items that are not specified in the range.
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------
	DECLARE @Trancount INT = @@TRANCOUNT;
	
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CredentialEntityRoleIDs = NULL;
	SET @DeleteUnspecified = ISNULL(@DeleteUnspecified, 0);
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- CredentialEntityID
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Role object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	/*
	-- RoleIDs
	IF dbo.fnIsNullOrWhiteSpace(@RoleID) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to create Role object. Object reference (@RoleID) is not set to an instance of an object. ' +
			'The parameter, @RoleID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/


	BEGIN TRY
	--------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		-------------------------------------------------------------------------------
		-- Temporary objects.
		-------------------------------------------------------------------------------
		DECLARE @tblOutput AS TABLE (
			Idx INT IDENTITY(1,1),
			CredentialEntityRoleID BIGINT
		);		

		-------------------------------------------------------------------------------
		-- Store ModifiedBy property in "session" like object.
		-------------------------------------------------------------------------------
		EXEC memory.spContextInfo_TriggerData_Set
			@ObjectName = @ProcedureName,
			@DataString = @ModifiedBy
		
		-------------------------------------------------------------------------------
		-- Remove unspecified items (if applicable).
		-- <Summary>
		-- Removes items that are not specified in the range of items.
		-- </Summary>
		-------------------------------------------------------------------------------
		IF @DeleteUnspecified = 1
		BEGIN
			DELETE cr
			FROM acl.CredentialEntityRole cr
			WHERE CredentialEntityID = @CredentialEntityID
				AND ( @ApplicationID IS NULL OR ApplicationID = @ApplicationID )
				AND ( @BusinessEntityID IS NULL OR BusinessEntityID = @BusinessEntityID )
				AND ( dbo.fnIsNullOrWhiteSpace(@RoleID) = 1 
					OR RoleID NOT IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@RoleID, ',')) )
		END
		
						
		-------------------------------------------------------------------------------
		-- Create new CredentialEntityRole object.
		-------------------------------------------------------------------------------
		INSERT INTO acl.CredentialEntityRole (
			ApplicationID,
			BusinessEntityID,
			CredentialEntityID,
			RoleID,
			CreatedBy
		)
		OUTPUT inserted.CredentialEntityRoleID INTO @tblOutput(CredentialEntityRoleID)
		SELECT
			@ApplicationID AS ApplicationID,
			@BusinessEntityID AS BusinessEntityID,
			@CredentialEntityID AS CredentialEntityID,
			Value AS RoleID,
			@CreatedBy
		FROM dbo.fnSplit(@RoleID, ',') r
			LEFT JOIN acl.CredentialEntityRole cr
				ON ( @ApplicationID IS NULL OR cr.ApplicationID = @ApplicationID )
					AND ( @BusinessEntityID IS NULL OR cr.BusinessEntityID = @BusinessEntityID )
					AND cr.CredentialEntityID = @CredentialEntityID
					AND cr.RoleID = CASE WHEN ISNUMERIC(r.Value) = 1 THEN r.Value ELSE NULL END
		WHERE ISNUMERIC(Value) = 1
			AND cr.CredentialEntityRoleID IS NULL
		
		-------------------------------------------------------------------------------
		-- Retrieve record identity value(s).
		-------------------------------------------------------------------------------
		SET @CredentialEntityRoleIDs = (
			SELECT 
				CONVERT(VARCHAR(25), CredentialEntityRoleID) + ','
			FROM @tblOutput
			ORDER BY CredentialEntityRoleID ASC
			FOR XML PATH('')
		);
		
		IF RIGHT(@CredentialEntityRoleIDs, 1) = ','
		BEGIN
			SET @CredentialEntityRoleIDs = LEFT(@CredentialEntityRoleIDs, LEN(@CredentialEntityRoleIDs) - 1);
		END

	--------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END		
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);	
	
	END CATCH
	
		
END
