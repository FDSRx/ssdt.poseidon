﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/31/2014
-- Description:	Indicates whether the credential entity is in the supplied role(s)
-- SAMPLE CALL:
/*
DECLARE 
	@CredentialEntityID BIGINT = 1, 
	@ApplicationID INT = 2, 
	@BusinessEntityID BIGINT = 92,
	@RoleData XML,
	@Exists BIT

EXEC acl.spCredentialEntity_IsInRole
	@CredentialEntityID = @CredentialEntityID,
	@Role = 'MPCS',
	@ApplicationID = @ApplicationID,
	@BusinessEntityID = @BusinessEntityID,
	@Exists = @Exists OUTPUT

SELECT @Exists AS IsFound
*/
-- =============================================
CREATE PROCEDURE [acl].[spCredentialEntity_IsInRole]
	@CredentialEntityID BIGINT,
	@Role VARCHAR(MAX),
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------
	-- Sanitize input
	----------------------------------------------------
	SET @Exists = 0;

	----------------------------------------------------
	-- Local variables
	----------------------------------------------------
	DECLARE 
		@RoleData XML
	
	----------------------------------------------------
	-- Create temporary resources
	----------------------------------------------------
	-- Existing user roles
	IF OBJECT_ID('tempdb..#tmpUserRoles') IS NOT NULL
	BEGIN
		DROP TABLE #tmpUserRoles;
	END
	
	CREATE TABLE #tmpUserRoles (
		Id INT,
		Code VARCHAR(25),
		Name VARCHAR(50),
		Description VARCHAR(1000)
	);
	
	-- Supplied roles for verification
	IF OBJECT_ID('tempdb..#tmpRolesCodeList') IS NOT NULL
	BEGIN
		DROP TABLE #tmpRolesCodeList;
	END
	
	CREATE TABLE #tmpRolesCodeList (
		Code VARCHAR(25)
	);

	----------------------------------------------------
	-- Parse roles list
	----------------------------------------------------
	INSERT INTO #tmpRolesCodeList (
		Code
	)
	SELECT
		Value
	FROM dbo.fnSplit(@Role, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
	
	
	----------------------------------------------------
	-- Fetch and parse role data
	-- <Summary>
	-- Return the xml output and parse into list.
	-- </Summary>
	-- <Remarks>
	-- The xml output option is used for scalability.
	-- </Remarks>	
	----------------------------------------------------
	EXEC acl.spCredentialEntityRole_Get_List
		@CredentialEntityID = @CredentialEntityID,
		@ApplicationID = @ApplicationID,
		@BusinessEntityID = @BusinessEntityID,
		@IsResultXml = 0,
		@IsResultXmlOutputOnly = 1,
		@RoleData = @RoleData OUTPUT
		
	INSERT INTO #tmpUserRoles (
		Id,
		Code,
		Name,
		Description
	)
	SELECT
		rs.value('data(RoleID)[1]', 'INT') AS Id,
		rs.value('data(Code)[1]', 'VARCHAR(25)') AS Code,
		rs.value('data(Name)[1]', 'VARCHAR(50)') AS Name,
		rs.value('data(Description)[1]', 'VARCHAR(1000)') AS Description
	FROM @RoleData.nodes('Roles/Role') AS r(rs)
	
	-- Debug
	--SELECT * FROM #tmpUserRoles

	----------------------------------------------------
	-- Determine if the role exists
	-- <Summary>
	-- Scans the roles list for the existence of the supplied
	-- roles
	-- </Summary>
	----------------------------------------------------
	IF EXISTS(
		SELECT TOP 1 * 
		FROM #tmpUserRoles rle
			JOIN #tmpRolesCodeList lst
				ON rle.Code = lst.Code
	)
	BEGIN
		SET @Exists = 1;
	END
	
	
	----------------------------------------------------
	-- Release resources
	----------------------------------------------------
	DROP TABLE #tmpRolesCodeList;
	DROP TABLE #tmpUserRoles;
	
	
END
