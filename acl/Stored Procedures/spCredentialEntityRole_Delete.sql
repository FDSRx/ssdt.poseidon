﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/2/2014
-- Description:	Delete's a CredentialEntityRole object.
-- SAMPLE CALL:
/*
-- Delete role by CredentialEnityID and RoleID.
EXEC acl.spCredentialEntityRole_Delete
	@CredentialEntityID = 959790,
	@RoleID = 10

-- Delete role by surrogate key.
EXEC acl.spCredentialEntityRole_Delete
	@CredentialEntityRoleID = 223
	
*/

-- SELECT * FROM acl.Roles
-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM acl.CredentialEntityRole
-- =============================================
CREATE PROCEDURE [acl].[spCredentialEntityRole_Delete]
	@CredentialEntityRoleID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@CredentialEntityID BIGINT = NULL,
	@RoleID INT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID)
		
	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- CredentialEntityRoleID, @CredentialEntityID, and @RoleID
	IF @CredentialEntityRoleID IS NULL AND @CredentialEntityID IS NULL AND @RoleID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to delete Role object. Object references (@CredentialEntityRoleID or @CredentialEntityID and @RoleID) ' +
			'are not set to an instance of an object. ' +
			'The parameters, @CredentialEntityRoleID or @CredentialEntityID and @RoleID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @CredentialEntityID (No CredentialEntityRoleID provided).
	IF @CredentialEntityRoleID IS NULL AND @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to delete Role object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END		
	
	-- @RoleID (No CredentialEntityRoleID provided).
	IF @CredentialEntityRoleID IS NULL AND @RoleID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to delete Role object. Object reference (@RoleID) is not set to an instance of an object. ' +
			'The parameter, @RoleID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	


	-------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy
	
	-------------------------------------------------------------------------------
	-- Delete CredentialEntityRole object.
	-------------------------------------------------------------------------------
	DELETE
	FROM acl.CredentialEntityRole
	WHERE CredentialEntityRoleID = @CredentialEntityRoleID
		OR (
			CredentialEntityID = @CredentialEntityID
			AND RoleID = @RoleID
			AND ( @ApplicationID IS NULL OR ApplicationID = @ApplicationID )
			AND ( @BusinessEntityID IS NULL OR BusinessEntityID = @BusinessEntityID )
		)

END
