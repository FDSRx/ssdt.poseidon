﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/2/2014
-- Description:	Creates a new CredentialEntityRole object.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityRoleID BIGINT = NULL
	
EXEC acl.spCredentialEntityRole_Create
	@CredentialEntityID = 959790,
	@BusinessEntityID = 3759,
	@ApplicationID = 8,
	@RoleID = 10,
	@CredentialEntityRoleID = @CredentialEntityRoleID OUTPUT

SELECT @CredentialEntityRoleID AS CredentialEntityRoleID

*/

-- SELECT * FROM acl.Roles
-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM acl.CredentialEntityRole
-- =============================================
CREATE PROCEDURE [acl].[spCredentialEntityRole_Create]
	@CredentialEntityID BIGINT,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@RoleID INT,
	@CreatedBy VARCHAR(256) = NULL,
	@CredentialEntityRoleID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CredentialEntityRoleID = NULL;
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- CredentialEntityID
	IF @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Role object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- RoleID
	IF @RoleID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Role object. Object reference (@RoleID) is not set to an instance of an object. ' +
			'The parameter, @RoleID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
		
	-------------------------------------------------------------------------------
	-- Create new CredentialEntityRole object.
	-------------------------------------------------------------------------------
	INSERT INTO acl.CredentialEntityRole (
		ApplicationID,
		BusinessEntityID,
		CredentialEntityID,
		RoleID,
		CreatedBy
	)
	SELECT
		@ApplicationID AS ApplicationID,
		@BusinessEntityID AS BusinessEntityID,
		@CredentialEntityID AS CredentialEntityID,
		@RoleID AS RoleID,
		@CreatedBy AS CreatedBy
	
	-- Retrieve record identity value.
	SET @CredentialEntityRoleID = SCOPE_IDENTITY();
		
END
