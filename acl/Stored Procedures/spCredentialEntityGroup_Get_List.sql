﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/8/2013
-- Description:	Gets the groups that are applicable to a user.
-- SAMPLE CALL:
/*
-- Test result set	
EXEC acl.spCredentialEntityGroup_Get_List
	@CredentialEntityID = 1,
	@ApplicationID = 2,
	@BusinessEntityID = 92

-- Test xml result set	
EXEC acl.spCredentialEntityGroup_Get_List
	@CredentialEntityID = 2,
	@ApplicationID = 2,
	@BusinessEntityID = 92,
	@IsResultXml = 1

-- Test xml output	
DECLARE @GroupData XML
EXEC acl.spCredentialEntityGroup_Get_List
	@CredentialEntityID = 1,
	@ApplicationID = 2,
	@BusinessEntityID = 92,
	@IsResultXml = 1,
	@IsResultXmlOutputOnly = 1,
	@GroupData = @GroupData OUTPUT
SELECT @GroupData AS GroupData

*/
-- =============================================
CREATE PROCEDURE [acl].[spCredentialEntityGroup_Get_List]
	@CredentialEntityID BIGINT,
	@ApplicationID INT = NULL,
	@BusinessEntityID INT = NULL,
	@IsResultXml BIT = 0,
	@IsResultXmlOutputOnly BIT = 0,
	@GroupData XML = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------
	-- Sanitize input
	----------------------------------------------------
	SET @IsResultXml = ISNULL(@IsResultXml, 0);
	SET @IsResultXmlOutputOnly = ISNULL(@IsResultXmlOutputOnly, 0);
	SET @GroupData = NULL;
	

	----------------------------------------------------
	-- Local variables
	----------------------------------------------------
	DECLARE
		@SentinelInt INT = -1999999999
	

	----------------------------------------------------
	-- Create temporary resources
	----------------------------------------------------
	-- Create credential group master listing
	IF OBJECT_ID('tempdb..#tmpCredentialGroup') IS NOT NULL
	BEGIN
		DROP TABLE #tmpCredentialGroup;
	END
	
	CREATE TABLE #tmpCredentialGroup (
		CredentialEntityID BIGINT,
		GroupID INT,
		Code VARCHAR(25),
		Name VARCHAR(50),
		Description VARCHAR(1000),
		RoleData XML
	)
	
	-- Create group listing
	IF OBJECT_ID('tempdb..#tmpGroupIDList') IS NOT NULL
	BEGIN
		DROP TABLE #tmpGroupIDList;
	END
	
	CREATE TABLE #tmpGroupIDList (
		Idx INT IDENTITY(1,1),
		ApplicationID INT,
		BusinessEntityID INT,
		GroupID INT
	)	

	----------------------------------------------------
	-- Fetch list of applicable groups
	-- <Summary>
	-- Obtains a list of applicable groupIDs
	-- Uses a bottom-up pattern to find all applicable roles.
	-- 1. Credential, Business, Application
	-- 2. Credential, Business
	-- 3. Credential, Application
	-- 4. Credential
	-- </Summary>	
	----------------------------------------------------
	-- Credential/Application/Business
	INSERT INTO #tmpGroupIDList (
		ApplicationID,
		BusinessEntityID,
		GroupID
	)
	SELECT
		ApplicationID,
		BusinessEntityID,
		GroupID
	FROM acl.vwCredentialEntityGroup grp
	WHERE CredentialEntityID = @CredentialEntityID
		AND ApplicationID = @ApplicationID
		AND BusinessEntityID = @BusinessEntityID
	
	-- Credential/Application
	INSERT INTO #tmpGroupIDList (
		ApplicationID,
		BusinessEntityID,
		GroupID
	)
	SELECT
		ApplicationID,
		BusinessEntityID,
		GroupID
	FROM acl.vwCredentialEntityGroup grp
	WHERE CredentialEntityID = @CredentialEntityID
		AND ApplicationID = @ApplicationID
		AND BusinessEntityID IS NULL
		AND NOT EXISTS (
			SELECT TOP(1) GroupID
			FROM #tmpGroupIDList virt
			WHERE virt.GroupID = grp.GroupId
				AND ISNULL(virt.ApplicationID, @SentinelInt)  = ISNULL(grp.ApplicationID, @SentinelInt)
				AND ISNULL(virt.BusinessEntityID, @SentinelInt) = ISNULL(grp.BusinessEntityID, @SentinelInt)
		)
		
	-- Credential/Business
	INSERT INTO #tmpGroupIDList (
		ApplicationID,
		BusinessEntityID,
		GroupID
	)
	SELECT
		ApplicationID,
		BusinessEntityID,
		GroupID
	FROM acl.vwCredentialEntityGroup grp
	WHERE CredentialEntityID = @CredentialEntityID
		AND ApplicationID IS NULL
		AND BusinessEntityID = @BusinessEntityID
		AND NOT EXISTS (
			SELECT TOP(1) GroupID
			FROM #tmpGroupIDList virt
			WHERE virt.GroupID = grp.GroupId
				AND ISNULL(virt.ApplicationID, @SentinelInt)  = ISNULL(grp.ApplicationID, @SentinelInt)
				AND ISNULL(virt.BusinessEntityID, @SentinelInt) = ISNULL(grp.BusinessEntityID, @SentinelInt)
		)

	-- Credential
	INSERT INTO #tmpGroupIDList (
		ApplicationID,
		BusinessEntityID,
		GroupID
	)
	SELECT
		ApplicationID,
		BusinessEntityID,
		GroupID
	FROM acl.vwCredentialEntityGroup grp
	WHERE CredentialEntityID = @CredentialEntityID
		AND ApplicationID IS NULL
		AND BusinessEntityID IS NULL
		AND NOT EXISTS (
			SELECT TOP(1) GroupID
			FROM #tmpGroupIDList virt
			WHERE virt.GroupID = grp.GroupId
				AND ISNULL(virt.ApplicationID, @SentinelInt)  = ISNULL(grp.ApplicationID, @SentinelInt)
				AND ISNULL(virt.BusinessEntityID, @SentinelInt) = ISNULL(grp.BusinessEntityID, @SentinelInt)
		)
		
	----------------------------------------------------
	-- Fetch data
	-- <Summary>
	-- Fetches data from the credential group table.
	-- </Summary>
	----------------------------------------------------
	INSERT INTO #tmpCredentialGroup (
		CredentialEntityID,
		GroupID,
		Code,
		Name,
		Description,
		RoleData
	)
	SELECT
		@CredentialEntityID AS CredentialEntityID,
		credgrp.GroupID,
		credgrp.GroupCode AS Code,
		credgrp.GroupName AS Name,
		credgrp.GroupDescription AS Description, 
		(
			SELECT
				grprle.RoleID,
				grprle.RoleCode AS Code,
				grprle.RoleName AS Name,
				grprle.RoleDescription AS Description,
				CONVERT(XML, (
					SELECT
						rp.PermissionID,
						rp.PermissionCode AS Code,
						rp.PermissionName AS Name,
						rp.PermissionDescription AS Description
					--SELECT *
					FROM acl.vwRolePermission rp
					WHERE rp.ApplicationID = grprle.ApplicationID
								AND rp.BusinessEntityID = grprle.BusinessEntityID
								AND rp.RoleID = grprle.RoleID
					FOR XML PATH ('Permission')
				)) AS [Permissions]
			--SELECT *
			FROM acl.vwGroupRole grprle
			WHERE grprle.ApplicationID = credgrp.ApplicationID
				AND grprle.BusinessEntityID = credgrp.BusinessEntityID
			FOR XML PATH ('Role')
		) AS RoleData
	--SELECT *
	FROM acl.vwCredentialEntityGroup credgrp
		JOIN #tmpGroupIDList tmp
			ON tmp.GroupID = credgrp.GroupId
				AND ISNULL(tmp.ApplicationID, @SentinelInt)  = ISNULL(credgrp.ApplicationID, @SentinelInt)
				AND ISNULL(tmp.BusinessEntityID, @SentinelInt) = ISNULL(credgrp.BusinessEntityID, @SentinelInt)
	WHERE credgrp.CredentialEntityID = @CredentialEntityID
	

	----------------------------------------------------
	-- Determine if data needs to be returned as XML format
	-- <Summary>
	-- Returns or outputs the data in XML format
	-- </Summary>
	----------------------------------------------------
	IF @IsResultXml = 1 OR @IsResultXmlOutputOnly = 1
	BEGIN
		SET @GroupData = (SELECT CONVERT(XML, (
			SELECT
				GroupID,
				Code,
				Name,
				Description,
				RoleData AS Roles
			FROM #tmpCredentialGroup
			FOR XML PATH('Group'), ROOT('Groups')
		)));
		
		IF @IsResultXmlOutputOnly = 0
		BEGIN
			SELECT @GroupData AS Roles
		END
	END
	ELSE
	BEGIN
		
		----------------------------------------------------
		-- Return the groups result set
		----------------------------------------------------
		SELECT
			GroupID,
			Code,
			Name,
			Description
		FROM #tmpCredentialGroup
	
	END
	
	
	----------------------------------------------------
	-- Release resources
	----------------------------------------------------
	DROP TABLE #tmpCredentialGroup;
	
	
END
