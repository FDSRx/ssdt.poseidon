﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/2/2014
-- Description:	Delete's a single or multiple CredentialEntityRole objects.
-- SAMPLE CALL:
/*
-- Delete role(s) by CredentialEnityID and RoleID.
EXEC acl.spCredentialEntityRole_DeleteRange
	@CredentialEntityID = 959790,
	@RoleID = '10, 11, 12'

-- Delete role(s) by CredentialEnityID and RoleID.
EXEC acl.spCredentialEntityRole_DeleteRange
	@CredentialEntityID = 959790,
	@RoleID = '10'
	
-- Delete role(s) by surrogate key.
EXEC acl.spCredentialEntityRole_DeleteRange
	@CredentialEntityRoleID = 223
	
*/

-- SELECT * FROM acl.Roles
-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM acl.CredentialEntityRole
-- =============================================
CREATE PROCEDURE [acl].[spCredentialEntityRole_DeleteRange]
	@CredentialEntityRoleID VARCHAR(MAX) = NULL,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@CredentialEntityID BIGINT = NULL,
	@RoleID VARCHAR(MAX) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		
	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- CredentialEntityRoleID, @CredentialEntityID, and @RoleID
	IF dbo.fnIsNullOrWhiteSpace(@CredentialEntityRoleID) = 1 
		AND @CredentialEntityID IS NULL
		AND dbo.fnIsNullOrWhiteSpace(@RoleID) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to delete Role object. Object references (@CredentialEntityRoleID or @CredentialEntityID and @RoleID) ' +
			'are not set to an instance of an object. ' +
			'The parameters, @CredentialEntityRoleID or @CredentialEntityID and @RoleID, cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @CredentialEntityID (No CredentialEntityRoleID provided).
	IF dbo.fnIsNullOrWhiteSpace(@CredentialEntityRoleID) = 1 
		AND @CredentialEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to delete Role object. Object reference (@CredentialEntityID) is not set to an instance of an object. ' +
			'The parameter, @CredentialEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END		
	
	-- @RoleID (No CredentialEntityRoleID provided).
	IF dbo.fnIsNullOrWhiteSpace(@CredentialEntityRoleID) = 1  
		AND dbo.fnIsNullOrWhiteSpace(@RoleID) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to delete Role object. Object reference (@RoleID) is not set to an instance of an object. ' +
			'The parameter, @RoleID, cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	-------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy

	-------------------------------------------------------------------------------
	-- Delete CredentialEntityRole object.
	-------------------------------------------------------------------------------
	DELETE
	FROM acl.CredentialEntityRole
	WHERE CredentialEntityRoleID IN (SElECT Value FROM dbo.fnSplit(@CredentialEntityRoleID, ','))
		OR (
			CredentialEntityID = @CredentialEntityID
			AND RoleID IN (SElECT Value FROM dbo.fnSplit(@RoleID, ','))
			AND ( @ApplicationID IS NULL OR ApplicationID = @ApplicationID )
			AND ( @BusinessEntityID IS NULL OR BusinessEntityID = @BusinessEntityID )
		)

END
