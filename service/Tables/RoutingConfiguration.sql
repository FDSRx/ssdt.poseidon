﻿CREATE TABLE [service].[RoutingConfiguration] (
    [RoutingConfigurationID] INT              IDENTITY (1, 1) NOT NULL,
    [ClientID]               INT              NULL,
    [ConfigurationXml]       XML              NOT NULL,
    [rowguid]                UNIQUEIDENTIFIER CONSTRAINT [DF_RoutingConfiguration_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_RoutingConfiguration_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]           DATETIME         CONSTRAINT [DF_RoutingConfiguration_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              VARCHAR (256)    NULL,
    [ModifiedBy]             VARCHAR (256)    NULL,
    CONSTRAINT [PK_RoutingConfiguration] PRIMARY KEY CLUSTERED ([RoutingConfigurationID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_RoutingConfiguration_rowguid]
    ON [service].[RoutingConfiguration]([rowguid] ASC);

