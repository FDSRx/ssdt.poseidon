﻿CREATE TABLE [service].[ServiceInstance] (
    [ServiceInstanceID]      INT              IDENTITY (1, 1) NOT NULL,
    [MachineName]            VARCHAR (128)    NOT NULL,
    [ServiceName]            VARCHAR (128)    NOT NULL,
    [DisplayName]            VARCHAR (256)    NOT NULL,
    [Description]            VARCHAR (512)    NULL,
    [AccountType]            INT              NULL,
    [UserName]               VARCHAR (50)     NULL,
    [Password]               VARCHAR (50)     NULL,
    [StartMode]              INT              NULL,
    [CurrentStatus]          INT              NULL,
    [QueuedAction]           INT              NULL,
    [IsLoggingOn]            BIT              NULL,
    [IsAutoStart]            BIT              NULL,
    [ExecutableFilepath]     VARCHAR (256)    NOT NULL,
    [RoutingConfigurationID] INT              NULL,
    [PollingInterval]        INT              NULL,
    [IdleTimespan]           INT              NULL,
    [StartOnTime]            TIME (7)         NULL,
    [EndOnTime]              TIME (7)         NULL,
    [RefreshOnTime]          TIME (7)         NULL,
    [FailoverMachineName]    VARCHAR (128)    NULL,
    [rowguid]                UNIQUEIDENTIFIER CONSTRAINT [DF_ServiceInstance_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_ServiceInstance_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]           DATETIME         CONSTRAINT [DF_ServiceInstance_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              VARCHAR (256)    NULL,
    [ModifiedBy]             VARCHAR (256)    NULL,
    CONSTRAINT [PK_ServiceInstance] PRIMARY KEY CLUSTERED ([ServiceInstanceID] ASC),
    CONSTRAINT [FK_ServiceInstance_RoutingConfiguration] FOREIGN KEY ([RoutingConfigurationID]) REFERENCES [service].[RoutingConfiguration] ([RoutingConfigurationID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ServiceInstance_MachineService]
    ON [service].[ServiceInstance]([MachineName] ASC, [ServiceName] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ServiceInstance_rowguid]
    ON [service].[ServiceInstance]([rowguid] ASC);

