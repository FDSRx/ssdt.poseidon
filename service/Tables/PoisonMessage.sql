﻿CREATE TABLE [service].[PoisonMessage] (
    [PoisonMessageID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ChannelType]     VARCHAR (50)     NULL,
    [ServiceInstance] VARCHAR (256)    NOT NULL,
    [Message]         VARCHAR (MAX)    NOT NULL,
    [IsDelete]        BIT              CONSTRAINT [DF_PoisonMessage_IsDelete] DEFAULT ((0)) NOT NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_PoisonMessage_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_PoisonMessage_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_PoisonMessage_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)    NULL,
    [ModifiedBy]      VARCHAR (256)    NULL,
    CONSTRAINT [PK_PoisonMessage] PRIMARY KEY CLUSTERED ([PoisonMessageID] ASC)
);

