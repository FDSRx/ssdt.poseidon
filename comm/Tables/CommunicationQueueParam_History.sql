﻿CREATE TABLE [comm].[CommunicationQueueParam_History] (
    [CommunicationQueueParamHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CommunicationQueueParamID]        BIGINT           NULL,
    [CommunicationQueueID]             BIGINT           NULL,
    [ParameterID]                      INT              NULL,
    [ValueString]                      VARCHAR (MAX)    NULL,
    [TypeOf]                           VARCHAR (50)     NULL,
    [rowguid]                          UNIQUEIDENTIFIER NULL,
    [DateCreated]                      DATETIME         NULL,
    [DateModified]                     DATETIME         NULL,
    [CreatedBy]                        VARCHAR (256)    NULL,
    [ModifiedBy]                       VARCHAR (256)    NULL,
    [AuditGuid]                        UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationQueueParam_History_AuditGuid] DEFAULT (newid()) NULL,
    [AuditAction]                      VARCHAR (10)     NULL,
    [DateAudited]                      DATETIME         CONSTRAINT [DF_CommunicationQueueParam_History_DateAuditied] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_CommunicationQueueParam_History] PRIMARY KEY CLUSTERED ([CommunicationQueueParamHistoryID] ASC)
);

