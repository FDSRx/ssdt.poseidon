﻿CREATE TABLE [comm].[CommunicationQueueParam] (
    [CommunicationQueueParamID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CommunicationQueueID]      BIGINT           NOT NULL,
    [ParameterID]               INT              NOT NULL,
    [ValueString]               VARCHAR (MAX)    NULL,
    [TypeOf]                    VARCHAR (50)     NULL,
    [rowguid]                   UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationQueueParam_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]               DATETIME         CONSTRAINT [DF_CommunicationQueueParam_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]              DATETIME         CONSTRAINT [DF_CommunicationQueueParam_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                 VARCHAR (256)    NULL,
    [ModifiedBy]                VARCHAR (256)    NULL,
    CONSTRAINT [PK_CommunicationQueueParam] PRIMARY KEY CLUSTERED ([CommunicationQueueParamID] ASC),
    CONSTRAINT [FK_CommunicationQueueParam_CommunicationQueue] FOREIGN KEY ([CommunicationQueueID]) REFERENCES [comm].[CommunicationQueue] ([CommunicationQueueID]),
    CONSTRAINT [FK_CommunicationQueueParam_Parameter] FOREIGN KEY ([ParameterID]) REFERENCES [data].[Parameter] ([ParameterID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationQueueParam_CommQueueParam]
    ON [comm].[CommunicationQueueParam]([CommunicationQueueID] ASC, [ParameterID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationQueueParam_rowguid]
    ON [comm].[CommunicationQueueParam]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/23/2015
-- Description:	Audits a record change of the comm.CommunicationQueueParam table

-- SELECT * FROM comm.CommunicationQueueParam_History
-- SELECT * FROM comm.CommunicationQueueParam 

-- TRUNCATE TABLE comm.CommunicationQueueParam_History
-- =============================================
CREATE TRIGGER [comm].[trigCommunicationQueueParam_Audit]
   ON  [comm].[CommunicationQueueParam]
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10); -- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-----------------------------------------------------------------------------
	-- Audit transaction
	-----------------------------------------------------------------------------
	-- Audit inserted records
	INSERT INTO comm.CommunicationQueueParam_History (
		CommunicationQueueParamID,
		CommunicationQueueID,
		ParameterID,
		ValueString,
		TypeOf,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		d.CommunicationQueueParamID,
		d.CommunicationQueueID,
		d.ParameterID,
		d.ValueString,
		d.TypeOf,
		d.rowguid,
		d.DateCreated,
		d.DateModified,
		d.CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_SNAME()
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d


	--SELECT * FROM comm.CommunicationQueueParam
	--SELECT * FROM comm.CommunicationQueueParam_History
	
	

END
