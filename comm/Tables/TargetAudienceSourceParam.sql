﻿CREATE TABLE [comm].[TargetAudienceSourceParam] (
    [TargetAudienceSourceParamID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [TargetAudienceSourceID]      INT              NOT NULL,
    [ParameterID]                 INT              NOT NULL,
    [TypeOf]                      VARCHAR (50)     NULL,
    [IsNullable]                  BIT              CONSTRAINT [DF_TargetAudienceSourceParam_IsNullable] DEFAULT ((0)) NULL,
    [IsRequired]                  BIT              CONSTRAINT [DF_TargetAudienceSourceParam_IsRequired] DEFAULT ((0)) NULL,
    [rowguid]                     UNIQUEIDENTIFIER CONSTRAINT [DF_TargetAudienceSourceParam_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                 DATETIME         CONSTRAINT [DF_TargetAudienceSourceParam_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                DATETIME         CONSTRAINT [DF_TargetAudienceSourceParam_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                   VARCHAR (256)    NULL,
    [ModifiedBy]                  VARCHAR (256)    NULL,
    CONSTRAINT [PK_TargetAudienceSourceParam] PRIMARY KEY CLUSTERED ([TargetAudienceSourceParamID] ASC),
    CONSTRAINT [FK_TargetAudienceSourceParam_Parameter] FOREIGN KEY ([ParameterID]) REFERENCES [data].[Parameter] ([ParameterID]),
    CONSTRAINT [FK_TargetAudienceSourceParam_TargetAudienceSource] FOREIGN KEY ([TargetAudienceSourceID]) REFERENCES [comm].[TargetAudienceSource] ([TargetAudienceSourceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TargetAudienceSourceParam_AudParam]
    ON [comm].[TargetAudienceSourceParam]([TargetAudienceSourceID] ASC, [ParameterID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TargetAudienceSourceParam_rowguid]
    ON [comm].[TargetAudienceSourceParam]([rowguid] ASC);

