﻿CREATE TABLE [comm].[MessageType] (
    [MessageTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]          VARCHAR (25)     NOT NULL,
    [Name]          VARCHAR (50)     NOT NULL,
    [Description]   VARCHAR (1000)   NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_MessageType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_MessageType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_MessageType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_Comm_MessageType] PRIMARY KEY CLUSTERED ([MessageTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Comm_MessageType_Code]
    ON [comm].[MessageType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Comm_MessageType_Name]
    ON [comm].[MessageType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Comm_MessageType_rowguid]
    ON [comm].[MessageType]([rowguid] ASC);

