﻿CREATE TABLE [comm].[MessageTypeParameter] (
    [MessageTypeParameterID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]          INT              NULL,
    [BusinessID]             BIGINT           NULL,
    [EntityID]               BIGINT           NULL,
    [MessageTypeID]          INT              NOT NULL,
    [ParameterID]            INT              NOT NULL,
    [ValueString]            VARCHAR (MAX)    NULL,
    [TypeOf]                 VARCHAR (25)     NULL,
    [rowguid]                UNIQUEIDENTIFIER CONSTRAINT [DF_MessageTypeParameter_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_MessageTypeParameter_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]           DATETIME         CONSTRAINT [DF_MessageTypeParameter_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              VARCHAR (256)    NULL,
    [ModifiedBy]             VARCHAR (256)    NULL,
    CONSTRAINT [PK_MessageTypeParameter] PRIMARY KEY CLUSTERED ([MessageTypeParameterID] ASC),
    CONSTRAINT [FK_MessageTypeParameter_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_MessageTypeParameter_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_MessageTypeParameter_BusinessEntity] FOREIGN KEY ([EntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_MessageTypeParameter_MessageType] FOREIGN KEY ([MessageTypeID]) REFERENCES [comm].[MessageType] ([MessageTypeID]),
    CONSTRAINT [FK_MessageTypeParameter_Parameter] FOREIGN KEY ([ParameterID]) REFERENCES [data].[Parameter] ([ParameterID])
);


GO
CREATE NONCLUSTERED INDEX [UIX_MessageTypeParameter_AppBizEntMsgTypParam]
    ON [comm].[MessageTypeParameter]([ApplicationID] ASC, [BusinessID] ASC, [EntityID] ASC, [MessageTypeID] ASC, [ParameterID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageTypeParameter_rowguid]
    ON [comm].[MessageTypeParameter]([rowguid] ASC);

