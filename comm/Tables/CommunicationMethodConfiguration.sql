﻿CREATE TABLE [comm].[CommunicationMethodConfiguration] (
    [CommunicationMethodConfigurationID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]                      INT              NULL,
    [BusinessID]                         BIGINT           NULL,
    [EntityID]                           BIGINT           NULL,
    [CommunicationMethodID]              INT              NOT NULL,
    [rowguid]                            UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationMethodConfiguration_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                        DATETIME         CONSTRAINT [DF_CommunicationMethodConfiguration_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                       DATETIME         CONSTRAINT [DF_CommunicationMethodConfiguration_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                          VARCHAR (256)    NULL,
    [ModifiedBy]                         VARCHAR (256)    NULL,
    CONSTRAINT [PK_CommunicationMethodConfiguration] PRIMARY KEY CLUSTERED ([CommunicationMethodConfigurationID] ASC),
    CONSTRAINT [FK_CommunicationMethodConfiguration_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CommunicationMethodConfiguration_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_CommunicationMethodConfiguration_CommunicationMethod] FOREIGN KEY ([CommunicationMethodID]) REFERENCES [comm].[CommunicationMethod] ([CommunicationMethodID]),
    CONSTRAINT [FK_CommunicationMethodConfiguration_Entity] FOREIGN KEY ([EntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationMethodConfiguration_AppBizEntMeth]
    ON [comm].[CommunicationMethodConfiguration]([ApplicationID] ASC, [BusinessID] ASC, [EntityID] ASC, [CommunicationMethodID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationMethodConfiguration_rowguid]
    ON [comm].[CommunicationMethodConfiguration]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/20/2015
-- Description:	Audits a record change of the comm.CommunicationMethodConfiguration table

-- SELECT * FROM comm.CommunicationMethodConfiguration_History
-- SELECT * FROM comm.CommunicationMethodConfiguration WHERE PersonID = 801018

-- TRUNCATE TABLE comm.CommunicationMethodConfiguration_History
-- =============================================
CREATE TRIGGER [comm].[trigCommunicationMethodConfiguration_Audit]
   ON  [comm].[CommunicationMethodConfiguration]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10); -- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-----------------------------------------------------------------------------
	-- Audit transaction
	-----------------------------------------------------------------------------
	-- Audit inserted records
	INSERT INTO comm.CommunicationMethodConfiguration_History (
		CommunicationMethodConfigurationID,
		ApplicationID,
		BusinessID,
		EntityID,
		CommunicationMethodID,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		d.CommunicationMethodConfigurationID,
		d.ApplicationID,
		d.BusinessID,
		d.EntityID,
		d.CommunicationMethodID,
		d.rowguid,	
		d.DateCreated,	
		d.DateModified,
		d.CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_SNAME() 
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d


	--SELECT * FROM comm.CommunicationMethodConfiguration
	--SELECT * FROM comm.CommunicationMethodConfiguration_History
	
	

END
