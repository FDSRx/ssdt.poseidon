﻿CREATE TABLE [comm].[CommunicationMethod] (
    [CommunicationMethodID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]                  VARCHAR (25)     NOT NULL,
    [Name]                  VARCHAR (50)     NOT NULL,
    [Description]           VARCHAR (1000)   NULL,
    [rowguid]               UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationMethod_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]           DATETIME         CONSTRAINT [DF_CommunicationMethod_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]          DATETIME         CONSTRAINT [DF_CommunicationMethod_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             VARCHAR (256)    NULL,
    [ModifiedBy]            VARCHAR (256)    NULL,
    CONSTRAINT [PK_CommunicationMethod] PRIMARY KEY CLUSTERED ([CommunicationMethodID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationMethod_Code]
    ON [comm].[CommunicationMethod]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationMethod_Name]
    ON [comm].[CommunicationMethod]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationMethod_rowguid]
    ON [comm].[CommunicationMethod]([rowguid] ASC);

