﻿CREATE TABLE [comm].[CommunicationConfiguration_History] (
    [CommunicationConfigurationHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CommunicationConfigurationID]        BIGINT           NOT NULL,
    [ApplicationID]                       INT              NULL,
    [BusinessID]                          BIGINT           NULL,
    [EntityID]                            BIGINT           NULL,
    [MessageTypeID]                       INT              NOT NULL,
    [CommunicationMethodID]               INT              NOT NULL,
    [MessagesPerMinutes]                  INT              NULL,
    [MessagesNumberOfMinutes]             INT              NULL,
    [MessagesPerDays]                     INT              NULL,
    [MessagesNumberOfDays]                INT              NULL,
    [IsDisabled]                          BIT              NULL,
    [rowguid]                             UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]                         DATETIME         NOT NULL,
    [DateModified]                        DATETIME         NOT NULL,
    [CreatedBy]                           VARCHAR (256)    NULL,
    [ModifiedBy]                          VARCHAR (256)    NULL,
    [AuditGuid]                           UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationConfiguration_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                         VARCHAR (10)     NULL,
    [DateAudited]                         DATETIME         CONSTRAINT [DF_CommunicationConfiguration_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CommunicationConfiguration_History] PRIMARY KEY CLUSTERED ([CommunicationConfigurationHistoryID] ASC)
);

