﻿CREATE TABLE [comm].[CommunicationQueue] (
    [CommunicationQueueID]   BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]          INT              NULL,
    [BusinessID]             BIGINT           NOT NULL,
    [MessageTypeID]          INT              NOT NULL,
    [CommunicationMethodID]  INT              NOT NULL,
    [CommunicationName]      VARCHAR (256)    NULL,
    [TargetAudienceSourceID] INT              NULL,
    [RunOn]                  DATETIME         NULL,
    [DateStart]              DATETIME         NULL,
    [DateEnd]                DATETIME         NULL,
    [ExecutionStatusID]      INT              NULL,
    [rowguid]                UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationQueue_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_CommunicationQueue_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]           DATETIME         CONSTRAINT [DF_CommunicationQueue_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              VARCHAR (256)    NULL,
    [ModifiedBy]             VARCHAR (256)    NULL,
    CONSTRAINT [PK_CommunicationQueue] PRIMARY KEY CLUSTERED ([CommunicationQueueID] ASC),
    CONSTRAINT [FK_CommunicationQueue_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CommunicationQueue_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_CommunicationQueue_CommunicationMethod] FOREIGN KEY ([CommunicationMethodID]) REFERENCES [comm].[CommunicationMethod] ([CommunicationMethodID]),
    CONSTRAINT [FK_CommunicationQueue_MessageType] FOREIGN KEY ([MessageTypeID]) REFERENCES [comm].[MessageType] ([MessageTypeID]),
    CONSTRAINT [FK_CommunicationQueue_TargetAudienceSource] FOREIGN KEY ([TargetAudienceSourceID]) REFERENCES [comm].[TargetAudienceSource] ([TargetAudienceSourceID])
);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/23/2015
-- Description:	Audits a record change of the comm.CommunicationQueue table

-- SELECT * FROM comm.CommunicationQueue_History
-- SELECT * FROM comm.CommunicationQueue 

-- TRUNCATE TABLE comm.CommunicationQueue_History
-- =============================================
CREATE TRIGGER [comm].[trigCommunicationQueue_Audit]
   ON  [comm].[CommunicationQueue]
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10); -- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-----------------------------------------------------------------------------
	-- Audit transaction
	-----------------------------------------------------------------------------
	-- Audit inserted records
	INSERT INTO comm.CommunicationQueue_History (
		CommunicationQueueID,
		ApplicationID,
		BusinessID,
		MessageTypeID,
		CommunicationMethodID,
		CommunicationName,
		TargetAudienceSourceID,
		RunOn,
		DateStart,
		DateEnd,
		ExecutionStatusID,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		d.CommunicationQueueID,
		d.ApplicationID,
		d.BusinessID,
		d.MessageTypeID,
		d.CommunicationMethodId,
		d.CommunicationName,
		d.TargetAudienceSourceID,
		d.RunOn,
		d.DateStart,
		d.DateEnd,
		d.ExecutionStatusID,
		d.rowguid,
		d.DateCreated,
		d.DateModified,
		d.CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_SNAME()
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d


	--SELECT * FROM comm.CommunicationQueue
	--SELECT * FROM comm.CommunicationQueue_History
	
	

END
