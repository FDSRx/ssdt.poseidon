﻿CREATE TABLE [comm].[MessageTypeConfiguration] (
    [MessageTypeConfigurationID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]              INT              NULL,
    [BusinessID]                 BIGINT           NULL,
    [EntityID]                   BIGINT           NULL,
    [MessageTypeID]              INT              NOT NULL,
    [rowguid]                    UNIQUEIDENTIFIER CONSTRAINT [DF_MessageTypeConfiguration_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                DATETIME         CONSTRAINT [DF_MessageTypeConfiguration_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]               DATETIME         CONSTRAINT [DF_MessageTypeConfiguration_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                  VARCHAR (256)    NULL,
    [ModifiedBy]                 VARCHAR (256)    NULL,
    CONSTRAINT [PK_MessageTypeConfiguration] PRIMARY KEY CLUSTERED ([MessageTypeConfigurationID] ASC),
    CONSTRAINT [FK_MessageTypeConfiguration_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_MessageTypeConfiguration_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_MessageTypeConfiguration_BusinessEntity] FOREIGN KEY ([EntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_MessageTypeConfiguration_MessageType] FOREIGN KEY ([MessageTypeID]) REFERENCES [comm].[MessageType] ([MessageTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageTypeConfiguration_AppBizEntMsg]
    ON [comm].[MessageTypeConfiguration]([ApplicationID] ASC, [BusinessID] ASC, [EntityID] ASC, [MessageTypeID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MessageTypeConfiguration_rowguid]
    ON [comm].[MessageTypeConfiguration]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/20/2015
-- Description:	Audits a record change of the comm.MessageTypeConfiguration table

-- SELECT * FROM comm.MessageTypeConfiguration_History
-- SELECT * FROM comm.MessageTypeConfiguration WHERE PersonID = 801018

-- TRUNCATE TABLE comm.MessageTypeConfiguration_History
-- =============================================
CREATE TRIGGER [comm].[trigMessageTypeConfiguration_Audit]
   ON  [comm].[MessageTypeConfiguration]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10); -- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-----------------------------------------------------------------------------
	-- Audit transaction
	-----------------------------------------------------------------------------
	-- Audit inserted records
	INSERT INTO comm.MessageTypeConfiguration_History (
		MessageTypeConfigurationID,
		ApplicationID,
		BusinessID,
		EntityID,
		MessageTypeID,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		d.MessageTypeConfigurationID,
		d.ApplicationID,
		d.BusinessID,
		d.EntityID,
		d.MessageTypeID,
		d.rowguid,	
		d.DateCreated,	
		d.DateModified,
		d.CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_SNAME()
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d


	--SELECT * FROM comm.MessageTypeConfiguration
	--SELECT * FROM comm.MessageTypeConfiguration_History
	
	

END
