﻿CREATE TABLE [comm].[CommunicationExecution] (
    [CommunicationExecutionID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]            INT              NULL,
    [BusinessID]               BIGINT           NULL,
    [MessageTypeID]            INT              NULL,
    [CommunicationMethodID]    INT              NULL,
    [CommunicationName]        VARCHAR (256)    NULL,
    [TargetAudienceSourceID]   INT              NULL,
    [DateScheduled]            DATETIME         NULL,
    [DateStart]                DATETIME         NULL,
    [DateEnd]                  DATETIME         NULL,
    [ExecutionStatusID]        INT              NULL,
    [ReadCount]                BIGINT           CONSTRAINT [DF_CommunicationExecution_ReadCount] DEFAULT ((0)) NOT NULL,
    [WriteCount]               BIGINT           CONSTRAINT [DF_CommunicationExecution_SendCount] DEFAULT ((0)) NOT NULL,
    [SkipCount]                BIGINT           CONSTRAINT [DF_CommunicationExecution_SkipCount] DEFAULT ((0)) NOT NULL,
    [ExitCode]                 VARCHAR (256)    NULL,
    [ExitMessage]              VARCHAR (MAX)    NULL,
    [Result]                   VARCHAR (MAX)    NULL,
    [Message]                  VARCHAR (MAX)    NULL,
    [Arguments]                VARCHAR (MAX)    NULL,
    [TransactionGuid]          UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationExecution_TransactionGuid] DEFAULT (newid()) NOT NULL,
    [rowguid]                  UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationExecution_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]              DATETIME         CONSTRAINT [DF_CommunicationExecution_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]             DATETIME         CONSTRAINT [DF_CommunicationExecution_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                VARCHAR (256)    NULL,
    [ModifiedBy]               VARCHAR (256)    NULL,
    CONSTRAINT [PK_CommunicationExecution] PRIMARY KEY CLUSTERED ([CommunicationExecutionID] ASC),
    CONSTRAINT [FK_CommunicationExecution_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CommunicationExecution_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_CommunicationExecution_CommunicationMethod] FOREIGN KEY ([CommunicationMethodID]) REFERENCES [comm].[CommunicationMethod] ([CommunicationMethodID]),
    CONSTRAINT [FK_CommunicationExecution_ExecutionStatus] FOREIGN KEY ([ExecutionStatusID]) REFERENCES [etl].[ExecutionStatus] ([ExecutionStatusID]),
    CONSTRAINT [FK_CommunicationExecution_MessageType] FOREIGN KEY ([MessageTypeID]) REFERENCES [comm].[MessageType] ([MessageTypeID]),
    CONSTRAINT [FK_CommunicationExecution_TargetAudienceSource] FOREIGN KEY ([TargetAudienceSourceID]) REFERENCES [comm].[TargetAudienceSource] ([TargetAudienceSourceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationExecution_rowguid]
    ON [comm].[CommunicationExecution]([rowguid] ASC);

