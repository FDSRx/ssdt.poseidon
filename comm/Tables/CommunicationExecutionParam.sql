﻿CREATE TABLE [comm].[CommunicationExecutionParam] (
    [CommunicationExecutionParamID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CommunicationExecutionID]      BIGINT           NOT NULL,
    [ParameterID]                   INT              NOT NULL,
    [ValueString]                   VARCHAR (MAX)    NULL,
    [TypeOf]                        VARCHAR (50)     NULL,
    [rowguid]                       UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationExecutionParam_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                   DATETIME         CONSTRAINT [DF_CommunicationExecutionParam_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                  DATETIME         CONSTRAINT [DF_CommunicationExecutionParam_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                     VARCHAR (256)    NULL,
    [ModifiedBy]                    VARCHAR (256)    NULL,
    CONSTRAINT [PK_CommunicationExecutionParam] PRIMARY KEY CLUSTERED ([CommunicationExecutionParamID] ASC),
    CONSTRAINT [FK_CommunicationExecutionParam_CommunicationExecution] FOREIGN KEY ([CommunicationExecutionID]) REFERENCES [comm].[CommunicationExecution] ([CommunicationExecutionID]),
    CONSTRAINT [FK_CommunicationExecutionParam_Parameter] FOREIGN KEY ([ParameterID]) REFERENCES [data].[Parameter] ([ParameterID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationExecutionParam_CommExecParam]
    ON [comm].[CommunicationExecutionParam]([CommunicationExecutionID] ASC, [ParameterID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationExecutionParam_rowguid]
    ON [comm].[CommunicationExecutionParam]([rowguid] ASC);

