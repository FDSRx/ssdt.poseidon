﻿CREATE TABLE [comm].[TargetAudienceSource] (
    [TargetAudienceSourceID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]                   VARCHAR (50)     NOT NULL,
    [Name]                   VARCHAR (256)    NOT NULL,
    [Description]            VARCHAR (1000)   NULL,
    [Script]                 VARCHAR (MAX)    NULL,
    [rowguid]                UNIQUEIDENTIFIER CONSTRAINT [DF_TargetAudienceSource_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]            DATETIME         CONSTRAINT [DF_TargetAudienceSource_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]           DATETIME         CONSTRAINT [DF_TargetAudienceSource_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              VARCHAR (256)    NULL,
    [ModifiedBy]             VARCHAR (256)    NULL,
    CONSTRAINT [PK_TargetAudienceSource] PRIMARY KEY CLUSTERED ([TargetAudienceSourceID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TargetAudienceSource_Code]
    ON [comm].[TargetAudienceSource]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TargetAudienceSource_Name]
    ON [comm].[TargetAudienceSource]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TargetAudienceSource_rowguid]
    ON [comm].[TargetAudienceSource]([rowguid] ASC);

