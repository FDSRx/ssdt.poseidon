﻿CREATE TABLE [comm].[CommunicationConfiguration] (
    [CommunicationConfigurationID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]                INT              NULL,
    [BusinessID]                   BIGINT           NULL,
    [EntityID]                     BIGINT           NULL,
    [MessageTypeID]                INT              NOT NULL,
    [CommunicationMethodID]        INT              NOT NULL,
    [MessagesPerMinutes]           INT              NULL,
    [MessagesNumberOfMinutes]      INT              NULL,
    [MessagesPerDays]              INT              NULL,
    [MessagesNumberOfDays]         INT              NULL,
    [IsDisabled]                   BIT              CONSTRAINT [DF_CommunicationConfiguration_IsEnabled] DEFAULT ((0)) NOT NULL,
    [rowguid]                      UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationConfiguration_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                  DATETIME         CONSTRAINT [DF_CommunicationConfiguration_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                 DATETIME         CONSTRAINT [DF_CommunicationConfiguration_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                    VARCHAR (256)    NULL,
    [ModifiedBy]                   VARCHAR (256)    NULL,
    CONSTRAINT [PK_CommunicationConfiguration] PRIMARY KEY CLUSTERED ([CommunicationConfigurationID] ASC),
    CONSTRAINT [FK_CommunicationConfiguration_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CommunicationConfiguration_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_CommunicationConfiguration_BusinessEntity] FOREIGN KEY ([EntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_CommunicationConfiguration_CommunicationMethod] FOREIGN KEY ([CommunicationMethodID]) REFERENCES [comm].[CommunicationMethod] ([CommunicationMethodID]),
    CONSTRAINT [FK_CommunicationConfiguration_MessageType] FOREIGN KEY ([MessageTypeID]) REFERENCES [comm].[MessageType] ([MessageTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationConfiguration_AppBizEntTypMeth]
    ON [comm].[CommunicationConfiguration]([ApplicationID] ASC, [BusinessID] ASC, [EntityID] ASC, [MessageTypeID] ASC, [CommunicationMethodID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CommunicationConfiguration_rowguid]
    ON [comm].[CommunicationConfiguration]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/27/2015
-- Description:	Audits a record change of the comm.CommunicationConfiguration table

-- SELECT * FROM comm.CommunicationConfiguration_History
-- SELECT * FROM comm.CommunicationConfiguration WHERE Entity = 801018

-- TRUNCATE TABLE comm.CommunicationConfiguration_History
-- =============================================
CREATE TRIGGER [comm].[trigCommunicationConfiguration_Audit]
   ON  [comm].[CommunicationConfiguration]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10); -- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	
	-----------------------------------------------------------------------------
	-- Audit transaction
	-----------------------------------------------------------------------------
	-- Audit inserted records
	INSERT INTO comm.CommunicationConfiguration_History (
		CommunicationConfigurationID,
		ApplicationID,
		BusinessID,
		EntityID,
		MessageTypeID,
		CommunicationMethodID,
		MessagesPerMinutes,
		MessagesNumberOfMinutes,
		MessagesPerDays,
		MessagesNumberOfDays,
		IsDisabled,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		d.CommunicationConfigurationID,
		d.ApplicationID,
		d.BusinessID,
		d.EntityID,
		d.MessageTypeID,
		d.CommunicationMethodID,
		MessagesPerMinutes,
		MessagesNumberOfMinutes,
		MessagesPerDays,
		MessagesNumberOfDays,
		IsDisabled,
		d.rowguid,	
		d.DateCreated,	
		d.DateModified,
		d.CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_SNAME()
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d


	--SELECT * FROM comm.CommunicationConfiguration
	--SELECT * FROM comm.CommunicationConfiguration_History
	
	

END
