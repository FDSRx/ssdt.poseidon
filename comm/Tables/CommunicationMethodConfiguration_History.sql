﻿CREATE TABLE [comm].[CommunicationMethodConfiguration_History] (
    [CommunicationMethodConfigurationHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CommunicationMethodConfigurationID]        BIGINT           NOT NULL,
    [ApplicationID]                             INT              NULL,
    [BusinessID]                                BIGINT           NULL,
    [EntityID]                                  BIGINT           NULL,
    [CommunicationMethodID]                     INT              NOT NULL,
    [rowguid]                                   UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]                               DATETIME         NOT NULL,
    [DateModified]                              DATETIME         NOT NULL,
    [CreatedBy]                                 VARCHAR (256)    NULL,
    [ModifiedBy]                                VARCHAR (256)    NULL,
    [AuditGuid]                                 UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationMethodConfiguration_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                               VARCHAR (10)     NOT NULL,
    [DateAudited]                               DATETIME         CONSTRAINT [DF_CommunicationMethodConfiguration_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CommunicationMethodConfiguration_History] PRIMARY KEY CLUSTERED ([CommunicationMethodConfigurationHistoryID] ASC)
);

