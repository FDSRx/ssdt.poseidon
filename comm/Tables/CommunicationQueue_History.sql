﻿CREATE TABLE [comm].[CommunicationQueue_History] (
    [CommunicationQueueHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CommunicationQueueID]        BIGINT           NULL,
    [ApplicationID]               INT              NULL,
    [BusinessID]                  BIGINT           NULL,
    [MessageTypeID]               INT              NULL,
    [CommunicationMethodID]       INT              NULL,
    [CommunicationName]           VARCHAR (256)    NULL,
    [TargetAudienceSourceID]      INT              NULL,
    [RunOn]                       DATETIME         NULL,
    [DateStart]                   DATETIME         NULL,
    [DateEnd]                     DATETIME         NULL,
    [ExecutionStatusID]           INT              NULL,
    [rowguid]                     UNIQUEIDENTIFIER NULL,
    [DateCreated]                 DATETIME         NULL,
    [DateModified]                DATETIME         NULL,
    [CreatedBy]                   VARCHAR (256)    NULL,
    [ModifiedBy]                  VARCHAR (256)    NULL,
    [AuditGuid]                   UNIQUEIDENTIFIER CONSTRAINT [DF_CommunicationQueue_History_AuditGuid] DEFAULT (newid()) NULL,
    [AuditAction]                 VARCHAR (10)     NULL,
    [DateAudited]                 DATETIME         CONSTRAINT [DF_CommunicationQueue_History_DateAudited] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_CommunicationQueue_History] PRIMARY KEY CLUSTERED ([CommunicationQueueHistoryID] ASC)
);

