﻿CREATE TABLE [comm].[MessageTypeConfiguration_History] (
    [MessageTypeConfigurationHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [MessageTypeConfigurationID]        BIGINT           NOT NULL,
    [ApplicationID]                     INT              NULL,
    [BusinessID]                        BIGINT           NULL,
    [EntityID]                          BIGINT           NULL,
    [MessageTypeID]                     INT              NOT NULL,
    [rowguid]                           UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]                       DATETIME         NOT NULL,
    [DateModified]                      DATETIME         NOT NULL,
    [CreatedBy]                         VARCHAR (256)    NULL,
    [ModifiedBy]                        VARCHAR (256)    NULL,
    [AuditGuid]                         UNIQUEIDENTIFIER CONSTRAINT [DF_MessageTypeConfiguration_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                       VARCHAR (10)     NOT NULL,
    [DateAudited]                       DATETIME         CONSTRAINT [DF_MessageTypeConfiguration_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MessageTypeConfiguration_History] PRIMARY KEY CLUSTERED ([MessageTypeConfigurationHistoryID] ASC)
);

