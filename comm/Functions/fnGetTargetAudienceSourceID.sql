﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/21/2015
-- Description:	Returns the TargetAudienceSource object identifier.

-- SAMPLE CALL: SELECT comm.fnGetTargetAudienceSourceID('MDMENGAP')
-- SAMPLE CALL: SELECT comm.fnGetTargetAudienceSourceID(1)

-- SELECT * FROM comm.TargetAudienceSource
-- =============================================
CREATE FUNCTION [comm].[fnGetTargetAudienceSourceID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT TargetAudienceSourceID FROM comm.TargetAudienceSource WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT TargetAudienceSourceID FROM comm.TargetAudienceSource WHERE TargetAudienceSourceID = @Key);
	END

	-- Return the result of the function
	RETURN @ID;

END

