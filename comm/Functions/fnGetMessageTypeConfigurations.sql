﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/21/2015
-- Description:	Returns a list of MessageType objects.
-- SAMPLE CALL:
/*

SELECT * FROM comm.fnGetMessageTypeConfigurations('ENG', DEFAULT, DEFAULT)
SELECT * FROM comm.fnGetMessageTypeConfigurations('MPC', DEFAULT, DEFAULT)
SELECT * FROM comm.fnGetMessageTypeConfigurations('ENG', 10684, DEFAULT)

*/

-- SELECT * FROM comm.MessageTypeConfiguration
-- SELECT * FROM comm.MessageType
-- =============================================
CREATE FUNCTION [comm].[fnGetMessageTypeConfigurations]
(
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@EntityID BIGINT = NULL
)
RETURNS @tblResult TABLE (
	MessageTypeConfigurationID BIGINT,
	ApplicationID INT,
	ApplicationCode VARCHAR(50),
	ApplicationName VARCHAR(50),
	BusinessID BIGINT,
	EntityID BIGINT,
	MessageTypeID INT,
	Code VARCHAR(25),
	Name VARCHAR(50),
	Description VARCHAR(1000),
	rowguid UNIQUEIDENTIFIER,
	DateCreated DATETIME,
	DateModified DATETIME,
	CreatedBy VARCHAR(256),
	ModifiedBy VARCHAR(256)
)
AS
BEGIN


	---------------------------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------------------------	
	DECLARE
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, DEFAULT),
		@RowCount INT = 0
	

	---------------------------------------------------------------------------------------
	-- Temporary resources.
	---------------------------------------------------------------------------------------		
	DECLARE @tblConfigurationArray AS TABLE (
		MessageTypeConfigurationID BIGINT
	);

	---------------------------------------------------------------------------------------
	-- Retrieve data.
	-- <Summary>
	-- Uses a hierarchical approach to determine the correct result set.
	-- </Summary>
	---------------------------------------------------------------------------------------	
	---------------------------------------------------------------------------------------
	-- Compile data.
	---------------------------------------------------------------------------------------	
	-- Attempt an exact match from the specified criteria.
	INSERT INTO @tblConfigurationArray (
		MessageTypeConfigurationID
	)
	SELECT
		MessageTypeConfigurationID
	--SELECT *
	FROM comm.MessageTypeConfiguration (NOLOCK)
	WHERE ( ApplicationID = @ApplicationID )
		AND ( BusinessID = @BusinessID )
		AND ( EntityID = @EntityID )	
	
	SET @RowCount = @@ROWCOUNT;
		
	-- If no rows were discovered then attempt a match on the business and application.
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblConfigurationArray (
			MessageTypeConfigurationID
		)
		SELECT
			MessageTypeConfigurationID
		--SELECT *
		FROM comm.MessageTypeConfiguration (NOLOCK)
		WHERE ( ApplicationID = @ApplicationID )
			AND ( BusinessID = @BusinessID )
			AND ( EntityID IS NULL	)
	
		SET @RowCount = @@ROWCOUNT;
	
	END
	
	-- If no rows were discovered then attempt a match on the application
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblConfigurationArray (
			MessageTypeConfigurationID
		)
		SELECT
			MessageTypeConfigurationID
		--SELECT *
		FROM comm.MessageTypeConfiguration (NOLOCK)
		WHERE ( ApplicationID = @ApplicationID )
			AND ( BusinessID IS NULL )
			AND ( EntityID IS NULL	)
	
		SET @RowCount = @@ROWCOUNT;
		
	END
	
	-- If no rows were discovered then attempt a match of NULLs across all criterion.
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblConfigurationArray (
			MessageTypeConfigurationID
		)
		SELECT
			MessageTypeConfigurationID
		--SELECT *
		FROM comm.MessageTypeConfiguration (NOLOCK)
		WHERE ( ApplicationID IS NULL )
			AND ( BusinessID IS NULL )
			AND ( EntityID IS NULL )
	
		SET @RowCount = @@ROWCOUNT;
	END
	
	---------------------------------------------------------------------------------------
	-- Return result set.
	---------------------------------------------------------------------------------------	
	INSERT INTO @tblResult (
		MessageTypeConfigurationID,
		ApplicationID,
		ApplicationCode,
		ApplicationName,
		BusinessID,
		EntityID,
		MessageTypeID,
		Code,
		Name,
		Description,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	)		
	SELECT
		cfg.MessageTypeConfigurationID,
		cfg.ApplicationID,
		app.Code AS ApplicationCode,
		app.Name AS ApplicationName,
		cfg.BusinessID,
		cfg.EntityID,
		cfg.MessageTypeID,
		typ.Code,
		typ.Name,
		typ.Description,
		cfg.rowguid,
		cfg.DateCreated,
		cfg.DateModified,
		cfg.CreatedBy,
		cfg.ModifiedBy
	FROM @tblConfigurationArray arr
		JOIN comm.MessageTypeConfiguration cfg (NOLOCK)
			ON arr.MessageTypeConfigurationID = cfg.MessageTypeConfigurationID
		JOIN comm.MessageType typ (NOLOCK)
			ON cfg.MessageTypeID = typ.MessageTypeID
		LEFT JOIN dbo.Application app (NOLOCK)
			ON cfg.ApplicationID = app.ApplicationID
	
	
	RETURN;

END

