﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/20/2015
-- Description: Gets the translated MessageType key or keys.
-- References: dbo.fnSlice
-- References: dbo.fnSplit

-- SAMPLE CALL: SELECT comm.fnMessageTypeKeyTranslator(1, 'MessageTypeID')
-- SAMPLE CALL: SELECT comm.fnMessageTypeKeyTranslator('HPPYBDAY', DEFAULT)
-- SAMPLE CALL: SELECT comm.fnMessageTypeKeyTranslator(3, DEFAULT)
-- SAMPLE CALL: SELECT comm.fnMessageTypeKeyTranslator('1,2,3', 'MessageTypeIDList')
-- SAMPLE CALL: SELECT comm.fnMessageTypeKeyTranslator('HPPYBDAY,MISSYOU,WLCME', 'CodeList')
-- SAMPLE CALL: SELECT comm.fnMessageTypeKeyTranslator('HPPYBDAY,MISSYOU,WLCME', DEFAULT)
-- SAMPLE CALL: SELECT comm.fnMessageTypeKeyTranslator('1,2,3', DEFAULT)

-- SELECT * FROM comm.MessageType
-- =============================================
CREATE FUNCTION [comm].[fnMessageTypeKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END	
	
	-- Determine if there is a value to evaluate.
	IF @Key IS NULL OR @Key = ''
	BEGIN
		RETURN @Ids;
	END	
	
	-- Evaluate input and translate accordingly.
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'MessageTypeID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, MessageTypeID) 
				FROM comm.MessageType
				WHERE MessageTypeID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, MessageTypeID) 
				FROM comm.MessageType
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, MessageTypeID) 
				FROM comm.MessageType
				WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, MessageTypeID) 
				FROM comm.MessageType
				WHERE MessageTypeID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, MessageTypeID) 
				FROM comm.MessageType
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'MessageTypeIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, MessageTypeID), '') + ','
				FROM comm.MessageType
				WHERE MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'MessageTypeCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, MessageTypeID), '') + ','
				FROM comm.MessageType
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'MessageTypeNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, MessageTypeID), '') + ','
				FROM comm.MessageType
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, MessageTypeID), '') + ','
				FROM comm.MessageType
				WHERE MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, MessageTypeID), '') + ','
				FROM comm.MessageType
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
