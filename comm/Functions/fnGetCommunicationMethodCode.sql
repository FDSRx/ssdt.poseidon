﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/3/2015
-- Description:	Gets the Code property value of the CommunicationMethod object.
-- SAMPLE CALL: SELECT comm.fnGetCommunicationMethodCode('EMAIL')
-- SAMPLE CALL: SELECT comm.fnGetCommunicationMethodCode(1)
-- =============================================
CREATE FUNCTION comm.[fnGetCommunicationMethodCode] 
(
	@Key VARCHAR(50)
)
RETURNS VARCHAR(25)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Code VARCHAR(25)

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Code = (SELECT Code FROM comm.CommunicationMethod WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @Code = (SELECT Code FROM comm.CommunicationMethod WHERE CommunicationMethodID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @Code;

END

