﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/19/2014
-- Description: Gets the translated Application key or keys.
-- References: dbo.fnSlice
-- References: dbo.fnSplit

-- SAMPLE CALL: SELECT comm.fnCommunicationMethodKeyTranslator(1, 'CommunicationMethodID')
-- SAMPLE CALL: SELECT comm.fnCommunicationMethodKeyTranslator('MAIL', DEFAULT)
-- SAMPLE CALL: SELECT comm.fnCommunicationMethodKeyTranslator(3, DEFAULT)
-- SAMPLE CALL: SELECT comm.fnCommunicationMethodKeyTranslator('1,2,3', 'CommunicationMethodIDList')
-- SAMPLE CALL: SELECT comm.fnCommunicationMethodKeyTranslator('EMAIL,MAIL,PHONE', 'CodeList')
-- SAMPLE CALL: SELECT comm.fnCommunicationMethodKeyTranslator('EMAIL,MAIL,PHONE', DEFAULT)
-- SAMPLE CALL: SELECT comm.fnCommunicationMethodKeyTranslator('1,2,3', DEFAULT)

-- SELECT * FROM comm.CommunicationMethod
-- =============================================
CREATE FUNCTION [comm].[fnCommunicationMethodKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END	
	
	-- Detemrine if there is a value to evaluate.
	IF @Key IS NULL OR @Key = ''
	BEGIN
		RETURN @Ids;
	END	
	
	-- Evaluate input and translate accordingly.
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'CommunicationMethodID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, CommunicationMethodID) 
				FROM comm.CommunicationMethod 
				WHERE CommunicationMethodID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, CommunicationMethodID) 
				FROM comm.CommunicationMethod 
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, CommunicationMethodID) 
				FROM comm.CommunicationMethod 
				WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, CommunicationMethodID) 
				FROM comm.CommunicationMethod 
				WHERE CommunicationMethodID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, CommunicationMethodID) 
				FROM comm.CommunicationMethod 
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'CommunicationMethodIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, CommunicationMethodID), '') + ','
				FROM comm.CommunicationMethod
				WHERE CommunicationMethodID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'CommunicationMethodCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, CommunicationMethodID), '') + ','
				FROM comm.CommunicationMethod
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'CommunicationMethodNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, CommunicationMethodID), '') + ','
				FROM comm.CommunicationMethod
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, CommunicationMethodID), '') + ','
				FROM comm.CommunicationMethod
				WHERE CommunicationMethodID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, CommunicationMethodID), '') + ','
				FROM comm.CommunicationMethod
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
