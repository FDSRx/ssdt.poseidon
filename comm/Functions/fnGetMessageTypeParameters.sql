﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/14/2015
-- Description:	Returns a list of MessageTypeParameter objects.
-- SAMPLE CALL:
/*

SELECT * FROM comm.fnGetMessageTypeParameters('ENG', DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
SELECT * FROM comm.fnGetMessageTypeParameters('MPC', DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
SELECT * FROM comm.fnGetMessageTypeParameters('ENG', 38, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
SELECT * FROM comm.fnGetMessageTypeParameters('ENG', 49, DEFAULT, DEFAULT, DEFAULT, DEFAULT)

*/

-- SELECT * FROM comm.MessageTypeParameter
-- SELECT * FROM comm.MessageType
-- =============================================
CREATE FUNCTION [comm].[fnGetMessageTypeParameters]
(
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@EntityKey VARCHAR(50) = NULL,
	@EntityKeyType VARCHAR(50) = NULL,
	@MessageTypeKey VARCHAR(50) = NULL
)
RETURNS @tblResult TABLE (
	MessageTypeParameterID BIGINT,
	ApplicationID INT,
	ApplicationCode VARCHAR(50),
	ApplicationName VARCHAR(50),
	BusinessID BIGINT,
	EntityID BIGINT,
	MessageTypeID INT,
	MessageTypeCode VARCHAR(25),
	MessageTypeName VARCHAR(50),
	MessageTypeDescription VARCHAR(1000),
	ParameterID INT,
	ParameterCode VARCHAR(25),
	ParameterName VARCHAR(256),
	ParameterDescription VARCHAR(1000),
	ValueString VARCHAR(MAX),
	TypeOf VARCHAR(50),
	rowguid UNIQUEIDENTIFIER,
	DateCreated DATETIME,
	DateModified DATETIME,
	CreatedBy VARCHAR(256),
	ModifiedBy VARCHAR(256)
)
AS
BEGIN


	---------------------------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------------------------	
	DECLARE
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType),
		@MessageTypeID INT = comm.fnGetMessageTypeID(@MessageTypeKey),
		@EntityID BIGINT = NULL,
		@RowCount INT = 0
	

	---------------------------------------------------------------------------------------
	-- Temporary resources.
	---------------------------------------------------------------------------------------		
	DECLARE @tblItems AS TABLE (
		Id BIGINT
	);

	---------------------------------------------------------------------------------------
	-- Retrieve data.
	-- <Summary>
	-- Uses a hierarchical approach to determine the correct result set.
	-- </Summary>
	---------------------------------------------------------------------------------------	
	---------------------------------------------------------------------------------------
	-- Compile data.
	---------------------------------------------------------------------------------------	
	-- Attempt an exact match from the specified criteria.
	INSERT INTO @tblItems (
		Id
	)
	SELECT
		MessageTypeParameterID
	--SELECT *
	FROM comm.MessageTypeParameter (NOLOCK)
	WHERE ( ApplicationID = @ApplicationID )
		AND ( BusinessID = @BusinessID )
		AND ( EntityID = @EntityID )	
		AND ( MessageTypeID = @MessageTypeID )
	
	SET @RowCount = @@ROWCOUNT;
	
	-- If no rows were discovered then attempt a match on the business, application, and entity.
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblItems (
			Id
		)
		SELECT
			MessageTypeParameterID
		--SELECT *
		FROM comm.MessageTypeParameter (NOLOCK)
		WHERE ( ApplicationID = @ApplicationID )
			AND ( BusinessID = @BusinessID )
			AND ( EntityID = @EntityID )
		
		SET @RowCount = @@ROWCOUNT;
	
	END
	
	-- If no rows were discovered then attempt a match on the application and business
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblItems (
			Id
		)
		SELECT
			MessageTypeParameterID
		--SELECT *
		FROM comm.MessageTypeParameter (NOLOCK)
		WHERE ( ApplicationID = @ApplicationID )
			AND ( BusinessID = @BusinessID )
			AND ( EntityID IS NULL	)
			
		SET @RowCount = @@ROWCOUNT;
	
	END
	
	-- If no rows were discovered then attempt a match on the business.
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblItems (
			Id
		)
		SELECT
			MessageTypeParameterID
		--SELECT *
		FROM comm.MessageTypeParameter (NOLOCK)
		WHERE ( ApplicationID IS NULL )
			AND ( BusinessID = @BusinessID )
			AND ( EntityID IS NULL	)
	
		SET @RowCount = @@ROWCOUNT;
	END

	-- If no rows were discovered then attempt a match on the application
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblItems (
			Id
		)
		SELECT
			MessageTypeParameterID
		--SELECT *
		FROM comm.MessageTypeParameter (NOLOCK)
		WHERE ( ApplicationID = @ApplicationID )
			AND ( BusinessID IS NULL )
			AND ( EntityID IS NULL	)
	
		SET @RowCount = @@ROWCOUNT;
	END	
	
	-- If no rows were discovered then attempt a match of NULLs across all criterion.
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblItems (
			Id
		)
		SELECT
			MessageTypeParameterID
		--SELECT *
		FROM comm.MessageTypeParameter (NOLOCK)
		WHERE ( ApplicationID IS NULL )
			AND ( BusinessID IS NULL )
			AND ( EntityID IS NULL )
			AND MessageTypeID IS NOT NULL
			AND ParameterID IS NOT NULL
		
		SET @RowCount = @@ROWCOUNT;
	
	END
	
	---------------------------------------------------------------------------------------
	-- Return result set.
	---------------------------------------------------------------------------------------	
	INSERT INTO @tblResult (
		MessageTypeParameterID,
		ApplicationID,
		ApplicationCode,
		ApplicationName,
		BusinessID,
		EntityID,
		MessageTypeID,
		MessageTypeCode,
		MessageTypeName,
		MessageTypeDescription,
		ParameterID,
		ParameterCode,
		ParameterName,
		ParameterDescription,
		ValueString,
		TypeOf,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	)		
	SELECT
		trgt.MessageTypeParameterID,
		trgt.ApplicationID,
		app.Code AS ApplicationCode,
		app.Name AS ApplicationName,
		trgt.BusinessID,
		trgt.EntityID,
		trgt.MessageTypeID,
		typ.Code AS MessageTypeCode,
		typ.Name AS MessageTypeName,
		typ.Description AS MessageTypeDescription,
		trgt.ParameterID,
		parm.Code AS ParameterCode,
		parm.Name AS ParameterName,
		NULL AS ParameterDescription,
		trgt.ValueString,
		trgt.TypeOf,
		trgt.rowguid,
		trgt.DateCreated,
		trgt.DateModified,
		trgt.CreatedBy,
		trgt.ModifiedBy
	FROM @tblItems arr
		JOIN comm.MessageTypeParameter trgt (NOLOCK)
			ON arr.Id = trgt.MessageTypeParameterID
		JOIN comm.MessageType typ (NOLOCK)
			ON trgt.MessageTypeID = typ.MessageTypeID
		JOIN data.Parameter parm (NOLOCK)
			ON trgt.ParameterID = parm.ParameterID
		LEFT JOIN dbo.Application app (NOLOCK)
			ON trgt.ApplicationID = app.ApplicationID
	
	
	RETURN;

END

