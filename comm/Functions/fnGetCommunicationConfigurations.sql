﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/21/2015
-- Description:	Returns a list of CommunicationConfiguration objects.
-- SAMPLE CALL:
/*

SELECT * FROM comm.fnGetCommunicationConfigurations('ENG', DEFAULT, DEFAULT, DEFAULT)
SELECT * FROM comm.fnGetCommunicationConfigurations('MPC', DEFAULT, DEFAULT, DEFAULT)
SELECT * FROM comm.fnGetCommunicationConfigurations('ENG', 10684, DEFAULT, DEFAULT)

*/

-- SELECT * FROM comm.CommunicationConfiguration
-- SELECT * FROM comm.MessageType
-- SELECT * FROM comm.CommunicationMethod
-- =============================================
CREATE FUNCTION [comm].[fnGetCommunicationConfigurations]
(
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@EntityID BIGINT = NULL,
	@MessageTypeKey VARCHAR(50) = NULL
)
RETURNS @tblResult TABLE (
	CommunicationConfigurationID BIGINT,
	ApplicationID INT,
	ApplicationCode VARCHAR(50),
	ApplicationName VARCHAR(50),
	BusinessID BIGINT,
	EntityID BIGINT,
	MessageTypeID INT,
	MessageTypeCode VARCHAR(25),
	MessageTypeName VARCHAR(50),
	MessageTypeDescription VARCHAR(1000),
	CommunicationMethodID INT,
	CommunicationMethodCode VARCHAR(25),
	CommunicationMethodName VARCHAR(50),
	CommunicationMethodDescription VARCHAR(1000),
	MessagesPerMinutes INT,
	MessagesNumberOfMinutes INT,
	MessagesPerDays INT,
	MessagesNumberOfDays INT,
	IsDisabled BIT,
	rowguid UNIQUEIDENTIFIER,
	DateCreated DATETIME,
	DateModified DATETIME,
	CreatedBy VARCHAR(256),
	ModifiedBy VARCHAR(256)
)
AS
BEGIN


	---------------------------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------------------------	
	DECLARE
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, DEFAULT),
		@MessageTypeID INT = comm.fnGetMessageTypeID(@MessageTypeKey),
		@RowCount INT = 0
	

	---------------------------------------------------------------------------------------
	-- Temporary resources.
	---------------------------------------------------------------------------------------		
	DECLARE @tblConfigurationArray AS TABLE (
		CommunicationConfigurationID BIGINT
	);

	---------------------------------------------------------------------------------------
	-- Retrieve data.
	-- <Summary>
	-- Uses a hierarchical approach to determine the correct result set.
	-- </Summary>
	---------------------------------------------------------------------------------------	
	---------------------------------------------------------------------------------------
	-- Compile data.
	---------------------------------------------------------------------------------------	
	-- Attempt an exact match from the specified criteria.
	INSERT INTO @tblConfigurationArray (
		CommunicationConfigurationID
	)
	SELECT
		CommunicationConfigurationID
	--SELECT *
	FROM comm.CommunicationConfiguration (NOLOCK)
	WHERE ( ApplicationID = @ApplicationID )
		AND ( BusinessID = @BusinessID )
		AND ( EntityID = @EntityID )	
		AND ( MessageTypeID = @MessageTypeID )
	
	SET @RowCount = @@ROWCOUNT;
	
	-- If no rows were discovered then attempt a match on the business, application, and entity.
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblConfigurationArray (
			CommunicationConfigurationID
		)
		SELECT
			CommunicationConfigurationID
		--SELECT *
		FROM comm.CommunicationConfiguration (NOLOCK)
		WHERE ( ApplicationID = @ApplicationID )
			AND ( BusinessID = @BusinessID )
			AND ( EntityID = @EntityID )
		
		SET @RowCount = @@ROWCOUNT;
	
	END
	
	-- If no rows were discovered then attempt a match on the application and business
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblConfigurationArray (
			CommunicationConfigurationID
		)
		SELECT
			CommunicationConfigurationID
		--SELECT *
		FROM comm.CommunicationConfiguration (NOLOCK)
		WHERE ( ApplicationID = @ApplicationID )
			AND ( BusinessID = @BusinessID )
			AND ( EntityID IS NULL	)
			
		SET @RowCount = @@ROWCOUNT;
	
	END
	
	-- If no rows were discovered then attempt a match on the application
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblConfigurationArray (
			CommunicationConfigurationID
		)
		SELECT
			CommunicationConfigurationID
		--SELECT *
		FROM comm.CommunicationConfiguration (NOLOCK)
		WHERE ( ApplicationID = @ApplicationID )
			AND ( BusinessID IS NULL )
			AND ( EntityID IS NULL	)
	
		SET @RowCount = @@ROWCOUNT;
	END
	
	
	-- If no rows were discovered then attempt a match of NULLs across all criterion.
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblConfigurationArray (
			CommunicationConfigurationID
		)
		SELECT
			CommunicationConfigurationID
		--SELECT *
		FROM comm.CommunicationConfiguration (NOLOCK)
		WHERE ( ApplicationID IS NULL )
			AND ( BusinessID IS NULL )
			AND ( EntityID IS NULL )
			AND MessageTypeID IS NOT NULL
			AND CommunicationMethodID IS NOT NULL
		
		SET @RowCount = @@ROWCOUNT;
	
	END
	
	---------------------------------------------------------------------------------------
	-- Return result set.
	---------------------------------------------------------------------------------------	
	INSERT INTO @tblResult (
		CommunicationConfigurationID,
		ApplicationID,
		ApplicationCode,
		ApplicationName,
		BusinessID,
		EntityID,
		MessageTypeID,
		MessageTypeCode,
		MessageTypeName,
		MessageTypeDescription,
		CommunicationMethodID,
		CommunicationMethodCode,
		CommunicationMethodName,
		CommunicationMethodDescription,
		MessagesPerMinutes,
		MessagesNumberOfMinutes,
		MessagesPerDays,
		MessagesNumberOfDays,
		IsDisabled,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	)		
	SELECT
		cfg.CommunicationConfigurationID,
		cfg.ApplicationID,
		app.Code AS ApplicationCode,
		app.Name AS ApplicationName,
		cfg.BusinessID,
		cfg.EntityID,
		cfg.MessageTypeID,
		typ.Code AS MessageTypeCode,
		typ.Name AS MessageTypeName,
		typ.Description AS MessageTypeDescription,
		cfg.CommunicationMethodID,
		meth.Code AS CommunicationMethodCode,
		meth.Name AS CommunicationMethodName,
		meth.Description AS CommunicationMethodDescription,
		cfg.MessagesPerMinutes,
		cfg.MessagesNumberOfMinutes,
		cfg.MessagesPerDays,
		cfg.MessagesNumberOfDays,
		cfg.IsDisabled,
		cfg.rowguid,
		cfg.DateCreated,
		cfg.DateModified,
		cfg.CreatedBy,
		cfg.ModifiedBy
	FROM @tblConfigurationArray arr
		JOIN comm.CommunicationConfiguration cfg (NOLOCK)
			ON arr.CommunicationConfigurationID = cfg.CommunicationConfigurationID
		LEFT JOIN comm.MessageType typ (NOLOCK)
			ON cfg.MessageTypeID = typ.MessageTypeID
		JOIN comm.CommunicationMethod meth (NOLOCK)
			ON cfg.CommunicationMethodID = meth.CommunicationMethodID
		LEFT JOIN dbo.Application app (NOLOCK)
			ON cfg.ApplicationID = app.ApplicationID
	
	
	RETURN;

END

