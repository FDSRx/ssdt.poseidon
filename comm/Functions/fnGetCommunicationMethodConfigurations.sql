﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/25/2015
-- Description:	Returns a list of CommunicationMethod objects.
-- SAMPLE CALL:
/*

SELECT * FROM comm.fnGetCommunicationMethodConfigurations('ENG', DEFAULT, DEFAULT)
SELECT * FROM comm.fnGetCommunicationMethodConfigurations('MPC', DEFAULT, DEFAULT)
SELECT * FROM comm.fnGetCommunicationMethodConfigurations('ENG', 10684, DEFAULT)

*/

-- SELECT * FROM comm.CommunicationMethodConfiguration
-- SELECT * FROM comm.CommunicationMethod
-- =============================================
CREATE FUNCTION [comm].[fnGetCommunicationMethodConfigurations]
(
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@EntityID BIGINT = NULL
)
RETURNS @tblResult TABLE (
	CommunicationMethodConfigurationID BIGINT,
	ApplicationID INT,
	ApplicationCode VARCHAR(50),
	ApplicationName VARCHAR(50),
	BusinessID BIGINT,
	EntityID BIGINT,
	CommunicationMethodID INT,
	Code VARCHAR(25),
	Name VARCHAR(50),
	Description VARCHAR(1000),
	rowguid UNIQUEIDENTIFIER,
	DateCreated DATETIME,
	DateModified DATETIME,
	CreatedBy VARCHAR(256),
	ModifiedBy VARCHAR(256)
)
AS
BEGIN


	---------------------------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------------------------	
	DECLARE
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, DEFAULT),
		@RowCount INT = 0
	

	---------------------------------------------------------------------------------------
	-- Temporary resources.
	---------------------------------------------------------------------------------------		
	DECLARE @tblConfigurationArray AS TABLE (
		CommunicationMethodConfigurationID BIGINT
	);

	---------------------------------------------------------------------------------------
	-- Retrieve data.
	-- <Summary>
	-- Uses a hierarchical approach to determine the correct result set.
	-- </Summary>
	---------------------------------------------------------------------------------------	
	---------------------------------------------------------------------------------------
	-- Compile data.
	---------------------------------------------------------------------------------------	
	-- Attempt an exact match from the specified criteria.
	INSERT INTO @tblConfigurationArray (
		CommunicationMethodConfigurationID
	)
	SELECT
		CommunicationMethodConfigurationID
	--SELECT *
	FROM comm.CommunicationMethodConfiguration (NOLOCK)
	WHERE ( ApplicationID = @ApplicationID )
		AND ( BusinessID = @BusinessID )
		AND ( EntityID = @EntityID )	
	
	SET @RowCount = @@ROWCOUNT;
		
	-- If no rows were discovered then attempt a match on the business and application.
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblConfigurationArray (
			CommunicationMethodConfigurationID
		)
		SELECT
			CommunicationMethodConfigurationID
		--SELECT *
		FROM comm.CommunicationMethodConfiguration (NOLOCK)
		WHERE ( ApplicationID = @ApplicationID )
			AND ( BusinessID = @BusinessID )
			AND ( EntityID IS NULL	)
	
		SET @RowCount = @@ROWCOUNT;
	
	END
	
	-- If no rows were discovered then attempt a match on the application
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblConfigurationArray (
			CommunicationMethodConfigurationID
		)
		SELECT
			CommunicationMethodConfigurationID
		--SELECT *
		FROM comm.CommunicationMethodConfiguration (NOLOCK)
		WHERE ( ApplicationID = @ApplicationID )
			AND ( BusinessID IS NULL )
			AND ( EntityID IS NULL	)
	
		SET @RowCount = @@ROWCOUNT;
		
	END
	
	-- If no rows were discovered then attempt a match of NULLs across all criterion.
	IF @RowCount = 0
	BEGIN
	
		INSERT INTO @tblConfigurationArray (
			CommunicationMethodConfigurationID
		)
		SELECT
			CommunicationMethodConfigurationID
		--SELECT *
		FROM comm.CommunicationMethodConfiguration (NOLOCK)
		WHERE ( ApplicationID IS NULL )
			AND ( BusinessID IS NULL )
			AND ( EntityID IS NULL )
	
		SET @RowCount = @@ROWCOUNT;
	END
	
	---------------------------------------------------------------------------------------
	-- Return result set.
	---------------------------------------------------------------------------------------	
	INSERT INTO @tblResult (
		CommunicationMethodConfigurationID,
		ApplicationID,
		ApplicationCode,
		ApplicationName,
		BusinessID,
		EntityID,
		CommunicationMethodID,
		Code,
		Name,
		Description,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	)		
	SELECT
		cfg.CommunicationMethodConfigurationID,
		cfg.ApplicationID,
		app.Code AS ApplicationCode,
		app.Name AS ApplicationName,
		cfg.BusinessID,
		cfg.EntityID,
		cfg.CommunicationMethodID,
		typ.Code,
		typ.Name,
		typ.Description,
		cfg.rowguid,
		cfg.DateCreated,
		cfg.DateModified,
		cfg.CreatedBy,
		cfg.ModifiedBy
	FROM @tblConfigurationArray arr
		JOIN comm.CommunicationMethodConfiguration cfg (NOLOCK)
			ON arr.CommunicationMethodConfigurationID = cfg.CommunicationMethodConfigurationID
		JOIN comm.CommunicationMethod typ (NOLOCK)
			ON cfg.CommunicationMethodID = typ.CommunicationMethodID
		LEFT JOIN dbo.Application app (NOLOCK)
			ON cfg.ApplicationID = app.ApplicationID
	
	
	RETURN;

END

