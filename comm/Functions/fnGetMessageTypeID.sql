﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/20/2015
-- Description:	Returns the MessageType object identifier.
-- SAMPLE CALL: SELECT comm.fnGetMessageTypeID('WLCME')
-- SAMPLE CALL: SELECT comm.fnGetMessageTypeID(4)

-- SELECT * FROM comm.MessageType
-- =============================================
CREATE FUNCTION [comm].[fnGetMessageTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT MessageTypeID FROM comm.MessageType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT MessageTypeID FROM comm.MessageType WHERE MessageTypeID = @Key);
	END

	-- Return the result of the function
	RETURN @ID;

END

