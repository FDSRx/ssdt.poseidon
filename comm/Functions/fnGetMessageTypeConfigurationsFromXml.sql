﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/21/2015
-- Description:	Parses MessageTypeConfiguration objects from the provided XML data.
-- SAMPLE CALL:
/*
DECLARE @Xml XML = '
<MessageTypeConfigurations>
  <MessageTypeConfiguration>
    <MessageTypeConfigurationID>58</MessageTypeConfigurationID>
    <ApplicationID>10</ApplicationID>
    <BusinessID>10684</BusinessID>
    <MessageTypeID>13</MessageTypeID>
    <Code>APPTRMDR</Code>
    <Name>Appointment Reminder</Name>
    <rowguid>27286A26-DC28-4036-BE34-92B8C06A0163</rowguid>
    <DateCreated>2015-06-29T14:16:43.367</DateCreated>
    <DateModified>2015-06-29T14:16:43.367</DateModified>
    <TotalRecordCount>11</TotalRecordCount>
  </MessageTypeConfiguration>
  <MessageTypeConfiguration>
    <MessageTypeConfigurationID>155</MessageTypeConfigurationID>
    <ApplicationID>10</ApplicationID>
    <BusinessID>10684</BusinessID>
    <MessageTypeID>4</MessageTypeID>
    <Code>APPRAGE</Code>
    <Name>Approaching Age</Name>
    <rowguid>DC96CDCB-265F-46BE-ADD3-F17FCFD1FFF8</rowguid>
    <DateCreated>2015-07-02T14:05:05.143</DateCreated>
    <DateModified>2015-07-02T14:05:05.143</DateModified>
    <CreatedBy>Alex Korolev</CreatedBy>
    <TotalRecordCount>11</TotalRecordCount>
  </MessageTypeConfiguration>
  </MessageTypeConfigurations>';

SELECT *
FROM comm.fnGetMessageTypeConfigurationsFromXml(@Xml);

*/

-- SELECT * FROM comm.MessageTypeConfiguration
-- =============================================
CREATE FUNCTION comm.fnGetMessageTypeConfigurationsFromXml
(	
	@Xml XML
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT
		ns.value('data(MessageTypeConfigurationID)[1]', 'BIGINT') AS MessageTypeConfigurationID,
		ns.value('data(ApplicationID)[1]', 'INT') AS ApplicationID,
		ns.value('data(BusinessID)[1]', 'BIGINT') AS BusinessID,
		ns.value('data(EntityID)[1]', 'BIGINT') AS EntityID,
		ns.value('data(MessageTypeID)[1]', 'INT') AS MessageTypeID,
		ns.value('data(Code)[1]', 'VARCHAR(50)') AS Code,
		ns.value('data(Name)[1]', 'VARCHAR(256)') AS Name,
		ns.value('data(Description)[1]', 'VARCHAR(1000)') AS Description,
		ns.value('data(rowguid)[1]', 'UNIQUEIDENTIFIER') AS rowguid,
		ns.value('data(DateCreated)[1]', 'DATETIME') AS DateCreated,
		ns.value('data(DateModified)[1]', 'DATETIME') AS DateModified,
		ns.value('data(CreatedBy)[1]', 'VARCHAR(256)') AS CreatedBy,
		ns.value('data(ModifiedBy)[1]', 'VARCHAR(256)') AS ModifiedBy
	FROM @Xml.nodes('*:MessageTypeConfigurations/*:MessageTypeConfiguration') AS n(ns)
)
