﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/15/2015
-- Description:	Returns the CommunicationMethod object identifier.
-- SAMPLE CALL: SELECT comm.fnGetCommunicationMethodID('EMAIL')
-- SAMPLE CALL: SELECT comm.fnGetCommunicationMethodID(4)

-- SELECT * FROM comm.CommunicationMethod
-- =============================================
CREATE FUNCTION comm.[fnGetCommunicationMethodID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT CommunicationMethodID FROM comm.CommunicationMethod WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT CommunicationMethodID FROM comm.CommunicationMethod WHERE CommunicationMethodID = @Key);
	END

	-- Return the result of the function
	RETURN @ID;

END

