﻿







CREATE VIEW [comm].[vwCommunicationQueueParam]
AS
SELECT
	qparm.CommunicationQueueParamID,
	qparm.CommunicationQueueID,
	qparm.ParameterID,
	parm.Code AS ParameterCode,
	parm.Name AS ParameterName,
	qparm.ValueString,
	qparm.TypeOf,
	qparm.rowguid,
	qparm.DateCreated,
	qparm.DateModified,
	qparm.CreatedBy,
	qparm.ModifiedBy
--SELECT *
FROM comm.CommunicationQueueParam qparm
	LEFT JOIN data.Parameter parm
		ON qparm.ParameterID = parm.ParameterID
