﻿






CREATE VIEW comm.vwCommunicationExecutionParam
AS
SELECT
	exeparm.CommunicationExecutionParamID,
	exeparm.CommunicationExecutionID,
	exeparm.ParameterID,
	parm.Code AS ParameterCode,
	parm.Name AS ParameterName,
	exeparm.ValueString,
	exeparm.TypeOf,
	exeparm.rowguid,
	exeparm.DateCreated,
	exeparm.DateModified,
	exeparm.CreatedBy,
	exeparm.ModifiedBy
--SELECT *
FROM comm.CommunicationExecutionParam exeparm
	LEFT JOIN data.Parameter parm
		ON exeparm.ParameterID = parm.ParameterID