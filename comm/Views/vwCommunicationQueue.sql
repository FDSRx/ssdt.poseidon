﻿







CREATE VIEW [comm].[vwCommunicationQueue]
AS
SELECT
	q.CommunicationQueueID,
	q.ApplicationID,
	app.Code AS ApplicationCode,
	app.Name AS ApplicationName,
	q.BusinessID,
	sto.Nabp,
	sto.Name AS StoreName,
	q.MessageTypeID,
	typ.Code AS MessageTypeCode,
	typ.Name AS MessageTypeName,
	q.CommunicationMethodID,
	meth.Code AS CommunicationMethodCode,
	meth.Name AS CommunicationMethodName,
	q.CommunicationName,
	q.TargetAudienceSourceID,
	aud.Code AS TargetAudienceSourceCode,
	aud.Name AS TargetAudienceSourceName,
	q.RunOn,
	q.DateStart,
	q.DateEnd,
	q.ExecutionStatusID,
	stat.Code AS ExecutionStatusCode,
	stat.Name AS ExecutionStatusName,
	tz.TimeZoneID,
	tz.Code AS TimeZoneCode,
	tz.Location AS TimeZoneName,
	tz.Prefix AS TimeZonePrefix,
	q.rowguid,
	q.DateCreated,
	q.DateModified,
	q.CreatedBy,
	q.ModifiedBy
--SELECT TOP 1 *
--SELECT *
FROM comm.CommunicationQueue q
	LEFT JOIN dbo.Application app
		ON q.ApplicationID = app.ApplicationID
	LEFT JOIN dbo.Store sto
		ON q.BusinessID = sto.BusinessEntityID
	LEFT JOIN dbo.TimeZone tz
		ON sto.TimeZoneID = tz.TimeZoneID
	LEFT JOIN comm.MessageType typ
		ON q.MessageTypeID = typ.MessageTypeID
	LEFT JOIN comm.CommunicationMethod meth
		ON q.CommunicationMethodID = meth.CommunicationMethodID
	LEFT JOIN etl.ExecutionStatus stat
		ON q.ExecutionStatusID = stat.ExecutionStatusID
	LEFT JOIN comm.TargetAudienceSource aud
		ON q.TargetAudienceSourceID = aud.TargetAudienceSourceID


