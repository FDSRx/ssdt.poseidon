﻿

CREATE VIEW [comm].[vwCommunicationExecution]
AS

SELECT
	exe.CommunicationExecutionID,
	exe.ApplicationID,
	app.Code AS ApplicationCode,
	app.Name AS ApplicationName,
	exe.BusinessID,
	sto.Nabp,
	sto.Name AS StoreName,
	exe.MessageTypeID,
	typ.Code AS MessageTypeCode,
	typ.Name AS MessageTypeName,
	exe.CommunicationMethodID,
	meth.Code AS CommunicationMethodCode,
	meth.Name AS CommunicationMethodName,
	exe.CommunicationName,
	exe.TargetAudienceSourceID,
	aud.Code AS TargetAudienceSourceCode,
	aud.Name AS TargetAudienceSourceName,
	exe.DateScheduled,
	exe.DateStart,
	exe.DateEnd,
	exe.ExecutionStatusID,
	stat.Code AS ExecutionStatusCode,
	stat.Name AS ExecutionStatusName,
	exe.ReadCount,
	exe.WriteCount,
	exe.SkipCount,
	exe.ExitCode,
	exe.ExitMessage,
	exe.Result,
	exe.Message,
	exe.Arguments,
	exe.TransactionGuid,
	exe.rowguid,
	exe.DateCreated,
	exe.DateModified,
	exe.CreatedBy,
	exe.ModifiedBy
FROM comm.CommunicationExecution exe
	LEFT JOIN dbo.Application app
		ON exe.ApplicationID = app.ApplicationID
	LEFT JOIN dbo.Store sto
		ON exe.BusinessID = sto.BusinessEntityID
	LEFT JOIN dbo.TimeZone tz
		ON sto.TimeZoneID = tz.TimeZoneID
	LEFT JOIN comm.MessageType typ
		ON exe.MessageTypeID = typ.MessageTypeID
	LEFT JOIN comm.CommunicationMethod meth
		ON exe.CommunicationMethodID = meth.CommunicationMethodID
	LEFT JOIN etl.ExecutionStatus stat
		ON exe.ExecutionStatusID = stat.ExecutionStatusID 
	LEFT JOIN comm.TargetAudienceSource aud
		ON exe.TargetAudienceSourceID = aud.TargetAudienceSourceID



