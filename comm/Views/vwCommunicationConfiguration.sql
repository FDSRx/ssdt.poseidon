﻿












CREATE VIEW [comm].[vwCommunicationConfiguration] AS 
SELECT
	cc.CommunicationConfigurationID,
	cc.ApplicationID,
	app.Code AS ApplicationCode,
	app.Name AS ApplicationName,
	cc.BusinessID,
	sto.Nabp,
	ISNULL(sto.Name, biz.Name) AS BusinessName,
	cc.EntityID,
	cc.MessageTypeID,
	typ.Code AS MessageTypeCode,
	typ.Name AS MessageTypeName,
	cc.CommunicationMethodID,
	meth.Code AS CommunicationMethodCode,
	meth.Name AS CommunicationMethodName,
	cc.MessagesPerMinutes,
	cc.MessagesNumberOfMinutes,
	cc.MessagesPerDays,
	cc.MessagesNumberOfDays,
	cc.IsDisabled,
	cc.rowguid,
	cc.DateCreated,
	cc.DateModified,
	cc.CreatedBy,
	cc.ModifiedBy
FROM comm.CommunicationConfiguration cc
	LEFT JOIN dbo.Application app
		ON cc.ApplicationID = app.ApplicationID
	LEFT JOIN dbo.Business biz
		ON cc.BusinessID = biz.BusinessEntityID
	LEFT JOIN dbo.Store sto
		ON sto.BusinessEntityID = cc.BusinessID
	LEFT JOIN comm.CommunicationMethod meth
		ON cc.CommunicationMethodID = meth.CommunicationMethodID
	LEFT JOIN comm.MessageType typ
		ON cc.MessageTypeID = typ.MessageTypeID
