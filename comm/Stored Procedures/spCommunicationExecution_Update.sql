﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2015
-- Description:	Updates a CommunicationExecution object.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationExecutionID BIGINT = 2,
	@TransactionDate DATETIME = GETDATE(),
	@ExecutionStatusID INT = etl.fnGetExecutionStatusID('S'),
	@CreatedBy VARCHAR(256) = 'dhughes',
	@ModifiedBy VARCHAR(256) = 'dhughes'

EXEC comm.spCommunicationExecution_Update
	@CommunicationExecutionID = @CommunicationExecutionID OUTPUT,
	@DateEnd = @TransactionDate,
	@ExecutionStatusID = @ExecutionStatusID,
	@ExitCode = NULL,
	@ExitMessage = NULL,
	@Result = NULL,
	@Message = NULL,
	@ModifiedBy = @ModifiedBy,
	@Debug = 1

SELECT @CommunicationExecutionID AS CommunicationExecutionID

*/

-- SELECT * FROM comm.CommunicationExecution
-- SELECT * FROM etl.ExecutionStatus
-- =============================================
CREATE PROCEDURE comm.[spCommunicationExecution_Update]
	@CommunicationExecutionID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL, -- Optional: Can be provided if the object identifier is not known.
	@BusinessID BIGINT = NULL,
	@MessageTypeID INT = NULL,
	@MessageTypeCode VARCHAR(50) = NULL, -- Optional: Can be provided if the object identifier is not known.
	@CommunicationMethodID INT = NULL,
	@CommunicationMethodCode VARCHAR(50) = NULL, -- Optional: Can be provided if the object identifier is not known.
	@CommunicationName VARCHAR(256) = NULL,
	@DateScheduled DATETIME = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@ExecutionStatusID INT = NULL,
	@ExecutionStatusCode VARCHAR(25) = NULL,
	@ReadCount BIGINT = NULL,
	@WriteCount BIGINT = NULL,
	@SkipCount BIGINT = NULL,
	@ExitCode VARCHAR(256) = NULL,
	@ExitMessage VARCHAR(MAX) = NULL,
	@Result VARCHAR(MAX) = NULL,
	@Message VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@Parameters VARCHAR(MAX) = NULL,
	@TransactionGuid UNIQUEIDENTIFIER = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationExecutionID=' + dbo.fnToStringOrEmpty(@CommunicationExecutionID) + ';' +
		-- Objects
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@MessageTypeID=' + dbo.fnToStringOrEmpty(@MessageTypeID) + ';' +
		'@MessageTypeCode=' + dbo.fnToStringOrEmpty(@MessageTypeCode) + ';' +
		'@CommunicationMethodID=' + dbo.fnToStringOrEmpty(@CommunicationMethodID) + ';' +
		'@CommunicationMethodCode=' + dbo.fnToStringOrEmpty(@CommunicationMethodCode) + ';' +
		'@CommunicationName=' + dbo.fnToStringOrEmpty(@CommunicationName) + ';' +
		'@DateScheduled=' + dbo.fnToStringOrEmpty(@DateScheduled) + ';' +
		-- Execution properties.
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart) + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd) + ';' +
		'@ExecutionStatusID=' + dbo.fnToStringOrEmpty(@ExecutionStatusID) + ';' +
		'@ExecutionStatusCode=' + dbo.fnToStringOrEmpty(@ExecutionStatusCode) + ';' +
		'@ReadCount=' + dbo.fnToStringOrEmpty(@ReadCount) + ';' +
		'@WriteCount=' + dbo.fnToStringOrEmpty(@WriteCount) + ';' +
		'@SkipCount=' + dbo.fnToStringOrEmpty(@SkipCount) + ';' +
		'@ExitCode=' + dbo.fnToStringOrEmpty(@ExitCode) + ';' +
		'@ExitMessage=' + dbo.fnToStringOrEmpty(@ExitMessage) + ';' +
		'@Result=' + dbo.fnToStringOrEmpty(@Result) + ';' +
		'@Message=' + dbo.fnToStringOrEmpty(@Message) + ';' +
		'@Arguments=' + dbo.fnToStringOrEmpty(@Arguments) + ';' +
		'@Parameters=' + dbo.fnToStringOrEmpty(@Parameters) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);

	--------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------
	-- No declarations.

	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	IF @CommunicationExecutionID IS NULL
	BEGIN
		RETURN;
	END


	--------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	BEGIN TRY
	--SET @Trancount = @@TRANCOUNT;
	--IF @Trancount = 0
	--BEGIN
	--	BEGIN TRANSACTION;
	--END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------

		--------------------------------------------------------------------------------------------------------
		-- Set variables.
		--------------------------------------------------------------------------------------------------------
		-- ApplicationID
		SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationID);
		SET @ApplicationID = CASE WHEN @ApplicationID IS NOT NULL THEN @ApplicationID ELSE dbo.fnGetApplicationID(@ApplicationCode) END;
		-- MessageTypeID
		SET @MessageTypeID = comm.fnGetMessageTypeID(@MessageTypeID);
		SET @MessageTypeID = CASE WHEN @MessageTypeID IS NOT NULL THEN @MessageTypeID ELSE comm.fnGetMessageTypeID(@MessageTypeCode) END;
		-- CommunicationMethodID
		SET @CommunicationMethodID = comm.fnGetCommunicationMethodID(@CommunicationMethodID);
		SET @CommunicationMethodID = CASE WHEN @CommunicationMethodID IS NOT NULL THEN @CommunicationMethodID ELSE comm.fnGetCommunicationMethodID(@CommunicationMethodCode) END;

		-- Retrieve an ExecutionStatus object by code if one is not provided.
		IF @ExecutionStatusID IS NULL AND @ExecutionStatusCode IS NOT NULL
		BEGIN
			SET @ExecutionStatusID = etl.fnGetExecutionStatusID(@ExecutionStatusCode);
		END

		-- Data review
		-- SELECT * FROM comm.ExecutionStatus

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Setting Variables' AS Method,
				@ApplicationID AS ApplicationID, @ApplicationCode AS ApplicationCode, @BusinessID AS BusinessID,
				@MessageTypeID AS MessageTypeID, @MessageTypeCode AS MessageTypeCode, 
				@CommunicationMethodID AS CommunicationMethodID, @TransactionGuid AS TransactionGuid,
				@CommunicationMethodCode AS CommunicationMethodCode, @DateScheduled AS DateScheduled,
				@DateEnd AS DateEnd, @CommunicationExecutionID AS BatchEntityID,				 
				@DateStart AS DateStart, @DateEnd AS DateEnd, @ExecutionStatusID AS ExecutionStatusID, 
				@ExitCode AS ExitCode, @ExitMessage AS ExitMessage,	@Result AS Result, 
				@Message AS Message, @Arguments AS Arguments, @Parameters AS Params
		END

		--------------------------------------------------------------------------------------------------------
		-- Update object.
		--------------------------------------------------------------------------------------------------------
		UPDATE obj
		SET
			obj.DateStart = CASE WHEN @DateStart IS NULL THEN obj.DateStart ELSE @DateStart END,
			obj.DateEnd = CASE WHEN @DateEnd IS NULL THEN obj.DateEnd ELSE @DateEnd END,
			obj.ExecutionStatusID = CASE WHEN @ExecutionStatusID IS NULL THEN obj.ExecutionStatusID ELSE @ExecutionStatusID END,
			obj.ReadCount = CASE WHEN @ReadCount IS NULL THEN obj.ReadCount ELSE @ReadCount END,
			obj.WriteCount = CASE WHEN @WriteCount IS NULL THEN obj.WriteCount ELSE @WriteCount END,
			obj.SkipCount = CASE WHEN @SkipCount IS NULL THEN obj.SkipCount ELSE @SkipCount END,
			obj.ExitCode = CASE WHEN @ExitCode IS NULL THEN obj.ExitCode ELSE @ExitCode END,
			obj.ExitMessage = CASE WHEN @ExitMessage IS NULL THEN obj.ExitMessage ELSE @ExitMessage END,
			obj.Result = CASE WHEN @Result IS NULL THEN obj.Result ELSE @Result END,
			obj.Message = CASE WHEN @Message IS NULL THEN obj.Message ELSE @Message END,
			obj.DateModified = GETDATE(),
			obj.ModifiedBy = CASE WHEN @ModifiedBy IS NULL THEN obj.ModifiedBy ELSE @ModifiedBy END
		FROM comm.CommunicationExecution obj
		WHERE obj.CommunicationExecutionID = @CommunicationExecutionID


 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	--IF @Trancount = 0
	--BEGIN
	--	COMMIT TRANSACTION;
	--END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		--IF @Trancount = 0 AND @@TRANCOUNT > 0
		--BEGIN
		--	ROLLBACK TRANSACTION;
		--END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH


END


