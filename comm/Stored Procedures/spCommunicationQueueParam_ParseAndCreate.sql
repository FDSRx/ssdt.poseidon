﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2015
-- Description:	Creates a new single or list of CommunicationQueueParam objects from the provided parameter string.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationQueueID BIGINT = 1,
	@CommunicationQueueParamID VARCHAR(MAX) = NULL,
	@TransactionDate DATETIME = GETDATE(),
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC comm.spCommunicationQueueParam_ParseAndCreate
	@CommunicationQueueParamID = @CommunicationQueueParamID OUTPUT,
	@CommunicationQueueID = @CommunicationQueueID,
	@ParamString = 'DRLB||90||int*||*MINFILL||1||int',
	@CreatedBy = 'dhughes'

SELECT @CommunicationQueueParamID AS CommunicationQueueParamID

*/

-- SELECT * FROM comm.CommunicationQueue
-- SELECT * FROM comm.CommunicationQueueParam
-- SELECT * FROM comm.MessageType
-- SELECT * FROM comm.CommunicationMethod
-- SELECT * FROM data.Parameter
-- =============================================
CREATE PROCEDURE comm.[spCommunicationQueueParam_ParseAndCreate]
	@CommunicationQueueID BIGINT = NULL,
	@ParamString VARCHAR(MAX) = NULL,
	@CustomKeyValueDelimiter VARCHAR(10) = NULL,
	@CustomDelimeter VARCHAR(10) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CommunicationQueueParamID VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	
	-------------------------------------------------------------------------------------------
	-- Record request.
	-------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationQueueID=' + dbo.fnToStringOrEmpty(@CommunicationQueueID) + ';' +
		'@ParamString=' + dbo.fnToStringOrEmpty(@ParamString) + ';' +
		'@CustomKeyValueDelimiter=' + dbo.fnToStringOrEmpty(@CustomKeyValueDelimiter) + ';' +
		'@CustomDelimeter=' + dbo.fnToStringOrEmpty(@CustomDelimeter) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@CommunicationQueueParamID=' + dbo.fnToStringOrEmpty(@CommunicationQueueParamID) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------
	SET @CommunicationQueueParamID = NULL;


	-------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	-------------------------------------------------------------------------------------------
	-- @CommunicationQueueID
	IF @CommunicationQueueID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @CommunicationQueueID cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	-------------------------------------------------------------------------------------------
	-- Short circuit logic.
	-------------------------------------------------------------------------------------------
	IF LEN(LTRIM(RTRIM(ISNULL(@ParamString, '')))) = 0
	BEGIN
		RETURN;
	END

	-------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------
	-- Do nothing.

	-------------------------------------------------------------------------------------------
	-- Temporary resources.
	-------------------------------------------------------------------------------------------
	DECLARE @tblOuput AS TABLE (
		CommunicationQueueParamID BIGINT
	);

	-------------------------------------------------------------------------------------------
	-- Create object.
	-------------------------------------------------------------------------------------------
	INSERT INTO comm.CommunicationQueueParam (
		CommunicationQueueID,
		ParameterID,
		ValueString,
		TypeOf,			
		CreatedBy
	)
	OUTPUT inserted.CommunicationQueueParamID INTO @tblOuput(CommunicationQueueParamID)
	SELECT
		@CommunicationQueueID AS CommunicationQueueID,
		COALESCE(parmId.ParameterID, parmCode.ParameterID, parmName.ParameterID) AS ParameterID,
		tmp.Value,
		tmp.TypeOf,		
		@CreatedBy AS CreatedBy
	--SELECT *
	--SELECT TOP 1 *
	FROM etl.fnSplitParameterString(@ParamString) tmp
		LEFT JOIN data.Parameter parmId
			ON tmp.Name = CONVERT(VARCHAR, parmId.ParameterID)
		LEFT JOIN data.parameter parmCode
			ON tmp.Name = parmCode.Code
		LEFT JOIN data.Parameter parmName
			ON tmp.Name = parmName.Name
	WHERE COALESCE(parmId.ParameterID, parmCode.ParameterID, parmName.ParameterID) IS NOT NULL -- Only valid parameters are allowed (no NULLs).
		

	-- Retrieve record identities.
	IF @@ROWCOUNT > 0
	BEGIN
		SET @CommunicationQueueParamID = (
			SELECT
				CONVERT(VARCHAR(25), CommunicationQueueParamID) + ','
			FROM @tblOuput
			FOR XML PATH('')
		);

		SET @CommunicationQueueParamID = LEFT(@CommunicationQueueParamID, LEN(@CommunicationQueueParamID) - 1);
	END





END

