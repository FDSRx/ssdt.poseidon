﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2015
-- Description:	Creates a new CommunicationQueueParam object.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationQueueID BIGINT = 1,
	@ParameterID INT = 1,
	@ValueString VARCHAR(MAX) = 30,
	@TypeOf VARCHAr(50) = 'int',
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC comm.spCommunicationQueueParam_Create
	@CommunicationQueueID = @CommunicationQueueID,
	@ParameterID = @ParameterID,
	@ValueString = @ValueString,
	@TypeOf = @TypeOf,
	@CreatedBy = @CreatedBy,
	@CommunicationQueueParamID = @CommunicationQueueParamID OUTPUT


SELECT @CommunicationQueueParamID AS CommunicationQueueParamID


*/

-- SELECT * FROM comm.CommunicationQueue
-- SELECT * FROM comm.CommunicationQueueParam
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationQueueParam_Create]
	@CommunicationQueueID BIGINT,
	@ParameterID BIGINT,
	@ParameterCode VARCHAR(50) = NULL,
	@ValueString VARCHAR(MAX) = NULL,
	@TypeOf VARCHAR(50) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CommunicationQueueParamID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	-------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationQueueID=' + dbo.fnToStringOrEmpty(@CommunicationQueueID) + ';' +
		'@ParameterID=' + dbo.fnToStringOrEmpty(@ParameterID) + ';' +
		'@ValueString=' + dbo.fnToStringOrEmpty(@ValueString) + ';' +
		'@TypeOf=' + dbo.fnToStringOrEmpty(@TypeOf) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	---------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------------------
	SET @CommunicationQueueParamID = NULL;


	---------------------------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------
	-- @CommunicationQueueID
	IF @CommunicationQueueID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference is not set to an instance of an object. ' +
			'The parameter, @CommunicationQueueID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @ParameterID
	IF @ParameterID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference is not set to an instance of an object. ' +
			'The parameter, @ParameterID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	


	---------------------------------------------------------------------------------------------------------
	-- Set variables.
	---------------------------------------------------------------------------------------------------------
	SET @ParameterID = data.fnGetParameterID(@ParameterID);

	SET @ParameterID = CASE WHEN @ParameterID IS NOT NULL THEN @ParameterID ELSE data.fnGetParameterID(@ParameterCode) END;


	---------------------------------------------------------------------------------------------------------
	-- Create object.
	---------------------------------------------------------------------------------------------------------
	INSERT INTO comm.CommunicationQueueParam (
		CommunicationQueueID,
		ParameterID,
		ValueString,
		TypeOf,
		CreatedBy
	)
	SELECT
		@CommunicationQueueID AS CommunicationQueueID,
		@ParameterID AS ParameterID,
		@ValueString AS ValueString,
		@TypeOf AS TypeOf,
		@CreatedBy AS CreatedBy

	-- Retrieve object identifier.
	SET @CommunicationQueueParamID = SCOPE_IDENTITY();


END
