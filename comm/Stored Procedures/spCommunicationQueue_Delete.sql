﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2015
-- Description:	Deletes a CommunicationQueue object.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationQueueID BIGINT = 1,
	@ModifiedBy VARCHAR(256) = 'dhughes'

EXEC comm.spCommunicationQueue_Delete
	@CommunicationQueueID = @CommunicationQueueID,
	@ModifiedBy = @ModifiedBy


*/

-- SELECT * FROM comm.CommunicationQueue
-- SELECT * FROM comm.CommunicationQueueParam
-- SELECT * FROM comm.MessageType
-- SELECT * FROM comm.CommunicationMethod
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationQueue_Delete]
	@CommunicationQueueID BIGINT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	-------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationQueueID=' + dbo.fnToStringOrEmpty(@CommunicationQueueID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	---------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------------------
	-- No sanitation required.

	/*
	---------------------------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------
	-- @CommunicationQueueID
	IF @CommunicationQueueID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference is not set to an instance of an object. ' +
			'The parameter, @CommunicationQueueID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/

	-------------------------------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy

	--------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------

		---------------------------------------------------------------------------------------------------------
		-- Delete object params.
		-- <Remarks>
		-- Due to referential integrity, the parameters associated with the object need to be removed before
		-- the object.
		-- </Remarks>
		---------------------------------------------------------------------------------------------------------
		EXEC comm.spCommunicationQueueParam_Delete
			@CommunicationQueueID = @CommunicationQueueID,
			@ModifiedBy = @ModifiedBy

		---------------------------------------------------------------------------------------------------------
		-- Delete object.
		---------------------------------------------------------------------------------------------------------
		DELETE obj
		--SELECT *
		FROM comm.CommunicationQueue obj
		WHERE obj.CommunicationQueueID = @CommunicationQueueID



 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH

END
