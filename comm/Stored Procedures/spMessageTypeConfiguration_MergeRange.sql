﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/29/2015
-- Description:	Ceates a single or many new MessageTypeConfiguration object.
-- SAMPLE CALL:
/*

DECLARE 
	@MessageTypeConfigurationIDList VARCHAR(MAX) = NULL
	
EXEC comm.spMessageTypeConfiguration_MergeRange
	@ApplicationID = 10,
	@BusinessID = 10684,
	@MessageTypeID = '1,2,3,4',
	@DeleteUnspecified = 1,
	@MessageTypeConfigurationIDList = @MessageTypeConfigurationIDList OUTPUT

SELECT @MessageTypeConfigurationIDList AS MessageTypeConfigurationIDList


*/

-- SELECT * FROM comm.MessageTypeConfiguration
-- =============================================
CREATE PROCEDURE [comm].[spMessageTypeConfiguration_MergeRange]
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@EntityID BIGINT = NULL,
	@MessageTypeID VARCHAR(MAX),
	@DeleteUnspecified BIT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@MessageTypeConfigurationIDList VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000),
		@ErrorSeverity INT = NULL,
		@ErrorState INT = NULL,
		@ErrorNumber INT = NULL,
		@ErrorLine INT = NULL,
		@ErrorProcedure  NVARCHAR(200) = NULL
		
		
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @MessageTypeConfigurationIDList = NULL;
	SET @DeleteUnspecified = ISNULL(@DeleteUnspecified, 0);
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @MessageTypeID
	IF @MessageTypeID IS NULL OR @MessageTypeID = ''
	BEGIN
		SET @ErrorMessage = 'Unable to create MessageTypeConfiguration object. Object reference (@MessageTypeID) ' +
			'is not set to an instance of an object. ' +
			'The parameter, @MessageTypeID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	
	BEGIN TRY
	--------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------	
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		-------------------------------------------------------------------------------
		-- Temporary objects.
		-------------------------------------------------------------------------------
		DECLARE @tblOutput AS TABLE (
			Idx BIGINT IDENTITY(1,1),
			MessageTypeConfigurationID BIGINT
		);
		
	
		-------------------------------------------------------------------------------
		-- Store ModifiedBy property in "session" like object.
		-------------------------------------------------------------------------------
		EXEC memory.spContextInfo_TriggerData_Set
			@ObjectName = @ProcedureName,
			@DataString = @ModifiedBy


		-------------------------------------------------------------------------------
		-- Remove unspecified items (if applicable).
		-- <Summary>
		-- Removes items that are not specified in the range of items.
		-- </Summary>
		-------------------------------------------------------------------------------
		IF @DeleteUnspecified = 1
		BEGIN
			DELETE mtc
			FROM comm.MessageTypeConfiguration mtc
			WHERE ( @ApplicationID IS NULL OR  mtc.ApplicationID = @ApplicationID )
				AND ( @BusinessID IS NULL OR mtc.BusinessID = @BusinessID )
				AND ( @EntityID IS NULL OR mtc.EntityID = @EntityID )
				AND (( 
					@ApplicationID IS NULL AND mtc.ApplicationID IS NULL
					AND @BusinessID IS NULL AND mtc.BusinessID IS NULL
					AND @EntityID IS NULL AND mtc.EntityID IS NULL					
				)
				OR (
					@ApplicationID IS NOT NULL AND mtc.ApplicationID = @ApplicationID
					AND @BusinessID IS NULL AND mtc.BusinessID IS NULL
					AND @EntityID IS NULL AND mtc.EntityID IS NULL
				) OR (
					@ApplicationID IS NOT NULL AND mtc.ApplicationID = @ApplicationID
					AND @BusinessID IS NOT NULL AND mtc.BusinessID = @BusinessID
					AND @EntityID IS NULL AND mtc.EntityID IS NULL
				) OR (
					@ApplicationID IS NOT NULL AND mtc.ApplicationID = @ApplicationID
					AND @BusinessID IS NOT NULL AND mtc.BusinessID = @BusinessID
					AND @EntityID IS NOT NULL AND mtc.EntityID = @EntityID
				))
				AND mtc.MessageTypeID NOT IN (SELECT Value FROM dbo.fnSplit(@MessageTypeID, ',') WHERE ISNUMERIC(Value) = 1)
					
		END			
						
		-------------------------------------------------------------------------------
		-- Create new object(s).
		-------------------------------------------------------------------------------
		INSERT INTO comm.MessageTypeConfiguration (
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			CreatedBy
		)
		OUTPUT inserted.MessageTypeConfigurationID INTO @tblOutput(MessageTypeConfigurationID)
		SELECT
			@ApplicationID AS ApplicationID,
			@BusinessID AS BusinessID,
			@EntityID AS EntityID,
			typ.Value AS MessagetTypeID,
			@CreatedBy AS CreatedBy
		FROM dbo.fnSplit(@MessageTypeID, ',') typ
			LEFT JOIN comm.MessageTypeConfiguration mtc
				ON ( @ApplicationID IS NULL OR mtc.ApplicationID = @ApplicationID )
					AND ( @BusinessID IS NULL OR mtc.BusinessID = @BusinessID )
					AND ( @EntityID IS NULL OR mtc.EntityID = @EntityID )
					AND mtc.MessageTypeID = CASE WHEN ISNUMERIC(typ.Value) = 1 THEN typ.Value ELSE NULL END
		WHERE ISNUMERIC(typ.Value) = 1
			AND mtc.MessageTypeConfigurationID IS NULL
		
		-------------------------------------------------------------------------------
		-- Retrieve record identity value(s).
		-------------------------------------------------------------------------------
		SET @MessageTypeConfigurationIDList = (
			SELECT 
				CONVERT(VARCHAR(25), MessageTypeConfigurationID) + ','
			FROM @tblOutput
			FOR XML PATH('')
		);
		
		IF RIGHT(@MessageTypeConfigurationIDList, 1) = ','
		BEGIN
			SET @MessageTypeConfigurationIDList = LEFT(@MessageTypeConfigurationIDList, LEN(@MessageTypeConfigurationIDList) - 1);
		END

	--------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END		
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>'),
			@ErrorMessage = ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);	
	
	END CATCH
		
		
END
