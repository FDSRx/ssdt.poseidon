﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/24/2015
-- Description:	Returns the messaging (communication) configuration option for the provided criteria.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationConfigurationID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@EntityID BIGINT = NULL,
	@MessageTypeID INT = NULL,
	@CommunicationMethodID INT = NULL,
	@MessagesPerMinutes INT = NULL,
	@MessagesNumberOfMinutes INT = NULL,
	@MessagesPerDays INT = NULL,
	@MessagesNumberOfDays INT = NULL,
	@IsDisabled BIT = NULL
	
EXEC comm.spCommunicationConfiguration_Get
	@CommunicationConfigurationID = @CommunicationConfigurationID,
	@ApplicationID = @ApplicationID,
	@BusinessID = @BusinessID,
	@EntityID = @EntityID,
	@MessageTypeID = @MessageTypeID,
	@CommunicationMethodID = @CommunicationMethodID,
	@MessagesPerMinutes = @MessagesPerMinutes OUTPUT,
	@MessagesNumberOfMinutes = @MessagesNumberOfMinutes OUTPUT,
	@MessagesPerDays = @MessagesPerDays OUTPUT,
	@MessagesNumberOfDays = @MessagesNumberOfDays OUTPUT,
	@IsDisabled = @IsDisabled OUTPUT

SELECT 
	@CommunicationConfigurationID AS CommunicationConfigurationID,
	@ApplicationID AS ApplicationID,
	@BusinessID AS BusinessID,
	@EntityID AS EntityID,
	@MessageTypeID AS MessageTypeID,
	@CommunicationMethodID AS CommunicationMethodID,
	@MessagesPerMinutes AS MessagesPerMinutes,
	@MessagesNumberOfMinutes AS MessagesNumberOfMinutes,
	@MessagesPerDays AS MessagesPerDays,
	@MessagesNumberOfDays AS MessagesNumberOfDays,
	@IsDisabled AS IsDisabled
	
*/

-- SELECT * FROM comm.CommunicationConfiguration
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationConfiguration_Get]
	@CommunicationConfigurationID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@EntityID BIGINT = NULL,
	@MessageTypeID INT = NULL,
	@CommunicationMethodID INT = NULL,
	@MessagesPerMinutes INT = NULL OUTPUT,
	@MessagesNumberOfMinutes INT = NULL OUTPUT,
	@MessagesPerDays INT = NULL OUTPUT,
	@MessagesNumberOfDays INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------
	-- Instance variables
	-------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationConfigurationID=' + dbo.fnToStringOrEmpty(@CommunicationConfigurationID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@EntityID=' + dbo.fnToStringOrEmpty(@EntityID) + ';' +
		'@MessageTypeID=' + dbo.fnToStringOrEmpty(@MessageTypeID) + ';' +
		'@CommunicationMethodID=' + dbo.fnToStringOrEmpty(@CommunicationMethodID) + ';' 
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
			
	-------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------
	SET @MessagesPerMinutes = NULL;
	SET @MessagesNumberOfMinutes = NULL;
	SET @MessagesPerDays = NULL;
	SET @MessagesNumberOfDays = NULL;
	
	-------------------------------------------------------------------------------------------
	-- Tempoarary resources.
	-------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpCommConfig') IS NOT NULL
	BEGIN
		DROP TABLE #tmpCommConfig;
	END
	
	CREATE TABLE #tmpCommConfig (
		CommunicationConfigurationID BIGINT,
		ApplicationID INT,
		BusinessID BIGINT,
		EntityID BIGINT,
		MessageTypeID INT,
		CommunicationMethodID INT,
		MessagesPerMinutes INT,
		MessagesNumberOfMinutes INT,
		MessagesPerDays INT,
		MessagesNumberOfDays INT,
		IsDisabled BIT
	);
		

	-------------------------------------------------------------------------------------------
	-- Retrieve configuration object.
	-- <Summary>
	-- Use a hierarchical lookup approach to retrieve the most appropriate
	-- configuration for the provided criteria.
	-- </Summary>
	-------------------------------------------------------------------------------------------
	-- Perform a lookup on the provided communication configuration identifier.
	INSERT INTO #tmpCommConfig (
		CommunicationConfigurationID,
		ApplicationID,
		BusinessID,
		EntityID,
		MessageTypeID,
		CommunicationMethodID,
		MessagesPerMinutes,
		MessagesNumberOfMinutes,
		MessagesPerDays,
		MessagesNumberOfDays,
		IsDisabled
	)
	SELECT
		CommunicationConfigurationID,
		ApplicationID,
		BusinessID,
		EntityID,
		MessageTypeID,
		CommunicationMethodID,
		MessagesPerMinutes,
		MessagesNumberOfMinutes,
		MessagesPerDays,
		MessagesNumberOfDays,
		IsDisabled
	FROM comm.CommunicationConfiguration
	WHERE CommunicationConfigurationID = @CommunicationConfigurationID
	
	-- If an identifier could not be located, then perform a lookup on an exact unique key match;
	-- ApplicationID, BusinessID, EntityID, MessageTypeID, CommunicationMethodID
	IF (SELECT COUNT(*) FROM #tmpCommConfig) = 0
	BEGIN
	
			INSERT INTO #tmpCommConfig (
			CommunicationConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			CommunicationMethodID,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled
		)
		SELECT
			CommunicationConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			CommunicationMethodID,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled
		FROM comm.CommunicationConfiguration
		WHERE ApplicationID = @ApplicationID
			AND BusinessID = @BusinessID
			AND EntityID = @EntityID
			AND MessageTypeID = @MessageTypeID
			AND CommunicationMethodID = @CommunicationMethodID	
	
	END
	
	-- If an identifier could not be located, then perform a lookup on the following criteria:
	-- ApplicationID, BusinessID, MessageTypeID, CommunicationMethodID
	IF (SELECT COUNT(*) FROM #tmpCommConfig) = 0
	BEGIN
	
		INSERT INTO #tmpCommConfig (
			CommunicationConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			CommunicationMethodID,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled
		)
		SELECT
			CommunicationConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			CommunicationMethodID,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled
		FROM comm.CommunicationConfiguration
		WHERE ApplicationID = @ApplicationID
			AND BusinessID = @BusinessID
			AND MessageTypeID = @MessageTypeID
			AND CommunicationMethodID = @CommunicationMethodID	
	
	END
	
	-- If an identifier could not be located, then perform a lookup on a partial match;
	-- BusinessID, MessageTypeID, CommunicationMethodID
	IF (SELECT COUNT(*) FROM #tmpCommConfig) = 0
	BEGIN
	
		INSERT INTO #tmpCommConfig (
			CommunicationConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			CommunicationMethodID,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled
		)
		SELECT
			CommunicationConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			CommunicationMethodID,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled
		FROM comm.CommunicationConfiguration
		WHERE ApplicationID IS NULL
			AND BusinessID = @BusinessID
			AND MessageTypeID = @MessageTypeID
			AND CommunicationMethodID = @CommunicationMethodID	
	
	END			

	-- If an identifier could not be located, then perform a lookup on a partial match;
	-- Application, MessageTypeID, CommunicationMethodID
	IF (SELECT COUNT(*) FROM #tmpCommConfig) = 0
	BEGIN
	
		INSERT INTO #tmpCommConfig (
			CommunicationConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			CommunicationMethodID,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled
		)
		SELECT
			CommunicationConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			CommunicationMethodID,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled
		FROM comm.CommunicationConfiguration
		WHERE ApplicationID = @ApplicationID
			AND BusinessID IS NULL
			AND MessageTypeID = @MessageTypeID
			AND CommunicationMethodID = @CommunicationMethodID	
	
	END		

	-- If an identifier could not be located, then perform a lookup on a partial match;
	-- MessageTypeID, CommunicationMethodID
	IF (SELECT COUNT(*) FROM #tmpCommConfig) = 0
	BEGIN
	
		INSERT INTO #tmpCommConfig (
			CommunicationConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			CommunicationMethodID,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled
		)
		SELECT
			CommunicationConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			CommunicationMethodID,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled
		FROM comm.CommunicationConfiguration
		WHERE ApplicationID IS NULL
			AND BusinessID IS NULL
			AND MessageTypeID = @MessageTypeID
			AND CommunicationMethodID = @CommunicationMethodID	
				
	END

	-------------------------------------------------------------------------------------------
	-- Assign the output variables.
	-------------------------------------------------------------------------------------------
	SELECT TOP 1
		@CommunicationConfigurationID = CommunicationConfigurationID,
		@ApplicationID = ApplicationID,
		@BusinessID = BusinessID,
		@EntityID = EntityID,
		@MessageTypeID = MessageTypeID,
		@CommunicationMethodID = CommunicationMethodID,
		@MessagesPerMinutes = MessagesPerMinutes,
		@MessagesNumberOfMinutes = MessagesNumberOfMinutes,
		@MessagesPerDays = MessagesPerDays,
		@MessagesNumberOfDays = MessagesNumberOfDays,
		@IsDisabled = IsDisabled
	FROM #tmpCommConfig
	
	
	-------------------------------------------------------------------------------------------
	-- Dispose of temporary resources.
	-------------------------------------------------------------------------------------------
	DROP TABLE #tmpCommConfig;













		
END
