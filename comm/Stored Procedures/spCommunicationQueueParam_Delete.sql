﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2015
-- Description:	Deletes a CommunicationQueueParam object.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationQueueParamID BIGINT = NULL,
	@CommunicationQueueID BIGINT = 1,
	@ModifiedBy VARCHAR(256) = 'dhughes'

EXEC comm.spCommunicationQueueParam_Delete
	@CommunicationQueueID = @CommunicationQueueID,
	@ModifiedBy = @ModifiedBy



*/

-- SELECT * FROM comm.CommunicationQueue
-- SELECT * FROM comm.CommunicationQueueParam
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationQueueParam_Delete]
	@CommunicationQueueParamID BIGINT = NULL,
	@CommunicationQueueID BIGINT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	-------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationQueueParamID=' + dbo.fnToStringOrEmpty(@CommunicationQueueParamID) + ';' +
		'@CommunicationQueueID=' + dbo.fnToStringOrEmpty(@CommunicationQueueID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy

	-------------------------------------------------------------------------------------------------------
	-- Remove object(s).
	-------------------------------------------------------------------------------------------------------
	DELETE obj
	FROM comm.CommunicationQueueParam obj
	WHERE ( @CommunicationQueueParamID IS NOT NULL AND obj.CommunicationQueueParamID = @CommunicationQueueParamID ) -- Remove primary if specified.
		-- Remove group, if no primary specified.
		OR ( @CommunicationQueueParamID IS NULL AND obj.CommunicationQueueID = @CommunicationQueueID ) 

END
