﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2015
-- Description:	Creates a new CommunicationExecutionParam object.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationExecutionID BIGINT = 1,
	@ParameterID INT = 1,
	@ValueString VARCHAR(MAX) = 30,
	@TypeOf VARCHAr(50) = 'int',
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC comm.spCommunicationExecutionParam_Create
	@CommunicationExecutionID = @CommunicationExecutionID,
	@ParameterID = @ParameterID,
	@ValueString = @ValueString,
	@TypeOf = @TypeOf,
	@CreatedBy = @CreatedBy


*/

-- SELECT * FROM comm.CommunicationExecution
-- SELECT * FROM comm.CommunicationExecutionParam
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationExecutionParam_Create]
	@CommunicationExecutionID BIGINT,
	@ParameterID BIGINT,
	@ParameterCode VARCHAR(50) = NULL,
	@ValueString VARCHAR(MAX) = NULL,
	@TypeOf VARCHAR(50) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CommunicationExecutionParamID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	-------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationExecutionID=' + dbo.fnToStringOrEmpty(@CommunicationExecutionID) + ';' +
		'@ParameterID=' + dbo.fnToStringOrEmpty(@ParameterID) + ';' +
		'@ValueString=' + dbo.fnToStringOrEmpty(@ValueString) + ';' +
		'@TypeOf=' + dbo.fnToStringOrEmpty(@TypeOf) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@CommunicationExecutionParamID=' + dbo.fnToStringOrEmpty(@CommunicationExecutionParamID) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	---------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------------------
	SET @CommunicationExecutionParamID = NULL;


	---------------------------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------
	-- @CommunicationExecutionID
	IF @CommunicationExecutionID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference is not set to an instance of an object. ' +
			'The parameter, @CommunicationExecutionID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @ParameterID
	IF @ParameterID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference is not set to an instance of an object. ' +
			'The parameter, @ParameterID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	


	---------------------------------------------------------------------------------------------------------
	-- Set variables.
	---------------------------------------------------------------------------------------------------------
	SET @ParameterID = data.fnGetParameterID(@ParameterID);

	SET @ParameterID = CASE WHEN @ParameterID IS NOT NULL THEN @ParameterID ELSE data.fnGetParameterID(@ParameterCode) END;


	---------------------------------------------------------------------------------------------------------
	-- Create object.
	---------------------------------------------------------------------------------------------------------
	INSERT INTO comm.CommunicationExecutionParam (
		CommunicationExecutionID,
		ParameterID,
		ValueString,
		TypeOf,
		CreatedBy
	)
	SELECT
		@CommunicationExecutionID AS CommunicationExecutionID,
		@ParameterID AS ParameterID,
		@ValueString AS ValueString,
		@TypeOf AS TypeOf,
		@CreatedBy AS CreatedBy

	-- Retrieve object identifier.
	SET @CommunicationExecutionParamID = SCOPE_IDENTITY();


END
