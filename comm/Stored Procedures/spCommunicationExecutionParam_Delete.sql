﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2015
-- Description:	Deletes a CommunicationExecutionParam object.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationExeuctionParamID BIGINT = NULL,
	@CommunicationExecutionID BIGINT = 1,
	@ModifiedBy VARCHAR(256) = 'dhughes'

EXEC comm.spCommunicationExecutionParam_Delete
	@CommunicationExecutionID = @CommunicationExecutionID,
	@ModifiedBy = @ModifiedBy



*/

-- SELECT * FROM comm.CommunicationExecution
-- SELECT * FROM comm.CommunicationExecutionParam
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationExecutionParam_Delete]
	@CommunicationExecutionParamID BIGINT = NULL,
	@CommunicationExecutionID BIGINT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	-------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationExecutionParamID=' + dbo.fnToStringOrEmpty(@CommunicationExecutionParamID) + ';' +
		'@CommunicationExecutionID=' + dbo.fnToStringOrEmpty(@CommunicationExecutionID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy

	-------------------------------------------------------------------------------------------------------
	-- Remove object(s).
	-------------------------------------------------------------------------------------------------------
	DELETE obj
	FROM comm.CommunicationExecutionParam obj
	WHERE ( @CommunicationExecutionParamID IS NOT NULL AND obj.CommunicationExecutionParamID = @CommunicationExecutionParamID ) -- Remove primary if specified.
		-- Remove group, if no primary specified.
		OR ( @CommunicationExecutionParamID IS NULL AND obj.CommunicationExecutionID = @CommunicationExecutionID ) 

END
