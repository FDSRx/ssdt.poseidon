﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/29/2014
-- Description:	Creates a new MessageTypeConfiguration object.
-- SAMPLE CALL:
/*

DECLARE 
	@MessageTypeConfigurationID BIGINT = NULL
	
EXEC comm.spMessageTypeConfiguration_Create
	@ApplicationID = 8,
	@BusinessID = 10684,
	@MessageTypeID = 10,
	@MessageTypeConfigurationID = @MessageTypeConfigurationID OUTPUT

SELECT @MessageTypeConfigurationID AS MessageTypeConfigurationID


*/

-- SELECT * FROM comm.MessageTypeConfiguration
-- =============================================
CREATE PROCEDURE comm.[spMessageTypeConfiguration_Create]
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@EntityID BIGINT = NULL,
	@MessageTypeID INT,
	@CreatedBy VARCHAR(256) = NULL,
	@MessageTypeConfigurationID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @MessageTypeConfigurationID = NULL;
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @MessageTypeID
	IF @MessageTypeID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create MessageTypeConfiguration object. Object reference (@MessageTypeID) ' +
			'is not set to an instance of an object. ' +
			'The parameter, @MessageTypeID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
		
	-------------------------------------------------------------------------------
	-- Create new MessageTypeConfiguration object.
	-------------------------------------------------------------------------------
	INSERT INTO comm.MessageTypeConfiguration (
		ApplicationID,
		BusinessID,
		EntityID,
		MessageTypeID,
		CreatedBy
	)
	SELECT
		@ApplicationID AS ApplicationID,
		@BusinessID AS BusinessID,
		@EntityID AS EntityID,
		@MessageTypeID AS MessageTypeID,
		@CreatedBy AS CreatedBy
	
	-- Retrieve record identity value.
	SET @MessageTypeConfigurationID = SCOPE_IDENTITY();
		
END
