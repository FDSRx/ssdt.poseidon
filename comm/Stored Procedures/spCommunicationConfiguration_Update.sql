﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/29/2015
-- Description:	Updates a CommunicationConfiguration object.
-- SAMPLE CALL:
/*

DECLARE 
	@CommunicationConfigurationID BIGINT = NULL
	
EXEC comm.spCommunicationConfiguration_Update
	@ApplicationID = 10,
	@BusinessID = 38,
	@MessageTypeID = 1,
	@CommunicationMethodID = 2,
	@CommunicationConfigurationID = @CommunicationConfigurationID OUTPUT

SELECT @CommunicationConfigurationID AS CommunicationConfigurationID


*/

-- SELECT * FROM comm.CommunicationConfiguration
-- SELECT * FROM comm.MessageType
-- SELECT * FROM comm.CommunicationMethod
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationConfiguration_Update]
	@CommunicationConfigurationID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@EntityID BIGINT = NULL,
	@MessageTypeID INT,
	@CommunicationMethodID INT,
	@MessagesPerMinutes INT = NULL,
	@MessagesNumberOfMinutes INT = NULL,
	@MessagesPerDays INT = NULL,
	@MessagesNumberOfDays INT = NULL,
	@IsDisabled BIT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000),
		@ErrorSeverity INT = NULL,
		@ErrorState INT = NULL,
		@ErrorNumber INT = NULL,
		@ErrorLine INT = NULL,
		@ErrorProcedure  NVARCHAR(200) = NULL
		
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CommunicationConfigurationID = NULL;
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------


	/*
	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @MessageTypeID
	IF @MessageTypeID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CommunicationConfiguration object. Object reference (@CommunicationConfiguration) ' +
			'is not set to an instance of an object. ' +
			'The parameter, @CommunicationConfiguration, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @CommunicationMethodID
	IF @CommunicationMethodID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CommunicationConfiguration object. Object reference (@CommunicationMethodID) ' +
			'is not set to an instance of an object. ' +
			'The parameter, @CommunicationMethodID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/
	
	-------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------	
	IF @CommunicationConfigurationID IS NOT NULL
	BEGIN
		SET @ApplicationID = NULL;
		SET @BusinessID = NULL;
		SET @EntityID = NULL;
		SET @MessageTypeID = NULL;
		SET @CommunicationMethodID = NULL;
	END	
	
		
	-------------------------------------------------------------------------------
	-- Update CommunicationConfiguration object.
	-------------------------------------------------------------------------------
	UPDATE cc
	SET
		cc.MessagesPerMinutes = CASE WHEN @MessagesPerMinutes IS NULL THEN cc.MessagesPerMinutes ELSE @MessagesNumberOfMinutes END,
		cc.MessagesNumberOfMinutes = CASE WHEN @MessagesNumberOfMinutes IS NULL THEN cc.MessagesNumberOfMinutes ELSE @MessagesNumberOfMinutes END,
		cc.MessagesPerDays = CASE WHEN @MessagesPerDays IS NULL THEN cc.MessagesPerDays ELSE @MessagesPerDays END,
		cc.MessagesNumberOfDays = CASE WHEN @MessagesNumberOfDays IS NULL THEN cc.MessagesNumberOfDays ELSE @MessagesNumberOfDays END,
		cc.IsDisabled = CASE WHEN @IsDisabled IS NULL THEN cc.IsDisabled ELSE @IsDisabled END,
		cc.DateModified = GETDATE(),
		cc.ModifiedBy = CASE WHEN @ModifiedBy IS NULL THEN cc.ModifiedBy ELSE @ModifiedBy END
	FROM comm.CommunicationConfiguration cc
	WHERE cc.CommunicationConfigurationID = @CommunicationConfigurationID
	
		
END
