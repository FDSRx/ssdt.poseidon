﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2015
-- Description:	Creates a new CommunicationExecution object.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationExecutionID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = 'ENG',
	@BusinessID BIGINT = 49,
	@MessageTypeID INT = NULL,
	@MessageTypeCode VARCHAR(50) = 'QC1',
	@CommunicationMethodID INT = NULL,
	@CommunicationMethodCode VARCHAR(50) = 'VOICE',
	@TargetAudienceSourceID INT = 1,
	@TargetAudienceSourceCode VARCHAR(50) = NULL,
	@TransactionDate DATETIME = GETDATE(),
	@ExecutionStatusID INT = etl.fnGetExecutionStatusID('CR'),
	@CreatedBy VARCHAR(256) = 'dhughes',
	@TransactionGuid UNIQUEIDENTIFIER = NEWID()

EXEC comm.spCommunicationExecution_Create
	@CommunicationExecutionID = @CommunicationExecutionID OUTPUT,
	@ApplicationID = @ApplicationID,
	@ApplicationCode = @ApplicationCode,
	@BusinessID = @BusinessID,
	@MessageTypeID = @MessageTypeID,
	@MessageTypeCode = @MessageTypeCode,
	@CommunicationMethodID = @CommunicationMethodID,
	@CommunicationMethodCode = @CommunicationMethodCode,
	@TargetAudienceSourceID = @TargetAudienceSourceID,
	@TargetAudienceSourceCode = @TargetAudienceSourceCode,
	@DateStart = @TransactionDate,
	@DateEnd = NULL,
	@ExecutionStatusID = @ExecutionStatusID,
	@ExitCode = NULL,
	@ExitMessage = NULL,
	@Result = NULL,
	@Message = NULL,
	@Arguments = NULL,
	@Parameters = 'DRLB||90||int*||*MINFILL||1||int*||*MINAGE||40||int',
	@CreatedBy = @CreatedBy

SELECT @CommunicationExecutionID AS CommunicationExecutionID

*/

-- SELECT * FROM comm.CommunicationExecution
-- SELECT * FROM comm.CommunicationExecutionParam 
-- SELECT * FROM dbo.Application
-- SELECT * FROM dbo.Business
-- SELECT * FROM comm.MessageType
-- SELECT * FROM comm.CommunicationMethod
-- SELECT * FROM etl.ExecutionStatus
-- SELECT * FROM data.Parameter
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationExecution_Create]
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL, -- Optional: Can be provided if the object identifier is not known.
	@BusinessID BIGINT = NULL,
	@MessageTypeID INT = NULL,
	@MessageTypeCode VARCHAR(50) = NULL, -- Optional: Can be provided if the object identifier is not known.
	@CommunicationMethodID INT = NULL,
	@CommunicationMethodCode VARCHAR(50) = NULL, -- Optional: Can be provided if the object identifier is not known.
	@CommunicationName VARCHAR(256) = NULL,
	@TargetAudienceSourceID INT = NULL,
	@TargetAudienceSourceCode VARCHAR(50) = NULL, -- Optional: Used to identify the object if the identifier is not known.
	@DateScheduled DATETIME = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@ExecutionStatusID INT = NULL,
	@ExecutionStatusCode VARCHAR(25) = NULL, -- Optional: Used to identify the object if the identifier is not known.
	@ReadCount BIGINT = NULL,
	@WriteCount BIGINT = NULL,
	@SkipCount BIGINT = NULL,
	@ExitCode VARCHAR(256) = NULL,
	@ExitMessage VARCHAR(MAX) = NULL,
	@Result VARCHAR(MAX) = NULL,
	@Message VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@Parameters VARCHAR(MAX) = NULL,
	@TransactionGuid UNIQUEIDENTIFIER = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	-- Output
	@CommunicationExecutionID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		-- Objects
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@MessageTypeID=' + dbo.fnToStringOrEmpty(@MessageTypeID) + ';' +
		'@MessageTypeCode=' + dbo.fnToStringOrEmpty(@MessageTypeCode) + ';' +
		'@CommunicationMethodID=' + dbo.fnToStringOrEmpty(@CommunicationMethodID) + ';' +
		'@CommunicationMethodCode=' + dbo.fnToStringOrEmpty(@CommunicationMethodCode) + ';' +
		'@TargetAudienceSourceID=' + dbo.fnToStringOrEmpty(@TargetAudienceSourceID) + ';' +
		'@TargetAudienceSourceCode=' + dbo.fnToStringOrEmpty(@TargetAudienceSourceCode) + ';' +
		'@CommunicationName=' + dbo.fnToStringOrEmpty(@CommunicationName) + ';' +
		'@DateScheduled=' + dbo.fnToStringOrEmpty(@DateScheduled) + ';' +
		-- Execution properties.
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart) + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd) + ';' +
		'@ExecutionStatusID=' + dbo.fnToStringOrEmpty(@ExecutionStatusID) + ';' +
		'@ExecutionStatusCode=' + dbo.fnToStringOrEmpty(@ExecutionStatusCode) + ';' +
		'@ReadCount=' + dbo.fnToStringOrEmpty(@ReadCount) + ';' +
		'@WriteCount=' + dbo.fnToStringOrEmpty(@WriteCount) + ';' +
		'@SkipCount=' + dbo.fnToStringOrEmpty(@SkipCount) + ';' +
		'@ExitCode=' + dbo.fnToStringOrEmpty(@ExitCode) + ';' +
		'@ExitMessage=' + dbo.fnToStringOrEmpty(@ExitMessage) + ';' +
		'@Result=' + dbo.fnToStringOrEmpty(@Result) + ';' +
		'@Message=' + dbo.fnToStringOrEmpty(@Message) + ';' +
		'@Arguments=' + dbo.fnToStringOrEmpty(@Arguments) + ';' +
		'@Parameters=' + dbo.fnToStringOrEmpty(@Parameters) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @TransactionGuid = ISNULL(@TransactionGuid, NEWID());
	SET @CommunicationExecutionID = NULL;
	SET @Debug = ISNULL(@Debug, 0);
	SET @Arguments = ISNULL(@Arguments, @Parameters);
	SET @ReadCount = ISNULL(@ReadCount, 0);
	SET @WriteCount = ISNULL(@WriteCount, 0);
	SET @SkipCount = ISNULL(@SkipCount, 0);

	--------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------
	-- No declarations.

	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	-- Do nothing.

		-- Debug
	--SELECT 'Debugger On' AS DebugMode, 'Before Variable Setters' AS MethodAction,
	--	@ApplicationID AS ApplicationID, @ApplicationCode AS ApplicationCode, @BusinessID AS BusinessID,
	--	@MessageTypeID AS MessageTypeID, @MessageTypeCode AS MessageTypeCode,
	--	@CommunicationMethodID AS CommunicationMethodID,
	--	@CommunicationMethodCode AS CommunicationMethodCode, @DateScheduled AS DateScheduled,
	--  @ExecutionStatusID AS ExecutionStatusID, @ExecutionStatusCode AS ExecutionStatusCode

	---------------------------------------------------------------------------------------------------------
	-- Set variables.
	---------------------------------------------------------------------------------------------------------
	-- ApplicationID
	SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationID);
	SET @ApplicationID = CASE WHEN @ApplicationID IS NOT NULL THEN @ApplicationID ELSE dbo.fnGetApplicationID(@ApplicationCode) END;
	-- MessageTypeID
	SET @MessageTypeID = comm.fnGetMessageTypeID(@MessageTypeID);
	SET @MessageTypeID = CASE WHEN @MessageTypeID IS NOT NULL THEN @MessageTypeID ELSE comm.fnGetMessageTypeID(@MessageTypeCode) END;
	-- CommunicationMethodID
	SET @CommunicationMethodID = comm.fnGetCommunicationMethodID(@CommunicationMethodID);
	SET @CommunicationMethodID = CASE WHEN @CommunicationMethodID IS NOT NULL THEN @CommunicationMethodID ELSE comm.fnGetCommunicationMethodID(@CommunicationMethodCode) END;
	-- TargetAudienceSourceID
	SET @TargetAudienceSourceID = comm.fnGetCommunicationMethodID(@TargetAudienceSourceID);
	SET @TargetAudienceSourceID = CASE WHEN @TargetAudienceSourceID IS NOT NULL THEN @TargetAudienceSourceID ELSE comm.fnGetTargetAudienceSourceID(@TargetAudienceSourceCode) END;	
	-- ExecutionStatusID
	SET @ExecutionStatusID = etl.fnGetExecutionStatusID(@ExecutionStatusID);
	SET @ExecutionStatusID = CASE WHEN @ExecutionStatusID IS NOT NULL THEN @ExecutionStatusID ELSE etl.fnGetExecutionStatusID(@ExecutionStatusCode) END;
	-- Default ExecutionStatusID
	SET @ExecutionStatusID = CASE WHEN @ExecutionStatusID IS NOT NULL THEN @ExecutionStatusID ELSE etl.fnGetExecutionStatusID('CR') END;

	-- Debug
	--SELECT 'Debugger On' AS DebugMode, 'After Variable Setters' AS MethodAction,
	--	@ApplicationID AS ApplicationID, @ApplicationCode AS ApplicationCode, @BusinessID AS BusinessID,
	--	@MessageTypeID AS MessageTypeID, @MessageTypeCode AS MessageTypeCode, 
	--	@CommunicationMethodID AS CommunicationMethodID, @TransactionGuid AS TransactionGuid,
	--	@CommunicationMethodCode AS CommunicationMethodCode, @DateScheduled AS DateScheduled,
	--	@ExecutionStatusID AS ExecutionStatusID, @ExecutionStatusCode AS ExecutionStatusCode

	--------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------

		--------------------------------------------------------------------------------------------------------
		-- Create object.
		--------------------------------------------------------------------------------------------------------
		INSERT INTO comm.CommunicationExecution(
			ApplicationID,
			BusinessID,
			MessageTypeID,
			CommunicationMethodID,
			CommunicationName,
			TargetAudienceSourceID,
			DateScheduled,
			DateStart,
			DateEnd,
			ExecutionStatusID,
			ReadCount,
			WriteCount,
			SkipCount,
			ExitCode,
			ExitMessage,
			Result,
			Message,
			Arguments,
			TransactionGuid,
			CreatedBy
		)
		SELECT
			@ApplicationID AS ApplicationID,
			@BusinessID AS BusinessID,
			@MessageTypeID AS MessageTypeID,
			@CommunicationMethodID AS CommunicationMethodID,
			@CommunicationName AS CommunicationName,
			@TargetAudienceSourceID AS TargetAudienceSourceID,
			@DateScheduled AS DateScheduled,
			@DateStart AS DateStart,
			@DateEnd AS DateEnd,
			@ExecutionStatusID AS ExecutionStatusID,
			@ReadCount AS ReadCount,
			@WriteCount AS WriteCount,
			@SkipCount AS SkipCount,
			@ExitCode AS ExitCode,
			@ExitMessage AS ExitMessage,
			@Result AS Result,
			@Message AS Message,
			@Arguments AS Arguments,
			@TransactionGuid AS TransactionGuid,
			@CreatedBy AS CreatedBy

		-- Retrieve object identifier.
		SET @CommunicationExecutionID = SCOPE_IDENTITY();

		--------------------------------------------------------------------------------------------------------
		-- Create object parameters.
		--------------------------------------------------------------------------------------------------------
		EXEC comm.spCommunicationExecutionParam_ParseAndCreate
			@CommunicationExecutionID = @CommunicationExecutionID,
			@ParamString = @Parameters,
			@CreatedBy = @CreatedBy

 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH


END

