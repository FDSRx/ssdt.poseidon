﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2015
-- Description:	Creates a new single or list of CommunicationExecutionParam objects from the provided parameter string.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationExecutionID BIGINT = 1,
	@CommunicationExecutionParamID VARCHAR(MAX) = NULL,
	@TransactionDate DATETIME = GETDATE(),
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC comm.spCommunicationExecutionParam_ParseAndCreate
	@CommunicationExecutionParamID = @CommunicationExecutionParamID OUTPUT,
	@CommunicationExecutionID = @CommunicationExecutionID,
	@ParamString = 'DRLB||90||int*||*MINFILL||1||int',
	@CreatedBy = 'dhughes'

SELECT @CommunicationExecutionParamID AS CommunicationExecutionParamID

*/

-- SELECT * FROM comm.CommunicationExecution
-- SELECT * FROM comm.CommunicationExecutionParam
-- SELECT * FROM comm.MessageType
-- SELECT * FROM comm.CommunicationMethod
-- SELECT * FROM data.Parameter
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationExecutionParam_ParseAndCreate]
	@CommunicationExecutionID BIGINT = NULL,
	@ParamString VARCHAR(MAX) = NULL,
	@CustomKeyValueDelimiter VARCHAR(10) = NULL,
	@CustomDelimeter VARCHAR(10) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CommunicationExecutionParamID VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	
	-------------------------------------------------------------------------------------------
	-- Record request.
	-------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationExecutionID=' + dbo.fnToStringOrEmpty(@CommunicationExecutionID) + ';' +
		'@ParamString=' + dbo.fnToStringOrEmpty(@ParamString) + ';' +
		'@CustomKeyValueDelimiter=' + dbo.fnToStringOrEmpty(@CustomKeyValueDelimiter) + ';' +
		'@CustomDelimeter=' + dbo.fnToStringOrEmpty(@CustomDelimeter) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@CommunicationExecutionParamID=' + dbo.fnToStringOrEmpty(@CommunicationExecutionParamID) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------
	SET @CommunicationExecutionParamID = NULL;


	-------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	-------------------------------------------------------------------------------------------
	-- @CommunicationExecutionID
	IF @CommunicationExecutionID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @CommunicationExecutionID cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	-------------------------------------------------------------------------------------------
	-- Short circuit logic.
	-------------------------------------------------------------------------------------------
	IF LEN(LTRIM(RTRIM(ISNULL(@ParamString, '')))) = 0
	BEGIN
		RETURN;
	END

	-------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------
	-- Do nothing.

	-------------------------------------------------------------------------------------------
	-- Temporary resources.
	-------------------------------------------------------------------------------------------
	DECLARE @tblOuput AS TABLE (
		CommunicationExecutionParamID BIGINT
	);

	-------------------------------------------------------------------------------------------
	-- Create object.
	-------------------------------------------------------------------------------------------
	INSERT INTO comm.CommunicationExecutionParam (
		CommunicationExecutionID,
		ParameterID,
		ValueString,
		TypeOf,			
		CreatedBy
	)
	OUTPUT inserted.CommunicationExecutionParamID INTO @tblOuput(CommunicationExecutionParamID)
	SELECT
		@CommunicationExecutionID AS CommunicationExecutionID,
		COALESCE(parmId.ParameterID, parmCode.ParameterID, parmName.ParameterID) AS ParameterID,
		tmp.Value,
		tmp.TypeOf,		
		@CreatedBy AS CreatedBy
	--SELECT *
	--SELECT TOP 1 *
	FROM etl.fnSplitParameterString(@ParamString) tmp
		LEFT JOIN data.Parameter parmId
			ON tmp.Name = CONVERT(VARCHAR, parmId.ParameterID)
		LEFT JOIN data.parameter parmCode
			ON tmp.Name = parmCode.Code
		LEFT JOIN data.Parameter parmName
			ON tmp.Name = parmName.Name
	WHERE COALESCE(parmId.ParameterID, parmCode.ParameterID, parmName.ParameterID) IS NOT NULL -- Only valid parameters are allowed (no NULLs).
		

	-- Retrieve record identities.
	IF @@ROWCOUNT > 0
	BEGIN
		SET @CommunicationExecutionParamID = (
			SELECT
				CONVERT(VARCHAR(25), CommunicationExecutionParamID) + ','
			FROM @tblOuput
			FOR XML PATH('')
		);

		SET @CommunicationExecutionParamID = LEFT(@CommunicationExecutionParamID, LEN(@CommunicationExecutionParamID) - 1);
	END





END

