﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/29/2015
-- Description:	Deletes a CommunicationConfiguration object.
-- SAMPLE CALL:
/*

DECLARE 
	@CommunicationConfigurationID BIGINT = NULL
	
EXEC comm.spCommunicationConfiguration_Delete
	@CommunicationConfigurationID = @CommunicationConfigurationID,
	@ApplicationID = 10,
	@BusinessID = 38,
	@MessageTypeID = 1,
	@CommunicationMethodID = 2,
	@ModifiedBy = 'dhughes'

*/

-- SELECT * FROM comm.CommunicationConfiguration
-- SELECT * FROM comm.CommunicationConfiguration_History
-- SELECT * FROM comm.MessageType
-- SELECT * FROM comm.CommunicationMethod
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationConfiguration_Delete]
	@CommunicationConfigurationID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@EntityID BIGINT = NULL,
	@MessageTypeID INT = NULL,
	@CommunicationMethodID INT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000),
		@ErrorSeverity INT = NULL,
		@ErrorState INT = NULL,
		@ErrorNumber INT = NULL,
		@ErrorLine INT = NULL,
		@ErrorProcedure  NVARCHAR(200) = NULL
		

	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------


	/*
	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @MessageTypeID
	IF @MessageTypeID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CommunicationConfiguration object. Object reference (@CommunicationConfiguration) ' +
			'is not set to an instance of an object. ' +
			'The parameter, @CommunicationConfiguration, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @CommunicationMethodID
	IF @CommunicationMethodID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CommunicationConfiguration object. Object reference (@CommunicationMethodID) ' +
			'is not set to an instance of an object. ' +
			'The parameter, @CommunicationMethodID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/
	
	-------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------
	IF @CommunicationConfigurationID IS NOT NULL
	BEGIN
		SET @ApplicationID = NULL;
		SET @BusinessID = NULL;
		SET @EntityID = NULL;
		SET @MessageTypeID = NULL;
		SET @CommunicationMethodID = NULL;
	END

	-------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy		

	-------------------------------------------------------------------------------
	-- Remove items from the collection.
	-------------------------------------------------------------------------------
	DELETE cc
	FROM comm.CommunicationConfiguration cc
	WHERE ( cc.CommunicationConfigurationID = @CommunicationConfigurationID )
		OR (
			(
				@ApplicationID IS NULL AND cc.ApplicationID IS NULL
				AND @BusinessID IS NULL AND cc.BusinessID IS NULL
				AND @EntityID IS NULL AND cc.EntityID IS NULL	
				AND cc.MessageTypeID = @MessageTypeID
				AND cc.CommunicationMethodID = @CommunicationMethodID				
			)
			OR (
				@ApplicationID IS NOT NULL AND cc.ApplicationID = @ApplicationID
				AND @BusinessID IS NULL AND cc.BusinessID IS NULL
				AND @EntityID IS NULL AND cc.EntityID IS NULL
				AND cc.MessageTypeID = @MessageTypeID
				AND cc.CommunicationMethodID = @CommunicationMethodID	
			) OR (
				@ApplicationID IS NOT NULL AND cc.ApplicationID = @ApplicationID
				AND @BusinessID IS NOT NULL AND cc.BusinessID = @BusinessID
				AND @EntityID IS NULL AND cc.EntityID IS NULL
				AND cc.MessageTypeID = @MessageTypeID
				AND cc.CommunicationMethodID = @CommunicationMethodID	
			) OR (
				@ApplicationID IS NOT NULL AND cc.ApplicationID = @ApplicationID
				AND @BusinessID IS NOT NULL AND cc.BusinessID = @BusinessID
				AND @EntityID IS NOT NULL AND cc.EntityID = @EntityID
				AND cc.MessageTypeID = @MessageTypeID
				AND cc.CommunicationMethodID = @CommunicationMethodID	
			)
		)

		
	
		
END
