﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2015
-- Description:	Creates a new CommunicationQueue object.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationQueueID BIGINT = NULL,
	@ApplicationID INT = 10,
	@BusinessID BIGINT = 49,
	@MessageTypeID INT = 5,
	@CommunicationMethodID INT = 3,
	@TargetAudienceSourceID INT = 1,
	@CommunicationName VARCHAR(256) = 'Test Flu Shots',
	@RunOn DATETIME = GETDATE(),
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC comm.spCommunicationQueue_Create
	@ApplicationID = @ApplicationID,
	@BusinessID = @BusinessID,
	@MessageTypeID = @MessageTypeID,
	@CommunicationMethodID = @CommunicationMethodID,
	@CommunicationName = @CommunicationName,
	@TargetAudienceSourceID = @TargetAudienceSourceID,
	@RunOn = @RunOn,
	@Parameters = 'DRLB||90||int*||*MINFILL||1||int*||*MINAGE||40||int',
	@CreatedBy = @CreatedBy,
	@CommunicationQueueID = @CommunicationQueueID OUTPUT

SELECT 	@CommunicationQueueID AS CommunicationQueueID

*/

-- SELECT * FROM comm.CommunicationQueue
-- SELECT * FROM comm.CommunicationQueueParam
-- SELECT * FROM comm.MessageType
-- SELECT * FROM comm.CommunicationMethod
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationQueue_Create]
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL, -- Optional: Used to identify the object if the identifier is not known.
	@BusinessID BIGINT,
	@MessageTypeID INT = NULL,
	@MessageTypeCode VARCHAR(50) = NULL, -- Optional: Used to identify the object if the identifier is not known.
	@CommunicationMethodID INT = NULL,
	@CommunicationMethodCode VARCHAR(50) = NULL, -- Optional: Used to identify the object if the identifier is not known.
	@CommunicationName VARCHAR(256) = NULL,
	@TargetAudienceSourceID INT = NULL,
	@TargetAudienceSourceCode VARCHAR(50) = NULL, -- Optional: Used to identify the object if the identifier is not known.
	@RunOn DATETIME = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@ExecutionStatusID INT = NULL,
	@ExecutionStatusCode VARCHAR(25) = NULL, -- Optional: Used to identify the object if the identifier is not known.
	@Parameters VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CommunicationQueueID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	-------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@MessageTypeID=' + dbo.fnToStringOrEmpty(@MessageTypeID) + ';' +
		'@MessageTypeCode=' + dbo.fnToStringOrEmpty(@MessageTypeCode) + ';' +
		'@CommunicationMethodID=' + dbo.fnToStringOrEmpty(@CommunicationMethodID) + ';' +
		'@CommunicationMethodCode=' + dbo.fnToStringOrEmpty(@CommunicationMethodCode) + ';' +
		'@RunOn=' + dbo.fnToStringOrEmpty(@RunOn) + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart) + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd) + ';' +
		'@ExecutionStatusID=' + dbo.fnToStringOrEmpty(@ExecutionStatusID) + ';' +
		'@Parameters=' + dbo.fnToStringOrEmpty(@Parameters) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@CommunicationQueueID=' + dbo.fnToStringOrEmpty(@CommunicationQueueID) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	---------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------------------
	SET @CommunicationQueueID = NULL;


	---------------------------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------
	-- @BusinessID
	IF @BusinessID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference is not set to an instance of an object. ' +
			'The parameter, @BusinessID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @MessageTypeID and @MessageTypeCode
	IF @MessageTypeID IS NULL AND @MessageTypeCode IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference is not set to an instance of an object. ' +
			'The parameters, @MessageTypeID and @MessageTypeCode, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	


	-- Debug
	--SELECT 'Debugger On' AS DebugMode, 'Before Variable Setters' AS MethodAction,
	--	@ApplicationID AS ApplicationID, @ApplicationCode AS ApplicationCode, @BusinessID AS BusinessID,
	--	@MessageTypeID AS MessageTypeID, @CommunicationMethodID AS CommunicationMethodID,
	--	@CommunicationMethodCode AS CommunicationMethodCode, @RunOn AS RunOn, @DateStart AS DateStart,
	--	@DateEnd AS DateEnd, @ExecutionStatusID AS ExecutionStatusID, @ExecutionStatusCode AS ExecutionStatusCode

	---------------------------------------------------------------------------------------------------------
	-- Set variables.
	---------------------------------------------------------------------------------------------------------
	-- ApplicationID
	SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationID);
	SET @ApplicationID = CASE WHEN @ApplicationID IS NOT NULL THEN @ApplicationID ELSE dbo.fnGetApplicationID(@ApplicationCode) END;
	-- MessageTypeID
	SET @MessageTypeID = comm.fnGetMessageTypeID(@MessageTypeID);
	SET @MessageTypeID = CASE WHEN @MessageTypeID IS NOT NULL THEN @MessageTypeID ELSE comm.fnGetMessageTypeID(@MessageTypeCode) END;
	-- CommunicationMethodID
	SET @CommunicationMethodID = comm.fnGetCommunicationMethodID(@CommunicationMethodID);
	SET @CommunicationMethodID = CASE WHEN @CommunicationMethodID IS NOT NULL THEN @CommunicationMethodID ELSE comm.fnGetCommunicationMethodID(@CommunicationMethodCode) END;
	-- TargetAudienceSourceID
	SET @TargetAudienceSourceID = comm.fnGetCommunicationMethodID(@TargetAudienceSourceID);
	SET @TargetAudienceSourceID = CASE WHEN @TargetAudienceSourceID IS NOT NULL THEN @TargetAudienceSourceID ELSE comm.fnGetTargetAudienceSourceID(@TargetAudienceSourceCode) END;
	-- ExecutionStatusID
	SET @ExecutionStatusID = etl.fnGetExecutionStatusID(@ExecutionStatusID);
	SET @ExecutionStatusID = CASE WHEN @ExecutionStatusID IS NOT NULL THEN @ExecutionStatusID ELSE etl.fnGetExecutionStatusID(@ExecutionStatusCode) END;
	-- Default ExecutionStatusID
	SET @ExecutionStatusID = CASE WHEN @ExecutionStatusID IS NOT NULL THEN @ExecutionStatusID ELSE etl.fnGetExecutionStatusID('CR') END;

	-- Debug
	--SELECT 'Debugger On' AS DebugMode, 'After Variable Setters' AS MethodAction,
	--	@ApplicationID AS ApplicationID, @ApplicationCode AS ApplicationCode, @BusinessID AS BusinessID,
	--	@MessageTypeID AS MessageTypeID, @CommunicationMethodID AS CommunicationMethodID,
	--	@CommunicationMethodCode AS CommunicationMethodCode, @TargetAudienceSourceID AS TargetAudienceSourceID,
	--	@TargetAudienceSourceCode AS TargetAudienceSourceCode, @RunOn AS RunOn, @DateStart AS DateStart, @DateEnd AS DateEnd,
	--	@ExecutionStatusID AS ExecutionStatusID, @ExecutionStatusCode AS ExecutionStatusCode


	--------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------
		---------------------------------------------------------------------------------------------------------
		-- Create object.
		---------------------------------------------------------------------------------------------------------
		INSERT INTO comm.CommunicationQueue (
			ApplicationID,
			BusinessID,
			MessageTypeID,
			CommunicationMethodID,
			CommunicationName,
			TargetAudienceSourceID,
			RunOn,
			DateStart,
			DateEnd,
			ExecutionStatusID,
			CreatedBy
		)
		SELECT
			@ApplicationID AS ApplicationID,
			@BusinessID AS BusinessID,
			@MessageTypeID AS MessageTypeID,
			@CommunicationMethodID AS CommunicationMethodID,
			@CommunicationName AS CommunicationName,
			@TargetAudienceSourceID AS TargetAudienceSourceID,
			@RunOn AS RunOn,
			@DateStart AS DateStart,
			@DateEnd AS DateEnd,
			@ExecutionStatusID AS ExecutionStatusID,
			@CreatedBy AS CreatedBy

		-- Retrieve object identifier.
		SET @CommunicationQueueID = SCOPE_IDENTITY();


		IF @CommunicationQueueID IS NOT NULL AND NULLIF(@Parameters, '') IS NOT NULL
		BEGIN
			--------------------------------------------------------------------------------------------------------
			-- Create object parameters.
			--------------------------------------------------------------------------------------------------------
			EXEC comm.spCommunicationQueueParam_ParseAndCreate
				@CommunicationQueueID = @CommunicationQueueID,
				@ParamString = @Parameters,
				@CreatedBy = @CreatedBy
		END


 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH

END
