﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/29/2015
-- Description:	Deletes a single or multiple MessageTypeConfiguration object(s).
-- SAMPLE CALL:
/*

	
EXEC comm.spMessageTypeConfiguration_DeleteRange
	@ApplicationID = 10,
	@BusinessID = 38,
	@MessageTypeID = '1,2'



*/

-- SELECT * FROM comm.MessageTypeConfiguration
-- =============================================
CREATE PROCEDURE [comm].[spMessageTypeConfiguration_DeleteRange]
	@MessageTypeConfigurationID VARCHAR(MAX) = NULL,
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@EntityID BIGINT = NULL,
	@MessageTypeID VARCHAR(MAX) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000),
		@ErrorSeverity INT = NULL,
		@ErrorState INT = NULL,
		@ErrorNumber INT = NULL,
		@ErrorLine INT = NULL,
		@ErrorProcedure  NVARCHAR(200) = NULL
		
		
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------

	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------

	/*
	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @MessageTypeID
	IF @MessageTypeID IS NULL OR @MessageTypeID = ''
	BEGIN
		SET @ErrorMessage = 'Unable to create MessageTypeConfiguration object. Object reference (@MessageTypeID) ' +
			'is not set to an instance of an object. ' +
			'The parameter, @MessageTypeID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/	
	
	-------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------	
	IF @MessageTypeConfigurationID IS NOT NULL OR @MessageTypeConfigurationID <> ''
	BEGIN
		SET @ApplicationID = NULL;
		SET @BusinessID = NULL;
		SET @EntityID = NULL;
		SET @MessageTypeID = NULL;
	END	
	
	-------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy



	-------------------------------------------------------------------------------
	-- Remove items from the collection.
	-------------------------------------------------------------------------------
	DELETE mtc
	FROM comm.MessageTypeConfiguration mtc
	WHERE ( mtc.MessageTypeConfigurationID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeConfigurationID, ',') WHERE ISNUMERIC(Value) = 1) )
		OR (
			( 
				@ApplicationID IS NULL AND mtc.ApplicationID IS NULL
				AND @BusinessID IS NULL AND mtc.BusinessID IS NULL
				AND @EntityID IS NULL AND mtc.EntityID IS NULL	
				AND mtc.MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeID, ',') WHERE ISNUMERIC(Value) = 1)			
			)
			OR (
				@ApplicationID IS NOT NULL AND mtc.ApplicationID = @ApplicationID
				AND @BusinessID IS NULL AND mtc.BusinessID IS NULL
				AND @EntityID IS NULL AND mtc.EntityID IS NULL
				AND mtc.MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeID, ',') WHERE ISNUMERIC(Value) = 1)
			) OR (
				@ApplicationID IS NOT NULL AND mtc.ApplicationID = @ApplicationID
				AND @BusinessID IS NOT NULL AND mtc.BusinessID = @BusinessID
				AND @EntityID IS NULL AND mtc.EntityID IS NULL
				AND mtc.MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeID, ',') WHERE ISNUMERIC(Value) = 1)
			) OR (
				@ApplicationID IS NOT NULL AND mtc.ApplicationID = @ApplicationID
				AND @BusinessID IS NOT NULL AND mtc.BusinessID = @BusinessID
				AND @EntityID IS NOT NULL AND mtc.EntityID = @EntityID
				AND mtc.MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeID, ',') WHERE ISNUMERIC(Value) = 1)
			)			
		)
				
	

		
		
END
