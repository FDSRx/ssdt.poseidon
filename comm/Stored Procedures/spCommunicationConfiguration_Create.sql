﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/29/2015
-- Description:	Creates a new CommunicationConfiguration object.
-- SAMPLE CALL:
/*

DECLARE 
	@CommunicationConfigurationID BIGINT = NULL
	
EXEC comm.spCommunicationConfiguration_Create
	@ApplicationID = 10,
	@BusinessID = 38,
	@MessageTypeID = 1,
	@CommunicationMethodID = 2,
	@CommunicationConfigurationID = @CommunicationConfigurationID OUTPUT

SELECT @CommunicationConfigurationID AS CommunicationConfigurationID


*/

-- SELECT * FROM comm.CommunicationConfiguration
-- SELECT * FROM comm.MessageType
-- SELECT * FROM comm.CommunicationMethod
-- =============================================
CREATE PROCEDURE [comm].[spCommunicationConfiguration_Create]
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@EntityID BIGINT = NULL,
	@MessageTypeID INT,
	@CommunicationMethodID INT,
	@MessagesPerMinutes INT = NULL,
	@MessagesNumberOfMinutes INT = NULL,
	@MessagesPerDays INT = NULL,
	@MessagesNumberOfDays INT = NULL,
	@IsDisabled BIT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CommunicationConfigurationID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @CommunicationConfigurationID = NULL;
	SET @IsDisabled = ISNULL(@IsDisabled, 0);
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @MessageTypeID
	IF @MessageTypeID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CommunicationConfiguration object. Object reference (@CommunicationConfiguration) ' +
			'is not set to an instance of an object. ' +
			'The parameter, @CommunicationConfiguration, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @CommunicationMethodID
	IF @CommunicationMethodID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create CommunicationConfiguration object. Object reference (@CommunicationMethodID) ' +
			'is not set to an instance of an object. ' +
			'The parameter, @CommunicationMethodID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
		
	-------------------------------------------------------------------------------
	-- Create new CommunicationConfiguration object.
	-------------------------------------------------------------------------------
	INSERT INTO comm.CommunicationConfiguration(
		ApplicationID,
		BusinessID,
		EntityID,
		MessageTypeID,
		CommunicationMethodID,
		MessagesPerMinutes,
		MessagesNumberOfMinutes,
		MessagesPerDays,
		MessagesNumberOfDays,
		IsDisabled,
		CreatedBy
	)
	SELECT
		@ApplicationID AS ApplicationID,
		@BusinessID AS BusinessID,
		@EntityID AS EntityID,
		@MessageTypeID AS MessageTypeID,
		@CommunicationMethodID AS CommunicationMethodID,
		@MessagesPerMinutes AS MessagesPerMinutes,
		@MessagesNumberOfMinutes AS MessagesNumberOfMinutes,
		@MessagesPerDays AS MessagesPerDays,
		@MessagesNumberOfDays AS MessagesNumberOfDays,
		@IsDisabled AS IsDisabled,
		@CreatedBy AS CreatedBy
	
	-- Retrieve record identity value.
	SET @CommunicationConfigurationID = SCOPE_IDENTITY();
		
END
