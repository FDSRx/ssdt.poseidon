﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/8/2013
-- Description:	API wrapper around the credential group get method.
-- SAMPLE CALL:
/*
-- Result set
EXEC api.spCreds_CredentialEntityGroup_Get_List
	@CredentialToken = 'D1D88CF0-5CDE-4410-81CE-EBC92A0D0636'
	
-- XML result set
EXEC api.spCreds_CredentialEntityGroup_Get_List
	@CredentialToken = 'D1D88CF0-5CDE-4410-81CE-EBC92A0D0636',
	@IsResultXml = 1
	
*/
-- SELECT * FROM creds.CredentialToken
-- EXEC creds.spCredentialToken_Update @Token = 'D1D88CF0-5CDE-4410-81CE-EBC92A0D0636', @ForceUpdate = 1
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialEntityGroup_Get_List]
	@ApplicationKey VARCHAR(25) = NULL,
	@BusinessToken VARCHAR(50) = NULL,
	@CredentialToken VARCHAR(50),
	@IsResultXml BIT = 0,
	@IsResultXmlOutputOnly BIT = 0,
	@GroupData XML = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------
	-- Sanitize input
	----------------------------------------------------
	SET @IsResultXml = ISNULL(@IsResultXml, 0);
	SET @IsResultXmlOutputOnly = ISNULL(@IsResultXmlOutputOnly, 0);
	SET @GroupData = NULL;

	----------------------------------------------------
	-- Local Variables
	----------------------------------------------------
	DECLARE
		@CredentialEntityID BIGINT,
		@ApplicationID INT,
		@BusinessEntityID INT
	
	
	----------------------------------------------------
	-- Get credential entity information
	----------------------------------------------------
	EXEC creds.spCredentialToken_Update	
		@Token = @CredentialToken OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessEntityID = @BusinessEntityID OUTPUT
		
	
	----------------------------------------------------
	-- Fetch list of applicable groups
	-- <Summary>
	-- Obtains a list of applicable GroupIDs
	-- </Summary>	
	-- <Remarks>
	-- Utilizes the core get groups call to resolve
	-- groups.  Allows logic to remain in a standarized location.
	-- </Remarks>
	----------------------------------------------------
	EXEC acl.spCredentialEntityGroup_Get_List
		@CredentialEntityID = @CredentialEntityID,
		@ApplicationID = @ApplicationID,
		@BusinessEntityID = @BusinessEntityID,
		@IsResultXml = @IsResultXml,
		@IsResultXmlOutputOnly = @IsResultXmlOutputOnly,
		@GroupData = @GroupData OUTPUT
		
		
END
