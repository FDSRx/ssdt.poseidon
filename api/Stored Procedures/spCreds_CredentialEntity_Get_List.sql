﻿

-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/3/2014
-- Description:	Returns a list of users that are applicable to the businesses and applications.
-- Change Log:
-- 2/17/2016 - dhughes - Modified the procedure's filtering logic to better handle enforcement rules and no attached applications
--						to a credential.
-- 2/17/2016 - dhughes - Modified the procedure to incorporate roles into the application return data (application xml).

-- SAMPLE CALL:
/*

-- Application and Business call.
EXEC api.spCreds_CredentialEntity_Get_List
	@ApplicationKey = '8',
	@BusinessKey = '3759'
	--,@FirstName = 'dan'
	--,@SortCollection = 'DateCreated|Asc'

-- Application call: Enforce application but not business specification.
EXEC api.spCreds_CredentialEntity_Get_List
	@ApplicationKey = '8',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 0

-- Application call: Enforce application but not business specification.	
EXEC api.spCreds_CredentialEntity_Get_List
	@ApplicationKey = '2',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 0

-- Role call	
EXEC api.spCreds_CredentialEntity_Get_List
	@ApplicationKey = '2',
	@RoleKey = 'Admin',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 0
	
*/

-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntityApplication
-- SELECT * FROM dbo.Application
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialEntity_Get_List]
	@ApplicationKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	@CredentialKey VARCHAR(MAX) = NULL,
	@CredentialTypeKey VARCHAR(50) = NULL,
	@RoleKey VARCHAR(MAX) = NULL,
	@Username VARCHAR(256) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@EnforceBusinessSpecification BIT = NULL,
	@EnforceApplicationSpecification BIT = NULL

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	--------------------------------------------------------------------------------------------
	-- Result schema binding
	-- <Summary>
	-- Returns a schema of the inteded result set.  This is a workaround to assist certain
	-- frameworks in determining the correct result sets (i.e. SSIS, Entity Framework, etc.).
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF (1 = 2)
	BEGIN
		DECLARE
			@Property VARCHAR(MAX) = NULL;
			
		SELECT 
			CONVERT(BIGINT, @Property) AS BusinessID,
			CONVERT(VARCHAR(50), @Property) AS BusinessToken,
			CONVERT(VARCHAR(256), @Property) AS BusinessName,
			CONVERT(BIGINT, @Property) AS CredentialEntityID,
			CONVERT(INT, @Property) AS CredentialEntityTypeID,
			CONVERT(VARCHAR(50), @Property) AS CredentialEntityTypeCode,
			CONVERT(VARCHAR(50), @Property) AS CredentialEntityTypeName,
			CONVERT(VARCHAR(256), @Property) AS Username,
			CONVERT(VARCHAR(75), @Property) AS FirstName,
			CONVERT(VARCHAR(75), @Property) AS LastName,
			CONVERT(DATETIME, @Property) AS BirthDate,
			CONVERT(VARCHAR(256), @Property) AS AddressLine1,
			CONVERT(VARCHAR(256), @Property) AS AddressLine2,
			CONVERT(VARCHAR(256), @Property) AS City,
			CONVERT(VARCHAR(256), @Property) AS State,
			CONVERT(VARCHAR(256), @Property) AS PostalCode,
			CONVERT(VARCHAR(256), @Property) AS HomePhone,
			CONVERT(VARCHAR(256), @Property) AS MobilePhone,
			CONVERT(VARCHAR(256), @Property) AS PrimaryEmail,
			CONVERT(VARCHAR(256), @Property) AS AlternateEmail,
			CONVERT(BIT, @Property) AS IsLockedOut,
			CONVERT(BIT, @Property) AS IsDisabled,
			CONVERT(VARCHAR(MAX), @Property) AS RoleData,
			CONVERT(VARCHAR(MAX), @Property) AS ApplicationData,
			CONVERT(VARCHAR(MAX), @Property) AS ImageData,
			CONVERT(DATETIME, @Property) AS DateCreated,
			CONVERT(DATETIME, @Property) AS DateModified,
			CONVERT(VARCHAR(256), @Property) AS CreatedBy,
			CONVERT(VARCHAR(256), @Property) AS ModifiedBy,
			CONVERT(BIGINT, @Property) AS TotalRecords
	END
		
	--------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName) + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';' +
		'@EnforceBusinessSpecification=' + dbo.fnToStringOrEmpty(@EnforceBusinessSpecification) + ';' +
		'@EnforceApplicationSpecification=' + dbo.fnToStringOrEmpty(@EnforceApplicationSpecification) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, NULLIF(@BusinessKeyType, ''));
	SET @EnforceBusinessSpecification = ISNULL(@EnforceBusinessSpecification, 1);
	SET @EnforceApplicationSpecification = ISNULL(@EnforceApplicationSpecification, 1);
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationID INT,
		@BusinessID BIGINT = NULL,
		@CredentialEntityTypeID INT = NULL,
		@CredentialEntityID BIGINT = NULL,
		@RoleIDArray VARCHAR(MAX) = NULL
		
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------	
	-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
	-- business key type.  If a key type was not supplied then attempt a trial and error conversion
	-- based on token or NABP.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialKey = @CredentialKey,
		@CredentialTypeKey = @CredentialTypeKey,
		@RoleKey = @RoleKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityTypeID = @CredentialEntityTypeID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@RoleIDArray = @RoleIDArray OUTPUT,
		@IsOutputOnly = 1


	
	-- Debug
	--SELECT @ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @CredentialEntityID AS CredentialEntityID
	
	--------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'BusinessEntityID', 'BusinessID', 'LastName', 'FirstName', 'Username') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;

	--------------------------------------------------------------------------------------------
	-- Find the applicable users.
	--------------------------------------------------------------------------------------------
	;WITH cteUsers AS (
	
		SELECT
			CASE 
				WHEN @SortField IN ('BusinessEntityID', 'BusinessID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY ce.BusinessEntityID ASC) 
				WHEN @SortField IN ('BusinessEntityID', 'BusinessID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY ce.BusinessEntityID DESC)     
				WHEN @SortField = 'FirstName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY psn.FirstName ASC) 
				WHEN @SortField = 'FirstName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY psn.FirstName DESC) 
				WHEN @SortField = 'LastName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY psn.LastName ASC) 
				WHEN @SortField = 'LastName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY psn.LastName DESC)
				WHEN @SortField = 'Username' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY eu.Username ASC) 
				WHEN @SortField = 'Username' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY eu.Username DESC)  
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY eu.DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY eu.DateCreated DESC)   
				ELSE ROW_NUMBER() OVER (ORDER BY psn.FirstName ASC)
			END AS RowNumber,
			biz.BusinessEntityID AS BusinessID,
			biz.BusinessToken AS BusinessToken,
			biz.Name AS BusinessName,
			ce.CredentialEntityID,
			typ.CredentialEntityTypeID,
			typ.Code AS CredentialEntityTypeCode,
			typ.Name AS CredentialEntityTypeName,
			eu.Username,
			psn.FirstName,
			psn.LastName,
			psn.BirthDate,
			psn.AddressLine1,
			psn.AddressLine2,
			psn.City,
			psn.State,
			psn.PostalCode,
			psn.HomePhone,
			psn.MobilePhone,
			psn.PrimaryEmail,
			psn.AlternateEmail,
			mem.IsLockedOut,
			mem.IsDisabled,
			CONVERT(XML, (
				SELECT
					cr.RoleID AS Id,
					r.Code,
					r.Name,
					CONVERT(XML, (
						SELECT
							crp.PermissionID AS Id,
							prm.Code,
							prm.Name
						FROM acl.CredentialEntityRolePermission crp
							JOIN acl.Permission prm
								ON prm.PermissionID = crp.PermissionID
						WHERE crp.CredentialEntityID = ce.CredentialEntityID
							AND crp.ApplicationID = a.ApplicationID
							AND crp.BusinessID = ce.BusinessEntityID
							AND crp.RoleID = r.RoleID	
						FOR XML PATH('Permission')								 
					)) AS [Permissions]
				--SELECT *
				FROM acl.CredentialEntityRole cr (NOLOCK)
					JOIN acl.Roles r (NOLOCK)
						ON cr.RoleID = r.RoleID
					JOIN dbo.Application a (NOLOCK)
						ON a.ApplicationID = cr.ApplicationID
				WHERE cr.BusinessEntityID = ce.BusinessEntityID
					AND cr.ApplicationID = @ApplicationID
					AND cr.CredentialEntityID = ce.CredentialEntityID
				FOR XML PATH('Role'), ROOT('Roles')
			)) AS RoleData,
			CONVERT(XML, (
				SELECT DISTINCT
					crp.PermissionID AS Id,
					prm.Code,
					prm.Name
				FROM acl.CredentialEntityRolePermission crp
					JOIN acl.Roles r (NOLOCK)
						ON crp.RoleID = r.RoleID
					JOIN dbo.Application a (NOLOCK)
						ON a.ApplicationID = crp.ApplicationID
					JOIN acl.Permission prm
						ON prm.PermissionID = crp.PermissionID
				WHERE crp.CredentialEntityID = ce.CredentialEntityID
					AND crp.ApplicationID = a.ApplicationID
					AND crp.BusinessID = ce.BusinessEntityID
					AND crp.RoleID = r.RoleID	
				FOR XML PATH('Permission'), ROOT('Permissions')							 
			)) AS PermissionData,
			CONVERT(XML, (
				SELECT
					ca.ApplicationID AS Id,
					a.Code,
					a.Name,
					CONVERT(XML, (
						SELECT
							cr.RoleID AS Id,
							r.Code,
							r.Name,
							CONVERT(XML, (
								SELECT
									crp.PermissionID AS Id,
									prm.Code,
									prm.Name
								FROM acl.CredentialEntityRolePermission crp
									JOIN acl.Permission prm
										ON prm.PermissionID = crp.PermissionID
								WHERE crp.CredentialEntityID = ce.CredentialEntityID
									AND crp.ApplicationID = a.ApplicationID
									AND crp.BusinessID = ce.BusinessEntityID
									AND crp.RoleID = r.RoleID	
								FOR XML PATH('Permission')								 
							)) AS [Permissions]
						--SELECT *
						FROM acl.CredentialEntityRole cr (NOLOCK)
							JOIN acl.Roles r (NOLOCK)
								ON cr.RoleID = r.RoleID
						WHERE cr.BusinessEntityID = ce.BusinessEntityID
							AND cr.ApplicationID = a.ApplicationID
							AND cr.CredentialEntityID = ce.CredentialEntityID
						FOR XML PATH('Role')
					)) AS Roles,
					CONVERT(XML, (
						SELECT DISTINCT
							crp.PermissionID AS Id,
							prm.Code,
							prm.Name
						FROM acl.CredentialEntityRolePermission crp
							JOIN acl.Roles r (NOLOCK)
								ON crp.RoleID = r.RoleID
							JOIN dbo.Application a (NOLOCK)
								ON a.ApplicationID = crp.ApplicationID
							JOIN acl.Permission prm
								ON prm.PermissionID = crp.PermissionID
						WHERE crp.CredentialEntityID = ce.CredentialEntityID
							AND crp.ApplicationID = a.ApplicationID
							AND crp.BusinessID = ce.BusinessEntityID
							AND crp.RoleID = r.RoleID	
						FOR XML PATH('Permission')					 
					)) AS [Permissions]
				FROM creds.CredentialEntityApplication ca (NOLOCK)
					JOIN dbo.Application a (NOLOCK)
						ON ca.ApplicationID = a.ApplicationID
				WHERE ca.CredentialEntityID = ce.CredentialEntityID
				FOR XML PATH('Application'), ROOT('Applications')
			)) AS ApplicationData,
			CONVERT(XML, (
				SELECT 
					img.ImageID AS Id,
					img.KeyName
				--SELECT *
				FROM dbo.Images img (NOLOCK)
				WHERE ApplicationID = @ApplicationID
					AND BusinessID = ce.BusinessEntityID
					AND PersonID = ISNULL(ce.PersonID, ce.CredentialEntityID)
					AND KeyName IN ('ProfilePictureSmall')
				FOR XML PATH('PictureFile'), ROOT('PictureFiles')
			)) AS ImageData,
			ce.DateCreated,
			ce.DateModified,
			ce.CreatedBy,
			ce.ModifiedBy
		-- SELECT *
		FROM creds.CredentialEntity ce (NOLOCK)
			LEFT JOIN dbo.Business biz (NOLOCK)
				ON ce.BusinessEntityID = biz.BusinessEntityID
			LEFT JOIN creds.CredentialEntityType typ	 (NOLOCK)
				ON ce.CredentialEntityTypeID = typ.CredentialEntityTypeID
			LEFT JOIN dbo.vwPerson psn (NOLOCK)
				ON ce.PersonID = psn.PersonID
			LEFT JOIN creds.ExtranetUser eu (NOLOCK)
				ON ce.CredentialEntityID = eu.CredentialEntityID
			LEFT JOIN creds.Membership mem (NOLOCK)
				ON ce.CredentialEntityID = mem.CredentialEntityID
		WHERE ( 
				( @EnforceBusinessSpecification = 0 AND ( NULLIF(@BusinessKey, '') IS NULL OR ( NULLIF(@BusinessKey, '') IS NOT NULL AND ce.BusinessEntityID = @BusinessID ) ))
					OR ( @EnforceBusinessSpecification = 1 AND ce.BusinessEntityID = @BusinessID )
			)
			AND ( 
				( @EnforceApplicationSpecification = 0 AND ( NULLIF(@ApplicationKey, '') IS NULL OR ( NULLIF(@ApplicationKey, '') IS NOT NULL
					AND EXISTS (
						SELECT TOP 1 *
						--SELECT *
						FROM creds.CredentialEntityApplication cea (NOLOCK)
						WHERE cea.CredentialEntityID = ce.CredentialEntityID
							AND cea.ApplicationID = @ApplicationID
					)
				)))
				OR (@EnforceApplicationSpecification = 1 AND EXISTS (
						SELECT TOP 1 *
						--SELECT *
						FROM creds.CredentialEntityApplication cea (NOLOCK)
						WHERE cea.CredentialEntityID = ce.CredentialEntityID
							AND cea.ApplicationID = @ApplicationID
					))
			)
			AND ( ( NULLIF(@Username, '') IS NULL ) OR ( NULLIF(@Username, '') IS NOT NULL AND eu.Username LIKE '%' + @Username + '%' ))
			AND ( ( NULLIF(@FirstName, '') IS NULL ) OR ( NULLIF(@FirstName, '') IS NOT NULL AND psn.FirstName LIKE '%' + @FirstName + '%' ))
			AND ( ( NULLIF(@LastName, '') IS NULL ) OR ( NULLIF(@LastName, '') IS NOT NULL AND psn.LastName LIKE '%' + @LastName + '%' ))
			AND ( ( NULLIF(@CredentialKey, '') IS NULL ) OR ( NULLIF(@CredentialKey, '') IS NOT NULL AND ce.CredentialEntityID IN (SELECT Value FROM dbo.fnSplit(@CredentialKey, ',') WHERE ISNUMERIC(Value) = 1 ) ))
			AND ( ( NULLIF(@RoleKey, '') IS NULL ) OR ( NULLIF(@RoleKey, '') IS NOT NULL AND EXISTS (
				SELECT TOP 1 * 
				--SELECT *
				FROM acl.CredentialEntityRole r (NOLOCK)
				WHERE r.ApplicationID = @ApplicationID
					AND r.RoleID IN ( SELECT Value FROM dbo.fnSplit(@RoleIDArray, ',') WHERE ISNUMERIC(Value) = 1 ) 
					AND r.CredentialEntityID = ce.CredentialEntityID
				)
			))
	)
	
	SELECT 
		BusinessID,
		BusinessToken,
		BusinessName,
		CredentialEntityID,
		CredentialEntityTypeID,
		CredentialEntityTypeCode,
		CredentialEntityTypeName,
		Username,
		FirstName,
		LastName,
		BirthDate,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		HomePhone,
		MobilePhone,
		PrimaryEmail,
		AlternateEmail,
		IsLockedOut,
		IsDisabled,
		RoleData,
		PermissionData,
		ApplicationData,
		ImageData,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteUsers) AS TotalRecords
	FROM cteUsers
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
	
END


