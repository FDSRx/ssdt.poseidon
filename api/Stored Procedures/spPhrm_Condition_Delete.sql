﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/2/2015
-- Description:	Deletes a Condition object.
-- SAMPLE CALL:

/*

DECLARE
	@ConditionID BIGINT

EXEC api.[spPhrm_Condition_Delete]
	@ConditionKey = -1,
	@BusinessKey = 135590

SELECT @ConditionID AS ConditionID

*/

-- SELECT * FROM phrm.Condition
-- SELECT * FROM dbo.CodeQualifier
-- SELECT * FROM phrm.ConditionType
-- SELECT * FROM dbo.Origin
-- SELECT * From phrm.vwPatient WHERE LastName LIKE '%Simmons%'
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Condition_Delete]
	@ConditionKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*
	-- Log incoming arguments
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	DECLARE @Args VARCHAR(MAX) =
		'@ConditionKey=' + dbo.fnToStringOrEmpty(@ConditionKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' 
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = NULL,
		@Arguments = @Args
	*/
	
	--------------------------------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------------------------------
	-- Sanitation code goes here.
	
	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE
		@ConditionID BIGINT = CONVERT(BIGINT, @ConditionKey),
		@BusinessID BIGINT = NULL,
		@PersonID BIGINT = NULL,
		@ErrorMessage VARCHAR(4000)
	
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------	
	-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
	-- business key type.  If a key type was not supplied then attempt a trial and error conversion
	-- based on token or NABP.
	--------------------------------------------------------------------------------------------	
	-- Auto translater
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PatientKey = @PatientKey,
		@PatientKeyType = @PatientKeyType,
		@BusinessID = @BusinessID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1
	
	--------------------------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	--------------------------------------------------------------------------------------------
	-- Argument validation code goes here.

	
	
	--------------------------------------------------------------------------------------------
	-- Remove condition record.
	-- <Summary>
	-- Deletes the Condition object from the condition repository.
	-- </Summary>
	--------------------------------------------------------------------------------------------
	EXEC phrm.spCondition_Delete
		@ConditionID = @ConditionID,
		@BusinessEntityID = @PersonID
	
	
	
	
	
	
	
	
	
			
		
END
