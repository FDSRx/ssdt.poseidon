﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Gets a list of Note objects that are applicable to a patient.
-- Change log:
-- 6/8/2016 - dhughes - Modified the procedure to accept the Application object as a parameter.

-- SAMPLE CALL;
/*

DECLARE
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = '7871787',
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = '21656',
	@PatientKeyType VARCHAR(50) = NULL,
	@ScopeKey VARCHAR(MAX) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Note object identifiers.
	@NoteTypeKey VARCHAR(MAX) = 'GNRL', -- Single or comma delimited list of NoteType object keys.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spPhrm_Patient_Note_Get_List
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@ScopeKey = @ScopeKey,
	@NoteKey = @NoteKey,
	@NoteTypeKey = @NoteTypeKey,
	@NotebookKey = @NotebookKey,
	@PriorityKey = @PriorityKey,
	@OriginKey = @OriginKey,
	@TagKey = @TagKey,
	@DataDateStart = @DataDateStart,
	@DataDateEnd = @DataDateEnd,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug

 */

-- SELECT * FROM phrm.fnGetPatientInformation(2) 
-- SELECT * FROM dbo.NoteType

-- SELECT * FROM dbo.ErrorLog
-- SELECT * FROM dbo.InformationLog
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Note_Get_List]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@ScopeKey VARCHAR(MAX) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Note object identifiers.
	@NoteTypeKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of NoteType object keys.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' +
		'@ScopeKey=' + dbo.fnToStringOrEmpty(@ScopeKey) + ';' +
		'@NoteTypeKey=' + dbo.fnToStringOrEmpty(@NoteTypeKey) + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey) + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey) + ';' +
		'@TagKey=' + dbo.fnToStringOrEmpty(@TagKey) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		'@DataDateStart=' + dbo.fnToStringOrEmpty(@DataDateStart) + ';' +
		'@DataDateEnd=' + dbo.fnToStringOrEmpty(@DataDateEnd) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------------------------------------------------------
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @PatientKeyType = ISNULL(@PatientKeyType, 'SourcePatientKey')

	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@ScopeID INT = dbo.fnGetScopeID('INTRN') -- only review internal notes
	
	------------------------------------------------------------------------------------------------------------
	-- Set local variables
	------------------------------------------------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, @BusinessKeyType, @PatientKey, @PatientKeyType);
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');
	

	-- debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod, 
			@PatientID AS PatientID, @personID AS PersonID,
			@ApplicationKey AS ApplicationKey, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@Args AS Arguments
	END
	
	------------------------------------------------------------------------------------------------------------
	-- Return patient Note objects.
	------------------------------------------------------------------------------------------------------------
	EXEC api.spDbo_Note_Get_List
		@NoteKey = @NoteKey,
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@ScopeKey = @ScopeID,
		@BusinessEntityKey = @PersonID,
		@NoteTypeKey = @NoteTypeKey,
		@NotebookKey = @NotebookKey,
		@PriorityKey = @PriorityKey,
		@OriginKey = @OriginKey,
		@TagKey = @TagKey,
		@DataDateStart = @DataDateStart,
		@DataDateEnd = @DataDateEnd,
		@Skip = @Skip,
		@Take = @Take,
		@SortCollection = @SortCollection
			
END
