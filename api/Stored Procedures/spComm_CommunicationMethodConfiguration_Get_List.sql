﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/25/2015
-- Description:	Returns a list of CommunicationMethod objects.
-- SAMPLE CALL:
/*

-- Identifier.
EXEC api.spComm_CommunicationMethodConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@CommunicationMethodConfigurationKey = 1
	
-- Business.
EXEC api.spComm_CommunicationMethodConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684'
	
-- Application.
EXEC api.spComm_CommunicationMethodConfiguration_Get_List
	@ApplicationKey = 'ENG'
	
-- MessageTypes.
EXEC api.spComm_CommunicationMethodConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@CommunicationMethodKey = 'VOICE,EMAIL'
	
-- Strict.
EXEC api.spComm_CommunicationMethodConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 1
		
-- Strict.
EXEC api.spComm_CommunicationMethodConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '38',
	@BusinessKeyType = 'Id',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 1


*/
-- SELECT * FROM comm.MessageTypeConfiguration
-- SELECT * FROM comm.MessageType
-- =============================================
CREATE PROCEDURE [api].[spComm_CommunicationMethodConfiguration_Get_List]
	@CommunicationMethodConfigurationKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonTypeKey VARCHAR(50) = NULL,
	@CommunicationMethodKey VARCHAR(MAX) = NULL,
	@EnforceApplicationSpecification BIT = NULl,
	@EnforceBusinessSpecification BIT = NULL,
	@EnforceEntitySpecification BIT = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Result schema binding
	-- <Summary>
	-- Returns a schema of the intended result set.  This is a workaround to assist certain
	-- frameworks in determining the correct result sets (i.e. SSIS, Entity Framework, etc.).
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF (1 = 2)
	BEGIN
		DECLARE
			@Property VARCHAR(MAX) = NULL;
			
		SELECT 
			CONVERT(BIGINT, @Property) AS CommunicationMethodConfigurationID,
			CONVERT(INT, @Property) AS ApplicationID,
			CONVERT(VARCHAR(50), @Property) AS ApplicationCode,
			CONVERT(VARCHAR(50), @Property) AS ApplicationName,
			CONVERT(BIGINT, @Property) AS BusinessID,
			CONVERT(BIGINT, @Property) AS EntityID,
			CONVERT(INT, @Property) AS MessageTypeID,
			CONVERT(VARCHAR(50), @Property) AS Code,
			CONVERT(VARCHAR(50), @Property) AS Name,
			CONVERT(VARCHAR(1000), @Property) AS Description,
			CONVERT(UNIQUEIDENTIFIER, @Property) AS rowguid,
			CONVERT(DATETIME, @Property) AS DateCreated,
			CONVERT(DATETIME, @Property) AS DateModified,
			CONVERT(VARCHAR(256), @Property) AS CreatedBy,
			CONVERT(VARCHAR(256), @Property) AS ModifiedBy,
			CONVERT(BIGINT, @Property) AS TotalRecords
	END
	
	--------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationMethodConfigurationKey=' + dbo.fnToStringOrEmpty(@CommunicationMethodConfigurationKey) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PersonKey=' + dbo.fnToStringOrEmpty(@PersonKey) + ';' +
		'@PersonTypeKey=' + dbo.fnToStringOrEmpty(@PersonTypeKey) + ';' +
		'@CommunicationMethodKey=' + dbo.fnToStringOrEmpty(@CommunicationMethodKey) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @EnforceApplicationSpecification = ISNULL(@EnforceApplicationSpecification, 0);
	SET @EnforceBusinessSpecification = ISNULL(@EnforceBusinessSpecification, 0);
	SET @EnforceEntitySpecification = ISNULL(@EnforceEntitySpecification, 0);
	
		
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL,
		@PersonID BIGINT = NULL,
		@CommunicationMethodIdArray VARCHAR(MAX) = comm.fnCommunicationMethodKeyTranslator(@CommunicationMethodKey, DEFAULT)

	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PersonKey = @PersonKey,
		@PersonTypeKey = @PersonTypeKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1	

	-- Debug
	--SELECT @ApplicationID AS AplicationID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
	--	@BusinessID AS BusinessID, @PersonKey AS PersonKey, @PersonTypeKey AS PersonTypeKey, @PersonID AS PersonID,
	--	@CommunicationMethodIdArray
		
		
	--------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
		
	--------------------------------------------------------------------------------------------
	-- Fetch results.
	-- <Summary>
	-- Uses "bubble up" functionality to determine results (i.e. looks at specific criteria,
	-- to least specific criteria).
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF @EnforceApplicationSpecification = 0 AND @EnforceBusinessSpecification = 0
		AND @EnforceEntitySpecification = 0
	BEGIN
	
		;WITH cteItems AS (
			SELECT
				CASE 
					WHEN @SortField IN ('CommunicationMethodID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationMethodID ASC) 
					WHEN @SortField IN ('CommunicationMethodID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationMethodID DESC)     
					WHEN @SortField = 'Code' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY Code ASC) 
					WHEN @SortField = 'Code' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY Code DESC) 
					WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY Name ASC) 
					WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY Name DESC)
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated ASC) 
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated DESC)   
					ELSE ROW_NUMBER() OVER (ORDER BY Name ASC)
				END AS RowNumber,
				CommunicationMethodConfigurationID,
				ApplicationID,
				ApplicationCode,
				ApplicationName,
				BusinessID,
				EntityID,
				CommunicationMethodID,
				Code,
				Name,
				Description,
				rowguid,
				DateCreated,
				DateModified,
				CreatedBy,
				ModifiedBy
			--SELECT *
			FROM comm.fnGetCommunicationMethodConfigurations(@ApplicationID, @BusinessID, @PersonID)
			WHERE ( @CommunicationMethodConfigurationKey IS NULL OR CommunicationMethodConfigurationID IN (
						SELECT Value FROM dbo.fnSplit(@CommunicationMethodConfigurationKey, ',') WHERE ISNUMERIC(Value) =1) 
				)
				AND (@CommunicationMethodKey IS NULL OR CommunicationMethodID IN (SELECT Value FROM dbo.fnSplit(@CommunicationMethodIdArray, ',') WHERE ISNUMERIC(Value) = 1))
		)

		SELECT
			CommunicationMethodConfigurationID,
			ApplicationID,
			ApplicationCode,
			ApplicationName,
			BusinessID,
			EntityID,
			CommunicationMethodID,
			Code,
			Name,
			Description,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy,
			(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
		FROM cteItems
		WHERE RowNumber > @Skip 
			AND RowNumber <= (@Skip + @Take)
		ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.

	END
	
	--------------------------------------------------------------------------------------------
	-- Fetch results.
	-- <Summary>
	-- Uses a more strict criterion based on the user's input.
	-- </Summary>
	--------------------------------------------------------------------------------------------	
	IF @EnforceApplicationSpecification = 1 OR @EnforceBusinessSpecification = 1
		OR @EnforceEntitySpecification = 1
	BEGIN

		;WITH cteItems AS (
			SELECT
				CASE 
					WHEN @SortField IN ('CommunicationMethodID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.CommunicationMethodID ASC) 
					WHEN @SortField IN ('CommunicationMethodID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.CommunicationMethodID DESC)     
					WHEN @SortField = 'Code' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Code ASC) 
					WHEN @SortField = 'Code' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Code DESC) 
					WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Name ASC) 
					WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Name DESC)
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY typ.DateCreated ASC) 
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY typ.DateCreated DESC)   
					ELSE ROW_NUMBER() OVER (ORDER BY typ.Name ASC)
				END AS RowNumber,
				cfg.CommunicationMethodConfigurationID,
				cfg.ApplicationID,
				app.Code AS ApplicationCode,
				app.Name AS ApplicationName,
				cfg.BusinessID,
				cfg.EntityID,
				cfg.CommunicationMethodID,
				typ.Code,
				typ.Name,
				typ.Description,
				cfg.rowguid,
				cfg.DateCreated,
				cfg.DateModified,
				cfg.CreatedBy,
				cfg.ModifiedBy
			--SELECT *
			FROM comm.CommunicationMethodConfiguration cfg
				JOIN comm.CommunicationMethod typ
					ON cfg.CommunicationMethodID = typ.CommunicationMethodID
				LEFT JOIN dbo.Application app
					ON cfg.ApplicationID = app.ApplicationID
			WHERE ( @CommunicationMethodConfigurationKey IS NULL OR cfg.CommunicationMethodConfigurationID IN (
						SELECT Value FROM dbo.fnSplit(@CommunicationMethodConfigurationKey, ',') WHERE ISNUMERIC(Value) =1) 
				)
				AND ( @EnforceApplicationSpecification = 0 OR cfg.ApplicationID = @ApplicationID )
				AND ( @EnforceBusinessSpecification = 0 OR cfg.BusinessID = @BusinessID )
				AND ( @EnforceEntitySpecification = 0 OR cfg.EntityID = @PersonID )
				AND (@CommunicationMethodKey IS NULL OR cfg.CommunicationMethodID IN (SELECT Value FROM dbo.fnSplit(@CommunicationMethodIdArray, ',') WHERE ISNUMERIC(Value) = 1))
		)

		SELECT
			CommunicationMethodConfigurationID,
			ApplicationID,
			ApplicationCode,
			ApplicationName,
			BusinessID,
			EntityID,
			CommunicationMethodID,
			Code,
			Name,
			Description,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy,
			(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
		FROM cteItems
		WHERE RowNumber > @Skip 
			AND RowNumber <= (@Skip + @Take)
		ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
			
	END
	
		
END
