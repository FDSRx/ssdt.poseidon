﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2014
-- Description:	Creates a new Patient Note object with an Interaction object (if provided).
-- Change log:
-- 6/9/2016 - dhughes - Modified the procedure to accept Application and Business objects.  

-- SAMPLE CALL: 
/*

DECLARE
	-- Api properties
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	-- Note properties
	@NoteID BIGINT = NULL OUTPUT,
	@ScopeKey VARCHAR(50) = NULL,
	@NoteTypeKey VARCHAR(50) = NULL,
	@NotebookKey VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX),
	@PriorityKey VARCHAR(50) = NULL,
	@TagKey VARCHAR(MAX) = NULL,
	@OriginKey VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData VARCHAR(MAX) = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	-- Interaction properties
	@InteractionID BIGINT = NULL OUTPUT,
	@InteractionTypeKey VARCHAR(25) = NULL,
	@DispositionTypeKey VARCHAR(25) = NULL,	
	@InitiatedByKey VARCHAR(50) = NULL,	
	@InitiatedByString VARCHAR(256) = NULL,	
	@InteractedWithKey VARCHAR(50) = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeKey VARCHAR(25) = NULL,
	-- Interaction Comment Note properties
	@InteractionCommentNoteID BIGINT = NULL OUTPUT,
	@InteractionCommentScopeKey INT = NULL,
	@InteractionCommentApplicationKey VARCHAR(50) = NULL,
	@InteractionCommentBusinessKey VARCHAR(50),
	@InteractionCommentBusinessKeyType VARCHAR(50) = NULL,
	--@InteractionCommentPatientKey VARCHAR(50) = NULL,
	--@InteractionCommentPatientKeyType VARCHAR(50) = NULL,
	@InteractionCommentNoteTypeKey VARCHAR(50) = NULL,
	@InteractionCommentNotebookKey VARCHAR(50) = NULL,
	@InteractionCommentTitle VARCHAR(1000) = NULL,
	@InteractionCommentMemo VARCHAR(MAX) = NULL,
	@InteractionCommentPriorityKey VARCHAR(50) = NULL,
	@InteractionCommentTagKey VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@InteractionCommentOriginKey VARCHAR(50) = NULL,
	@InteractionCommentOriginDataKey VARCHAR(256) = NULL,
	@InteractionCommentAdditionalData XML = NULL,
	@InteractionCommentCorrelationKey VARCHAR(256) = NULL,
	@InteractionCommentCreatedBy VARCHAR(256) = NULL,
	-- Comment Note properties
	@CommentNoteID BIGINT = NULL OUTPUT,
	@CommentScopeKey VARCHAR(50) = NULL,
	@CommentApplicationKey VARCHAR(50) = NULL,
	@CommentBusinessKey VARCHAR(50),
	@CommentBusinessKeyType VARCHAR(50) = NULL,
	--@CommentPatientKey VARCHAR(50) = NULL,
	--@CommentPatientKeyType VARCHAR(50) = NULL,
	@CommentNoteTypeKey VARCHAR(50) = NULL,
	@CommentNotebookKey  VARCHAR(50) = NULL,
	@CommentTitle VARCHAR(1000) = NULL,
	@CommentMemo VARCHAR(MAX) = NULL,
	@CommentPriorityKey VARCHAR(50) = NULL,
	@CommentTagKey VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@CommentOriginKey VARCHAR(50) = NULL,
	@CommentOriginDataKey VARCHAR(256) = NULL,
	@CommentAdditionalData XML = NULL,
	@CommentCorrelationKey VARCHAR(256) = NULL,
	@CommentCreatedBy VARCHAR(256) = NULL,
	-- Common properties
	@CreatedBy VARCHAR(256) = NULL,
	-- Output properties
	@NoteInteractionID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL

EXEC api.spPhrm_Patient_NoteWithOptionalInteraction_Create
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@NoteID = @NoteID OUTPUT,
	@ScopeKey = @ScopeKey,
	@NoteTypeKey = @NoteTypeKey,
	@NotebookKey = @NotebookKey,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityKey = @PriorityKey,
	@TagKey = @TagKey,
	@OriginKey = @OriginKey,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@InteractionID = @InteractionID OUTPUT,
	@InteractionTypeKey = @InteractionTypeKey,
	@DispositionTypeKey = @DispositionTypeKey,
	@InitiatedByKey = @InitiatedByKey,
	@InitiatedByString = @InitiatedByString,
	@InteractedWithKey = @InteractedWithKey,
	@InteractedWithString = @InteractedWithString,
	@ContactTypeKey = @ContactTypeKey,
	@InteractionCommentNoteID = @InteractionCommentNoteID OUTPUT,
	@InteractionCommentScopeKey = @InteractionCommentScopeKey,
	@InteractionCommentApplicationKey = @InteractionCommentApplicationKey,
	@InteractionCommentBusinessKey = @InteractionCommentBusinessKey,
	@InteractionCommentBusinessKeyType = @InteractionCommentBusinessKeyType,
	@InteractionCommentNoteTypeKey = @InteractionCommentNoteTypeKey,
	@InteractionCommentNotebookKey = @InteractionCommentNotebookKey,
	@InteractionCommentTitle = @InteractionCommentTitle,
	@InteractionCommentMemo = @InteractionCommentMemo,
	@InteractionCommentPriorityKey = @InteractionCommentPriorityKey,
	@InteractionCommentTagKey = @InteractionCommentTagKey,
	@InteractionCommentOriginKey = @InteractionCommentOriginKey,
	@InteractionCommentOriginDataKey = @InteractionCommentOriginDataKey,
	@InteractionCommentAdditionalData = @InteractionCommentAdditionalData,
	@InteractionCommentCorrelationKey = @InteractionCommentCorrelationKey,
	@InteractionCommentCreatedBy = @InteractionCommentCreatedBy,
	@CommentNoteID = @CommentNoteID OUTPUT,
	@CommentScopeKey = @CommentScopeKey,
	@CommentApplicationKey = @CommentApplicationKey,
	@CommentBusinessKey = @CommentBusinessKey,
	@CommentBusinessKeyType = @CommentBusinessKeyType,
	@CommentNoteTypeKey = @CommentNoteTypeKey,
	@CommentNotebookKey = @CommentNotebookKey,
	@CommentTitle = @CommentTitle,
	@CommentMemo = @CommentMemo,
	@CommentPriorityKey = @CommentPriorityKey,
	@CommentTagKey = @CommentTagKey,
	@CommentOriginKey = @CommentOriginKey,
	@CommentOriginDataKey = @CommentOriginDataKey,
	@CommentAdditionalData = @CommentAdditionalData,
	@CommentCorrelationKey = @CommentCorrelationKey,
	@CommentCreatedBy = @CommentCreatedBy,
	@CreatedBy = @CreatedBy,
	@NoteInteractionID = @NoteInteractionID OUTPUT,
	@Debug = @Debug


SELECT @NoteID AS NoteID, @InteractionID AS InteractionID, 
	@InteractionCommentNoteID AS InteractionCommentNoteID,
	@CommentNoteID AS CommentNoteID, @NoteInteractionID AS NoteInteractionID

*/

-- SELECT * FROM dbo.NoteInteraction
-- SELECT * FROM dbo.vwTask
-- SELECT * FROM dbo.Interaction
-- SELECT * FROM dbo.InteractionType
-- SELECT * FROM dbo.DispositionType
-- SELECT * FROM dbo.Origin
-- SELECT * FROM dbo.Notebook
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Priority
-- SELECT * FROM dbo.ContactType
-- SELECT * FROM dbo.Note

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_NoteWithOptionalInteraction_Create]
	-- Api properties
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	-- Note properties
	@NoteID BIGINT = NULL OUTPUT,
	@ScopeKey VARCHAR(50) = NULL,
	@NoteTypeKey VARCHAR(50) = NULL,
	@NotebookKey VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX),
	@PriorityKey VARCHAR(50) = NULL,
	@TagKey VARCHAR(MAX) = NULL,
	@OriginKey VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData VARCHAR(MAX) = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	-- Interaction properties
	@InteractionID BIGINT = NULL OUTPUT,
	@InteractionTypeKey VARCHAR(25) = NULL,
	@DispositionTypeKey VARCHAR(25) = NULL,	
	@InitiatedByKey VARCHAR(50) = NULL,	
	@InitiatedByString VARCHAR(256) = NULL,	
	@InteractedWithKey VARCHAR(50) = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeKey VARCHAR(25) = NULL,
	-- Interaction Comment Note properties
	@InteractionCommentNoteID BIGINT = NULL OUTPUT,
	@InteractionCommentScopeKey INT = NULL,
	@InteractionCommentApplicationKey VARCHAR(50) = NULL,
	@InteractionCommentBusinessKey VARCHAR(50) = NULL,
	@InteractionCommentBusinessKeyType VARCHAR(50) = NULL,
	--@InteractionCommentPatientKey VARCHAR(50) = NULL,
	--@InteractionCommentPatientKeyType VARCHAR(50) = NULL,
	@InteractionCommentNoteTypeKey VARCHAR(50) = NULL,
	@InteractionCommentNotebookKey VARCHAR(50) = NULL,
	@InteractionCommentTitle VARCHAR(1000) = NULL,
	@InteractionCommentMemo VARCHAR(MAX) = NULL,
	@InteractionCommentPriorityKey VARCHAR(50) = NULL,
	@InteractionCommentTagKey VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@InteractionCommentOriginKey VARCHAR(50) = NULL,
	@InteractionCommentOriginDataKey VARCHAR(256) = NULL,
	@InteractionCommentAdditionalData XML = NULL,
	@InteractionCommentCorrelationKey VARCHAR(256) = NULL,
	@InteractionCommentCreatedBy VARCHAR(256) = NULL,
	-- Comment Note properties
	@CommentNoteID BIGINT = NULL OUTPUT,
	@CommentScopeKey VARCHAR(50) = NULL,
	@CommentApplicationKey VARCHAR(50) = NULL,
	@CommentBusinessKey VARCHAR(50) = NULL,
	@CommentBusinessKeyType VARCHAR(50) = NULL,
	--@CommentPatientKey VARCHAR(50) = NULL,
	--@CommentPatientKeyType VARCHAR(50) = NULL,
	@CommentNoteTypeKey VARCHAR(50) = NULL,
	@CommentNotebookKey  VARCHAR(50) = NULL,
	@CommentTitle VARCHAR(1000) = NULL,
	@CommentMemo VARCHAR(MAX) = NULL,
	@CommentPriorityKey VARCHAR(50) = NULL,
	@CommentTagKey VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@CommentOriginKey VARCHAR(50) = NULL,
	@CommentOriginDataKey VARCHAR(256) = NULL,
	@CommentAdditionalData XML = NULL,
	@CommentCorrelationKey VARCHAR(256) = NULL,
	@CommentCreatedBy VARCHAR(256) = NULL,
	-- Common properties
	@CreatedBy VARCHAR(256) = NULL,
	-- Output properties
	@NoteInteractionID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' +
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ScopeKey=' + dbo.fnToStringOrEmpty(@ScopeKey) + ';' +
		'@NoteTypeKey=' + dbo.fnToStringOrEmpty(@NoteTypeKey) + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey) + ';' +
		'@TagKey=' + dbo.fnToStringOrEmpty(@TagKey) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@InteractionID=' + dbo.fnToStringOrEmpty(@InteractionID) + ';' +
		'@InteractionTypeKey=' + dbo.fnToStringOrEmpty(@InteractionTypeKey) + ';' +
		'@DispositionTypeKey=' + dbo.fnToStringOrEmpty(@DispositionTypeKey) + ';' +
		'@InitiatedByKey=' + dbo.fnToStringOrEmpty(@InitiatedByKey) + ';' +
		'@InitiatedByString=' + dbo.fnToStringOrEmpty(@InitiatedByString) + ';' +
		'@InteractedWithKey=' + dbo.fnToStringOrEmpty(@InteractedWithKey) + ';' +
		'@InteractedWithString=' + dbo.fnToStringOrEmpty(@InteractedWithString) + ';' +
		'@ContactTypeKey=' + dbo.fnToStringOrEmpty(@ContactTypeKey) + ';' +
		'@InteractionCommentNoteID=' + dbo.fnToStringOrEmpty(@InteractionCommentNoteID) + ';' +
		'@InteractionCommentScopeKey=' + dbo.fnToStringOrEmpty(@InteractionCommentScopeKey) + ';' +
		'@InteractionCommentApplicationKey=' + dbo.fnToStringOrEmpty(@InteractionCommentApplicationKey) + ';' +
		'@InteractionCommentBusinessKey=' + dbo.fnToStringOrEmpty(@InteractionCommentBusinessKey) + ';' +
		'@InteractionCommentBusinessKeyType=' + dbo.fnToStringOrEmpty(@InteractionCommentBusinessKeyType) + ';' +
		'@InteractionCommentNoteTypeKey=' + dbo.fnToStringOrEmpty(@InteractionCommentNoteTypeKey) + ';' +
		'@InteractionCommentNotebookKey=' + dbo.fnToStringOrEmpty(@InteractionCommentNotebookKey) + ';' +
		'@InteractionCommentTitle=' + dbo.fnToStringOrEmpty(@InteractionCommentTitle) + ';' +
		'@InteractionCommentMemo=' + dbo.fnToStringOrEmpty(@InteractionCommentMemo) + ';' +
		'@InteractionCommentPriorityKey=' + dbo.fnToStringOrEmpty(@InteractionCommentPriorityKey) + ';' +
		'@InteractionCommentTagKey=' + dbo.fnToStringOrEmpty(@InteractionCommentTagKey) + ';' +
		'@InteractionCommentOriginKey=' + dbo.fnToStringOrEmpty(@InteractionCommentOriginKey) + ';' +
		'@InteractionCommentOriginDataKey=' + dbo.fnToStringOrEmpty(@InteractionCommentOriginDataKey) + ';' +
		'@InteractionCommentAdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @InteractionCommentAdditionalData)) + ';' +
		'@InteractionCommentCorrelationKey=' + dbo.fnToStringOrEmpty(@InteractionCommentCorrelationKey) + ';' +
		'@InteractionCommentCreatedBy=' + dbo.fnToStringOrEmpty(@InteractionCommentCreatedBy) + ';' +
		'@CommentNoteID=' + dbo.fnToStringOrEmpty(@CommentNoteID) + ';' +
		'@CommentScopeKey=' + dbo.fnToStringOrEmpty(@CommentScopeKey) + ';' +
		'@CommentApplicationKey=' + dbo.fnToStringOrEmpty(@CommentApplicationKey) + ';' +
		'@CommentBusinessKey=' + dbo.fnToStringOrEmpty(@CommentBusinessKey) + ';' +
		'@CommentBusinessKeyType=' + dbo.fnToStringOrEmpty(@CommentBusinessKeyType) + ';' +
		'@CommentNoteTypeKey=' + dbo.fnToStringOrEmpty(@CommentNoteTypeKey) + ';' +
		'@CommentNotebookKey=' + dbo.fnToStringOrEmpty(@CommentNotebookKey) + ';' +
		'@CommentTitle=' + dbo.fnToStringOrEmpty(@CommentTitle) + ';' +
		'@CommentMemo=' + dbo.fnToStringOrEmpty(@CommentMemo) + ';' +
		'@CommentPriorityKey=' + dbo.fnToStringOrEmpty(@CommentPriorityKey) + ';' +
		'@CommentTagKey=' + dbo.fnToStringOrEmpty(@CommentTagKey) + ';' +
		'@CommentOriginKey=' + dbo.fnToStringOrEmpty(@CommentOriginKey) + ';' +
		'@CommentOriginDataKey=' + dbo.fnToStringOrEmpty(@CommentOriginDataKey) + ';' +
		'@CommentAdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @CommentAdditionalData)) + ';' +
		'@CommentCorrelationKey=' + dbo.fnToStringOrEmpty(@CommentCorrelationKey) + ';' +
		'@CommentCreatedBy=' + dbo.fnToStringOrEmpty(@CommentCreatedBy) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@NoteInteractionID=' + dbo.fnToStringOrEmpty(@NoteInteractionID) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = NULL,
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	
		
	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
	DECLARE
		@ScopeID_Internal INT = dbo.fnGetScopeID('INTRN'),
		@NoteTypeID_General INT = dbo.fnGetNoteTypeID('GNRL'),
		@PriorityID_None INT = dbo.fnGetPriorityID('NONE'),
		@OriginID_Unknown INT = dbo.fnGetOriginID('UNKN')

	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		-- Note properties
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType),
		@ScopeID INT = @ScopeID_Internal, -- only review internal notes
		@NoteTypeID INT = ISNULL(dbo.fnGetNoteTypeID(@NoteTypeKey), @NoteTypeID_General),
		@NotebookID INT = dbo.fnGetNotebookID(@NotebookKey),
		@PriorityID INT = ISNULL(dbo.fnGetPriorityID(@PriorityKey), @PriorityID_None),
		@OriginID INT = ISNULL(dbo.fnGetOriginID(@OriginKey), @OriginID_Unknown),
		-- Interaction properties
		@ContactTypeID INT = ISNULL(dbo.fnGetContactTypeID(@ContactTypeKey), dbo.fnGetContactTypeID('UNKN')),
		@InteractionTypeID INT = dbo.fnGetInteractionTypeID(@InteractionTypeKey),
		@DispositionTypeID INT = dbo.fnGetDispositionTypeID(@DispositionTypeKey),
		@InitiatedByID BIGINT = dbo.fnGetPersonID(@InitiatedByKey),
		@InteractedWithID BIGINT = dbo.fnGetPersonID(@InteractedWithKey),
		-- Interaction Comment Note properties
		@InteractionCommentApplicationID INT = dbo.fnGetApplicationID(@InteractionCommentApplicationKey),
		@InteractionCommentBusinessID BIGINT = dbo.fnBusinessIDTranslator(@InteractionCommentBusinessKey, @InteractionCommentBusinessKeyType),
		@InteractionCommentScopeID INT = @ScopeID_Internal, -- only review internal notes
		@InteractionCommentNoteTypeID INT = ISNULL(dbo.fnGetNoteTypeID(@InteractionCommentNoteTypeKey), @NoteTypeID_General),
		@InteractionCommentNotebookID INT = dbo.fnGetNotebookID(@InteractionCommentNotebookKey),
		@InteractionCommentPriorityID INT = ISNULL(dbo.fnGetPriorityID(@InteractionCommentPriorityKey), @PriorityID_None),
		@InteractionCommentOriginID INT = ISNULL(dbo.fnGetOriginID(@InteractionCommentOriginKey), @OriginID_Unknown),
		-- Comment note properties
		@CommentApplicationID INT = dbo.fnGetApplicationID(@CommentApplicationKey),
		@CommentBusinessID BIGINT = dbo.fnBusinessIDTranslator(@CommentBusinessKey, @CommentBusinessKeyType),
		@CommentScopeID INT = @ScopeID_Internal, -- only review internal notes
		@CommentNoteTypeID INT = ISNULL(dbo.fnGetNoteTypeID(@CommentNoteTypeKey), @NoteTypeID_General),
		@CommentNotebookID INT = dbo.fnGetNotebookID(@CommentNotebookKey),
		@CommentPriorityID INT = ISNULL(dbo.fnGetPriorityID(@CommentPriorityKey), @PriorityID_None),
		@CommentOriginID INT = ISNULL(dbo.fnGetOriginID(@CommentOriginKey), @OriginID_Unknown)
	
	------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');

	-- Set the interaction comment and the standard comment to the application and business if 
	-- no information was provided.
	SET @InteractionCommentApplicationID = ISNULL(@InteractionCommentApplicationID, @ApplicationID);
	SET @InteractionCommentBusinessID = ISNULL(@InteractionCommentBusinessID, @BusinessID);
	SET @CommentApplicationID = ISNULL(@CommentApplicationID, @ApplicationID);
	SET @CommentBusinessID = ISNULL(@CommentBusinessID, @BusinessID);

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod, 
			@ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @BusinessKey AS BusinessKey, 
			@BusinessKeyType AS BusinessKeyType, @PatientID AS PatientID, @PersonID AS PersonID,
			@Args AS Arguments
	END	

	------------------------------------------------------------------------------------------------------------
	-- Argument resolution exceptions.
	-- <Summary>
	-- Validates resolved values.
	-- </Summary>
	------------------------------------------------------------------------------------------------------------
	-- PersonID
	IF @PersonID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create a Note with Interaction object. Object reference (@PatientKey) was not able to be translated. ' +
			'A valid @PatientKey value is required.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	------------------------------------------------------------------------------------------------------------
	-- Create new patient Note and Interaction object (if provided).
	------------------------------------------------------------------------------------------------------------
	EXEC dbo.spNoteWithOptionalInteraction_Create
		-- Note properties.
		@ScopeID = @ScopeID,
		@ApplicationID = @ApplicationID,
		@BusinessID = @BusinessID,
		@BusinessEntityID = @PersonID, -- Translated patient key to its applicable person identifier.
		@NoteTypeID = @NoteTypeID,
		@NotebookID = @NotebookID,
		@PriorityID = @PriorityID,
		@Title = @Title,
		@Memo = @Memo,
		@Tags = @TagKey,
		@OriginID = @OriginID,
		@OriginDataKey = @OriginDataKey,
		@AdditionalData = @AdditionalData,
		@NoteID = @NoteID OUTPUT,
		-- Note Interaction properties.
		@NoteInteractionID = @NoteInteractionID OUTPUT,
		@InteractionID = @InteractionID OUTPUT,
		@InteractedWithID = @InteractedWithID,
		@InteractedWithString = @InteractedWithString,
		@ContactTypeID = @ContactTypeID,
		@InteractionTypeID = @InteractionTypeID,
		@DispositionTypeID = @DispositionTypeID,
		@InitiatedByID = @InitiatedByID,
		@InitiatedByString = @InitiatedByString,
		-- Interaction Comment Note
		@InteractionCommentNoteID = @InteractionCommentNoteID,
		@InteractionCommentApplicationID = @InteractionCommentApplicationID,
		@InteractionCommentBusinessID = @InteractionCommentBusinessID,
		@InteractionCommentScopeID = @InteractionCommentScopeID,
		@InteractionCommentBusinessEntityID = @PersonID,
		@InteractionCommentNoteTypeID = @InteractionCommentNoteTypeID,
		@InteractionCommentNotebookID = @InteractionCommentNotebookID,
		@InteractionCommentTitle = @InteractionCommentTitle,
		@InteractionCommentMemo = @InteractionCommentMemo,
		@InteractionCommentPriorityID = @InteractionCommentPriorityID,
		@InteractionCommentTags = @InteractionCommentTagKey, -- A comma separated list of tags that identify a note.
		@InteractionCommentOriginID = @InteractionCommentOriginID,
		@InteractionCommentOriginDataKey = @InteractionCommentOriginDataKey,
		@InteractionCommentAdditionalData = @InteractionCommentAdditionalData,
		@InteractionCommentCorrelationKey = @InteractionCommentCorrelationKey,
		@InteractionCommentCreatedBy = @InteractionCommentCreatedBy,
		-- Comment Note
		@CommentNoteID = @CommentNoteID,
		@CommentApplicationID = @CommentApplicationID,
		@CommentBusinessID = @CommentBusinessID,
		@CommentScopeID = @CommentScopeID,
		@CommentBusinessEntityID = @PersonID,
		@CommentNoteTypeID = @CommentNoteTypeID,
		@CommentNotebookID = @CommentNotebookID,
		@CommentTitle = @CommentTitle,
		@CommentMemo = @CommentMemo,
		@CommentPriorityID = @CommentPriorityID,
		@CommentTags = @CommentTagKey, -- A comma separated list of tags that identify a note.
		@CommentOriginID = @CommentOriginID,
		@CommentOriginDataKey = @CommentOriginDataKey,
		@CommentAdditionalData = @CommentAdditionalData,
		@CommentCorrelationKey = @CommentCorrelationKey,
		-- Common properties
		@CreatedBy = @CreatedBy
		
END
