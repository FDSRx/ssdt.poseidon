﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/14/2014
-- Description:	Returns a list of images.
-- SAMPLE CALL:
/*

EXEC api.spDbo_Image_Get_List;

EXEC api.spDbo_Image_Get_List
	@ApplicationKey = 2,
	@BusinessKey = 10684,
	@PersonKey = 135590

*/

-- SELECT * FROM dbo.Images
-- =============================================
CREATE PROCEDURE [api].[spDbo_Image_Get_List]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@ImageKey VARCHAR(MAX) = NULL,
	@KeyName VARCHAR(MAX) = NULL,
	@LanguageKey VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = dbo.fnGetBusinessID(@BusinessKey),
		@PersonID BIGINT = dbo.fnGetPersonID(@PersonKey),
		@LanguageID INT = ISNULL(dbo.fnGetLanguageID(@LanguageKey), dbo.fnGetLanguageDefaultID()),
		@DefaultLanguageID INT = dbo.fnGetLanguageDefaultID(),
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	-- Debug
	--SELECT @ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @PersonID AS PersonID, @LanguageID AS LanguageID, @DefaultLanguageID AS DefaultLanguageID
	
	
	-----------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'Name', 'FileName') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;
		
	-----------------------------------------------------------------------------------
	-- Temporary resources.
	-----------------------------------------------------------------------------------
	-- Image id listing.
	CREATE TABLE #tmpImageIDList (
		ImageID BIGINT
	);
	
	-- KeyName listing
	CREATE TABLE #tmpKeyNameList (
		KeyName VARCHAR(256)
	);
	
	-- Image temporary repository
	CREATE TABLE #tmpImageList (
		Idx BIGINT IDENTITY(1,1),
		ImageID BIGINT,
		ApplicationID INT,
		BusinessID BIGINT,
		PersonID BIGINT,
		KeyName VARCHAR(256),
		Name VARCHAR(256),
		Caption VARCHAR(4000),
		Description VARCHAR(MAX),
		FileName VARCHAR(256),
		FilePath VARCHAR(1000),
		FileDataString VARCHAR(MAX),
		FileDataBinary VARBINARY(MAX),
		LanguageID INT,
		rowguid UNIQUEIDENTIFIER,
		DateCreated DATETIME,
		DateModified DATETIME
	);
	
	-- Image temporary repository
	CREATE TABLE #tmpResultList (
		Idx BIGINT IDENTITY(1,1),
		ImageID BIGINT,
		ApplicationID INT,
		BusinessID BIGINT,
		PersonID BIGINT,
		KeyName VARCHAR(256),
		Name VARCHAR(256),
		Caption VARCHAR(4000),
		Description VARCHAR(MAX),
		FileName VARCHAR(256),
		FilePath VARCHAR(1000),
		FileDataString VARCHAR(MAX),
		FileDataBinary VARBINARY(MAX),
		LanguageID INT,
		rowguid UNIQUEIDENTIFIER,
		DateCreated DATETIME,
		DateModified DATETIME
	);

	-----------------------------------------------------------------------------------
	-- Parse lists.
	-----------------------------------------------------------------------------------
	DECLARE
		@ImageIDArrayLength INT = 0,
		@ImageKeyNameArrayLength INT = 0
		
	----------------------------
	-- Parse image ids
	----------------------------
	INSERT INTO #tmpImageIDList(
		ImageID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@ImageKey, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	SET @ImageIDArrayLength = @@ROWCOUNT;

	----------------------------
	-- Parse image key names
	----------------------------
	INSERT INTO #tmpKeyNameList(
		KeyName
	)
	SELECT
		Value
	FROM dbo.fnSplit(@KeyName, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
	
	SET @ImageKeyNameArrayLength = @@ROWCOUNT;

	-- Debug
	--SELECT @ImageIDArrayLength AS ImageIDArrayLength, @ImageKeyNameArrayLength AS ImageKeyNameArrayLength
	
	
	-----------------------------------------------------------------------------------
	-- Get image listing
	-- <Summary>
	-- Returns a listing of all applicable images.
	-- Further filtering will be required to retrieve the 
	-- globalized image (i.e. English, Spanish, etc.).
	-- <Summary>
	-----------------------------------------------------------------------------------
	INSERT INTO #tmpImageList (
		ImageID,
		ApplicationID,
		BusinessID,
		PersonID,
		KeyName,
		Name,
		Caption,
		Description,
		FileName,
		FilePath,
		FileDataString,
		FileDataBinary,
		LanguageID,
		rowguid,
		DateCreated,
		DateModified
	)
	SELECT
		img.ImageID,
		img.ApplicationID,
		img.BusinessID,
		img.PersonID,
		img.KeyName,
		img.Name,
		img.Caption,
		img.Description,
		img.FileName,
		img.FilePath,
		img.FileDataString,
		img.FileDataBinary,
		img.LanguageID,
		img.rowguid,
		img.DateCreated,
		img.DateModified
	FROM dbo.Images img (NOLOCK)
	WHERE ( ( @ApplicationKey IS NULL OR @ApplicationKey = '' ) OR img.ApplicationID = @ApplicationID )
		AND ( ( @BusinessKey IS NULL OR @BusinessKey = '' ) OR img.BusinessID = @BusinessID )
		AND ( ( @PersonKey IS NULL OR @PersonKey = '' ) OR img.PersonID = @PersonID )
		AND ( ( @ImageKey IS NULL OR @ImageKey = '' OR @ImageIDArrayLength = 0 ) 
				OR img.ImageID IN (SELECT ImageID FROM #tmpImageIDList) )
		AND ( ( @KeyName IS NULL OR @KeyName = '' OR @ImageKeyNameArrayLength = 0 ) 
				OR img.KeyName IN (SELECT KeyName FROM #tmpKeyNameList) )

	-- Debug
	--SELECT * FROM #tmpImageList

	-----------------------------------------------------------------------------------
	-- Globalize images
	-- <Summary>
	-- Selects the appropriate image culture from the list of
	-- images.
	-- <Summary>
	-----------------------------------------------------------------------------------
	-- Get user specified image and language.
	INSERT INTO #tmpResultList (
		ImageID,
		ApplicationID,
		BusinessID,
		PersonID,
		KeyName,
		Name,
		Caption,
		Description,
		FileName,
		FilePath,
		FileDataString,
		FileDataBinary,
		LanguageID,
		rowguid,
		DateCreated,
		DateModified
	)
	SELECT
		ImageID,
		ApplicationID,
		BusinessID,
		PersonID,
		KeyName,
		Name,
		Caption,
		Description,
		FileName,
		FilePath,
		FileDataString,
		FileDataBinary,
		LanguageID,
		rowguid,
		DateCreated,
		DateModified
	FROM #tmpImageList
	WHERE LanguageID = @LanguageID
	
	-- If we could not find the file, then we will just pull the default language file.
	IF @@ROWCOUNT = 0
	BEGIN
		-- Get user specified image and language.
		INSERT INTO #tmpResultList (
			ImageID,
			ApplicationID,
			BusinessID,
			PersonID,
			KeyName,
			Name,
			Caption,
			Description,
			FileName,
			FilePath,
			FileDataString,
			FileDataBinary,
			LanguageID,
			rowguid,
			DateCreated,
			DateModified
		)
		SELECT
			ImageID,
			ApplicationID,
			BusinessID,
			PersonID,
			KeyName,
			Name,
			Caption,
			Description,
			FileName,
			FilePath,
			FileDataString,
			FileDataBinary,
			LanguageID,
			rowguid,
			DateCreated,
			DateModified
		FROM #tmpImageList
		WHERE LanguageID = @DefaultLanguageID
	
	END	

	-----------------------------------------------------------------------------------
	-- Render results
	-- <Summary>
	-- Apply paging attribute (ROW_NUMER) and sorting mechnanism to result data.
	-- <Summary>
	-----------------------------------------------------------------------------------			
	;WITH cteImages AS (
		SELECT
			CASE    
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY DateCreated DESC)
				WHEN @SortField = 'Name' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY Name ASC) 
				WHEN @SortField = 'Name' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY Name DESC)  
				ELSE ROW_NUMBER() OVER (ORDER BY DateCreated DESC)
			END AS RowNumber,
			ImageID,
			ApplicationID,
			BusinessID,
			PersonID,
			KeyName,
			Name,
			Caption,
			Description,
			FileName,
			FilePath,
			FileDataString,
			FileDataBinary,
			LanguageID,
			rowguid,
			DateCreated,
			DateModified
		FROM #tmpImageList				
	)
	

	-----------------------------------------------------------------------------------
	-- Return image data.
	-----------------------------------------------------------------------------------	
	SELECT
		ImageID,
		ApplicationID,
		BusinessID,
		PersonID,
		KeyName,
		Name,
		Caption,
		Description,
		FileName,
		FilePath,
		FileDataString,
		FileDataBinary,
		LanguageID,
		rowguid,
		DateCreated,
		DateModified,
		(SELECT COUNT(*) FROM cteImages) AS TotalRecordCount
	FROM cteImages
	WHERE RowNumber > @Skip AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
	-----------------------------------------------------------------------------------
	-- Dispose of objects.
	-----------------------------------------------------------------------------------
	DROP TABLE #tmpImageIDList;
	DROP TABLE #tmpKeyNameList;
	DROP TABLE #tmpImageList;
	DROP TABLE #tmpResultList;
		
END
