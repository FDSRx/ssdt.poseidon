﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/6/2014
-- Description:	Returns a list of PrescriberType items.
-- SAMPLE CALL: api.spPhrm_PrescriberType_Get_List
-- =============================================
CREATE PROCEDURE [api].[spPhrm_PrescriberType_Get_List]
	@PrescriberTypeID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		PrescriberTypeID,
		Code,
		Name,
		Description,
		DateCreated
	FROM phrm.PrescriberType 
	WHERE ( @PrescriberTypeID IS NULL OR PrescriberTypeID IN (SELECT Value FROM dbo.fnSplit(@PrescriberTypeID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	
END
