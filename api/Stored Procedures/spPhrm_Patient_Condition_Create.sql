﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/8/2014
-- Description:	Creates a new patient Condition object.
-- SAMPLE CALL:
/*
DECLARE
	@ConditionID BIGINT

EXEC api.spPhrm_Patient_Condition_Create
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@Name = 'Rabbit Allergies',
	@CreatedBy = 'dhughes',
	@ConditionID = @ConditionID OUTPUT

SELECT @ConditionID AS ConditionID
*/
 
-- SELECT * FROM phrm.Condition
-- SELECT * FROM dbo.CodeQualifier
-- SELECT * FROM phrm.ConditionType
-- SELECT * FROM dbo.Origin
-- SELECT * From phrm.vwPatient WHERE LastName LIKE '%Simmons%'
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Condition_Create]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@ConditionTypeKey VARCHAR(25) = NULL,
	@CodeQualifierKey VARCHAR(25) = NULL,
	@Code VARCHAR(25) = NULL,
	@Name VARCHAR(512),
	@Description VARCHAR(1000) = NULL,
	@OriginKey VARCHAR(25) = NULL,
	@DateInactive DATETIME = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ConditionID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*
	-- Log incoming arguments
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	DECLARE @Args VARCHAR(MAX) =
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' +
		'@ConditionTypeKey=' + dbo.fnToStringOrEmpty(@ConditionTypeKey) + ';' +
		'@CodeQualifierKey=' + dbo.fnToStringOrEmpty(@CodeQualifierKey) + ';' +
		'@Code=' + dbo.fnToStringOrEmpty(@Code) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@Description=' + dbo.fnToStringOrEmpty(@Description) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		'@DateInactive=' + dbo.fnToStringOrEmpty(@DateInactive) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ConditionID=' + dbo.fnToStringOrEmpty(@ConditionID) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = NULL,
		@Arguments = @Args
	*/
	
	---------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------
	SET @ConditionID = NULL;
	
	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@ConditionTypeID INT = phrm.fnGetConditionTypeID(@ConditionTypeKey),
		@CodeQualifierID INT = dbo.fnGetCodeQualifierID(@CodeQualifierKey),
		@OriginID INT = dbo.fnGetOriginID(@OriginKey)
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');	

	
	---------------------------------------------------------------------------
	-- Create patient condition record.
	-- <Summary>
	-- Creates a new patient Condition object and returns the object's identity.
	-- </Summary>
	---------------------------------------------------------------------------
	EXEC phrm.spCondition_Create
		@BusinessEntityID = @PersonID,
		@ConditionTypeID = @ConditionTypeID,
		@CodeQualifierID = @CodeQualifierID,
		@Code = @Code,
		@Name = @Name,
		@Description = @Description,
		@OriginID = @OriginID,
		@DateInactive = @DateInactive,
		@CreatedBy = @CreatedBy,
		@ConditionID = @ConditionID OUTPUT
			
END
