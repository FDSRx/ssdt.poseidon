﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/10/2014
-- Description:	Returns a list of a persons conditions.
-- SAMPLE CALL:
/*
EXEC api.spPhrm_Condition_Get_List
	@BusinessEntityKey = '135590'
*/
-- SELECT * FROM phrm.Condition
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Condition_Get_List]
	@ConditionKey VARCHAR(MAX) = NULL,
	@BusinessEntityKey VARCHAR(MAX), -- NOTE: A single or comma delimited list of BusinessEntity keys.
	@ConditionTypeKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of ConditionTye object keys.
	@CodeQualifierKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of CodeQualifier object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of OriginKey object keys.
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	SET @ConditionTypeKey = phrm.fnConditionTypeKeyTranslator(@ConditionTypeKey, DEFAULT);
	SET @CodeQualifierKey = dbo.fnCodeQualifierKeyTranslator(@CodeQualifierKey, DEFAULT);
	SET @OriginKey = dbo.fnOriginKeyTranslator(@OriginKey, DEFAULT);
	
	--------------------------------------------------------------------------------------------
	-- Data paging.
	--------------------------------------------------------------------------------------------	
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows
	
	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'Name') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;
	
	--------------------------------------------------------------------------------------------
	-- Create result set
	--------------------------------------------------------------------------------------------
	;WITH cteConditions (
		RowNumber,
		ConditionID,
		ConditionTypeID,
		ConditionTypeCode,
		ConditionTypeName,
		CodeQualifierID,
		CodeQualifierCode,
		CodeQualifierName,
		Code,
		Name,
		Description,
		OriginID,
		OriginCode,
		OriginName,
		DateInactive
	) AS (
		SELECT
			CASE    
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY con.DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY con.DateCreated DESC)
				WHEN @SortField = 'Name' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY con.Name ASC) 
				WHEN @SortField = 'Name' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY con.Name DESC)  
				ELSE ROW_NUMBER() OVER (ORDER BY con.Name ASC)
			END AS RowNumber,	
			con.ConditionID,
			con.ConditionTypeID,
			typ.Code AS ConditionTypeCode,
			typ.Name AS ConditionTypeName,
			con.CodeQualifierID,
			cq.Code AS CodeQualifierID,
			cq.Name AS CodeQualifierName,
			con.Code,
			con.Name,
			con.Description,
			con.OriginID,
			org.Code AS OriginCode,
			org.Name AS OriginName,
			con.DateInactive
		FROM phrm.Condition con
			LEFT JOIN phrm.ConditionType typ
				ON con.ConditionTypeID = typ.ConditionTypeID
			LEFT JOIN dbo.CodeQualifier cq
				ON con.CodeQualifierID = cq.CodeQualifierID
			LEFT JOIN dbo.Origin org
				ON con.OriginID = org.OriginID
		WHERE BusinessEntityID IN (SELECT Value FROM dbo.fnSplit(@BusinessEntityKey, ',') WHERE ISNUMERIC(Value) = 1)
			AND ( @ConditionKey IS NULL OR con.ConditionID IN (SELECT Value FROM dbo.fnSplit(@ConditionKey, ',') WHERE ISNUMERIC(Value) = 1)) 
			AND ( @ConditionTypeKey IS NULL OR con.ConditionTypeID IN (SELECT Value FROM dbo.fnSplit(@ConditionTypeKey, ',') WHERE ISNUMERIC(Value) = 1)) 
			AND ( @CodeQualifierKey IS NULL OR con.CodeQualifierID IN (SELECT Value FROM dbo.fnSplit(@CodeQualifierKey, ',') WHERE ISNUMERIC(Value) = 1)) 
			AND ( @OriginKey IS NULL OR con.OriginID IN (SELECT Value FROM dbo.fnSplit(@OriginKey, ',') WHERE ISNUMERIC(Value) = 1)) 
	)
	
	SELECT
		RowNumber,
		ConditionID,
		ConditionTypeID,
		ConditionTypeCode,
		ConditionTypeName,
		CodeQualifierID,
		CodeQualifierCode,
		CodeQualifierName,
		Code,
		Name,
		Description,
		OriginID,
		OriginCode,
		OriginName,
		DateInactive,
		(SELECT COUNT(*) FROM cteConditions) AS TotalRecordCount
	FROM cteConditions
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
END
