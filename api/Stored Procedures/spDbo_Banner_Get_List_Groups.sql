﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/20/2016
-- Description:	Returns a list of banners (within their contained groups) that are applicable to the specified criteria.
/*

DECLARE
	@BannerID VARCHAR(MAX) = NULL,
	@BannerTypeKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonKeyType VARCHAR(50) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@GroupKey VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_Banner_Get_List_Groups
	@BannerID = @BannerID,
	@BannerTypeKey = @BannerTypeKey,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PersonKey = @PersonKey,
	@PersonKeyType = @PersonKeyType,
	@DateStart = @DateStart,
	@DateEnd = @DateEnd,
	@GroupKey = @GroupKey,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug


UPDATE trgt 
SET 
	trgt.DateEnd = DATEADD(dd, 5, GETDATE()),
	trgt.DateModified = GETDATE()
FROM dbo.BannerSchedule trgt WHERE BannerID = 6


*/


 --SELECT * FROM dbo.Banner
 --SELECT * FROM dbo.BannerSchedule
 --SELECT * FROM dbo.Application
 --SELECT * FROM dbo.BannerType

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spDbo_Banner_Get_List_Groups]
	@BannerID VARCHAR(MAX) = NULL,
	@BannerTypeKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonKeyType VARCHAR(50) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@GroupKey VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@BannerID=' + dbo.fnToStringOrEmpty(@BannerID)  + ';' +
		'@BannerTypeKey=' + dbo.fnToStringOrEmpty(@BannerTypeKey)  + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@PersonKey=' + dbo.fnToStringOrEmpty(@PersonKey)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @DateStart = ISNULL(@DateStart, CONVERT(DATE, '1/1/1900'));
	SET @DateEnd = ISNULL(@DateEnd, CONVERT(DATE, '12/31/9999'));
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL,
		@CredentialEntityID BIGINT = NULL,
		@PersonID BIGINT = NULL
	
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.

	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey ,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialKey = @PersonKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@IsOutputOnly = 1
	
	-- Determine PersonID
	SET @PersonID = @CredentialEntityID;

	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@BannerID AS BannerKey, @ApplicationKey AS ApplicationKey, @ApplicationID AS ApplicationID, 
			@BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@BusinessID AS BusinessID, @PersonID AS PersonID, @DateStart AS DateStart, @DateEnd AS DateEnd,
			@GroupKey AS GroupKey
	END

	--------------------------------------------------------------------------------------------
	-- Paging.
	--------------------------------------------------------------------------------------------		
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('BannerID', 'BannerTypeName', 'ApplicationName', 'BusinessName', 'Title', 'DateStart') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;	

	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @SortCollection AS SortCollection, @SortField AS SortField, @SortDirection AS SortDirection
	END



	--------------------------------------------------------------------------------------------
	-- Create result set
	--------------------------------------------------------------------------------------------
	;WITH cteBanners AS (
	
		SELECT
			CASE    
				WHEN @SortField = 'BannerID' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY b.BannerID ASC) 
				WHEN @SortField = 'BannerID' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY b.BannerID DESC)
				WHEN @SortField = 'BannerTypeName' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY typ.Name ASC) 
				WHEN @SortField = 'BannerTypeName' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY typ.Name DESC)  
				WHEN @SortField = 'ApplicationName' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY app.Name ASC) 
				WHEN @SortField = 'ApplicationName' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY app.Name DESC)
				WHEN @SortField = 'BusinessName' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY biz.Name ASC) 
				WHEN @SortField = 'BusinessName' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY biz.Name DESC)
				WHEN @SortField = 'Title' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY b.Name ASC) 
				WHEN @SortField = 'Title' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY b.Name DESC)
				WHEN @SortField = 'DateStart' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY schd.DateStart ASC)
				WHEN @SortField = 'DateStart' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY schd.DateStart DESC)   
				ELSE ROW_NUMBER() OVER (ORDER BY schd.DateCreated DESC)
			END AS RowNumber,
			b.BannerID,
			typ.BannerTypeID,
			typ.Code AS BannerTypeCode,
			typ.Name AS BannerTypeName,
			schd.ApplicationID,
			app.Code AS ApplicationCode,
			app.Name AS ApplicationName,
			schd.BusinessID,
			biz.Name AS BusinessName,
			schd.PersonID,
			b.Name AS Title,
			b.Header,
			b.Body,
			b.Footer,
			b.LanguageID,
			lang.Code AS LanguageCode,
			lang.Name AS LanguageName,
			schd.DateStart,
			schd.DateEnd,
			schd.SortOrder,
			b.DateCreated,
			b.DateModified,
			b.CreatedBy,
			b.ModifiedBy
		--SELECT *
		FROM dbo.Banner b (NOLOCK)
			JOIN dbo.BannerSchedule schd (NOLOCK)
				ON schd.BannerID = b.BannerID
			JOIN dbo.BannerType typ (NOLOCK)
				ON b.BannerTypeID = typ.BannerTypeID
			JOIN dbo.Language lang (NOLOCK)
				ON b.LanguageID = lang.LanguageID
			LEFT JOIN dbo.Application app
				ON app.ApplicationID = schd.ApplicationID
			LEFT JOIN dbo.Business biz
				ON biz.BusinessEntityID = schd.BusinessID
		WHERE ( (
			@GroupKey IN ('GLOBAL', 'GBL', 'GLB')
				AND schd.ApplicationID IS NULL
				AND schd.BusinessID IS NULL
				AND schd.PersonID IS NULL
			)
			OR (
			@GroupKey IN ('APPLICATION', 'APP')
				AND schd.ApplicationID IS NOT NULL
				AND schd.BusinessID IS NULL
				AND schd.PersonID IS NULL
			)
			OR (
			@GroupKey IN ('BUSINESS', 'BIZ')
				--AND schd.ApplicationID IS NULL
				AND schd.BusinessID IS NOT NULL
				--AND schd.PersonID IS NULL
			))
			AND schd.DateStart >= @DateStart
			AND schd.DateEnd < @DateEnd
							
	)
	
	SELECT 
		BannerID,
		BannerTypeID,
		BannerTypeCode,
		BannerTypeName,
		ApplicationID,
		ApplicationCode,
		ApplicationName,
		BusinessID,
		BusinessName,
		PersonID,
		Title,
		Header,
		Body,
		Footer,
		LanguageID,
		LanguageCode,
		LanguageName,
		DateStart,
		DateEnd,
		SortOrder,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteBanners) AS TotalRecordCount
	FROM cteBanners
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
			
END