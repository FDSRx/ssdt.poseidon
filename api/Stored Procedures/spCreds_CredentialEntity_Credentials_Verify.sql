﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/27/2014
-- Description:	Inspects the credential and validates accordingly. No actual login/authentication is performed.

-- SAMPLE CALL:
/*

DECLARE 
	@IsValid BIT = 0,
	@CredentialEntityID BIGINT = NULL,
	@CredentialToken VARCHAR(50) = NULL

EXEC api.spCreds_CredentialEntity_Credentials_Verify
	@ApplicationKey = 'ESMT',
	@BusinessKey = '7871787',
	@Username = 'dhughes',
	@Password = 'password',	
	@CredentialToken = @CredentialToken OUTPUT,
	@CredentialEntityTypeKey = 1,
	@Role = 'ADMIN',
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@IsValid = @IsValid OUTPUT

SELECT @IsValid AS IsValid, @CredentialEntityID AS CredentialEntityID, @CredentialToken AS CredentialToken

*/

-- SELECT * FROM dbo.vwStore WHERE BusinessEntityID = 10684
-- SELECT * FROM acl.vwCredentialEntityRole WHERE Username = 'dhughes'
-- SELECT * FROM dbo.Application
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialEntity_Credentials_Verify]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	@Username VARCHAR(256) = NULL,
	@Password VARCHAR(50) = NULL,
	@CredentialToken VARCHAR(50) = NULL OUTPUT,
	@CredentialEntityTypeKey VARCHAR(25) = NULL,
	@Role VARCHAR(MAX) = NULL,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL OUTPUT,
	@BusinessID BIGINT = NULL OUTPUT,
	@IsValid BIT = 0 OUTPUT,
	-- Core membership properties ****************************
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT,
	-- *******************************************************
	@IsChangePasswordRequired BIT = NULL OUTPUT,
	@IsFirstLogin BIT = NULL OUTPUT,
	@IsOutputOnly BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@Password=' + dbo.fnToStringOrEmpty(@Password) + ';' +
		'@CredentialToken=' + dbo.fnToStringOrEmpty(@CredentialToken) + ';' +
		'@CredentialEntityTypeKey=' + dbo.fnToStringOrEmpty(@CredentialEntityTypeKey) + ';' +
		'@Role=' + dbo.fnToStringOrEmpty(@Role) + ';' 

	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	---------------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------------
	SET @IsValid = 0;
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, NULLIF(@BusinessKeyType, ''));
	SET @ApplicationID = NULL;
	SET @BusinessID = NULL;
	SET @IsApproved = 0;
	SET	@IsLockedOut = 0;
	SET	@DateLastLoggedIn = NULL;
	SET	@DateLastPasswordChanged = NULL;
	SET @DatePasswordExpires = NULL;
	SET	@DateLastLockedOut = NULL;
	SET @DateLockOutExpires = NULL;
	SET	@FailedPasswordAttemptCount = 0;
	SET	@FailedPasswordAnswerAttemptCount = 0;
	SET @IsDisabled = 0;	
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);	
	
	
	---------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------	
	DECLARE 
		@CredentialEntityTypeID INT = creds.fnGetCredentialEntityTypeID(@CredentialEntityTypeKey)


	---------------------------------------------------------------------------
	-- CredentialEntityType Validation
	---------------------------------------------------------------------------	
	IF @CredentialEntityTypeID IS NULL
	BEGIN
		SELECT
			@IsValid AS IsValid,
			@CredentialEntityID AS CredentialEntityID,
			@CredentialToken AS CredentialToken	
		
		RETURN;
	END
	
	
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialTypeKey = @CredentialEntityTypeKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityTypeID = @CredentialEntityTypeID OUTPUT,
		@IsOutputOnly = 1
	
	-- Debug
	--SELECT @ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @Username AS Username, @Password AS Password, @CredentialEntityTypeID AS CredentialEntityTypeID
	
	
	---------------------------------------------------------------------------
	-- Verify credentials
	-- <Summary>
	-- Executes the core verification stored procedure with the translated
	-- keys.
	-- </Summary>
	---------------------------------------------------------------------------
	EXEC creds.spCredentialEntity_Credentials_Verify
		@ApplicationID = @ApplicationID,
		@BusinessID = @BusinessID,
		@Username = @Username,
		@Password = @Password,
		@CredentialEntityTypeID = @CredentialEntityTypeID,
		@CredentialToken = @CredentialToken OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@IsValid = @IsValid OUTPUT,
		-- Core membership properties ****************************
		@IsApproved = @IsApproved OUTPUT,
		@IsLockedOut = @IsLockedOut OUTPUT,
		@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
		@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
		@DatePasswordExpires = @DatePasswordExpires OUTPUT,
		@DateLastLockedOut = @DateLastLockedOut OUTPUT,
		@DateLockOutExpires = @DateLockOutExpires OUTPUT,		
		@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
		@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
		@IsDisabled = @IsDisabled OUTPUT,
		-- *******************************************************
		@IsChangePasswordRequired = @IsChangePasswordRequired OUTPUT,
		@IsFirstLogin = @IsFirstLogin OUTPUT
	
	
	---------------------------------------------------------------------------
	-- Validate provided roles.
	-- <Summary>
	-- If a role was provided for validation, then we need to verify the user
	-- has the provided role.
	-- </Summary>
	---------------------------------------------------------------------------	
	IF @CredentialEntityID IS NOT NULL AND dbo.fnIsNullOrWhiteSpace(@Role) = 0
	BEGIN
	
		DECLARE @IsInRole BIT = 0;
		
		EXEC acl.spCredentialEntity_IsInRole
			@CredentialEntityID = @CredentialEntityID,
			@Role = @Role,
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessID,
			@Exists = @IsInRole OUTPUT
		
		-- If the role was not found, then the invalidate the data.
		IF @IsInRole = 0
		BEGIN
			SET @IsValid = 0;
			SET @CredentialEntityID = NULL;
			SET @CredentialToken = NULL;
		END
	
	END
	
	
	---------------------------------------------------------------------------
	-- Response data.
	-- <Summary>
	-- Returns a set of credential items in the response.
	-- </Summary>
	---------------------------------------------------------------------------
	IF @IsOutputOnly = 0
	BEGIN
	
		SELECT
			@IsValid AS IsValid,
			@CredentialEntityID AS CredentialEntityID,
			@CredentialToken AS CredentialToken,
			@ApplicationID AS ApplicationID,
			@BusinessID AS BusinessID,
			-- Core membership properties *******************************************
			@IsApproved AS IsApproved,
			@IsLockedOut AS IsLockedOut,
			@DateLastLoggedIn AS DateLastLoggedIn,
			@DateLastPasswordChanged AS DateLastPasswordChanged,
			@DatePasswordExpires AS DatePasswordExpires,
			@DateLastLockedOut AS DateLastLocked,
			@DateLockOutExpires AS DateLockOutExpires,
			@FailedPasswordAttemptCount AS FailedPasswordAttemptCount,
			@FailedPasswordAnswerAttemptCount AS FailedPassswordAnswerAttemptCount,
			@IsDisabled AS IsDisabled,
			-- *******************************************************
			@IsChangePasswordRequired AS IsChangePasswordRequired,
			@IsFirstLogin AS IsFirstLogin
		
	END	
	
	
	
	
	
	
	
END
