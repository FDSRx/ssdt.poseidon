﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/1/2079
-- Description:	Returns the messaging configuration option for the provided criteria.
-- SAMPLE CALL:
/*

DECLARE
	@CommunicationConfigurationID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@MessageTypeID INT = NULL,
	@MessageTypeKey VARCHAR(50) = NULL,
	@CommunicationMethodID INT = NULL,
	@CommunicationMethodKey VARCHAR(50) = NULL,
	@MessagesPerMinutes INT = NULL,
	@MessagesNumberOfMinutes INT = NULL,
	@MessagesPerDays INT = NULL,
	@MessagesNumberOfDays INT = NULL,
	@IsDisabled BIT = NULL,
	@IsOutputOnly BIT = 0
	
EXEC api.spComm_CommunicationConfiguration_Get
	@CommunicationConfigurationID = @CommunicationConfigurationID,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@MessageTypeKey = @MessageTypeKey,
	@CommunicationMethodKey = @CommunicationMethodKey,
	@MessagesPerMinutes = @MessagesPerMinutes,
	@MessagesNumberOfMinutes = @MessagesNumberOfMinutes,
	@MessagesPerDays = @MessagesPerDays,
	@MessagesNumberOfDays = @MessagesNumberOfDays,
	@IsDisabled = @IsDisabled,
	@IsOutputOnly = @IsOutputOnly

SELECT 
	@CommunicationConfigurationID AS CommunicationConfigurationID,
	@ApplicationKey AS ApplicationKey,
	@BusinessKey AS BusinessKey,
	@BusinessKeyType AS BusinessKeyType,
	@MessageTypeKey AS MessageTypeKey,
	@CommunicationMethodKey AS CommunicationMethodKey,
	@MessagesPerMinutes AS MessagesPerMinutes,
	@MessagesNumberOfMinutes AS MessagesNumberOfMinutes,
	@MessagesPerDays AS MessagesPerDays,
	@MessagesNumberOfDays AS MessagesNumberOfDays,
	@IsDisabled AS IsDisabled
	
*/

-- SELECT * FROM comm.CommunicationConfiguration
-- =============================================
CREATE PROCEDURE [api].[spComm_CommunicationConfiguration_Get]
	@CommunicationConfigurationID BIGINT = NULL OUTPUT,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@EntityKey VARCHAR(50) = NULL,
	@EntityKeyType VARCHAR(50) = NULL,
	@MessageTypeKey VARCHAR(50) = NULL,
	@CommunicationMethodKey VARCHAR(50) = NULL,
	@MessagesPerMinutes INT = NULL OUTPUT,
	@MessagesNumberOfMinutes INT = NULL OUTPUT,
	@MessagesPerDays INT = NULL OUTPUT,
	@MessagesNumberOfDays INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT,
	@IsOutputOnly BIT = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------
	-- Instance variables
	-------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationConfigurationID=' + dbo.fnToStringOrEmpty(@CommunicationConfigurationID) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@EntityKey=' + dbo.fnToStringOrEmpty(@EntityKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@MessageTypeKey=' + dbo.fnToStringOrEmpty(@MessageTypeKey) + ';' +
		'@CommunicationMethodKey=' + dbo.fnToStringOrEmpty(@CommunicationMethodKey) + ';' +
		'@IsOutputOnly=' + dbo.fnToStringOrEmpty(@IsOutputOnly) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' 
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
		
	-------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	SET @Debug = ISNULL(@Debug, 0);
	--SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP')

	-------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------	
	DECLARE
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL,
		@PersonID BIGINT = NULL,
		@MessageTypeID INT = NULL,
		@CommunicationMethodID INT = NULL

	-------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PersonKey = @EntityKey,
		@PersonTypeKey = @EntityKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1
		
	SET	@MessageTypeID = comm.fnGetMessageTypeID(@MessageTypeKey);
	SET	@CommunicationMethodID = comm.fnGetCommunicationMethodID(@CommunicationMethodKey);

	IF @Debug = 1
	BEGIN
		SELECT @ApplicationID AS AppliationID, @BusinessID AS BusinessID, @MessageTypeID AS MessageTypeID,
			@CommunicationConfigurationID AS CommunicationMethodID
	END

	-------------------------------------------------------------------------------------------
	-- Retrieve data.
	-------------------------------------------------------------------------------------------
	EXEC comm.spCommunicationConfiguration_Get 
		@CommunicationConfigurationID = @CommunicationConfigurationID,
		@ApplicationID = @ApplicationID,
		@BusinessID = @BusinessID,
		@EntityID = @PersonID,
		@MessageTypeID = @MessageTypeID,
		@CommunicationMethodID = @CommunicationMethodID,
		@MessagesPerMinutes = @MessagesPerMinutes OUTPUT,
		@MessagesNumberOfMinutes = @MessagesNumberOfMinutes OUTPUT,
		@MessagesPerDays = @MessagesPerDays OUTPUT,
		@MessagesNumberOfDays = @MessagesNumberOfDays OUTPUT,
		@IsDisabled = @IsDisabled OUTPUT

	-------------------------------------------------------------------------------------------
	-- Return data.
	-- <Summary>
	-- Returns the data as a single row result set; otherwise, if the "ISOutputOnly"
	-- flag is set, the data will be returned via the output parameters.
	-- </Summary>
	-------------------------------------------------------------------------------------------
	IF ISNULL(@IsOutputOnly, 0) = 0
	BEGIN
		SELECT 
			@CommunicationConfigurationID AS CommunicationConfigurationID,
			@ApplicationID AS ApplicationID,
			@BusinessID AS BusinessID,
			@MessageTypeID AS MessageTypeID,
			@CommunicationMethodID AS CommunicationMethodID,
			@MessagesPerMinutes AS MessagesPerMinutes,
			@MessagesNumberOfMinutes AS MessagesNumberOfMinutes,
			@MessagesPerDays AS MessagesPerDays,
			@MessagesNumberOfDays AS MessagesNumberOfDays,
			@IsDisabled AS IsDisabled	
	END
	
	


END
