﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/8/2013
-- Description:	API wrapper around the credential role get method.
-- SAMPLE CALL:
/*
-- Result set
EXEC api.spCreds_CredentialEntityRole_Get_List
	@CredentialToken = 'D1D88CF0-5CDE-4410-81CE-EBC92A0D0636'
	
-- XML result set
EXEC api.spCreds_CredentialEntityRole_Get_List
	@CredentialToken = 'D1D88CF0-5CDE-4410-81CE-EBC92A0D0636',
	@IsResultXml = 1
	
*/
-- SELECT * FROM creds.CredentialToken
-- EXEC creds.spCredentialToken_Update @Token = 'D1D88CF0-5CDE-4410-81CE-EBC92A0D0636', @ForceUpdate = 1
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialEntityRole_Get_List]
	@ApplicationKey VARCHAR(25) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessToken VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@CredentialKey VARCHAR(50) = NULL,
	@CredentialEntityID BIGINT = NULL,
	@CredentialToken VARCHAR(50) = NULL,
	@IsResultXml BIT = 0,
	@IsResultXmlOutputOnly BIT = 0,
	@RoleData XML = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	--Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessToken=' + dbo.fnToStringOrEmpty(@BusinessToken) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@CredentialKey=' + dbo.fnToStringOrEmpty(@CredentialKey) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@CredentialToken=' + dbo.fnToStringOrEmpty(@CredentialToken) + ';' +
		'@IsResultXml=' + dbo.fnToStringOrEmpty(@IsResultXml) + ';'
	
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	--------------------------------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------------------------------
	SET @IsResultXml = ISNULL(@IsResultXml, 0);
	SET @IsResultXmlOutputOnly = ISNULL(@IsResultXmlOutputOnly, 0);
	SET @RoleData = NULL;

	--------------------------------------------------------------------------------------------
	-- Local Variables
	--------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = dbo.fnGetBusinessID(@BusinessToken),
		@ErrorMessage VARCHAR(4000)
	
	
	--------------------------------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF @CredentialEntityID IS NULL 
		AND dbo.fnIsNullOrWhiteSpace(@CredentialKey) = 1
		AND dbo.fnIsNullOrWhiteSpace(@CredentialToken) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to retrieve credential role(s).  Object reference are not set to an instance of an object. ' +
			'A credential token or identifier cannot be null or empty.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END

	IF @BusinessID IS NULL
	BEGIN
		--------------------------------------------------------------------------------------------
		-- Interogate business key and address accordingly.
		-- <Summary>
		-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
		-- business key type.  If a key type was not supplied then attempt a trial and error conversion
		-- based on token or NABP.
		-- </Summary>
		--------------------------------------------------------------------------------------------	
		-- Auto translater
		SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);	
		-- Busienss token
		SET @BusinessID = CASE WHEN @BusinessID IS NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'BusinessToken') ELSE @BusinessID END;
		-- NABP
		SET @BusinessID = CASE WHEN @BusinessID IS NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'NABP') ELSE @BusinessID END;
		-- Determine if it is a special scenario (i.e. all stores, etc.)
		SET @BusinessID = 
			CASE 
				WHEN @BusinessID IS NULL 
					AND ( ISNUMERIC(@BusinessKey) = 1 AND CONVERT(BIGINT, @BusinessKey) < 0) 
						THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'BusinessID') 
				ELSE @BusinessID 
			END;
	END

	--------------------------------------------------------------------------------------------
	-- CredentialEntityID Parsing (if applicable)
	--------------------------------------------------------------------------------------------
	-- If a CredentialEntityID was not supplied, then perform logic based on a token.
	-- If a token is not provided then let's see if some key in general was supplied.
	IF @CredentialEntityID IS NULL
	BEGIN
	
		IF dbo.fnIsNullOrWhiteSpace(@CredentialToken) = 0
		BEGIN
			--------------------------------------------------------------------------------------------
			-- Validate token
			--------------------------------------------------------------------------------------------	
			EXEC creds.spCredentialToken_Update	
				@Token = @CredentialToken OUTPUT,
				@CredentialEntityID = @CredentialEntityID OUTPUT,
				@ApplicationID = @ApplicationID OUTPUT,
				@BusinessEntityID = @BusinessID OUTPUT
				
		END
		ELSE
		BEGIN
			--------------------------------------------------------------------------------------------
			-- Interrogate key.
			-- <Summary>
			-- Inspect the key to determine if it is a unique identifier
			-- (authentication token) or if an actual CredentialEntityID has
			-- been supplied.
			-- </Summary>
			--------------------------------------------------------------------------------------------	
			IF dbo.fnIsNullOrWhiteSpace(@CredentialKey) = 0
			BEGIN
			
				-- If the key is a unique identifier then set as the CredentialToken
				-- and attempt an update on said token.
				IF dbo.fnIsUniqueIdentifier(@CredentialKey) = 1
				BEGIN
					SET @CredentialToken = @CredentialKey;
					
					--------------------------------------------------------------------------------------------
					-- Validate token
					--------------------------------------------------------------------------------------------	
					EXEC creds.spCredentialToken_Update	
						@Token = @CredentialToken OUTPUT,
						@CredentialEntityID = @CredentialEntityID OUTPUT,
						@ApplicationID = @ApplicationID OUTPUT,
						@BusinessEntityID = @BusinessID OUTPUT
					
				END
				
				-- If a CredentialEntityID or CredentialToken was not resolved
				-- and the key is numeric, then assume it is a CredentialEntityID.
				IF ( @CredentialEntityID IS NULL OR @CredentialToken IS NULL ) 
					AND ISNUMERIC(@CredentialKey) = 1
				BEGIN
					SET @CredentialEntityID = @CredentialKey;
				END
			
			END
		
		
		END
		
	END
								
	
	--------------------------------------------------------------------------------------------
	-- Fetch list of applicable roles
	-- <Summary>
	-- Obtains a list of applicable RoleIDs from both a 
	-- credentials groups and roles listing
	-- </Summary>	
	-- <Remarks>
	-- Utilizes the core get roles call to resolve
	-- roles.  Allows logic to remain in a standarized location
	-- </Remarks>
	--------------------------------------------------------------------------------------------
	EXEC acl.spCredentialEntityRole_Get_List
		@CredentialEntityID = @CredentialEntityID,
		@ApplicationID = @ApplicationID,
		@BusinessEntityID = @BusinessID,
		@IsResultXml = @IsResultXml,
		@IsResultXmlOutputOnly = @IsResultXmlOutputOnly,
		@RoleData = @RoleData OUTPUT
		
		
END
