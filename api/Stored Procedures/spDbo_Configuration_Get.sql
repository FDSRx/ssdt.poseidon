﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/9/2014
-- Description:	Gets a configuration value based on the specified parameters.
-- SAMPLE CALL:
/*

DECLARE
	@ConfigurationKey VARCHAR(50) = NULL,
	@ApplicationKey VARCHAR(50) = 'ENG',
	@BusinessKey VARCHAR(50) = '1715640',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@ConfigurationID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@KeyName VARCHAR(256) = 'IsMedSyncBinVerificationQueueEnabled',
	@KeyValue VARCHAR(MAX) = NULL,
	@DataTypeID INT = NULL,
	@TypeOf VARCHAR(256) = NULL,
	@Description VARCHAR(1000) = NULL,
	@IsOutputOnly BIT = 1,
	@Debug BIT = 1
	
EXEC api.spDbo_Configuration_Get
	@ConfigurationKey = @ConfigurationKey,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@ConfigurationID = @ConfigurationID,
	@ApplicationID = @ApplicationID,
	@BusinessEntityID = @BusinessEntityID,
	@KeyName = @KeyName,
	@KeyValue = @KeyValue OUTPUT,
	@DataTypeID = @DataTypeID,
	@TypeOf = @TypeOf,
	@Description = @Description,
	@IsOutputOnly = @IsOutputOnly,
	@Debug = @Debug


SELECT @KeyValue AS KeyValue

*/

-- SELECT * FROM dbo.Configuration
-- SELECT * FROM dbo.DataType
-- =============================================
CREATE PROCEDURE [api].[spDbo_Configuration_Get]
	@ConfigurationKey VARCHAR(50) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ConfigurationID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@KeyName VARCHAR(256),
	@KeyValue VARCHAR(MAX) = NULL OUTPUT,
	@DataTypeID INT = NULL OUTPUT,
	@TypeOf VARCHAR(256) = NULL OUTPUT,
	@Description VARCHAR(1000) = NULL OUTPUT,
	@IsOutputOnly BIT = NULL,
	@Debug BIT = NULL
AS
BEGIN


	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ConfigurationKey=' + dbo.fnToStringOrEmpty(@ConfigurationKey) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@ConfigurationID=' + dbo.fnToStringOrEmpty(@ConfigurationID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@KeyName=' + dbo.fnToStringOrEmpty(@KeyName) + ';' +
		'@KeyValue=' + dbo.fnToStringOrEmpty(@KeyValue) + ';' +
		'@DataTypeID=' + dbo.fnToStringOrEmpty(@DataTypeID) + ';' +
		'@TypeOf=' + dbo.fnToStringOrEmpty(@TypeOf) + ';' +
		'@Description=' + dbo.fnToStringOrEmpty(@Description) + ';' +
		'@IsOutputOnly=' + dbo.fnToStringOrEmpty(@IsOutputOnly) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;


	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @ConfigurationID = NULL;
	SET @ApplicationID = NULL;
	SET @BusinessEntityID = NULL;
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	SET @KeyValue = NULL;
	
	
	------------------------------------------------------------------------------------------------------------
	-- Translate keys.
	------------------------------------------------------------------------------------------------------------
	SET @ConfigurationID =
		CASE
			WHEN dbo.fnIsUniqueIdentifier(@ConfigurationKey) = 1 THEN (
				SELECT TOP 1 ConfigurationID
				FROM dbo.Configuration
				WHERE rowguid = @ConfigurationKey
			)
			ELSE @ConfigurationID
		END;
	
	-- Translate basic system keys.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessEntityID OUTPUT,
		@IsOutputOnly = 1

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debug Mode On' AS DebuggerMode, @ProcedureName AS ProcedureName, 'Get/Set variables.' AS ActionMethod,
			@ConfigurationKey AS ConfigurationKey, @ApplicationID AS ApplicationID, @BusinessEntityID AS BusinessEntityID, 
			@KeyName AS KeyName
	END

	------------------------------------------------------------------------------------------------------------
	-- Retrieve configuration value.
	------------------------------------------------------------------------------------------------------------	
	EXEC dbo.spConfiguration_Get
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessEntityID = @BusinessEntityID OUTPUT,
		@KeyName = @KeyName,
		@KeyValue = @KeyValue OUTPUT,
		@DataTypeID = @DataTypeID OUTPUT,
		@TypeOf = @TypeOf OUTPUT,
		@Description = @Description OUTPUT,
		@Debug = @Debug
	
	------------------------------------------------------------------------------------------------------------
	-- Return result set unless specified otherwise.
	------------------------------------------------------------------------------------------------------------
	IF @IsOutputOnly = 0
	BEGIN
		SELECT
			@ApplicationID AS ApplicationID,
			@BusinessEntityID AS BusinessEntityID,
			@KeyName AS KeyName,
			@KeyValue AS KeyValue,
			@DataTypeID AS DataTypeID,
			@TypeOf AS TypeOf,
			@Description AS Description
	END	
			
END
