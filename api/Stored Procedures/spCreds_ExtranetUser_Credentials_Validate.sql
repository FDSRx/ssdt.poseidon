﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/31/2013
-- Description:	Validates the credentials that are necessary to be considered an
--		authenticated extranet user.

-- SAMPLE CALL:
/*

DECLARE
	@ApplicationKey VARCHAR(25) = 'ISMT',
	@BusinessKey VARCHAR(50) = '7878799', -- BusinessToken or NABP supported
	@BusinessKeyType VARCHAR(50) = NULL,
	@Username VARCHAR(256) = 'dhughes',
	@Password VARCHAR(50) = 'password',
	@CredentialEntityTypeKey VARCHAR(25) = NULL,
	@IpAddress VARCHAR(256) = NULL,
	@BrowserName VARCHAR(256) = NULL,
	@BrowserVersion VARCHAR(50) = NULL,
	@DeviceName VARCHAR(50) = NULL,
	@IsMobile BIT = NULL,
	@UserAgent VARCHAR(500) = NULL,
	@Comments VARCHAR(MAX) = NULL,
	@CredentialToken VARCHAR(50) = NULL,
	@CredentialEntityID BIGINT = NULL,
	@IsValid BIT = 0,
	-- Core membership properties ****************************
	@IsApproved BIT = NULL,
	@IsLockedOut BIT = NULL,
	@DateLastLoggedIn DATETIME = NULL,
	@DateLastPasswordChanged DATETIME = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@DateLastLockedOut DATETIME = NULL,
	@DateLockOutExpires DATETIME = NULL,
	@FailedPasswordAttemptCount INT = NULL,
	@FailedPasswordAnswerAttemptCount INT = NULL,
	@IsDisabled BIT = NULL,
	-- *******************************************************
	@IsChangePasswordRequired BIT = NULL,
	@IsFirstLogin BIT = NULL,
	@RequestedBy VARCHAR(256) = 'dhughes',
	@IsOutputOnly BIT = NULL,
	@Debug BIT = 1

EXEC api.spCreds_ExtranetUser_Credentials_Validate
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@Username = @Username,
	@Password = @Password,
	@CredentialEntityTypeKey = @CredentialEntityTypeKey,
	@IpAddress = @IpAddress,
	@BrowserName = @BrowserName,
	@BrowserVersion = @BrowserVersion,
	@DeviceName = @DeviceName,
	@IsMobile = @IsMobile,
	@UserAgent = @UserAgent,
	@Comments = @Comments,
	@CredentialToken = @CredentialToken,
	@CredentialEntityID = @CredentialEntityID,
	@IsValid = @IsValid,
	@IsApproved = @IsApproved,
	@IsLockedOut = @IsLockedOut,
	@DateLastLoggedIn = @DateLastLoggedIn,
	@DateLastPasswordChanged = @DateLastPasswordChanged,
	@DatePasswordExpires = @DatePasswordExpires,
	@DateLastLockedOut = @DateLastLockedOut,
	@DateLockOutExpires = @DateLockOutExpires,
	@FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
	@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
	@IsDisabled = @IsDisabled,
	@IsChangePasswordRequired = @IsChangePasswordRequired,
	@IsFirstLogin = @IsFirstLogin,
	@RequestedBy = @RequestedBy,
	@IsOutputOnly = @IsOutputOnly,
	@Debug = @Debug

		
*/

-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM dbo.Store WHERE BusinessEntityID = 92
-- SELECT * FROM dbo.Application
-- SELECT * FROM dbo.RequestTracker
-- SELECT * FROM creds.CredentialRequest ORDER BY CredentialRequestID DESC

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- SELECT * FROM dbo.EventLog ORDER BY EventLogID DESC
-- =============================================
CREATE PROCEDURE [api].[spCreds_ExtranetUser_Credentials_Validate]
	@ApplicationKey VARCHAR(25) = NULL,
	@BusinessKey VARCHAR(50), -- BusinessToken or NABP supported
	@BusinessKeyType VARCHAR(50) = NULL,
	@Username VARCHAR(256),
	@Password VARCHAR(50),
	@CredentialEntityTypeKey VARCHAR(25) = NULL,
	@IpAddress VARCHAR(256) = NULL,
	@BrowserName VARCHAR(256) = NULL,
	@BrowserVersion VARCHAR(50) = NULL,
	@DeviceName VARCHAR(50) = NULL,
	@IsMobile BIT = NULL,
	@UserAgent VARCHAR(500) = NULL,
	@Comments VARCHAR(MAX) = NULL,
	@CredentialToken VARCHAR(50) = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@IsValid BIT = 0 OUTPUT,
	-- Core membership properties ****************************
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT,
	-- *******************************************************
	@IsChangePasswordRequired BIT = NULL OUTPUT,
	@IsFirstLogin BIT = NULL OUTPUT,
	@RequestedBy VARCHAR(256) = NULL,
	@IsOutputOnly BIT = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()


	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' + 
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' + 
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' + 
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' + 
		'@Password=' + dbo.fnToStringOrEmpty(@Password) + ';' + 
		'@CredentialEntityTypeKey=' + dbo.fnToStringOrEmpty(@CredentialEntityTypeKey) + ';' + 
		'@IpAddress=' + dbo.fnToStringOrEmpty(@IpAddress) + ';' + 
		'@BrowserName=' + dbo.fnToStringOrEmpty(@BrowserName) + ';' + 
		'@BrowserVersion=' + dbo.fnToStringOrEmpty(@BrowserVersion) + ';' + 
		'@DeviceName=' + dbo.fnToStringOrEmpty(@DeviceName) + ';' + 
		'@IsMobile=' + dbo.fnToStringOrEmpty(@IsMobile) + ';' + 
		'@UserAgent=' + dbo.fnToStringOrEmpty(@UserAgent) + ';' + 
		'@Comments=' + dbo.fnToStringOrEmpty(@Comments) + ';' + 
		'@CredentialToken=' + dbo.fnToStringOrEmpty(@CredentialToken) + ';' + 
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' + 
		'@IsValid=' + dbo.fnToStringOrEmpty(@IsValid) + ';' + 
		'@IsApproved=' + dbo.fnToStringOrEmpty(@IsApproved) + ';' + 
		'@IsLockedOut=' + dbo.fnToStringOrEmpty(@IsLockedOut) + ';' + 
		'@DateLastLoggedIn=' + dbo.fnToStringOrEmpty(@DateLastLoggedIn) + ';' + 
		'@DateLastPasswordChanged=' + dbo.fnToStringOrEmpty(@DateLastPasswordChanged) + ';' + 
		'@DatePasswordExpires=' + dbo.fnToStringOrEmpty(@DatePasswordExpires) + ';' + 
		'@DateLastLockedOut=' + dbo.fnToStringOrEmpty(@DateLastLockedOut) + ';' + 
		'@DateLockOutExpires=' + dbo.fnToStringOrEmpty(@DateLockOutExpires) + ';' + 
		'@FailedPasswordAttemptCount=' + dbo.fnToStringOrEmpty(@FailedPasswordAttemptCount) + ';' + 
		'@FailedPasswordAnswerAttemptCount=' + dbo.fnToStringOrEmpty(@FailedPasswordAnswerAttemptCount) + ';' + 
		'@IsDisabled=' + dbo.fnToStringOrEmpty(@IsDisabled) + ';' + 
		'@IsChangePasswordRequired=' + dbo.fnToStringOrEmpty(@IsChangePasswordRequired) + ';' + 
		'@IsFirstLogin=' + dbo.fnToStringOrEmpty(@IsFirstLogin) + ';' + 
		'@RequestedBy=' + dbo.fnToStringOrEmpty(@RequestedBy) + ';' + 
		'@IsOutputOnly=' + dbo.fnToStringOrEmpty(@IsOutputOnly) + ';' ;


	/*
	-------------------------------------------------------------------------------------------------------------------------
	-- Log request
	-------------------------------------------------------------------------------------------------------------------------		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	-------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-------------------------------------------------------------------------------------------------------------------------	
	SET @CredentialToken = NULL;
	SET @IsValid = 0;
	SET @IsApproved = 0;
	SET	@IsLockedOut = 0;
	SET	@DateLastLoggedIn = NULL;
	SET	@DateLastPasswordChanged = NULL;
	SET @DatePasswordExpires = NULL;
	SET	@DateLastLockedOut = NULL;
	SET @DateLockOutExpires = NULL;
	SET	@FailedPasswordAttemptCount = 0;
	SET	@FailedPasswordAnswerAttemptCount = 0;
	SET @IsDisabled = 0;	
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);	

	-------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT,
		@BusinessID BIGINT,
		@CredentialEntityTypeID INT,
		@Message VARCHAR(4000)

	-------------------------------------------------------------------------------------------------------------------------
	-- Set variables
	-------------------------------------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialTypeKey = @CredentialEntityTypeKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityTypeID = @CredentialEntityTypeID OUTPUT,
		@IsOutputOnly = 1
	
	
	-------------------------------------------------------------------------------------------------------------------------
	-- Validate the extranet credentials
	-------------------------------------------------------------------------------------------------------------------------	
	EXEC creds.spExtranetUser_Credentials_Validate
		@ApplicationID = @ApplicationID,
		@BusinessEntityID = @BusinessID,
		@Username = @Username,
		@Password = @Password,
		@RequestedBy = @RequestedBy,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@CredentialToken = @CredentialToken OUTPUT,
		@IsValid = @IsValid OUTPUT,
		-- Core membership properties ****************************
		@IsApproved = @IsApproved OUTPUT,
		@IsLockedOut = @IsLockedOut OUTPUT,
		@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
		@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
		@DatePasswordExpires = @DatePasswordExpires OUTPUT,
		@DateLastLockedOut = @DateLastLockedOut OUTPUT,
		@DateLockOutExpires = @DateLockOutExpires OUTPUT,		
		@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
		@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
		@IsDisabled = @IsDisabled OUTPUT,
		-- *******************************************************
		@IsChangePasswordRequired = @IsChangePasswordRequired OUTPUT,
		@IsFirstLogin = @IsFirstLogin OUTPUT
	
	-------------------------------------------------------------------------------------------------------------------------
	-- Record request.
	-- <Summary>
	-- Records the raw request.
	-- </Summary>
	-------------------------------------------------------------------------------------------------------------------------
	BEGIN TRY
	
		DECLARE 
			@EncryptedPassword VARCHAR(256) = dbo.fnDataEncrypt(@Password),
			@RequestTypeID INT = dbo.fnGetRequestTypeID('AUTH')	
		
		EXEC creds.spCredentialRequest_Create
			@RequestTypeID = @RequestTypeID,
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessID,
			@CredentialEntityID = @CredentialEntityID,
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessKey,
			@BusinessKeyType = @BusinessKeyType,
			@CredentialTypeKey = NULL,
			@CredentialKey = @Username,
			@PassKey = @EncryptedPassword,
			@SessionKey = @CredentialToken,
			@IsValid = @IsValid,
			@IpAddress = @IpAddress,
			@BrowserName = @BrowserName,
			@BrowserVersion = @BrowserVersion,
			@DeviceName = @DeviceName,
			@IsMobile = @IsMobile,
			@UserAgent = @UserAgent,
			@Comments = NULL,
			@CreatedBy = @RequestedBy
			
	END TRY
	BEGIN CATCH
	END CATCH
		
	-------------------------------------------------------------------------------------------------------------------------
	-- Response data.
	-- <Summary>
	-- Returns a set of credential items in the response.
	-- </Summary>
	-------------------------------------------------------------------------------------------------------------------------
	IF @IsOutputOnly = 0
	BEGIN
	
		SELECT
			@IsValid AS IsValid,
			@CredentialEntityID AS CredentialEntityID,
			@CredentialToken AS CredentialToken,
			-- Core membership properties *******************************************
			@IsApproved AS IsApproved,
			@IsLockedOut AS IsLockedOut,
			@DateLastLoggedIn AS DateLastLoggedIn,
			@DateLastPasswordChanged AS DateLastPasswordChanged,
			@DatePasswordExpires AS DatePasswordExpires,
			@DateLastLockedOut AS DateLastLocked,
			@DateLockOutExpires AS DateLockOutExpires,
			@FailedPasswordAttemptCount AS FailedPasswordAttemptCount,
			@FailedPasswordAnswerAttemptCount AS FailedPassswordAnswerAttemptCount,
			@IsDisabled AS IsDisabled,
			-- ***********************************************************************
			@IsChangePasswordRequired AS IsChangePasswordRequired,
			@IsFirstLogin AS IsFirstLogin,
			@Message AS Message
	
	END
	
END
