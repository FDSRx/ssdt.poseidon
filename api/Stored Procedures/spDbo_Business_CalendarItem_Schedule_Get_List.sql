﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/23/2014
-- Description:	Returns a list of Calendar items that are applicable to the business.
-- Change log:
-- 6/22/2016 - dhughes - Modidfied the procedure to pass along the Application and Business objects into the base query.

/*

DECLARE
	@ApplicationKey VARCHAR(50) = 'ENG',
	@BusinessKey VARCHAR(50) = '2501624',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Task object identifiers.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@Location VARCHAR(500) = NULL,
	@DateTimeFrameStart DATETIME = NULL,
	@DateTimeFrameEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_Business_CalendarItem_Schedule_Get_List
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@NoteKey = @NoteKey,
	@NotebookKey = @NotebookKey,
	@PriorityKey = @PriorityKey,
	@OriginKey = @OriginKey,
	@TagKey = @TagKey,
	@Location = @Location,
	@DateTimeFrameStart = @DateTimeFrameStart,
	@DateTimeFrameEnd = @DateTimeFrameEnd,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug


*/

-- SELECT * FROM dbo.vwNote
-- SELECT * FROM dbo.vwStore WHERE Name LIKE '%Tommy%'

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spDbo_Business_CalendarItem_Schedule_Get_List]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Task object identifiers.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@Location VARCHAR(500) = NULL,
	@DateTimeFrameStart DATETIME = NULL,
	@DateTimeFrameEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey) + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey) + ';' +
		'@TagKey=' + dbo.fnToStringOrEmpty(@TagKey) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		'@Location=' + dbo.fnToStringOrEmpty(@Location) + ';' +
		'@DataDateStart=' + dbo.fnToStringOrEmpty(@DateTimeFrameStart) + ';' +
		'@DataDateEnd=' + dbo.fnToStringOrEmpty(@DateTimeFrameEnd) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT,
		@BusinessID BIGINT = NULL,
		@ScopeID INT = dbo.fnGetScopeID('INTRN') -- only review internal notes
	
	------------------------------------------------------------------------------------------------------------
	-- Set local variables
	------------------------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1
		
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Deubber On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationKey AS ApplicationKey, @ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey, 
			@BusinessKeyType AS BusinessKeType,	@BusinessID AS BusinessID, @NoteKey AS NoteKey,
			@NotebookKey AS NotebookKey, @PriorityKey AS PriorityKey, @DateTimeFrameStart AS DateTimeFrameStart,
			@DateTimeFrameEnd AS DateTimeFrameEnd
	END

	------------------------------------------------------------------------------------------------------------
	-- Return patient CalendarItem objects.
	------------------------------------------------------------------------------------------------------------
	IF @BusinessID IS NOT NULL
	BEGIN
	
		EXEC api.spDbo_CalendarItem_Schedule_Get_List
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessKey,
			@BusinessKeyType = @BusinessKeyType,
			@NoteKey = @NoteKey,
			@ScopeKey = @ScopeID,
			@BusinessEntityKey = @BusinessID,
			@NotebookKey = @NotebookKey,
			@PriorityKey = @PriorityKey,
			@OriginKey = @OriginKey,
			@TagKey = @TagKey,
			@Location = @Location,
			@DateTimeFrameStart = @DateTimeFrameStart,
			@DateTimeFrameEnd = @DateTimeFrameEnd,
			@Skip = @Skip,
			@Take = @Take,
			@SortCollection = @SortCollection,
			@Debug = @Debug
	
	END

		
END

