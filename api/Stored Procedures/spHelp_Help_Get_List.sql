﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/3/2015
-- Description:	Returns a list of Article objects.
-- 3/8/2016 - dhughes - Modified the procedure to properly retrieve article items based on strict criteria to global criteria.

-- SAMPLE CALL:
/*

DECLARE
	@ApplicationKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = '2501624', -- PRICE CHOPPER 25 -- '2501624' CITY DRUG
	@BusinessKeyType VARCHAR(50) = NULL,
	@ArticleKey VARCHAR(MAX) = NULL,
	@CategoryKey VARCHAR(MAX) = NULL,
	@SectionKey VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@Debug BIT = 1

EXEC api.spHelp_Help_Get_List
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@ArticleKey = @ArticleKey,
	@CategoryKey = @CategoryKey,
	@SectionKey = @SectionKey,
	@Skip = @Skip,
	@Take = @Take,
	@Debug = @Debug

*/

-- SELECT * FROM help.Article
-- SELECT * FROM help.vwArticle
-- SELECT * FROM help.Help
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spHelp_Help_Get_List]
	@ApplicationKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ArticleKey VARCHAR(MAX) = NULL,
	@ArticleTypeKey VARCHAR(MAX) = NULL,
	@ArticleFormatKey VARCHAR(MAX) = NULL,
	@CategoryKey VARCHAR(MAX) = NULL,
	@SectionKey VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	-------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------------------	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@ArticleKey=' + dbo.fnToStringOrEmpty(@ArticleKey) + ';' +
		'@CategoryKey=' + dbo.fnToStringOrEmpty(@CategoryKey) + ';' +
		'@SectionKey=' + dbo.fnToStringOrEmpty(@SectionKey) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------------------------------
	-- Parameter sniffing fix.
	-------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationKey_Local VARCHAR(MAX) = @ApplicationKey,
		@BusinessKey_Local VARCHAR(MAX) = @BusinessKey,
		@BusinessKeyType_Local VARCHAR(50) = @BusinessKeyType,
		@ArticleKey_Local VARCHAR(MAX) = @ArticleKey,
		@ArticleTypeKey_Local VARCHAR(MAX) = @ArticleTypeKey,
		@ArticleFormatKey_Local VARCHAR(MAX) = @ArticleFormatKey,
		@CategoryKey_Local VARCHAR(MAX) = @CategoryKey,
		@SectionKey_Local VARCHAR(MAX) = @SectionKey,
		@Skip_Local BIGINT = @Skip,
		@Take_Local BIGINT = @Take

	-------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @BusinessKeyType_Local = ISNULL(@BusinessKeyType, 'NABP');


	
	-------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey_Local),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey_Local, @BusinessKeyType_Local)


	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod, 
			@ApplicationKey AS ApplicationKey, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@ApplicationID AS ApplicationID, @BusinessID AS BusinessID
	END

	-------------------------------------------------------------------------------------------------------------------
	-- Paging
	-------------------------------------------------------------------------------------------------------------------
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);

	-- Localize paging
	SET @Skip_Local = @Skip;
	SET @Take_Local = @Take;

	-------------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	-------------------------------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpHelpItems') IS NOT NULL
	BEGIN
		DROP TABLE #tmpHelpItems;
	END

	CREATE TABLE #tmpHelpItems (
		Idx BIGINT IDENTITY(1,1),
		HelpID BIGINT,
		ArticleID BIGINT
	);

	-------------------------------------------------------------------------------------------------------------------
	-- Fetch help item list.
	-- <Summar>
	-- Retrieves the appliable help items that match the provided criteria. 
	-- </Summary>
	-- <Remarks>
	-- These items can be applied as a filter when fetching the results.
	-- </Remarks>
	-------------------------------------------------------------------------------------------------------------------
	-- Strict lookup (Business and Application)
	INSERT INTO #tmpHelpItems (
		HelpID,
		ArticleID
	)
	SELECT DISTINCT
		HelpID,
		ArticleID
	--SELECT *
	FROM help.Help
	WHERE ApplicationID = @ApplicationID
		AND BusinessID = @BusinessID

	-- Business lookup (Business only)
	INSERT INTO #tmpHelpItems (
		HelpID,
		ArticleID
	)
	SELECT DISTINCT
		src.HelpID,
		src.ArticleID
	--SELECT *
	FROM help.Help src
		LEFT JOIN #tmpHelpItems tmp
			ON tmp.ArticleID = src.ArticleID
	WHERE src.ApplicationID IS NULL
		AND BusinessID = @BusinessID
		AND tmp.ArticleID IS NULL

	-- Application lookup (Application only)
	INSERT INTO #tmpHelpItems (
		HelpID,
		ArticleID
	)
	SELECT DISTINCT
		src.HelpID,
		src.ArticleID
	--SELECT *
	FROM help.Help src
		LEFT JOIN #tmpHelpItems tmp
			ON tmp.ArticleID = src.ArticleID
	WHERE src.ApplicationID = @ApplicationID
		AND BusinessID IS NULL
		AND tmp.ArticleID IS NULL

	-- All files
	INSERT INTO #tmpHelpItems (
		HelpID,
		ArticleID
	)
	SELECT DISTINCT
		src.HelpID,
		src.ArticleID
	--SELECT *
	FROM help.Help src
		LEFT JOIN #tmpHelpItems tmp
			ON tmp.ArticleID = src.ArticleID
	WHERE src.ApplicationID IS NULL
		AND BusinessID IS NULL
		AND tmp.ArticleID IS NULL


	-- Debug
	-- SELECT * FROM #tmpHelpItems

	-------------------------------------------------------------------------------------------------------------------
	-- Fetch results.
	-------------------------------------------------------------------------------------------------------------------
	;WITH cteItems AS (

		SELECT
			ROW_NUMBER() OVER (ORDER BY 
				CASE WHEN @SortField = 'SortOrder' AND @SortDirection = 'asc'  THEN art.SortOrder END ASC,
				CASE WHEN @SortField = 'SortOrder' AND @SortDirection = 'desc'  THEN art.SortOrder END DESC,  
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'asc'  THEN art.Title END ASC, 
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'desc'  THEN art.Title END DESC, 
				CASE WHEN @SortField IS NULL OR @SortField = '' THEN art.SortOrder ELSE art.SortOrder END DESC,
				CASE WHEN @SortField IS NULL OR @SortField = '' THEN art.Title ELSE art.Title END DESC
			) AS RowNumber,
			hlp.ArticleID,
			art.ArticleTypeID,
			typ.Code AS ArticleTypeCode,
			typ.Name AS ArticleTypeName,
			art.ArticleFormatID,
			frmt.Code AS ArticleFormatCode,
			frmt.Name AS ArticleFormatName,
			art.CategoryID,
			cat.Code AS CategoryCode,
			cat.Name AS CategoryName,
			sec.SectionID AS SectionID,
			sec.Code AS SectionCode,
			sec.Name AS SectionName,
			art.Title,
			art.Description,
			art.FilePath,
			art.AuthorID,
			psn.FirstName AS AuthorFirstName,
			psn.LastName AS AuthorLastName,
			mart.MediaTypeID,
			medtyp.Code AS MediaTypeCode,
			medtyp.Name AS MediaTypeName,
			mart.DurationString AS MediaDurationString,
			mart.DurationMinutes AS MediaDurationMinutes,
			art.IsInactive,
			art.SortOrder,
			art.rowguid,
			art.DateCreated,
			art.DateModified,
			art.CreatedBy,
			art.ModifiedBy
		--SELECT *
		FROM help.Help hlp -- SELECT * FROM help.Help
			JOIN news.Article art -- SELECT * FROM help.Article
				ON hlp.ArticleID = art.ArticleID
			JOIN news.ArticleType typ  -- SELECT * FROM help.ArticleType
				ON art.ArticleTypeID = typ.ArticleTypeID
			JOIN news.ArticleFormat frmt -- SELECT * FROM help.ArticleFormat
				ON art.ArticleFormatID = frmt.ArticleFormatID
			JOIN news.Category cat -- SELECT * FROM help.Category
				ON art.CategoryID = cat.CategoryID
			LEFT JOIN news.Section sec -- SELECT * FROM help.Section
				ON art.SectionID = sec.SectionID
			LEFT JOIN dbo.Person psn -- SELECT * FROM prsn.Person
				ON art.AuthorID = psn.BusinessEntityID
			LEFT JOIN news.MediaArticle mart -- SELECT * FROM help.MediaArticle
				ON art.ArticleID = mart.ArticleID
			LEFT JOIN media.MediaType medtyp -- SELECT * FROM media.MediaType
				ON mart.MediaTypeID = medtyp.MediaTypeID
			JOIN #tmpHelpItems tmp
				ON tmp.HelpID = hlp.HelpID
		WHERE art.IsInactive = 0

	)

	-- Return items
	SELECT
		ArticleID,
		ArticleTypeID,
		ArticleTypeCode,
		ArticleTypeName,
		ArticleFormatID,
		ArticleFormatCode,
		ArticleFormatName,
		CategoryID,
		CategoryCode,
		CategoryName,
		SectionID,
		SectionCode,
		SectionName,
		Title,
		Description,
		FilePath,
		AuthorID,
		AuthorFirstName,
		AuthorLastName,
		IsInactive,
		MediaTypeID,
		MediaTypeCode,
		MediaTypeName,
		MediaDurationString,
		MediaDurationMinutes,
		SortOrder,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	FROM cteItems
	WHERE RowNumber > @Skip_Local 
		AND RowNumber <= (@Skip_Local + @Take_Local)
	ORDER BY RowNumber ASC

END
