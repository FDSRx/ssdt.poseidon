﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/25/2016
-- Description:	Retruns a single or collection of BusinessEntityProvider objects.
-- SAMPLE CALL:
/*

DECLARE
	@BusinessKey VARCHAR(50) = '2501624',
	@BusinessKeyType VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_StoreProvider_Get_List
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug

*/

-- SELECT * FROM dbo.Provider
-- SELECT * FROM dbo.Store
-- SELECT * FROM dbo.StoreProvider
-- =============================================
CREATE PROCEDURE api.spDbo_StoreProvider_Get_List
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE @ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @BusinessKey = NULLIF(@BusinessKey, '');
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @Debug = ISNULL(@Debug, 0);

	--------------------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------------------			
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'Code', 'Name') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;


	--------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType)


	--------------------------------------------------------------------------------------------------------
	-- Set variables.
	--------------------------------------------------------------------------------------------------------
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables.' AS ActionMethod,
			@BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType, @BusinessID AS BusinessID
	END


	--------------------------------------------------------------------------------------------------------
	-- Fetch results.
	--------------------------------------------------------------------------------------------------------
	;WITH cteItems AS (
		SELECT
			--CASE 
			--	WHEN @SortField IN ('StoreName') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY StoreName ASC) 
			--	WHEN @SortField IN ('StoreName') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY StoreName DESC)     
			--	WHEN @SortField = 'ProviderCode' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY ProviderCode ASC) 
			--	WHEN @SortField = 'ProviderCode' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY ProviderCode DESC) 
			--	WHEN @SortField = 'ProviderName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY ProviderName ASC) 
			--	WHEN @SortField = 'ProviderName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY ProviderName DESC)
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated ASC) 
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated DESC)   
			--	ELSE ROW_NUMBER() OVER (ORDER BY StoreName ASC)
			--END AS RowNumber,
			ROW_NUMBER() OVER (ORDER BY	
				CASE WHEN @SortField IN ('StoreName') AND @SortDirection = 'asc'  THEN StoreName END ASC, 
				CASE WHEN @SortField IN ('StoreName') AND @SortDirection = 'desc'  THEN StoreName END DESC, 
				CASE WHEN @SortField = 'ProviderCode' AND @SortDirection = 'asc'  THEN ProviderCode END ASC,
				CASE WHEN @SortField = 'ProviderCode' AND @SortDirection = 'desc'  THEN ProviderCode END DESC, 
				CASE WHEN @SortField = 'ProviderName' AND @SortDirection = 'asc'  THEN ProviderName END ASC,
				CASE WHEN @SortField = 'ProviderName' AND @SortDirection = 'desc'  THEN ProviderName END DESC,
				CASE WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN DateCreated END ASC,
				CASE WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN DateCreated END DESC,
				CASE WHEN @SortField IS NULL OR @SortField = '' THEN BusinessEntityProviderID ELSE BusinessEntityProviderID  END DESC
			) AS RowNumber,
			BusinessEntityProviderID,
			StoreID,
			Nabp,
			StoreName,
			ProviderID,
			ProviderCode,
			ProviderName,
			Username,
			PasswordUnencrypted,
			PasswordEncrypted,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy
		--SELECT *
		FROM dbo.vwStoreProvider
		WHERE (@BusinessKey IS NULL OR (@BusinessKey IS NOT NULL AND StoreID = @BusinessID))
	)

	SELECT
		BusinessEntityProviderID,
		StoreID,
		Nabp,
		StoreName,
		ProviderID,
		ProviderCode,
		ProviderName,
		Username,
		PasswordUnencrypted,
		PasswordEncrypted,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
	FROM cteItems
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.


END
