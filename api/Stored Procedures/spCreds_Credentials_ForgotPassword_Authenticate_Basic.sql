﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2014
-- Description:	Authenticates the user information to determine if a password reset/change can be performed
--	by the user making the request (i.e. let's do our best to verify the person is who they say they are).
-- SAMPLE CALL:
/*

EXEC api.spCreds_Credentials_ForgotPassword_Authenticate_Basic
	@ApplicationKey = NULL,
	@BusinessKey = 'ID_10684',
	@Username = 'dhughes',
	@CredentialTypeKey = 'Extranet',
	@BirthDate = '3/14/82'
	,@FirstName = 'daniel'
	,@LastName = 'hughes'
	,@ValidateName = 1

*/

-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM dbo.RequestTracker ORDER By RequestTrackerID DESC
-- SELECT * FROM dbo.EventLog ORDER BY EventLogID DESC
-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
-- SELECT * FROM dbo.TokenType
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [api].[spCreds_Credentials_ForgotPassword_Authenticate_Basic]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@CredentialTypeKey VARCHAR(50) = NULL,
	@Username VARCHAR(256),
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@BirthDate DATETIME = NULL,
	@ValidateName BIT = NULL,
	-- Core device properties ************************************************************
	@IpAddress VARCHAR(256) = NULL,
	@BrowserName VARCHAR(256) = NULL,
	@BrowserVersion VARCHAR(50) = NULL,
	@DeviceName VARCHAR(50) = NULL,
	@IsMobile BIT = NULL,
	@UserAgent VARCHAR(500) = NULL,
	@Comments VARCHAR(MAX) = NULL,
	-- ***********************************************************************************
	@IsValid BIT = NULL,
	@CredentialToken VARCHAR(50) = NULL OUTPUT,
	-- Core membership properties ****************************
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT,
	-- *******************************************************
	@IsChangePasswordRequired BIT = NULL OUTPUT,
	@LanguageKey VARCHAR(25) = NULL,
	@RequestedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@CredentialTypeKey=' + dbo.fnToStringOrEmpty(@CredentialTypeKey) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName) + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName) + ';' +
		'@BirthDate=' + dbo.fnToStringOrEmpty(@BirthDate) + ';' +
		'@ValidateName=' + dbo.fnToStringOrEmpty(@ValidateName) + ';' 

	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	--------------------------------------------------------------------------------------------
	-- Instance.
	--------------------------------------------------------------------------------------------
	DECLARE @Source VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	--------------------------------------------------------------------------------------------
	-- Sanitize inut.
	--------------------------------------------------------------------------------------------
	SET @IsValid = 0;
	SET @ValidateName = ISNULL(@ValidateName, 0);


	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT = NULL,
		@BusinessID INT = NULL,
		@RecordRequest BIT = ISNULL(CONVERT(BIT, dbo.fnGetConfigValue('RFPBA')), 0),
		@RequestTypeID INT = dbo.fnGetRequestTypeID('FRGTPWD'),
		@CredentialEntityTypeID INT = NULL,
		@CredentialEntityID BIGINT = NULL,
		@CredentialExists BIT = 0,
		@ErrorMessage VARCHAR(4000),
		@Message VARCHAR(4000) = 'We have successfully located your account. Please use the provided security token when reseting your password.',
		@RequestKey VARCHAR(256),
		@IsSuccess BIT = 0
	
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialTypeKey = @CredentialTypeKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityTypeID = @CredentialEntityTypeID OUTPUT,		
		@IsOutputOnly = 1
		
	
	-- Debug
	--SELECT @ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @CredentialEntityTypeID AS CredentialEntityTypeID
	

	BEGIN TRY
	
		--------------------------------------------------------------------------------------------
		-- Record request.
		--------------------------------------------------------------------------------------------
		IF ISNULL(@RecordRequest, 0) = 1
		BEGIN
	    
			DECLARE @RequestData VARCHAR(MAX) =
				'<ForgotPasswordAuthenticationRequest>' +
					'<BusinessToken>' + ISNULL(@BusinessKey, '') + '</BusinessToken>' +
					'<BusinessID>' + ISNULL(CONVERT(VARCHAR, @BusinessID), '') + '</BusinessID>' +
					'<Username>' + ISNULL(@Username, '') + '</Username>' +
					'<FirstName>' + ISNULL(@FirstName, '') + '</FirstName>' +
					'<LastName>' + ISNULL(@LastName, '') + '</LastName>' +
					'<BirthDate>' + ISNULL(@BirthDate, '') + '</BirthDate>' +
					'<IpAddress>' + ISNULL(@IPAddress, '') + '</IpAddress>' +
					'<BrowserName>' + ISNULL(@BrowserName, '') + '</BrowserName>' +
					'<BrowserVersion>' + ISNULL(@BrowserVersion, '') + '</BrowserVersion>' +
					'<DeviceName>' + ISNULL(@DeviceName, '') + '</DeviceName>' +
					'<IsMobile>' + ISNULL(CONVERT(VARCHAR, @IsMobile), '0') + '</IsMobile>' +
					'<UserAgent>' + ISNULL(@UserAgent, '') + '</UserAgent>' +
					'<LanguageKey>' + ISNULL(@LanguageKey, '') + '</LanguageKey>' +
				'</ForgotPasswordAuthenticationRequest>'
		    
			EXEC dbo.spRequestLog_Create
				@ApplicationKey = @ApplicationKey,
				@BusinessKey = @BusinessKey,
				@RequesterKey = @Username,
				@ApplicationID = @ApplicationID,
				@BusinessEntityID = @BusinessID,
				@SourceName = @Source,
				@RequestData = @RequestData,
				@RequestTypeID = @RequestTypeID
		
		END
	
		
		SET @RequestKey = dbo.fnIfNullOrEmpty(@BusinessID, '<NULLBUSINESS>') + '-FRGTPWD-' + dbo.fnIfNullOrEmpty(@Username, '<NULLUSERNAME>')
			
		-----------------------------------------------------------------------------------------------
		-- Fetch credential entity
		-- <Summary>
		-- Interrogates the credentials for the provided type
		-- to determine if the supplied credentials meets the required
		-- criteria to be authenticated.
		-- </Summary>
		-----------------------------------------------------------------------------------------------
		EXEC creds.spCredentialEntity_Derived_Exists
			@CredentialEntityTypeID = @CredentialEntityTypeID,
			@BusinessEntityID = @BusinessID,
			@DerivedCredentialKey = @Username,
			@CredentialEntityID = @CredentialEntityID OUTPUT,
			@Exists = @CredentialExists OUTPUT
		

		-- If a CredentialEntityID was discovered, then verify certain demographics of the user.
		IF @CredentialEntityID IS NOT NULL
		BEGIN
										
			SELECT
				@IsValid = 1
			FROM creds.CredentialEntity ent	
				JOIN dbo.Person psn
					ON ent.PersonId = psn.BusinessEntityID
			WHERE ent.CredentialEntityID = @CredentialEntityID
				AND psn.BirthDate = @BirthDate
				AND ( @ValidateName = 0
					OR ( @ValidateName = 1 
						AND psn.FirstName = @FirstName
						AND psn.LastName = @LastName
					)
				)				
			
		END
		
		-- Set validity of demographics and/or user discovery.
		SET @IsValid = CASE WHEN @CredentialEntityID IS NULL THEN 0 ELSE @IsValid END;
		SET @Message = 
			CASE 
				WHEN @IsValid = 1 THEN @Message 
				ELSE 'We''re sorry. The information provided could not be located in our system. Please ensure ' +
					'you have entered your information correctly.' 
			END;
		
		
		-- Debug
		--SELECT @CredentialEntityID AS CredentialEntityID, @CredentialExists AS IsFound, @IsValid AS IsValid, @RequestKey AS RequestKey

		---------------------------------------------------------------------------------------------------
		-- Track the request (regardless of being valid).
		-- <Summary>
		-- Tracks the attempt.  If the request is valid and not locked out, then it will
		-- be removed from the tracking table. Otherwise, the attempt will be recorded
		-- for possible further inspection.
		-- </Summary>
		---------------------------------------------------------------------------------------------------	
		DECLARE 
			@RequestTrackerID BIGINT = NULL,
			@MaxRequestsAllowed INT = NULL,
			@RequestCount INT = NULL
		
		EXEC dbo.spRequestTracker_TrackWithLockOut
			@RequestTypeID = @RequestTypeID,
			@SourceName = @Source,
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessKey,
			@RequesterKey = @Username,
			@RequestKey = @RequestKey,
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessID,
			@RequesterID = @CredentialEntityID,
			@CreatedBy = @RequestedBy,
			@ModifiedBy = @RequestedBy,
			@MaxRequestsAllowed = @MaxRequestsAllowed OUTPUT,
			@RequestCount = @RequestCount OUTPUT,
			@IsLocked = @IsLockedOut OUTPUT,
			@RequestTrackerID = @RequestTrackerID OUTPUT
			
		---------------------------------------------------------------------------------------------------
		-- Validate the lock status of the account and determine if that affects
		-- the validity of the user/password verification.
		---------------------------------------------------------------------------------------------------	
		IF @IsLockedOut = 1
		BEGIN
		
			IF @RequestCount = @MaxRequestsAllowed AND @IsValid = 0
			BEGIN
				SET @IsValid = 0;
				SET @IsLockedOut = 1
			END
			ELSE IF @RequestCount >= @MaxRequestsAllowed AND @IsValid = 0
			BEGIN
				SET @IsValid = 0;
				SET @IsLockedOut = 1
			END
			ELSE IF @RequestCount <= @MaxRequestsAllowed AND @IsValid = 1
			BEGIN
				SET @IsValid = 1;
				SET @IsLockedOut = 0 -- Unlock request as it is not really locked.
			END
			ELSE
			BEGIN
				SET @IsValid = 0;
			END
			
			-- Set message accordingly due to locked status.
			SET @Message = 
				CASE 
					WHEN @IsLockedOut = 1 THEN 'The requested account is currently locked due to too many unsuccessful attempts. ' +
								'Please try again later.'
					ELSE @Message
				END;
			
			---------------------------------------------------------------------------------------------------
			-- Record  locked event.  
			-- <Summary>
			-- Records the locked event, but only during its first encounter.
			-- </Summary>
			-- <Remarks>
			-- Do not fail the process if the event was
			-- unable to be recorded.
			-- </Remarks>
			---------------------------------------------------------------------------------------------------
			IF @RequestCount = @MaxRequestsAllowed AND @IsValid = 0
			BEGIN
			
				BEGIN TRY
				
					DECLARE @LockOutEventID INT = creds.fnGetCredentialEventID('LO');
					
					DECLARE @LockOutEventData XML = 
						'<CredentialEventData>' +
							dbo.fnXmlWriteElementString('ApplicationID', @ApplicationID) +
							dbo.fnXmlWriteElementString('BusinessID', @BusinessID) +
							dbo.fnXmlWriteElementString('CredentialEntityID', @CredentialEntityID) +
							dbo.fnXmlWriteElementString('Username', @Username) +
							dbo.fnXmlWriteElementString('CredentialEntityTypeID', @CredentialEntityTypeID) +
							dbo.fnXmlWriteElementString('CredentialToken', NULL) +
							dbo.fnXmlWriteElementString('MaxRequestsAllowed', @MaxRequestsAllowed) +
						'</CredentialEventData>';
					
					EXEC creds.spCredentialEventLog_Create
						@EventID = @LockOutEventID,
						@EventSource = @Source,
						@EventDataXml = @LockOutEventData,
						@CreatedBy = @RequestedBy
				
				END TRY
				BEGIN CATCH		
				END CATCH
			
		
			END	
			
		END
	

		---------------------------------------------------------------------------------------------------
		-- If a valid response was received from the
		-- query then we need to delete the tracked request as it was successful.
		---------------------------------------------------------------------------------------------------
		IF @CredentialEntityID IS NOT NULL AND @IsValid = 1
		BEGIN
		
			EXEC dbo.spRequestTracker_Delete
				@RequestTrackerID = @RequestTrackerID,
				@ApplicationKey = @ApplicationID,
				@BusinessKey = @BusinessID,
				@RequesterKey = @CredentialEntityID,
				@RequestKey = @RequestKey
			
			
			DECLARE 
				@TokenTypeID INT = dbo.fnGetTokenTypeID('RSTPWD'),
				@TokenDurationMinutes INT = 30
			
			
			EXEC creds.spCredentialToken_Create
				@CredentialEntityID = @CredentialEntityID,
				@ApplicationID = @ApplicationID,
				@BusinessEntityID = @BusinessID,
				@TokenTypeID = @TokenTypeID,
				@TokenDurationMinutes = @TokenDurationMinutes,
				@CreatedBy = @RequestedBy,
				@Token = @CredentialToken OUTPUT
			
			SET @IsSuccess = 1;
			
			-- Record Forgot Password Request Event
			BEGIN TRY
				
					DECLARE @AuthentciationEventID INT = creds.fnGetCredentialEventID('FPBA');
					
					DECLARE @AuthenticationEventData XML = 
						'<CredentialEventData>' +
							dbo.fnXmlWriteElementString('ApplicationID', @ApplicationID) +
							dbo.fnXmlWriteElementString('BusinessID', @BusinessID) +
							dbo.fnXmlWriteElementString('CredentialEntityID', @CredentialEntityID) +
							dbo.fnXmlWriteElementString('Username', @Username) +
							dbo.fnXmlWriteElementString('CredentialEntityTypeID', @CredentialEntityTypeID) +
							dbo.fnXmlWriteElementString('CredentialToken', @CredentialToken) +
						'</CredentialEventData>';
					
					EXEC creds.spCredentialEventLog_Create
						@EventID = @AuthentciationEventID,
						@EventSource = @Source,
						@EventDataXml = @AuthenticationEventData,
						@CreatedBy = @RequestedBy
				
				END TRY
				BEGIN CATCH		
				END CATCH	
				
		END	
		

	
	END TRY
	BEGIN CATCH
		
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
	
	END CATCH
	
	

	---------------------------------------------------------------------------------------------------
	-- If a valid response was received from the membership
	-- query then we need to delete the tracked request as it was successful.
	---------------------------------------------------------------------------------------------------			
	SELECT 
		@IsSuccess AS IsSuccess,
		@IsValid AS IsValid,
		@IsLockedOut AS IsLockedOut,
		@IsDisabled AS IsDisabled,
		@Message AS Message,
		@CredentialToken AS Token
		
END
