﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/29/2014
-- Description:	Creates a new patient note interaction object.
-- Change log:
-- 6/9/2016 - dhughes - Added the Application object as a parameter.

/*

DECLARE 
	@ApplicationKey INT = NULL,
	-- Business/Patient properties
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	-- Note interaction properties
	@NoteID BIGINT,
	@NoteInteractionID BIGINT = NULL,
	@InteractionID BIGINT = NULL,
	@InteractedWithKey VARCHAR(50) = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeKey VARCHAR(25) = NULL,
	@InteractionTypeKey VARCHAR(25) = NULL,
	@DispositionTypeKey VARCHAR(25) = NULL,
	@InitiatedByKey VARCHAR(50) = NULL,
	@InitiatedByString VARCHAR(256) = NULL,
	-- Comment Note
	@CommentNoteID BIGINT = NULL,
	@NoteTypeKey VARCHAR(25) = NULL,
	@NotebookKey VARCHAR(25) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityKey INT = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@OriginKey VARCHAR(25) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	-- Entry properties
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = 1

EXEC api.spPhrm_Patient_NoteInteraction_Create
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@NoteID = @NoteID,
	@NoteInteractionID = @NoteInteractionID OUTPUT,
	@InteractionID = @InteractionID OUTPUT,
	@InteractedWithKey = @InteractedWithKey,
	@InteractedWithString = @InteractedWithString,
	@ContactTypeKey = @ContactTypeKey,
	@InteractionTypeKey = @InteractionTypeKey,
	@DispositionTypeKey = @DispositionTypeKey,
	@InitiatedByKey = @InitiatedByKey,
	@InitiatedByString = @InitiatedByString,
	@CommentNoteID = @CommentNoteID OUTPUT,
	@NoteTypeKey = @NoteTypeKey,
	@NotebookKey = @NotebookKey,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityKey = @PriorityKey,
	@Tags = @Tags,
	@OriginKey = @OriginKey,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug


SELECT @NoteID AS NoteID, @InteractionID AS InteractionID, @NoteInteractionID AS NoteInteractionID

*/

-- SELECT * FROM dbo.ContactType
-- SELECT * FROM dbo.vwTask ORDER BY NoteID DESC
-- SELECT * FROM dbo.vwNote ORDER BY NoteID DESC
-- SELECT * FROM dbo.vwCalendarItem ORDER BY NoteID DESC
-- SELECT * FROM dbo.Interaction ORDER BY InteractionID DESC
-- SELECT * FROM dbo.NoteInteraction ORDER BY NoteInteractionID DESC
-- SELECT * FROM creds.CredentialEntity ORDER BY CredentialEntityID DESC

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_NoteInteraction_Create]
	@ApplicationKey VARCHAR(50) = NULL,
	-- Business/Patient properties
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	-- Note interaction properties
	@NoteID BIGINT,
	@NoteInteractionID BIGINT = NULL OUTPUT,
	@InteractionID BIGINT = NULL OUTPUT,
	@InteractedWithKey VARCHAR(50) = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeKey VARCHAR(25) = NULL,
	@InteractionTypeKey VARCHAR(25) = NULL,
	@DispositionTypeKey VARCHAR(25) = NULL,
	@InitiatedByKey VARCHAR(50) = NULL,
	@InitiatedByString VARCHAR(256) = NULL,
	-- Comment Note
	@CommentNoteID BIGINT = NULL OUTPUT,
	@NoteTypeKey VARCHAR(25) = NULL OUTPUT,
	@NotebookKey VARCHAR(25) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityKey INT = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@OriginKey VARCHAR(25) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData VARCHAR(MAX) = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	-- Entry properties
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' +
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@NoteInteractionID=' + dbo.fnToStringOrEmpty(@NoteInteractionID) + ';' +
		'@InteractionID=' + dbo.fnToStringOrEmpty(@InteractionID) + ';' +
		'@InteractedWithKey=' + dbo.fnToStringOrEmpty(@InteractedWithKey) + ';' +
		'@InteractedWithString=' + dbo.fnToStringOrEmpty(@InteractedWithString) + ';' +
		'@ContactTypeKey=' + dbo.fnToStringOrEmpty(@ContactTypeKey) + ';' +
		'@InteractionTypeKey=' + dbo.fnToStringOrEmpty(@InteractionTypeKey) + ';' +
		'@DispositionTypeKey=' + dbo.fnToStringOrEmpty(@DispositionTypeKey) + ';' +
		'@InitiatedByKey=' + dbo.fnToStringOrEmpty(@InitiatedByKey) + ';' +
		'@InitiatedByString=' + dbo.fnToStringOrEmpty(@InitiatedByString) + ';' +
		'@CommentNoteID=' + dbo.fnToStringOrEmpty(@CommentNoteID) + ';' +
		'@NoteTypeKey=' + dbo.fnToStringOrEmpty(@NoteTypeKey) + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(@AdditionalData) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' ;


	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
		
	------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------------------------------------------------------
	SET @NoteInteractionID = NULL;
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @PatientKeyType = ISNULL(@PatientKeyType, 'SourcePatientKey')
	
	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
	DEClARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@CompletedByID BIGINT,
		@InteractedWithID BIGINT,
		@InitiatedByID BIGINT,
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType),
		@ScopeID INT = dbo.fnGetScopeID('INTRN'), -- only create internal notes
		@NoteTypeID INT = ISNULL(dbo.fnGetNoteTypeID(@NoteTypeKey), dbo.fnGetNoteTypeID('GNRL')),
		@NotebookID INT = dbo.fnGetNotebookID(@NotebookKey),
		@PriorityID INT = ISNULL(dbo.fnGetPriorityID(@PriorityKey), dbo.fnGetPriorityID('NONE')),
		@OriginID INT = ISNULL(dbo.fnGetOriginID(@OriginKey), dbo.fnGetOriginID('UNKN')),
		@ContactTypeID INT = ISNULL(dbo.fnGetContactTypeID(@ContactTypeKey), dbo.fnGetContactTypeID('UNKN')),
		@InteractionTypeID INT = dbo.fnGetInteractionTypeID(@InteractionTypeKey),
		@DispositionTypeID INT = dbo.fnGetDispositionTypeID(@DispositionTypeKey)

	------------------------------------------------------------------------------------------------------------
	-- Set local variables
	------------------------------------------------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, @BusinessKeyType, @PatientKey, @PatientKeyType);
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod, 
			@PatientID AS PatientID, @personID AS PersonID, @BusinessID AS BusinessID, @ApplicationID AS ApplicationID,
			@ApplicationKey AS ApplicationKey, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@Args AS Arguments
	END
		
	------------------------------------------------------------------------------------------------------------
	-- Create a note interaction
	------------------------------------------------------------------------------------------------------------
	EXEC dbo.spNoteInteraction_Create
		-- Note properties
		@NoteID = @NoteID,
		@ApplicationID = @ApplicationID,
		@BusinessID = @BusinessID,
		-- Note interaction properties
		@NoteInteractionID = @NoteInteractionID OUTPUT,
		@InteractionID = @InteractionID OUTPUT,
		@InteractedWithID = @InteractedWithID,
		@InteractedWithString = @InteractedWithString,
		@ContactTypeID = @ContactTypeID,
		@InteractionTypeID = @InteractionTypeID,
		@DispositionTypeID = @DispositionTypeID,
		@InitiatedByID = @InitiatedByID,
		@InitiatedByString = @InitiatedByString,
		-- Comment Note
		@CommentNoteID = @CommentNoteID,
		@ScopeID = @ScopeID,
		@BusinessEntityID = @PersonID,
		@NoteTypeID = @NoteTypeID,
		@NotebookID = @NotebookID,
		@Title = @Title,
		@Memo = @Memo,
		@PriorityID = @PriorityID,
		@Tags = @Tags, -- A comma separated list of tags that identify a note.
		@OriginID = @OriginID,
		@OriginDataKey = @OriginDataKey,
		@AdditionalData = @AdditionalData,
		@CorrelationKey = @CorrelationKey,
		-- Entry properties
		@CreatedBy = @CreatedBy
	
	
END
