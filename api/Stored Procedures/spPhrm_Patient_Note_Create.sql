﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/17/2014
-- Description:	Creates a new Note object for a patient.
-- Change log:
-- 6/8/2016 - dhughes - Modified the procedure to accept the Application object as a parameter.

-- SAMPLE CALL;
/*

DECLARE 
	@ApplicationKey VARCHAR(50) = 'ENG',
	@BusinessKey VARCHAR(50) = '7871787',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@PatientKey VARCHAR(50) = '21656',
	@PatientKeyType VARCHAR(50) = NULL,
	@ScopeKey VARCHAR(50) = NULL,
	@NoteTypeKey VARCHAR(50) = 'GNRL',
	@NotebookKey VARCHAR(50) = NULL,
	@OriginKey VARCHAR(50) = 'Manual SQL',
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData VARCHAR(MAX) = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@Title VARCHAR(1000) = 'Test API Sticky Note On ' + CONVERT(VARCHAR, GETDATE()),
	@Memo VARCHAR(MAX) = 'This is a test note call from the API #: ' + CONVERT(VARCHAR, RAND()),
	@PriorityKey VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = 'dhughes',
	@NoteID BIGINT = NULL,
	@Debug BIT = 1

	
EXEC api.spPhrm_Patient_Note_Create
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@ScopeKey = @ScopeKey,
	@NoteTypeKey = @NoteTypeKey,
	@NotebookKey = @NotebookKey,
	@OriginKey = @OriginKey,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityKey = @PriorityKey,
	@Tags = @Tags,
	@CreatedBy = @CreatedBy,
	@NoteID = @NoteID OUTPUT,
	@Debug = @Debug


SELECT @NoteID AS NoteID

 */
 
-- SELECT * FROM dbo.Note ORDER BY NoteID DESC
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook

-- SELECT * FROM dbo.ErrorLog
-- SELECT * FROM dbo.InformationLog
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Note_Create]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@ScopeKey VARCHAR(50) = NULL,
	@NoteTypeKey VARCHAR(50) = NULL,
	@NotebookKey VARCHAR(50) = NULL,
	@OriginKey VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData VARCHAR(MAX) = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX),
	@PriorityKey VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@NoteID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' +
		'@ScopeKey=' + dbo.fnToStringOrEmpty(@ScopeKey) + ';' +
		'@NoteTypeKey=' + dbo.fnToStringOrEmpty(@NoteTypeKey) + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey) + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(@AdditionalData) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------------------------------------------------------
	SET @NoteID = NULL;
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @PatientKeyType = ISNULL(@PatientKeyType, 'SourcePatientKey')
	
	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@BusinessID BIGINT = NULL,
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@ScopeID INT = dbo.fnGetScopeID('INTRN'), -- only review internal notes
		@NoteTypeID INT = dbo.fnGetNoteTypeID(@NoteTypeKey),
		@NotebookID INT = dbo.fnGetNotebookID(@NotebookKey),
		@PriorityID INT = dbo.fnGetPriorityID(@PriorityKey),
		@OriginID INT = dbo.fnGetOriginID(@OriginKey)
	
	------------------------------------------------------------------------------------------------------------
	-- Set local variables
	------------------------------------------------------------------------------------------------------------
	SET @BusinessID = CASE WHEN @BusinessKeyType IS NOT NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType) ELSE NULL END;

	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, @BusinessKeyType, @PatientKey, @PatientKeyType);
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');


	-- debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod, 
			@NoteTypeID AS NoteTypeID, @ScopeID AS ScopeID, @NoteTypeID AS NoteTypeID, @ApplicationID AS ApplicationID,
			@NotebookID AS NotebookID, @PriorityID AS PriorityID, @OriginID AS OriginID, @PersonID AS PersonID,
			@PatientID AS PatientID, @Args AS Arguments
	END	

	
	------------------------------------------------------------------------------------------------------------
	-- Return patient Note objects.
	------------------------------------------------------------------------------------------------------------
	EXEC dbo.spNote_Create
		@ApplicationID = @ApplicationID,
		@BusinessID = @BusinessID,
		@ScopeID = @ScopeID,
		@BusinessEntityID = @PersonID,
		@NoteTypeID = @NoteTypeID,
		@NotebookID = @NotebookID,
		@PriorityID = @PriorityID,
		@Title = @Title,
		@Memo = @Memo,
		@Tags = @Tags,
		@OriginID = @OriginID,
		@OriginDataKey = @OriginDataKey,
		@AdditionalData = @AdditionalData,
		@CorrelationKey = @CorrelationKey,
		@CreatedBy = @CreatedBy,
		@NoteID = @NoteID OUTPUT
			
END
