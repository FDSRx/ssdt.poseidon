﻿-- =============================================
-- Author:		Emily Kizer
-- Create date: 10/31/2016
-- Description:	Returns a list of banner types
-- =============================================
/*
DECLARE
	@BannerTypeKey VARCHAR(25) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_BannerType_Get_List
	@BannerTypeKey = @BannerTypeKey,
	@Debug = @Debug
*/
CREATE PROCEDURE [api].[spDbo_BannerType_Get_List]
	-- Add the parameters for the stored procedure here
	@BannerTypeKey VARCHAR(25) = NULL,
	@Debug BIT = NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL

	DECLARE @Args VARCHAR(MAX) = 
		'@BannerTypeKey=' + dbo.fnToStringOrEmpty(@BannerTypeKey)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);

	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE 
		@BannerTypeID INT = NULL

		--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BannerTypeKey object (using the system key translator) 
	-- into a BannerTypeID object using the supplied BannerType key

	-- Translate system key variables using the system translator.
	IF @BannerTypeKey IS NOT NULL
	BEGIN
		SET @BannerTypeID = dbo.fnGetBannerTypeID(@BannerTypeKey)
	END



	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@BannerTypeKey AS BannerTypeKey, @BannerTypeID AS BannerTypeID
	END
    
	-- Insert statements for procedure here
	SELECT
		BannerTypeID,
		Code,
		Name,
		DateCreated,
		DateModified
	--SELECT *
	FROM dbo.BannerType
	WHERE @BannerTypeID IS NULL OR BannerTypeID = @BannerTypeID
END