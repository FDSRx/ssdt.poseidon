﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Gets the specified patient note.
-- SAMPLE CALL;
/*

DECLARE
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = '7871787',
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = '21656',
	@NoteID BIGINT = 451,
	@Debug BIT = 1

EXEC api.spPhrm_Patient_Note_Get_Single
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@NoteID = @NoteID,
	@Debug = @Debug

 */

-- SELECT * FROM phrm.fnGetPatientInformation(2) 
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.NoteType

-- SELECT * FROM dbo.ErrorLog
-- SELECT * FROM dbo.InformationLog
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Note_Get_Single]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteID BIGINT = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@ScopeID INT = dbo.fnGetScopeID('INTRN') -- only review internal notes
	
	------------------------------------------------------------------------------------------------------------
	-- Set local variables
	------------------------------------------------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');
	

	
	------------------------------------------------------------------------------------------------------------
	-- Return patient Note object.
	------------------------------------------------------------------------------------------------------------
	EXEC api.spDbo_Note_Get_Single
		@NoteID = @NoteID,
		@ScopeID = @ScopeID,
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@BusinessEntityID = @PersonID
			
END
