﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/21/2014
-- Description:	Returns a list of notebooks.
-- SAMPLE CALL: api.spDbo_Notebook_Get_List
-- =============================================
CREATE PROCEDURE [api].[spDbo_Notebook_Get_List]
	@NotebookID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		NotebookID,
		Code,
		Name,
		DateCreated
	FROM dbo.Notebook
	WHERE ( @NotebookID IS NULL OR NotebookID IN (SELECT Value FROM dbo.fnSplit(@NotebookID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	
END
