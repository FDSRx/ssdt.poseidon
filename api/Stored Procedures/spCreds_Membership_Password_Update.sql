﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/4/2014
-- Description:	Changes the credential's password.
/*

-- Credentials (username and password) verification and update.
EXEC api.spCreds_Membership_Password_Update
	@ApplicationKey = 'ESMT',
	@BusinessKey = 10684,
	@BusinessKeyType = NULL,
	@Username = 'dhughes',
	@Password = 'password',
	@CredentialKey = NULL,
	@CredentialEntityTypeKey = 'EXTRANET',
	@NewPassword = 'password',
	@ConfirmNewPassword = 'password',
	@ModifiedBy = 'dhughes'

-- Credential Key or Id verification and update.
EXEC api.spCreds_Membership_Password_Update
	@ApplicationKey = 'ESMT',
	@BusinessKey = 10684,
	@BusinessKeyType = NULL,
	--@Username = 'dhughes',
	--@Password = 'password',
	@CredentialKey = '801050',
	@VerifyCredentialKeyOnly = 1,
	@CredentialEntityTypeKey = 'EXTRANET',
	@NewPassword = 'password',
	@ConfirmNewPassword = 'password',
	@ModifiedBy = 'dhughes'
	
EXEC creds.spMembership_Unlock
	@CredentialEntityID = 801050,
	@ForceUnlock = 1	
	
*/

-- SELECT * FROM creds.vwExtranetUser WHERE CredentialEntityID = 959790
-- SELECT * FROM creds.Membership
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [api].[spCreds_Membership_Password_Update]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(25) = NULL,
	@Username VARCHAR(256) = NULL,
	@Password VARCHAR(50) = NULL,
	@CredentialKey VARCHAR(50) = NULL,
	@CredentialEntityTypeKey VARCHAR(25) = NULL,
	@VerifyCredentialKeyOnly BIT = NULL,
	@ConfirmPassword VARCHAR(50) = NULL,
	@NewPassword VARCHAR(50),
	@ConfirmNewPassword VARCHAR(50),
	@IgnoreMembershipValidation BIT = NULL,
	@IgnoreIsApprovedValidation BIT = NULL,
	@IgnoreLockedOutValidation BIT = NULL,
	@IgnoreDisabledValidation BIT = NULL,
	@IgnoreChangePasswordValidation BIT = NULL,
	@IsValid BIT = 0 OUTPUT,
	-- Core membership properties ****************************
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT,
	-- *******************************************************
	@IsChangePasswordRequired BIT = NULL OUTPUT,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@Password=' + dbo.fnToStringOrEmpty(@Password) + ';' +
		'@CredentialKey=' + dbo.fnToStringOrEmpty(@CredentialKey) + ';' +
		'@CredentialEntityTypeKey=' + dbo.fnToStringOrEmpty(@CredentialEntityTypeKey) + ';' +
		'@ConfirmPassword=' + dbo.fnToStringOrEmpty(@ConfirmPassword) + ';' +
		'@NewPassword=' + dbo.fnToStringOrEmpty(@NewPassword) + ';' +
		'@ConfirmNewPassword=' + dbo.fnToStringOrEmpty(@NewPassword) + ';' +
		'@DatePasswordExpires=' + dbo.fnToStringOrEmpty(@DatePasswordExpires) + ';'

	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	
	--------------------------------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------------------------------
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, NULLIF(@BusinessKeyType, ''));
	SET @VerifyCredentialKeyOnly = ISNULL(@VerifyCredentialKeyOnly, 0);
	SET @IgnoreIsApprovedValidation = ISNULL(@IgnoreIsApprovedValidation, 0);
	SET @IgnoreMembershipValidation = ISNULL(@IgnoreMembershipValidation, 0);
	SET @IgnoreLockedOutValidation = ISNULL(@IgnoreLockedOutValidation, 0);
	SET @IgnoreDisabledValidation = ISNULL(@IgnoreDisabledValidation, 0);
	SET @IgnoreChangePasswordValidation = ISNULL(@IgnoreChangePasswordValidation, 0);
	
	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------	
	DECLARE 
		@BusinessID BIGINT = NULL,
		@ApplicationID INT,
		@CredentialEntityTypeID INT,
		@Message VARCHAR(4000),
		@IsSuccess BIT = 1,
		@CredentialEntityID BIGINT,
		@IsException BIT = 0,
		@CredentialToken VARCHAR(50) = NULL,
		@DatePasswordExpires_Clone DATETIME = @DatePasswordExpires,
		@CredentialKey_Clone VARCHAR(50) = CASE WHEN @VerifyCredentialKeyOnly = 1 THEN @CredentialKey ELSE NULL END


	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialTypeKey = @CredentialEntityTypeKey,
		@CredentialKey = @CredentialKey_Clone,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityTypeID = @CredentialEntityTypeID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@IsOutputOnly = 1

	-- Debug
	--SELECT @ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @Username AS Username, 
	--	@Password AS Password, @CredentialEntityID AS CredentialEntityID, @CredentialEntityTypeID AS CredentialEntityTypeID

	--------------------------------------------------------------------------------------------
	-- CredentialEntityType Validation
	-- <Summary>
	-- If a credential key is provided then we know the exact user we need to update.
	-- Otherwise, we need to know the type of user we are to manipulate.
	-- </Summary>
	--------------------------------------------------------------------------------------------	
	IF @CredentialKey IS NULL AND @CredentialEntityTypeID IS NULL
	BEGIN
		SELECT
			@IsSuccess = 0,
			@Message = 'Unable to update the User''s password. The User type (e.g. Extranet, internet, etc) was not specified. Please ' +
				'specifiy the type of user you are attempting to update.'
		
		GOTO EOF;	
	END
	
	--------------------------------------------------------------------------------------------
	-- CredentialEntityType Validation
	-- <Summary>
	-- If a credential key is provided then we know the exact user we need to update.
	-- Otherwise, we need to know the type of user we are to manipulate.
	-- </Summary>
	--------------------------------------------------------------------------------------------	
	IF @CredentialKey IS NULL AND ISNULL(@VerifyCredentialKeyOnly, 0) = 1
	BEGIN
		SELECT
			@IsSuccess = 0,
			@Message = 'Unable to update the User''s password. Object reference (@CredentialKey) is not set to an instance of an object. ' +
				'The parameter, @CredentialKey, is required when using CredentialKey only verification.'
		
		GOTO EOF;
		
		RETURN;	
	END

	--------------------------------------------------------------------------------------------
	-- Verify business
	--------------------------------------------------------------------------------------------
	-- No business provided.
	IF dbo.fnIsNullOrEmpty(@BusinessKey) = 1
	BEGIN
		SELECT
			@IsSuccess = 0,
			@Message = 'Unable to update the User''s password.  Object reference (@BusinessKey) not set to an instance of an object. ' +
				'The parameter, @BusinessKey, cannot be null or empty.'
			
		GOTO EOF;
		
		RETURN;		
	END
	
	-- Unable to locate provided business.
	IF @BusinessID IS NULL
	BEGIN
		SELECT
			@IsSuccess = 0,
			@Message = 'Unable to update the User''s password. The provided business was not located.'
			
		GOTO EOF;
		
		RETURN;		
	END
	
	--------------------------------------------------------------------------------------------
	-- Ensure the confirmation password matches the existing password.
	--------------------------------------------------------------------------------------------	
	--IF @Password <> @ConfirmPassword
	--BEGIN
	
	--	SELECT
	--		@IsSuccess = 0,
	--		@Message = 'Unable to update the User''s password. The orginal password was not successfully confirmed.'
			
	--	GOTO EOF;			
	--END
	
	--------------------------------------------------------------------------------------------
	-- Ensure the new confirmation password property matches the newly provided password.
	--------------------------------------------------------------------------------------------	
	IF @NewPassword <> @ConfirmNewPassword
	BEGIN
	
		SELECT
			@IsSuccess = 0,
			@Message = 'Unable to update the User''s password. The new password was not successfully confirmed.'
			
		GOTO EOF;	
		
		RETURN;		
	END


	-- Debug
	--SELECT @CredentialEntityID AS CredentialEntityID, @VerifyCredentialKeyOnly AS VerifyCredentialKeyOnly

	--------------------------------------------------------------------------------------------
	-- Verify credentials
	-- <Summary>
	-- Executes the core verification stored procedure with the translated
	-- keys.
	-- </Summary>
	--------------------------------------------------------------------------------------------
	-- Verify a username/password combination.
	IF @CredentialEntityID IS NULL AND ISNULL(@VerifyCredentialKeyOnly, 0) = 0
	BEGIN
	
		EXEC creds.spCredentialEntity_Credentials_Verify
			@ApplicationID = @ApplicationID,
			@BusinessID = @BusinessID,
			@Username = @Username,
			@Password = @Password,
			@CredentialEntityTypeID = @CredentialEntityTypeID,
			@CredentialToken = @CredentialKey OUTPUT,
			@CredentialEntityID = @CredentialEntityID OUTPUT,
			@IgnoreLockedOutValidation = @IgnoreLockedOutValidation,
			@IgnoreDisabledValidation = @IgnoreDisabledValidation,
			@IgnoreChangePasswordValidation = @IgnoreChangePasswordValidation,
			@IsValid = @IsValid OUTPUT,
			-- Core membership properties ****************************
			@IsApproved = @IsApproved OUTPUT,
			@IsLockedOut = @IsLockedOut OUTPUT,
			@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
			@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
			@DatePasswordExpires = @DatePasswordExpires OUTPUT,
			@DateLastLockedOut = @DateLastLockedOut OUTPUT,
			@DateLockOutExpires = @DateLockOutExpires OUTPUT,		
			@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
			@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
			@IsDisabled = @IsDisabled OUTPUT,
			-- *******************************************************
			@IsChangePasswordRequired = @IsChangePasswordRequired OUTPUT
			
		--------------------------------------------------------------------------------------------
		-- Verify results
		-- <Summary>
		-- Inspect the results and determine the appropriate response to send to the caller.
		-- </Summary>
		--------------------------------------------------------------------------------------------
		IF ISNULL(@IsValid, 0) = 0
		BEGIN
		
			-- Build result items.	
			SELECT
				@IsSuccess = 0,
				@Message = 			
					CASE
						WHEN ISNULL(@IsLockedOut, 0) = 1 THEN 'Unable to update the User''s password. The account is currently locked.'
						WHEN ISNULL(@IsDisabled, 0) = 1 THEN 'Unable to update the User''s password. The account is currently disabled.'
						WHEN ISNULL(@VerifyCredentialKeyOnly, 0) = 1 THEN 'Unable to locate the provided User.'
						ELSE 'Unable to update the User''s password. The username and/or password is incorrect.'
					END;
		
		END

			
	END	
	--Verify a credential/authentication token or credential identifier.
	ELSE IF @CredentialEntityID IS NOT NULL AND ISNULL(@VerifyCredentialKeyOnly, 0) = 1
	BEGIN
		
		EXEC creds.spMembership_Get
			@CredentialEntityID = @CredentialEntityID,
			@IsApproved = @IsApproved OUTPUT,
			@IsLockedOut = @IsLockedOut OUTPUT,
			@IsDisabled = @IsDisabled OUTPUT
		
		-- debug
		--SELECT @IsApproved AS IsApproved, @IsLockedOut AS IsLockedOut, @IsDisabled AS IsDisabled
		
		-----------------------------------------------------------------------------------------------
		-- Inspect results and determine validity.
		-----------------------------------------------------------------------------------------------
		SET @IsValid = 
			CASE
				WHEN @IgnoreMembershipValidation = 1 THEN 1
				WHEN @IsLockedOut = 1 AND @IgnoreLockedOutValidation = 0 THEN 0
				WHEN @IsDisabled = 1 AND @IgnoreDisabledValidation = 0 THEN 0
				--WHEN @IsApproved = 0 AND @IgnoreIsApprovedValidation = 0 THEN 0
				ELSE 1
			END;
		
		-- Build result items.	
		SELECT
			@IsSuccess = CASE WHEN @IsValid = 1 THEN 1 ELSE 0 END,
			@Message = 			
				CASE
					WHEN @IsValid = 1 THEN @Message
					WHEN @IsLockedOut = 1 AND @IgnoreLockedOutValidation = 0 THEN 'Unable to update the User''s password. The account is currently locked.'
					WHEN @IsDisabled = 1 AND @IgnoreDisabledValidation = 0 THEN 'Unable to locate the provided User.'
					--WHEN @IsApproved = 0 AND @IgnoreIsApprovedValidation = 0 THEN 0
					ELSE 'Unable to update the User''s password. The User''s account is currently not allowing modifications.'
				END;
	
	END
	-- Unable to verify credential due to incomplete information.
	ELSE IF @CredentialEntityID IS NULL AND ISNULL(@VerifyCredentialKeyOnly, 0) = 1
	BEGIN

		SET @IsValid = 0;
		
		-- Build result items.	
		SELECT
			@IsSuccess = 0,
			@Message = 'Unable to update the User''s password. The user was unable to be located.'
			
	END
	-- Invalid catch all clause
	ELSE
	BEGIN
	
		SET @IsValid = 0;
		
		-- Build result items.	
		SELECT
			@IsSuccess = 0,
			@Message = 'Unable to update the User''s password. Not enough information provided.'
	
	END
	
	
	-- Debug
	--SELECT @CredentialEntityID AS CredentialEntityID, @VerifyCredentialKeyOnly AS VerifyCredentialKeyOnly, 
	--	@IsValid AS IsValid


	
	--------------------------------------------------------------------------------------------
	-- Update the user/credential's password.
	-- <Summary>
	-- If the user is considered valid via the verification process, then update the user's
	-- password; otherwise, inform the caller of the unsuccessful attempt.
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF ISNULL(@IsValid, 0) = 1	
	BEGIN
		EXEC creds.spMembership_Password_Update
			@CredentialEntityID = @CredentialEntityID,
			@Password = @NewPassword,
			@DatePasswordExpires = @DatePasswordExpires_Clone,
			@ModifiedBy = @ModifiedBy
				
		SELECT
			@Message = 'The user''s password was successfully updated.'
	END	


				
	--------------------------------------------------------------------------------------------
	-- Return results.
	--------------------------------------------------------------------------------------------	
	EOF:		
	SELECT
		@IsValid AS IsValid,
		@IsLockedOut AS IsLockedOut,
		@IsDisabled AS IsDisabled,
		@IsSuccess AS IsSuccess,
		@Message AS Message
	

	
END
