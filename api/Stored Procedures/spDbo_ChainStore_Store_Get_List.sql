﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/28/2016
-- Description:	Returns a list of stores that are applicable to the store and or chain.
-- SAMPLE CALL:
/*

DECLARE
	@StoreKey VARCHAR(MAX) = 'E3D4CE5A-8E79-422A-B294-E526BB17D190',
	@StoreKeyType VARCHAR(50) = NULL,
	@ChainKey VARCHAR(MAX) = NULL,
	@ChainKeyType VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_ChainStore_Store_Get_List
	@StoreKey = @StoreKey,
	@StoreKeyType = @StoreKeyType,
	@ChainKey = @ChainKey,
	@ChainKeyType = @ChainKeyType,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug

*/

-- SELECT * FROM dbo.vwStore
-- SElECT * FROM dbo.Store

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spDbo_ChainStore_Store_Get_List]
	@StoreKey VARCHAR(MAX) = NULL,
	@StoreKeyType VARCHAR(50) = NULL,
	@ChainKey VARCHAR(MAX) = NULL,
	@ChainKeyType VARCHAR(50) = NULL,
	@ServiceKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(MAX) = NULL,
	@MessageTypeKey VARCHAR(MAX) = NULL,
	@CommunicationMethodKey VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@StoreKey=' + dbo.fnToStringOrEmpty(@StoreKey) + ';' +
		'@StoreKeyType=' + dbo.fnToStringOrEmpty(@StoreKeyType) + ';' +
		'@ChainKey=' + dbo.fnToStringOrEmpty(@ChainKey) + ';' +
		'@ChainKeyType=' + dbo.fnToStringOrEmpty(@ChainKeyType) + ';' +
		'@ServiceKey=' + dbo.fnToStringOrEmpty(@ServiceKey) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@MessageTypeKey=' + dbo.fnToStringOrEmpty(@MessageTypeKey) + ';' +
		'@CommunicationMethodKey=' + dbo.fnToStringOrEmpty(@CommunicationMethodKey) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	/*
	------------------------------------------------------------------------------------------------------------------------
	-- Result schema binding
	-- <Summary>
	-- Returns a schema of the intended result set.  This is a workaround to assist certain
	-- frameworks in determining the correct result sets (i.e. SSIS, Entity Framework, etc.).
	-- </Summary>
	------------------------------------------------------------------------------------------------------------------------	
	IF 1 = 0 
	BEGIN
	
		DECLARE @Property VARCHAR(10) = '';
		
		SELECT TOP 1
			CAST(@Property AS VARCHAR(50)) AS BusinessEntityID,
			CAST(@Property AS VARCHAR(256)) AS Name,
			CAST(@Property AS VARCHAR(75)) AS SourceStoreID,
			CAST(@Property AS VARCHAR(75)) AS NABP,
			CAST(@Property AS VARCHAR(50)) AS BusinessToken,
			CAST(@Property AS VARCHAR(50)) AS BusinessNumber,
			CAST(@Property AS VARCHAR(50)) AS LicenseNumber,
			CAST(@Property AS VARCHAR(50)) AS SourceSystemKey,
			CAST(@Property AS VARCHAR(50)) AS ChainID,
			CAST(@Property AS VARCHAR(50)) AS ChainToken,
			CAST(@Property AS VARCHAR(256)) AS AddressLine1,
			CAST(@Property AS VARCHAR(256)) AS AddressLine2,
			CAST(@Property AS VARCHAR(256)) AS City,
			CAST(@Property AS VARCHAR(256)) AS State,
			CAST(@Property AS VARCHAR(256)) AS PostalCode,
			CAST(@Property AS VARCHAR(256)) AS Latitude,
			CAST(@Property AS VARCHAR(256)) AS Longitude,
			CAST(@Property AS VARCHAR(256)) AS Website,
			CAST(@Property AS VARCHAR(256)) AS PhoneNumber,
			CAST(@Property AS VARCHAR(256)) AS EmailAddress,
			CAST(@Property AS VARCHAR(256)) AS FaxNumber,
			CAST(@Property AS VARCHAR(256)) AS TimeZoneCode,
			CAST(@Property AS VARCHAR(256)) AS TimeZoneLocation,
			CAST(@Property AS INT) AS TimeZoneOffSet,
			CAST(@Property AS BIT) AS SupportDST
				
	END	
	*/


	------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------------------
	SET @StoreKey = NULLIF(@StoreKey, '');
	SET @ChainKey = NULLIF(@ChainKey, '');


	------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ChainID VARCHAR(MAX) = CASE WHEN @ChainKeyType IN ('Id', 'BusinessEntityID', 'BusinessID', 'BID') THEN @ChainKey ELSE NULL END,

		@SourceChainKey VARCHAR(MAX) = CASE WHEN @ChainKeyType IN ('SourceChainKey', 'SCK', 'SRC', 'SourceChainID', 'SCID') THEN @ChainKey ELSE NULL END,

		@ChainToken VARCHAR(MAX) = CASE WHEN @ChainKeyType IN ('ChainToken', 'BusinessToken', 'Token') 
			OR dbo.fnIsUniqueIdentifier(@ChainKey) = 1 THEN @ChainKey ELSE NULL END,

		@Nabp VARCHAR(MAX) =  CASE WHEN @StoreKeyType IN ('NABP', 'NCPDP', 'NCPDPID') THEN @StoreKey ELSE NULL END,

		@StoreID VARCHAR(MAX) = CASE WHEN @StoreKeyType IN ('Id', 'BusinessEntityID', 'BusinessID', 'BID') THEN @StoreKey ELSE NULL END,

		@StoreToken VARCHAR(MAX) = CASE WHEN @StoreKeyType IN ('StoreToken', 'BusinessToken', 'Token') 
			OR dbo.fnIsUniqueIdentifier(@StoreKey) = 1 THEN @StoreKey ELSE NULL END,

		@SourceStoreKey VARCHAR(MAX) = CASE WHEN @StoreKeyType IN ('SourceStoreKey', 'STk', 'SRC', 'SourceStoreID', 'SSID') THEN @StoreKey ELSE NULL END


	------------------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	------------------------------------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpChainIds') IS NOT NULL
	BEGIN
		DROP TABLE #tmpChainIds;
	END

	CREATE TABLE #tmpChainIds (
		Idx BIGINT IDENTITY(1,1),
		ChainID BIGINT
	);


	------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------------------
	-- Retrieve the ChainID list if there was no specific chain key provided.
	IF @ChainKey IS NULL
		AND (
			@Nabp IS NOT NULL 
				OR @SourceChainKey IS NOT NULL 
				OR @StoreToken IS NOT NULL
				OR @SourceStoreKey IS NOT NULL
		)
	BEGIN

		INSERT INTO #tmpChainIds (
			ChainId
		)
		SELECT
			cs.ChainID
		-- SELECT *
		-- SELECT TOP 1 *
		FROM dbo.vwChainStore cs
		WHERE (@Nabp IS NULL OR (@Nabp IS NOT NULL AND cs.Nabp IN (SELECT Value FROM dbo.fnSplit(@Nabp, ','))))
			AND (@StoreID IS NULL OR (@StoreID IS NOT NULL AND cs.StoreID IN (SELECT Value FROM dbo.fnSplit(@StoreID, ',') WHERE ISNUMERIC(Value) = 1)))
			AND (@StoreToken IS NULL OR (@StoreToken IS NOT NULL AND cs.StoreToken IN (SELECT Value FROM dbo.fnSplit(@StoreToken, ',') WHERE dbo.fnIsUniqueIdentifier(Value) = 1)))
			AND (@SourceStoreKey IS NULL OR (@SourceStoreKey IS NOT NULL AND cs.SourceStoreID IN (SELECT Value FROM dbo.fnSplit(@SourceStoreKey, ',') WHERE ISNUMERIC(Value) = 1)))
			
	END

	-- Debug
	-- SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get list of chains' AS ActionMethod, * FROM #tmpChainIds

	-- Retrieve the ChainID list if a specific chain (or chains) were provided.
	IF @ChainKey IS NOT NULL
		AND (
			 @ChainID IS NOT NULL 
				OR @SourceChainKey IS NOT NULL 
				OR @ChainToken IS NOT NULL
		)
	BEGIN

		INSERT INTO #tmpChainIds (
			ChainId
		)
		SELECT
			cs.ChainID
		-- SELECT *
		-- SELECT TOP 1 *
		FROM dbo.vwChainStore cs
		WHERE (@ChainID IS NULL OR (@ChainID IS NOT NULL AND cs.ChainID IN (SELECT Value FROM dbo.fnSplit(@ChainID, ',') WHERE ISNUMERIC(Value) = 1)))
			AND (@ChainToken IS NULL OR (@ChainToken IS NOT NULL AND cs.ChainToken IN (SELECT Value FROM dbo.fnSplit(@ChainToken, ',') WHERE dbo.fnIsUniqueIdentifier(Value) = 1)))
			AND (@SourceChainKey IS NULL OR (@SourceChainKey IS NOT NULL AND cs.SourceChainID IN (SELECT Value FROM dbo.fnSplit(@SourceChainKey, ',') WHERE ISNUMERIC(Value) = 1)))
			
	END

	-- Debug
	-- SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Review chains.' AS ActionMethod, * FROM #tmpChainIds


	SET @ChainID = (SELECT CONVERT(VARCHAR, ChainID) + ',' FROM #tmpChainIds FOR XML PATH('')); 


	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables.' AS ActionMethod,
			@StoreKey AS StoreKey, @StoreKeyType AS StoreKeyType, @ChainKey AS ChainKey, @ChainKeyType AS ChainKeyType,
			@StoreID AS StoreID, @Nabp AS Nabp, @StoreToken AS StoreToken, @SourceStoreKey AS SourceStoreKey,
			@ChainID AS ChainID, @ChainToken AS ChainToken, @SourceChainKey AS SourceChainKey
	END



	------------------------------------------------------------------------------------------------------------------------
	-- Return results.
	------------------------------------------------------------------------------------------------------------------------
	IF NULLIF(@ChainID, '') IS NOT NULL
	BEGIN

		EXEC api.spDbo_Store_Get_List
			@ChainKey = @ChainID,
			@ServiceKey = @ServiceKey,
			@ApplicationKey = @ApplicationKey,
			@MessageTypeKey = @MessageTypeKey,
			@CommunicationMethodKey = @CommunicationMethodKey,
			@Skip = @Skip,
			@Take = @Take,
			@SortCollection = @SortCollection

	END


END
