﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/31/2014
-- Description:	Indicates whether the credential entity is in the supplied role(s)
-- SAMPLE CALL:
/*

EXEC api.spCreds_CredentialEntity_IsInRole
	@CredentialKey = '193C7866-D54A-4474-8D80-25E861CABD10',
	@Role = 'MPCS',
	@ApplicationKey = '2',
	@BusinessKey = '2E9758B4-CBD0-4284-B390-232AB4EFF033'
	
EXEC api.spCreds_CredentialEntity_IsInRole
	@CredentialToken= '193C7866-D54A-4474-8D80-25E861CABD10',
	@Role = 'MPCS',
	@ApplicationKey = '2',
	@BusinessKey = '2E9758B4-CBD0-4284-B390-232AB4EFF033'
	
EXEC api.spCreds_CredentialEntityRole_Get_List
	@CredentialToken = '193C7866-D54A-4474-8D80-25E861CABD10',
	@ApplicationKey = '2',
	@BusinessToken= '2E9758B4-CBD0-4284-B390-232AB4EFF033'
	
*/

--SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
--SELECT * FROM dbo.InformationLog ORDER By InformationLogID DESC
--SELECT * FROM dbo.Application
--SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
--SELECT * FROM dbo.vwStore WHERE BusinessEntityID = 92
--SELECT * FROM creds.vwExtranetUser
--EXEC creds.spCredentialToken_Update @Token = '193C7866-D54A-4474-8D80-25E861CABD10', @ForceUpdate = 1
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialEntity_IsInRole]
	@CredentialKey VARCHAR(50) = NULL,
	@CredentialEntityID BIGINT = NULL,
	@CredentialToken VARCHAR(50) = NULL,
	@ApplicationKey VARCHAR(25) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	@Role VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CredentialToken=' + dbo.fnToStringOrEmpty(@CredentialToken) + ';' +
		'@Role=' + dbo.fnToStringOrEmpty(@Role) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = 'api.spCreds_CredentialEntity_IsInRole',
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, NULLIF(@BusinessKeyType, ''));
	
	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE 
		@Exists BIT = 0,
		@BusinessID BIGINT,
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@IsSuccess BIT = 1,
		@Message VARCHAR(4000),
		@ErrorMessage VARCHAR(4000)


	--------------------------------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF @CredentialEntityID IS NULL 
		AND dbo.fnIsNullOrWhiteSpace(@CredentialKey) = 1
		AND dbo.fnIsNullOrWhiteSpace(@CredentialToken) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to verify credential role(s).  Object reference is not set to an instance of an object. ' +
			'A credential token or identifier cannot be null or empty.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END

	BEGIN TRY
		
		--------------------------------------------------------------------------------------------
		-- Interogate business key and address accordingly.
		-- <Summary>
		-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
		-- business key type.  If a key type was not supplied then attempt a trial and error conversion
		-- based on token or NABP.
		-- </Summary>
		--------------------------------------------------------------------------------------------	
		-- Auto translater
		SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);	
		-- Busienss token
		SET @BusinessID = CASE WHEN @BusinessID IS NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'BusinessToken') ELSE @BusinessID END;
		-- NABP
		SET @BusinessID = CASE WHEN @BusinessID IS NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'NABP') ELSE @BusinessID END;
		-- Determine if it is a special scenario (i.e. all stores, etc.)
		SET @BusinessID = 
			CASE 
				WHEN @BusinessID IS NULL 
					AND ( ISNUMERIC(@BusinessKey) = 1 AND CONVERT(BIGINT, @BusinessKey) < 0) 
						THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'BusinessID') 
				ELSE @BusinessID 
			END;
		
		--------------------------------------------------------------------------------------------
		-- CredentialEntityID Parsing (if applicable)
		--------------------------------------------------------------------------------------------
		-- If a CredentialEntityID was not supplied, then perform logic based on a token.
		-- If a token is not provided then let's see if some key in general was supplied.
		IF @CredentialEntityID IS NULL
		BEGIN
		
			IF dbo.fnIsNullOrWhiteSpace(@CredentialToken) = 0
			BEGIN
				--------------------------------------------------------------------------------------------
				-- Validate token
				--------------------------------------------------------------------------------------------	
				EXEC creds.spCredentialToken_Update 
					@Token = @CredentialToken OUTPUT,
					@CredentialEntityID = @CredentialEntityID OUTPUT
			END
			ELSE
			BEGIN
				--------------------------------------------------------------------------------------------
				-- Interrogate key.
				-- <Summary>
				-- Inspect the key to determine if it is a unique identifier
				-- (authentication token) or if an actual CredentialEntityID has
				-- been supplied.
				-- </Summary>
				--------------------------------------------------------------------------------------------	
				IF dbo.fnIsNullOrWhiteSpace(@CredentialKey) = 0
				BEGIN
				
					-- If the key is a unique identifier then set as the CredentialToken
					-- and attempt an update on said token.
					IF dbo.fnIsUniqueIdentifier(@CredentialKey) = 1
					BEGIN
						SET @CredentialToken = @CredentialKey;
						
						--------------------------------------------------------------------------------------------
						-- Validate token
						--------------------------------------------------------------------------------------------	
						EXEC creds.spCredentialToken_Update 
							@Token = @CredentialToken OUTPUT,
							@CredentialEntityID = @CredentialEntityID OUTPUT
						
					END
					
					-- If a CredentialEntityID or CredentialToken was not resolved
					-- and the key is numeric, then assume it is a CredentialEntityID.
					IF ( @CredentialEntityID IS NULL OR @CredentialToken IS NULL ) 
						AND ISNUMERIC(@CredentialKey) = 1
					BEGIN
						SET @CredentialEntityID = @CredentialKey;
					END
				
				END
			
			
			END
			
		END
		
		-- Debug
		--SELECT @CredentialEntityID AS CredentialEntityID, @CredentialToken AS CrednetialToken, @ApplicationID AS ApplicationID, @BusinessID AS BusinessID
		/*
		-- Log mapping outcome
		SET @Args =
			'@CredentitalEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
			'@Role=' + dbo.fnToStringOrEmpty(@Role) + ';' +
			'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
			'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';'
			
		EXEC dbo.spLogInformation
			@Message = 'Inspecting mapping outcome...',
			@Arguments = @Args
		*/
				
		--------------------------------------------------------------------------------------------
		-- Determine if role exists
		--------------------------------------------------------------------------------------------
		EXEC acl.spCredentialEntity_IsInRole
			@CredentialEntityID = @CredentialEntityID,
			@Role = @Role,
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessID,
			@Exists = @Exists OUTPUT
		

	END TRY
	BEGIN CATCH
	
		EXEC dbo.spLogError;
		
		SET @IsSuccess = 0;
		
	END CATCH	
	
	
	--------------------------------------------------------------------------------------------
	-- Return results	
	--------------------------------------------------------------------------------------------
	SElECT
		@IsSuccess AS IsSuccess,
		@Exists AS HasRole,
		@CredentialToken AS CredentialToken,
		CONVERT(BIT, CASE WHEN @CredentialToken IS NOT NULL THEN 0 ELSE 1 END) AS IsCredentialTokenExpired
	
END
