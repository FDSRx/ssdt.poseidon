﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Gets a list of Task objects that are applicable to a patient.
-- CHANGE LOG:
-- 12/16/2014 - DRH - Modified the result query not to return "Invalid" task items.  Items that have been marked with
--		a status of invalid are technically to be ignored as they are erroneous entries or previous tasks that have been replaced
--		by a valid task.
-- 11/20/2015 - dhughes - Modified the "ROW_NUMBER()" method to only perform the process once vs. multiple scenarios to help increase
--							speed and performance.
-- 11/20/2015 - dhughes - Modified the process to generate the tag(s) XML information after the final record set has been determined.
-- 3/2/2016 - dhughes - Modified the procedure to utilize patient denormalized vs. the patient view to improve speed and performance.
--					Added the skeleton for handling parameter sniffing.
-- 3/11/2016 - dhughes - Modified the procedure to ignore "Auto In Progress" task status items.
-- 4/19/2016 - dhughes - Modified the procedure to NOT ignore "Auto" or "Auto In Progress" task status items.  These items will now
--							be managed via the client side.  Only invalids are still NOT returned.
-- 6/21/2016 - dhughes - Modified the procedure to look at the business stamped on the Task object vs. having to go to the Patient
--						object to narrow down the results.

-- SAMPLE CALL;
/*

DECLARE
	@ApplicationKey VARCHAR(50) = 'ENG',
	@BusinessKey VARCHAR(50) = '2501624',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Note object identifiers.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = 'Med Sync Fill, Med. Sync Fill', -- Single or comma delimited list of Tag object keys.
	@TagKey_IsNotAnyOf VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys that should not be returned.
	@DateDueStart DATETIME = '1/1/1900',
	@DateDueEnd DATETIME = DATEADD(dd, 1, GETDATE()),
	@DateCompletedStart DATETIME = NULL,
	@DateCompletedEnd DATETIME = NULL,
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@IsCompleted BIT = 0,
	@TaskStatusKey VARCHAR(MAX) = NULL,
	@TaskStatusKey_IsNotAnyOf VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC [api].[spPhrm_Patient_Task_Get_List]
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@NoteKey = @NoteKey,
	@NotebookKey = @NotebookKey,
	@PriorityKey = @PriorityKey,
	@OriginKey = @OriginKey,
	@TagKey = @TagKey,
	@TagKey_IsNotAnyOf = @TagKey_IsNotAnyOf,
	@DateDueStart = @DateDueStart,
	@DateDueEnd = @DateDueEnd,
	@DateCompletedStart = @DateCompletedStart,
	@DateCompletedEnd = @DateCompletedEnd,
	@DataDateStart = @DataDateStart,
	@DataDateEnd = @DataDateEnd,
	@IsCompleted = @IsCompleted,
	@TaskStatusKey = @TaskStatusKey,
	@TaskStatusKey_IsNotAnyOf = @TaskStatusKey_IsNotAnyOf,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug

	
*/

-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Task
-- SELECT * FROM dbo.vwTask
-- SELECT * FROM phrm.fnGetPatientInformation(2) 
-- SELECT * FROM dbo.NoteType

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Task_Get_List]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Note object identifiers.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@TagKey_IsNotAnyOf VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys that should not be returned.
	@DateDueStart DATETIME = NULL,
	@DateDueEnd DATETIME = NULL,
	@DateCompletedStart DATETIME = NULL,
	@DateCompletedEnd DATETIME = NULL,
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@IsCompleted BIT = NULL,
	@TaskStatusKey VARCHAR(MAX) = NULL,
	@TaskStatusKey_IsNotAnyOf VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN


	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	---------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' +
		'@NoteKey=' + dbo.fnToStringOrEmpty(@NoteKey) + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey) + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		'@TagKey=' + dbo.fnToStringOrEmpty(@TagKey) + ';' +
		'@TagKey_IsNotAnyOf=' + dbo.fnToStringOrEmpty(@TagKey_IsNotAnyOf) + ';' +
		'@DateDueStart=' + dbo.fnToStringOrEmpty(@DateDueStart) + ';' +
		'@DateDueEnd=' + dbo.fnToStringOrEmpty(@DateDueEnd) + ';' +
		'@DateCompletedStart=' + dbo.fnToStringOrEmpty(@DateCompletedStart) + ';' +
		'@DateCompletedEnd=' + dbo.fnToStringOrEmpty(@DateCompletedEnd) + ';' +
		'@DataDateStart=' + dbo.fnToStringOrEmpty(@DataDateStart) + ';' +
		'@DataDateEnd=' + dbo.fnToStringOrEmpty(@DataDateEnd) + ';' +
		'@IsCompleted=' + dbo.fnToStringOrEmpty(@IsCompleted) + ';' +
		'@TaskStatusKey=' + dbo.fnToStringOrEmpty(@TaskStatusKey) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	---------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	---------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	---------------------------------------------------------------------------------------------------------------------------
	-- Procedure bootstrapper.
	---------------------------------------------------------------------------------------------------------------------------
	DECLARE
		-- ** Add items to be excluded from the list here.
		@TaskStatusKey_IsNotAnyOf_Bootstrap VARCHAR(MAX) = dbo.fnTaskStatusKeyTranslator('INV', DEFAULT);

	-- debug
	IF @Debug = 1
	BEGIN
		SELECT 'DebuggerOn' AS DebugMode, @ProcedureName AS procedureName, 'Bootstrapper' AS ActionMethod,
			@TaskStatusKey_IsNotAnyOf_Bootstrap AS TaskStatusKey_IsNotAnyOf_Bootstrap
	END

	---------------------------------------------------------------------------------------------------------------------------
	-- Parameter Sniffing Fix
	-- <Summary>
	-- Saves a copy of the global input variables to a local variable and then the local
	-- variable is used to perform all the necessary tasks within the procedure.  This allows
	-- the optimizer to use statistics instead of a cached plan that might not be optimized
	-- for the sparatic queries.
	-- <Summary/>
	---------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationKey_Local VARCHAR(50) = @ApplicationKey,
		@BusinessKey_Local VARCHAR(50) = @BusinessKey,
		@BusinessKeyType_Local VARCHAR(50) = @BusinessKeyType,
		@PatientKey_Local VARCHAR(50) = @PatientKey,
		@PatientKeyType_Local VARCHAR(50) = @PatientKeyType,
		@NoteKey_Local VARCHAR(MAX) = @NoteKey, 
		@NotebookKey_Local VARCHAR(MAX) = @NotebookKey, 
		@PriorityKey_Local VARCHAR(MAX) = @PriorityKey,
		@OriginKey_Local VARCHAR(MAX) = @OriginKey, 
		@TagKey_Local VARCHAR(MAX) = @TagKey,
		@TagKey_IsNotAnyOf_Local VARCHAR(MAX) = @TagKey_IsNotAnyOf,
		@DateDueStart_Local DATETIME = @DateDueStart,
		@DateDueEnd_Local DATETIME = @DateDueEnd,
		@DateCompletedStart_Local DATETIME = @DateCompletedStart,
		@DateCompletedEnd_Local DATETIME = @DateCompletedEnd,
		@DataDateStart_Local DATETIME = @DataDateStart,
		@DataDateEnd_Local DATETIME = @DataDateEnd,
		@IsCompleted_Local BIT = @IsCompleted,
		@TaskStatusKey_Local VARCHAR(MAX) = @TaskStatusKey,
		@TaskStatusKey_IsNotAnyOf_Local VARCHAR(MAX) = @TaskStatusKey_IsNotAnyOf,
		@Skip_Local BIGINT = @Skip,
		@Take_Local BIGINT = @Take,
		@SortCollection_Local VARCHAR(MAX) = @SortCollection,
		@Debug_Local BIT = @Debug
		
	---------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------------------------------------------------------------
	SET @DataDateStart = ISNULL(@DataDateStart, '1/1/1900');
	SET @Debug = ISNULL(@debug, 0);
	
	---------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonKeyArray VARCHAR(MAX),
		@PatientKeyArray VARCHAR(MAX),
		@ScopeID INT = dbo.fnGetScopeID('INTRN'), -- only review internal notes
		@NoteTypeKey VARCHAR(25),
		@BusinessID BIGINT = NULL,
		@ApplicationID INT = NULL,
		@PatientPictureKey VARCHAR(256) = 'PatientPictureSmall',
		@TagKeyArray VARCHAR(MAX),
		@TagKeyArray_IsNotAnyOf VARCHAR(MAX),
		@TaskStatusIDArray VARCHAR(MAX) = NULL,
		@TaskStatusIDArray_IsNotAnyOf VARCHAR(MAX) = NULL
	
	---------------------------------------------------------------------------------------------------------------------------
	-- Set variables
	---------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------	
	-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
	-- business key type.  If a key type was not supplied then attempt a trial and error conversion
	-- based on token or NABP.
	---------------------------------------------------------------------------------------------------------------------------	
	-- Auto translater
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1
		
	SET @PatientKeyArray = phrm.fnPatientKeyTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKeyList'));
	
	--SET @PersonKeyArray = dbo.fnPersonIDTranslator(@PatientKey, 'PatientIDList');
	SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationKey);
	SET @NoteTypeKey = dbo.fnNoteTypeKeyTranslator('TSK', DEFAULT);
	SET @NotebookKey = dbo.fnNotebookKeyTranslator(@NotebookKey, DEFAULT);
	SET @PriorityKey = dbo.fnPriorityKeyTranslator(@PriorityKey, DEFAULT);
	SET @OriginKey = dbo.fnOriginKeyTranslator(@OriginKey, DEFAULT);
	SET @TagKeyArray = dbo.fnTagKeyTranslator(@TagKey, DEFAULT);
	SET @TagKeyArray_IsNotAnyOf = dbo.fnTagKeyTranslator(@TagKey_IsNotAnyOf, DEFAULT);
	SET @TaskStatusIDArray = dbo.fnTaskStatusKeyTranslator(@TaskStatusKey, DEFAULT);
	SET @TaskStatusIDArray_IsNotAnyOf = Poseidon.dbo.fnTaskStatusKeyTranslator(@TaskStatusKey_IsNotAnyOf, DEFAULT);
	
	IF @IsCompleted IS NOT NULL AND @IsCompleted = 1
	BEGIN
		SET @DateCompletedStart = ISNULL(@DateCompletedStart, '1/1/1900');
	END
	
	-- debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS procedureName, 'Get/Set variables.' AS ActionMethod,
			@ApplicationKey AS ApplicationKey, @ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey,
			@BusinessKeyType AS BueinessKeyType, @BusinessID AS BusinessID, @NoteTypeKey AS NoteTypeKey, 
			@NotebookKey AS NotebookKey, @PriorityKey AS PriorityKey, @TagKey AS TagKey, 
			@TagKeyArray AS TagKeyArray, @TagKeyArray_IsNotAnyOf AS TagKeyArray_IsNotAnyOf, @TaskStatusKey AS TaskStatusKey,
			@TaskStatusIDArray AS TaskStatusIDArray, @DateDueStart AS DateDueStart, @DateDueEnd AS DateDueEnd
	END


	---------------------------------------------------------------------------------------------------------------------------
	-- Configure paging setup.
	---------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)
			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (
		Idx INT, 
		Value VARCHAR(20)
	);
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	
	---------------------------------------------------------------------------------------------------------------------------
	-- Create result set
	---------------------------------------------------------------------------------------------------------------------------
	-- SELECT * FROM dbo.Note
	
	;WITH cteNotes AS (
		
		SELECT
			ROW_NUMBER() OVER (ORDER BY	
				CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC,		
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'asc'  THEN n.Title END  ASC,
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'desc'  THEN n.Title END DESC,
				CASE WHEN @SortField = 'DateDue' AND @SortDirection = 'asc'  THEN n.Title END  ASC,
				CASE WHEN @SortField = 'DateDue' AND @SortDirection = 'desc'  THEN n.Title END DESC,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'asc'  THEN n.DateCreated END ASC ,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'desc'  THEN n.DateCreated END DESC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'asc'  THEN n.DateModified END ASC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'desc'  THEN n.DateModified END DESC ,     
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'asc'  THEN n.CreatedBy END ASC,
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'desc'  THEN n.CreatedBy END DESC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'asc'  THEN n.ModifiedBy END ASC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'desc'  THEN n.ModifiedBy END  DESC,
				CASE WHEN tsk.DateDue IS NOT NULL THEN 1 ELSE 0 END ASC,
				tsk.DateDue DESC,
				n.NoteID DESC
			) AS RowNumber,
		--	CASE    
		--		WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
		--			THEN ROW_NUMBER() OVER (ORDER BY CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC, n.DateCreated ASC) 
		--		WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
		--			THEN ROW_NUMBER() OVER (ORDER BY CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC, n.DateCreated DESC)
		--		WHEN @SortField = 'Title' AND @SortDirection = 'asc'  
		--			THEN ROW_NUMBER() OVER (ORDER BY CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC, n.Title ASC) 
		--		WHEN @SortField = 'Title' AND @SortDirection = 'desc'  
		--			THEN ROW_NUMBER() OVER (ORDER BY CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC, n.Title DESC)  
		--		WHEN @SortField = 'DateDue' AND @SortDirection = 'asc'  
		--			THEN ROW_NUMBER() OVER (ORDER BY CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC, tsk.DateDue ASC) 
		--		WHEN @SortField = 'DateDue' AND @SortDirection = 'desc'  
		--			THEN ROW_NUMBER() OVER (ORDER BY CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC, tsk.DateDue DESC)  
		--		ELSE ROW_NUMBER() OVER (
		--			ORDER BY 
		--				CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC,
		--				CASE WHEN tsk.DateDue IS NOT NULL THEN 1 ELSE 0 END ASC,
		--				tsk.DateDue DESC,
		--				n.NoteID DESC
		--		)
		--	END AS RowNumber,	
			tsk.NoteID AS TaskID,
			pat.StoreID AS BusinessID,
			pat.PatientID,
			pat.SourcePatientKey,
			pat.PersonID,
			pat.FirstName,
			pat.LastName,
			pat.BirthDate,
			pat.HomePhone,
			pat.IsCallAllowedToHomePhone,
			pat.IsTextAllowedToHomePhone,
			pat.IsHomePhonePreferred,
			pat.MobilePhone,
			pat.IsCallAllowedToMobilePhone,
			pat.IsTextAllowedToMobilePhone,
			pat.IsMobilePhonePreferred,
			pat.PrimaryEmail,
			pat.IsEmailAllowedToPrimaryEmail,
			pat.IsPrimaryEmailPreferred,
			pat.AlternateEmail,
			pat.IsEmailAllowedToAlternateEmail,
			pat.IsAlternateEmailPreferred,
			--(
			--	SELECT TOP 1 
			--		ImageID 
			--	FROM dbo.Images img
			--	WHERE img.ApplicationID = @ApplicationID
			--		AND pat.BusinessEntityID = img.BusinessID
			--		AND pat.PersonID = img.PersonID
			--		AND img.KeyName = @PatientPictureKey 
			--) AS PatientImageID,			
			n.NotebookID AS NotebookID,
			nb.Code AS NotebookCode,
			nb.Name AS NotebookName,
			n.Title,
			n.Memo,
			--(
			--	SELECT t.TagID AS Id, t.Name AS Name
			--	FROM dbo.NoteTag ntag
			--		JOIN dbo.Tag t
			--			ON ntag.TagID = t.TagID
			--	WHERE ntag.NoteID = n.NoteID
			--	FOR XML PATH('Tag'), ROOT('Tags')
			--) AS Tags,
			n.PriorityID,
			p.Code AS PriorityCode,
			p.Name AS PriorityName,
			n.OriginID,
			ds.Code AS OriginCode,
			ds.Name AS OriginName,
			n.OriginDataKey,
			tsk.DateDue,
			tsk.DateCompleted,
			tsk.CompletedByString,
			n.AdditionalData AS AdditionalData,
			n.CorrelationKey,
			tsk.TaskStatusID,
			stat.Code AS TaskStatusCode,
			stat.Name AS TaskStatusName,
			tsk.DateCreated,
			tsk.DateModified,
			tsk.CreatedBy,
			tsk.ModifiedBy	
		--SELECT *		
		FROM dbo.Task tsk  -- SELECT * FROM dbo.Task
			JOIN dbo.Note n -- SELECT * FROM dbo.Note
				ON n.NoteID = tsk.NoteID
			JOIN phrm.PatientDenormalized pat -- SELECT * FROM phrm.PatientDenormalized
				ON pat.StoreID = tsk.BusinessID
					AND pat.PersonID = tsk.BusinessEntityID
			LEFT JOIN dbo.NoteType nt
				ON n.NoteTypeID = nt.NoteTypeID
			LEFT JOIN dbo.Priority p
				ON n.PriorityID = p.PriorityID
			LEFT JOIN dbo.Notebook nb
				ON n.NotebookID = nb.NotebookID	
			LEFT JOIN dbo.Origin ds
				ON n.OriginID = ds.OriginID
			LEFT JOIN dbo.TaskStatus stat
				ON tsk.TaskStatusID = stat.TaskStatusID
		WHERE tsk.BusinessID = @BusinessID
			--pat.StoreID = @BusinessID
			AND n.ScopeID = @ScopeID
			AND ( @PatientKey IS NULL OR (@PatientKey IS NOT NULL AND pat.PatientID IN (SELECT Value FROM dbo.fnSplit(@PatientKeyArray, ',') WHERE ISNUMERIC(Value) = 1))) 
			AND ( @NoteKey IS NULL OR (@NoteKey IS NOT NULL AND tsk.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteKey, ',') WHERE ISNUMERIC(Value) = 1)))				
			AND ( @NoteTypeKey IS NULL OR (@NoteTypeKey IS NOT NULL AND n.NoteTypeID IN (SELECT Value FROM dbo.fnSplit(@NoteTypeKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @NotebookKey IS NULL OR (@NotebookKey IS NOT NULL AND n.NotebookID IN (SELECT Value FROM dbo.fnSplit(@NotebookKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @PriorityKey IS NULL OR (@PriorityKey IS NOT NULL AND n.PriorityID IN (SELECT Value FROM dbo.fnSplit(@PriorityKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @OriginKey IS NULL OR (@OriginKey IS NOT NULL AND n.OriginID IN (SELECT Value FROM dbo.fnSplit(@OriginKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @TagKey IS NULL OR (@TagKey IS NOT NULL AND EXISTS(
				SELECT TOP 1 *
				FROM NoteTag tmp
				WHERE tsk.NoteID = tmp.NoteID
					AND TagID IN(SELECT Value FROM dbo.fnSplit(@TagKeyArray, ',') WHERE ISNUMERIC(Value) = 1))
			))
			AND ( @TagKey_IsNotAnyOf IS NULL OR (@TagKey_IsNotAnyOf IS NOT NULL AND NOT EXISTS (
				SELECT TOP 1 *
				FROM NoteTag tmp
				WHERE tsk.NoteID = tmp.NoteID
					AND TagID IN(SELECT Value FROM dbo.fnSplit(@TagKeyArray_IsNotAnyOf, ',') WHERE ISNUMERIC(Value) = 1))
			))
			AND ( @DataDateEnd IS NULL OR (@DataDateEnd IS NOT NULL AND (
				tsk.DateCreated >= @DataDateStart
					AND tsk.DateCreated < @DataDateEnd
			)))
			AND ( @DateDueEnd IS NULL OR tsk.DateDue IS NULL OR (
				tsk.DateDue >= @DateDueStart
					AND tsk.DateDue < @DateDueEnd
			))			
			AND (@IsCompleted IS NULL OR ( 
				(
					@IsCompleted = 1 
					AND tsk.DateCompleted IS NOT NULL
					AND (@DateCompletedEnd IS NULL OR (
						tsk.DateCompleted >= @DateCompletedStart
						AND tsk.DateCompleted < @DateCompletedEnd
					))
				) OR 
				(
					@IsCompleted = 0 
					AND tsk.DateCompleted IS NULL
				)
			))
			-- Do not include invalid tasks in the data
		AND ( 
			( tsk.TaskStatusID IS NULL OR tsk.TaskStatusID NOT IN (SELECT Value FROM Poseidon.dbo.fnSplit(@TaskStatusKey_IsNotAnyOf_Bootstrap, ',') WHERE ISNUMERIC(Value) = 1))
			AND ( @TaskStatusKey_IsNotAnyOf IS NULL OR ( @TaskStatusKey_IsNotAnyOf IS NOT NULL AND tsk.TaskStatusID NOT IN (SELECT Value FROM Poseidon.dbo.fnSplit(@TaskStatusIDArray_IsNotAnyOf, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @TaskStatusKey IS NULL OR (@TaskStatusKey IS NOT NULL AND tsk.TaskStatusID IN (SELECT Value FROM dbo.fnSplit(@TaskStatusIDArray, ',') WHERE ISNUMERIC(Value) = 1)))
		)
	)
	
	-- Fetch results from common table expression
	SELECT
		TaskID,
		PatientID,
		SourcePatientKey,
		FirstName,
		LastName,
		BirthDate,
		HomePhone,
		IsCallAllowedToHomePhone,
		IsTextAllowedToHomePhone,
		IsHomePhonePreferred,
		MobilePhone,
		IsCallAllowedToMobilePhone,
		IsTextAllowedToMobilePhone,
		IsMobilePhonePreferred,
		PrimaryEmail,
		IsEmailAllowedToPrimaryEmail,
		IsPrimaryEmailPreferred,
		AlternateEmail,
		IsEmailAllowedToAlternateEmail,
		IsAlternateEmailPreferred,
		(
			SELECT TOP 1 
				ImageID 
			FROM dbo.Images img
			WHERE img.ApplicationID = @ApplicationID
				AND img.BusinessID = res.BusinessID
				AND img.PersonID = res.PersonID
				AND img.KeyName = @PatientPictureKey 
		) AS PatientImageID,	
		NotebookID,
		NotebookCode,
		NotebookName,
		Title,
		Memo,
		(
			SELECT t.TagID AS Id, t.Name AS Name
			FROM dbo.NoteTag ntag
				JOIN dbo.Tag t
					ON ntag.TagID = t.TagID
			WHERE ntag.NoteID = res.TaskID
			FOR XML PATH('Tag'), ROOT('Tags')
		) AS Tags,
		--CONVERT(XML, Tags) AS Tags,
		PriorityID,
		PriorityCode,
		PriorityName,
		OriginID,
		OriginCode,
		OriginName,
		OriginDataKey,
		DateDue,
		DateCompleted,
		CompletedByString,
		AdditionalData,
		CorrelationKey,
		TaskStatusID,
		TaskStatusCode,
		TaskStatusName,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteNotes) AS TotalRecordCount
	FROM cteNotes res
	WHERE RowNumber > @Skip AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
	
	
	
	
	
	
	/*
	---------------------------------------------------------------------------------------------------------------------------
	-- Return patient Task objects.
	---------------------------------------------------------------------------------------------------------------------------
	EXEC api.spDbo_Task_Get_List
		@NoteKey = @NoteKey,
		@ScopeKey = @ScopeID,
		@BusinessEntityKey = @PersonID,
		@NotebookKey = @NotebookKey,
		@PriorityKey = @PriorityKey,
		@OriginKey = @OriginKey,
		@TagKey = @TagKey,
		@DataDateStart = @DataDateStart,
		@DataDateEnd = @DataDateEnd,
		@Skip = @Skip,
		@Take = @Take,
		@SortCollection = @SortCollection
	*/
	
	
	
	
			
END
