﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/7/2014
-- Description:	Translates the input into their appropriate system keys.
-- SAMPLE CALL:
/*

EXEC api.spDbo_System_KeyTranslator
	@ApplicationKey = 'ESMT',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@RoleKey = 'ADMIN',
	@IsOutputOnly = false

*/
-- =============================================
CREATE PROCEDURE [api].[spDbo_System_KeyTranslator]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@CredentialKey VARCHAR(50) = NULL,
	@CredentialTypeKey VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonTypeKey VARCHAR(50) = NULL,
	@RoleKey VARCHAR(50) = NULL,
	@CredentialProviderKey VARCHAR(50) = NULL,
	@ApplicationID INT = NULL OUTPUT,
	@BusinessID BIGINT = NULL OUTPUT,
	@PersonID BIGINT = NULL  OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@CredentialEntityTypeID INT = NULL OUTPUT,
	@CredentialToken VARCHAR(50) = NULL OUTPUT,
	@PatientID BIGINT = NULL OUTPUT,
	@CustomerID BIGINT = NULL OUTPUT,
	@RoleID INT = NULL OUTPUT,
	@RoleIDArray VARCHAR(MAX) = NULL OUTPUT,
	@CredentialProviderID INT = NULL OUTPUT,
	@IsOutputOnly BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @ApplicationID = NULL;
	SET @BusinessID = NULL;
	SET @PersonID = NULL;
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	
	
	--------------------------------------------------------------------------------------------
	-- Translate keys.
	-- <Summary>
	-- Translates the specified keys into their applicable system identifiers.
	-- <Summary>
	--------------------------------------------------------------------------------------------
	
	
	IF LTRIM(RTRIM(ISNULL(@ApplicationKey, ''))) <> ''
	BEGIN
		-- Translate the application key.
		SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationKey);
	END
	
	
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
	-- business key type.  If a key type was not supplied then attempt a trial and error conversion
	-- based on token or NABP.
	--------------------------------------------------------------------------------------------
	IF LTRIM(RTRIM(ISNULL(@BusinessKey, ''))) <> ''
	BEGIN
	
		-- Auto translater
		SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);	
		-- Busienss token
		SET @BusinessID = CASE WHEN @BusinessID IS NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'BusinessToken') ELSE @BusinessID END;
		-- NABP
		SET @BusinessID = CASE WHEN @BusinessID IS NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'NABP') ELSE @BusinessID END;
		-- Determine if it is a special scenario (i.e. all stores, etc.)
		SET @BusinessID = 
			CASE 
				WHEN @BusinessID IS NULL 
					AND ( ISNUMERIC(@BusinessKey) = 1 AND CONVERT(BIGINT, @BusinessKey) < 0) 
						THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'BusinessID') 
				ELSE @BusinessID 
			END;
			
	END
		
	--------------------------------------------------------------------------------------------
	-- Translate credential key.
	-- <Summary>
	-- Inspect the key to determine if it is a unique identifier
	-- (authentication token) or if an actual CredentialEntityID has
	-- been supplied.
	-- </Summary>
	--------------------------------------------------------------------------------------------
	-- Translate type of key (if applicable).
	IF LTRIM(RTRIM(ISNULL(@CredentialTypeKey, ''))) <> ''
	BEGIN
		SET @CredentialEntityTypeID = creds.fnGetCredentialEntityTypeID(@CredentialTypeKey);
	END
	
	
	-- Translate CredentialToken (if provided).
	-- <Remarks>
	-- CredentialKey will truncate/trump CredentialToken in terms of precedence.
	-- CredentialKey will be determined as the priamry credential identifier to inspect
	-- and can/will potentially void a CredentialToken if both are supplied at the same time.
	-- </Remarks>
	IF dbo.fnIsNullOrWhiteSpace(@CredentialToken) = 0
	BEGIN
		--------------------------------------------------------------------------------------------
		-- Validate token
		--------------------------------------------------------------------------------------------	
		EXEC creds.spCredentialToken_Update	
			@Token = @CredentialToken OUTPUT,
			@CredentialEntityID = @CredentialEntityID OUTPUT
			--@ApplicationID = @ApplicationID OUTPUT,
			--@BusinessEntityID = @BusinessID OUTPUT
			
	END
	
	-- Translate credential key.	
	IF dbo.fnIsNullOrWhiteSpace(@CredentialKey) = 0
	BEGIN
			
		-- If the key is a unique identifier then set as the CredentialToken
		-- and attempt an update on said token.
		IF dbo.fnIsUniqueIdentifier(@CredentialKey) = 1
		BEGIN
			SET @CredentialToken = @CredentialKey;
			
			--------------------------------------------------------------------------------------------
			-- Validate token
			--------------------------------------------------------------------------------------------	
			EXEC creds.spCredentialToken_Update 
				@Token = @CredentialToken OUTPUT,
				@CredentialEntityID = @CredentialEntityID OUTPUT
				--@ApplicationID = @ApplicationID OUTPUT,
				--@BusinessEntityID = @BusinessID OUTPUT
			
		END
		
		-- If a CredentialEntityID or CredentialToken was not resolved
		-- and the key is numeric, then assume it is a CredentialEntityID.
		IF ( @CredentialEntityID IS NULL OR @CredentialToken IS NULL ) 
			AND ISNUMERIC(@CredentialKey) = 1
		BEGIN
			SET @CredentialEntityID = (SELECT CredentialEntityID FROM creds.CredentialEntity WHERE CredentialEntityID = CONVERT(INT, @CredentialKey) );
		END
		
		IF @CredentialEntityID IS NOT NULL
		BEGIN
			SET @PersonID = dbo.fnPersonIDTranslator(@CredentialEntityID, 'CredentialEntityID');	
		END
	
	END
	
	
	--------------------------------------------------------------------------------------------
	-- Fetch patient (if applicable).
	--------------------------------------------------------------------------------------------
	IF LTRIM(RTRIM(ISNULL(@PatientKey, ''))) <> ''
	BEGIN
	
		SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	
		SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');	
		
	END
	
	--------------------------------------------------------------------------------------------
	-- Translate roles
	--------------------------------------------------------------------------------------------
	IF dbo.fnIsNullOrWhiteSpace(@RoleKey) = 0
	BEGIN
		SET @RoleIDArray = acl.fnRoleKeyTranslator(@RoleKey, DEFAULT);
		
		SET @RoleID = 
			CASE 
				WHEN CHARINDEX(@RoleIDArray, ',') = 0 AND ISNUMERIC(@RoleIDArray) = 1 THEN @RoleIDArray 
				ELSE NULL 
			END;
	END
	
	--------------------------------------------------------------------------------------------
	-- Translate the credential provider.
	--------------------------------------------------------------------------------------------
	IF dbo.fnIsNullOrWhiteSpace(@CredentialProviderKey) = 0
	BEGIN
		SET @CredentialProviderID = creds.fnGetCredentialProviderID(@CredentialProviderKey);
	END
	
	
	--------------------------------------------------------------------------------------------
	-- Return results (if applicable)
	--------------------------------------------------------------------------------------------
	IF @IsOutputOnly = 0
	BEGIN
	
		SELECT
			@ApplicationID AS ApplicationID,
			@BusinessID AS BusinessID,
			@PersonID AS PersonID,
			@CredentialEntityID AS CredentialEntityID,
			@PatientID AS PatientID,
			@RoleID AS RoleID,
			@CredentialProviderID AS CredentialProviderID
			
	END
	
	
	

END
