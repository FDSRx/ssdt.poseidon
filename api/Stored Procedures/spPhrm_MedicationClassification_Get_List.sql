﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/29/2014
-- Description:	Returns a list of MedicationClassification items.
-- SAMPLE CALL: api.spDbo_ContactType_Get_List
-- =============================================
CREATE PROCEDURE [api].[spPhrm_MedicationClassification_Get_List]
	@MedicationClassificationID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------
	-- Return MedicationClassification objects
	-----------------------------------------------------------------
	SELECT
		MedicationClassificationID,
		Code,
		Name,
		Description,
		DateCreated
	FROM phrm.MedicationClassification
	WHERE ( @MedicationClassificationID IS NULL OR MedicationClassificationID IN (SELECT Value FROM dbo.fnSplit(@MedicationClassificationID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	ORDER BY Name
	
	
END
