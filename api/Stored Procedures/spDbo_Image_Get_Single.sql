﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/15/2014
-- Description:	Returns a single image.
-- SAMPLE CALL: api.spDbo_Image_Get_Single @ImageID = 1 

-- SELECT * FROM dbo.Images
-- =============================================
CREATE PROCEDURE api.spDbo_Image_Get_Single
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@ImageID BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------
	-- Return image data.
	-----------------------------------------------------------------------
	EXEC api.spDbo_Image_Get_List
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@PersonKey = @PersonKey,
		@ImageKey = @ImageID,
		@Take = 1
	
END
