﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/14/2015
-- Description:	Returns a single MessageType object and its parameters.
-- SAMPLE CALL:
/*

-- Identifier.
EXEC api.spComm_MessageTypeParameter_Get_Single
	@ApplicationKey = 'ENG',
	@MessageTypeKey = 'QC1',
	@ParameterKey = 1
	
-- Identifier.
EXEC api.spComm_MessageTypeParameter_Get_Single
	@ApplicationKey = 'ENG',
	@BusinessKey = '49',
	@BusinessKeyType = 'Id',
	@MessageTypeKey = 'QC1'

*/
-- SELECT * FROM comm.MessageTypeParameter
-- SELECT * FROM data.Parameter
-- =============================================
CREATE PROCEDURE [api].[spComm_MessageTypeParameter_Get_Single]
	@MessageTypeParamterKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@EntityKey VARCHAR(50) = NULL,
	@EntityKeyType VARCHAR(50) = NULL,
	@MessageTypeKey VARCHAR(50) = NULL,
	@ParameterKey VARCHAR(50) = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Result schema binding
	-- <Summary>
	-- Returns a schema of the intended result set.  This is a workaround to assist certain
	-- frameworks in determining the correct result sets (i.e. SSIS, Entity Framework, etc.).
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF (1 = 2)
	BEGIN
		DECLARE
			@Property VARCHAR(MAX) = NULL;

		SELECT 
			CONVERT(BIGINT, @Property) AS MessageTypeParamterID,
			CONVERT(INT, @Property) AS ApplicationID,
			CONVERT(VARCHAR(50), @Property) AS ApplicationCode,
			CONVERT(VARCHAR(50), @Property) AS ApplicationName,
			CONVERT(BIGINT, @Property) AS BusinessID,
			CONVERT(BIGINT, @Property) AS EntityID,
			CONVERT(INT, @Property) AS MessageTypeID,
			CONVERT(VARCHAR(50), @Property) AS MessageTypeCode,
			CONVERT(VARCHAR(50), @Property) AS MessageTypeName,
			CONVERT(VARCHAR(1000), @Property) AS MessageTypeDescription,
			CONVERT(INT, @Property) AS ParameterID,
			CONVERT(VARCHAR(50), @Property) AS ParameterCode,
			CONVERT(VARCHAR(50), @Property) AS ParameterName,
			CONVERT(VARCHAR(1000), @Property) AS ParameterDescription,
			CONVERT(VARCHAR(MAX), @Property) AS ValueString,
			CONVERT(VARCHAR(50), @Property) AS TypeOf,
			CONVERT(UNIQUEIDENTIFIER, @Property) AS rowguid,
			CONVERT(DATETIME, @Property) AS DateCreated,
			CONVERT(DATETIME, @Property) AS DateModified,
			CONVERT(VARCHAR(256), @Property) AS CreatedBy,
			CONVERT(VARCHAR(256), @Property) AS ModifiedBy,
			CONVERT(BIGINT, @Property) AS TotalRecords
	END

	-------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	

	-------------------------------------------------------------------------------------
	-- Record request.
	-------------------------------------------------------------------------------------	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@MessageTypeParamterKey=' + dbo.fnToStringOrEmpty(@MessageTypeParamterKey) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@EntityKey=' + dbo.fnToStringOrEmpty(@EntityKey) + ';' +
		'@EntityKeyType=' + dbo.fnToStringOrEmpty(@EntityKeyType) + ';' +
		'@ParameterKey=' + dbo.fnToStringOrEmpty(@ParameterKey) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	

	-------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------
	DECLARE
		@MessageTypeID INT = comm.fnGetMessageTypeID(@MessageTypeKey),
		@ParameterID INT = data.fnGetParameterID(@ParameterKey)
	
	-- Debug
	-- SELECT @MessageTypeID AS MessageTypeID, @ParameterID AS ParameterID
	
	-------------------------------------------------------------------------------------
	-- Return object.
	-------------------------------------------------------------------------------------
	IF @MessageTypeID IS NOT NULL
	BEGIN
	
		EXEC api.spComm_MessageTypeParameter_Get_List
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessKey,
			@BusinessKeyType = @BusinessKeyType,
			@EntityKey = @EntityKey,
			@EntityKeyType = @EntityKeyType,
			@MessageTypeKey = @MessageTypeID,
			@ParameterKey = @ParameterID		
	END
	
END
