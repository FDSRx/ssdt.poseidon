﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/10/2014
-- Description:	Returns a list of services that are applicable to a patient.
-- SAMPLE CALL: 
/*

EXEC api.spPhrm_Patient_Service_Get_List 
	@BusinessKey = '7871787', 
	@PatientKey = '21656'

*/

-- SELECT * FROM phrm.vwPatient WHERE LastName LIKE '%Simmons%'
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Service_Get_List]
	@BusinessKey VARCHAR(50),
	@PatientKey VARCHAR(50),
	@ServiceKey VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@LanguageKey VARCHAR(5) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PersonID BIGINT,
		@PatientID BIGINT,
		@ServiceID INT
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, 'NABP', @PatientKey, 'SourcePatientKey');
	
	SET @PersonID = (SELECT TOP 1 PersonID FROM phrm.Patient WHERE PatientID = @PatientID);
	
	SET @ServiceID = dbo.fnGetServiceID(@ServiceKey);
	
	---------------------------------------------------------------------
	-- Retrieve patient service list.
	---------------------------------------------------------------------	
	EXEC api.spDbo_BusinessEntityService_Get_List
		@BusinessEntityID = @PersonID,
		@ServiceID = @ServiceID,
		@Skip = @Skip,
		@Take = @Take,
		@SortCollection = @SortCollection
	

END
