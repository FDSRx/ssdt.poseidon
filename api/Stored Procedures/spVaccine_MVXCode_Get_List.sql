﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/22/2014
-- Description:	Returns a list of MVXCode items.
-- SAMPLE CALL: api.spVaccine_MVXCode_Get_List
-- =============================================
CREATE PROCEDURE [api].spVaccine_MVXCode_Get_List
	@MVXCodeID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		ref.MVXCodeID,
		ref.Code,
		ref.Name,
		ref.Status,
		ref.Notes,
		ref.DateLastUpdated,
		ref.DateCreated,
		ref.DateModified
	--SELECT *
	FROM vaccine.MVXCode ref
	WHERE ( @MVXCodeID IS NULL OR ref.MVXCodeID IN (SELECT Value FROM dbo.fnSplit(@MVXCodeID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR ref.Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	
END
