﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/28/2014
-- Description:	Validates/updates a credential token
-- SAMPLE CALL:
/*

DECLARE 
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@Token VARCHAR(50) = '452718CF-CE57-4B15-94A4-1B3DFA142BCA',
	@IsBusinessVerificationRequired BIT = NULL,
	@SetAsyncLock BIT = 1,
	@Debug BIT = 1

EXEC api.[spCreds_CredentialToken_Authenticate]
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@Token = @Token,
	@IsBusinessVerificationRequired = @IsBusinessVerificationRequired,
	@SetAsyncLock = @SetAsyncLock,
	@Debug = @Debug

*/

-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
-- SELECT * FROM dbo.Configuration

-- SELECT * FROM data.ProcessLock
-- TRUNCATE TABLE data.ProcessLock

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC 
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialToken_Authenticate]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@Token VARCHAR(50),
	@IsBusinessVerificationRequired BIT = NULL,
	@SetAsyncLock BIT = 0,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------------------------------------------
	-- Instance variables.
	----------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@Token=' + dbo.fnToStringOrEmpty(@Token) + ';' +
		'@IsBusinessVerificationRequired=' + dbo.fnToStringOrEmpty(@IsBusinessVerificationRequired) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;
	
	----------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------------------------------
	SET @IsBusinessVerificationRequired = ISNULL(@IsBusinessVerificationRequired, 0);
	SET @Debug = ISNULL(@Debug, 0);
	SET @SetAsyncLock = ISNULL(@SetAsyncLock, 0);
	
	
	----------------------------------------------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------------------------------------------
	DECLARE
		@IsExpired BIT,
		@DateExpires DATETIME,
		@BusinessEntityID BIGINT,
		@BusinessID BIGINT,
		@ApplicationID INT,
		@LockToken UNIQUEIDENTIFIER = NULL


	BEGIN TRY

		IF @SetAsyncLock = 1
		BEGIN

			----------------------------------------------------------------------------------------------------------
			-- Create a lock on the record to stop contention.
			----------------------------------------------------------------------------------------------------------
			DECLARE
				@ProcessKey VARCHAR(256) = 'SID_' + ISNULL(@Token, CONVERT(VARCHAR(256), NEWID())),
				@MaxAcquireLockTimeInSeconds INT = 30,
				@Now DATETIME = GETDATE(),
				@CurrentNumberOfSeconds INT = 0
					
			-- Create a lock on the record.
			EXEC data.spProcessLock_Create 
				@ProcessName = @ProcedureName,
				@ProcessKey = @ProcessKey,
				@LockToken = @LockToken OUTPUT

			WHILE @LockToken IS NULL 
				AND @CurrentNumberOfSeconds < @MaxAcquireLockTimeInSeconds
			BEGIN

				WAITFOR DELAY '00:00:00.001' 

				--EXEC dbo.spLogInformation
				--	@Message = 'Could not acquire lock on first try.',
				--	@Arguments = @Token 

				-- Create a lock on the record.
				EXEC data.spProcessLock_Create 
					@ProcessName = @ProcedureName,
					@ProcessKey = @ProcessKey,
					@LockToken = @LockToken OUTPUT

				SET @CurrentNumberOfSeconds = DATEDIFF(ss, @Now, GETDATE());

			END

			IF @CurrentNumberOfSeconds >= @MaxAcquireLockTimeInSeconds
			BEGIN

				SET @ErrorMessage = 'Unable to acquire a lock on authorization token (SID), ' + @Token +
					' A timeout has been encountered.  A lock could not be acquired within ' + @MaxAcquireLockTimeInSeconds +
					' seconds.';
		
				-- throw error.
				RAISERROR (
					@ErrorMessage, -- Message text.
					16, -- Severity.
					1 -- State.
				);
			END

		END


		----------------------------------------------------------------------------------------------------------
		-- Set variables
		----------------------------------------------------------------------------------------------------------	
		-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
		-- business key type.  If a key type was not supplied then attempt a trial and error conversion
		-- based on token or NABP.
		
		-- Translate system key variables using the system translator.
		EXEC api.spDbo_System_KeyTranslator
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessKey,
			@BusinessKeyType = @BusinessKeyType,
			@ApplicationID = @ApplicationID OUTPUT,
			@BusinessID = @BusinessID OUTPUT,
			@IsOutputOnly = 1
			
		----------------------------------------------------------------------------------------------------------
		-- Validate token.
		----------------------------------------------------------------------------------------------------------	
		EXEC creds.spCredentialToken_Update 
			@Token = @Token OUTPUT,
			@IsExpired = @IsExpired OUTPUT,
			@DateExpires = @DateExpires OUTPUT,
			@BusinessEntityID = @BusinessEntityID OUTPUT
	
	
		IF @Debug = 1
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set/Validate data.' AS ActionMethod,
				@Token AS Token, @IsBusinessVerificationRequired AS IsBusinessVerificationRequired, 
				@BusinessEntityID AS BusinessEntityID, @BusinessID AS BusinessID, @LockToken AS LockToken
		END
	
		----------------------------------------------------------------------------------------------------------
		-- Validate the provided business matches the business attached to the token.
		----------------------------------------------------------------------------------------------------------	
		IF @IsBusinessVerificationRequired = 1 AND 
			ISNULL(@BusinessEntityID, -9999998) <> ISNULL(@BusinessID, -9999999)
		BEGIN
		
			SET @Token = NULL;
			SET @IsExpired = 1;
			SET @DateExpires = GETDATE();
			
		END
		
	
		-- Release lock.
		EXEC data.spProcessLock_Delete
			@LockToken = @LockToken
				
	END TRY
	BEGIN CATCH

		EXECUTE dbo.spLogException
			@Arguments = @Args

		-- Release lock.
		EXEC data.spProcessLock_Delete
			@LockToken = @LockToken
		
		SET @ErrorMessage = ERROR_MESSAGE();
		
		-- Re-throw error
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
	
	END CATCH
	

	----------------------------------------------------------------------------------------------------------
	-- Return results
	----------------------------------------------------------------------------------------------------------	
	SELECT
		@Token AS Token,
		@IsExpired AS IsExpired,
		@DateExpires AS DateExpires
	
	
END

