﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/5/2014
-- Description:	Returns a list of services.
/*

EXEC api.spDbo_Business_Service_Get_List
	@BusinessKey = '10684'

*/
-- SELECT * FROM dbo.Service	
-- SELECT * FROM dbo.BusinessEntityService WHERE BusinessEntityID = '10684'
-- SELECT * FROM dbo.vwStore WHERE Name LIKE '%Tommy%'
-- =============================================
CREATE PROCEDURE [api].[spDbo_Business_Service_Get_List]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@ServiceKey VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------------------------------------
	DECLARE
		@BusinessID BIGINT,
		@ServiceID INT

	----------------------------------------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
	-- business key type.  If a key type was not supplied then attempt a trial and error conversion
	-- based on token or NABP.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1	
	
	SET @ServiceID = dbo.fnGetServiceID(@ServiceKey);


	---------------------------------------------------------------------
	-- Retrieve business service list.
	---------------------------------------------------------------------	
	EXEC api.spDbo_BusinessEntityService_Get_List
		@BusinessEntityID = @BusinessID,
		@ServiceID = @ServiceID,
		@Skip = @Skip,
		@Take = @Take,
		@SortCollection = @SortCollection

END
