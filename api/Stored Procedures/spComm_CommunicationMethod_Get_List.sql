﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/15/2015
-- Description:	Returns a list of CommunicationMethod objects.
-- SAMPLE CALL:
/*

-- Identifier.
EXEC api.spComm_CommunicationMethod_Get_List
	@CommunicationMethodKey = 1
	
-- Code.
EXEC api.spComm_CommunicationMethod_Get_List
	@CommunicationMethodKey = 'SMS'

*/
-- SELECT * FROM comm.CommunicationMethod
-- SELECT * FROM msg.MessageType
-- =============================================
CREATE PROCEDURE [api].[spComm_CommunicationMethod_Get_List]
	@CommunicationMethodKey VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationMethodKey=' + dbo.fnToStringOrEmpty(@CommunicationMethodKey) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	-------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@CommuniationMethodIdArray VARCHAR(MAX) = comm.fnCommunicationMethodKeyTranslator(@CommunicationMethodKey, DEFAULT)
	
	
	--------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'Code', 'Name') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;
		
	-------------------------------------------------------------------------------------
	-- Fetch results.
	-------------------------------------------------------------------------------------
	;WITH cteItems AS (
		SELECT
			CASE 
				WHEN @SortField IN ('CommunicationMethodID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationMethodID ASC) 
				WHEN @SortField IN ('CommunicationMethodID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationMethodID DESC)     
				WHEN @SortField = 'Code' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY Code ASC) 
				WHEN @SortField = 'Code' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY Code DESC) 
				WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY Name ASC) 
				WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY Name DESC)
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated DESC)   
				ELSE ROW_NUMBER() OVER (ORDER BY CommunicationMethodID ASC)
			END AS RowNumber,
			CommunicationMethodID,
			Code,
			Name,
			Description,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy
		--SELECT *
		FROM comm.CommunicationMethod
		WHERE (@CommunicationMethodKey IS NULL OR CommunicationMethodID IN (SELECT Value FROM dbo.fnSplit(@CommuniationMethodIdArray, ',') WHERE ISNUMERIC(Value) = 1))
	)

	SELECT
		CommunicationMethodID,
		Code,
		Name,
		Description,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
	FROM cteItems
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
END
