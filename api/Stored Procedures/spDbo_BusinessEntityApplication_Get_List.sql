﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/16/2016
-- Description:	Returns a single or collection of BusinessEntityApplication objects that are applicable to the specified criteria.

/*

DECLARE
	@BusinessEntityApplicationID VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = '7871787',
	@BusinessKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonKeyType VARCHAR(50) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_BusinessEntityApplication_Get_List
	@BusinessEntityApplicationID = @BusinessEntityApplicationID,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PersonKey = @PersonKey,
	@PersonKeyType = @PersonKeyType,
	@DateStart = @DateStart,
	@DateEnd = @DateEnd,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug


*/


-- SELECT * FROM dbo.BusinessEntityApplication
-- SELECT * FROM dbo.Application
-- SELECT * FROM dbo.Store
-- SELECT * FROM dbo.Business

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spDbo_BusinessEntityApplication_Get_List]
	@BusinessEntityApplicationID VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ApplicationKey VARCHAR(MAX) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonKeyType VARCHAR(50) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@BusinessEntityApplicationID=' + dbo.fnToStringOrEmpty(@BusinessEntityApplicationID)  + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@PersonKey=' + dbo.fnToStringOrEmpty(@PersonKey)  + ';' +
		'@PersonKeyType=' + dbo.fnToStringOrEmpty(@PersonKeyType)  + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart)  + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @DateStart = ISNULL(@DateStart, CONVERT(DATE, GETDATE()));
	SET @DateEnd = ISNULL(@DateEnd, CONVERT(DATE, '12/31/9999'));
	
	------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL
	
	------------------------------------------------------------------------------------------------------------
	-- Set variables
	------------------------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.

	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey ,
		@BusinessKeyType = @BusinessKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1
	
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationKey AS ApplicationKey, @ApplicationID AS ApplicationID, 
			@BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@BusinessID AS BusinessID, @DateStart AS DateStart, @DateEnd AS DateEnd
	END

	------------------------------------------------------------------------------------------------------------
	-- Paging.
	------------------------------------------------------------------------------------------------------------	
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)
			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);


	------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	------------------------------------------------------------------------------------------------------------

	

	------------------------------------------------------------------------------------------------------------
	-- Create result set
	------------------------------------------------------------------------------------------------------------
	;WITH cteItems AS (
	
		SELECT
			ROW_NUMBER() OVER(ORDER BY
				CASE WHEN @SortField IN ('DateCreated') AND @SortDirection = 'asc'  THEN ba.DateCreated END ASC, 
				CASE WHEN @SortField IN ('DateCreated') AND @SortDirection = 'desc'  THEN ba.DateCreated END DESC,   
				CASE WHEN @SortField IN ('BusinessID', 'BusinessEntityID', 'Business') AND @SortDirection = 'asc'  THEN ba.BusinessEntityID END ASC,
				CASE WHEN @SortField IN ('BusinessID', 'BusinessEntityID', 'Business')  AND @SortDirection = 'desc'  THEN ba.BusinessEntityID END DESC,  
				COALESCE(sto.Name, chn.Name, LTRIM(RTRIM(ISNULL(psn.FirstName, '') + ' ' + ISNULL(psn.LastName, ''))) ) ASC
			) AS RowNumber,
			ba.BusinessEntityApplicationID,
			ba.BusinessEntityID,
			biz.BusinessEntityTypeID,
			biztyp.Code AS BusinessEntityTypeCode,
			biztyp.Name AS BusinessEntityTypeName,
			COALESCE(sto.Name, chn.Name, LTRIM(RTRIM(ISNULL(psn.FirstName, '') + ' ' + ISNULL(psn.LastName, ''))) ) AS BusinessEntityName,
			ba.ApplicationID,
			bu.BusinessToken,
			app.ApplicationTypeID,
			apptyp.Code AS ApplicationTypeCode,
			apptyp.Name AS ApplicationTypeName,
			app.Code AS ApplicationCode,
			app.Name AS ApplicationName,
			COALESCE(ba.ApplicationPath, app.ApplicationPath) AS ApplicationPath,
			app.ApplicationPath AS DefaultApplicationPath,
			ba.rowguid,
			ba.DateCreated,
			ba.DateModified,
			ba.CreatedBy,
			ba.ModifiedBy
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.BusinessEntityApplication ba
			JOIN dbo.BusinessEntity biz
				ON ba.BusinessEntityID = biz.BusinessEntityID
			LEFT JOIN dbo.BusinessEntityType biztyp
				ON biztyp.BusinessEntityTypeID = biz.BusinessEntityTypeID
			LEFT JOIN dbo.Business bu
				on ba.BusinessEntityID = bu.BusinessEntityID
			JOIN dbo.Application app
				ON app.ApplicationID = ba.ApplicationID
			LEFT JOIN dbo.ApplicationType apptyp
				ON apptyp.ApplicationTypeID = app.ApplicationTypeID
			LEFT JOIN dbo.Store sto
				ON biztyp.Code = 'STRE'
					AND sto.BusinessEntityID = ba.BusinessEntityID
			LEFT JOIN dbo.Chain chn
				ON biztyp.Code = 'CHN'
					AND chn.BusinessEntityID = ba.BusinessEntityID
			LEFT JOIN dbo.Person psn
				ON biztyp.Code IN ('PRSN', 'PSN')
					AND psn.BusinessEntityID = ba.BusinessEntityID
		WHERE ( @BusinessEntityApplicationID IS NULL OR (@BusinessEntityApplicationID IS NOT NULL 
				AND ba.BusinessEntityApplicationID IN (SELECT Value FROM dbo.fnSplit(@BusinessEntityApplicationID, ',') WHERE ISNUMERIC(Value) = 1 )))
			AND ( @BusinessKey IS NULL OR (@BusinessKey IS NOT NULL AND ba.BusinessEntityID = @BusinessID))
			AND ( @ApplicationKey IS NULL OR (@ApplicationKey IS NOT NULL AND ba.ApplicationID = @ApplicationID)) 
	)
	
	SELECT 
		BusinessEntityApplicationID,
		BusinessEntityID,
		BusinessEntityTypeID,
		BusinessEntityTypeCode,
		BusinessEntityTypeName,
		BusinessEntityName,
		BusinessToken,
		ApplicationID,
		ApplicationTypeID,
		ApplicationTypeCode,
		ApplicationTypeName,
		ApplicationCode,
		ApplicationName,
		ApplicationPath,
		DefaultApplicationPath,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	FROM cteItems res
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.


	
	
				
END