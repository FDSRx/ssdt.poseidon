﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/21/2014
-- Description:	Returns a list of scopes.
-- SAMPLE CALL: api.spDbo_Scope_Get_List
-- =============================================
CREATE PROCEDURE [api].[spDbo_Scope_Get_List]
	@ScopeID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		ScopeID,
		Code,
		Name,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	FROM dbo.Scope
	WHERE ( @ScopeID IS NULL OR ScopeID IN (SELECT Value FROM dbo.fnSplit(@ScopeID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	
END
