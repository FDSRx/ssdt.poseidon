﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/11/2014
-- Description:	Returns a patients standard demographic information
-- SAMPLE CALL: EXEC api.spPhrm_Patient_Demographics_Get  @BusinessKey = '7871787', @PatientKey = '21656'
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Demographics_Get]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));

	---------------------------------------------------------------------
	-- Return patient data.
	---------------------------------------------------------------------
	SELECT
		SourcePatientKey,
		Title,
		FirstName,
		LastName,
		MiddleName,
		Suffix,
		CONVERT(VARCHAR(10), BirthDate, 101) AS BirthDate,
		Gender,
		Language,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		dbo.fnFormatPhoneNumber(HomePhone) AS HomePhone,
		IsCallAllowedToHomePhone,
		IsTextAllowedToHomePhone,
		IsHomePhonePreferred,
		dbo.fnFormatPhoneNumber(MobilePhone) AS MobilePhone,
		IsCallAllowedToMobilePhone,
		IsTextAllowedToMobilePhone,
		IsMobilePhonePreferred,
		PrimaryEmail,
		IsEmailAllowedToPrimaryEmail,
		IsPrimaryEmailPreferred,
		AlternateEmail,
		IsEmailAllowedToAlternateEmail,
		IsAlternateEmailPreferred,
		DateCreated,
		DateModified,
		@PatientID AS PatientId
	FROM phrm.fnGetPatientInformation(@PatientID)
	
END
