﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/22/2014
-- Description:	Returns a list of FinancialClass items.
-- SAMPLE CALL: api.spDbo_FinancialClass_Get_List
-- =============================================
CREATE PROCEDURE [api].spDbo_FinancialClass_Get_List
	@FinancialClassID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		ref.FinancialClassID,
		ref.Code,
		ref.Description,
		ref.DateCreated,
		ref.DateModified
	--SELECT *
	FROM dbo.FinancialClass ref
	WHERE ( @FinancialClassID IS NULL OR ref.FinancialClassID IN (SELECT Value FROM dbo.fnSplit(@FinancialClassID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR ref.Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	
END
