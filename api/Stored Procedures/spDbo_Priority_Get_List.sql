﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/21/2014
-- Description:	Returns a list of note priorities.
-- SAMPLE CALL: api.spDbo_Priority_Get_List
-- =============================================
CREATE PROCEDURE [api].[spDbo_Priority_Get_List]
	@PriorityID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		PriorityID,
		Code,
		Name,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	FROM dbo.Priority
	WHERE ( @PriorityID IS NULL OR PriorityID IN (SELECT Value FROM dbo.fnSplit(@PriorityID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	
END
