﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/22/2014
-- Description:	Returns a list of AdministrationRoute items.
-- SAMPLE CALL: api.spVaccine_AdministrationRoute_Get_List
-- =============================================
CREATE PROCEDURE [api].spVaccine_AdministrationRoute_Get_List
	@AdministrationRouteID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		ref.AdministrationRouteID,
		ref.Code,
		ref.Description,
		ref.Definition,
		ref.DateCreated,
		ref.DateModified
	--SELECT *
	FROM vaccine.AdministrationRoute ref
	WHERE ( @AdministrationRouteID IS NULL OR ref.AdministrationRouteID IN (SELECT Value FROM dbo.fnSplit(@AdministrationRouteID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR ref.Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	
END
