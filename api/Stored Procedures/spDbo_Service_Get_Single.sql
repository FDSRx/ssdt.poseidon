﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/3/2015
-- Description:	Returns a single service object.
/*

-- Get service only
EXEC api.spDbo_Service_Get_Single
	@ServiceKey = 'ENG'

-- Get service with business
EXEC api.spDbo_Service_Get_Single
	@ServiceKey = 'ENG',
	@BusinessKey = '10684'

-- Get service with business
EXEC api.spDbo_Service_Get_Single
	@ServiceKey = 'ESMT',
	@BusinessKey = '10684'

*/

-- SELECT * FROM dbo.Service
-- =============================================
CREATE PROCEDURE [api].[spDbo_Service_Get_Single]
	@ServiceKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE @ServiceID INT;
	
	--------------------------------------------------------------------------------------------
	-- Set variable.
	--------------------------------------------------------------------------------------------
	SET @ServiceID = dbo.fnGetServiceID(@ServiceKey);
	
	--------------------------------------------------------------------------------------------
	-- Returns a single object.
	--------------------------------------------------------------------------------------------
	IF @ServiceID IS NOT NULL
	BEGIN
	
		EXEC api.spDbo_Service_Get_List
			@ServiceKey = @ServiceID,
			@BusinessKey = @BusinessKey,
			@BusinessKeyType = @BusinessKeyType,
			@Take = 1
			
	END
	
			
END
