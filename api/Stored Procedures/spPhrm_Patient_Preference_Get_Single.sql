﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/20/2015
-- Description:	Retrieves a single BusinessEntityPreference object.
-- SAMPLE CALL;
/*

DECLARE
	@BusinessEntityPreferenceID BIGINT = NULL
	
EXEC api.spPhrm_Patient_Preference_Get_Single
	@BusinessKey = '7871787',
	@PatientKey = '21656',
	@PreferenceKey = 'DNC',
	@ModifiedBy = 'dhughes',
	@BusinessEntityPreferenceID = @BusinessEntityPreferenceID


*/

-- SELECT * FROM dbo.BusinessEntityPreference
-- SELECT * FROM dbo.vwPatient 
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Preference_Get_Single]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@PreferenceKey VARCHAR(50) = NULL,
	@BusinessEntityPreferenceID BIGINT = NULL,
	@ValueString VARCHAR(MAX) = NULL,
	@ValueBoolean BIT = NULL,
	@ValueNumeric DECIMAL(19, 8) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@EnforceRecordIdentifierPatientMatch BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------
	SET @EnforceRecordIdentifierPatientMatch = ISNULL(@EnforceRecordIdentifierPatientMatch, 1);

	-----------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------
	DECLARE
		@PersonID BIGINT,
		@BusinessID BIGINT,
		@PreferenceID INT
		
		
	-----------------------------------------------------------------------------------------------
	-- Set local variables.
	-----------------------------------------------------------------------------------------------
	EXEC api.spDbo_System_KeyTranslator
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PatientKey = @PatientKey,
		@PatientKeyType = @PatientKeyType,
		@BusinessID = @BusinessID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1	
	
	SET @PreferenceID = dbo.fnGetPreferenceID(@PreferenceKey);
		
	
	-----------------------------------------------------------------------------------------------
	-- Fetch the patient preference object.
	-----------------------------------------------------------------------------------------------
	SELECT TOP 1
		BusinessEntityPreferenceID,
		BusinessEntityID,
		bp.PreferenceID,
		pref.Code AS PreferenceCode,
		pref.Name AS PreferenceName,
		ValueString,
		ValueBoolean,
		ValueNumeric,
		bp.DateCreated,
		bp.DateModified,
		bp.CreatedBy,
		bp.ModifiedBy
	FROM dbo.BusinessEntityPreference bp
		JOIN dbo.Preference pref
			ON bp.PreferenceID = pref.PreferenceID
	WHERE ( bp.BusinessEntityPreferenceID = @BusinessEntityPreferenceID
		AND bp.BusinessEntityID = CASE WHEN @EnforceRecordIdentifierPatientMatch = 0 THEN bp.BusinessEntityID ELSE @PersonID END )
		OR ( bp.BusinessEntityID = @PersonID
			AND bp.PreferenceID = @PreferenceID )
		
	

END
