﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/12/2014
-- Description:	Indicates whether a patient exists.
-- SAMPLE CALL: 
/*
DECLARE @Exists BIT

EXEC api.spPhrm_Patient_Exists
	@Nabp = '0102804', 
	@SourcePatientKey = 93738,
	@Exists = @Exists OUTPUT

SELECT @Exists AS IsFound
*/
--SELECT TOP 50 * FROM phrm.vwPatient
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Exists]
	-- Patient
	@PatientID BIGINT = NULL OUTPUT,
	--Business
	@StoreID INT = NULL,
	@Nabp VARCHAR(25) = NULL,
	@BusinessEntityID BIGINT = NULL,
	--Source Patient
	@SourcePatientKey VARCHAR(50) = NULL,
	--Person
	@PersonID BIGINT = NULL,
	--Output
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-----------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------	
	SET @Exists = 0;
	
	-----------------------------------------------------------------------
	-- Translate raw data keys into business objects.
	-----------------------------------------------------------------------
	-- Get BusinessEntityID (Store)
	SET @BusinessEntityID = 
		CASE 
			WHEN @BusinessEntityID IS NOT NULL THEN @BusinessEntityID
			WHEN @StoreID IS NOT NULL THEN dbo.fnGetStoreIDFromSourceID(@StoreID)
			WHEN ISNULL(@Nabp, '') <> '' THEN dbo.fnGetStoreIDByNABP(@Nabp)
			ELSE NULL
		END;
	
	
	-----------------------------------------------------------------------
	-- Determine if the patient exists in the system.
	-----------------------------------------------------------------------
	EXEC phrm.spPatient_Exists
		--Patient
		@PatientID = @PatientID OUTPUT,
		--Business
		@BusinessEntityID = @BusinessEntityID,
		--Pharmacy Key
		@SourcePatientKey = @SourcePatientKey,
		--Person
		@PersonID = @PersonID OUTPUT,
		--Output
		@Exists = @Exists OUTPUT
		
				
				
END
