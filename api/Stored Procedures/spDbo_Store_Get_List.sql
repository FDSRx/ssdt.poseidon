﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/26/2014
-- Description:	Returns a list of all known stores.
-- Change Log:
-- 10/22/24 - dhughes - Modified the procedure to improve performance by better handling sorting and added the ability to
--				read uncommitted store data (i.e. NOLOCK) to prevent locking.
-- 3/16/2016 - dhughes - Added additional outputs (LicenseNumber & SourceSystemKey).
-- 11/1/2016 - ekizer - Added additional output (ChainName).
-- 11/9/2016 - dhughes - Modified the procedure to search the "ChainName" property along with the "StoreName" property,
--					when the "Name" property is populated.

-- SAMPLE CALL: 
/*

EXEC api.spDbo_Store_Get_List

EXEC api.spDbo_Store_Get_List 
	@ChainKey = '3685'
	
EXEC api.spDbo_Store_Get_List 
	@Name = 'ACADIA'

EXEC api.spDbo_Store_Get_List 
	@Name = 'Balls'
	
EXEC api.spDbo_Store_Get_List 
	@Nabp = '4512287'
	
EXEC api.spDbo_Store_Get_List 
	@ServiceKey = 'ENG'
	
EXEC api.spDbo_Store_Get_List 
	@ApplicationKey = 'ENG'
	
EXEC api.spDbo_Store_Get_List 
	@ApplicationKey = 'ENG', 
	@BusinessKey = '10684', 
	@BusinessKeyType = 'ID',
	@MessageTypeKey = 'HPPYBDAY'
	
EXEC api.spDbo_Store_Get_List 
	@ApplicationKey = 'ENG', 
	@BusinessKey = '38', 
	@BusinessKeyType = 'ID',
	@MessageTypeKey = 'WLCME'

EXEC api.spDbo_Store_Get_List 
	@ServiceKey = 'ENG',
	@ApplicationKey = 'ENG',
	@MessageTypeKey = 'HPPYBDAY'
	
*/

-- SELECT * FROM dbo.vwStore
-- SELECT * FROM dbo.ChainStore
-- SELECT * FROM dbo.Service
-- SELECT * FROM comm.CommunicationConfiguration
-- =============================================
CREATE PROCEDURE [api].[spDbo_Store_Get_List]
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType  VARCHAR(50) = NULL,
	@SourceStoreKey VARCHAR(MAX) = NULL,
	@ChainKey VARCHAR(MAX) = NULL,
	@Nabp VARCHAR(MAX) = NULL,
	@Name VARCHAR(256) = NULL,
	@ServiceKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(MAX) = NULL,
	@MessageTypeKey VARCHAR(MAX) = NULL,
	@CommunicationMethodKey VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN


	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Result schema binding
	-- <Summary>
	-- Returns a schema of the intended result set.  This is a workaround to assist certain
	-- frameworks in determining the correct result sets (i.e. SSIS, Entity Framework, etc.).
	-- </Summary>
	--------------------------------------------------------------------------------------------	
	IF 1 = 0 
	BEGIN
	
		DECLARE @Property VARCHAR(10) = '';
		
		SELECT TOP 1
			CAST(@Property AS VARCHAR(50)) AS BusinessEntityID,
			CAST(@Property AS VARCHAR(256)) AS Name,
			CAST(@Property AS VARCHAR(75)) AS SourceStoreID,
			CAST(@Property AS VARCHAR(75)) AS NABP,
			CAST(@Property AS VARCHAR(50)) AS BusinessToken,
			CAST(@Property AS VARCHAR(50)) AS BusinessNumber,
			CAST(@Property AS VARCHAR(50)) AS LicenseNumber,
			CAST(@Property AS VARCHAR(50)) AS SourceSystemKey,
			CAST(@Property AS VARCHAR(50)) AS ChainID,
			CAST(@Property AS VARCHAR(50)) AS ChainToken,
			CAST(@Property AS VARCHAR(256)) AS AddressLine1,
			CAST(@Property AS VARCHAR(256)) AS AddressLine2,
			CAST(@Property AS VARCHAR(256)) AS City,
			CAST(@Property AS VARCHAR(256)) AS State,
			CAST(@Property AS VARCHAR(256)) AS PostalCode,
			CAST(@Property AS VARCHAR(256)) AS Latitude,
			CAST(@Property AS VARCHAR(256)) AS Longitude,
			CAST(@Property AS VARCHAR(256)) AS Website,
			CAST(@Property AS VARCHAR(256)) AS PhoneNumber,
			CAST(@Property AS VARCHAR(256)) AS EmailAddress,
			CAST(@Property AS VARCHAR(256)) AS FaxNumber,
			CAST(@Property AS VARCHAR(256)) AS TimeZoneCode,
			CAST(@Property AS VARCHAR(256)) AS TimeZoneLocation,
			CAST(@Property AS INT) AS TimeZoneOffSet,
			CAST(@Property AS BIT) AS SupportDST
				
	END	
	
	--------------------------------------------------------------------------------------------
	-- Parameter Sniffing Fix
	-- <Summary>
	-- Saves a copy of the global input variables to a local variable and then the local
	-- variable is used to perform all the necessary tasks within the procedure.  This allows
	-- the optimizer to use statistics instead of a cached plan that might not be optimized
	-- for the sparatic queries.
	-- </Summary>
	--------------------------------------------------------------------------------------------
	DECLARE
		@BusinessKey_Local VARCHAR(MAX) = @BusinessKey,
		@SourceStoreKey_Local VARCHAR(MAX) = @SourceStoreKey,
		@ChainKey_Local VARCHAR(MAX) = @ChainKey,
		@Nabp_Local VARCHAR(MAX) = @Nabp,
		@Name_Local VARCHAR(256) = @Name,
		@ServiceKey_Local VARCHAR(MAX) = @ServiceKey,
		@ApplicationKey_Local VARCHAR(MAX) = @ApplicationKey,
		@MessageTypeKey_Local VARCHAR(MAX) = @MessageTypeKey,
		@CommunicationMethodKey_Local VARCHAR(MAX) = @CommunicationMethodKey,
		@Skip_Local BIGINT = @Skip,
		@Take_Local BIGINT = @Take,
		@SortCollection_Local VARCHAR(MAX) = @SortCollection	
	
	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE 
		@BusinessID BIGINT = NULL,
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ServiceIdArray VARCHAR(MAX),
		@ApplicationIdArray VARCHAR(MAX),
		@MessageTypeIdArray VARCHAR(MAX),
		@CommunicationMethodIdArray VARCHAR(MAX)
		
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	SET @ServiceIdArray = dbo.fnServiceKeyTranslator(@ServiceKey_Local, DEFAULT);
	SET @ApplicationIdArray = dbo.fnApplicationKeyTranslator(@ApplicationKey_Local, DEFAULT);
	SET @MessageTypeIdArray = comm.fnMessageTypeKeyTranslator(@MessageTypeKey_Local, DEFAULT);
	SET @CommunicationMethodIdArray = comm.fnCommunicationMethodKeyTranslator(@CommunicationMethodKey_Local, DEFAULT);
	--SET	@BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);


	-- Debug
	--SELECT 
	--	@ServiceKey_Local AS ServiceKey, @ApplicationKey_Local AS Application, 
	--	@MessageTypeKey_Local AS MessageTypeKey, @CommunicationMethodKey_Local AS CommunicationMethodKey,
	--	@ServiceIdArray AS ServiceIdArray, @ApplicationIdArray AS ApplicationIdArray, 
	--	@MessageTypeIdArray AS MessageTypeIdArray, @CommunicationMethodIdArray AS CommunicationMethodIdArray
		

	--------------------------------------------------------------------------------------------
	-- Paging.
	--------------------------------------------------------------------------------------------	
	-- Support paging
	SET @Skip_Local = ISNULL(@Skip_Local, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take_Local = ISNULL(@Take_Local, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection_Local, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);

	--------------------------------------------------------------------------------------------
	-- Build results
	--------------------------------------------------------------------------------------------
	;WITH cteStores AS (	
		SELECT DISTINCT
			ROW_NUMBER() OVER(ORDER BY
				CASE WHEN @SortField IN ('BusinessEntityID', 'BusinessID', 'StoreID') AND @SortDirection = 'asc'  THEN sto.BusinessEntityID END ASC, 
				CASE WHEN @SortField IN ('BusinessEntityID', 'BusinessID', 'StoreID') AND @SortDirection = 'desc'  THEN sto.BusinessEntityID END DESC,   
				CASE WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN sto.Name END ASC,
				CASE WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN sto.Name END DESC,  
				CASE WHEN @SortField IS NULL OR @SortField = '' THEN sto.Name ELSE sto.Name END ASC
			) AS RowNumber,
			--CASE 
			--	WHEN @SortField IN ('BusinessEntityID', 'BusinessID', 'StoreID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY sto.BusinessEntityID ASC) 
			--	WHEN @SortField IN ('BusinessEntityID', 'BusinessID', 'StoreID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY sto.BusinessEntityID DESC)     
			--	WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY sto.Name ASC) 
			--	WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY sto.Name DESC)  
			--	ELSE ROW_NUMBER() OVER (ORDER BY sto.Name ASC)
			--END AS RowNumber,	
			sto.BusinessEntityID,
			sto.Name,
			sto.SourceStoreID,
			sto.NABP,
			sto.BusinessToken,
			sto.BusinessNumber,
			sto.LicenseNumber,
			sto.SourceSystemKey,
			chn.ChainID,
			sto.ChainName,
			bizchn.BusinessToken AS ChainToken,
			sto.AddressLine1,
			sto.AddressLine2,
			sto.City,
			sto.State,
			sto.PostalCode,
			sto.Latitude,
			sto.Longitude,
			sto.Website,
			sto.PhoneNumber,
			sto.EmailAddress,
			sto.FaxNumber,
			sto.TimeZoneCode,
			sto.TimeZoneLocation,
			sto.TimeZoneOffSet,
			sto.SupportDST
		--SELECT TOP 1 *
		--SELECT *
		FROM dbo.vwStore sto (NOLOCK)
			LEFT JOIN dbo.ChainStore chn (NOLOCK)
				ON sto.BusinessEntityID = chn.StoreID
			LEFT JOIN dbo.Business bizchn (NOLOCK)
				ON chn.ChainID = bizchn.BusinessEntityID
		WHERE ( NULLIF(@BusinessKey_Local, '') IS NULL OR (NULLIF(@BusinessKey_Local, '')  IS NOT NULL AND sto.BusinessEntityID IN ( SELECT Value FROM dbo.fnSplit(@BusinessKey_Local, ',') WHERE ISNUMERIC(Value) = 1)) )
			AND ( NULLIF(@Nabp_Local, '') IS NULL OR (NULLIF(@Nabp_Local, '')  IS NOT NULL AND NABP IN ( SELECT Value FROM dbo.fnSplit(@Nabp_Local, ',')) ) )
			AND ( NULLIF(@SourceStoreKey_Local, '') IS NULL OR (NULLIF(@SourceStoreKey_Local, '') IS NOT NULL AND sto.SourceStoreID IN (SELECT Value FROM dbo.fnSplit(@SourceStoreKey_Local, ',')) ) )
			AND ( NULLIF(@ChainKey_Local, '') IS NULL OR (
				NULLIF(@ChainKey_Local, '') IS NOT NULL AND chn.ChainID IN ( SELECT Value FROM dbo.fnSplit(@ChainKey_Local, ',') WHERE ISNUMERIC(Value) = 1)
					AND chn.ChainStoreID IS NOT NULL)
			)
			AND ( 
				(NULLIF(@Name_Local, '') IS NULL  OR (NULLIF(@Name_Local, '') IS NOT NULL AND  sto.Name LIKE '%' + @Name_Local + '%' ))
				OR (NULLIF(@Name_Local, '') IS NULL  OR (NULLIF(@Name_Local, '') IS NOT NULL AND  sto.ChainName LIKE '%' + @Name_Local + '%' ))
			)
			AND ( NULLIF(@ServiceKey_Local, '') IS NULL OR (NULLIF(@ServiceKey_Local, '') IS NOT NULL AND EXISTS (
				SELECT TOP 1 *
				FROM dbo.BusinessEntityService tmp (NOLOCK)
				WHERE tmp.BusinessEntityID = sto.BusinessEntityID
					AND tmp.ServiceID IN (SELECT Value FROM dbo.fnSplit(@ServiceIdArray, ',') WHERE ISNUMERIC(Value) = 1)
			) ))
			AND ( NULLIF(@ApplicationKey_Local, '') IS NULL OR (NULLIF(@ApplicationKey, '') IS NOT NULL AND EXISTS (
				SELECT TOP 1 *
				FROM dbo.BusinessEntityApplication tmp (NOLOCK)
				WHERE tmp.BusinessEntityID = sto.BusinessEntityID
					AND tmp.ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationIdArray, ',') WHERE ISNUMERIC(Value) = 1)
			) ))
			AND ( NULLIF(@MessageTypeKey_Local, '') IS NULL OR ( NULLIF(@MessageTypeKey_Local, '') IS NOT NULL AND EXISTS (
				SELECT TOP 1 *
				FROM comm.CommunicationConfiguration comm (NOLOCK)
					JOIN dbo.BusinessEntityApplication app (NOLOCK)
						ON comm.ApplicationID = app.ApplicationID
							AND comm.BusinessID = app.BusinessEntityID
				WHERE ( 
					( NULLIF(@ApplicationKey_Local, '') IS NULL OR (NULLIF(@ApplicationKey_Local, '') IS NOT NULL AND comm.ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationIdArray, ',') WHERE ISNUMERIC(Value) = 1) ))
					AND comm.BusinessID = sto.BusinessEntityID
					AND comm.MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeIdArray, ',') WHERE ISNUMERIC(Value) = 1)
			))))
	)
	
	-- Return a pageable result set.
	SELECT
		BusinessEntityID,
		Name,
		SourceStoreID,
		NABP,
		BusinessToken,
		BusinessNumber,
		LicenseNumber,
		SourceSystemKey,
		ChainID,
		ChainName,
		ChainToken,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		Latitude,
		Longitude,
		Website,
		PhoneNumber,
		EmailAddress,
		FaxNumber,
		TimeZoneCode,
		TimeZoneLocation,
		TimeZoneOffSet,
		SupportDST,
		(SELECT COUNT(*) FROM cteStores) AS TotalRecords
	FROM cteStores
	WHERE RowNumber > @Skip_Local 
		AND RowNumber <= (@Skip_Local + @Take_Local)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
END
