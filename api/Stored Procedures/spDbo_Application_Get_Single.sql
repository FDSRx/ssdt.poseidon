﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/17/2015
-- Description:	Returns a single application object.
/*

-- Get application only
EXEC api.spDbo_Application_Get_Single
	@ApplicationKey = 'ENG'

-- Get application with business
EXEC api.spDbo_Application_Get_Single
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684'

-- Get application with business
EXEC api.spDbo_Application_Get_Single
	@ApplicationKey = 'ESMT',
	@BusinessKey = '10684'

*/

-- SELECT * FROM dbo.Application
-- =============================================
CREATE PROCEDURE [api].[spDbo_Application_Get_Single]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE @ApplicationID INT;
	
	--------------------------------------------------------------------------------------------
	-- Set variable.
	--------------------------------------------------------------------------------------------
	SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationKey);
	
	--------------------------------------------------------------------------------------------
	-- Returns a single object.
	--------------------------------------------------------------------------------------------
	IF @ApplicationID IS NOT NULL
	BEGIN
	
		EXEC api.spDbo_Application_Get_List
			@ApplicationKey = @ApplicationID,
			@BusinessKey = @BusinessKey,
			@BusinessKeyType = @BusinessKeyType,
			@Take = 1
			
	END
	
			
END
