﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/22/2014
-- Description:	Returns a list of CVXCode items.
-- SAMPLE CALL: api.spVaccine_CVXCode_Get_List
-- =============================================
CREATE PROCEDURE [api].spVaccine_CVXCode_Get_List
	@CVXCodeID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		ref.CVXCodeID,
		ref.Code,
		ref.FullVaccineName AS Name,
		ref.ShortDescription,
		ref.VaccineStatus,
		ref.Notes,
		ref.DateLastUpdated,
		ref.DateCreated,
		ref.DateModified
	--SELECT *
	FROM vaccine.CVXCode ref
	WHERE ( @CVXCodeID IS NULL OR ref.CVXCodeID IN (SELECT Value FROM dbo.fnSplit(@CVXCodeID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR ref.Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	
END
