﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/31/2014
-- Description:	Uses the credential token to identify the credential and reset the user's password.
-- SAMPLE CALL:
/*

DECLARE @Token VARCHAR(50) = '97E4A656-6F8F-438C-ABBE-825B5A3F48DE'

EXEC api.spCreds_Credentials_ForgotPassword_Reset
	@CredentialToken = @Token,
	@NewPassword = 'password',
	@ConfirmNewPassword = 'password',
	@RequestedBy = 'dhughes'

*/

-- SELECT * FROM dbo.RequestType
-- SELECT * FROM dbo.TokenType
-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
-- SELECT * FROM creds.Membership WHERE CredentialEntityID = 801050
-- =============================================
CREATE PROCEDURE [api].[spCreds_Credentials_ForgotPassword_Reset]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	@CredentialToken VARCHAR(50),
	@NewPassword VARCHAR(50),
	@ConfirmNewPassword VARCHAR(50),
	@DatePasswordExpires DATETIME = NULL,
	-- Core device properties ************************************************************
	@IpAddress VARCHAR(256) = NULL,
	@BrowserName VARCHAR(256) = NULL,
	@BrowserVersion VARCHAR(50) = NULL,
	@DeviceName VARCHAR(50) = NULL,
	@IsMobile BIT = NULL,
	@UserAgent VARCHAR(500) = NULL,
	@Comments VARCHAR(MAX) = NULL,
	-- ***********************************************************************************
	-- Core membership properties ************************************************************
	@IsLockedOut BIT = NULL,
	-- Core membership properties ************************************************************
	@LanguageKey VARCHAR(50) = NULL,
	@RequestedBy VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Instance.
	--------------------------------------------------------------------------------------------
	DECLARE @Source VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @DatePasswordExpires = '12/31/9999';
	SET @IsLockedOut = 0;
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT = NULL,
		@BusinessID INT = NULL,
		@Message VARCHAR(MAX),
		@ErrorMessage VARCHAR(MAX),
		@RecordRequest BIT = ISNULL(CONVERT(BIT, dbo.fnGetConfigValue('RSTPWD')), 0),
		@RequestTypeID INT = dbo.fnGetRequestTypeID('RSTPWD'),
		@RequestKey VARCHAR(256),
		@CredentialEntityID BIGINT = NULL,
		@IsValid BIT = 0,
		@IsSuccess BIT = 0
	
	--------------------------------------------------------------------------------------------
	-- Set variables.
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,		
		@IsOutputOnly = 1
	

	--------------------------------------------------------------------------------------------
	-- Reset process.
	--------------------------------------------------------------------------------------------	
	
	BEGIN TRY
	
		--------------------------------------------------------------------------------------------
		-- Record request.
		--------------------------------------------------------------------------------------------
		IF ISNULL(@RecordRequest, 0) = 1
		BEGIN
	    
			DECLARE @RequestData VARCHAR(MAX) =
				'<ForgotPasswordResetRequest>' +
					'<BusinessToken>' + ISNULL(@BusinessKey, '') + '</BusinessToken>' +
					'<BusinessID>' + ISNULL(CONVERT(VARCHAR, @BusinessID), '') + '</BusinessID>' +
					'<CredentialToken>' + ISNULL(@CredentialToken, '') + '</CredentialToken>' +
					'<IpAddress>' + ISNULL(@IPAddress, '') + '</IpAddress>' +
					'<BrowserName>' + ISNULL(@BrowserName, '') + '</BrowserName>' +
					'<BrowserVersion>' + ISNULL(@BrowserVersion, '') + '</BrowserVersion>' +
					'<DeviceName>' + ISNULL(@DeviceName, '') + '</DeviceName>' +
					'<IsMobile>' + ISNULL(CONVERT(VARCHAR, @IsMobile), '0') + '</IsMobile>' +
					'<UserAgent>' + ISNULL(@UserAgent, '') + '</UserAgent>' +
					'<LanguageKey>' + ISNULL(@LanguageKey, '') + '</LanguageKey>' +
				'</ForgotPasswordResetRequest>'
		    
			EXEC dbo.spRequestLog_Create
				@ApplicationKey = @ApplicationKey,
				@BusinessKey = @BusinessKey,
				@RequesterKey = @RequestedBy,
				@ApplicationID = @ApplicationID,
				@BusinessEntityID = @BusinessID,
				@SourceName = @Source,
				@RequestData = @RequestData,
				@RequestTypeID = @RequestTypeID
		
		END
		
		-- Build request key.
		SET @RequestKey = dbo.fnIfNullOrEmpty(@BusinessID, '<NULLBUSINESS>') + '-RSTPWD-' + dbo.fnIfNullOrEmpty(@CredentialToken, '<NULLTOKEN>');
		
		
		--------------------------------------------------------------------------------------------
		-- Argument validation.
		--------------------------------------------------------------------------------------------	
		--------------------------------------------------------------------------------------------
		-- Ensure the new confirmation password property matches the newly provided password.
		--------------------------------------------------------------------------------------------	
		IF @NewPassword <> @ConfirmNewPassword
		BEGIN
		
			SELECT
				@IsSuccess = 0,
				@Message = 'Unable to update the User''s password. The new password was not successfully confirmed.'
				
			GOTO EOF;	
			
			RETURN;		
		END
	
		--------------------------------------------------------------------------------------------
		-- Fetch Credential object data.
		--------------------------------------------------------------------------------------------
		DECLARE 
			@TokenTypeID INT = dbo.fnGetTokenTypeID('RSTPWD'),
			@IsExpired BIT,
			@DateExpires DATETIME,
			@IsFound BIT = 0
		
		EXEC creds.spCredentialToken_IsActive
			@ApplicationID = @ApplicationID,
			@BusinessID = @BusinessID,
			@TokenTypeID = @TokenTypeID,
			@Token = @CredentialToken OUTPUT,
			@CredentialEntityID = @CredentialEntityID OUTPUT,
			@IsExpired = @IsExpired OUTPUT,
			@DateExpires = @DateExpires OUTPUT,
			@InvalidateDataOnExpired = 1,
			@Exists = @IsFound OUTPUT
		
		-- Debug
		--SELECT @CredentialEntityID AS CredentialEntityID, @CredentialToken AS Token, @IsExpired AS IsExpired, @DateExpires AS DateExpires
		
		-- If a CredentialEntityID was discovered, then verify certain demographics of the user.
		SET @IsValid = CASE WHEN @CredentialEntityID IS NOT NULL THEN 1 ELSE 0 END; 
		SET @Message = 
			CASE 
				WHEN @IsValid = 1 THEN @Message
				ELSE 'We''re sorry. The information provided did not resolve to an indiviual in our system. ' +
					'Please ensure you have entered the correct security token or the security token has not expired.' 
			END;
		
		-- Debug
		--SELECT @CredentialEntityID AS CredentialEntityID, @IsValid AS IsValid, @RequestKey AS RequestKey

		---------------------------------------------------------------------------------------------------
		-- Track the request (regardless of being valid).
		-- <Summary>
		-- Tracks the attempt.  If the request is valid and not locked out, then it will
		-- be removed from the tracking table. Otherwise, the attempt will be recorded
		-- for possible further inspection.
		-- </Summary>
		---------------------------------------------------------------------------------------------------	
		DECLARE 
			@RequestTrackerID BIGINT = NULL,
			@MaxRequestsAllowed INT = NULL,
			@RequestCount INT = NULL
		
		EXEC dbo.spRequestTracker_TrackWithLockOut
			@RequestTypeID = @RequestTypeID,
			@SourceName = @Source,
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessKey,
			@RequesterKey = @RequestedBy,
			@RequestKey = @RequestKey,
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessID,
			@RequesterID = @CredentialEntityID,
			@CreatedBy = @RequestedBy,
			@ModifiedBy = @RequestedBy,
			@MaxRequestsAllowed = @MaxRequestsAllowed OUTPUT,
			@RequestCount = @RequestCount OUTPUT,
			@IsLocked = @IsLockedOut OUTPUT,
			@RequestTrackerID = @RequestTrackerID OUTPUT
			
		---------------------------------------------------------------------------------------------------
		-- Validate the lock status of the account and determine if that affects
		-- the validity of the user/password verification.
		---------------------------------------------------------------------------------------------------	
		IF @IsLockedOut = 1
		BEGIN
		
			IF @RequestCount = @MaxRequestsAllowed AND @IsValid = 0
			BEGIN
				SET @IsValid = 0;
				SET @IsLockedOut = 1
			END
			ELSE IF @RequestCount >= @MaxRequestsAllowed AND @IsValid = 0
			BEGIN
				SET @IsValid = 0;
				SET @IsLockedOut = 1
			END
			ELSE IF @RequestCount <= @MaxRequestsAllowed AND @IsValid = 1
			BEGIN
				SET @IsValid = 1;
				SET @IsLockedOut = 0 -- Unlock request as it is not really locked.
			END
			ELSE
			BEGIN
				SET @IsValid = 0;
			END
		
		END
	
		-- Set message accordingly due to locked status.
		SET @Message = 
			CASE 
				WHEN @IsLockedOut = 1 THEN 'The requested account is currently locked due to too many unsuccessful attempts. ' +
							'Please try again later.'
				ELSE @Message
			END;
		
		---------------------------------------------------------------------------------------------------
		-- Record  locked event.  
		-- <Summary>
		-- Records the locked event, but only during its first encounter.
		-- </Summary>
		-- <Remarks>
		-- Do not fail the process if the event was
		-- unable to be recorded.
		-- </Remarks>
		---------------------------------------------------------------------------------------------------
		IF @RequestCount = @MaxRequestsAllowed AND @IsValid = 0
		BEGIN
		
			BEGIN TRY
			
				DECLARE @LockOutEventID INT = creds.fnGetCredentialEventID('LO');
				
				DECLARE @LockOutEventData XML = 
					'<CredentialEventData>' +
						dbo.fnXmlWriteElementString('ApplicationID', @ApplicationID) +
						dbo.fnXmlWriteElementString('BusinessID', @BusinessID) +
						dbo.fnXmlWriteElementString('CredentialEntityID', @CredentialEntityID) +
						dbo.fnXmlWriteElementString('CredentialToken', @CredentialToken) +
						dbo.fnXmlWriteElementString('MaxRequestsAllowed', @MaxRequestsAllowed) +
					'</CredentialEventData>';
				
				EXEC creds.spCredentialEventLog_Create
					@EventID = @LockOutEventID,
					@EventSource = @Source,
					@EventDataXml = @LockOutEventData,
					@CreatedBy = @RequestedBy
			
			END TRY
			BEGIN CATCH		
			END CATCH
		
		END	
		
		---------------------------------------------------------------------------------------------------
		-- If a valid response was received from the
		-- query then we need to delete the tracked request as it was successful.
		---------------------------------------------------------------------------------------------------
		IF @CredentialEntityID IS NOT NULL AND @IsValid = 1
		BEGIN
		
			EXEC dbo.spRequestTracker_Delete
				@RequestTrackerID = @RequestTrackerID,
				@ApplicationKey = @ApplicationID,
				@BusinessKey = @BusinessID,
				@RequesterKey = @CredentialEntityID,
				@RequestKey = @RequestKey		
			

			EXEC creds.spMembership_Password_Update
				@CredentialEntityID = @CredentialEntityID,
				@Password = @NewPassword,
				@DatePasswordExpires = @DatePasswordExpires,
				@ModifiedBy = @RequestedBy
			
			EXEC creds.spCredentialToken_Expire
				@Token = @CredentialToken,
				@ModifiedBy = @RequestedBy
			
			SET @IsSuccess = 1;
			SET @Message = 'The password has been successfully changed.';
			
			-- Record Reset Event
			BEGIN TRY
				
					DECLARE @AuthentciationEventID INT = creds.fnGetCredentialEventID('FPRST');
					
					DECLARE @AuthenticationEventData XML = 
						'<CredentialEventData>' +
							dbo.fnXmlWriteElementString('ApplicationID', @ApplicationID) +
							dbo.fnXmlWriteElementString('BusinessID', @BusinessID) +
							dbo.fnXmlWriteElementString('CredentialEntityID', @CredentialEntityID) +
							dbo.fnXmlWriteElementString('CredentialToken', @CredentialToken) +
						'</CredentialEventData>';
					
					EXEC creds.spCredentialEventLog_Create
						@EventID = @AuthentciationEventID,
						@EventSource = @Source,
						@EventDataXml = @AuthenticationEventData,
						@CreatedBy = @RequestedBy
				
				END TRY
				BEGIN CATCH		
				END CATCH	
				
		END	
			
							
	
	END TRY
	BEGIN CATCH
		
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
	
	END CATCH
	
	
	---------------------------------------------------------------------------------------------------
	-- If a valid response was received from the membership
	-- query then we need to delete the tracked request as it was successful.
	---------------------------------------------------------------------------------------------------	
	EOF:		
	SELECT 
		@IsSuccess AS IsSuccess,
		@Message AS Message
	
END
