﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description:	Returns the specified patient Medication object.
-- SAMPLE CALL;
/*
EXEC api.spPhrm_Patient_Medication_Get_Single
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@MedicationID = '1'
 */
-- SELECT * FROM phrm.fnGetPatientInformation(2) 
-- SELECT * FROM dbo.Medication
-- SELECT * FROM dbo.MedicationType
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Medication_Get_Single]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@MedicationID BIGINT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');	

	
	---------------------------------------------------------------------
	-- Return patient Medication object.
	---------------------------------------------------------------------
	EXEC api.spPhrm_Medication_Get_Single
			@MedicationID = @MedicationID,
			@PersonID = @PersonID

			
END
