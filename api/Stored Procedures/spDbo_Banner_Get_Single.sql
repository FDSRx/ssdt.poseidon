﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/24/2014
-- Description:	Returns a list of banners that are applicable to the specified criteria.
/*

DECLARE
	@BannerID BIGINT = 7,
	@BannerTypeKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonKeyType VARCHAR(50) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_Banner_Get_Single
	@BannerID = @BannerID,
	@BannerTypeKey = @BannerTypeKey,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PersonKey = @PersonKey,
	@PersonKeyType = @PersonKeyType,
	@DateStart = @DateStart,
	@DateEnd = @DateEnd,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug


UPDATE trgt 
SET 
	trgt.DateEnd = DATEADD(dd, 5, GETDATE()),
	trgt.DateModified = GETDATE()
FROM dbo.BannerSchedule trgt WHERE BannerID = 6


*/
-- SELECT * FROM dbo.Banner
-- SELECT * FROM dbo.BannerSchedule
-- SELECT * FROM dbo.Application

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spDbo_Banner_Get_Single]
	@BannerID BIGINT = NULL,
	@BannerTypeKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonKeyType VARCHAR(50) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@BannerID=' + dbo.fnToStringOrEmpty(@BannerID)  + ';' +
		'@BannerTypeKey=' + dbo.fnToStringOrEmpty(@BannerTypeKey)  + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@PersonKey=' + dbo.fnToStringOrEmpty(@PersonKey)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationKey AS ApplicationKey, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType
	END

	--------------------------------------------------------------------------------------------
	-- Get object.
	--------------------------------------------------------------------------------------------		
	EXEC api.spDbo_Banner_Get_List
		@BannerID = @BannerID,
		@BannerTypeKey = @BannerTypeKey,
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PersonKey = @PersonKey,
		@PersonKeyType = @PersonKeyType,
		@DateStart = @DateStart,
		@DateEnd = @DateEnd,
		@Skip = @Skip,
		@Take = 1,
		@SortCollection = @SortCollection,
		@Debug = @Debug


				
END