﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/17/2015
-- Description:	Returns a list of known applications.
/*

-- All applications
EXEC api.spDbo_Application_Get_List
	@ApplicationKey = NULL

-- Applications by business
EXEC api.spDbo_Application_Get_List
	@BusinessKey = '10684'

-- Applications by NABP
EXEC api.spDbo_Application_Get_List
	@BusinessKey = '2501624',
	@BusinessKeyType = 'NABP'

*/

-- SELECT * FROM dbo.Application
-- =============================================
CREATE PROCEDURE [api].[spDbo_Application_Get_List]
	@ApplicationKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------

	
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL,
		@CredentialEntityID BIGINT = NULL,
		@PersonID BIGINT = NULL,
		@ApplicationIdArray VARCHAR(MAX)
	
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	SET	@ApplicationIdArray = dbo.fnApplicationKeyTranslator(@ApplicationKey, DEFAULT);
		
	SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);
	

	--------------------------------------------------------------------------------------------
	-- Paging.
	--------------------------------------------------------------------------------------------		
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	

	--------------------------------------------------------------------------------------------
	-- Create result set
	--------------------------------------------------------------------------------------------
	;WITH cteItems AS (
	
		SELECT
			CASE    
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY app.DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY app.DateCreated DESC)
				WHEN @SortField = 'Name' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY app.Name ASC) 
				WHEN @SortField = 'Name' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY app.Name DESC)  
				ELSE ROW_NUMBER() OVER (ORDER BY app.Name ASC)
			END AS RowNumber,
			app.ApplicationID,
			app.Code,
			app.Name,
			app.Description,
			typ.ApplicationTypeID,
			typ.Code AS ApplicationTypeCode,
			typ.Name AS ApplicationTypeName,
			typ.Description AS ApplicationTypeDescription,
			app.ApplicationPath,
			app.DateCreated,
			app.DateModified,
			app.CreatedBy,
			app.ModifiedBy
		--SELECT *
		FROM dbo.Application app
			JOIN dbo.ApplicationType typ
				ON app.ApplicationTypeID = typ.ApplicationTypeID
		WHERE ( @ApplicationKey IS NULL OR app.ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationIdArray, ',') WHERE ISNUMERIC(Value) = 1) )	
			AND ( @BusinessKey IS NULL OR EXISTS (
				SELECT TOP 1 *
				FROM dbo.BusinessEntityApplication bea
				WHERE bea.BusinessEntityID = @BusinessID
					AND bea.ApplicationID = app.ApplicationID
			))
		
				
	)
	
	SELECT 
		ApplicationID,
		Code,
		Name,
		Description,
		ApplicationTypeID,
		ApplicationTypeCode,
		ApplicationTypeName,
		ApplicationPath,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
	FROM cteItems
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
			
END
