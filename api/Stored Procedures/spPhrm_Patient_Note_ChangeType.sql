﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/2/2014
-- Description:	Changes the patient's note type (e.g. Sticky to general, general to task, etc.).
-- Change log:
-- 6/9/2016 - dhughes - Added the Application object as a parameter.

-- SAMPLE CALL:
/*

DECLARE
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteID BIGINT,
	@NoteTypeKey VARCHAR(25),
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL

EXEC api.spPhrm_Patient_Note_ChangeType
	@ApplicationKey = @ApplicationKey
	@BusinessKey = @BusinessKey, 
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType
	@NoteID = @NoteID,
	@NoteTypeKey = @NoteTypeKey,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/

-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Note_History
-- SELECT * FROM dbo.NoteType

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Note_ChangeType]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteID BIGINT,
	@NoteTypeKey VARCHAR(25),
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' +
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@NoteTypeKey=' + dbo.fnToStringOrEmpty(@NoteTypeKey) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;


	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------------------------------------------------------
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @PatientKeyType = ISNULL(@PatientKeyType, 'SourcePatientKey')

	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType),
		@ScopeID INT = dbo.fnGetScopeID('INTRN'), -- only review internal notes
		@NoteTypeID INT = dbo.fnGetNoteTypeID(@NoteTypeKey)

	
	------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, @BusinessKeyType, @PatientKey, @PatientKeyType);
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod, 
			@PatientID AS PatientID, @personID AS PersonID, @BusinessID AS BusinessID, @ApplicationID AS ApplicationID,
			@ApplicationKey AS ApplicationKey, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@Args AS Arguments
	END
	
	---------------------------------------------------------------------
	-- Change the patient's note type.
	-- <Summary>
	-- If the patient was resolved, then update the patient's note.
	-- </Summary>
	---------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
		EXEC dbo.spNote_ChangeType
			@NoteID = @NoteID,
			@ApplicationID = @ApplicationID,
			@BusinessID = @BusinessID,
			@BusinessEntityID = @PersonID,
			@NoteTypeID = @NoteTypeID,
			@ModifiedBy = @ModifiedBy
	END
END
