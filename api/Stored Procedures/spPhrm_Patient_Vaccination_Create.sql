﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/25/2014
-- Description: Creates a new patient Vaccination object.
-- SAMPLE CALL:
/*
DECLARE
	@VaccinationID BIGINT = NULL,
	@DateAdministered DATETIME = GETDATE()

EXEC api.spPhrm_Patient_Vaccination_Create
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@DateAdministered = @DateAdministered,
	@CVXCodeKey = 1,
	@AdministrationRouteKey = 1,
	@AdministrationSiteKey = 1,
	@Dosage = '100 cc',
	@MVXCodeKey = 10,
	@FinancialClassKey = 1,
	@AdministerTypeKey = 1,
	@AdministerFirstName = 'Sarah',
	@AdministerLastName = 'Doe',
	@VaccineLotNumber = 'H107',
	@DateExpires = '1/19/2015',
	@Notes = NULL,
	@CreatedBy = 'dhughes',
	@VaccinationID = @VaccinationID OUTPUT
		

SELECT @VaccinationID AS VaccinationID
	
*/

-- SELECT * FROM vaccine.Vaccination ORDER BY VaccinationID DESC
-- SELECT * FROM phrm.vwPatient WHERE LastName LIKE '%Simmons%'
-- SELECT * FROM vaccine.CVXCode WHERE FullVaccineName LIKE '%infl%'
-- SELECT * FROM vaccine.MVXCode
-- SELECT * FROM vaccine.AdministrationRoute
-- SELECT * FROM vaccine.AdministrationSite
-- SELECT * FROM dbo.FinancialClass
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Vaccination_Create]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@DateAdministered DATETIME,
	@CVXCodeKey VARCHAR(25),
	@AdministrationRouteKey VARCHAR(25),
	@AdministrationSiteKey VARCHAR(25),
	@Dosage VARCHAR(50),
	@MVXCodeKey VARCHAR(25),
	@FinancialClassKey VARCHAR(25),
	@AdministerTypeKey VARCHAR(25) = NULL,
	@AdministerFirstName VARCHAR(75) = NULL,
	@AdministerLastName VARCHAR(75) = NULL,
	@VaccineLotNumber VARCHAR(25) = NULL,
	@DateExpires DATETIME = NULL,
	@Notes VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256),
	@VaccinationID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------
	SET @VaccinationID = NULL;
	
	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@CVXCodeID INT = @CVXCodeKey,
		@AdministrationRouteID INT = vaccine.fnGetAdministrationRouteID(@AdministrationRouteKey),
		@AdministrationSiteID INT = vaccine.fnGetAdministrationSiteID(@AdministrationSiteKey),
		@MVXCodeID INT = vaccine.fnGetMVXCodeID(@MVXCodeKey),
		@FinancialClassID INT = dbo.fnGetFinancialClassID(@FinancialClassKey),
		@AdministerTypeID INT = medical.fnGetAdministerTypeID(@AdministerTypeKey),
		@ErrorMessage VARCHAR(4000)
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');

	---------------------------------------------------------------------------
	-- Argument resolution exceptions.
	-- <Summary>
	-- Validates resolved values.
	-- </Summary>
	---------------------------------------------------------------------------
	-- PersonID
	IF @PersonID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Vaccination object. Object reference (@PatientKey) was not able to be translated. ' +
			'A valid @PatientKey value is required.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	---------------------------------------------------------------------
	-- Create new patient Vaccincation object.
	---------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
	
		EXEC vaccine.spVaccination_Create
			@PersonID = @PersonID,
			@DateAdministered = @DateAdministered,
			@CVXCodeID = @CVXCodeID,
			@AdministrationRouteID = @AdministrationRouteID,
			@AdministrationSiteID = @AdministrationSiteID,
			@Dosage = @Dosage,
			@MVXCodeID = @MVXCodeID,
			@FinancialClassID = @FinancialClassID,
			@AdministerTypeID = @AdministerTypeID,
			@AdministerFirstName = @AdministerFirstName,
			@AdministerLastName = @AdministerLastName,
			@VaccineLotNumber = @VaccineLotNumber,
			@DateExpires = @DateExpires,
			@Notes = @Notes,
			@CreatedBy = @CreatedBy,
			@VaccinationID = @VaccinationID OUTPUT
	END
		
		
	
END
