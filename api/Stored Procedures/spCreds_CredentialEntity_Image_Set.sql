﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/14/2014
-- Description:	Adds/updates a CredentialEntity Image object.
-- SAMPLE CALL:
/*

DECLARE 
	@ImageID BIGINT = NULL
	
EXEC api.spCreds_CredentialEntity_Image_Set
	@ApplicationKey = 2,
	@BusinessKey = '92',
	@CredentialKey = '801018',
	@KeyName = 'ProfilePictureTiny',
	@Name = 'Tiny Profile Picture. Extremely tiny.',
	@FileName = 'DanHughes.JPG',
	@FilePath = '/',
	@CreatedBy = 'dhughes',
	@ModifiedBy = 'dhughes',
	@ImageID = @ImageID OUTPUT

SELECT @ImageID AS ImageID

*/

-- SELECT * FROM dbo.Images
-- SELECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser WHERE LastName LIKE '%Hughes%'
-- SELECT TOP 10 * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialEntity_Image_Set]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@CredentialKey VARCHAR(50),
	@CredentialTypeKey VARCHAR(50) = NULL,
	@ImageID BIGINT = NULL OUTPUT,
	@KeyName VARCHAR(256) = NULL,
	@Name VARCHAR(256) = NULL,
	@Caption VARCHAR(4000) = NULL,
	@Description VARCHAR(MAX) = NULL,
	@FileName VARCHAR(256) = NULL,
	@FilePath VARCHAR(1000) = NULL,
	@FileDataString VARCHAR(MAX) = NULL,
	@FileDataBinary VARBINARY(MAX) = NULL,
	@LanguageCode VARCHAR(5) = NULL,
	@IsDelete BIT = 0,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@LanguageKey VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ImageID=' + dbo.fnToStringOrEmpty(@ImageID) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@CredentialKey=' + dbo.fnToStringOrEmpty(@CredentialKey) + ';' +
		'@KeyName=' + dbo.fnToStringOrEmpty(@KeyName) + ';' +
		'@LanguageKey=' + dbo.fnToStringOrEmpty(@LanguageKey) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	
	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@CredentialEntityID BIGINT,
		@PersonID BIGINT,
		@ApplicationID INT,
		@BusinessID BIGINT = NULL,
		@LanguageID INT = ISNULL(dbo.fnGetLanguageID(@LanguageKey), dbo.fnGetLanguageID('en')),
		@ErrorMessage VARCHAR(4000)
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialKey = @CredentialKey,
		@CredentialTypeKey = @CredentialTypeKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1
	
	---------------------------------------------------------------------------
	-- Argument resolution exceptions.
	-- <Summary>
	-- Validates resolved values.
	-- </Summary>
	---------------------------------------------------------------------------
	-- PersonID
	IF @PersonID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create/update Image object. Object reference (@CredentialKey) was not able to be translated. ' +
			'A valid @CredentialKey value is required.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END


	---------------------------------------------------------------------	
	-- Create a new Patient image object.
	---------------------------------------------------------------------
	IF @PersonID IS NOT NULL
		AND @BusinessID IS NOT NULL
	BEGIN
	
		EXEC dbo.spImage_Set
			@ImageID = @ImageID OUTPUT,
			@ApplicationID = @ApplicationID,
			@BusinessID = @BusinessID,
			@PersonID = @PersonID,
			@KeyName = @KeyName,
			@Name = @Name,
			@Caption = @Caption,
			@Description = @Description,
			@FileName = @FileName,
			@FilePath = @FilePath,
			@FileDataString = @FileDataString,
			@FileDataBinary = @FileDataBinary,
			@LanguageID = @LanguageID,
			@IsDelete = @IsDelete,
			@CreatedBy = @CreatedBy,
			@ModifiedBy = @ModifiedBy
	
	END	
		
END
