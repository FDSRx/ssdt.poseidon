﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/26/2014
-- Description:	Returns a list of all known chains.
-- SAMPLE CALL: EXEC api.spDbo_Chain_Get_List
-- SAMPLE CALL: EXEC api.spDbo_Chain_Get_List @BusinessKey = '18444,18436'
-- SAMPLE CALL: EXEC api.spDbo_Chain_Get_List @Take = 5
-- SAMPLE CALL: EXEC api.spDbo_Chain_Get_List @Name = 'Associated'

-- SELECT * FROM dbo.vwChain 
-- =============================================
CREATE PROCEDURE [api].[spDbo_Chain_Get_List]
	@BusinessKey VARCHAR(MAX) = NULL,
	@Name VARCHAR(256) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)
		
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------	
	
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'BusinessEntityID', 'BusinessID', 'Name', 'ChainID') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;
	

	--------------------------------------------------------------------------------------------
	-- Build results
	--------------------------------------------------------------------------------------------
	;WITH cteChains AS (	
		SELECT
			CASE 
				WHEN @SortField IN ('BusinessEntityID', 'BusinessID', 'ChainID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY BusinessEntityID ASC) 
				WHEN @SortField IN ('BusinessEntityID', 'BusinessID', 'ChainID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY BusinessEntityID DESC)     
				WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY Name ASC) 
				WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY Name DESC)  
				ELSE ROW_NUMBER() OVER (ORDER BY Name ASC)
			END AS RowNumber,	
			BusinessEntityID,
			Name,
			SourceChainID,
			BusinessToken,
			BusinessNumber,
			AddressLine1,
			AddressLine2,
			City,
			State,
			PostalCode,
			Latitude,
			Longitude,
			PhoneNumber,
			EmailAddress,
			FaxNumber
		FROM dbo.vwChain
		WHERE ( @BusinessKey IS NULL OR BusinessEntityID IN ( SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@BusinessKey, ',')) )
			AND ( ( @Name IS NULL OR @Name = '' ) OR Name LIKE '%' + @Name + '%' )
	)
	
	-- Return a pageable result set.
	SELECT
		BusinessEntityID,
		Name,
		SourceChainID,
		BusinessToken,
		BusinessNumber,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		Latitude,
		Longitude,
		PhoneNumber,
		EmailAddress,
		FaxNumber,
		(SELECT COUNT(*) FROM cteChains) AS TotalRecords
	FROM cteChains
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
	
END
