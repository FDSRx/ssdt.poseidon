﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/4/2016
-- Description:	Returns a single BusinessEntityService object.
-- Change log:


/*

DECLARE
	@BusinessEntityServiceID VARCHAR(MAX) = NULL,
	@BusinessEntityID BIGINT = 38,
	@ServiceID VARCHAR(50) = 1,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_BusinessEntityService_Get_Single
	@BusinessEntityServiceID = @BusinessEntityServiceID,
	@BusinessEntityID = @BusinessEntityID,
	@ServiceID = @ServiceID,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug

*/


-- SELECT * FROM dbo.Service	
-- SELECT * FROM dbo.BusinessEntityService WHERE BusinessEntityID = '10684'
-- SELECT * FROM dbo.vwStore WHERE Name LIKE '%Tommy%'
-- =============================================
CREATE PROCEDURE [api].[spDbo_BusinessEntityService_Get_Single]
	@BusinessEntityServiceID BIGINT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@ServiceID VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@BusinessEntityServiceID=' + dbo.fnToStringOrEmpty(@BusinessEntityServiceID)  + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID)  + ';' +
		'@ServiceID=' + dbo.fnToStringOrEmpty(@ServiceID)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);


	------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------


	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@BusinessEntityID AS BusinessEntityID, @ServiceID AS ServiceID
	END
	
	

	
	------------------------------------------------------------------------------------------------------------
	-- Return results
	------------------------------------------------------------------------------------------------------------
	EXEC api.spDbo_BusinessEntityService_Get_List
		@BusinessEntityServiceID = @BusinessEntityServiceID,
		@BusinessEntityID = @BusinessEntityID,
		@ServiceID = @ServiceID,
		@Take = 1,
		@Debug = @Debug		


END