﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/27/2014
-- Description:	Returns a list of chains applicable to a user by (or across) applications.
-- SAMPLE CALL: 
/*
EXEC api.spCreds_CredentialEntity_Chain_Get_List
	@CredentialEntityKey = 959790,
	@ApplicationKey = 2 
	
EXEC api.spCreds_CredentialEntity_Chain_Get_List
	@BusinessKey = '0F7EEA03-3A41-45FD-ADFA-C33956D8AC74',
	@CredentialEntityKey = 801050,
	@ApplicationKey = 2 
	
EXEC api.spCreds_CredentialEntity_Chain_Get_List
	@CredentialEntityKey = 801050,
	@ApplicationKey = 2 

*/

-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM dbo.Application
-- SELECT * FROM dbo.vwChain WHERE Name LIKE '%Associated%'
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialEntity_Chain_Get_List]
	@CredentialEntityKey VARCHAR(50),
	@ApplicationKey VARCHAR(25) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	@ChainKey VARCHAR(MAX) = NULL,
	@ChainKeyType VARCHAR(25) = NULL,
	@Name VARCHAR(256) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, NULLIF(@BusinessKeyType, ''));
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@CredentialEntityID BIGINT = creds.fnGetCredentialEntityID(@CredentialEntityKey),
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = NULL
		
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------	
	-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
	-- business key type.  If a key type was not supplied then attempt a trial and error conversion
	-- based on token or NABP.
	
	-- Auto translater
	SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);	
	-- Busienss token
	SET @BusinessID = CASE WHEN @BusinessID IS NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'BusinessToken') ELSE @BusinessID END;
	-- NABP
	SET @BusinessID = CASE WHEN @BusinessID IS NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'NABP') ELSE @BusinessID END;
	-- Determine if it is a special scenario (i.e. all stores, etc.)
	SET @BusinessID = 
		CASE 
			WHEN @BusinessID IS NULL 
				AND ( ISNUMERIC(@BusinessKey) = 1 AND CONVERT(BIGINT, @BusinessKey) < 0) 
					THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'BusinessID') 
			ELSE @BusinessID 
		END;


	--------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'BusinessEntityID', 'BusinessID', 'Name', 'ChainID') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;

	--------------------------------------------------------------------------------------------
	-- Find the businesses that are applicable to the specified credential and application.
	--------------------------------------------------------------------------------------------
	DECLARE 
		@CredentialBusinessCount INT = 0,
		@CredentialBusinessEntityID BIGINT = NULL
	
	IF OBJECT_ID('tempdb..#tmpBusinesses') IS NOT NULL
	BEGIN
		DROP TABLE #tmpBusinesses;
	END
	
	-- Create temporary store table
	CREATE TABLE #tmpBusinesses (
		Idx INT IDENTITY(1,1),
		BusinessID BIGINT
	);
	
	-- Fetch the store applicable to the credential.
	INSERT INTO #tmpBusinesses (
		BusinessID
	)
	SELECT DISTINCT
		biz.BusinessEntityID
	FROM creds.CredentialEntityBusinessEntity biz
	WHERE biz.CredentialEntityID = @CredentialEntityID
			AND ( ApplicationID = @ApplicationID
				OR ApplicationID IS NULL
			)

	-- Retrieve the number of stores.
	SET @CredentialBusinessCount = @@ROWCOUNT;
	
	-- Retrieve the credential's assigned bussiness.
	SET @CredentialBusinessEntityID = (
		SELECT BusinessEntityID 
		FROM creds.CredentialEntity 
		WHERE CredentialEntityID = @CredentialEntityID
	);
	
	-- If the business was not supplied, use the credentials business.
	SET @BusinessID = ISNULL(@BusinessID, @CredentialBusinessEntityID);
	
	-- Debug
	-- SELECT @BusinessID AS BusinessID, @CredentialBusinessEntityID AS CredentialBusinessEntityID
		
	--------------------------------------------------------------------------------------------
	-- Build results
	--------------------------------------------------------------------------------------------
	;WITH cteChains AS (	
		SELECT
			CASE 
				WHEN @SortField IN ('BusinessEntityID', 'BusinessID', 'ChainID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY BusinessEntityID ASC) 
				WHEN @SortField IN ('BusinessEntityID', 'BusinessID', 'ChainID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY BusinessEntityID DESC)     
				WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY Name ASC) 
				WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY Name DESC)  
				ELSE ROW_NUMBER() OVER (ORDER BY Name ASC)
			END AS RowNumber,	
			BusinessEntityID,
			Name,
			SourceChainID,
			BusinessToken,
			BusinessNumber,
			AddressLine1,
			AddressLine2,
			City,
			State,
			PostalCode,
			Latitude,
			Longitude,
			PhoneNumber,
			EmailAddress,
			FaxNumber
		FROM dbo.vwChain chn
		WHERE @CredentialEntityID IS NOT NULL -- A credential must be supplied.
			AND chn.BusinessEntityID = @BusinessID		
			AND ( @ChainKey IS NULL OR BusinessEntityID IN ( SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@ChainKey, ',')) )
			AND ( ( @Name IS NULL OR @Name = '' ) OR Name LIKE '%' + @Name + '%' )
			-- Retrieve credential store listing.  If the credential had filtered chains then we must only select from that chain set.
			-- If the credential did not have any filtered chains, then retrieve the listing of chains that are applicable to the
			-- business entity id.
			AND ( 
				( @CredentialBusinessCount > 0 AND 
					( chn.BusinessEntityID IN (SELECT BusinessID FROM #tmpBusinesses) ))
				OR @CredentialBusinessCount = 0 AND ( 
					( chn.BusinessEntityID = @CredentialBusinessEntityID OR @CredentialBusinessEntityID = -1 ))
			)
	)
	
	-- Return a pageable result set.
	SELECT
		BusinessEntityID,
		Name,
		SourceChainID,
		BusinessToken,
		BusinessNumber,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		Latitude,
		Longitude,
		PhoneNumber,
		EmailAddress,
		FaxNumber,
		(SELECT COUNT(*) FROM cteChains) AS TotalRecords
	FROM cteChains
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.

		
	--------------------------------------------------------------------------------------------
	-- Dispose of resources
	--------------------------------------------------------------------------------------------
	DROP TABLE #tmpBusinesses;

END
