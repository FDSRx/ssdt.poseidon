﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/9/2014
-- Description:	Gets a list of CalendarItem objects that are applicable to a patient.
-- Change Log:
-- 11/20/2015 - dhughes - Modified the "ROW_NUMBER()" method to only perform the process once vs. multiple scenarios to help increase
--							speed and performance.
-- 11/20/2015 - dhughes - Modified the process to generate the tag(s) XML information after the final record set has been determined.
-- 6/17/2016 - dhughes - Adding logging capabilities of method call.

-- SAMPLE CALL;
/*

DECLARE
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Note object identifiers.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@Location VARCHAR(500) = NULL,	
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spPhrm_Patient_CalendarItem_Get_List
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@NoteKey = @NoteKey,
	@NotebookKey = @NotebookKey,
	@PriorityKey = @PriorityKey,
	@OriginKey = @OriginKey,
	@TagKey = @TagKey,
	@Location = @Location,
	@DataDateStart = @DataDateStart,
	@DataDateEnd = @DataDateEnd,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug


*/

-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.CalendarItem
-- SELECT * FROM schedule.NoteSchedule
-- SELECT * FROM phrm.fnGetPatientInformation(2) 
-- SELECT * FROM dbo.NoteType

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_CalendarItem_Get_List]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Note object identifiers.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@Location VARCHAR(500) = NULL,	
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN


	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey)  + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType)  + ';' +
		'@NoteKey=' + dbo.fnToStringOrEmpty(@NoteKey)  + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey)  + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey)  + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey)  + ';' +
		'@TagKey=' + dbo.fnToStringOrEmpty(@TagKey)  + ';' +
		'@Location=' + dbo.fnToStringOrEmpty(@Location)  + ';' +
		'@DataDateStart=' + dbo.fnToStringOrEmpty(@DataDateStart)  + ';' +
		'@DataDateEnd=' + dbo.fnToStringOrEmpty(@DataDateEnd)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;


	-----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	-----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------------------------------------------------------------
	SET @DataDateStart = ISNULL(@DataDateStart, '1/1/1900');
	SET @Debug = ISNULL(@Debug, 0);
	
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT,
		@PatientID BIGINT,
		@PatientKeyArray VARCHAR(MAX),
		@PersonID BIGINT,
		@ScopeID INT = dbo.fnGetScopeID('INTRN'), -- only review internal notes
		@BusinessID BIGINT = NULL,
		@PatientPictureKey VARCHAR(256) = 'PatientPictureSmall',
		@NoteTypeKey VARCHAR(25)
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Set local variables
	-----------------------------------------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1
		
	SET @PatientKeyArray = phrm.fnPatientKeyTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKeyList'));


	SET @NoteTypeKey = dbo.fnNoteTypeKeyTranslator('CI', DEFAULT);
	SET @NotebookKey = dbo.fnNotebookKeyTranslator(@NotebookKey, DEFAULT);
	SET @PriorityKey = dbo.fnPriorityKeyTranslator(@PriorityKey, DEFAULT);
	SET @OriginKey = dbo.fnOriginKeyTranslator(@OriginKey, DEFAULT);
	SET @TagKey = dbo.fnTagKeyTranslator(@TagKey, DEFAULT);
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Deubber On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeType,
			@BusinessID AS BusinessID, @NoteTypeKey AS NoteTypeKey, @NotebookKey AS NotebookKey, @PriorityKey AS PriorityKey,
			@DataDateStart AS DateStart, @DataDateEnd AS DateEnd
	END

	
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Configure paging setup.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'Title', 'DateStart') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Create result set.
	-----------------------------------------------------------------------------------------------------------------------------
	-- SELECT * FROM dbo.Note
	
	;WITH cteNotes AS (
	
		SELECT
			ROW_NUMBER() OVER (ORDER BY		
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'asc'  THEN n.Title END  ASC,
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'desc'  THEN n.Title END DESC,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'asc'  THEN n.DateCreated END ASC ,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'desc'  THEN n.DateCreated END DESC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'asc'  THEN n.DateModified END ASC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'desc'  THEN n.DateModified END DESC ,     
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'asc'  THEN n.CreatedBy END ASC,
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'desc'  THEN n.CreatedBy END DESC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'asc'  THEN n.ModifiedBy END ASC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'desc'  THEN n.ModifiedBy END  DESC,
				CASE WHEN @SortField IS NULL OR @SortField = '' THEN ci.DateCreated ELSE ci.DateCreated  END DESC
			) AS RowNumber,
			--CASE    
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.DateCreated ASC) 
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.DateCreated DESC)
			--	WHEN @SortField = 'Title' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.Title ASC) 
			--	WHEN @SortField = 'Title' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.Title DESC)  
			--	ELSE ROW_NUMBER() OVER (ORDER BY ci.DateCreated DESC)
			--END AS RowNumber,	
			ci.NoteID AS CalendarItemID,
			pat.PatientID,
			pat.SourcePatientKey,
			psn.FirstName,
			psn.LastName,
			psn.BirthDate,
			(
				SELECT TOP 1 
					ImageID 
				FROM dbo.Images img
				WHERE img.ApplicationID = @ApplicationID
					AND pat.BusinessEntityID = img.BusinessID
					AND pat.PersonID = img.PersonID
					AND img.KeyName = @PatientPictureKey 
			) AS PatientImageID,	
			n.NotebookID AS NotebookID,
			nb.Code AS NotebookCode,
			nb.Name AS NotebookName,
			n.Title,
			n.Memo,
			--(
			--	SELECT t.TagID AS Id, t.Name AS Name
			--	FROM dbo.NoteTag ntag
			--		JOIN dbo.Tag t
			--			ON ntag.TagID = t.TagID
			--	WHERE ntag.NoteID = n.NoteID
			--	FOR XML PATH('Tag'), ROOT('Tags')
			--) AS Tags,
			n.PriorityID,
			p.Code AS PriorityCode,
			p.Name AS PriorityName,
			n.OriginID,
			ds.Code AS OriginCode,
			ds.Name AS OriginName,
			n.OriginDataKey,
			ci.Location,
			ci.DateCreated,
			ci.DateModified,
			ci.CreatedBy,
			ci.ModifiedBy			
		FROM dbo.CalendarItem ci
			JOIN dbo.Note n
				ON ci.NoteID = n.NoteID
			JOIN phrm.Patient pat
				ON n.BusinessEntityID = pat.PersonID
			JOIN dbo.Person psn
				ON pat.PersonID = psn.BusinessEntityID
			LEFT JOIN dbo.NoteType nt
				ON n.NoteTypeID = nt.NoteTypeID
			LEFT JOIN dbo.Priority p
				ON n.PriorityID = p.PriorityID
			LEFT JOIN dbo.Notebook nb
				ON n.NotebookID = nb.NotebookID	
			LEFT JOIN dbo.Origin ds
				ON n.OriginID = ds.OriginID
		WHERE pat.BusinessEntityID = @BusinessID
			AND ScopeID = @ScopeID
			AND ( @PatientKey IS NULL OR (@PatientKey IS NOT NULL AND pat.PatientID IN (SELECT Value FROM dbo.fnSplit(@PatientKeyArray, ',') WHERE ISNUMERIC(Value) = 1))) 
			AND ( @NoteKey IS NULL OR (@NoteKey IS NOT NULL AND ci.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @NoteTypeKey IS NULL OR (@NoteTypeKey IS NOT NULL AND n.NoteTypeID IN (SELECT Value FROM dbo.fnSplit(@NoteTypeKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @NotebookKey IS NULL OR (@NotebookKey IS NOT NULL AND n.NotebookID IN (SELECT Value FROM dbo.fnSplit(@NotebookKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @PriorityKey IS NULL OR (@PriorityKey IS NOT NULL AND n.PriorityID IN (SELECT Value FROM dbo.fnSplit(@PriorityKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @OriginKey IS NULL OR (@OriginKey IS NOT NULL AND n.OriginID IN (SELECT Value FROM dbo.fnSplit(@OriginKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( (@Location IS NULL OR @Location = '' ) OR ( (@Location IS NOT NULL OR @Location <> '') AND ci.Location LIKE '%' + @Location + '%'))
			AND ( @TagKey IS NULL OR (@TagKey IS NOT NULL AND n.NoteID IN (
				SELECT DISTINCT NoteID 
				FROM NoteTag 
				WHERE TagID IN(SELECT Value FROM dbo.fnSplit(@TagKey, ',') WHERE ISNUMERIC(Value) = 1))
			))
			AND ( @DataDateEnd IS NULL OR (@DataDateEnd IS NOT NULL AND (
				ci.DateCreated >= @DataDateStart
					AND ci.DateCreated <= @DataDateEnd
			)))
	)
	
	-- Fetch results from common table expression
	SELECT
		CalendarItemID,
		PatientID,
		SourcePatientKey,
		FirstName,
		LastName,
		BirthDate,
		PatientImageID,
		NotebookID,
		NotebookCode,
		NotebookName,
		Title,
		Memo,
		(
			SELECT t.TagID AS Id, t.Name AS Name
			FROM dbo.NoteTag ntag
				JOIN dbo.Tag t
					ON ntag.TagID = t.TagID
			WHERE ntag.NoteID = res.CalendarItemID
			FOR XML PATH('Tag'), ROOT('Tags')
		) AS Tags,
		--CONVERT(XML, Tags) AS Tags,
		PriorityID,
		PriorityCode,
		PriorityName,
		OriginID,
		OriginCode,
		OriginName,
		OriginDataKey,
		Location,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteNotes) AS TotalRecordCount
	FROM cteNotes res
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.





















/*	
	---------------------------------------------------------------------
	-- Return patient CalendarItem objects.
	---------------------------------------------------------------------
	EXEC api.spDbo_CalendarItem_Get_List
		@NoteKey = @NoteKey,
		@ScopeKey = @ScopeID,
		@BusinessEntityKey = @PersonID,
		@NotebookKey = @NotebookKey,
		@PriorityKey = @PriorityKey,
		@OriginKey = @OriginKey,
		@TagKey = @TagKey,
		@Location = @Location,
		@DataDateStart = @DataDateStart,
		@DataDateEnd = @DataDateEnd,
		@Skip = @Skip,
		@Take = @Take,
		@SortCollection = @SortCollection

*/


			
END
