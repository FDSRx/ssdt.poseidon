﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/26/2015
-- Description:	Returns the OAuthClient object security context.
-- SAMPLE CALL:
/*

EXEC api.spCreds_OAuthClient_SecurityContext_Get
	@ApplicationKey = 'ENG',
	@ClientID = 'EAE24BAD-1ABF-4AD8-8473-DB5435FC6124',
	@SecretKey = 'EA712596-215A-46A9-9CD4-39C3AE01D739'
	--,@RolePrefixKey = 'APPCODE' -- Demonstrates the ability to add a prefix to the role list at an app level

*/

-- SELECT * FROM creds.OAuthClient
-- =============================================
CREATE PROCEDURE [api].[spCreds_OAuthClient_SecurityContext_Get]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ClientID NVARCHAR(256), -- a.k.a. Username
	@SecretKey NVARCHAR(256), -- a.k.a. Password
	@CredentialEntityID BIGINT = NULL OUTPUT, -- a.k.a. Client identifier
	@Name VARCHAR(256) = NULL OUTPUT,
	@IsValid BIT = NULL OUTPUT,
	@Roles VARCHAR(MAX) = NULL OUTPUT,
	@RoleProperty VARCHAR(50) = NULL,
	@RolePrefixKey VARCHAR(50) = NULL,
	@RolePrefixCustomName VARCHAR(50) = NULL,
	@RoleData XML = NULL OUTPUT,
	@Applications VARCHAR(MAX) = NULL OUTPUT,
	@ApplicationProperty VARCHAR(50) = NULL,
	@ApplicationData XML = NULL OUTPUT,
	@Businesses VARCHAR(MAX) = NULL OUTPUT,
	@BusinessProperty VARCHAR(50) = NULL,
	@BusinessData XML = NULL OUTPUT,
	@ListDelimiter VARCHAR(10) = NULL,
	@IsOutputOnly BIT = NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------
	-- Instance variables.
	---------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	/*
	---------------------------------------------------------------------------------------------------
	-- Log request
	---------------------------------------------------------------------------------------------------
	DECLARE 
		@Args VARCHAR(MAX)
	
	SET @Args =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@ClientID=' + dbo.fnToStringOrEmpty(@ClientID) + ';' +
		'@SecretKey=' + dbo.fnToStringOrEmpty(@SecretKey) + ';' +
		'@IsOutputOnly=' + dbo.fnToStringOrEmpty(@IsOutputOnly) + ';' ;
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Arguments = @Args
	*/

	---------------------------------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT,
		@BusinessID BIGINT

	---------------------------------------------------------------------------------------------------
	-- Set variables
	---------------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1

	-- Debug
	--SELECT @ApplicationKey AS ApplicationKey, @ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey,
	--	@BusinessKeyType AS BusinessKeyType, @BusinessID AS BusinessID


	---------------------------------------------------------------------------------------------------
	-- Return data.
	---------------------------------------------------------------------------------------------------
	EXEC creds.spOAuthClient_SecurityContext_Get
		@ApplicationID = @ApplicationID,
		@BusinessID = @BusinessID,
		@ClientID = @ClientID,
		@SecretKey = @SecretKey,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@Name = @Name OUTPUT,
		@IsValid = @IsValid OUTPUT,
		@Roles = @Roles OUTPUT,
		@RoleProperty = @RoleProperty,
		@RolePrefixKey = @RolePrefixKey,
		@RolePrefixCustomName = @RolePrefixCustomName,
		@RoleData = @RoleData OUTPUT,
		@Applications = @Applications OUTPUT,
		@ApplicationProperty = @ApplicationProperty,
		@ApplicationData = @ApplicationData OUTPUT,
		@Businesses = @Businesses OUTPUT,
		@BusinessProperty = @BusinessProperty,
		@BusinessData = @BusinessData OUTPUT,
		@ListDelimiter = @ListDelimiter,
		@IsOutputOnly = @IsOutputOnly


END
