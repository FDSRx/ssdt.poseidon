﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/15/2015
-- Description:	Returns a list of CommunicationMethod objects.
-- SAMPLE CALL:
/*

-- Identifier.
EXEC api.spComm_MessageType_Get_Single
	@MessageTypeKey = 1
	
-- Code.
EXEC api.spComm_MessageType_Get_Single
	@MessageTypeKey = 'WLCME'

*/
-- SELECT * FROM comm.MessageType
-- =============================================
CREATE PROCEDURE [api].[spComm_MessageType_Get_Single]
	@MessageTypeKey VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@MessageTypeKey=' + dbo.fnToStringOrEmpty(@MessageTypeKey) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	

	-------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------
	DECLARE
		@MessageTypeID INT = comm.fnGetMessageTypeID(@MessageTypeKey);
	
	
	-------------------------------------------------------------------------------------
	-- Return object.
	-------------------------------------------------------------------------------------
	IF @MessageTypeID IS NOT NULL
	BEGIN
	
		EXEC api.spComm_MessageType_Get_List
			@MessageTypeKey = @MessageTypeID,
			@Take = 1
		
	END
	
END
