﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/02/2014
-- Description:	Gets a list of CalendarItem objects.
-- Change Log:
-- 11/20/2015 - dhughes - Modified the "ROW_NUMBER()" method to only perform the process once vs. multiple scenarios to help increase
--							speed and performance.
-- 11/20/2015 - dhughes - Modified the process to generate the tag(s) XML information after the final record set has been determined.
-- 4/18/2016 - dhughes - Modified the proedure to return the "AdditionalData" property.
-- 6/17/2016 - dhughes - Modified the procedure to accept the Application and Business object parameters.

-- SAMPLE CALL:
/*

DECLARE 
	@ApplicationKey VARCHAR(50) = 'ENG',
	@BusinessKey VARCHAR(50) = '2501624',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Task object identifiers.
	@ScopeKey VARCHAR(MAX) = NULL, -- NOTE: Key will only be used in absense of Id.
	@BusinessEntityKey VARCHAR(MAX), -- NOTE: A single or comma delimited list of BusinessEntity keys.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Location VARCHAR(500) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1
	
EXEC api.spDbo_CalendarItem_Get_List
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@NoteKey = @NoteKey,
	@ScopeKey = @ScopeKey,
	@BusinessEntityKey = @BusinessEntityKey,
	@NotebookKey = @NotebookKey,
	@PriorityKey = @PriorityKey,
	@OriginKey = @OriginKey,
	@TagKey = @TagKey,
	@DataDateStart = @DataDateStart,
	@DataDateEnd = @DataDateEnd,
	@Location = @Location,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug


	
*/

-- SELECT * FROM dbo.CalendarItem
-- SELECT * FROM schedule.NoteSchedule
-- SELECT * FROM dbo.vwNote
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook
-- SELECT * FROM dbo.Store WHERE Nabp = '2501624'
-- =============================================
CREATE PROCEDURE [api].[spDbo_CalendarItem_Get_List]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Task object identifiers.
	@ScopeKey VARCHAR(MAX) = NULL, -- NOTE: Key will only be used in absense of Id.
	@BusinessEntityKey VARCHAR(MAX), -- NOTE: A single or comma delimited list of BusinessEntity keys.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Location VARCHAR(500) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@NoteKey=' + dbo.fnToStringOrEmpty(@NoteKey)  + ';' +
		'@ScopeKey=' + dbo.fnToStringOrEmpty(@ScopeKey)  + ';' +
		'@BusinessEntityKey=' + dbo.fnToStringOrEmpty(@BusinessEntityKey)  + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey)  + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey)  + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey)  + ';' +
		'@TagKey=' + dbo.fnToStringOrEmpty(@TagKey)  + ';' +
		'@DataDateStart=' + dbo.fnToStringOrEmpty(@DataDateStart)  + ';' +
		'@DataDateEnd=' + dbo.fnToStringOrEmpty(@DataDateEnd)  + ';' +
		'@Location=' + dbo.fnToStringOrEmpty(@Location)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;


	-----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------------------------------------------------------------
	SET @DataDateStart = ISNULL(@DataDateStart, '1/1/1900');
	SET @Debug = ISNULL(@Debug, 0);


	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@NoteTypeKey VARCHAR(25),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType),
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey)
		
	-----------------------------------------------------------------------------------------------------------------------------
	-- Set variables
	-----------------------------------------------------------------------------------------------------------------------------	
	SET @ScopeKey = dbo.fnGetScopeID(@ScopeKey);
	SET @NoteTypeKey = dbo.fnNoteTypeKeyTranslator('CI', DEFAULT);
	SET @NotebookKey = dbo.fnNotebookKeyTranslator(@NotebookKey, DEFAULT);
	SET @PriorityKey = dbo.fnPriorityKeyTranslator(@PriorityKey, DEFAULT);
	SET @OriginKey = dbo.fnOriginKeyTranslator(@OriginKey, DEFAULT);
	SET @TagKey = dbo.fnTagKeyTranslator(@TagKey, DEFAULT);
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Deubber On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationKey AS ApplicationKey, @ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey, 
			@BusinessKeyType AS BusinessKeType,	@BusinessID AS BusinessID, @NoteKey AS NoteKey, @NoteTypeKey AS NoteTypeKey, 
			@NotebookKey AS NotebookKey, @PriorityKey AS PriorityKey, @DataDateStart AS DataDateStart,
			@DataDateEnd AS DataDateEnd
	END
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Paging configuration.
	-----------------------------------------------------------------------------------------------------------------------------	
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'Title', 'DateStart') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Create result set
	-----------------------------------------------------------------------------------------------------------------------------
	-- SELECT * FROM dbo.Note
	
	;WITH cteNotes AS (
	
		SELECT
			ROW_NUMBER() OVER (ORDER BY		
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'asc'  THEN n.Title END  ASC,
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'desc'  THEN n.Title END DESC,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'asc'  THEN n.DateCreated END ASC ,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'desc'  THEN n.DateCreated END DESC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'asc'  THEN n.DateModified END ASC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'desc'  THEN n.DateModified END DESC ,     
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'asc'  THEN n.CreatedBy END ASC,
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'desc'  THEN n.CreatedBy END DESC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'asc'  THEN n.ModifiedBy END ASC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'desc'  THEN n.ModifiedBy END  DESC,
				CASE WHEN @SortField IS NULL OR @SortField = '' THEN ci.DateCreated ELSE ci.DateCreated  END DESC
			) AS RowNumber,
			--CASE    
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.DateCreated ASC) 
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.DateCreated DESC)
			--	WHEN @SortField = 'Title' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.Title ASC) 
			--	WHEN @SortField = 'Title' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.Title DESC)  
			--	ELSE ROW_NUMBER() OVER (ORDER BY ci.DateCreated DESC)
			--END AS RowNumber,	
			ci.NoteID AS CalendarItemID,
			n.NotebookID AS NotebookID,
			nb.Code AS NotebookCode,
			nb.Name AS NotebookName,
			n.Title,
			n.Memo,
			--(
			--	SELECT t.TagID AS Id, t.Name AS Name
			--	FROM dbo.NoteTag ntag (NOLOCK)
			--		JOIN dbo.Tag t
			--			ON ntag.TagID = t.TagID
			--	WHERE ntag.NoteID = n.NoteID
			--	FOR XML PATH('Tag'), ROOT('Tags')
			--) AS Tags,
			n.PriorityID,
			p.Code AS PriorityCode,
			p.Name AS PriorityName,
			n.OriginID,
			ds.Code AS OriginCode,
			ds.Name AS OriginName,
			n.OriginDataKey,
			n.AdditionalData,
			ci.Location,
			ci.DateCreated,
			ci.DateModified,
			ci.CreatedBy,
			ci.ModifiedBy			
		FROM dbo.CalendarItem ci (NOLOCK)
			JOIN dbo.Note n (NOLOCK)
				ON ci.NoteID = n.NoteID
			LEFT JOIN dbo.NoteType nt (NOLOCK)
				ON n.NoteTypeID = nt.NoteTypeID
			LEFT JOIN dbo.Priority p (NOLOCK)
				ON n.PriorityID = p.PriorityID
			LEFT JOIN dbo.Notebook nb (NOLOCK)
				ON n.NotebookID = nb.NotebookID	
			LEFT JOIN dbo.Origin ds (NOLOCK)
				ON n.OriginID = ds.OriginID
		WHERE ( @NoteKey IS NULL OR (@NoteKey IS NOT NULL AND ci.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @BusinessKey IS NULL OR (@BusinessKey IS NOT NULL AND ci.BusinessID = @BusinessID ))
			AND ScopeID = @ScopeKey
			AND ( @BusinessEntityKey IS NULL OR (@BusinessEntityKey IS NOT NULL AND n.BusinessEntityID IN (SELECT Value FROM dbo.fnSplit(@BusinessEntityKey, ',') WHERE ISNUMERIC(Value) = 1))) 
			AND ( @NoteTypeKey IS NULL OR (@NoteTypeKey IS NOT NULL AND n.NoteTypeID IN (SELECT Value FROM dbo.fnSplit(@NoteTypeKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @NotebookKey IS NULL OR (@NotebookKey IS NOT NULL AND n.NotebookID IN (SELECT Value FROM dbo.fnSplit(@NotebookKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @PriorityKey IS NULL OR (@PriorityKey IS NOT NULL AND n.PriorityID IN (SELECT Value FROM dbo.fnSplit(@PriorityKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @OriginKey IS NULL OR (@OriginKey IS NOT NULL AND n.OriginID IN (SELECT Value FROM dbo.fnSplit(@OriginKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( (@Location IS NULL OR @Location = '' ) OR ( (@Location IS NOT NULL OR @Location <> '') AND ci.Location LIKE '%' + @Location + '%'))
			AND ( @TagKey IS NULL OR (@TagKey IS NOT NULL AND EXISTS(
				SELECT TOP 1 *
				FROM NoteTag tmp
				WHERE ci.NoteID = tmp.NoteID
					AND TagID IN(SELECT Value FROM dbo.fnSplit(@TagKey, ',') WHERE ISNUMERIC(Value) = 1))
			))
			AND ( @DataDateEnd IS NULL OR (@DataDateEnd IS NOT NULL AND (
				ci.DateCreated >= @DataDateStart
					AND ci.DateCreated <= @DataDateEnd
			)))
	)
	
	-- Fetch results from common table expression
	SELECT
		CalendarItemID,
		NotebookID,
		NotebookCode,
		NotebookName,
		Title,
		Memo,
		(
			SELECT t.TagID AS Id, t.Name AS Name
			FROM dbo.NoteTag ntag (NOLOCK)
				JOIN dbo.Tag t
					ON ntag.TagID = t.TagID
			WHERE ntag.NoteID = res.CalendarItemID
			FOR XML PATH('Tag'), ROOT('Tags')
		) AS Tags,
		--CONVERT(XML, Tags) AS Tags,
		PriorityID,
		PriorityCode,
		PriorityName,
		OriginID,
		OriginCode,
		OriginName,
		OriginDataKey,
		AdditionalData,
		Location,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteNotes) AS TotalRecordCount
	FROM cteNotes res
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
			
END
