﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/25/2014
-- Description:	Returns a list of Vaccination objects.
-- SAMPLE CALL:
/*

EXEC api.spVaccine_Vaccination_Get_List
	@PersonKey = '135590'

*/

-- SELECT * FROM vaccine.Vaccination
-- SELECT * FROM phrm.vwPatient WHERE LastName LIKE '%Simmons%'
-- SELECT * FROM vaccine.CVXCode WHERE FullVaccineName LIKE '%infl%'
-- SELECT * FROM vaccine.MVXCode
-- SELECT * FROM vaccine.AdministrationRoute
-- SELECT * FROM vaccine.AdministrationSite
-- SELECT * FROM dbo.FinancialClass
-- =============================================
CREATE PROCEDURE [api].[spVaccine_Vaccination_Get_List]
	@VaccinationKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of Vaccination Ids
	@PersonKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of Person Ids
	@CVXCodeKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of CVXCode Ids
	@AdministrationRouteKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of AdmistrationRoute Ids
	@AdministrationSiteKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of AdmistrationSite Ids
	@MVXCodeKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of MVXCode Ids
	@FinancialClassKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of FinancialClass Ids
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--------------------------------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------------------------------
	SET @DataDateEnd = ISNULL(@DataDateEnd, '12/31/9999');
	
	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;
	
	
	--------------------------------------------------------------------------------------------
	-- Create result set
	--------------------------------------------------------------------------------------------
	;WITH cteVaccinations AS
	(
		SELECT
			CASE    
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY vac.DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY vac.DateCreated DESC)
				ELSE ROW_NUMBER() OVER (ORDER BY vac.DateCreated DESC)
			END AS RowNumber,
			vac.VaccinationID,
			vac.PersonID,
			psn.FirstName,
			psn.LastName,
			psn.BirthDate,
			vac.DateAdministered,
			vac.CVXCodeID,
			cvx.Code AS CVXCode,
			cvx.FullVaccineName,
			vac.AdministrationRouteID,
			ar.Code AS AdministrationRouteCode,
			ar.Description AS AdministrationRouteDescription,
			vac.AdministrationSiteID,
			st.Code AS AdministrationSiteCode,
			st.Description AS AdministrationSiteDescription,
			vac.Dosage,
			vac.MVXCodeID,
			mvx.Code AS MVXCode,
			mvx.Name AS MVXName,
			vac.FinancialClassID,
			fc.Code AS FinancialClassCode,
			fc.Description AS FinancialClassDescription,
			vac.AdministerTypeID, 
			atyp.Code AS AdministerTypeCode, 
			atyp.Name AS AdministerTypeName,
            vac.AdministerFirstName, 
            vac.AdministerLastName, 
            vac.VaccineLotNumber, 
            vac.DateExpires,
            vac.Notes,
			vac.DateCreated,
			vac.DateModified,
			vac.CreatedBy,
			vac.ModifiedBy
		FROM vaccine.Vaccination vac
			JOIN dbo.Person psn
				ON vac.PersonID = psn.BusinessEntityID
			LEFT JOIN vaccine.CVXCode cvx
				ON vac.CVXCodeID = cvx.CVXCodeID
			LEFT JOIN vaccine.AdministrationRoute ar
				ON vac.AdministrationRouteID = ar.AdministrationRouteID
			LEFT JOIN vaccine.AdministrationSite st
				ON vac.AdministrationSiteID = st.AdministrationSiteID
			LEFT JOIN vaccine.MVXCode mvx
				ON vac.MVXCodeID = mvx.MVXCodeID
			LEFT JOIN dbo.FinancialClass fc
				ON vac.FinancialClassID = fc.FinancialClassID
			LEFT JOIN medical.AdministerType atyp
				ON vac.AdministerTypeID = atyp.AdministerTypeID
		WHERE ( @VaccinationKey IS NULL OR vac.VaccinationID IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@VaccinationKey, ',')) )
			AND ( @PersonKey IS NULL OR vac.PersonID IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@PersonKey, ',')) )
			AND ( @CVXCodeKey IS NULL OR vac.CVXCodeID IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@CVXCodeKey, ',')) )
			AND ( @AdministrationRouteKey IS NULL OR vac.AdministrationRouteID IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@AdministrationRouteKey, ',')) )
			AND ( @AdministrationSiteKey IS NULL OR vac.AdministrationSiteID IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@AdministrationSiteKey, ',')) )
			AND ( @MVXCodeKey IS NULL OR vac.MVXCodeID IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@MVXCodeKey, ',')) )
			AND ( @FinancialClassKey IS NULL OR vac.FinancialClassID IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@FinancialClassKey, ',')) )
			AND (
				@DataDateStart IS NULL OR (
					vac.DateCreated >= @DataDateStart
						AND vac.DateCreated <= @DataDateEnd
				)
			)
	
	)
	
	-- Fetch results from common table expression
	SELECT
		VaccinationID,
		PersonID,
		FirstName,
		LastName,
		BirthDate,
		DateAdministered,
		CVXCodeID,
		CVXCode,
		FullVaccineName,
		AdministrationRouteID,
		AdministrationRouteCode,
		AdministrationRouteDescription,
		AdministrationSiteID,
		AdministrationSiteCode,
		AdministrationSiteDescription,
		Dosage,
		MVXCodeID,
		MVXCode,
		MVXName,
		FinancialClassID,
		FinancialClassCode,
		FinancialClassDescription,
		AdministerTypeID,
		AdministerTypeCode,
		AdministerTypeName,
		AdministerFirstName,
		AdministerLastName,
		VaccineLotNumber,
		DateExpires,
		Notes,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteVaccinations) AS TotalRecordCount
	FROM cteVaccinations
	WHERE RowNumber > @Skip AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
	
	
	
	
END
