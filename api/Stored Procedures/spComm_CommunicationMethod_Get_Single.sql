﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/15/2015
-- Description:	Returns a single CommunicationMethod object.
-- SAMPLE CALL:
/*

-- Identifier.
EXEC api.spComm_CommunicationMethod_Get_Single
	@CommunicationMethodKey = 1
	
-- Code.
EXEC api.spComm_CommunicationMethod_Get_Single
	@CommunicationMethodKey = 'SMS'

*/
-- SELECT * FROM comm.CommunicationMethod
-- SELECT * FROM msg.MessageType
-- =============================================
CREATE PROCEDURE [api].[spComm_CommunicationMethod_Get_Single]
	@CommunicationMethodKey VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationMethodKey=' + dbo.fnToStringOrEmpty(@CommunicationMethodKey) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	

	-------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------
	DECLARE
		@CommunicationMethodID INT = comm.fnGetCommunicationMethodID(@CommunicationMethodKey);
	
	
	-------------------------------------------------------------------------------------
	-- Return object.
	-------------------------------------------------------------------------------------
	IF @CommunicationMethodID IS NOT NULL
	BEGIN
	
		EXEC api.spComm_CommunicationMethod_Get_List
			@CommunicationMethodKey = @CommunicationMethodID,
			@Take = 1
		
	END
	
END
