﻿-- Author:		Daniel Hughes
-- Create date: 12/16/2016
-- Description:	Returns a single BusinessEntityApplication object.

/*

DECLARE
	@BusinessEntityApplicationID VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = 10,
	@BusinessKey VARCHAR(50) = 34,
	@BusinessKeyType VARCHAR(50) = 'ID',
	@PersonKey VARCHAR(50) = NULL,
	@PersonKeyType VARCHAR(50) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_BusinessEntityApplication_Get_Single
	@BusinessEntityApplicationID = @BusinessEntityApplicationID,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PersonKey = @PersonKey,
	@PersonKeyType = @PersonKeyType,
	@DateStart = @DateStart,
	@DateEnd = @DateEnd,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug


*/


-- SELECT * FROM dbo.BusinessEntityApplication
-- SELECT * FROM dbo.Application
-- SELECT * FROM dbo.Store
-- SELECT * FROM dbo.Business

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spDbo_BusinessEntityApplication_Get_Single]
	@BusinessEntityApplicationID BIGINT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonKeyType VARCHAR(50) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@PersonKey=' + dbo.fnToStringOrEmpty(@PersonKey)  + ';' +
		'@PersonKeyType=' + dbo.fnToStringOrEmpty(@PersonKeyType)  + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart)  + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @DateStart = ISNULL(@DateStart, CONVERT(DATE, GETDATE()));
	SET @DateEnd = ISNULL(@DateEnd, CONVERT(DATE, '12/31/9999'));

	------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL
	
	------------------------------------------------------------------------------------------------------------
	-- Set variables
	------------------------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.

	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey ,
		@BusinessKeyType = @BusinessKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1
	

	SET @BusinessEntityApplicationID = 
		ISNULL(
			@BusinessEntityApplicationID, 
			(
				SELECT 
					BusinessEntityApplicationID 
				FROM dbo.BusinessEntityApplication 
				WHERE BusinessEntityID = @BusinessID
					AND ApplicationID = @ApplicationID
			)
		);

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@BusinessEntityApplicationID AS BusinessEntityApplicationID, @ApplicationKey AS ApplicationKey, 
			@ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@BusinessID AS BusinessID, @DateStart AS DateStart, @DateEnd AS DateEnd
	END
	
	--------------------------------------------------------------------------------------------
	-- Returns a single object.
	--------------------------------------------------------------------------------------------
	IF @BusinessEntityApplicationID IS NOT NULL
	BEGIN
	
		EXEC api.spDbo_BusinessEntityApplication_Get_List
			@BusinessEntityApplicationID = @BusinessEntityApplicationID,
			@Take = 1
			
	END
	
			
END