﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/24/2014
-- Description:	Returns a list of banners that are applicable to the specified criteria.
-- Emily Kizer (10/28/2016): Updated to return banner name as 'Title'
/*

DECLARE
	@BannerID VARCHAR(MAX) = NULL,
	@BannerTypeKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonKeyType VARCHAR(50) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_Banner_Get_List
	@BannerID = @BannerID,
	@BannerTypeKey = @BannerTypeKey,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PersonKey = @PersonKey,
	@PersonKeyType = @PersonKeyType,
	@DateStart = @DateStart,
	@DateEnd = @DateEnd,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug


UPDATE trgt 
SET 
	trgt.DateEnd = DATEADD(dd, 5, GETDATE()),
	trgt.DateModified = GETDATE()
FROM dbo.BannerSchedule trgt WHERE BannerID = 6


*/


-- SELECT * FROM dbo.Banner
-- SELECT * FROM dbo.BannerSchedule
-- SELECT * FROM dbo.Application
-- SELECT * FROM dbo.BannerType

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spDbo_Banner_Get_List]
	@BannerID VARCHAR(MAX) = NULL,
	@BannerTypeKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonKeyType VARCHAR(50) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@BannerID=' + dbo.fnToStringOrEmpty(@BannerID)  + ';' +
		'@BannerTypeKey=' + dbo.fnToStringOrEmpty(@BannerTypeKey)  + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@PersonKey=' + dbo.fnToStringOrEmpty(@PersonKey)  + ';' +
		'@PersonKeyType=' + dbo.fnToStringOrEmpty(@PersonKeyType)  + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart)  + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @DateStart = ISNULL(@DateStart, CONVERT(DATE, GETDATE()));
	SET @DateEnd = ISNULL(@DateEnd, CONVERT(DATE, '12/31/9999'));
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL,
		@CredentialEntityID BIGINT = NULL,
		@PersonID BIGINT = NULL,
		@RowCount BIGINT = NULL
	
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.

	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey ,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialKey = @PersonKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@IsOutputOnly = 1
	
	-- Determine PersonID
	SET @PersonID = @CredentialEntityID;

	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@BannerID AS BannerID, @ApplicationKey AS ApplicationKey, @ApplicationID AS ApplicationID, 
			@BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@BusinessID AS BusinessID, @PersonID AS PersonID, @DateStart AS DateStart, @DateEnd AS DateEnd
	END

	--------------------------------------------------------------------------------------------
	-- Paging.
	--------------------------------------------------------------------------------------------		
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'Title', 'DateStart') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;	


	--------------------------------------------------------------------------------------------
	-- Temporary resources.
	--------------------------------------------------------------------------------------------
	IF OBJECT_ID('#tmpBanners') IS NOT NULL
	BEGIN
		DROP TABLE #tmpBannerSchedules;
	END

	CREATE TABLE #tmpBannerSchedules (
		BannerScheduleID BIGINT,
		DataSource VARCHAR(50)
	);

	--------------------------------------------------------------------------------------------
	-- Fetch identifiers.
	--------------------------------------------------------------------------------------------
	-- Identifier lookup
	IF @BannerID IS NOT NULL
	BEGIN

		INSERT INTO #tmpBannerSchedules (
			BannerScheduleID,
			DataSource
		)
		SELECT
			src.BannerScheduleID,
			'Identifer' AS DataSource
		-- SELECT *
		FROM dbo.BannerSchedule src
			LEFT JOIN #tmpBannerSchedules trgt
				ON trgt.BannerScheduleID = src.BannerScheduleID
		WHERE src.BannerID IN (SELECT Value FROM dbo.fnSplit(@BannerID, ',') WHERE ISNUMERIC(Value) = 1)

		SET @RowCount = @@ROWCOUNT;

		-- Debug
		IF @Debug = 1 
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Identifier lookup' AS ActionMethod, * FROM #tmpBannerSchedules
		END

	END

	-- Strict lookup - Application, Business, Person
	IF @BannerID IS NULL
	BEGIN
		INSERT INTO #tmpBannerSchedules (
			BannerScheduleID,
			DataSource
		)
		SELECT
			src.BannerScheduleID,
			'Application/Business/Person' AS DataSource
		-- SELECT *
		FROM dbo.BannerSchedule src
			LEFT JOIN #tmpBannerSchedules trgt
				ON trgt.BannerScheduleID = src.BannerScheduleID
		WHERE src.ApplicationID = @ApplicationID 
			AND src.BusinessID = @BusinessID 
			AND src.PersonID = @PersonID 
			AND trgt.BannerScheduleID IS NULL

		SET @RowCount = @@ROWCOUNT;

		-- Debug
		IF @Debug = 1 
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Appliation/Business/Person' AS ActionMethod, * FROM #tmpBannerSchedules
		END
	END


	-- Semi strcit lookup - Application, Business
	IF @BannerID IS NULL
	BEGIN
		INSERT INTO #tmpBannerSchedules (
			BannerScheduleID,
			DataSource
		)
		SELECT
			src.BannerScheduleID,
			'Application/Business' AS DataSource
		-- SELECT *
		FROM dbo.BannerSchedule src
			LEFT JOIN #tmpBannerSchedules trgt
				ON trgt.BannerScheduleID = src.BannerScheduleID
		WHERE src.ApplicationID = @ApplicationID
			AND src.BusinessID = @BusinessID
			AND src.PersonID IS NULL
			AND trgt.BannerScheduleID IS NULL

		SET @RowCount = @@ROWCOUNT;

		-- Debug
		IF @Debug = 1 
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Appliation/Business' AS ActionMethod, * FROM #tmpBannerSchedules
		END
	END

	-- Application lookup - Application
	IF @BannerID IS NULL
	BEGIN
		INSERT INTO #tmpBannerSchedules (
			BannerScheduleID,
			DataSource
		)
		SELECT
			src.BannerScheduleID,
			'Application' AS DataSource
		-- SELECT *
		FROM dbo.BannerSchedule src
			LEFT JOIN #tmpBannerSchedules trgt
				ON trgt.BannerScheduleID = src.BannerScheduleID
		WHERE src.ApplicationID = @ApplicationID
			AND src.BusinessID IS NULL
			AND src.PersonID IS NULL
			AND trgt.BannerScheduleID IS NULL

		SET @RowCount = @@ROWCOUNT;

		-- Debug
		IF @Debug = 1 
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Appliation' AS ActionMethod, * FROM #tmpBannerSchedules
		END
	END

	-- Business lookup - Business
	IF @BannerID IS NULL
	BEGIN
		INSERT INTO #tmpBannerSchedules (
			BannerScheduleID,
			DataSource
		)
		SELECT
			src.BannerScheduleID,
			'Business' AS DataSource
		-- SELECT *
		FROM dbo.BannerSchedule src
			LEFT JOIN #tmpBannerSchedules trgt
				ON trgt.BannerScheduleID = src.BannerScheduleID
		WHERE src.ApplicationID IS NULL
			AND src.BusinessID = @BusinessID
			AND src.PersonID IS NULL
			AND trgt.BannerScheduleID IS NULL

		SET @RowCount = @@ROWCOUNT;

		-- Debug
		IF @Debug = 1 
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Business' AS ActionMethod, * FROM #tmpBannerSchedules
		END
	END


	-- Global lookup - Global
	IF @BannerID IS NULL
	BEGIN
		INSERT INTO #tmpBannerSchedules (
			BannerScheduleID,
			DataSource
		)
		SELECT
			src.BannerScheduleID,
			'Global' AS DataSource
		-- SELECT *
		FROM dbo.BannerSchedule src
			LEFT JOIN #tmpBannerSchedules trgt
				ON trgt.BannerScheduleID = src.BannerScheduleID
		WHERE src.ApplicationID IS NULL
			AND src.BusinessID IS NULL
			AND src.PersonID IS NULL
			AND trgt.BannerScheduleID IS NULL

		SET @RowCount = @@ROWCOUNT;

		-- Debug
		IF @Debug = 1 
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Global' AS ActionMethod, * FROM #tmpBannerSchedules
		END
	END

	--------------------------------------------------------------------------------------------
	-- Create result set
	--------------------------------------------------------------------------------------------
	;WITH cteBanners AS (
	
		SELECT
			CASE    
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY b.DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY b.DateCreated DESC)
				WHEN @SortField = 'Title' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY b.Name ASC) 
				WHEN @SortField = 'Title' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY b.Name DESC)  
				WHEN @SortField = 'DateStart' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY schd.DateStart ASC) 
				WHEN @SortField = 'DateStart' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY schd.DateStart DESC)  
				ELSE ROW_NUMBER() OVER (ORDER BY schd.DateCreated DESC)
			END AS RowNumber,
			schd.BannerScheduleID,
			b.BannerID,
			typ.BannerTypeID,
			typ.Code AS BannerTypeCode,
			typ.Name AS BannerTypeName,
			schd.ApplicationID,
			app.Code AS ApplicationCode,
			app.Name AS ApplicationName,
			schd.BusinessID,
			biz.Name AS BusinessName,
			schd.PersonID,
			b.Name AS Title,
			b.Header,
			b.Body,
			b.Footer,
			b.LanguageID,
			lang.Code AS LanguageCode,
			lang.Name AS LanguageName,
			schd.DateStart,
			schd.DateEnd,
			schd.SortOrder,
			b.DateCreated,
			b.DateModified,
			b.CreatedBy,
			b.ModifiedBy
		--SELECT *
		FROM dbo.Banner b (NOLOCK)
			JOIN dbo.BannerSchedule schd (NOLOCK)
				ON schd.BannerID = b.BannerID
			JOIN #tmpBannerSchedules tmp (NOLOCK)
				ON tmp.BannerScheduleID = schd.BannerScheduleID
			JOIN dbo.BannerType typ (NOLOCK)
				ON b.BannerTypeID = typ.BannerTypeID
			JOIN dbo.Language lang (NOLOCK)
				ON b.LanguageID = lang.LanguageID
			LEFT JOIN dbo.Application app
				ON app.ApplicationID = schd.ApplicationID
			LEFT JOIN dbo.Business biz
				ON biz.BusinessEntityID = schd.BusinessID
		WHERE (
			@BannerID IS NOT NULL OR (
				@BannerID IS NULL 
				AND schd.DateStart < @DateEnd
				AND schd.DateEnd > @DateStart
			)
		)							
	)
	
	SELECT 
		BannerScheduleID,
		BannerID,
		BannerTypeID,
		BannerTypeCode,
		BannerTypeName,
		ApplicationID,
		ApplicationCode,
		ApplicationName,
		BusinessID,
		BusinessName,
		PersonID,
		Title,
		Header,
		Body,
		Footer,
		LanguageID,
		LanguageCode,
		LanguageName,
		DateStart,
		DateEnd,
		SortOrder,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		CONVERT(XML, CASE 
			WHEN @BannerID IS NOT NULL THEN (
					SELECT DISTINCT
						app.ApplicationID AS Id,
						app.Code,
						app.Name
					FROM dbo.BannerSchedule tmp
						JOIN dbo.Application app
							ON app.ApplicationID = tmp.ApplicationID
					WHERE tmp.BannerID = res.BannerID
					FOR XML PATH('Application'), ROOT('Applications')
				)
			ELSE NULL
		END) AS ApplicationData,
		CONVERT(XML, CASE 
			WHEN @BannerID IS NOT NULL THEN (
					SELECT DISTINCT
						sto.BusinessEntityID AS BusinessId,
						sto.Nabp,
						sto.Name,
						sto.Name AS StoreName,
						sto.ChainName
					FROM dbo.BannerSchedule tmp
						JOIN dbo.vwStore sto
							ON sto.BusinessEntityID = tmp.BusinessID
					WHERE tmp.BannerID = res.BannerID
					FOR XML PATH('Business'), ROOT('Businesses')
				)
			ELSE NULL
		END) AS BusinessData,
		(SELECT COUNT(*) FROM cteBanners) AS TotalRecordCount
	FROM cteBanners res
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.


	--------------------------------------------------------------------------------------------
	-- Dispose of resources.
	--------------------------------------------------------------------------------------------
	DROP TABLE #tmpBannerSchedules;
	
	
				
END
