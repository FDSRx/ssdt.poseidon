﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/7/2014
-- Description:	Returns the date/time from the server.
-- SAMPLE CALL: api.spSys_DateTime_Now
-- =============================================
CREATE PROCEDURE api.spSys_DateTime_Now
	@DateTime DATETIME = NULL OUTPUT,
	@IsOutputOnly BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	SET @DateTime = GETDATE();
	
	--------------------------------------------------------------------
	-- Return date/time.
	--------------------------------------------------------------------
	IF @IsOutputOnly = 0
	BEGIN
		SELECT @DateTime AS CurrentDateTime
	END
END
