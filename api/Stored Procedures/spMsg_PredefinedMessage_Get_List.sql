﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/23/2014
-- Description:	Returns a list of predefined messages.
-- SAMPLE CALL:
/*

-- List
EXEC api.spMsg_PredefinedMessage_Get_List
	@ApplicationKey = NULL,
	@BusinessKey = NULL,
	@BusinessKeyType = NULL

-- Single item.
EXEC api.spMsg_PredefinedMessage_Get_List
	@ApplicationKey = NULL,
	@BusinessKey = NULL,
	@BusinessKeyType = NULL,
	@PredefinedMessageKey = 1

*/


-- =============================================
CREATE PROCEDURE [api].[spMsg_PredefinedMessage_Get_List]
	@ApplicationKey INT = NULL,
	@BusinessKey BIGINT = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PredefinedMessageKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of PredefinedMessageIds
	@Name VARCHAR(256) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@ApplicationID INT,
		@BusinessID BIGINT = NULL,
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1	


	--------------------------------------------------------------------------------------------
	-- Configure paging setup.
	--------------------------------------------------------------------------------------------
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'Name') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;


	--------------------------------------------------------------------------------------------
	-- Fetch Data
	--------------------------------------------------------------------------------------------
	;WITH cteMessages AS (
			
		SELECT
			CASE    
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY pm.DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY pm.DateCreated DESC)
				WHEN @SortField = 'Name' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY pm.Name ASC) 
				WHEN @SortField = 'Name' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY pm.Name DESC)  
				ELSE ROW_NUMBER() OVER (ORDER BY pm.Name DESC)
			END AS RowNumber,	
			PredefinedMessageID,
			ApplicationID,
			BusinessEntityID,
			pm.Name,
			Header,
			HeaderFilePath,
			HeaderFileName,
			Body,
			BodyFilePath,
			BodyFileName,
			Footer,
			FooterFilePath,
			FooterFileName,
			pm.LanguageID,
			lang.Code AS LanguageCode,
			lang.Name AS LanguageName,
			pm.DateCreated,
			pm.DateModified,
			pm.CreatedBy,
			pm.ModifiedBy
		--SELECT *
		FROM msg.PredefinedMessage pm
			LEFT JOIN dbo.Language lang
				ON pm.LanguageID = lang.LanguageID
		WHERE ( 
			ApplicationID IS NULL
			AND BusinessEntityID IS NULL
			AND ( @PredefinedMessageKey IS NULL 
					OR PredefinedMessageID IN (SELECT Value FROM dbo.fnSplit(@PredefinedMessageKey, ',') WHERE ISNUMERIC(Value) = 1) )
		)
		
		
	)
	
	SELECT
		PredefinedMessageID,
		ApplicationID,
		BusinessEntityID,
		Name,
		Header,
		HeaderFilePath,
		HeaderFileName,
		Body,
		BodyFilePath,
		BodyFileName,
		Footer,
		FooterFilePath,
		FooterFileName,
		LanguageID,
		LanguageCode,
		LanguageName,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteMessages) AS TotalRecordCount
	FROM cteMessages
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
	
	
	
	
	
	
END
