﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/25/2014
-- Description:	Returns a single Vaccination object.
-- SAMPLE CALL: 
/*
EXEC api.spVaccine_Vaccination_Get_Single
	@VaccinationID = 1,
	@PersonID = 135590
*/

-- SELECT * FROM vaccine.Vaccination
-- SELECT * FROM phrm.vwPatient WHERE LastName LIKE '%Simmons%'
-- SELECT * FROM vaccine.CVXCode WHERE FullVaccineName LIKE '%infl%'
-- SELECT * FROM vaccine.MVXCode
-- SELECT * FROM vaccine.AdministrationRoute
-- SELECT * FROM vaccine.AdministrationSite
-- SELECT * FROM dbo.FinancialClass
-- =============================================
CREATE PROCEDURE [api].[spVaccine_Vaccination_Get_Single]
	@VaccinationID BIGINT,
	@PersonID BIGINT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
	
	---------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	---------------------------------------------------------------------------
	IF @VaccinationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to retrieve Vaccincation object. Object reference (@VaccinationID) is not set to an instance of an object. ' +
			'The @VaccinationID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	---------------------------------------------------------------------------
	-- Retrieve Medication object
	---------------------------------------------------------------------------
	EXEC api.spVaccine_Vaccination_Get_List
		@VaccinationKey = @VaccinationID,
		@PersonKey = @PersonID


END
