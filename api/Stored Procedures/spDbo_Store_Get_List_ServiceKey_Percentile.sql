﻿-- =============================================
-- Author:		Daniel Hughes/Steve Simmons
-- Create date: 8/26/2014
-- Description:	Returns a list of all known stores fro a Service in specified percentile partitions
-- Change Log:
-- 11/30/15 - ssimmmons - added a percentile option so we can have multiple paralled jobs running at same time working on different partitions

-- SAMPLE CALL: 
/*

EXEC api.spDbo_Store_Get_List_ServiceKey_Percentile 
	@ServiceKey = 'ENG',
	@StartPercentile = 0,
	@EndPercentile = 100
	
*/

-- SELECT * FROM dbo.vwStore
-- SELECT * FROM dbo.ChainStore
-- SELECT * FROM dbo.Service
-- SELECT * FROM comm.CommunicationConfiguration
-- =============================================
CREATE PROCEDURE [api].[spDbo_Store_Get_List_ServiceKey_Percentile]
	@ServiceKey VARCHAR(MAX) = NULL,
	@StartPercentile smallint = 0,
	@EndPercentile smallint = 0
AS
BEGIN


	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Result schema binding
	-- <Summary>
	-- Returns a schema of the intended result set.  This is a workaround to assist certain
	-- frameworks in determining the correct result sets (i.e. SSIS, Entity Framework, etc.).
	-- </Summary>
	--------------------------------------------------------------------------------------------	
	IF 1 = 0 
	BEGIN
	
		DECLARE @Property VARCHAR(10) = '';
		
		SELECT TOP 1
			CAST(@Property AS VARCHAR(50)) AS BusinessEntityID,
			CAST(@Property AS VARCHAR(256)) AS Name,
			CAST(@Property AS VARCHAR(75)) AS SourceStoreID,
			CAST(@Property AS VARCHAR(75)) AS NABP,
			CAST(@Property AS VARCHAR(50)) AS BusinessToken,
			CAST(@Property AS VARCHAR(50)) AS BusinessNumber,
			CAST(@Property AS VARCHAR(50)) AS ChainID,
			CAST(@Property AS VARCHAR(50)) AS ChainToken,
			CAST(@Property AS VARCHAR(256)) AS AddressLine1,
			CAST(@Property AS VARCHAR(256)) AS AddressLine2,
			CAST(@Property AS VARCHAR(256)) AS City,
			CAST(@Property AS VARCHAR(256)) AS State,
			CAST(@Property AS VARCHAR(256)) AS PostalCode,
			CAST(@Property AS VARCHAR(256)) AS Latitude,
			CAST(@Property AS VARCHAR(256)) AS Longitude,
			CAST(@Property AS VARCHAR(256)) AS Website,
			CAST(@Property AS VARCHAR(256)) AS PhoneNumber,
			CAST(@Property AS VARCHAR(256)) AS EmailAddress,
			CAST(@Property AS VARCHAR(256)) AS FaxNumber,
			CAST(@Property AS VARCHAR(256)) AS TimeZoneCode,
			CAST(@Property AS VARCHAR(256)) AS TimeZoneLocation,
			CAST(@Property AS INT) AS TimeZoneOffSet,
			CAST(@Property AS BIT) AS SupportDST
				
	END	
	
	--------------------------------------------------------------------------------------------
	-- Parameter Sniffing Fix
	-- <Summary>
	-- Saves a copy of the global input variables to a local variable and then the local
	-- variable is used to perform all the necessary tasks within the procedure.  This allows
	-- the optimizer to use statistics instead of a cached plan that might not be optimized
	-- for the sparatic queries.
	-- </Summary>
	--------------------------------------------------------------------------------------------
	DECLARE
		@ServiceKey_Local VARCHAR(MAX) = @ServiceKey,
		@TotalRecords int
	
	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE 
		@ServiceIdArray VARCHAR(MAX)
		
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	SET @ServiceIdArray = dbo.fnServiceKeyTranslator(@ServiceKey_Local, DEFAULT);

	--------------------------------------------------------------------------------------------
	-- Build results
	--------------------------------------------------------------------------------------------
	;WITH cteStores AS (	
		SELECT DISTINCT
			ROW_NUMBER() OVER(ORDER BY sto.BusinessEntityID) AS RowNumber,
			sto.BusinessEntityID,
			sto.Name,
			sto.SourceStoreID,
			sto.NABP,
			sto.BusinessToken,
			sto.BusinessNumber,
			chn.ChainID,
			bizchn.BusinessToken AS ChainToken,
			sto.AddressLine1,
			sto.AddressLine2,
			sto.City,
			sto.State,
			sto.PostalCode,
			sto.Latitude,
			sto.Longitude,
			sto.Website,
			sto.PhoneNumber,
			sto.EmailAddress,
			sto.FaxNumber,
			sto.TimeZoneCode,
			sto.TimeZoneLocation,
			sto.TimeZoneOffSet,
			sto.SupportDST
		--SELECT TOP 1 *
		--SELECT *
		FROM dbo.vwStore sto (NOLOCK)
			LEFT JOIN dbo.ChainStore chn (NOLOCK)
				ON sto.BusinessEntityID = chn.StoreID
			LEFT JOIN dbo.Business bizchn (NOLOCK)
				ON chn.ChainID = bizchn.BusinessEntityID
		WHERE 
			( ( @ServiceKey_Local IS NULL OR @ServiceKey_Local = '' ) OR EXISTS (
				SELECT TOP 1 *
				FROM dbo.BusinessEntityService tmp (NOLOCK)
				WHERE tmp.BusinessEntityID = sto.BusinessEntityID
					AND tmp.ServiceID IN (SELECT Value FROM dbo.fnSplit(@ServiceIdArray, ',') WHERE ISNUMERIC(Value) = 1)
			) )
	)

	-- Return a pageable result set.
	SELECT
		BusinessEntityID,
		Name,
		SourceStoreID,
		NABP,
		BusinessToken,
		BusinessNumber,
		ChainID,
		ChainToken,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		Latitude,
		Longitude,
		Website,
		PhoneNumber,
		EmailAddress,
		FaxNumber,
		TimeZoneCode,
		TimeZoneLocation,
		TimeZoneOffSet,
		SupportDST,
		(SELECT COUNT(*) FROM cteStores) AS TotalRecords
--		CAST((RowNumber/CONVERT(decimal(9,2),(SELECT COUNT(*) FROM cteStores))*100) AS INT)
	FROM cteStores
	WHERE CAST((RowNumber/CONVERT(decimal(9,2),(SELECT COUNT(*) FROM cteStores))*100) AS INT) >= @StartPercentile
		AND CAST((RowNumber/CONVERT(decimal(9,2),(SELECT COUNT(*) FROM cteStores))*100) AS INT) <= @EndPercentile
	ORDER BY RowNumber 
	
END
