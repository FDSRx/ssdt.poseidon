﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/20/2015
-- Description:	Updates a BusinessEntityPreference object.
-- SAMPLE CALL;
/*

DECLARE 
	@Exists BIT = NULL,
	@BusinessEntityPreferenceID BIGINT = NULL

EXEC api.spPhrm_Patient_Preference_Merge
	@BusinessKey = '7871787',
	@PatientKey = '21656',
	@PreferenceKey = 'DNC',
	@ValueBoolean = 1,
	@ModifiedBy = 'dhughes',
	@Exists = @Exists OUTPUT,
	@BusinessEntityPreferenceID = @BusinessEntityPreferenceID OUTPUT

SELECT @Exists AS IsFound, @BusinessEntityPreferenceID AS BusinessEntityPreferenceID

*/

-- SELECT * FROM dbo.BusinessEntityPreference
-- SELECT * FROM dbo.vwPatient 
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Preference_Merge]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@PreferenceKey VARCHAR(50) = NULL OUTPUT,
	@BusinessEntityPreferenceID BIGINT = NULL OUTPUT,
	@ValueString VARCHAR(MAX) = NULL,
	@ValueBoolean BIT = NULL,
	@ValueNumeric DECIMAL(19, 8) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------
	SET @Exists = 0;

	-----------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------
	DECLARE
		@PersonID BIGINT,
		@BusinessID BIGINT,
		@PreferenceID INT
		
		
	-----------------------------------------------------------------------------------------------
	-- Set local variables.
	-----------------------------------------------------------------------------------------------
	EXEC api.spDbo_System_KeyTranslator
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PatientKey = @PatientKey,
		@PatientKeyType = @PatientKeyType,
		@BusinessID = @BusinessID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1	
	
	SET @PreferenceID = dbo.fnGetPreferenceID(@PreferenceKey);
		
	-----------------------------------------------------------------------------------------------
	-- Merge (create or update) BusinessEntityPreference object.
	-----------------------------------------------------------------------------------------------
	EXEC dbo.spBusinessEntityPreference_Merge
		@BusinessEntityPreferenceID = @BusinessEntityPreferenceID OUTPUT,
		@BusinessEntityID = @PersonID,
		@PreferenceID = @PreferenceID,
		@ValueString = @ValueString,
		@ValueBoolean = @ValueBoolean,
		@ValueNumeric = @ValueNumeric,
		@CreatedBy = @CreatedBy,
		@ModifiedBy = @ModifiedBy,
		@Exists = @Exists OUTPUT
	

END
