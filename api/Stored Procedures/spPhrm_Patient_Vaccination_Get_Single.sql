﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/25/2014
-- Description:	Returns the specified patient Vaccination object.
-- SAMPLE CALL;
/*
EXEC api.spPhrm_Patient_Vaccination_Get_Single
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@VaccinationID = '1'
 */


-- SELECT * FROM dbo.Vaccincation
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Vaccination_Get_Single]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@VaccinationID BIGINT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');	

	
	---------------------------------------------------------------------
	-- Return patient Vaccination object.
	---------------------------------------------------------------------
	EXEC api.spVaccine_Vaccination_Get_Single
			@VaccinationID = @VaccinationID,
			@PersonID = @PersonID

			
END
