﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/7/2014
-- Description:	Returns a list of DispositionType objects.
-- SAMPLE CALL: api.spDbo_DispositionType_Get_List
-- =============================================
CREATE PROCEDURE [api].[spDbo_DispositionType_Get_List]
	@DispositionTypeID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		DispositionTypeID,
		Code,
		Name,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	FROM dbo.DispositionType
	WHERE ( @DispositionTypeID IS NULL OR DispositionTypeID IN (SELECT Value FROM dbo.fnSplit(@DispositionTypeID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	ORDER BY Name
	
END
