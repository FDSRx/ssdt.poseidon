﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/10/2014
-- Description:	Updates an Extranet user.
-- SAMPLE CALL:
/*

DECLARE
	-- Credential Identifier
	@CredentialKey VARCHAR(50) = 'dhughes',
	-- Application
	@ApplicationKey VARCHAR(MAX) = NULL,
	-- Business
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	-- Roles
	@RoleKey VARCHAR(MAX) = NULL,
	@DeleteUnspecifiedRoles BIT = NULL,
	-- User
	@Username VARCHAR(256) = NULL,
	@Password VARCHAR(50) = NULL,
	@PasswordQuestionID INT = NULL,
	@PasswordAnswer VARCHAR(500) = NULL,
	--Membership
	@IsApproved BIT = NULL,
	@IsChangePasswordRequired BIT = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@IsDisabled BIT = NULL,
	-- Person
	@PersonID BIGINT = 68323,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderKey VARCHAR(25) = NULL,
	@LanguageKey VARCHAR(25) = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@ThrowCaughtException BIT = 1,
	@Debug BIT = 1

EXEC api.spCreds_ExtranetUser_Update
	@CredentialKey = @CredentialKey,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@RoleKey = @RoleKey,
	@DeleteUnspecifiedRoles = @DeleteUnspecifiedRoles,
	@Username = @Username,
	@Password = @Password,
	@PasswordQuestionID = @PasswordQuestionID,
	@PasswordAnswer = @PasswordAnswer,
	@IsApproved = @IsApproved,
	@IsChangePasswordRequired = @IsChangePasswordRequired,
	@DatePasswordExpires = @DatePasswordExpires,
	@IsDisabled = @IsDisabled,
	@PersonID = @PersonID,
	@Title = @Title,
	@FirstName = @FirstName,
	@LastName = @LastName,
	@MiddleName = @MiddleName,
	@Suffix = @Suffix,
	@BirthDate = @BirthDate,
	@GenderKey = @GenderKey,
	@LanguageKey = @LanguageKey,
	@AddressLine1 = @AddressLine1,
	@AddressLine2 = @AddressLine2,
	@City = @City,
	@State = @State,
	@PostalCode = @PostalCode,
	@PhoneNumber_Home = @PhoneNumber_Home,
	@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
	@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
	@PhoneNumber_Mobile = @PhoneNumber_Mobile,
	@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
	@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
	@EmailAddress_Primary = @EmailAddress_Primary,
	@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
	@EmailAddress_Alternate = @EmailAddress_Alternate,
	@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@ThrowCaughtException = @ThrowCaughtException,
	@Debug = @Debug

	
*/


-- SELECT * FROM creds.CredentialEntity
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- SELECT * FROM dbo.Person WHERE BusinessEntityID = 801053
-- SELECT * FROM dbo.Person_History WHERE BusinessEntityID = 801053
-- SELECT * FROM dbo.Person ORDER BY BusinessEntityID DESC

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spCreds_ExtranetUser_Update]
	-- Credential Identifier
	@CredentialKey VARCHAR(50),
	-- Application
	@ApplicationKey VARCHAR(MAX) = NULL,
	-- Business
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	-- Roles
	@RoleKey VARCHAR(MAX) = NULL,
	@DeleteUnspecifiedRoles BIT = NULL,
	-- User
	@Username VARCHAR(256) = NULL,
	@Password VARCHAR(50) = NULL,
	@PasswordQuestionID INT = NULL,
	@PasswordAnswer VARCHAR(500) = NULL,
	--Membership
	@IsApproved BIT = NULL,
	@IsChangePasswordRequired BIT = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@IsDisabled BIT = NULL,
	-- Person
	@PersonID BIGINT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderKey VARCHAR(25) = NULL,
	@LanguageKey VARCHAR(25) = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@ThrowCaughtException BIT = NULL,
	@Debug BIT = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID), 
		@ErrorMessage VARCHAR(4000),
		@DebugMessage VARCHAR(4000),
		@Trancount INT = @@TRANCOUNT

	DECLARE @Args VARCHAR(MAX) = 
		'@CredentialKey=' + dbo.fnToStringOrEmpty(@CredentialKey) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@RoleKey=' + dbo.fnToStringOrEmpty(@RoleKey) + ';' +
		'@DeleteUnspecifiedRoles=' + dbo.fnToStringOrEmpty(@DeleteUnspecifiedRoles) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@Password=' + dbo.fnToStringOrEmpty(@Password) + ';' +
		'@PasswordQuestionID=' + dbo.fnToStringOrEmpty(@PasswordQuestionID) + ';' +
		'@PasswordAnswer=' + dbo.fnToStringOrEmpty(@PasswordAnswer) + ';' +
		'@IsApproved=' + dbo.fnToStringOrEmpty(@IsApproved) + ';' +
		'@IsChangePasswordRequired=' + dbo.fnToStringOrEmpty(@IsChangePasswordRequired) + ';' +
		'@DatePasswordExpires=' + dbo.fnToStringOrEmpty(@DatePasswordExpires) + ';' +
		'@IsDisabled=' + dbo.fnToStringOrEmpty(@IsDisabled) + ';' +
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName) + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName) + ';' +
		'@MiddleName=' + dbo.fnToStringOrEmpty(@MiddleName) + ';' +
		'@Suffix=' + dbo.fnToStringOrEmpty(@Suffix) + ';' +
		'@BirthDate=' + dbo.fnToStringOrEmpty(@BirthDate) + ';' +
		'@GenderKey=' + dbo.fnToStringOrEmpty(@GenderKey) + ';' +
		'@LanguageKey=' + dbo.fnToStringOrEmpty(@LanguageKey) + ';' +
		'@AddressLine1=' + dbo.fnToStringOrEmpty(@AddressLine1) + ';' +
		'@AddressLine2=' + dbo.fnToStringOrEmpty(@AddressLine2) + ';' +
		'@City=' + dbo.fnToStringOrEmpty(@City) + ';' +
		'@State=' + dbo.fnToStringOrEmpty(@State) + ';' +
		'@PostalCode=' + dbo.fnToStringOrEmpty(@PostalCode) + ';' +
		'@PhoneNumber_Home=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home) + ';' +
		'@PhoneNumber_Home_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsCallAllowed) + ';' +
		'@PhoneNumber_Home_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsTextAllowed) + ';' +
		'@PhoneNumber_Mobile=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile) + ';' +
		'@PhoneNumber_Mobile_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsCallAllowed) + ';' +
		'@PhoneNumber_Mobile_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsTextAllowed) + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary) + ';' +
		'@EmailAddress_Primary_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsEmailAllowed) + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate) + ';' +
		'@EmailAddress_Alternate_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsEmailAllowed) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@ThrowCaughtException=' + dbo.fnToStringOrEmpty(@ThrowCaughtException) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	---------------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	---------------------------------------------------------------------------------------------------------------------------------
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	---------------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------------------------------------------
	SET @DeleteUnspecifiedRoles = ISNULL(@DeleteUnspecifiedRoles, 0);
	SET @Debug = ISNULL(@Debug, 0);
	SET @ThrowCaughtException = ISNULL(@ThrowCaughtException, 0);
	
	
	---------------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT,
		@CredentialEntityID BIGINT,
		@BusinessID BIGINT,
		@GenderID INT = dbo.fnGetGenderID(@GenderKey),
		@LanguageID INT = dbo.fnGetLanguageID(@LanguageKey),
		@Message VARCHAR(4000) = 'The user was successfully updated.'
	
	
	---------------------------------------------------------------------------------------------------------------------------------
	-- Set variables
	---------------------------------------------------------------------------------------------------------------------------------
	-- Translate basic system keys.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@CredentialKey = @CredentialKey,
		--@CredentialTypeKey = 'EXTRANET',
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@IsOutputOnly = 1
		

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationKey AS ApplicationKey, @CredentialKey AS CredentialKey, @BusinessKey AS BusinessKey,
			@BusinessKeyType AS BusinessKeyType, @ApplicationID AS ApplicationID, @BusinessID AS BusinessID,
			@CredentialEntityID AS CredentialEntityID
	END



	---------------------------------------------------------------------------------------------------------------------------------
	-- Update the CredentialEntity object.
	---------------------------------------------------------------------------------------------------------------------------------
	BEGIN TRY
	
		EXEC creds.spExtranetUser_Update
			-- Credential Identifier
			@CredentialEntityID = @CredentialEntityID,
			-- Application
			@ApplicationID = @ApplicationID,
			@ApplicationIDList = @ApplicationKey,
			-- Business
			@BusinessEntityID = @BusinessID,
			-- Roles
			@RoleIDList = @RoleKey,
			@DeleteUnspecifiedRoles = @DeleteUnspecifiedRoles,
			-- User
			@Username = @Username,
			@Password = @Password,
			@PasswordQuestionID = @PasswordQuestionID,
			@PasswordAnswer = @PasswordAnswer,
			--Membership
			@IsApproved = @IsApproved,
			@IsChangePasswordRequired = @IsChangePasswordRequired,
			@DatePasswordExpires = @DatePasswordExpires,
			@IsDisabled = @IsDisabled,
			--Person
			@PersonID = @PersonID,
			@Title = @Title,
			@FirstName = @FirstName,
			@LastName = @LastName,
			@MiddleName = @MiddleName,
			@Suffix = @Suffix,
			@BirthDate = @BirthDate,
			@GenderID = @GenderID,
			@LanguageID = @LanguageID,
			--Person Address
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			--Person Phone
			--	Home
			@PhoneNumber_Home = @PhoneNumber_Home,
			@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
			@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
			-- Mobile
			@PhoneNumber_Mobile = @PhoneNumber_Mobile,
			@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
			@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
			--Person Email
			--	Primary
			@EmailAddress_Primary = @EmailAddress_Primary,
			@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
			--	Alternate
			@EmailAddress_Alternate = @EmailAddress_Alternate,
			@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
			-- Caller
			@ModifiedBy = @ModifiedBy,
			@CreatedBy = @CreatedBy
		
	
	END TRY
	BEGIN CATCH
		
		-- Log and rethrow error (if applicable)
		EXECUTE dbo.spLogException
			@Arguments = @Args
		
		-- re-throw error
		SET @ErrorMessage = ERROR_MESSAGE();
		
		IF @ThrowCaughtException = 1
		BEGIN
			RAISERROR (
				@ErrorMessage, -- Message text.
				16, -- Severity.
				1 -- State.
			);	
		END
		
	END CATCH
	
	
	---------------------------------------------------------------------------------------------------------------------------------
	-- Return response.
	---------------------------------------------------------------------------------------------------------------------------------
	SELECT
		CONVERT(BIT, 
			CASE
				WHEN @ErrorMessage IS NOT NULL THEN 0
				ELSE 1
			END
		) AS IsSuccess,
		CASE 
			WHEN @ErrorMessage IS NOT NULL THEN @ErrorMessage
			ELSE @Message
		END AS Message
	
				
END
