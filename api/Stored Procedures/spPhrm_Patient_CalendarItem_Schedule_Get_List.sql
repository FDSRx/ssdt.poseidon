﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/9/2014
-- Description:	Gets a list of CalendarItem schedule objects that are applicable to a patient.
-- Change Log:
-- 11/20/2015 - dhughes - Modified the "ROW_NUMBER()" method to only perform the process once vs. multiple scenarios to help increase
--							speed and performance.
-- 11/20/2015 - dhughes - Modified the process to generate the tag(s) XML information after the final record set has been determined.
-- 6/17/2016 - dhughes - Modified the procedure to handle logging and tracking of inputs.
-- 6/22/2016 - dhughes - Modified the procedure to use the Business object on the CalendarItem to perform a faster filter.

-- SAMPLE CALL;
/*

DECLARE
	@ApplicationKey VARCHAR(25) = 'ENG',
	@BusinessKey VARCHAR(50) = '7871787',
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = '21656',
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Note object identifiers.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@Location VARCHAR(500) = NULL,
	@DateTimeFrameStart DATETIME = NULL,
	@DateTimeFrameEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spPhrm_Patient_CalendarItem_Schedule_Get_List
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@NoteKey = @NoteKey,
	@NotebookKey = @NotebookKey,
	@PriorityKey = @PriorityKey,
	@OriginKey = @OriginKey,
	@TagKey = @TagKey,
	@Location = @Location,
	@DateTimeFrameStart = @DateTimeFrameStart,
	@DateTimeFrameEnd = @DateTimeFrameEnd,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug


*/

-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.CalendarItem
-- SELECT * FROM schedule.NoteSchedule
-- SELECT * FROM phrm.fnGetPatientInformation(2) 
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_CalendarItem_Schedule_Get_List]
	@ApplicationKey VARCHAR(25) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Note object identifiers.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@Location VARCHAR(500) = NULL,
	@DateTimeFrameStart DATETIME = NULL,
	@DateTimeFrameEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey)  + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType)  + ';' +
		'@NoteKey=' + dbo.fnToStringOrEmpty(@NoteKey)  + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey)  + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey)  + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey)  + ';' +
		'@TagKey=' + dbo.fnToStringOrEmpty(@TagKey)  + ';' +
		'@Location=' + dbo.fnToStringOrEmpty(@Location)  + ';' +
		'@DateTimeFrameStart=' + dbo.fnToStringOrEmpty(@DateTimeFrameStart)  + ';' +
		'@DateTimeFrameEnd=' + dbo.fnToStringOrEmpty(@DateTimeFrameEnd)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;


	-----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@PersonKeyArray VARCHAR(MAX),
		@PatientKeyArray VARCHAR(MAX),
		@ScopeID INT = dbo.fnGetScopeID('INTRN'), -- only review internal notes
		@PatientPictureKey VARCHAR(256) = 'PatientPictureSmall',
		@BusinessID BIGINT = NULL,
		@ApplicationID INT = NULL,
		@NoteTypeKey VARCHAR(25)
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Set local variables.
	-----------------------------------------------------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------------------------------------------------	
	-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
	-- business key type.  If a key type was not supplied then attempt a trial and error conversion
	-- based on token or NABP.
	-----------------------------------------------------------------------------------------------------------------------------	
	-- Auto translater
	SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);	
	-- Busienss token
	SET @BusinessID = CASE WHEN @BusinessID IS NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'BusinessToken') ELSE @BusinessID END;
	-- NABP
	SET @BusinessID = CASE WHEN @BusinessID IS NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'NABP') ELSE @BusinessID END;
	-- Determine if it is a special scenario (i.e. all stores, etc.)
	SET @BusinessID = 
		CASE 
			WHEN @BusinessID IS NULL 
				AND ( ISNUMERIC(@BusinessKey) = 1 AND CONVERT(BIGINT, @BusinessKey) < 0) 
					THEN dbo.fnBusinessIDTranslator(@BusinessKey, 'BusinessID') 
			ELSE @BusinessID 
		END;
		
	SET @PatientKeyArray = phrm.fnPatientKeyTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKeyList'));

	--SET @PersonKeyArray = dbo.fnPersonIDTranslator(@PatientKey, 'PatientIDList');	
	SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationKey);	
	SET @NoteTypeKey = dbo.fnNoteTypeKeyTranslator('CI', DEFAULT);
	SET @NotebookKey = dbo.fnNotebookKeyTranslator(@NotebookKey, DEFAULT);
	SET @PriorityKey = dbo.fnPriorityKeyTranslator(@PriorityKey, DEFAULT);
	SET @OriginKey = dbo.fnOriginKeyTranslator(@OriginKey, DEFAULT);
	SET @TagKey = dbo.fnTagKeyTranslator(@TagKey, DEFAULT);
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Deubber On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeType,
			@BusinessID AS BusinessID, @NoteTypeKey AS NoteTypeKey, @NotebookKey AS NotebookKey, @PriorityKey AS PriorityKey,
			@PatientKeyArray AS PatientKeyArray
	END

	-----------------------------------------------------------------------------------------------------------------------------
	-- Configure paging setup.
	-----------------------------------------------------------------------------------------------------------------------------	
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Create result set
	-----------------------------------------------------------------------------------------------------------------------------
	-- SELECT * FROM dbo.Note
	
	;WITH cteNotes AS (
	
		SELECT
			ROW_NUMBER() OVER (ORDER BY		
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'asc'  THEN n.Title END  ASC,
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'desc'  THEN n.Title END DESC,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'asc'  THEN n.DateCreated END ASC ,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'desc'  THEN n.DateCreated END DESC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'asc'  THEN n.DateModified END ASC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'desc'  THEN n.DateModified END DESC ,     
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'asc'  THEN n.CreatedBy END ASC,
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'desc'  THEN n.CreatedBy END DESC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'asc'  THEN n.ModifiedBy END ASC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'desc'  THEN n.ModifiedBy END  DESC,
				ns.DateStart DESC
			) AS RowNumber,
			--CASE    
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.DateCreated ASC) 
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.DateCreated DESC)
			--	WHEN @SortField = 'DateStart' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY ns.DateStart ASC) 
			--	WHEN @SortField = 'DateStart' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY ns.DateStart DESC)
			--	WHEN @SortField = 'Title' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.Title ASC) 
			--	WHEN @SortField = 'Title' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.Title DESC)  
			--	ELSE ROW_NUMBER() OVER (ORDER BY ns.DateStart DESC)
			--END AS RowNumber,
			ns.NoteScheduleID,
			ci.BusinessID,
			pat.PatientID,
			pat.SourcePatientKey,
			pat.PersonID,
			pat.FirstName,
			pat.LastName,
			pat.BirthDate,
			--(
			--	SELECT TOP 1 
			--		ImageID 
			--	FROM dbo.Images img
			--	WHERE img.ApplicationID = @ApplicationID
			--		AND pat.BusinessEntityID = img.BusinessID
			--		AND pat.PersonID = img.PersonID
			--		AND img.KeyName = @PatientPictureKey 
			--) AS PatientImageID,		
			ci.NoteID AS CalendarItemID,
			n.NotebookID AS NotebookID,
			nb.Code AS NotebookCode,
			nb.Name AS NotebookName,
			n.Title,
			n.Memo,
			--(
			--	SELECT t.TagID AS Id, t.Name AS Name
			--	FROM dbo.NoteTag ntag
			--		JOIN dbo.Tag t
			--			ON ntag.TagID = t.TagID
			--	WHERE ntag.NoteID = n.NoteID
			--	FOR XML PATH('Tag'), ROOT('Tags')
			--) AS Tags,
			n.PriorityID,
			p.Code AS PriorityCode,
			p.Name AS PriorityName,
			n.OriginID,
			ds.Code AS OriginCode,
			ds.Name AS OriginName,
			n.OriginDataKey,
			ci.Location,
			freq.FrequencyTypeID,
			freq.Code AS FrequencyTypeCode,
			freq.Name AS FrequencyTypeName,
			ns.DateStart,
			ns.DateEnd,
			ns.IsAllDayEvent,
			ns.DateEffectiveStart,
			ns.DateEffectiveEnd,
			ci.DateCreated,
			ci.DateModified,
			ci.CreatedBy,
			ci.ModifiedBy
		--SELECT TOP 1 *
		--SELECT *			
		FROM dbo.CalendarItem ci
			JOIN dbo.Note n
				ON n.NoteID = ci.NoteID
			JOIN phrm.PatientDenormalized pat -- SELECT * FROM phrm.PatientDenormalized
				ON pat.StoreID = ci.BusinessID
					AND pat.PersonID = ci.BusinessEntityID
			JOIN schedule.NoteSchedule ns
				ON ci.NoteID = ns.NoteID
			JOIN schedule.FrequencyType freq
				ON ns.FrequencyTypeID = freq.FrequencyTypeID
			LEFT JOIN dbo.NoteType nt
				ON n.NoteTypeID = nt.NoteTypeID
			LEFT JOIN dbo.Priority p
				ON n.PriorityID = p.PriorityID
			LEFT JOIN dbo.Notebook nb
				ON n.NotebookID = nb.NotebookID	
			LEFT JOIN dbo.Origin ds
				ON n.OriginID = ds.OriginID
		WHERE ci.BusinessID = @BusinessID
			--pat.BusinessEntityID = @BusinessID
			AND ScopeID = @ScopeID
			AND ( @NoteKey IS NULL OR (@NoteKey IS NOT NULL AND ci.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteKey, ',') WHERE ISNUMERIC(Value) = 1)))			
			AND ( @PatientKey IS NULL OR (@PatientKey IS NOT NULL AND pat.PatientID IN (SELECT Value FROM dbo.fnSplit(@PatientKeyArray, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @NoteTypeKey IS NULL OR (@NoteTypeKey IS NOT NULL AND n.NoteTypeID IN (SELECT Value FROM dbo.fnSplit(@NoteTypeKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @NotebookKey IS NULL OR (@NotebookKey IS NOT NULL AND n.NotebookID IN (SELECT Value FROM dbo.fnSplit(@NotebookKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @PriorityKey IS NULL OR (@PriorityKey IS NOT NULL AND n.PriorityID IN (SELECT Value FROM dbo.fnSplit(@PriorityKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @OriginKey IS NULL OR (@OriginKey IS NOT NULL AND n.OriginID IN (SELECT Value FROM dbo.fnSplit(@OriginKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( (@Location IS NULL OR @Location = '' ) OR ( (@Location IS NOT NULL OR @Location <> '') AND ci.Location LIKE '%' + @Location + '%'))
			AND ( @DateTimeFrameStart IS NULL OR (@DateTimeFrameStart IS NOT NULL AND (
				ns.DateStart >= @DateTimeFrameStart
				AND ns.DateEnd <= @DateTimeframeEnd
			)))
			AND ( @TagKey IS NULL OR (@TagKey IS NOT NULL AND EXISTS(
				SELECT TOP 1 *
				FROM NoteTag tmp
				WHERE ci.NoteID = tmp.NoteID
					AND TagID IN(SELECT Value FROM dbo.fnSplit(@TagKey, ',') WHERE ISNUMERIC(Value) = 1))
			))

	)
	
	-- Fetch results from common table expression
	SELECT
		NoteScheduleID,
		PatientID,
		SourcePatientKey,
		FirstName,
		LastName,
		BirthDate,
		(
			SELECT TOP 1 
				ImageID 
			FROM dbo.Images img
			WHERE img.ApplicationID = @ApplicationID
				AND img.BusinessID = res.BusinessID
				AND img.PersonID = res.PersonID
				AND img.KeyName = @PatientPictureKey 
		) AS PatientImageID,
		CalendarItemID,
		NotebookID,
		NotebookCode,
		NotebookName,
		Title,
		Memo,
		(
			SELECT t.TagID AS Id, t.Name AS Name
			FROM dbo.NoteTag ntag
				JOIN dbo.Tag t
					ON ntag.TagID = t.TagID
			WHERE ntag.NoteID = res.CalendarItemID
			FOR XML PATH('Tag'), ROOT('Tags')
		) AS Tags,
		--CONVERT(XML, Tags) AS Tags,
		PriorityID,
		PriorityCode,
		PriorityName,
		OriginID,
		OriginCode,
		OriginName,
		OriginDataKey,
		Location,
		FrequencyTypeID,
		FrequencyTypeCode,
		FrequencyTypeName,
		DateStart,
		DateEnd,
		IsAllDayEvent,
		DateEffectiveStart,
		DateEffectiveEnd,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteNotes) AS TotalRecordCount
	FROM cteNotes res
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
			
	
	
	
	
	
	
	
	/*
	-----------------------------------------------------------------------------------------------------------------------------
	-- Return patient CalendarItem objects.
	-----------------------------------------------------------------------------------------------------------------------------
	EXEC api.spDbo_CalendarItem_Schedule_Get_List
		@NoteKey = @NoteKey,
		@ScopeKey = @ScopeID,
		@BusinessEntityKey = @PersonID,
		@NotebookKey = @NotebookKey,
		@PriorityKey = @PriorityKey,
		@OriginKey = @OriginKey,
		@TagKey = @TagKey,
		@Location = @Location,
		@DateTimeFrameStart = @DateTimeFrameStart,
		@DateTimeFrameEnd = @DateTimeFrameEnd,
		@Skip = @Skip,
		@Take = @Take,
		@SortCollection = @SortCollection
	*/
			
END
