﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/25/2014
-- Description:	Sets a device address (i.e. phone number or email address) as a Patient's
--	preferred contact method.
-- SAMPLE CALL:
/*

EXEC api.spPhrm_Patient_CommunicationMethod_PreferredDevice_Set
	@BusinessKey = '7871787',
	@PatientKey = '21656',
	@CommunicationMethodKey = 2,
	--@DeviceAddressID = 1569,
	@DeviceAddress = 'dhughes@hcc-care.com',
	@IsPreferred = 1,
	@ModifiedBy = 'dhughes'

*/

-- SELECT * FROM dbo.BusinessEntityPhone WHERE BusinessEntityID = 135590
-- SELECT * FROM dbo.BusinessEntityEmailAddress WHERE BusinessEntityID = 135590
-- SELECT * FROM phrm.vwPatient WHERE PersonID = 135590
-- SELECT * FROM comm.CommunicationMethod
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_CommunicationMethod_PreferredDevice_Set]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@CommunicationMethodKey VARCHAR(50) = NULL,
	@DeviceTypeKey VARCHAR(50) = NULL,
	@DeviceAddressID BIGINT = NULL,
	@DeviceAddress VARCHAR(256) = NULL,
	@IsPreferred BIT = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' +
		'@CommunicationMethodKey=' + dbo.fnToStringOrEmpty(@CommunicationMethodKey) + ';' +
		'@DeviceTypeKey=' + dbo.fnToStringOrEmpty(@DeviceTypeKey) + ';' +
		'@DeviceAddressID=' + dbo.fnToStringOrEmpty(@DeviceAddressID) + ';' +
		'@DeviceAddress=' + dbo.fnToStringOrEmpty(@DeviceAddress) + ';' +
		'@IsPreferred=' + dbo.fnToStringOrEmpty(@IsPreferred) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' 

	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	---------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);

	---------------------------------------------------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------------------------------------------------
	DECLARE
		@CommunicationMethodID VARCHAR(25) = comm.fnGetCommunicationMethodID(@CommunicationMethodKey),
		@ErrorMessage VARCHAR(4000),
		@ApplicationID INT,
		@BusinessID BIGINT,
		@PatientID BIGINT,
		@PersonID BIGINT,
		@CommunicationMethodCode VARCHAR(50) = comm.fnGetCommunicationMethodCode(@CommunicationMethodKey),
		@DeviceTypeID INT
	
	---------------------------------------------------------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------------------------------------------------------
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PatientKey = @PatientKey,
		@PatientKeyType = @PatientKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@PatientID = @PatientID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1

	-- Determine DeviceTypeID by investigating the CommunicationMethod object and the DeviceTypeKey object.
	IF @CommunicationMethodCode IN ('EMAIL')
	BEGIN
		SET @DeviceTypeID = dbo.fnGetEmailAddressTypeID(@DeviceTypeKey);
	END
	ELSE IF @CommunicationMethodCode IN ('PHONE', 'VOICE', 'SMS', 'TEXT')
	BEGIN
		SET @DeviceTypeID = dbo.fnGetPhoneNumberTypeID(@DeviceTypeKey);
	END

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT @ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @PatientID AS PatientID, @PersonID AS PersonID,
			@CommunicationMethodID AS CommunicationMethodID, @CommunicationMethodCode AS CommunicationMethodCode, 
			@DeviceTypeID AS DeviceTypeID
	END

	---------------------------------------------------------------------------------------------------------------------
	-- Set patient preferred device address
	---------------------------------------------------------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
		EXEC dbo.spBusinessEntity_CommunicationMethod_PreferredDevice_Set
			@BusinessEntityID = @PersonID,
			@CommunicationMethodID = @CommunicationMethodID,
			@DeviceTypeID = @DeviceTypeID,
			@DeviceAddressID = @DeviceAddressID,
			@DeviceAddress = @DeviceAddress,
			@IsPreferred = @IsPreferred,
			@ModifiedBy = @ModifiedBy
	END

	

END
