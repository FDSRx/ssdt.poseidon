﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/12/2014
-- Description:	Indicates whether a patient exists and has the specified service.
-- SAMPLE CALL: 
/*
DECLARE @Exists BIT, @HasService BIT, @IsSelfOperated BIT

EXEC api.spPhrm_Patient_IsInService
	@Nabp = '0105886', 
	@SourcePatientKey = '29295',
	@ServiceKey = 'MPC',
	@PatientExists = @Exists OUTPUT,
	@HasService = @HasService OUTPUT,
	@IsSelfOperated = @IsSelfOperated OUTPUT

SELECT @Exists AS IsFound, @HasService AS HasService, @IsSelfOperated AS IsSelfOperated
*/
--SELECT * FROM phrm.vwPatient
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_IsInService]
	--Business
	@StoreID INT = NULL,
	@Nabp VARCHAR(25) = NULL,
	@BusinessEntityID BIGINT = NULL,
	--Person
	@PersonID BIGINT = NULL OUTPUT,
	--Patient
	@PatientID BIGINT = NULL OUTPUT,
	@SourcePatientKey VARCHAR(256) = NULL OUTPUT,
	--Service
	@ServiceKey VARCHAR(25) = NULL,
	@ServiceID INT = NULL,
	--Output
	@PatientExists BIT = NULL OUTPUT,
	@HasService BIT = NULL OUTPUT,
	@IsSelfOperated BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------
	SET @PatientExists = 0;
	SET	@HasService = 0;
	SET @IsSelfOperated = 0;

	-----------------------------------------------------------------------
	-- Translate raw data keys into business objects.
	-----------------------------------------------------------------------
	-- Get BusinessEntityID (Store)
	SET @BusinessEntityID = 
		CASE 
			WHEN @BusinessEntityID IS NOT NULL THEN @BusinessEntityID
			WHEN @StoreID IS NOT NULL THEN dbo.fnGetStoreIDFromSourceID(@StoreID)
			WHEN ISNULL(@Nabp, '') <> '' THEN dbo.fnGetStoreIDByNABP(@Nabp)
			ELSE NULL
		END;

	-- Get ServiceID.
	SET @ServiceID = ISNULL(@ServiceID, dbo.fnGetServiceID(@ServiceKey));

			
	-------------------------------------------------
	-- Determine if the patient exists and has the
	-- provided service.
	-------------------------------------------------
	EXEC phrm.spPatient_IsInService
		@BusinessEntityID = @BusinessEntityID, 
		@PersonID = @PersonID OUTPUT,
		@PatientID = @PatientID OUTPUT,		
		@SourcePatientKey = @SourcePatientKey,
		@ServiceID = @ServiceID,
		@PatientExists = @PatientExists OUTPUT,
		@HasService = @HasService OUTPUT,
		@IsSelfOperated = @IsSelfOperated OUTPUT
		


	
		
END
