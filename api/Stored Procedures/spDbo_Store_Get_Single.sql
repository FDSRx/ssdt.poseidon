﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/4/2014
-- Description:	Returns a single Store object.
-- SAMPLE CALL:
/*
	-- BusinessEntityID
	EXEC api.spDbo_Store_Get_Single
		@BusinessKey = '11061'
		
	EXEC api.spDbo_Store_Get_Single
		@BusinessKey = '7871787',
		@BusinessKeyType = 'NABP'
*/

-- SELECT * FROM dbo.Store WHERE Name LIKE '%Tommy%'
-- =============================================
CREATE PROCEDURE [api].[spDbo_Store_Get_Single]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';'

	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args

	*/

	--------------------------------------------------------------------------------------------------
	-- Unique identiifer short circit.
	-- <Summary>
	-- Uses the unique identifier to extract the data vs. the generic process of determining the
	-- internal identifier via the key and key type.
	-- </Summary>
	-- <Remarks>
	-- SMS - 6/18/15 - Example of how Dan Goldberg method is a big performance drain 'BusinessToken'
	-- </Remarks>
	--------------------------------------------------------------------------------------------------	
	IF @BusinessKeyType IS NULL 
		AND dbo.fnIsUniqueIdentifier(@BusinessKey) = 1
	BEGIN
			
		SELECT TOP 1
			1 AS RowNumber,	
			sto.BusinessEntityID,
			sto.Name,
			sto.SourceStoreID,
			sto.NABP,
			sto.BusinessToken,
			sto.BusinessNumber,
			chn.ChainID,
			bizchn.BusinessToken AS ChainToken,
			sto.AddressLine1,
			sto.AddressLine2,
			sto.City,
			sto.State,
			sto.PostalCode,
			sto.Latitude,
			sto.Longitude,
			sto.Website,
			sto.PhoneNumber,
			sto.EmailAddress,
			sto.FaxNumber,
			sto.TimeZoneCode,
			sto.TimeZoneLocation,
			sto.TimeZoneOffSet,
			sto.SupportDST
		FROM dbo.vwStore sto
			LEFT JOIN dbo.ChainStore chn
				ON sto.BusinessEntityID = chn.StoreID
			LEFT JOIN dbo.Business bizchn
				ON chn.ChainID = bizchn.BusinessEntityID
		WHERE sto.BusinessToken = @BusinessKey

		RETURN; -- Exit code
	END
		
	--------------------------------------------------------------------------------------------
	-- Declare variables
	--------------------------------------------------------------------------------------------
	DECLARE
		@BusinessID BIGINT = NULL			

	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------	
	-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
	-- business key type.  If a key type was not supplied then attempt a trial and error conversion
	-- based on token or NABP.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1
	
	-- If a business was translated then attempt to fetch the store data.
	IF @BusinessID IS NOT NULL
	BEGIN	
		--------------------------------------------------------------------------------------------
		-- Return Store object.
		--------------------------------------------------------------------------------------------
		EXEC api.spDbo_Store_Get_List
			@BusinessKey = @BusinessID,
			@Take = 1
	END
	
		    

	
	
	
	
	
	
END

