﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/29/2014
-- Description:	Retrieves a list of ContactType objects.
-- SAMPLE CALL: api.spDbo_ContactType_Get_List
-- =============================================
CREATE PROCEDURE [api].[spDbo_ContactType_Get_List]
	@ContactTypeID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------
	-- Return ContactType objects
	-----------------------------------------------------------------
	SELECT
		ContactTypeID,
		Code,
		Name
	FROM dbo.ContactType
	WHERE ( @ContactTypeID IS NULL OR ContactTypeID IN (SELECT Value FROM dbo.fnSplit(@ContactTypeID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	ORDER BY Name
	
	
END
