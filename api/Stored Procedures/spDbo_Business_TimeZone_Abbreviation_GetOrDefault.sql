﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 5/19/2015
-- Description:	Returns the abbreviated time zone for the provided business. Returns the system defined
--				default time zone if one is not able to located for the provided store.
-- SAMPLE CALL:
/*
	DECLARE @Abbreviation VARCHAR(10) = NULL
	
	EXEC api.spDbo_Business_TimeZone_Abbreviation_GetOrDefault
		@BusinessKey = '2501624',
		@BusinessKeyType = 'NABP',
		@Abbreviation = @Abbreviation OUTPUT
	
	SELECT @Abbreviation AS Abbreviation
*/

-- SELECT * FROM dbo.TimeZone
-- =============================================
CREATE PROCEDURE [api].[spDbo_Business_TimeZone_Abbreviation_GetOrDefault]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@IsOutputOnly BIT = NULL,
	@Abbreviation VARCHAR(10) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	SET @Abbreviation = NULL;
	
	-------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------	
	SELECT @Abbreviation = Poseidon.dbo.fnGetBusinessTimeZoneAbbreviationOrDefault(@BusinessKey, @BusinessKeyType);
	
	-------------------------------------------------------------------------------
	-- Return results (if applicable)
	-------------------------------------------------------------------------------	
	IF @IsOutputOnly = 0
	BEGIN
		SELECT @Abbreviation AS Abbreviation
	END	
	

END
