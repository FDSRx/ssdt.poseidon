﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Gets a list of Note objects.
-- Change Log:
-- 11/19/2015 - dhughes - Modified the "ROW_NUMBER()" method to only perform the process once vs. multiple scenarios to help increase
--							speed and performance.
-- 11/19/2015 - dhughes - Modified the process to generate the tag(s) XML information after the final record set has been determined.

-- SAMPLE CALL:
/*

DECLARE
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Note object identifiers.
	@ApplicationKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeKey VARCHAR(MAX) = 2, -- NOTE: Key will only be used in absense of Id.
	@BusinessEntityKey VARCHAR(MAX) = '135590', -- NOTE: A single or comma delimited list of BusinessEntity keys.
	@NoteTypeKey VARCHAR(MAX) = 'GNRL', -- Single or comma delimited list of NoteType object keys.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_Note_Get_List
	@NoteKey = @NoteKey,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@ScopeKey = @ScopeKey,
	@BusinessEntityKey = @BusinessEntityKey,
	@NoteTypeKey = @NoteTypeKey,
	@NotebookKey = @NotebookKey,
	@PriorityKey = @PriorityKey,
	@OriginKey = @OriginKey,
	@TagKey = @TagKey,
	@DataDateStart = @DataDateStart,
	@DataDateEnd = @DataDateEnd,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug
	
*/
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook
-- SELECT * FROM dbo.Tag

-- SELECT * FROM dbo.ErrorLog
-- SELECT * FROM dbo.InformationLog
-- =============================================
CREATE PROCEDURE [api].[spDbo_Note_Get_List]
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Note object identifiers.
	@ApplicationKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeKey VARCHAR(MAX) = NULL, -- NOTE: Key will only be used in absense of Id.
	@BusinessEntityKey VARCHAR(MAX), -- NOTE: A single or comma delimited list of BusinessEntity keys.
	@NoteTypeKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of NoteType object keys.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteKey=' + dbo.fnToStringOrEmpty(@NoteKey) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@ScopeKey=' + dbo.fnToStringOrEmpty(@ScopeKey) + ';' +
		'@BusinessEntityKey=' + dbo.fnToStringOrEmpty(@BusinessEntityKey) + ';' +
		'@NoteTypeKey=' + dbo.fnToStringOrEmpty(@NoteTypeKey) + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey) + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey) + ';' +
		'@TagKey=' + dbo.fnToStringOrEmpty(@TagKey) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		'@DataDateStart=' + dbo.fnToStringOrEmpty(@DataDateStart) + ';' +
		'@DataDateEnd=' + dbo.fnToStringOrEmpty(@DataDateEnd) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------------------------------------------------------
	SET @DataDateStart = ISNULL(@DataDateStart, '1/1/1900');
	
	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
	DECLARE
		@BusinessID BIGINT = NULL
		
	------------------------------------------------------------------------------------------------------------
	-- Set variables
	------------------------------------------------------------------------------------------------------------	
	SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);
	SET @ScopeKey = dbo.fnGetScopeID(@ScopeKey);
	SET @ApplicationKey = dbo.fnApplicationKeyTranslator(@ApplicationKey, DEFAULT);
	SET @NoteTypeKey = dbo.fnNoteTypeKeyTranslator(@NoteTypeKey, DEFAULT);
	SET @NotebookKey = dbo.fnNotebookKeyTranslator(@NotebookKey, DEFAULT);
	SET @PriorityKey = dbo.fnPriorityKeyTranslator(@PriorityKey, DEFAULT);
	SET @OriginKey = dbo.fnOriginKeyTranslator(@OriginKey, DEFAULT);
	SET @TagKey = dbo.fnTagKeyTranslator(@TagKey, DEFAULT);
	
	-- debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod, 
			@NoteTypeKey AS NoteTypeKey, @NotebookKey AS NotebookKey, @PriorityKey AS PriorityKey,
			@ApplicationKey AS ApplicationKey, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@Args AS Arguments
	END
	
	------------------------------------------------------------------------------------------------------------
	-- Paging configuration.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'Title') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;
	
	------------------------------------------------------------------------------------------------------------
	-- Create result set
	------------------------------------------------------------------------------------------------------------
	-- SELECT * FROM dbo.Note
	
	;WITH cteNotes AS (
		
		SELECT
			ROW_NUMBER() OVER (ORDER BY				
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'asc'  THEN n.Title END  ASC,
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'desc'  THEN n.Title END DESC,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'asc'  THEN n.DateCreated END ASC ,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'desc'  THEN n.DateCreated END DESC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'asc'  THEN n.DateModified END ASC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'desc'  THEN n.DateModified END DESC ,     
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'asc'  THEN n.CreatedBy END ASC,
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'desc'  THEN n.CreatedBy END DESC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'asc'  THEN n.ModifiedBy END ASC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'desc'  THEN n.ModifiedBy END  DESC,
				CASE WHEN @SortField IS NULL OR @SortField = '' THEN n.NoteID ELSE n.NoteID  END DESC
			) AS RowNumber,
			--CASE    
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY n.DateCreated ASC) 
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY n.DateCreated DESC)
			--	WHEN @SortField = 'Title' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY n.Title ASC) 
			--	WHEN @SortField = 'Title' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY n.Title DESC)  
			--	ELSE ROW_NUMBER() OVER (ORDER BY n.NoteID DESC)
			--END AS RowNumber,	
			n.NoteID,
			n.NoteTypeID,
			n.ApplicationID,
			app.Code AS ApplicationCode,
			app.Name AS ApplicationName,
			nt.Code AS NoteTypeCode,
			nt.Name AS NoteTypeName,
			n.NotebookID AS NotebookID,
			nb.Code AS NotebookCode,
			nb.Name AS NotebookName,
			n.Title,
			n.Memo,
			--(
			--	SELECT t.TagID AS Id, t.Name AS Name
			--	FROM dbo.NoteTag ntag (NOLOCK)
			--		JOIN dbo.Tag t
			--			ON ntag.TagID = t.TagID
			--	WHERE ntag.NoteID = n.NoteID
			--	FOR XML PATH('Tag'), ROOT('Tags')
			--) AS Tags,
			n.PriorityID,
			p.Code AS PriorityCode,
			p.Name AS PriorityName,
			n.OriginID,
			ds.Code AS OriginCode,
			ds.Name AS OriginName,
			n.OriginDataKey,
			n.DateCreated,
			n.DateModified,
			n.CreatedBy,
			n.ModifiedBy			
		FROM dbo.Note n (NOLOCK)
			LEFT JOIN dbo.NoteType nt (NOLOCK)
				ON n.NoteTypeID = nt.NoteTypeID
			LEFT JOIN dbo.Priority p (NOLOCK)
				ON n.PriorityID = p.PriorityID
			LEFT JOIN dbo.Notebook nb (NOLOCK)
				ON n.NotebookID = nb.NotebookID
			LEFT JOIN dbo.Origin ds (NOLOCK)
				ON n.OriginID = ds.OriginID
			LEFT JOIN dbo.Application app (NOLOCK)
				ON app.ApplicationID = n.ApplicationID
		WHERE ( @NoteKey IS NULL OR (@NoteKey IS NOT NULL AND n.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ScopeID = @ScopeKey
			AND ( @ApplicationKey IS NULL OR (@ApplicationKey IS NOT NULL AND n.ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @BusinessKey IS NULL OR (@BusinessKey IS NOT NULL AND n.BusinessID = @BusinessID) )
			AND ( @BusinessEntityKey IS NULL OR (@BusinessEntityKey IS NOT NULL AND n.BusinessEntityID IN (SELECT Value FROM dbo.fnSplit(@BusinessEntityKey, ',') WHERE ISNUMERIC(Value) = 1))) 
			AND ( @NoteTypeKey IS NULL OR (@NoteTypeKey IS NOT NULL AND n.NoteTypeID IN (SELECT Value FROM dbo.fnSplit(@NoteTypeKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @NotebookKey IS NULL OR (@NotebookKey IS NOT NULL AND n.NotebookID IN (SELECT Value FROM dbo.fnSplit(@NotebookKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @PriorityKey IS NULL OR (@PriorityKey IS NOT NULL AND n.PriorityID IN (SELECT Value FROM dbo.fnSplit(@PriorityKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @OriginKey IS NULL OR (@OriginKey IS NOT NULL AND n.OriginID IN (SELECT Value FROM dbo.fnSplit(@OriginKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @TagKey IS NULL OR (@TagKey IS NOT NULL AND n.NoteID IN (
				SELECT DISTINCT 
					NoteID 
				FROM NoteTag 
				WHERE TagID IN (SELECT Value FROM dbo.fnSplit(@TagKey, ',') WHERE ISNUMERIC(Value) = 1))
			))
			AND ( @DataDateEnd IS NULL OR ( @DataDateEnd IS NOT NULL AND (
				n.DateCreated >= @DataDateStart
					AND n.DateCreated <= @DataDateEnd
			)))

	)
	
	-- Fetch results from common table expression
	SELECT
		NoteID,
		ApplicationID,
		ApplicationCode,
		ApplicationName,
		NoteTypeID,
		NoteTypeCode,
		NoteTypeName,
		NotebookID,
		NotebookCode,
		NotebookName,
		Title,
		Memo,
		--CONVERT(XML, Tags) AS Tags,
		(
			SELECT t.TagID AS Id, t.Name AS Name
			FROM dbo.NoteTag ntag (NOLOCK)
				JOIN dbo.Tag t (NOLOCK)
					ON ntag.TagID = t.TagID
			WHERE ntag.NoteID = res.NoteID
			FOR XML PATH('Tag'), ROOT('Tags')
		) AS Tags,
		PriorityID,
		PriorityCode,
		PriorityName,
		OriginID,
		OriginCode,
		OriginName,
		OriginDataKey,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteNotes) AS TotalRecordCount
	FROM cteNotes res
	WHERE RowNumber > @Skip AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
			
END
