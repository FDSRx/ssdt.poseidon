﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description: Creates a new patient Medication object.
-- SAMPLE CALL:
/*
DECLARE 
	@MedicationID BIGINT = NULL
	
EXEC api.spPhrm_Patient_Medication_Create
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@Name = 'Lyrica',
	@Strength = '150 mg',
	@Directions = '1 capsule morning and night.',
	@Indication = 'Nerve pain.',
	@Quantity = 30,
	@DateFilled = '8/7/2014',
	@MedicationID = @MedicationID OUTPUT,
	@CreatedBy = 'dhughes'

SELECT @MedicationID AS MedicationID
	
*/
-- SELECT * FROM phrm.Medication
-- SELECT * FROM phrm.MedicationType
-- SELECT * FROM phrm.Provider
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Medication_Create]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@MedicationTypeKey VARCHAR(25) = NULL,
	@Name VARCHAR(256),
	@NDC VARCHAR(25) = NULL,
	@Quantity DECIMAL(9,2) = NULL,
	@Strength VARCHAR(50) = NULL,
	@Directions VARCHAR(1000) = NULL,
	@Indication VARCHAR(1000) = NULL,
	@DateFilled DATETIME = NULL,
	@PrescriberTypeKey VARCHAR(25) = NULL,
	@PrescriberFirstName VARCHAR(75) = NULL,
	@PrescriberLastName VARCHAR(75) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@MedicationID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------
	SET @MedicationID = NULL;
	
	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@MedicationTypeID INT = ISNULL(phrm.fnGetMedicationTypeID(@MedicationTypeKey), 
			phrm.fnGetMedicationTypeID('OTC')), -- default to "Over the Counter (OTC)" meds if left blank
		@PrescriberTypeID INT = phrm.fnGetPrescriberTypeID(@PrescriberTypeKey),
		@ErrorMessage VARCHAR(4000)
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');

	---------------------------------------------------------------------------
	-- Argument resolution exceptions.
	-- <Summary>
	-- Validates resolved values.
	-- </Summary>
	---------------------------------------------------------------------------
	-- PersonID
	IF @PersonID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Medication object. Object reference (@PatientKey) was not able to be translated. ' +
			'A valid @PatientKey value is required.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	---------------------------------------------------------------------
	-- Create new patient Medication object.
	---------------------------------------------------------------------
	EXEC phrm.spMedication_Create
		@PersonID = @PersonID,
		@MedicationTypeID = @MedicationTypeID,
		@Name = @Name,
		@NDC = @NDC,
		@Quantity = @Quantity,
		@Strength = @Strength,
		@Directions = @Directions,
		@Indication = @Indication,
		@DateFilled = @DateFilled,
		@PrescriberTypeID = @PrescriberTypeID,
		@PrescriberFirstName = @PrescriberFirstName,
		@PrescriberLastName = @PrescriberLastName,
		@CreatedBy = @CreatedBy,
		@MedicationID = @MedicationID OUTPUT
	
		
	
END
