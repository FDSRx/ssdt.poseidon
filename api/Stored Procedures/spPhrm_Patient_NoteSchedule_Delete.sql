﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/8/2014
-- Description:	Deletes a patient NoteSchedule object.
-- SAMPLE CALL:
/*

EXEC api.spPhrm_Patient_NoteSchedule_Delete
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@NoteScheduleID = 570

*/
 
-- SELECT * FROM schedule.NoteSchedule
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_NoteSchedule_Delete]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteScheduleID BIGINT = NULL,
	@NoteID BIGINT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');	

	
	---------------------------------------------------------------------------
	-- Deletes a patient's NoteSchedule object.
	---------------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
		EXEC schedule.spNoteSchedule_Delete
			@NoteScheduleID = @NoteScheduleID,
			@NoteID = @NoteID
	END
			
END
