﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 5/14/2015
-- Description:	Returns a list of MessageType objects and their applicable parameters for the provided criteria.
-- SAMPLE CALL:
/*

-- Identifier.
EXEC api.spComm_MessageTypeParameter_Get_List
	@ApplicationKey = 'ENG',
	@MessageTypeParameterKey = 1
	
-- Business.
EXEC api.spComm_MessageTypeParameter_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '49',
	@BusinessKeyType = 'Id'
	
-- Application.
EXEC api.spComm_MessageTypeParameter_Get_List
	@ApplicationKey = 'ENG'
	
-- MessageTypes.
EXEC api.spComm_MessageTypeParameter_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@MessageTypeKey = 'QC1'

-- Strict.
EXEC api.spComm_MessageTypeParameter_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@MessageTypeKey = 'QC1',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 1
	
-- Strict.
EXEC api.spComm_MessageTypeParameter_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@MessageTypeKey = '2,3,4,5,6',
	@ParameterKey = 'MINFILL',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 1

*/

-- SELECT * FROM comm.MessageTypeParameter
-- SELECT * FROM comm.MessageType
-- =============================================
CREATE PROCEDURE [api].[spComm_MessageTypeParameter_Get_List]
	@MessageTypeParameterKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@EntityKey VARCHAR(50) = NULL,
	@EntityKeyType VARCHAR(50) = NULL,
	@MessageTypeKey VARCHAR(MAX) = NULL,
	@ParameterKey VARCHAR(MAX) = NULL,
	@EnforceApplicationSpecification BIT = NULl,
	@EnforceBusinessSpecification BIT = NULL,
	@EnforceEntitySpecification BIT = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Result schema binding
	-- <Summary>
	-- Returns a schema of the intended result set.  This is a workaround to assist certain
	-- frameworks in determining the correct result sets (i.e. SSIS, Entity Framework, etc.).
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF (1 = 2)
	BEGIN
		DECLARE
			@Property VARCHAR(MAX) = NULL;

		SELECT 
			CONVERT(BIGINT, @Property) AS MessageTypeParamterID,
			CONVERT(INT, @Property) AS ApplicationID,
			CONVERT(VARCHAR(50), @Property) AS ApplicationCode,
			CONVERT(VARCHAR(50), @Property) AS ApplicationName,
			CONVERT(BIGINT, @Property) AS BusinessID,
			CONVERT(BIGINT, @Property) AS EntityID,
			CONVERT(INT, @Property) AS MessageTypeID,
			CONVERT(VARCHAR(50), @Property) AS MessageTypeCode,
			CONVERT(VARCHAR(50), @Property) AS MessageTypeName,
			CONVERT(VARCHAR(1000), @Property) AS MessageTypeDescription,
			CONVERT(INT, @Property) AS ParameterID,
			CONVERT(VARCHAR(50), @Property) AS ParameterCode,
			CONVERT(VARCHAR(50), @Property) AS ParameterName,
			CONVERT(VARCHAR(1000), @Property) AS ParameterDescription,
			CONVERT(VARCHAR(MAX), @Property) AS ValueString,
			CONVERT(VARCHAR(50), @Property) AS TypeOf,
			CONVERT(UNIQUEIDENTIFIER, @Property) AS rowguid,
			CONVERT(DATETIME, @Property) AS DateCreated,
			CONVERT(DATETIME, @Property) AS DateModified,
			CONVERT(VARCHAR(256), @Property) AS CreatedBy,
			CONVERT(VARCHAR(256), @Property) AS ModifiedBy,
			CONVERT(BIGINT, @Property) AS TotalRecords
	END
	
	--------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@MessageTypeParameterKey=' + dbo.fnToStringOrEmpty(@MessageTypeParameterKey) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@EntityKey=' + dbo.fnToStringOrEmpty(@EntityKey) + ';' +
		'@EntityKeyType=' + dbo.fnToStringOrEmpty(@EntityKeyType) + ';' +
		'@MessageTypeKey=' + dbo.fnToStringOrEmpty(@MessageTypeKey) + ';' +
		'@ParameterKey=' + dbo.fnToStringOrEmpty(@ParameterKey) + ';' +
		'@EnforceApplicationSpecification=' + dbo.fnToStringOrEmpty(@EnforceApplicationSpecification) + ';' +
		'@EnforceBusinessSpecification=' + dbo.fnToStringOrEmpty(@EnforceBusinessSpecification) + ';' +
		'@EnforceEntitySpecification=' + dbo.fnToStringOrEmpty(@EnforceEntitySpecification) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @EnforceApplicationSpecification = ISNULL(@EnforceApplicationSpecification, 0);
	SET @EnforceBusinessSpecification = ISNULL(@EnforceBusinessSpecification, 0);
	SET @EnforceEntitySpecification = ISNULL(@EnforceEntitySpecification, 0);
	
		
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL,
		@PersonID BIGINT = NULL,
		@MessageTypeIdArray VARCHAR(MAX) = comm.fnMessageTypeKeyTranslator(@MessageTypeKey, DEFAULT),
		@ParameterIdArray VARCHAR(MAX) = data.fnParameterKeyTranslator(@ParameterKey, DEFAULT)
	
	-- Debug
	-- SELECT @MessageTypeIdArray AS MessageTypeIdArray, @ParameterIdArray AS ParameterIdArray

	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PersonKey = @EntityKey,
		@PersonTypeKey = @EntityKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1	

	-- Debug
	--SELECT @ApplicationID AS AplicationID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
	--	@BusinessID AS BusinessID, @EntityKey AS EntityKey, @EntityKeyType AS EntityKeyType, @PersonID AS PersonID,
	--	@MessageTypeIdArray AS MessageTypeIdArray, @ParameterIdArray AS ParameterIdArray
		
		
	--------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	--------------------------------------------------------------------------------------------
	-- Fetch results.
	-- <Summary>
	-- Uses "bubble up" functionality to determine results (i.e. looks at specific criteria,
	-- to least specific criteria).
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF @EnforceApplicationSpecification = 0 AND @EnforceBusinessSpecification = 0
		AND @EnforceEntitySpecification = 0
	BEGIN
		
		;WITH cteItems AS (
			SELECT
				CASE 
					WHEN @SortField IN ('MessageTypeParameterID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeParameterID ASC) 
					WHEN @SortField IN ('MessageTypeParameterID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeParameterID DESC)
					WHEN @SortField IN ('ParameterID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY ParameterID ASC) 
					WHEN @SortField IN ('ParameterID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY ParameterID DESC)        
					WHEN @SortField = 'ParameterCode' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY ParameterCode ASC) 
					WHEN @SortField = 'ParameterCode' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY ParameterCode DESC) 
					WHEN @SortField = 'ParameterName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY ParameterName ASC) 
					WHEN @SortField = 'ParameterName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY ParameterName DESC)
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeID ASC) 
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeID DESC) 				
					WHEN @SortField = 'MessageTypeCode' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeCode ASC) 
					WHEN @SortField = 'MessageTypeCode' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeCode DESC) 
					WHEN @SortField = 'MessageTypeName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeName ASC) 
					WHEN @SortField = 'MessageTypeName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeName DESC)
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated ASC) 
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated DESC)   
					ELSE ROW_NUMBER() OVER (ORDER BY MessageTypeName ASC)
				END AS RowNumber,
				MessageTypeParameterID,
				ApplicationID,
				ApplicationCode,
				ApplicationName,
				BusinessID,
				EntityID,
				MessageTypeID,
				MessageTypeCode,
				MessageTypeName,
				MessageTypeDescription,
				ParameterID,
				ParameterCode,
				ParameterName,
				ParameterDescription,
				ValueString,
				TypeOf,
				rowguid,
				DateCreated,
				DateModified,
				CreatedBy,
				ModifiedBy
			--SELECT *
			FROM comm.fnGetMessageTypeParameters(@ApplicationID, @BusinessKey, @BusinessKeyType, @EntityKey, @EntityKeyType, DEFAULT) trgt
			WHERE ( @MessageTypeParameterKey IS NULL OR (@MessageTypeParameterKey IS NOT NULL AND MessageTypeParameterID IN (
						SELECT Value FROM dbo.fnSplit(@MessageTypeParameterKey, ',') WHERE ISNUMERIC(Value) =1) 
				))
				AND (@MessageTypeKey IS NULL OR (@MessageTypeKey IS NOT NULL AND trgt.MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeIdArray, ',') WHERE ISNUMERIC(Value) = 1)))
				AND (@ParameterKey IS NULL OR (@ParameterKey IS NOT NULL AND trgt.ParameterID IN (SELECT Value FROM dbo.fnSplit(@ParameterIdArray, ',') WHERE ISNUMERIC(Value) = 1)))
		)

		SELECT
			MessageTypeParameterID,
			ApplicationID,
			ApplicationCode,
			ApplicationName,
			BusinessID,
			EntityID,
			MessageTypeID,
			MessageTypeCode,
			MessageTypeName,
			MessageTypeDescription,
			ParameterID,
			ParameterCode,
			ParameterName,
			ParameterDescription,
			ValueString,
			TypeOf,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy,
			(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
		FROM cteItems
		WHERE RowNumber > @Skip 
			AND RowNumber <= (@Skip + @Take)
		ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.

	END	
	
	--------------------------------------------------------------------------------------------
	-- Fetch results.
	-- <Summary>
	-- Uses a more strict criterion based on the user's input.
	-- </Summary>
	--------------------------------------------------------------------------------------------	
	IF @EnforceApplicationSpecification = 1 OR @EnforceBusinessSpecification = 1
		OR @EnforceEntitySpecification = 1
	BEGIN

		;WITH cteItems AS (
			SELECT
				CASE 
					WHEN @SortField IN ('MessageTypeParameterID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY trgt.MessageTypeParameterID ASC) 
					WHEN @SortField IN ('MessageTypeParameterID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY trgt.MessageTypeParameterID DESC)
					WHEN @SortField IN ('ParameterID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY trgt.ParameterID ASC) 
					WHEN @SortField IN ('ParameterID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY trgt.ParameterID DESC)        
					WHEN @SortField = 'ParameterCode' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY parm.Code ASC) 
					WHEN @SortField = 'ParameterCode' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY parm.Code DESC) 
					WHEN @SortField = 'ParameterName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY parm.Name ASC) 
					WHEN @SortField = 'ParameterName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY parm.Name DESC)
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY trgt.MessageTypeID ASC) 
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY trgt.MessageTypeID DESC) 				
					WHEN @SortField = 'MessageTypeCode' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Code ASC) 
					WHEN @SortField = 'MessageTypeCode' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Code DESC) 
					WHEN @SortField = 'MessageTypeName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Name ASC) 
					WHEN @SortField = 'MessageTypeName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Name DESC)
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY trgt.DateCreated ASC) 
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY trgt.DateCreated DESC)   
					ELSE ROW_NUMBER() OVER (ORDER BY typ.Name ASC)
				END AS RowNumber,
				trgt.MessageTypeParameterID,
				trgt.ApplicationID,
				app.Code AS ApplicationCode,
				app.Name AS ApplicationName,
				trgt.BusinessID,
				trgt.EntityID,
				trgt.MessageTypeID,
				typ.Code AS MessageTypeCode,
				typ.Name AS MessageTypeName,
				typ.Description AS MessageTypeDescription,
				trgt.ParameterID,
				parm.Code AS ParameterCode,
				parm.Name AS ParameterName,
				NULL AS ParameterDescription,
				trgt.ValueString,
				trgt.TypeOf,
				trgt.rowguid,
				trgt.DateCreated,
				trgt.DateModified,
				trgt.CreatedBy,
				trgt.ModifiedBy
			--SELECT *
			FROM comm.MessageTypeParameter trgt
				LEFT JOIN comm.MessageType typ
					ON trgt.MessageTypeID = typ.MessageTypeID
				JOIN data.Parameter parm
					ON trgt.ParameterID = parm.ParameterID
				LEFT JOIN dbo.Application app
					ON trgt.ApplicationID = app.ApplicationID
			WHERE ( @MessageTypeParameterKey IS NULL OR (@MessageTypeParameterKey IS NOT NULL AND trgt.MessageTypeParameterID IN (
						SELECT Value FROM dbo.fnSplit(@MessageTypeParameterKey, ',') WHERE ISNUMERIC(Value) =1) 
				))
				AND ( @EnforceApplicationSpecification = 0 OR trgt.ApplicationID = @ApplicationID )
				AND ( @EnforceBusinessSpecification = 0 OR trgt.BusinessID = @BusinessID )
				AND ( @EnforceEntitySpecification = 0 OR trgt.EntityID = @PersonID )
				AND ( @MessageTypeKey IS NULL OR (@MessageTypeKey IS NOT NULL AND trgt.MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeIdArray, ',') WHERE ISNUMERIC(Value) = 1)))
				AND ( @ParameterKey IS NULL OR (@ParameterKey IS NOT NULL AND trgt.ParameterID IN (SELECT Value FROM dbo.fnSplit(@ParameterIdArray, ',') WHERE ISNUMERIC(Value) = 1)))
		)

		SELECT
			MessageTypeParameterID,
			ApplicationID,
			ApplicationCode,
			ApplicationName,
			BusinessID,
			EntityID,
			MessageTypeID,
			MessageTypeCode,
			MessageTypeName,
			MessageTypeDescription,
			ParameterID,
			ParameterCode,
			ParameterName,
			ParameterDescription,
			ValueString,
			TypeOf,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy,
			(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
		FROM cteItems
		WHERE RowNumber > @Skip 
			AND RowNumber <= (@Skip + @Take)
		ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.	
	
	
	END
	
	
END
