﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/14/2015
-- Description:	Updates a Task object for a patient.
-- Change Log:
-- 1/14/2016 - dhughes - Modified the procedure to accept more input parameters.
-- SAMPLE CALL:
/*

DECLARE 
	@NoteID BIGINT = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@NotebookKey VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX),
	@PriorityKey VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@OriginKey VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@DateDue DATETIME = NULL,
	@AssignedByID BIGINT = NULL,
	@AssignedByString VARCHAR(256) = NULL,
	@AssignedToID BIGINT = NULL,
	@AssignedToString VARCHAR(256) = NULL,
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusKey VARCHAR(50) = NULL,
	@TaskStatusID INT = NULL,
	@ParentTaskID BIGINT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@IsDateDueNullificationAllowed BIT = NULL,
	@IsAssignedByIDNullificationAllowed BIT = NULL,
	@IsAssignedByStringNullificationAllowed BIT = NULL,
	@IsAssignedToIDNullificationAllowed BIT = NULL,
	@IsAssignedToStringNullificationAllowed BIT = NULL,
	@IsCompletedByIDNullificationAllowed BIT = NULL,
	@IsCompletedByStringNullificationAllowed BIT = NULL,
	@IsDateCompletedNullificationAllowed BIT = NULL,
	@IsParentTaskIDNullificationAllowed BIT = NULL,
	@TaskID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
	
EXEC api.spPhrm_Patient_Task_Update
	@NoteID = @NoteID,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@NotebookKey = @NotebookKey,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityKey = @PriorityKey,
	@Tags = @Tags,
	@OriginKey = @OriginKey,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@DateDue = @DateDue,
	@AssignedByID = @AssignedByID,
	@AssignedByString = @AssignedByString,
	@AssignedToID = @AssignedToID,
	@AssignedToString = @AssignedToString,
	@CompletedByID = @CompletedByID,
	@CompletedByString = @CompletedByString,
	@DateCompleted = @DateCompleted,
	@TaskStatusKey = @TaskStatusKey,
	@TaskStatusID = @TaskStatusID,
	@ParentTaskID = @ParentTaskID,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@IsDateDueNullificationAllowed = @IsDateDueNullificationAllowed,
	@IsAssignedByIDNullificationAllowed = @IsAssignedByIDNullificationAllowed,
	@IsAssignedByStringNullificationAllowed = @IsAssignedByStringNullificationAllowed,
	@IsAssignedToIDNullificationAllowed = @IsAssignedToIDNullificationAllowed,
	@IsAssignedToStringNullificationAllowed = @IsAssignedToStringNullificationAllowed,
	@IsCompletedByIDNullificationAllowed = @IsCompletedByIDNullificationAllowed,
	@IsCompletedByStringNullificationAllowed = @IsCompletedByStringNullificationAllowed,
	@IsDateCompletedNullificationAllowed = @IsDateCompletedNullificationAllowed,
	@IsParentTaskIDNullificationAllowed = @IsParentTaskIDNullificationAllowed,
	@TaskID = @TaskID OUTPUT,
	@Debug = @Debug


SELECT @TaskID AS TaskID

 */
 
-- SELECT * FROM dbo.Note ORDER BY NoteID DESC
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook
-- SELECT * FROM dbo.vwTask

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Task_Update]
	@NoteID BIGINT = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@NotebookKey VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX),
	@PriorityKey VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@OriginKey VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData VARCHAR(MAX) = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@DateDue DATETIME = NULL,
	@AssignedByID BIGINT = NULL,
	@AssignedByString VARCHAR(256) = NULL,
	@AssignedToID BIGINT = NULL,
	@AssignedToString VARCHAR(256) = NULL,
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusKey VARCHAR(50) = NULL,
	@TaskStatusID INT = NULL,
	@ParentTaskID BIGINT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@IsDateDueNullificationAllowed BIT = NULL,
	@IsAssignedByIDNullificationAllowed BIT = NULL,
	@IsAssignedByStringNullificationAllowed BIT = NULL,
	@IsAssignedToIDNullificationAllowed BIT = NULL,
	@IsAssignedToStringNullificationAllowed BIT = NULL,
	@IsCompletedByIDNullificationAllowed BIT = NULL,
	@IsCompletedByStringNullificationAllowed BIT = NULL,
	@IsDateCompletedNullificationAllowed BIT = NULL,
	@IsParentTaskIDNullificationAllowed BIT = NULL,
	@TaskID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE(),
		@ErrorMessage VARCHAR(4000) = NULL

	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(@AdditionalData) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@DateDue=' + dbo.fnToStringOrEmpty(@DateDue) + ';' +
		'@AssignedByID=' + dbo.fnToStringOrEmpty(@AssignedByID) + ';' +
		'@AssignedByString=' + dbo.fnToStringOrEmpty(@AssignedByString) + ';' +
		'@AssignedToID=' + dbo.fnToStringOrEmpty(@AssignedToID) + ';' +
		'@AssignedToString=' + dbo.fnToStringOrEmpty(@AssignedToString) + ';' +
		'@CompletedByID=' + dbo.fnToStringOrEmpty(@CompletedByID) + ';' +
		'@CompletedByString=' + dbo.fnToStringOrEmpty(@CompletedByString) + ';' +
		'@DateCompleted=' + dbo.fnToStringOrEmpty(@DateCompleted) + ';' +
		'@TaskStatusKey=' + dbo.fnToStringOrEmpty(@TaskStatusKey) + ';' +
		'@TaskStatusID=' + dbo.fnToStringOrEmpty(@TaskStatusID) + ';' +
		'@ParentTaskID=' + dbo.fnToStringOrEmpty(@ParentTaskID) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@IsDateDueNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsDateDueNullificationAllowed) + ';' +
		'@IsAssignedByIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedByIDNullificationAllowed) + ';' +
		'@IsAssignedByStringNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedByStringNullificationAllowed) + ';' +
		'@IsAssignedToIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedToIDNullificationAllowed) + ';' +
		'@IsAssignedToStringNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedToStringNullificationAllowed) + ';' +
		'@IsCompletedByIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsCompletedByIDNullificationAllowed) + ';' +
		'@IsCompletedByStringNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsCompletedByStringNullificationAllowed) + ';' +
		'@IsDateCompletedNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsDateCompletedNullificationAllowed) + ';' +
		'@IsParentTaskIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsParentTaskIDNullificationAllowed) + ';' +
		'@TaskID=' + dbo.fnToStringOrEmpty(@TaskID) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;


	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	
	*/

	-----------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------------------------------------------------------
	SET @TaskID = NULL;
	SET @Debug = ISNULL(@Debug, 0);
	SET @BusinesskeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @PatientKeyType = ISNULL(@PatientKeyType, 'SourcePatientKey');
			
	-----------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE
		@BusinessID BIGINT = NULL,
		@PatientID BIGINT,
		@PersonID BIGINT,
		@ScopeID INT = dbo.fnGetScopeID('INTRN'), -- only create internal notes
		@NoteTypeID INT = dbo.fnGetNoteTypeID('TSK'),
		@NotebookID INT = dbo.fnGetNotebookID(@NotebookKey),
		@PriorityID INT = dbo.fnGetPriorityID(@PriorityKey),
		@OriginID INT = dbo.fnGetOriginID(@OriginKey),
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey)

	
	-----------------------------------------------------------------------------------------------------------------------
	-- Set local variables
	-----------------------------------------------------------------------------------------------------------------------
	SET @BusinessID = CASE WHEN @BusinessKeyType IS NOT NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType) ELSE NULL END;

	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, @BusinessKeyType, @PatientKey, @PatientKeyType);
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');

	SET @TaskStatusID = ISNULL(dbo.fnGetTaskStatusID(@TaskStatusID), dbo.fnGetTaskStatusID(@TaskStatusKey))

	
	-----------------------------------------------------------------------------------------------------------------------
	-- Update the patient Task object.
	-- <Summary>
	-- Updates the provided Task object.
	-- <Summary>
	-----------------------------------------------------------------------------------------------------------------------
	-- If I was able to discover a Person (Patient) object,then let's update their Note/Task object.
	IF @PersonID IS NOT NULL
	BEGIN
		EXEC dbo.spTask_Update
			@NoteID = @NoteID,
			@ApplicationID = @ApplicationID,
			@BusinessID = @BusinessID,
			@ScopeID = @ScopeID,
			@BusinessEntityID = @PersonID,
			@NotebookID = @NotebookID,
			@PriorityID = @PriorityID,
			@Title = @Title,
			@Memo = @Memo,
			@Tags = @Tags,
			@OriginID = @OriginID,
			@OriginDataKey = @OriginDataKey,
			@AdditionalData = @AdditionalData,
			@CorrelationKey = @CorrelationKey,
			@DateDue = @DateDue,
			@AssignedByID = @AssignedByID,
			@AssignedByString = @AssignedByString,
			@AssignedToID = @AssignedToID,
			@AssignedToString = @AssignedToString,
			@CompletedByID = @CompletedByID,
			@CompletedByString = @CompletedByString,
			@DateCompleted = @DateCompleted,
			@TaskStatusID = @TaskStatusID,
			@ParentTaskID = @ParentTaskID,
			@ModifiedBy = @ModifiedBy,
			@IsDateDueNullificationAllowed = @IsDateDueNullificationAllowed,
			@IsAssignedByIDNullificationAllowed = @IsAssignedByIDNullificationAllowed,
			@IsAssignedByStringNullificationAllowed = @IsAssignedByStringNullificationAllowed,
			@IsAssignedToIDNullificationAllowed = @IsAssignedToIDNullificationAllowed,
			@IsAssignedToStringNullificationAllowed = @IsAssignedToStringNullificationAllowed,
			@IsCompletedByIDNullificationAllowed = @IsCompletedByIDNullificationAllowed,
			@IsCompletedByStringNullificationAllowed = @IsCompletedByStringNullificationAllowed,
			@IsDateCompletedNullificationAllowed = @IsDateCompletedNullificationAllowed,
			@IsParentTaskIDNullificationAllowed = @IsParentTaskIDNullificationAllowed
	END
			
END

