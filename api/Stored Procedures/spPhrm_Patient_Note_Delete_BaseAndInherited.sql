﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/18/2016
-- Description:	Deletes a single or comma delimited list of notes and all of its inherited objects that are applicable to a patient.
-- SAMPLE CALL:
/*

DECLARE
	@ApplicationKey VARCHAR(50) = 'ENG',
	@BusinessKey VARCHAR(50) = '2501624',
	@BusinessKeyType VARCHAr(50) = 'NABP',
	@PatientKey VARCHAR(50) = '14440',
	@PatientKeyType VARCHAR(50) = 'SRC',
	@NoteID VARCHAR(MAX) = '71251',
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@Debug BIT = 1

EXEC api.spPhrm_Patient_Note_Delete
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@NoteID = @NoteID,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/


-- SELECT TOP 50 * FROM dbo.vwNote ORDER BY 1 DESC

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Note_Delete_BaseAndInherited]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAr(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteID VARCHAR(MAX),
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	SET @NoteID = NULLIF(@NoteID, '');
	SET @BusinessKeyType = ISNULL(NULLIF(@BusinessKeyType, ''), 'NABP');
	SET @PatientKeyType = ISNULL(NULLIF(@PatientKeyType, ''), 'SRC');

	------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL,
		@PersonID BIGINT = NULL,
		@NoteID_Verified VARCHAR(MAX) = NULL
	
	------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PatientKey = @PatientKey,
		@PatientKeyType = @PatientKeyType,
		@PersonID = @PersonID OUTPUT,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1

	-- Get note list that pertain's to patient only.
	SET @NoteID_Verified = (
		SELECT
			ISNULL(CONVERT(VARCHAR, NoteID), '') + ','
		FROM dbo.Note trgt
		WHERE trgt.BusinessID = @BusinessID
			AND trgt.BusinessEntityID = @PersonID
			AND trgt.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteID, ',') WHERE ISNUMERIC(Value) = 1)
	);
			
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Deubber On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationKey AS ApplicationKey, @ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey, 
			@BusinessKeyType AS BusinessKeType,	@BusinessID AS BusinessID, @PatientKey AS PatientKey,
			@PatientKeyType AS PatientKeyType, @NoteID AS NoteID, @PersonID AS PersonID, 
			@NoteID_Verified AS NoteID_Verified
	END


	------------------------------------------------------------------------------------------------------------
	-- Delete object.
	------------------------------------------------------------------------------------------------------------
	IF @PersonID IS NOT NULL 
		AND @NoteID IS NOT NULL 
		AND @NoteID_Verified IS NOT NULL
	BEGIN
		EXEC dbo.spNote_Delete_BaseAndInherited
			@NoteID = @NoteID_Verified,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug
	END

END
