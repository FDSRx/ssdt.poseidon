﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/18/2017
-- Description:	Returns a list of configuration settings.
-- Change log:
-- 5/13/2016 - dhughes - Modified the procedure to union global keys with with specific criteria so that all keys are present
--					when returning data. Modified the row number logic to be more efficient.

-- SAMPLE CALL:
/*

DECLARE
	@ConfigurationKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = '1',
	@BusinessKey VARCHAR(50) = '7871787',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@KeyName VARCHAR(256) = NULL,
	@ConfigurationLevel VARCHAR(MAX) = NULL,
	@ConfigurationLevel_IsNotAnyOf VARCHAR(MAX) = '',
	@IsCustomValue BIT = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = 'KeyName|ASC',
	@OutputData XML = NULL,
	@IsOutputOnly BIT = NULL,
	@Debug BIT = NULL

EXEC api.spDbo_Configuration_Get_List
	@ConfigurationKey = @ConfigurationKey,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@KeyName = @KeyName,
	@ConfigurationLevel = @ConfigurationLevel,
	@ConfigurationLevel_IsNotAnyOf = @ConfigurationLevel_IsNotAnyOf,
	@IsCustomValue = @IsCustomValue,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@OutputData = @OutputData,
	@IsOutputOnly = @IsOutputOnly,
	@Debug = @Debug

*/

-- SELECT * FROM dbo.Configuration
-- =============================================
CREATE PROCEDURE [api].[spDbo_Configuration_Get_List]
	@ConfigurationKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@KeyName VARCHAR(256) = NULL,
	@ConfigurationLevel VARCHAR(MAX) = NULL,
	@ConfigurationLevel_IsNotAnyOf VARCHAR(MAX) = NULL,
	@IsCustomValue BIT = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@OutputData XML = NULL OUTPUT,
	@IsOutputOnly BIT = NULL,
	@Debug BIT = NULL
AS
BEGIN


	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ConfigurationKey=' + dbo.fnToStringOrEmpty(@ConfigurationKey) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@KeyName=' + dbo.fnToStringOrEmpty(@KeyName) + ';' +
		'@ConfigurationLevel=' + dbo.fnToStringOrEmpty(@ConfigurationLevel) + ';' +
		'@ConfigurationLevel_IsNotAnyOf=' + dbo.fnToStringOrEmpty(@ConfigurationLevel_IsNotAnyOf) + ';' +
		'@IsCustomValue=' + dbo.fnToStringOrEmpty(@IsCustomValue) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';' ;

	------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	SET @OutputData = NULL;

	------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT,
		@BusinessEntityID BIGINT,
		@NullValue UNIQUEIDENTIFIER = NEWID()

	------------------------------------------------------------------------------------------------
	-- Set variables / translate keys.
	------------------------------------------------------------------------------------------------
	-- Translate basic system keys.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessEntityID OUTPUT,
		@IsOutputOnly = 1

	------------------------------------------------------------------------------------------------
	-- Paging
	------------------------------------------------------------------------------------------------			
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);


	------------------------------------------------------------------------------------------------
	-- Temporary resources.
	------------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpConfigurationManager') IS NOT NULL
	BEGIN
		DROP TABLE #tmpConfigurationManager;
	END

	CREATE TABLE #tmpConfigurationManager (
		Idx BIGINT IDENTITY(1,1),
		ConfigurationID BIGINT,
		KeyName VARCHAR(256),
		ConfigurationLevel VARCHAR(10)
	);


	------------------------------------------------------------------------------------------------
	-- Compile/Query data.
	------------------------------------------------------------------------------------------------	
	-- Get specific records
	INSERT INTO #tmpConfigurationManager (
		ConfigurationID,
		KeyName,
		ConfigurationLevel
	)
	SELECT
		trgt.ConfigurationID,
		trgt.KeyName,
		'APPBIZ' AS ConfigurationLevel
	FROM dbo.Configuration trgt
	WHERE trgt.ApplicationID = @ApplicationID
		AND trgt.BusinessEntityID = @BusinessEntityID

	-- Debug
	--SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Review app/biz configs.' AS ActionMethod, * FROM #tmpConfigurationManager

	-- Get business records only
	INSERT INTO #tmpConfigurationManager (
		ConfigurationID,
		KeyName,
		ConfigurationLevel
	)
	SELECT
		trgt.ConfigurationID,
		trgt.KeyName,
		'BIZ' AS ConfigurationLevel
	FROM dbo.Configuration trgt
	WHERE trgt.ApplicationID IS NULL
		AND trgt.BusinessEntityID = @BusinessEntityID
		AND NOT EXISTS (
			SELECT TOP 1 *
			FROM #tmpConfigurationManager src
			WHERE src.KeyName = trgt.KeyName
		)

	-- Debug
	--SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Review business configs.' AS ActionMethod, * FROM #tmpConfigurationManager


	-- Get application records only
	INSERT INTO #tmpConfigurationManager (
		ConfigurationID,
		KeyName,
		ConfigurationLevel
	)
	SELECT
		trgt.ConfigurationID,
		trgt.KeyName,
		'APP' AS ConfigurationLevel
	FROM dbo.Configuration trgt
	WHERE trgt.ApplicationID = @ApplicationID
		AND trgt.BusinessEntityID IS NULL
		AND NOT EXISTS (
			SELECT TOP 1 *
			FROM #tmpConfigurationManager src
			WHERE src.KeyName = trgt.KeyName
		)

	-- Debug
	--SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Review app configs.' AS ActionMethod, * FROM #tmpConfigurationManager


	-- Get global records (across all applications and businesses).
	INSERT INTO #tmpConfigurationManager (
		ConfigurationID,
		KeyName,
		ConfigurationLevel
	)
	SELECT
		trgt.ConfigurationID,
		trgt.KeyName,
		'GLBL' AS ConfigurationLevel
	FROM dbo.Configuration trgt
	WHERE trgt.ApplicationID IS NULL
		AND trgt.BusinessEntityID IS NULL
		AND NOT EXISTS (
			SELECT TOP 1 *
			FROM #tmpConfigurationManager src
			WHERE src.KeyName = trgt.KeyName
		)
	
	-- Debug
	--SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Review global configs.' AS ActionMethod, * FROM #tmpConfigurationManager


	-- Run query.
	INSERT INTO #tmpConfigurationManager (
		ConfigurationID,
		KeyName,
		ConfigurationLevel
	)
	SELECT
		trgt.ConfigurationID,
		trgt.KeyName,
		'QRY' AS ConfigurationLevel
	FROM dbo.Configuration trgt
	WHERE ( @ConfigurationKey IS NULL OR trgt.ConfigurationID IN (SELECT Value FROM dbo.fnSplit(@ConfigurationKey, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @ApplicationKey IS NULL OR trgt.ApplicationID = @ApplicationID )
		AND ( @BusinessKey IS NULL OR trgt.BusinessEntityID = @BusinessEntityID )
		AND ( @KeyName IS NULL OR trgt.KeyName = @KeyName )
		AND NOT EXISTS (
			SELECT TOP 1 *
			FROM #tmpConfigurationManager src
			WHERE src.KeyName = trgt.KeyName
		)
	
	-- Debug
	--SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Query configs.' AS ActionMethod, * FROM #tmpConfigurationManager


	-- Unconfigured keys.
	INSERT INTO #tmpConfigurationManager (
		ConfigurationID,
		KeyName,
		ConfigurationLevel
	)
	SELECT
		NULL AS ConfigurationID,
		trgt.Name AS KeyName,
		'NOCFG' AS ConfigurationLevel
	--SELECT *
	FROM config.ConfigurationKey trgt
	WHERE NOT EXISTS (
			SELECT TOP 1 *
			FROM #tmpConfigurationManager src
			WHERE src.KeyName = trgt.Name
		)
	
	-- Debug
	-- SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Query configs.' AS ActionMethod, * FROM #tmpConfigurationManager


	------------------------------------------------------------------------------------------------
	-- Fetch data.
	------------------------------------------------------------------------------------------------
	;WITH cteItems AS (
		SELECT
			ROW_NUMBER() OVER(ORDER BY
				CASE WHEN @SortField IN ('Name', 'KeyName') AND @SortDirection = 'asc'  THEN tmp.KeyName END ASC,
				CASE WHEN @SortField IN ('Name', 'KeyName')  AND @SortDirection = 'desc'  THEN tmp.KeyName END DESC, 
				CASE WHEN @SortField IN ('DateCreated') AND @SortDirection = 'asc'  THEN tmp.DateCreated END ASC, 
				CASE WHEN @SortField IN ('DateCreated') AND @SortDirection = 'desc'  THEN tmp.DateCreated END DESC,   
				CASE WHEN @SortField IN ('BusinessID', 'BusinessEntityID', 'Business') AND @SortDirection = 'asc'  THEN tmp.BusinessEntityID END ASC,
				CASE WHEN @SortField IN ('BusinessID', 'BusinessEntityID', 'Business')  AND @SortDirection = 'desc'  THEN tmp.BusinessEntityID END DESC,  
				tmp.KeyName ASC
			) AS RowNumber,
			--CASE				
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.DateCreated ASC) 
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.DateCreated DESC)   
			--	ELSE ROW_NUMBER() OVER (ORDER BY cfg.ConfigurationID DESC)
			--END AS RowNumber,
			tmp.ConfigurationID,
			tmp.ApplicationID,
			tmp.ApplicationCode,
			tmp.ApplicationName,
			tmp.BusinessEntityID,
			tmp.KeyName,
			tmp.KeyValue,
			COALESCE(appdflt.KeyValue, glbldflt.KeyValue, tmp.KeyValue) AS DefaultKeyValue,
			tmp.DataTypeID,
			tmp.DataTypeName,
			tmp.DataSize,
			tmp.Description,
			tmp.rowguid,
			tmp.DateCreated,
			tmp.DateModified,
			tmp.CreatedBy,
			tmp.ModifiedBy,
			tmp.ConfigurationLevel
		--SELECT * 
		FROM (
			SELECT 
				cfg.ConfigurationID,
				cfg.ApplicationID,
				app.Code AS ApplicationCode,
				app.Name AS ApplicationName,
				cfg.BusinessEntityID,
				cfg.KeyName,
				cfg.KeyValue,
				cfg.DataTypeID,
				typ.Name AS DataTypeName,
				cfg.DataSize,
				cfgkey.Description,
				cfg.rowguid,
				cfg.DateCreated,
				cfg.DateModified,
				cfg.CreatedBy,
				cfg.ModifiedBy,
				tmp.ConfigurationLevel
			FROM dbo.Configuration cfg
				LEFT JOIN dbo.Application app
					ON cfg.ApplicationID = app.ApplicationID
				LEFT JOIN dbo.DataType typ
					ON cfg.DataTypeID = typ.DataTypeID
				JOIN config.ConfigurationKey cfgkey
					ON cfg.KeyName = cfgkey.Name
				JOIN #tmpConfigurationManager tmp
					ON tmp.ConfigurationID = cfg.ConfigurationID
						AND tmp.ConfigurationLevel <> 'NOCFG'

			UNION

			SELECT
				NULL AS ConfigurationID,
				NULL AS ApplicationID,
				NULL AS ApplicationCode,
				NULL AS ApplicationName,
				NULL AS BusinessEntityID,
				trgt.Name AS KeyName,
				NULL AS KeyValue,
				NULL AS DataTypeID,
				NULL AS DataTypeName,
				NULL AS DataSize,
				trgt.Description AS Description,
				NULL AS rowguid,
				NULL AS DateCreated,
				NULL AS DateModified,
				NULL AS CreatedBy,
				NULL AS ModifiedBy,
				src.ConfigurationLevel
			--SELECT *
			FROM config.ConfigurationKey trgt
				JOIN #tmpConfigurationManager src
					ON src.KeyName = trgt.Name
						AND src.ConfigurationLevel = 'NOCFG'
		) tmp
			-- Application level default values.
			LEFT JOIN dbo.Configuration appdflt
				ON tmp.KeyName = appdflt.KeyName
					AND tmp.ApplicationID = appdflt.ApplicationID
					AND appdflt.BusinessEntityID IS NULL
			-- Global level default values.
			LEFT JOIN dbo.Configuration glbldflt
				ON tmp.KeyName = glbldflt.KeyName
					AND glbldflt.ApplicationID IS NULL
					AND glbldflt.BusinessEntityID IS NULL
		WHERE ( @ConfigurationLevel_IsNotAnyOf IS NULL OR @ConfigurationLevel_IsNotAnyOf IS NOT NULL 
			AND tmp.ConfigurationLevel NOT IN (SELECT Value FROM dbo.fnSplit(@ConfigurationLevel_IsNotAnyOf, ',')) )
			AND ( @IsCustomValue IS NULL
				OR (@IsCustomValue IS NOT NULL AND @IsCustomValue = 0
					AND ISNULL(tmp.KeyValue, @NullValue) = COALESCE(
						CASE WHEN appdflt.ConfigurationID IS NOT NULL THEN ISNULL(appdflt.KeyValue, @NullValue) ELSE appdflt.KeyValue END, 
						CASE WHEN glbldflt.ConfigurationID IS NOT NULL THEN ISNULL(glbldflt.KeyValue, @NullValue) ELSE glbldflt.KeyValue END,
						ISNULL(tmp.KeyValue, @NullValue) 
					) 
				)  
				OR (@IsCustomValue IS NOT NULL AND @IsCustomValue = 1
					AND ISNULL(tmp.KeyValue, @NullValue) <> COALESCE(
						CASE WHEN appdflt.ConfigurationID IS NOT NULL THEN ISNULL(appdflt.KeyValue, @NullValue) ELSE appdflt.KeyValue END, 
						CASE WHEN glbldflt.ConfigurationID IS NOT NULL THEN ISNULL(glbldflt.KeyValue, @NullValue) ELSE glbldflt.KeyValue END,
						ISNULL(tmp.KeyValue, @NullValue) 
					) 
				)  
			)
			
		
			
	)


	SELECT
		res.ConfigurationID,
		res.ApplicationID,
		res.ApplicationCode,
		res.ApplicationName,
		res.BusinessEntityID,
		res.KeyName,
		res.KeyValue,
		res.DefaultKeyValue,
		res.DataTypeID,
		res.DataTypeName,
		res.DataSize,
		res.Description,
		res.rowguid,
		res.DateCreated,
		res.DateModified,
		res.CreatedBy,
		res.ModifiedBy,
		res.ConfigurationLevel,
		(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
	FROM cteItems res
	WHERE res.RowNumber > @Skip 
		AND res.RowNumber <= (@Skip + @Take)
	ORDER BY res.RowNumber -- Performs sort.  Sort is handled in the CTE above.	









	



END
