﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/06/2014
-- Description:	Creates a new CalendarItem object for a patient.
-- SAMPLE CALL;
/*

DECLARE 
	@NoteID BIGINT = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@NotebookKey VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX),
	@PriorityKey VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@OriginKey VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData VARCHAR(MAX) = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@Location VARCHAR(500) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CalendarItemID BIGINT = NULL OUTPUT,
	@Debug BIT = 1
	
EXEC api.spPhrm_Patient_CalendarItem_Create
	@NoteID = @NoteID,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@NotebookKey = @NotebookKey,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityKey = @PriorityKey,
	@Tags = @Tags,
	@OriginKey = @OriginKey,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@Location = @Location,
	@CreatedBy = @CreatedBy,
	@CalendarItemID = @CalendarItemID OUTPUT,
	@Debug = @Debug


SELECT @CalendarItemID AS CalendarItemID

 */
 
-- SELECT * FROM dbo.Note ORDER BY NoteID DESC
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook
-- SELECT * FROM dbo.CalendarItem
-- SELECT * FROM schedule.NoteSchedule

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_CalendarItem_Create]
	@NoteID BIGINT = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@NotebookKey VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX),
	@PriorityKey VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@OriginKey VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData VARCHAR(MAX) = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@Location VARCHAR(500) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CalendarItemID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID)  + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey)  + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType)  + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey)  + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title)  + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo)  + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey)  + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags)  + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey)  + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey)  + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(@AdditionalData)  + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey)  + ';' +
		'@Location=' + dbo.fnToStringOrEmpty(@Location)  + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy)  + ';' +
		'@CalendarItemID=' + dbo.fnToStringOrEmpty(@CalendarItemID)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------------------------------------------------------
	SET @CalendarItemID = NULL;
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @PatientKeyType = ISNULL(@PatientKeyType, 'SRC');
	
	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT,
		@PatientID BIGINT,
		@PersonID BIGINT,
		@ScopeID INT = dbo.fnGetScopeID('INTRN'), -- only create internal notes
		@NoteTypeID INT = dbo.fnGetNoteTypeID('TSK'),
		@NotebookID INT = dbo.fnGetNotebookID(@NotebookKey),
		@PriorityID INT = dbo.fnGetPriorityID(@PriorityKey),
		@OriginID INT = dbo.fnGetOriginID(@OriginKey)
	
	------------------------------------------------------------------------------------------------------------
	-- Set local variables
	------------------------------------------------------------------------------------------------------------
	SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);

	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, @BusinessKeyType, @PatientKey, @PatientKeyType);
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');	

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationKey AS ApplicationKey, @ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey,
			@BusinessKeyType AS BusinessKeyType, @BusinessID AS BusinessID, @PatientID AS PatientID, @PersonID AS PersonID,
			@Args AS Arguments
	END
	
	------------------------------------------------------------------------------------------------------------
	-- Create and return a patient CalendarItem object.
	-- <Summary>
	-- Creates a calendar item and returns the item's identity.
	-- <Summary>
	------------------------------------------------------------------------------------------------------------
	EXEC dbo.spCalendarItem_Create
		@NoteID = @NoteID,
		@ApplicationID = @ApplicationID,
		@BusinessID = @BusinessID,
		@ScopeID = @ScopeID,
		@BusinessEntityID = @PersonID,
		@NotebookID = @NotebookID,
		@PriorityID = @PriorityID,
		@Title = @Title,
		@Memo = @Memo,
		@Tags = @Tags,
		@OriginID = @OriginID,
		@OriginDataKey = @OriginDataKey,
		@Location = @Location,
		@CreatedBy = @CreatedBy,
		@CalendarItemID = @CalendarItemID OUTPUT
			
END
