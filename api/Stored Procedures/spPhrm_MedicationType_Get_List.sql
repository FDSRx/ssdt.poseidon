﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/6/2014
-- Description:	Returns a list of MedicationType items.
-- SAMPLE CALL: api.spPhrm_MedicationType_Get_List
-- =============================================
CREATE PROCEDURE [api].[spPhrm_MedicationType_Get_List]
	@MedicationTypeID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		mc.MedicationClassificationID,
		mc.Code AS MedicationClassificationCode,
		mc.Name AS MedicationClassificationName,
		mt.MedicationTypeID,
		mt.Code,
		mt.Name,
		mt.Description,
		mt.DateCreated
	FROM phrm.MedicationType mt
		LEFT JOIN phrm.MedicationClassification mc
			ON mt.MedicationClassificationID = mc.MedicationClassificationID
	WHERE ( @MedicationTypeID IS NULL OR mt.MedicationTypeID IN (SELECT Value FROM dbo.fnSplit(@MedicationTypeID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR mt.Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	
END
