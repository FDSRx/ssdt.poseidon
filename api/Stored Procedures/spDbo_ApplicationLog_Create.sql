﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/14/2014
-- Description:	Creates a new ApplicationLog object entry.
-- SAMPLE CALL:
/*

DECLARE
	@ApplicationLogID BIGINT = NULL
	
EXEC api.spDbo_ApplicationLog_Create
	@ApplicationKey = 'ESMT',
	@LogEntryTypeKey = 'INFO',
	@SourceName = 'api.spDbo_ApplicationLog_Create',
	@UserName = 'dhughes',
	@Message = 'Sample informational log entry from the API POV.',
	@ApplicationLogID = @ApplicationLogID OUTPUT

SELECT @ApplicationLogID AS ApplicationLogID

*/

-- SELECT * FROM dbo.ApplicationLog
-- SELECT * FROM dbo.Application
-- =============================================
CREATE PROCEDURE [api].[spDbo_ApplicationLog_Create]
	@ApplicationKey VARCHAR(50) = NULL,
	@LogEntryTypeKey VARCHAR(50) = NULL,
	@MachineName VARCHAR(256) = NULL,
	@ServiceName VARCHAR(256) = NULL,
	@SourceName VARCHAR(256) = NULL,
	@EventName VARCHAR(128) = NULL,
	@UserName VARCHAR(256) = NULL,
	@StartTime DATETIME = NULL,
	@EndTime DATETIME = NULL,
	@Message VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@StackTrace VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ApplicationLogID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------
	SET @StartTime = ISNULL(@StartTime, GETDATE());
	SET @EndTime = ISNULL(@EndTime, GETDATE());
	SET @UserName = ISNULL(@UserName, SUSER_SNAME());
	SET @MachineName = ISNULL(@MachineName, HOST_NAME());

	---------------------------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------------------------	
	DECLARE
		@ApplicationID INT = NULL,
		@LogEntryTypeID INT = NULL
	
	---------------------------------------------------------------------------------------
	-- Set variables.
	---------------------------------------------------------------------------------------	
	SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationKey);
	SET @LogEntryTypeID = dbo.fnGetLogEntryTypeID(@LogEntryTypeKey);
	
	-- Debug
	--SELECT @ApplicationID AS ApplicationID, @LogEntryTypeID AS LogEntryTypeID
	
	---------------------------------------------------------------------------------------
	-- Create new ApplicationLog object.
	---------------------------------------------------------------------------------------
	EXEC dbo.spApplicationLog_Create
		@ApplicationID = @ApplicationID,
		@LogEntryTypeID = @LogEntryTypeID,
		@MachineName = @MachineName,
		@ServiceName = @ServiceName,
		@SourceName = @SourceName,
		@EventName = @EventName,
		@UserName = @UserName,
		@StartTime = @StartTime,
		@EndTime = @EndTime,
		@Message = @Message,
		@Arguments = @Arguments,
		@StackTrace = @StackTrace,
		@CreatedBy = @CreatedBy,
		@ApplicationLogID = @ApplicationLogID OUTPUT
		
		

	
	
END
