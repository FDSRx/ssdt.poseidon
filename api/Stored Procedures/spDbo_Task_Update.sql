﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/23/2014
-- Description:	Updates a Task object.
-- 11/9/2015 - dhughes - Modified the procedure to receive a list of Note objects vs. a single object so that it is able
--					to perform batch updates (when applicable).
-- SAMPLE CALL:
/*

DECLARE
	@NoteID VARCHAR(MAX) = 21094,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@ScopeKey VARCHAR(50) = NULL,
	@NoteTypeKey VARCHAR(50) = NULL,
	@NotebookKey VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = '<div>MedSync fill for <a href="javascript:urlRedirect(''MED_SYNC'',30466 )">VIRGINIA SIMMS</a> on 01-07-2015</div><div>CARB/LEVO    TAB 25-250MG (2/6.00).</div>',
	@PriorityKey VARCHAR(50) = NULL,
	@OriginKey VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@DateDue DATETIME = GETDATE(),
	@AssignedByID BIGINT = NULL,
	@AssignedByString VARCHAR(256) = NULL,
	@AssignedToID BIGINT = NULL,
	@AssignedToString VARCHAR(256) = NULL,
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusKey VARCHAR(50) = NULL,
	@ParentTaskID BIGINT = NULL,
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@IsDateDueNullificationAllowed BIT = NULL,
	@IsAssignedByIDNullificationAllowed BIT = NULL,
	@IsAssignedByStringNullificationAllowed BIT = NULL,
	@IsAssignedToIDNullificationAllowed BIT = NULL,
	@IsAssignedToStringNullificationAllowed BIT = NULL,
	@IsCompletedByIDNullificationAllowed BIT = NULL,
	@IsCompletedByStringNullificationAllowed BIT = NULL,
	@IsDateCompletedNullificationAllowed BIT = NULL,
	@IsParentTaskIDNullificationAllowed BIT = NULL,
	@Debug BIT = 1
	
EXEC api.spDbo_Task_Update
	@NoteID = @NoteID,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@BusinessEntityID = @BusinessEntityID,
	@ScopeKey = @ScopeKey,
	@NoteTypeKey = @NoteTypeKey,
	@NotebookKey = @NotebookKey,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityKey = @PriorityKey,
	@OriginKey = @OriginKey,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@DateDue = @DateDue,
	@AssignedByID = @AssignedByID,
	@AssignedByString = @AssignedByString,
	@AssignedToID = @AssignedToID,
	@AssignedToString = @AssignedToString,
	@CompletedByID = @CompletedByID,
	@CompletedByString = @CompletedByString,
	@DateCompleted = @DateCompleted,
	@TaskStatusKey = @TaskStatusKey,
	@ParentTaskID = @ParentTaskID,
	@ModifiedBy = @ModifiedBy,
	@IsDateDueNullificationAllowed = @IsDateDueNullificationAllowed,
	@IsAssignedByIDNullificationAllowed = @IsAssignedByIDNullificationAllowed,
	@IsAssignedByStringNullificationAllowed = @IsAssignedByStringNullificationAllowed,
	@IsAssignedToIDNullificationAllowed = @IsAssignedToIDNullificationAllowed,
	@IsAssignedToStringNullificationAllowed = @IsAssignedToStringNullificationAllowed,
	@IsCompletedByIDNullificationAllowed = @IsCompletedByIDNullificationAllowed,
	@IsCompletedByStringNullificationAllowed = @IsCompletedByStringNullificationAllowed,
	@IsDateCompletedNullificationAllowed = @IsDateCompletedNullificationAllowed,
	@IsParentTaskIDNullificationAllowed = @IsParentTaskIDNullificationAllowed,
	@Debug = @Debug


*/

-- SELECT * FROM dbo.Task
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.NoteBook
-- SELECT * FROM dbo.vwTask WHERE NoteID = 21094
-- SELECT * FROM dbo.Note WHERE NoteID = 21094
-- =============================================
CREATE PROCEDURE [api].[spDbo_Task_Update]
	@NoteID VARCHAR(MAX),
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@ScopeKey VARCHAR(50) = NULL,
	@NoteTypeKey VARCHAR(50) = NULL,
	@NotebookKey VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityKey VARCHAR(50) = NULL,
	@OriginKey VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@DateDue DATETIME = NULL,
	@AssignedByID BIGINT = NULL,
	@AssignedByString VARCHAR(256) = NULL,
	@AssignedToID BIGINT = NULL,
	@AssignedToString VARCHAR(256) = NULL,
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusKey VARCHAR(50) = NULL,
	@ParentTaskID BIGINT = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@IsDateDueNullificationAllowed BIT = NULL,
	@IsAssignedByIDNullificationAllowed BIT = NULL,
	@IsAssignedByStringNullificationAllowed BIT = NULL,
	@IsAssignedToIDNullificationAllowed BIT = NULL,
	@IsAssignedToStringNullificationAllowed BIT = NULL,
	@IsCompletedByIDNullificationAllowed BIT = NULL,
	@IsCompletedByStringNullificationAllowed BIT = NULL,
	@IsDateCompletedNullificationAllowed BIT = NULL,
	@IsParentTaskIDNullificationAllowed BIT = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@ScopeKey=' + dbo.fnToStringOrEmpty(@ScopeKey) + ';' +
		'@NoteTypeKey=' + dbo.fnToStringOrEmpty(@NoteTypeKey) + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		--'@AdditionalData=' + dbo.fnToStringOrEmpty(@AdditionalData) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@DateDue=' + dbo.fnToStringOrEmpty(@DateDue) + ';' +
		'@AssignedByID=' + dbo.fnToStringOrEmpty(@AssignedByID) + ';' +
		'@AssignedByString=' + dbo.fnToStringOrEmpty(@AssignedByString) + ';' +
		'@AssignedToID=' + dbo.fnToStringOrEmpty(@AssignedToID) + ';' +
		'@AssignedToString=' + dbo.fnToStringOrEmpty(@AssignedToString) + ';' +
		'@CompletedByID=' + dbo.fnToStringOrEmpty(@CompletedByID) + ';' +
		'@CompletedByString=' + dbo.fnToStringOrEmpty(@CompletedByString) + ';' +
		'@DateCompleted=' + dbo.fnToStringOrEmpty(@DateCompleted) + ';' +
		'@TaskStatusKey=' + dbo.fnToStringOrEmpty(@TaskStatusKey) + ';' +
		'@ParentTaskID=' + dbo.fnToStringOrEmpty(@ParentTaskID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@IsDateDueNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsDateDueNullificationAllowed) + ';' +
		'@IsAssignedByIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedByIDNullificationAllowed) + ';' +
		'@IsAssignedByStringNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedByStringNullificationAllowed) + ';' +
		'@IsAssignedToIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedToIDNullificationAllowed) + ';' +
		'@IsAssignedToStringNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedToStringNullificationAllowed) + ';' +
		'@IsCompletedByIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsCompletedByIDNullificationAllowed) + ';' +
		'@IsCompletedByStringNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsCompletedByStringNullificationAllowed) + ';' +
		'@IsDateCompletedNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsDateCompletedNullificationAllowed) + ';' +
		'@IsParentTaskIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsParentTaskIDNullificationAllowed) + ';' ;

	-------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	-------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ScopeID INT = NULL,
		@NoteTypeID INT = NULL,
		@NotebookID INT = NULL,
		@PriorityID INT = NULL,
		@OriginID INT = NULL,
		@TaskStatusID INT = NULL,
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL
	
	-------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------------------------------------
	SET @ScopeID = dbo.fnGetScopeID(@ScopeKey);
	SET @NoteTypeID = dbo.fnGetNoteTypeID(@NoteTypeKey);
	SET @NotebookID = dbo.fnGetNotebookID(@NoteBookKey);
	SET @PriorityID = dbo.fnGetPriorityID(@PriorityKey);
	SET @OriginID = dbo.fnGetOriginID(@OriginKey);
	SET @TaskStatusID = dbo.fnGetTaskStatusID(@TaskStatusKey);
	SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationID);
	SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables.' AS ActionMethod,
			@ScopeID AS ScopeID, @NoteTypeID AS NoteTypeID, @NotebookID AS NotebookID, @PriorityID AS PriorityID,
			@OriginID AS OriginID, @TaskStatusID AS TaskStatusID, @ApplicationID AS ApplicationID,
			@BusinessID AS BusinessID, @Args AS Arguments
	END

	-------------------------------------------------------------------------------------------------------------------------
	-- Update Task object.
	-------------------------------------------------------------------------------------------------------------------------	
	EXEC dbo.spTask_Update
		@NoteID = @NoteID,
		@ApplicationID = @ApplicationID,
		@BusinessID = @BusinessID,
		@BusinessEntityID = @BusinessEntityID,
		@ScopeID = @ScopeID,
		@NoteTypeID = @NoteTypeID,
		@NoteBookID = @NoteBookID,
		@Title = @Title,
		@Memo = @Memo,
		@PriorityID = @PriorityID,
		@OriginID = @OriginID,
		@OriginDataKey = @OriginDataKey,
		@AdditionalData = @AdditionalData,
		@CorrelationKey = @CorrelationKey,
		@DateDue = @DateDue,
		@AssignedByID = @AssignedByID,
		@AssignedByString = @AssignedByString,
		@AssignedToID = @AssignedToID,
		@AssignedToString = @AssignedToString,
		@CompletedByID = @CompletedByID,
		@CompletedByString = @CompletedByString,
		@DateCompleted = @DateCompleted,
		@TaskStatusID = @TaskStatusID,
		@ParentTaskID = @ParentTaskID,
		@ModifiedBy = @ModifiedBy,
		@IsDateDueNullificationAllowed = @IsDateDueNullificationAllowed,
		@IsAssignedByIDNullificationAllowed = @IsAssignedByIDNullificationAllowed,
		@IsAssignedByStringNullificationAllowed = @IsAssignedByStringNullificationAllowed,
		@IsAssignedToIDNullificationAllowed = @IsAssignedToIDNullificationAllowed,
		@IsAssignedToStringNullificationAllowed = @IsAssignedToStringNullificationAllowed,
		@IsCompletedByIDNullificationAllowed = @IsCompletedByIDNullificationAllowed,
		@IsCompletedByStringNullificationAllowed = @IsCompletedByStringNullificationAllowed,
		@IsDateCompletedNullificationAllowed = @IsDateCompletedNullificationAllowed,
		@IsParentTaskIDNullificationAllowed = @IsParentTaskIDNullificationAllowed,
		@Debug = @Debug
		



END
