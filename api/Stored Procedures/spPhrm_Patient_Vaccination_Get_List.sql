﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/25/2014
-- Description:	Gets a list of Vaccination objects that are applicable to a patient.
-- SAMPLE CALL;
/*

EXEC api.spPhrm_Patient_Vaccination_Get_List
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@Skip = NULL,
	@Take = NULL
	
*/
 
-- SELECT * FROM phrm.fnGetPatientInformation(2) 
-- SELECT * FROM dbo.MedicationType
-- SELECT * FROM dbo.PrescriberType
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Vaccination_Get_List]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@VaccinationKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of Vaccination Ids
	@CVXCodeKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of CVXCode Ids
	@AdministrationRouteKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of AdmistrationRoute Ids
	@AdministrationSiteKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of AdmistrationSite Ids
	@MVXCodeKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of MVXCode Ids
	@FinancialClassKey VARCHAR(MAX) = NULL, -- Single or comma delmited list of FinancialClass Ids
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');	

	
	---------------------------------------------------------------------
	-- Return patient Vaccincation objects.
	---------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
		EXEC api.spVaccine_Vaccination_Get_List
			@VaccinationKey = @VaccinationKey,
			@PersonKey = @PersonID, 
			@AdministrationRouteKey = @AdministrationRouteKey, 
			@AdministrationSiteKey = @AdministrationSiteKey,
			@MVXCodeKey = @MVXCodeKey, 
			@FinancialClassKey = @FinancialClassKey, 
			@DataDateStart = @DataDateStart,
			@DataDateEnd = @DataDateEnd,
			@Skip = @Skip,
			@Take = @Take,
			@SortCollection = @SortCollection
	END
			
END
