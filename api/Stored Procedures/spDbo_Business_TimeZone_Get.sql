﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 5/19/2015
-- Description:	Returns the time zone for the provided business.
-- SAMPLE CALL:
/*
	DECLARE @Abbreviation VARCHAR(10) = NULL
	
	EXEC api.spDbo_Business_TimeZone_Get
		@BusinessKey = '2501624',
		@BusinessKeyType = 'NABP',
		@Abbreviation = @Abbreviation OUTPUT
	
	SELECT @Abbreviation AS Abbreviation
*/

-- SELECT * FROM dbo.TimeZone
-- =============================================
CREATE PROCEDURE [api].[spDbo_Business_TimeZone_Get]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@IsOutputOnly BIT = NULL,
	@TimeZoneID INT = NULL OUTPUT,
	@Code VARCHAR(25) = NULL OUTPUT,
	@Name VARCHAR(50) = NULL OUTPUT,
	@Prefix VARCHAR(10) = NULL OUTPUT,
	@Abbreviation VARCHAR(10) = NULL OUTPUT,
	@Offset INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	SET @TimeZoneID = NULL;
	SET @Code = NULL;
	SET @Name = NULL;
	SET @Prefix = NULL;
	SET @Abbreviation = NULL;
	SET @Offset = NULL;

	-------------------------------------------------------------------------------
	-- Declare variables.
	-------------------------------------------------------------------------------	
	
	-------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------	
	SELECT @TimeZoneID = Poseidon.dbo.fnGetBusinessTimeZoneID(@BusinessKey, @BusinessKeyType);

	-- Debug
	-- SELECT @TimeZoneID AS TimeZoneID

	-------------------------------------------------------------------------------
	-- Retrieve data.
	-------------------------------------------------------------------------------	
	SELECT TOP 1
		@Code = Code,
		@Name = Location,
		@Prefix = Prefix,
		@Offset = Offset
	--SELECT TOP 1 *
	FROM dbo.TimeZone
	WHERE TimeZoneID = @TimeZoneID
	
	SET @Abbreviation = dbo.fnGetBusinessTimeZoneAbbreviation(@BusinessKey, @BusinessKeyType);
	
	-------------------------------------------------------------------------------
	-- Return results (if applicable)
	-------------------------------------------------------------------------------	
	IF @IsOutputOnly = 0
	BEGIN
		SELECT 
			@TimeZoneID AS TimeZoneID,
			@Code AS Code,
			@Name AS Name,
			@Prefix AS Prefix,
			@Abbreviation AS Abbreviation,
			@Offset AS Offset
	END	
	

END
