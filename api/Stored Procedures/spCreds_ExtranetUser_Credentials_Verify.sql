﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/7/2014
-- Description:	Authenticates the provided credentials.
-- SAMPLE CALL:
/*

DECLARE 
	@IsValid BIT = 0,
	@CredentialEntityID BIGINT = NULL,
	@CredentialToken VARCHAR(50) = NULL

EXEC api.spCreds_ExtranetUser_Credentials_Verify
	@ApplicationKey = NULL,
	@BusinessKey = '7871787',
	@Username = 'dhughes',
	@Password = 'password',
	@CredentialToken = @CredentialToken OUTPUT,
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@IsValid = @IsValid OUTPUT

SELECT @IsValid AS IsValid, @CredentialEntityID AS CredentialEntityID, @CredentialToken AS CredentialToken

*/
-- SELECT * FROM dbo.vwStore WHERE BusinessEntityID = 10684
-- =============================================
CREATE PROCEDURE [api].[spCreds_ExtranetUser_Credentials_Verify]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	@Username VARCHAR(256) = NULL,
	@Password VARCHAR(50) = NULL,
	@CredentialEntityTypeKey VARCHAR(25) = NULL,
	@CredentialToken VARCHAR(50) = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@IsValid BIT = 0 OUTPUT,
	-- Core membership properties ****************************
	@IsApproved BIT = NULL OUTPUT,
	@IsLockedOut BIT = NULL OUTPUT,
	@DateLastLoggedIn DATETIME = NULL OUTPUT,
	@DateLastPasswordChanged DATETIME = NULL OUTPUT,
	@DatePasswordExpires DATETIME = NULL OUTPUT,
	@DateLastLockedOut DATETIME = NULL OUTPUT,
	@DateLockOutExpires DATETIME = NULL OUTPUT,
	@FailedPasswordAttemptCount INT = NULL OUTPUT,
	@FailedPasswordAnswerAttemptCount INT = NULL OUTPUT,
	@IsDisabled BIT = NULL OUTPUT,
	-- *******************************************************
	@IsChangePasswordRequired BIT = NULL OUTPUT,
	@IsFirstLogin BIT = NULL OUTPUT,
	@IsOutputOnly BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------------
	SET @IsValid = 0;
	SET @IsApproved = 0;
	SET	@IsLockedOut = 0;
	SET	@DateLastLoggedIn = NULL;
	SET	@DateLastPasswordChanged = NULL;
	SET @DatePasswordExpires = NULL;
	SET	@DateLastLockedOut = NULL;
	SET @DateLockOutExpires = NULL;
	SET	@FailedPasswordAttemptCount = 0;
	SET	@FailedPasswordAnswerAttemptCount = 0;
	SET @IsDisabled = 0;	
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);	

	---------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------	
	DECLARE 
		@BusinessID BIGINT = NULL,
		@ApplicationID INT,
		@CredentialEntityTypeID INT


	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialTypeKey = @CredentialEntityTypeKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityTypeID = @CredentialEntityTypeID OUTPUT,
		@IsOutputOnly = 1
	
	-- Debug
	--SELECT @ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @Username AS Username, @Password AS Password, @CredentialEntityTypeID AS CredentialEntityTypeID
	
	
	
	---------------------------------------------------------------------------
	-- Authenticate credentials
	---------------------------------------------------------------------------
	EXEC creds.spExtranetUser_Credentials_Verify
		@ApplicationID = @ApplicationID,
		@BusinessID = @BusinessID,
		@Username = @Username,
		@Password = @Password,
		@CredentialToken = @CredentialToken OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@IsValid = @IsValid OUTPUT,
		-- Core membership properties ****************************
		@IsApproved = @IsApproved OUTPUT,
		@IsLockedOut = @IsLockedOut OUTPUT,
		@DateLastLoggedIn = @DateLastLoggedIn OUTPUT,
		@DateLastPasswordChanged = @DateLastPasswordChanged OUTPUT,
		@DatePasswordExpires = @DatePasswordExpires OUTPUT,
		@DateLastLockedOut = @DateLastLockedOut OUTPUT,
		@DateLockOutExpires = @DateLockOutExpires OUTPUT,		
		@FailedPasswordAttemptCount = @FailedPasswordAttemptCount OUTPUT,
		@FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount OUTPUT,
		@IsDisabled = @IsDisabled OUTPUT,
		-- *******************************************************
		@IsChangePasswordRequired = @IsChangePasswordRequired OUTPUT,
		@IsFirstLogin = @IsFirstLogin OUTPUT
	
	
	---------------------------------------------------------------------------
	-- Response data.
	-- <Summary>
	-- Returns a set of credential items in the response.
	-- </Summary>
	---------------------------------------------------------------------------
	IF @IsOutputOnly = 0
	BEGIN
	
		SELECT
			@IsValid AS IsValid,
			@CredentialEntityID AS CredentialEntityID,
			@CredentialToken AS CredentialToken,
			@ApplicationID AS ApplicationID,
			@BusinessID AS BusinessID,
			-- Core membership properties *******************************************
			@IsApproved AS IsApproved,
			@IsLockedOut AS IsLockedOut,
			@DateLastLoggedIn AS DateLastLoggedIn,
			@DateLastPasswordChanged AS DateLastPasswordChanged,
			@DatePasswordExpires AS DatePasswordExpires,
			@DateLastLockedOut AS DateLastLocked,
			@DateLockOutExpires AS DateLockOutExpires,
			@FailedPasswordAttemptCount AS FailedPasswordAttemptCount,
			@FailedPasswordAnswerAttemptCount AS FailedPassswordAnswerAttemptCount,
			@IsDisabled AS IsDisabled,
			-- *******************************************************
			@IsChangePasswordRequired AS IsChangePasswordRequired,
			@IsFirstLogin AS IsFirstLogin
		
	END	
	
	
	
	
	
		
END
