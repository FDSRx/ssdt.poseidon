﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/7/2014
-- Description:	Marks a task(s) as being complete.
-- Change Log:
-- 11/9/2015 - dhughes - Modified the procedure to receive a list of Note objects vs. a single object so that it is able
--					to perform batch updates (when applicable).
-- 6/10/2016 - dhughes - Modified the procedure to accept the Application object parameter.

-- SAMPLE CALL:
/*

DECLARE
	-- Business/Patient properties
	@BusinessKey VARCHAR(50) = '2510964',
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50) = '57454',
	@PatientKeyType VARCHAR(50) = NULL,
	-- Task properties
	@NoteID BIGINT = 41678,
	@CompletedByKey VARCHAR(50) = NULL,
	@CompletedByString VARCHAR(256)	= NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusKey VARCHAR(50) = NULL,
	-- Note interaction properties
	@NoteInteractionID BIGINT = NULL,
	@InteractionID BIGINT = NULL,
	@InteractedWithKey VARCHAR(50) = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeKey VARCHAR(25) = NULL,
	-- Interaction properties
	@InteractionTypeKey VARCHAR(25) = NULL,
	@DispositionTypeKey VARCHAR(25) = NULL,
	@InitiatedByKey VARCHAR(50) = NULL,
	@InitiatedByString VARCHAR(256) = NULL,
	-- Comment Note
	@CommentNoteID BIGINT = NULL,
	@NoteTypeKey VARCHAR(25) = NULL,
	@NotebookKey VARCHAR(25) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityKey INT = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@OriginKey INT = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData VARCHAR(MAX) = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	-- Common properties
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = 'supportAdmin',
	@Debug BIT = 1

EXEC api.spPhrm_Patient_Task_Complete
	-- Business/Patient properties
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	-- Task properties
	@NoteID = @NoteID,
	@CompletedByKey = @CompletedByKey,
	@CompletedByString = @CompletedByString,
	@DateCompleted = @DateCompleted,
	@TaskStatusKey = @TaskStatusKey,
	-- Note interaction properties
	@NoteInteractionID = @NoteInteractionID OUTPUT,
	@InteractionID = @InteractionID OUTPUT,
	@InteractedWithKey = @InteractedWithKey,
	@InteractedWithString = @InteractedWithString,
	@ContactTypeKey = @ContactTypeKey,
	-- Interaction properties
	@InteractionTypeKey = @InteractionTypeKey,
	@DispositionTypeKey = @DispositionTypeKey,
	@InitiatedByKey = @InitiatedByKey,
	@InitiatedByString = @InitiatedByString,
	-- Comment Note
	@CommentNoteID = @CommentNoteID OUTPUT,
	@NoteTypeKey = @NoteTypeKey OUTPUT,
	@NotebookKey = @NotebookKey,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityKey = @PriorityKey,
	@Tags = @Tags, -- A comma separated list of tags that identify a note.
	@OriginKey = @OriginKey,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	-- Common properties
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

SELECT @NoteID AS NoteID, @InteractionID AS InteractionID, @NoteInteractionID AS NoteInteractionID

*/

-- SELECT * FROM dbo.vwTask ORDER BY NoteID DESC
-- SELECT * FROM dbo.vwNote ORDER BY NoteID DESC
-- SELECT * FROM dbo.vwCalendarItem ORDER BY NoteID DESC
-- SELECT * FROM dbo.Interaction ORDER BY InteractionID DESC
-- SELECT * FROM dbo.NoteInteraction ORDER BY NoteInteractionID DESC
-- SELECT * FROM creds.CredentialEntity ORDER BY CredentialEntityID DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Task_Complete]
	-- Business/Patient properties
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	-- Task properties
	@NoteID VARCHAR(MAX),
	@CompletedByKey VARCHAR(50) = NULL,
	@CompletedByString VARCHAR(256)	= NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusKey VARCHAR(50) = NULL,
	-- Note interaction properties
	@NoteInteractionID BIGINT = NULL OUTPUT,
	@InteractionID BIGINT = NULL OUTPUT,
	@InteractedWithKey VARCHAR(50) = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeKey VARCHAR(25) = NULL,
	-- Interaction properties
	@InteractionTypeKey VARCHAR(25) = NULL,
	@DispositionTypeKey VARCHAR(25) = NULL,
	@InitiatedByKey VARCHAR(50) = NULL,
	@InitiatedByString VARCHAR(256) = NULL,
	-- Comment Note
	@CommentNoteID BIGINT = NULL OUTPUT,
	@NoteTypeKey VARCHAR(25) = NULL OUTPUT,
	@NotebookKey VARCHAR(25) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityKey INT = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@OriginKey INT = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData VARCHAR(MAX) = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	-- Common properties
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
	
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@Trancount INT = @@TRANCOUNT,
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL

	DECLARE @Args VARCHAR(MAX) =
		-- Business/Patient properties
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType) + ';' +
		-- Task properties
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@CompletedByKey=' + dbo.fnToStringOrEmpty(@CompletedByKey) + ';' +
		'@CompletedByString=' + dbo.fnToStringOrEmpty(@CompletedByString) + ';' +
		'@DateCompleted=' + dbo.fnToStringOrEmpty(@DateCompleted) + ';' +
		'@TaskStatusKey=' + dbo.fnToStringOrEmpty(@TaskStatusKey) + ';' +
		-- Note interaction properties
		'@NoteInteractionID=' + dbo.fnToStringOrEmpty(@NoteInteractionID) + ';' +
		'@InteractionID=' + dbo.fnToStringOrEmpty(@InteractionID) + ';' +
		'@InteractedWithKey=' + dbo.fnToStringOrEmpty(@InteractedWithKey) + ';' +
		'@InteractedWithString=' + dbo.fnToStringOrEmpty(@InteractedWithString) + ';' +
		'@ContactTypeKey=' + dbo.fnToStringOrEmpty(@ContactTypeKey) + ';' +
		-- Interaction properties
		'@InteractionTypeKey=' + dbo.fnToStringOrEmpty(@InteractionTypeKey) + ';' +
		'@DispositionTypeKey=' + dbo.fnToStringOrEmpty(@DispositionTypeKey) + ';' +
		'@InitiatedByKey=' + dbo.fnToStringOrEmpty(@InitiatedByKey) + ';' +
		'@InitiatedByString=' + dbo.fnToStringOrEmpty(@InitiatedByString) + ';' +
		-- Comment Note
		'@CommentNoteID=' + dbo.fnToStringOrEmpty(@CommentNoteID) + ';' +
		'@NoteTypeKey=' + dbo.fnToStringOrEmpty(@NoteTypeKey) + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(@AdditionalData) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		-- Common properties
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request	
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = NULL,
		@Arguments = @Args
	*/
	
	------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------------------------------------------------------
	SET @DateCompleted = ISNULL(@DateCompleted, GETDATE());
	SET @Debug = ISNULL(@Debug, 0);
	SET @BusinesskeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @PatientKeyType = ISNULL(@PatientKeyType, 'SourcePatientKey');
	
	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
	DEClARE
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT,
		@PatientID BIGINT,
		@PersonID BIGINT,
		@CompletedByID BIGINT,
		@InteractedWithID BIGINT,
		@InitiatedByID BIGINT,
		@ScopeID INT = dbo.fnGetScopeID('INTRN'), -- only create internal notes
		@NoteTypeID INT = ISNULL(dbo.fnGetNoteTypeID(@NoteTypeKey), dbo.fnGetNoteTypeID('GNRL')),
		@NotebookID INT = dbo.fnGetNotebookID(@NotebookKey),
		@PriorityID INT = dbo.fnGetPriorityID(@PriorityKey),
		@OriginID INT = dbo.fnGetOriginID(@OriginKey),
		@ContactTypeID INT = dbo.fnGetContactTypeID(@ContactTypeKey),
		@InteractionTypeID INT = dbo.fnGetInteractionTypeID(@InteractionTypeKey),
		@DispositionTypeID INT = dbo.fnGetDispositionTypeID(@DispositionTypeKey),
		@TaskStatusID INT = dbo.fnGetTaskStatusID(@TaskStatusKey)

	------------------------------------------------------------------------------------------------------------
	-- Set local variables
	------------------------------------------------------------------------------------------------------------
	SET @BusinessID = CASE WHEN @BusinessKeyType IS NOT NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType) ELSE NULL END;

	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, @BusinessKeyType, @PatientKey, @PatientKeyType);
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');
	
	SET @CompletedByID = creds.fnGetCredentialEntityID(@CompletedByKey);
	
	SET @InteractedWithID = dbo.fnGetBusinessEntityID(@InteractedWithKey);
	
	SET @InitiatedByID = @CompletedByID;

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set method.' AS ActionMethod,
			@Args AS Arguments, @NoteID AS NoteID, @BusinessID AS BusinessID, @PatientID AS PatientID, @PersonID AS PersonID,
			@CompletedByID AS CompletedByID, @InteractedWithID AS InteractedWithID, @InitiatedByID AS InitiatedByID
	END
	
	------------------------------------------------------------------------------------------------------------
	-- Complete/Close the patient task
	-- <Summary>
	-- Marks the patient task as completed and adds an Interaction object
	-- and supplemental information (if applicable).
	-- <Summary>
	------------------------------------------------------------------------------------------------------------
	IF @PatientID IS NOT NULL
		OR (@BusinessID IS NOT NULL AND @NoteID IS NOT NULL)
	BEGIN

		EXEC dbo.spTask_Complete
			-- Task properties
			@NoteID = @NoteID,
			@ApplicationID = @ApplicationID,
			@BusinessID = @BusinessID,
			@CompletedByID = @CompletedByID,
			@CompletedByString = @CompletedByString,
			@DateCompleted = @DateCompleted,
			@TaskStatusID = @TaskStatusID,
			-- Note interaction properties
			@NoteInteractionID = @NoteInteractionID OUTPUT,
			@InteractionID = @InteractionID OUTPUT,
			@InteractedWithID = @InteractedWithID,
			@InteractedWithString = @InteractedWithString,
			@ContactTypeID = @ContactTypeID,
			-- Interaction properties
			@InteractionTypeID = @InteractionTypeID,
			@DispositionTypeID = @DispositionTypeID,
			@InitiatedByID = @InitiatedByID,
			@InitiatedByString = @InitiatedByString,
			-- Comment Note
			@CommentNoteID = @CommentNoteID,
			@ScopeID = @ScopeID,
			@BusinessEntityID = @PersonID,
			@NoteTypeID = @NoteTypeID,
			@NotebookID = @NotebookID,
			@Title = @Title,
			@Memo = @Memo,
			@PriorityID = @PriorityID,
			@Tags = @Tags, -- A comma separated list of tags that identify a note.
			@OriginID = @OriginID,
			@OriginDataKey = @OriginDataKey,
			@AdditionalData = @AdditionalData,
			@CorrelationKey = @CorrelationKey,
			-- Entry properties
			@CreatedBy = @CreatedBy,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug

	END
	
	
END
