﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/24/2014
-- Description:	Returns a single PredefinedMessage item.
-- SAMPLE CALL:
/*

EXEC api.spMsg_PredefinedMessage_Get_Single
	@ApplicationKey = NULL,
	@BusinessKey = NULL,
	@BusinessKeyType = NULL,
	@PredefinedMessageID = 1
	
*/
-- SELECT * FROM msg.PredefinedMessage
-- =============================================
CREATE PROCEDURE api.spMsg_PredefinedMessage_Get_Single
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PredefinedMessageID BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	----------------------------------------------------------------------
	-- Fetch data
	----------------------------------------------------------------------
	EXEC api.spMsg_PredefinedMessage_Get_List
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PredefinedMessageKey = @PredefinedMessageID
END
