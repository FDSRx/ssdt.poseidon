﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/6/2014
-- Description:	Api wrapper to create a patient record.
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Create]
	--Business
	@StoreID INT = NULL,
	@Nabp VARCHAR(25) = NULL,
	@BusinessEntityID INT = NULL,
	--Patient
	@SourcePatientKey VARCHAR(50),
	--Person
	@PersonID INT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL, -- Required to try and create a unique person
	@LastName VARCHAR(75) = NULL, -- Required to try and create a unique person
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL, -- Required to try and create a unique person
	@GenderCode VARCHAR(25) = NULL,
	@GenderID INT  = NULL, -- Required to try and create a unique person
	@LanguageCode VARCHAR(25) = NULL,
	@LanguageID INT = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15), -- Required to try and create a unique person
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	-- MPC Pharmacy Service
	@Service_Mpc_ConnectionStatusID INT = NULL,
	@Service_Mpc_ConnectionStatusCode VARCHAR(25) = NULL,
	@Service_Mpc_AccountKey VARCHAR(256) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Error
	@ErrorLogID INT = NULL OUTPUT,
	-- User output 
	@PatientID INT = NULL OUTPUT,
	@PatientExists BIT = 0 OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	-----------------------------------------------------------------------
	-- Translate raw data keys into business objects.
	-----------------------------------------------------------------------
	-- Get BusinessEntityID (Store)
	SET @BusinessEntityID = 
		CASE 
			WHEN @BusinessEntityID IS NOT NULL THEN @BusinessEntityID
			WHEN @StoreID IS NOT NULL THEN dbo.fnGetStoreIDFromSourceID(@StoreID)
			WHEN ISNULL(@Nabp, '') <> '' THEN dbo.fnGetStoreIDByNABP(@Nabp)
			ELSE NULL
		END;
	
	-- Get LanguageID
	SET @LanguageID = ISNULL(@LanguageID, dbo.fnGetLanguageID(@LanguageCode));
	
	-- Get GenderID
	SET @GenderID = ISNULL(@GenderID, dbo.fnGetGenderID(@GenderCode));
	
	-- Get MPC service connection status.
	SET @Service_Mpc_ConnectionStatusID = ISNULL(@Service_Mpc_ConnectionStatusID, dbo.fnGetConnectionStatusID(@Service_Mpc_ConnectionStatusCode));
	
	
	-----------------------------------------------------------------------
	-- Create patient record.
	-----------------------------------------------------------------------
	EXEC phrm.spPatient_Create
		--Business
		@BusinessEntityID = @BusinessEntityID,
		--Pharmacy Key
		@SourcePatientKey = @SourcePatientKey,
		--Person
		@Title = @Title,
		@FirstName = @FirstName,
		@LastName = @LastName,
		@MiddleName = @MiddleName,
		@Suffix = @Suffix,
		@BirthDate = @BirthDate,
		@GenderID = @GenderID,
		@LanguageID = @LanguageID,
		--Person Address
		@AddressLine1 = @AddressLine1,
		@AddressLine2 = @AddressLine2,
		@City = @City,
		@State = @State,
		@PostalCode = @PostalCode,
		--Person Phone
		--	Home
		@PhoneNumber_Home = @PhoneNumber_Home,
		@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
		@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
		-- Mobile
		@PhoneNumber_Mobile = @PhoneNumber_Mobile,
		@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
		@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
		--Person Email
		--	Primary
		@EmailAddress_Primary = @EmailAddress_Primary,
		@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
		--	Alternate
		@EmailAddress_Alternate = @EmailAddress_Alternate,
		@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
		-- MPC Pharmacy Service
		@Service_Mpc_ConnectionStatusID = @Service_Mpc_ConnectionStatusID,
		@Service_Mpc_AccountKey = @Service_Mpc_AccountKey,
		-- Caller
		@CreatedBy = @CreatedBy,
		--Output
		@PatientID = @PatientID OUTPUT,
		@PatientExists = @PatientExists OUTPUT,
		@ErrorLogID = @ErrorLogID OUTPUT
				
				
END
