﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/4/2016
-- Description:	Returns a list of ConnectionStatus objects.

/*

DECLARE
	@ConnectionStatusID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL,
	@Name VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1


EXEC api.spDbo_ConnectionStatus_Get_List
	@ConnectionStatusID = @ConnectionStatusID,
	@Code = @Code,
	@Name = @Name,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug

*/

-- SELECT * FROM dbo.ConnectionStatus
-- =============================================
CREATE PROCEDURE [api].[spDbo_ConnectionStatus_Get_List]
	@ConnectionStatusID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL,
	@Name VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN


	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@ConnectionStatusID=' + dbo.fnToStringOrEmpty(@ConnectionStatusID)  + ';' +
		'@Code=' + dbo.fnToStringOrEmpty(@Code)  + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);

	
	
	------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------

	
	------------------------------------------------------------------------------------------------------------
	-- Set variables
	------------------------------------------------------------------------------------------------------------
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ConnectionStatusID AS ConnectionStatusID, @Code AS Code, @Name AS Name
	END
	

	------------------------------------------------------------------------------------------------------------
	-- Paging.
	------------------------------------------------------------------------------------------------------------		
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	

	------------------------------------------------------------------------------------------------------------
	-- Create result set
	------------------------------------------------------------------------------------------------------------
	;WITH cteItems AS (
	
		SELECT
			ROW_NUMBER() OVER(ORDER BY
				CASE WHEN @SortField IN ('DateCreated') AND @SortDirection = 'asc'  THEN cs.DateCreated END ASC, 
				CASE WHEN @SortField IN ('DateCreated') AND @SortDirection = 'desc'  THEN cs.DateCreated END DESC,   
				CASE WHEN @SortField IN ('Code') AND @SortDirection = 'asc'  THEN cs.Code END ASC,
				CASE WHEN @SortField IN ('Code')  AND @SortDirection = 'desc'  THEN cs.code END DESC,  
				CASE WHEN @SortField IN ('Name') AND @SortDirection = 'asc'  THEN cs.Name END ASC,
				CASE WHEN @SortField IN ('Name')  AND @SortDirection = 'desc'  THEN cs.Name END DESC,  
				cs.Name
			) AS RowNumber,
			cs.ConnectionStatusID,
			cs.Code,
			cs.Name,
			NULL AS Description,
			cs.IsActive,
			cs.IsSelfOperated,
			cs.rowguid,
			cs.DateCreated,
			cs.DateModified,
			cs.CreatedBy,
			cs.ModifiedBy
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.ConnectionStatus cs				
	)
	
	SELECT 
		ConnectionStatusID,
		Code,
		Name,
		Description,
		IsActive,
		IsSelfOperated,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
	FROM cteItems
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
			
END