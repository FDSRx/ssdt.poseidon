﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/3/2014
-- Description:	Creates a new Extranet User object.
-- SAMPLE CALL:

/*
-- Good call
EXEC api.spCreds_ExtranetUser_Create
	@ApplicationKey = 1,
	@BusinessKey = 10684,
	@Username = 'cpollard',
	@Password = 'password',
	@RoleKey = 'Admin',
	--,@PersonID = 1
	--,@FirstName = 'Test Administrator'
	--,@LastName = 'hughes'
*/

-- SELECT * FROM acl.Roles
-- SELECT * FROM dbo.Application
-- SELECT * FROM creds.CredentialEntity
-- SELECT * FROM creds.Membership
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- SELECT * FROM dbo.Person ORDER BY PersonID DESC
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [api].[spCreds_ExtranetUser_Create]
	--Application
	@ApplicationKey VARCHAR(MAX),
	--Business
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(25) = NULL,
	-- Roles
	@RoleKey VARCHAR(MAX) = NULL,
	--User
	@Username VARCHAR(256),
	@Password VARCHAR(50),
	@PasswordQuestionID INT = NULL,
	@PasswordAnswer VARCHAR(500) = NULL,
	--Membership
	@IsApproved BIT = NULL,
	@IsChangePasswordRequired BIT = NULL,
	@DatePasswordExpires DATETIME = NULL,
	@IsDisabled BIT = NULL,
	--Person
	@PersonID INT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderKey VARCHAR(25) = NULL,
	@LanguageKey VARCHAR(25) = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Ouput
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@CredentialEntityExists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Debug: Log request
	/*
	--Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID) + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName) + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName) + ';' +
		'@BirthDate=' + dbo.fnToStringOrEmpty(@BirthDate) + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary) + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate) + ';' +
		'@PhoneNumber_Home=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home) + ';' +
		'@PhoneNumber_Mobile=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile) + ';' 
	
	DECLARE @Source VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Source,
		@Message = 'The request was fired.',
		@Arguments = @Args	
	*/

	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @CredentialEntityID = NULL;
	
	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE
		@BusinessID BIGINT = NULL,
		@GenderID INT = NULL,
		@LanguageID INT = NULL,
		@ErrorMessage VARCHAR(4000)

	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	SET @GenderID = dbo.fnGetGenderID(@GenderKey);
	SET @LanguageID = dbo.fnGetLanguageID(@LanguageID);	
	
	-- Translate basic system keys.
	EXEC api.spDbo_System_KeyTranslator
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1
	
	
	BEGIN TRY
	
		--------------------------------------------------------------------------------------------
		-- Create a New Exranet User object.
		--------------------------------------------------------------------------------------------	
		EXEC creds.spExtranetUser_Create
			--Application
			@ApplicationIDList = @ApplicationKey,
			--Business
			@BusinessEntityID = @BusinessID,
			-- Roles
			@RoleIDList = @RoleKey,
			--User
			@Username = @Username,
			@Password = @Password,
			@PasswordQuestionID = @PasswordQuestionID,
			@PasswordAnswer = @PasswordAnswer,
			--Membership
			@IsApproved = @IsApproved,
			@IsChangePasswordRequired = @IsChangePasswordRequired,
			@DatePasswordExpires = @DatePasswordExpires,
			@IsDisabled = @IsDisabled,
			--Person
			@Title = @Title,
			@FirstName = @FirstName,
			@LastName = @LastName,
			@MiddleName = @MiddleName,
			@Suffix = @Suffix,
			@BirthDate = @BirthDate,
			@GenderID = @GenderID,
			@LanguageID = @LanguageID,
			--Person Address
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			--Person Phone
			--	Home
			@PhoneNumber_Home = @PhoneNumber_Home,
			@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
			@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
			-- Mobile
			@PhoneNumber_Mobile = @PhoneNumber_Mobile,
			@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
			@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
			--Person Email
			--	Primary
			@EmailAddress_Primary = @EmailAddress_Primary,
			@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
			--	Alternate
			@EmailAddress_Alternate = @EmailAddress_Alternate,
			@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
			-- Caller
			@CreatedBy = @CreatedBy,
			-- Ouput
			@CredentialEntityID = @CredentialEntityID OUTPUT,
			@CredentialEntityExists = @CredentialEntityExists OUTPUT
			
	END TRY
	BEGIN CATCH	
		
		EXECUTE [dbo].spLogError;
		
		SET @ErrorMessage = ERROR_MESSAGE();		
	
	END CATCH
	
	
	--------------------------------------------------------------------------------------------
	-- Return response.
	--------------------------------------------------------------------------------------------
	DECLARE @ExistsWildcard VARCHAR(256) = 'CredentialEntityID already exists';
	
	SELECT
		CONVERT(BIT, 
			CASE
				WHEN @ErrorMessage IS NOT NULL THEN 0
				ELSE 1
			END) AS IsSuccess,
		CASE 
			WHEN @ErrorMessage LIKE '%' + @ExistsWildcard + '%' THEN 'The user you are trying to create already exists in the system.'
			ELSE @ErrorMessage
		END AS Message,
		@CredentialEntityID AS CredentialEntityID,
		CONVERT(BIT, 
			CASE 
				WHEN @ErrorMessage LIKE '%' + @ExistsWildcard + '%' THEN 1
				ELSE 0
			END) AS CredentialExists
	
	
END
