﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/02/2014
-- Description:	Gets a list of Task objects.
-- Change Log:
-- 11/19/2015 - dhughes - Modified the "ROW_NUMBER()" method to only perform the process once vs. multiple scenarios to help increase
--							speed and performance.
-- 11/19/2015 - dhughes - Modified the process to generate the tag(s) XML information after the final record set has been determined.
-- 6/17/2016 - dhughes - Modified the procedure to accept the Application and Business object parameters.

-- SAMPLE CALL:
/*

DECLARE
	@ApplicationKey VARCHAR(50) = 'ENG',
	@BusinessKey VARCHAR(50) = '2501624',
	@BusinessKeyType VARCHAR(50) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Task object identifiers.
	@ScopeKey VARCHAR(MAX) = 2, -- NOTE: Key will only be used in absense of Id.
	@BusinessEntityKey VARCHAR(MAX), -- NOTE: A single or comma delimited list of BusinessEntity keys.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_Task_Get_List
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@NoteKey = @NoteKey,
	@ScopeKey = @ScopeKey,
	@BusinessEntityKey = @BusinessEntityKey,
	@NotebookKey = @NotebookKey,
	@PriorityKey = @PriorityKey,
	@OriginKey = @OriginKey,
	@TagKey = @TagKey,
	@DataDateStart = @DataDateStart,
	@DataDateEnd = @DataDateEnd,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug

	
*/
-- SELECT * FROM dbo.Task
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spDbo_Task_Get_List]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Task object identifiers.
	@ScopeKey VARCHAR(MAX) = NULL, -- NOTE: Key will only be used in absense of Id.
	@BusinessEntityKey VARCHAR(MAX), -- NOTE: A single or comma delimited list of BusinessEntity keys.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@DataDateStart DATETIME = NULL,
	@DataDateEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@NoteKey=' + dbo.fnToStringOrEmpty(@NoteKey)  + ';' +
		'@ScopeKey=' + dbo.fnToStringOrEmpty(@ScopeKey)  + ';' +
		'@BusinessEntityKey=' + dbo.fnToStringOrEmpty(@BusinessEntityKey)  + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey)  + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey)  + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey)  + ';' +
		'@TagKey=' + dbo.fnToStringOrEmpty(@TagKey)  + ';' +
		'@DataDateStart=' + dbo.fnToStringOrEmpty(@DataDateStart)  + ';' +
		'@DataDateEnd=' + dbo.fnToStringOrEmpty(@DataDateEnd)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;


	-----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------------------------------------------------------------
	SET @DataDateStart = ISNULL(@DataDateStart, '1/1/1900');
	SET @Debug = ISNULL(@Debug, 0);
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@NoteTypeKey VARCHAR(25),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType),
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey)
		
	-----------------------------------------------------------------------------------------------------------------------------
	-- Set variables
	-----------------------------------------------------------------------------------------------------------------------------	
	SET @ScopeKey = dbo.fnGetScopeID(@ScopeKey);
	SET @NoteTypeKey = dbo.fnNoteTypeKeyTranslator('TSK', DEFAULT);
	SET @NotebookKey = dbo.fnNotebookKeyTranslator(@NotebookKey, DEFAULT);
	SET @PriorityKey = dbo.fnPriorityKeyTranslator(@PriorityKey, DEFAULT);
	SET @OriginKey = dbo.fnOriginKeyTranslator(@OriginKey, DEFAULT);
	SET @TagKey = dbo.fnTagKeyTranslator(@TagKey, DEFAULT);
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Deubber On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeType,
			@BusinessID AS BusinessID, @NoteTypeKey AS NoteTypeKey, @NotebookKey AS NotebookKey, @PriorityKey AS PriorityKey
	END


	-----------------------------------------------------------------------------------------------------------------------------
	-- Paging configuration.
	-----------------------------------------------------------------------------------------------------------------------------	
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)
			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'Title') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Create result set
	-----------------------------------------------------------------------------------------------------------------------------
	-- SELECT * FROM dbo.Note
	
	;WITH cteNotes AS (
	
		SELECT
			ROW_NUMBER() OVER (ORDER BY	
				CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC,		
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'asc'  THEN n.Title END  ASC,
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'desc'  THEN n.Title END DESC,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'asc'  THEN n.DateCreated END ASC ,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'desc'  THEN n.DateCreated END DESC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'asc'  THEN n.DateModified END ASC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'desc'  THEN n.DateModified END DESC ,     
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'asc'  THEN n.CreatedBy END ASC,
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'desc'  THEN n.CreatedBy END DESC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'asc'  THEN n.ModifiedBy END ASC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'desc'  THEN n.ModifiedBy END  DESC,
				CASE WHEN @SortField IS NULL OR @SortField = '' THEN n.NoteID ELSE n.NoteID  END DESC
			) AS RowNumber,
			--CASE    
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC, n.DateCreated ASC) 
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC, n.DateCreated DESC)
			--	WHEN @SortField = 'Title' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC, n.Title ASC) 
			--	WHEN @SortField = 'Title' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC, n.Title DESC)  
			--	ELSE ROW_NUMBER() OVER (ORDER BY CASE WHEN tsk.DateCompleted IS NOT NULL THEN 1 ELSE 0 END ASC, n.NoteID DESC)
			--END AS RowNumber,	
			tsk.NoteID AS TaskID,
			n.NotebookID AS NotebookID,
			nb.Code AS NotebookCode,
			nb.Name AS NotebookName,
			n.Title,
			n.Memo,
			--(
			--	SELECT t.TagID AS Id, t.Name AS Name
			--	FROM dbo.NoteTag ntag (NOLOCK)
			--		JOIN dbo.Tag t
			--			ON ntag.TagID = t.TagID
			--	WHERE ntag.NoteID = n.NoteID
			--	FOR XML PATH('Tag'), ROOT('Tags')
			--) AS Tags,
			n.PriorityID,
			p.Code AS PriorityCode,
			p.Name AS PriorityName,
			n.OriginID,
			ds.Code AS OriginCode,
			ds.Name AS OriginName,
			n.OriginDataKey,
			tsk.DateDue,
			tsk.DateCompleted,
			tsk.DateCreated,
			tsk.DateModified,
			tsk.CreatedBy,
			tsk.ModifiedBy			
		FROM dbo.Task tsk (NOLOCK)
			JOIN dbo.Note n (NOLOCK)
				ON tsk.NoteID = n.NoteID
			LEFT JOIN dbo.NoteType nt (NOLOCK)
				ON n.NoteTypeID = nt.NoteTypeID
			LEFT JOIN dbo.Priority p (NOLOCK)
				ON n.PriorityID = p.PriorityID
			LEFT JOIN dbo.Notebook nb (NOLOCK)
				ON n.NotebookID = nb.NotebookID	
			LEFT JOIN dbo.Origin ds (NOLOCK)
				ON n.OriginID = ds.OriginID
		WHERE ( @NoteKey IS NULL OR (@NoteKey IS NOT NULL AND tsk.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ScopeID = @ScopeKey
			AND ( @BusinessEntityKey IS NULL OR (@BusinessEntityKey IS NOT NULL AND n.BusinessEntityID IN (SELECT Value FROM dbo.fnSplit(@BusinessEntityKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @NoteTypeKey IS NULL OR (@NoteTypeKey IS NOT NULL AND n.NoteTypeID IN (SELECT Value FROM dbo.fnSplit(@NoteTypeKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @NotebookKey IS NULL OR (@NotebookKey IS NOT NULL AND n.NotebookID IN (SELECT Value FROM dbo.fnSplit(@NotebookKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @PriorityKey IS NULL OR (@PriorityKey IS NOT NULL AND n.PriorityID IN (SELECT Value FROM dbo.fnSplit(@PriorityKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @OriginKey IS NULL OR (@OriginKey IS NOT NULL AND n.OriginID IN (SELECT Value FROM dbo.fnSplit(@OriginKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @TagKey IS NULL OR (@TagKey IS NOT NULL AND n.NoteID IN (
				SELECT DISTINCT NoteID FROM NoteTag WHERE TagID IN(SELECT Value FROM dbo.fnSplit(@TagKey, ',') WHERE ISNUMERIC(Value) = 1))
			))
			AND ( @DataDateEnd IS NULL OR (@DataDateEnd IS NOT NULL AND (
				tsk.DateCreated >= @DataDateStart
					AND tsk.DateCreated <= @DataDateEnd
			)))
	)
	
	-- Fetch results from common table expression
	SELECT
		TaskID,
		NotebookID,
		NotebookCode,
		NotebookName,
		Title,
		Memo,
		(
			SELECT t.TagID AS Id, t.Name AS Name
			FROM dbo.NoteTag ntag (NOLOCK)
				JOIN dbo.Tag t
					ON ntag.TagID = t.TagID
			WHERE ntag.NoteID = res.TaskID
			FOR XML PATH('Tag'), ROOT('Tags')
		) AS Tags,
		--CONVERT(XML, Tags) AS Tags,
		PriorityID,
		PriorityCode,
		PriorityName,
		OriginID,
		OriginCode,
		OriginName,
		OriginDataKey,
		DateDue,
		DateCompleted,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteNotes) AS TotalRecordCount
	FROM cteNotes res
	WHERE RowNumber > @Skip AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
			
END
