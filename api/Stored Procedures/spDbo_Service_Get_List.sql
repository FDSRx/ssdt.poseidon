﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/3/2015
-- Description:	Returns a list of services.
/*

-- All Services
EXEC api.spDbo_Service_Get_List
	@ServiceKey = NULL
	
-- Single service
EXEC api.spDbo_Service_Get_List
	@ServiceKey = 'ENG'

-- Services by business
EXEC api.spDbo_Service_Get_List
	@BusinessKey = '10684'

-- Services by NABP
EXEC api.spDbo_Service_Get_List
	@BusinessKey = '2501624',
	@BusinessKeyType = 'NABP'

*/

-- SELECT * FROM dbo.Service
-- =============================================
CREATE PROCEDURE [api].[spDbo_Service_Get_List]
	@ServiceKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------

	
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL,
		@CredentialEntityID BIGINT = NULL,
		@PersonID BIGINT = NULL,
		@ServiceIDArray VARCHAR(MAX)
	
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	SET	@ServiceIDArray = dbo.fnServiceKeyTranslator(@ServiceKey, DEFAULT);
		
	SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);
	
	-- Debug
	-- SELECT @ServiceIDArray AS ServiceIDArray, @BusinessID AS BusinessID
	

	--------------------------------------------------------------------------------------------
	-- Paging.
	--------------------------------------------------------------------------------------------		
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	

	--------------------------------------------------------------------------------------------
	-- Create result set
	--------------------------------------------------------------------------------------------
	;WITH cteItems AS (
	
		SELECT
			CASE    
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY obj.DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY obj.DateCreated DESC)
				WHEN @SortField = 'Name' AND @SortDirection = 'asc'  
					THEN ROW_NUMBER() OVER (ORDER BY obj.Name ASC) 
				WHEN @SortField = 'Name' AND @SortDirection = 'desc'  
					THEN ROW_NUMBER() OVER (ORDER BY obj.Name DESC)  
				ELSE ROW_NUMBER() OVER (ORDER BY obj.Name ASC)
			END AS RowNumber,
			obj.ServiceID,
			obj.Code,
			obj.Name,
			obj.Description,
			obj.DateCreated,
			obj.DateModified,
			obj.CreatedBy,
			obj.ModifiedBy
		--SELECT *
		FROM dbo.Service obj
		WHERE ( @ServiceKey IS NULL OR obj.ServiceID IN (SELECT Value FROM dbo.fnSplit(@ServiceIDArray, ',') WHERE ISNUMERIC(Value) = 1) )	
			AND ( @BusinessKey IS NULL OR EXISTS (
				SELECT TOP 1 *
				FROM dbo.BusinessEntityService bes
				WHERE bes.BusinessEntityID = @BusinessID
					AND bes.ServiceID = obj.ServiceID
			))		
				
	)
	
	SELECT 
		ServiceID,
		Code,
		Name,
		Description,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
	FROM cteItems
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
			
END
