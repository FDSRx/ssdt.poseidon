﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/23/2014
-- Description:	Returns a list of applicable Tags.
-- SAMPLE CALL: api.spDbo_Tag_Get_List
-- SAMPLE CALL: api.spDbo_Tag_Get_List '1'
-- SAMPLE CALL: api.spDbo_Tag_Get_List 'UD'
-- SAMPLE CALL: api.spDbo_Tag_Get_List 'UD,PHRM'
-- SAMPLE CALL: api.spDbo_Tag_Get_List '1,2'
-- SAMPLE CALL: api.spDbo_Tag_Get_List '2'
-- SELECT * FROM dbo.Tag
-- =============================================
CREATE PROCEDURE [api].[spDbo_Tag_Get_List] 
	@TagTypeKey VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------
	-- Local variables
	----------------------------------------------------------
	DECLARE
		@TagTypeID VARCHAR(MAX) = dbo.fnTagTypeKeyTranslator(@TagTypeKey, DEFAULT);
		
	SELECT DISTINCT
		tags.TagID,
		tags.Name
	FROM dbo.Tag tags
		JOIN dbo.TagTagType tt
			ON tags.TagID = tt.TagID
	WHERE ( @TagTypeKey IS NULL OR ( @TagTypeID IS NOT NULL 
		AND tt.TagTypeID IN (SELECT Value FROM dbo.fnSplit(@TagTypeID, ',') WHERE ISNUMERIC(Value) = 1)) )
END
