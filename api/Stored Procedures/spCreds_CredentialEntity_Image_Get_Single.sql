﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/8/2014
-- Description:	Returns a single credential image based on its unique key.
/*

EXEC api.spCreds_CredentialEntity_Image_Get_Single
	@BusinessKey = '10684', 
	@CredentialKey = '801050',
	@ImageID = 14

*/

-- SELECT * FROM dbo.Images
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialEntity_Image_Get_Single]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@CredentialKey VARCHAR(50),
	@CredentialTypeKey VARCHAR(50) = NULL,
	@ImageID BIGINT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------
	DECLARE
		@CredentialEntityID BIGINT,
		@ApplicationID INT,
		@PersonID BIGINT,
		@BusinessID BIGINT = NULL
	
	---------------------------------------------------------------------
	-- Set local variables.
	---------------------------------------------------------------------
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialKey = @CredentialKey,
		@CredentialTypeKey = @CredentialTypeKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1
	
	---------------------------------------------------------------------
	-- Fetch credential image records.
	---------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
		EXEC api.spDbo_Image_Get_List
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessID,
			@PersonKey = @PersonID,
			@ImageKey = @ImageID
	END
		
		
		
		
		
END
