﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description:	Returns a single Medication object.
-- SAMPLE CALL: 
/*
EXEC api.spPhrm_Medication_Get_Single
	@MedicationID = 1,
	@PersonID = 135590
*/
-- SELECT * FROM phrm.Medication
-- =============================================
CREATE PROCEDURE api.spPhrm_Medication_Get_Single
	@MedicationID BIGINT,
	@PersonID BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
	
	---------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	---------------------------------------------------------------------------
	IF @MedicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to retrieve Note object. Object reference (@MedicationID) is not set to an instance of an object. ' +
			'The @MedicationID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	---------------------------------------------------------------------------
	-- Retrieve Medication object
	---------------------------------------------------------------------------
	EXEC api.spPhrm_Medication_Get_List
		@MedicationKey = @MedicationID,
		@PersonKey = @PersonID


END
