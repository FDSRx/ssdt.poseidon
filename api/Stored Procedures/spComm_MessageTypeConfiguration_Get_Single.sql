﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/15/2015
-- Description:	Returns a single MessageType object.
-- SAMPLE CALL:
/*

-- Identifier.
EXEC api.spComm_MessageTypeConfiguration_Get_Single
	@ApplicationKey = 'ENG',
	@MessageTypeKey = 1
	
-- Identifier.
EXEC api.spComm_MessageTypeConfiguration_Get_Single
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@MessageTypeKey = 'WLCME'

*/
-- SELECT * FROM comm.MessageTypeConfiguration
-- SELECT * FROM comm.MessageType
-- =============================================
CREATE PROCEDURE [api].[spComm_MessageTypeConfiguration_Get_Single]
	@MessageTypeConfigurationKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonTypeKey VARCHAR(50) = NULL,
	@MessageTypeKey VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@MessageTypeConfigurationKey=' + dbo.fnToStringOrEmpty(@MessageTypeConfigurationKey) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PersonKey=' + dbo.fnToStringOrEmpty(@PersonKey) + ';' +
		'@PersonTypeKey=' + dbo.fnToStringOrEmpty(@PersonTypeKey) + ';' +
		'@MessageTypeKey=' + dbo.fnToStringOrEmpty(@MessageTypeKey) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	

	-------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------
	DECLARE
		@MessageTypeID INT = comm.fnGetMessageTypeID(@MessageTypeKey);
	
	
	-------------------------------------------------------------------------------------
	-- Return object.
	-------------------------------------------------------------------------------------
	IF @MessageTypeID IS NOT NULL
	BEGIN
	
		EXEC api.spComm_MessageTypeConfiguration_Get_List
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessKey,
			@BusinessKeyType = @BusinessKeyType,
			@PersonKey = @PersonKey,
			@PersonTypeKey = @PersonTypeKey,
			@MessageTypeKey = @MessageTypeID,
			@Take = 1
		
	END
	
END
