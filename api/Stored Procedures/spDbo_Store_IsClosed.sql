﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 5/18/2016
-- Description:	Determines if the store is cloesd is on the provided date.
-- SAMPLE CALL:
/*

DECLARE
	@BusinessKey VARCHAR(50) = '2501624',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@Date DATETIME = '6/6/2016',
	@IsClosed BIT = NULL,
	@ResultSet VARCHAR(25) = 'SINGLE',
	@Debug BIT = 1

EXEC api.spDbo_Store_IsClosed
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@Date = @Date,
	@IsClosed = @IsClosed OUTPUT,
	@ResultSet = @ResultSet,
	@Debug = @Debug


SELECT @IsClosed AS IsClosed

*/

-- SELECT * FROM dbo.Store WHERE BusinessEntityID = 38
-- =============================================
CREATE PROCEDURE [api].[spDbo_Store_IsClosed]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@Date DATETIME,
	@IsClosed BIT = NULL OUTPUT,
	@ResultSet VARCHAR(25) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-------------------------------------------------------------------------------------------------------------
	-- Instance variables
	-------------------------------------------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000)

	
	-------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------------
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @Debug = ISNULL(@Debug, 0);
	SET @IsClosed = 0;
	SET @ResultSet = ISNULL(@ResultSet, 'SINGLE');
	
	-------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------------------------
	DECLARE
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType)
		
	
	-------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------------------------
	-- No initialization required.

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Deubber On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables.' AS ActionMethod,
			@BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType, @BusinessID AS BusinessID
	END
	
	-------------------------------------------------------------------------------------------------------------
	-- Determine if the store is open on the provided day.
	-------------------------------------------------------------------------------------------------------------
	EXEC dbo.spStore_IsClosed
		@BusinessID = @BusinessID,
		@Date = @Date,
		@IsClosed = @IsClosed OUTPUT,
		@Debug = @Debug,
		@ResultSet = @ResultSet

	

	
	
END
