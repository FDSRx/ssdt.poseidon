﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/3/2014
-- Description:	Returns a list of users that are "online" and are applicable to the businesses and applications provided.
-- SAMPLE CALL:
/*
api.spCreds_CredentialEntity_Online_Get_List
	@ApplicationKey = '8',
	@BusinessKey = '3759'
	--,@FirstName = 'dan'
	--,@SortCollection = 'DateCreated|Asc'

api.spCreds_CredentialEntity_Online_Get_List
	@ApplicationKey = '8',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 0
	
api.spCreds_CredentialEntity_Online_Get_List
	@EnforceApplicationSpecification = 0,
	@EnforceBusinessSpecification = 0
	
*/

-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntityApplication
-- SELECT * FROM dbo.Application
-- SELECT * FROM creds.Membership
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialEntity_Online_Get_List]
	@ApplicationKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	@CredentialKey VARCHAR(MAX) = NULL,
	@CredentialTypeKey VARCHAR(25) = NULL,
	@Username VARCHAR(256) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@EnforceBusinessSpecification BIT = NULL,
	@EnforceApplicationSpecification BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName) + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';' +
		'@EnforceBusinessSpecification=' + dbo.fnToStringOrEmpty(@EnforceBusinessSpecification) + ';' +
		'@EnforceApplicationSpecification=' + dbo.fnToStringOrEmpty(@EnforceApplicationSpecification) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, NULLIF(@BusinessKeyType, ''));
	SET @EnforceBusinessSpecification = ISNULL(@EnforceBusinessSpecification, 1);
	SET @EnforceApplicationSpecification = ISNULL(@EnforceApplicationSpecification, 1);
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationID INT,
		@BusinessID BIGINT = NULL,
		@TokenTypeID INT = NULL
		
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------	
	SET @TokenTypeID = dbo.fnGetTokenTypeID('LGN');
	
	-- Attempt to tranlate the BusinessKey object into a BusinessID object using the supplied
	-- business key type.  If a key type was not supplied then attempt a trial and error conversion
	-- based on token or NABP.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1


	-- Debug
	--SELECT @ApplicationID AS ApplicationID, @BusinessID AS BusinessID
	
	--------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'BusinessEntityID', 'BusinessID', 'LastName', 'FirstName', 'Username') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;


	--------------------------------------------------------------------------------------------
	-- Temporary objects.
	--------------------------------------------------------------------------------------------
	CREATE TABLE #tmpOnlineUsers (
		ApplicationID INT,
		BusinessEntityID BIGINT,
		CredentialEntityID BIGINT,
		DateExpires DATETIME,
		DateCreated DATETIME,
		DateModified DATETIME
	);
	
	--------------------------------------------------------------------------------------------
	-- Fetch current online users
	-- <Summary>
	-- Interrogate credential tokens to interrupt which users are still active or at the ver least
	-- were recently logged in.
	-- </Summary>
	--------------------------------------------------------------------------------------------
	INSERT INTO #tmpOnlineUsers (
		ApplicationID,
		BusinessEntityID,
		CredentialEntityID,
		DateExpires,
		DateCreated,
		DateModified
	)	
	SELECT
		tok.ApplicationID,
		tok.BusinessEntityID,
		tok.CredentialEntityID,
		tok.DateExpires,
		tok.DateCreated,
		tok.DateModified
	--SELECT *
	FROM creds.CredentialToken tok
		JOIN (
			SELECT
				ApplicationID,
				BusinessEntityID,
				CredentialEntityID,
				MAX(CredentialTokenID) AS CredentialTokenID
			--SELECT *
			FROM creds.CredentialToken ct
			WHERE DateExpires >= GETDATE()
				AND TokenTypeID = @TokenTypeID
				AND ( @EnforceBusinessSpecification = 0 AND ( ( @BusinessKey IS NULL OR @BusinessKey = '' ) OR ct.BusinessEntityID = @BusinessID ) 
							OR @EnforceBusinessSpecification = 1 AND ct.BusinessEntityID = @BusinessID )
				AND EXISTS (
					SELECT TOP 1 *
					--SELECT *
					FROM creds.CredentialEntityApplication cea
					WHERE cea.CredentialEntityID = ct.CredentialEntityID
						AND ( @EnforceApplicationSpecification = 0 AND ( (@ApplicationKey IS NULL OR @ApplicationKey = '' ) OR cea.ApplicationID = @ApplicationID )
							OR @EnforceApplicationSpecification = 1 AND cea.ApplicationID = @ApplicationID ) 
					)
				AND ( ( @CredentialKey IS NULL OR @CredentialKey = '' ) 
							OR ct.CredentialEntityID IN (SELECT Value FROM dbo.fnSplit(@CredentialKey, ',') WHERE ISNUMERIC(Value) = 1 ) )
			GROUP BY
				ApplicationID,
				BusinessEntityID,
				CredentialEntityID
		) virt
			ON tok.CredentialTokenID = virt.CredentialTokenID
	
	-- Debug
	--SELECT * FROM #tmpOnlineUsers
	
		
	--------------------------------------------------------------------------------------------
	-- Find the applicable users.
	--------------------------------------------------------------------------------------------
	;WITH cteUsers AS (
	
		SELECT
			CASE 
				WHEN @SortField IN ('BusinessEntityID', 'BusinessID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY ce.BusinessEntityID ASC) 
				WHEN @SortField IN ('BusinessEntityID', 'BusinessID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY ce.BusinessEntityID DESC)     
				WHEN @SortField = 'FirstName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY psn.FirstName ASC) 
				WHEN @SortField = 'FirstName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY psn.FirstName DESC) 
				WHEN @SortField = 'LastName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY psn.LastName ASC) 
				WHEN @SortField = 'LastName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY psn.LastName DESC)
				WHEN @SortField = 'Username' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY eu.Username ASC) 
				WHEN @SortField = 'Username' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY eu.Username DESC)  
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY eu.DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY eu.DateCreated DESC)   
				ELSE ROW_NUMBER() OVER (ORDER BY psn.FirstName ASC)
			END AS RowNumber,
			onusr.ApplicationID,
			biz.BusinessEntityID AS BusinessID,
			biz.BusinessToken AS BusinessToken,
			biz.Name AS BusinessName,
			ce.CredentialEntityID,
			typ.CredentialEntityTypeID,
			typ.Code AS CredentialEntityTypeCode,
			typ.Name AS CredentialEntityTypeName,
			eu.Username,
			psn.FirstName,
			psn.LastName,
			psn.AddressLine1,
			psn.AddressLine2,
			psn.City,
			psn.State,
			psn.PostalCode,
			psn.HomePhone,
			psn.MobilePhone,
			psn.PrimaryEmail,
			psn.AlternateEmail,
			CONVERT(XML, (
				SELECT
					cr.RoleID AS Id,
					r.Code,
					r.Name
				FROM acl.CredentialEntityRole cr
					JOIN acl.Roles r
						ON cr.RoleID = r.RoleID
				WHERE cr.BusinessEntityID = ce.BusinessEntityID
					AND cr.ApplicationID = @ApplicationID
					AND cr.CredentialEntityID = ce.CredentialEntityID
				FOR XML PATH('Role'), ROOT('Roles')
			)) AS RoleData,
			CONVERT(XML, (
				SELECT
					ca.ApplicationID AS Id,
					a.Code,
					a.Name
				FROM creds.CredentialEntityApplication ca
					JOIN dbo.Application a
						ON ca.ApplicationID = a.ApplicationID
				WHERE ca.CredentialEntityID = ce.CredentialEntityID
				FOR XML PATH('Application'), ROOT('Applications')
			)) AS ApplicationData,
			CONVERT(XML, (
				SELECT 
					img.ImageID AS Id,
					img.KeyName
				--SELECT *
				FROM dbo.Images img
				WHERE ApplicationID = onusr.ApplicationID
					AND BusinessID = onusr.BusinessEntityID
					AND PersonID = ISNULL(ce.PersonID, ce.CredentialEntityID)
					AND KeyName = 'ProfilePictureTiny'
				FOR XML PATH('PictureFile'), ROOT('PictureFiles')
			)) AS ImageData,
			onusr.DateCreated AS DateLastLoggedIn,
			onusr.DateModified AS DateLastActive
		-- SELECT *
		FROM creds.CredentialEntity ce
			JOIN #tmpOnlineUsers onusr
				ON ce.CredentialEntityID = onusr.CredentialEntityID
			LEFT JOIN dbo.Business biz
				ON ce.BusinessEntityID = biz.BusinessEntityID
			LEFT JOIN creds.CredentialEntityType typ	
				ON ce.CredentialEntityTypeID = typ.CredentialEntityTypeID
			LEFT JOIN dbo.vwPerson psn
				ON ce.PersonID = psn.PersonID
			LEFT JOIN creds.ExtranetUser eu
				ON ce.CredentialEntityID = eu.CredentialEntityID
		WHERE ( ( @Username = '' OR @Username IS NULL ) OR eu.Username LIKE '%' + @Username + '%' )
			AND ( ( @FirstName = '' OR @FirstName IS NULL ) OR psn.FirstName LIKE '%' + @FirstName + '%' )
			AND ( ( @LastName = '' OR @LastName IS NULL ) OR psn.LastName LIKE '%' + @LastName + '%' )
	)	
	

	
	SELECT 
		ApplicationID,
		BusinessID,
		BusinessToken,
		BusinessName,
		CredentialEntityID,
		CredentialEntityTypeID,
		CredentialEntityTypeCode,
		CredentialEntityTypeName,
		Username,
		FirstName,
		LastName,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		HomePhone,
		MobilePhone,
		PrimaryEmail,
		AlternateEmail,
		ApplicationData,
		RoleData,
		ImageData,
		DateLastLoggedIn,
		DateLastActive,
		(SELECT COUNT(*) FROM cteUsers) AS TotalRecords
	FROM cteUsers
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	

	--------------------------------------------------------------------------------------------
	-- Dispose of objects.
	--------------------------------------------------------------------------------------------
	DROP TABLE #tmpOnlineUsers;
	
	
	
		
END
