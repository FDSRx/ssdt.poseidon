﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/22/2014
-- Description:	Returns a list of AdministrationSite items.
-- SAMPLE CALL: api.spVaccine_AdministrationSite_Get_List
-- =============================================
CREATE PROCEDURE [api].spVaccine_AdministrationSite_Get_List
	@AdministrationSiteID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		ref.AdministrationSiteID,
		ref.Code,
		ref.Description,
		ref.DateCreated,
		ref.DateModified
	--SELECT *
	FROM vaccine.AdministrationSite ref
	WHERE ( @AdministrationSiteID IS NULL OR ref.AdministrationSiteID IN (SELECT Value FROM dbo.fnSplit(@AdministrationSiteID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR ref.Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	
END
