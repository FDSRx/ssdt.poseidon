﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/17/2014
-- Description:	Returns a list of images that are applicable to a patient.
/*

EXEC api.spPhrm_Patient_Image_Get_List
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@KeyName = 'ProfilePictureTiny'

*/

-- SELECT * FROM dbo.Images
-- =============================================
CREATE PROCEDURE api.spPhrm_Patient_Image_Get_List
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@ImageKey VARCHAR(MAX) = NULL,
	@KeyName VARCHAR(MAX) = NULL,
	@LanguageKey VARCHAR(25) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@BusinessID BIGINT = NULL
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');
	SET @BusinessID = (SELECT BusinessEntityID FROM phrm.Patient WHERE PatientID = @PatientID);
	
	---------------------------------------------------------------------
	-- Fetch patient image records.
	---------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
		EXEC api.spDbo_Image_Get_List
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessID,
			@PersonKey = @PersonID,
			@ImageKey = @ImageKey,
			@KeyName = @KeyName,
			@LanguageKey = @LanguageKey,
			@Skip = @Skip,
			@Take = @Take,
			@SortCollection = @SortCollection
	END
		
		
		
		
		
END
