﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/29/2015
-- Description:	Creates a new OAuth object.
-- SAMPLE CALL:

/*
DECLARE
	-- Credential
	@CredentialEntityID BIGINT = NULL OUTPUT,
	--Application(s)
	@ApplicationKey VARCHAR(MAX),
	--Business(s)
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(25) = NULL,
	-- Role(s)
	@RoleKey VARCHAR(MAX) = NULL,
	-- OAuth Client
	@Name VARCHAR(256) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Ouput
	@ClientID UNIQUEIDENTIFIER = NULL OUTPUT,
	@SecretKey UNIQUEIDENTIFIER = NULL OUTPUT,
	@OAuthClientExists BIT = NULL OUTPUT

EXEC api.spCreds_OAuthClient_DataService_Create
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@RoleKey = @RoleKey,
	@Name = @Name OUTPUT,
	@CreatedBy = @CreatedBy,
	@ClientID = @ClientID,
	@SecretKey = @SecretKey,
	@OAuthClientExists = @OAuthClientExists

SELECT @CredentialEntityID AS CredentialEntityID, @ClientID AS ClientID, @SecretKey AS SecretKey

*/

-- SELECT * FROM acl.Roles
-- SELECT * FROM dbo.Application
-- SELECT * FROM creds.CredentialEntity
-- SELECT * FROM creds.CredentialEntityType
-- SELECT * FROM creds.OAuthClient
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spCreds_OAuthClient_DataService_Create]
	-- Credential
	@CredentialEntityID BIGINT = NULL OUTPUT,
	--Application(s)
	@ApplicationKey VARCHAR(MAX),
	--Business(s)
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(25) = NULL,
	-- Role(s)
	@RoleKey VARCHAR(MAX) = NULL,
	-- OAuth Client
	@Name VARCHAR(256) = NULL,
	@Description VARCHAR(1000) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Ouput
	@ClientID UNIQUEIDENTIFIER = NULL OUTPUT,
	@SecretKey UNIQUEIDENTIFIER = NULL OUTPUT,
	@OAuthClientExists BIT = NULL OUTPUT,
	-- Result
	@ResultSet VARCHAR(25) = NULL,
	--Debug
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--------------------------------------------------------------------------------------------
	-- Result schema binding
	-- <Summary>
	-- Returns a schema of the inteded result set.  This is a workaround to assist certain
	-- frameworks in determining the correct result sets (i.e. SSIS, Entity Framework, etc.).
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF (1 = 2)
	BEGIN
		DECLARE
			@Property VARCHAR(MAX) = NULL;
			
		SELECT
		    CONVERT(VARCHAR(256), @Property) AS Name,
			CONVERT(VARCHAR(256), @Property) AS Description,
			CONVERT(UNIQUEIDENTIFIER, @Property) AS ClientID,
			CONVERT(UNIQUEIDENTIFIER, @Property) AS SecretKey,
			CONVERT(BIT, @OAuthClientExists) AS OAuthClientExists
		 
			--CONVERT(BIGINT, @Property) AS CredentialEntityID,
			--CONVERT(INT, @Property) AS ApplicationID,
			--CONVERT(BIGINT, @Property) AS BusinessID,
			--CONVERT(INT, @Property) AS RoleID,			
			--CONVERT(VARCHAR(1000), @Property) AS Description,
			--CONVERT(VARCHAR(256), @Property) AS CreatedBy
	 END
		




	-----------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------	
	-- Debug: Log request
	/*
	--Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@RoleKey=' + dbo.fnToStringOrEmpty(@RoleKey) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ClientID=' + dbo.fnToStringOrEmpty(@ClientID) + ';' +
		'@SecretKey=' + dbo.fnToStringOrEmpty(@SecretKey) + ';' 
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args	
	*/

	-----------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------------
	--SET @ClientID = NULL; 
	--SET @SecretKey = NULL;
	SET @ResultSet = ISNULL(@ResultSet, 'OUT');
	SET @CreatedBy = ISNULL(@CreatedBy, SUSER_NAME());
	SEt @OAuthClientExists = 0;
	SET @Debug = ISNULL(@Debug, 0);

	
	-----------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE
		@BusinessID BIGINT = NULL,
		@CredentialEntityTypeID INT = creds.fnGetCredentialEntityTypeID('DATASERV')

	-----------------------------------------------------------------------------------------------------------------------
	-- Set variables
	-----------------------------------------------------------------------------------------------------------------------	
	-- Translate basic system keys.
	EXEC api.spDbo_System_KeyTranslator
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@BusinessID = @BusinessID OUTPUT,
		@IsOutputOnly = 1

	-----------------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	-----------------------------------------------------------------------------------------------------------------------
	-- Applications
	IF OBJECT_ID('tempdb..#tmpApplications') IS NOT NULL
	BEGIN
		DROP TABLE #tmpApplications;
	END

	CREATE TABLE #tmpApplications (
		ApplicationID INT
	);	

	-- Roles
	IF OBJECT_ID('tempdb..#tmpRoles') IS NOT NULL
	BEGIN
		DROP TABLE #tmpRoles;
	END

	CREATE TABLE #tmpRoles (
		RoleID INT
	);		


	-----------------------------------------------------------------------------------------------------------------------
	-- Parse Application objects.
	-- <Remarks>
	-- There should be at least one application object available for assignment.
	-- </Remarks>
	-----------------------------------------------------------------------------------------------------------------------			
	INSERT INTO #tmpApplications (
		ApplicationID
	)
	SELECT DISTINCT
		dbo.fnGetApplicationID(Value)
	FROM dbo.fnSplit(@ApplicationKey, ',')
	WHERE ISNULL(Value, '') <> ''

	-- Debug
	-- SELECT * FROM #tmpApplications 


	-----------------------------------------------------------------------------------------------------------------------
	-- Parse Role objects.
	-----------------------------------------------------------------------------------------------------------------------			
	INSERT INTO #tmpRoles (
		RoleID
	)
	SELECT DISTINCT
		acl.fnGetRoleID(Value)
	FROM dbo.fnSplit(@RoleKey, ',')
	WHERE ISNULL(Value, '') <> ''

	-- Debug
	-- SELECT * FROM #tmpRoles 

	-----------------------------------------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------------
	-- BusinessID.
	IF @BusinessID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create OAuthClient object. Object reference (@BusinessID) is not set to an instance of an object. ' +
			'The parameter, @BusinessID, cannot be null. Unable to translate the @BusinessKey and @BusinessKeyType into an @BusinessID object.';
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		 );	
		  
		RETURN;
	END		

	-- Application validation.
	IF NOT EXISTS (SELECT TOP 1 * FROM #tmpApplications)
	BEGIN
		SET @ErrorMessage = 'Unable to create OAuthClient object. There were no valid applications provided to assign to the Credential.';
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		 );	
		  
		RETURN;
	END	

	-- @ClientID validation.
	IF @ClientID IS NOT NULL AND dbo.fnIsUniqueIdentifier(@ClientID) <> 1
	BEGIN
		SET @ErrorMessage = 'Unable to create OAuthClient object. The parameter, @ClientID, is not a valid GUID.';
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		 );	
		  
		RETURN;
	END

	-- @SecretKey validation.
	IF @SecretKey IS NOT NULL AND dbo.fnIsUniqueIdentifier(@SecretKey) <> 1
	BEGIN
		SET @ErrorMessage = 'Unable to create OAuthClient object. The parameter, @SecretKey, is not a valid GUID.';
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		 );	
		  
		RETURN;
	END
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Getter/Setter method.' AS ActionName,
			@CredentialEntityTypeID AS CredentialEntityTypeID, @CredentialEntityID AS CredentialEntityID, 
			@BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType, @Name AS Name,
			@BusinessID AS BusinessID, @ApplicationKey AS ApplicationKey, @RoleKey AS RoleKey
	END
					
	-----------------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------------	
	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END		
		
		-----------------------------------------------------------------------------------------------------------------------
		-- Create CredentialEntity object if one was not provided.
		-----------------------------------------------------------------------------------------------------------------------	
		IF @CredentialEntityID IS NULL
		BEGIN
			
			-- Data Review
			-- SELECT * FROM creds.CredentialEntityType
			
			EXEC creds.spCredentialEntity_Create
				@CredentialEntityTypeID = @CredentialEntityTypeID,
				@BusinessEntityID = @BusinessID,
				@CreatedBy = @CreatedBy,
				@CredentialentityID = @CredentialEntityID OUTPUT

		END

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'CredentialEntity Creator.' AS ActionName,
				@CredentialEntityTypeID AS CredentialEntityTypeID, @CredentialEntityID AS CredentialEntityID
		END

		-----------------------------------------------------------------------------------------------------------------------
		-- Determine if the CredentialEntity object has been setup as an OAuthClient object.
		-----------------------------------------------------------------------------------------------------------------------
		DECLARE
			@OAuthCredentialEntityID BIGINT,
			@ClientID_Copy UNIQUEIDENTIFIER = @ClientID,
			@SecretKey_Copy UNIQUEIDENTIFIER = @SecretKey

		EXEC creds.spOAuthClient_Exists
			@CredentialEntityID = @OAuthCredentialEntityID OUTPUT,
			@ClientID = @ClientID_Copy OUTPUT,
			@SecretKey = @SecretKey_Copy OUTPUT,
			@Exists = @OAuthClientExists OUTPUT

		-- Assign proper credential entity.
		SET @CredentialEntityID = ISNULL(@OAuthCredentialEntityID, @CredentialEntityID);

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'CredentialEntity Validate.' AS ActionName,
				@CredentialEntityTypeID AS CredentialEntityTypeID, @CredentialEntityID AS CredentialEntityID, 
				@OAuthCredentialEntityID AS OAuthCredentialEntityID, @OAuthClientExists AS OAuthClientExists, @Name AS Name,
				@ClientID AS ClientID, @ClientID_Copy AS ClientID_Copy, @SecretKey AS SecretKey, @SecretKey_Copy AS SecretKey_Copy
		END

		-----------------------------------------------------------------------------------------------------------------------
		-- Create OAuthClient object.
		-----------------------------------------------------------------------------------------------------------------------	
		IF ISNULL(@OAuthClientExists, 0) = 0
		BEGIN
			EXEC creds.spOAuthClient_Create
				@CredentialEntityID = @CredentialEntityID,
				@ClientID = @ClientID OUTPUT,
				@SecretKey = @SecretKey OUTPUT,
				@Name = @Name,
				@Description = @Description,
				@CreatedBy = @CreatedBy
		END

		-----------------------------------------------------------------------------------------------------------------------
		-- Assign Application objects to the CredentialEntity object.
		-----------------------------------------------------------------------------------------------------------------------	
		-- Data Review
		-- SELECT * FROM creds.CredentialEntityApplication

		INSERT INTO creds.CredentialEntityApplication (
			CredentialEntityID,
			ApplicationID,
			CreatedBy
		)
		SELECT DISTINCT
			@CredentialEntityID AS CredentialEntityID,
			tmp.ApplicationID,
			@CreatedBy AS CreatedBy
		FROM #tmpApplications tmp
			LEFT JOIN creds.CredentialEntityApplication app
				ON app.CredentialEntityID = @CredentialEntityID
					AND tmp.ApplicationID = app.ApplicationID
		WHERE app.CredentialEntityApplicationID IS NULL


		-----------------------------------------------------------------------------------------------------------------------
		-- Assign Role objects to the CredentialEntity object.
		-----------------------------------------------------------------------------------------------------------------------	
		-- Data Review
		-- SELECT * FROM acl.CredentialEntityRole

		INSERT INTO acl.CredentialEntityRole (
			CredentialEntityID,
			ApplicationID,
			BusinessEntityID,
			RoleID,
			CreatedBy
		)
		SELECT DISTINCT
			@CredentialEntityID AS CredentialEntityID,
			tmpapp.ApplicationID,
			@BusinessID AS BusinessID,
			tmprle.RoleID,
			@CreatedBy AS CreatedBy
		FROM #tmpApplications tmpapp
			CROSS APPLY #tmpRoles tmprle
			LEFT JOIN acl.CredentialEntityRole trgt
				ON trgt.CredentialEntityID = @CredentialEntityID
					AND trgt.ApplicationID = tmpapp.ApplicationID
					AND trgt.BusinessEntityID = @BusinessID
					AND trgt.RoleID = tmprle.RoleID
		WHERE trgt.CredentialEntityRoleID IS NULL


	EOF:
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END		
	END TRY
	BEGIN CATCH	

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
				
		EXECUTE [dbo].spLogError;
	
		SET @ErrorMessage = 'Unable to create OAuthClient object. Exeception: ' + ERROR_MESSAGE();
				
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
	
	END CATCH



	-----------------------------------------------------------------------------------------------------------------------
	-- Render results.
	-----------------------------------------------------------------------------------------------------------------------	
	IF ISNULL(@ResultSet, 'OUT') IN ('SINGLE', 'FULL')
	BEGIN
		SELECT
			@Name AS Name,
			@Description AS Description,
			@ClientID AS ClientID,
			@SecretKey AS SecretKey,
			@OAuthClientExists AS OAuthClientExists
	END
	ELSE IF ISNULL(@ResultSet, 'OUT') = 'XML'
	BEGIN
		SELECT CONVERT(XML,
			'<DataServiceResult>' +
				dbo.fnXmlWriteElementString('Name', @Name) +
				dbo.fnXmlWriteElementString('Description', @Description) +
				dbo.fnXmlWriteElementString('ClientID', @ClientID) +
				dbo.fnXmlWriteElementString('SecrectKey', @SecretKey) +
				dbo.fnXmlWriteElementString('OAuthClientExists', @OAuthClientExists) +
			'</DataServiceResult>'
		) AS Result;
	END
	

	-----------------------------------------------------------------------------------------------------------------------
	-- Dispose of Resources.
	-----------------------------------------------------------------------------------------------------------------------	
	DROP TABLE #tmpApplications;
	DROP TABLE #tmpRoles;
	
	
END
