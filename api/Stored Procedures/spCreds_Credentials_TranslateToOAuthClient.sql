﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/26/2015
-- Description:	Translate Credentials to OAuthClient object.
-- SAMPLE CALL:

/*

DECLARE
	@ApplicationKey VARCHAR(50) = 'ENG',
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@CredentialKey VARCHAR(50) = '96E1F568-287E-4A61-B48B-B3D8231B0A6E',
	@IsValid BIT = NULL,
	@ClientID NVARCHAR(256) = NULL,
	@SecretKey NVARCHAR(256) = NULL,
	@CredentialEntityID BIGINT = NULL

EXEC api.spCreds_Credentials_TranslateToOAuthClient
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@CredentialKey = @CredentialKey,
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@ClientID = @ClientID OUTPUT,
	@SecretKey = @SecretKey OUTPUT,
	@IsValid = @IsValid OUTPUT

*/

-- SELECT * FROM creds.CredentialEntityType
-- SELECT * FROM creds.CredentialToken ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE api.spCreds_Credentials_TranslateToOAuthClient
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@CredentialKey VARCHAR(50) = NULL, -- i.e. token, guid, etc.
	@CredentialEntityTypeKey VARCHAR(50) = NULL,
	@UserName VARCHAR(256) = NULL,
	@Password VARCHAR(50) = NULL,
	@ClientID NVARCHAR(256) = NULL OUTPUT,
	@SecretKey NVARCHAR(256) = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@IsValid BIT = NULL OUTPUT,
	@IsOutputOnly BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-------------------------------------------------------------------------------------------------------
	-- Log request.
	-------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@AppilicationKey=' + dbo.fnToStringOrEmpty(@AppilicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@CredentialKey=' + dbo.fnToStringOrEmpty(@CredentialKey) + ';' +
		'@UserName=' + dbo.fnToStringOrEmpty(@UserName) + ';' +
		'@Password=' + dbo.fnToStringOrEmpty(@Password) + ';' +
		'@ClientID=' + dbo.fnToStringOrEmpty(@ClientID) + ';' +
		'@SecretKey=' + dbo.fnToStringOrEmpty(@SecretKey) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@IsValid=' + dbo.fnToStringOrEmpty(@IsValid) + ';' +
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	SET @CredentialEntityID = NULL;
	SET @ClientID = NULL;
	SET @SecretKey = NULL;
	SET @IsValid = 0;

	-------------------------------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------------------------------	
	DECLARE 
		@BusinessID BIGINT = NULL,
		@ApplicationID INT = NULL,
		@CredentialEntityTypeID INT

	-------------------------------------------------------------------------------------------------------
	-- Set variables
	-------------------------------------------------------------------------------------------------------
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialTypeKey = @CredentialEntityTypeKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityTypeID = @CredentialEntityTypeID OUTPUT,
		@IsOutputOnly = 1

	-------------------------------------------------------------------------------------------------------
	-- Validate the credentials.
	-- <Summary>
	-- Interrogate the arguments and determine how to retrieve the OAuthClient object.
	-- If a CredentialKey arg was provided, then assume that is the only method for obtaining
	-- the OAuthClient object. Otherwise, use the UserName and Password args to find the object.
	-- </Summary>
	-------------------------------------------------------------------------------------------------------
	IF @CredentialKey IS NOT NULL
	BEGIN

		EXEC creds.spCredentialToken_TranslateToOAuthClient
			@Token = @CredentialKey,
			@ValidateTokenExpiration = 1,
			@CredentialEntityID = @CredentialEntityID OUTPUT,
			@ClientID = @ClientID OUTPUT,
			@SecretKey = @SecretKey OUTPUT,
			@IsValid = @IsValid OUTPUT

	END
	ELSE
	BEGIN
		-- PLACE HOLDER: Will eventually be replaced with UserName and Password arg lookup (if necessary).
		SET @IsValid = 0;
	END
	

	-------------------------------------------------------------------------------------------------------
	-- Set variables
	-------------------------------------------------------------------------------------------------------
	IF ISNULL(@IsOutputOnly, 0) = 0
	BEGIN

		SELECT
			@IsValid AS IsValid,
			@ApplicationID AS ApplicationID,
			@BusinessID AS BusinessID,
			@CredentialEntityID AS CredentialEntityID,
			@ClientID AS ClientID,
			@SecretKey AS SecretKey

	END

END
