﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Gets the Note object.
-- SAMPLE CALL:
/*

DECLARE
	@NoteID BIGINT = 451,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT = 2,
	@BusinessEntityID BIGINT = 135589,
	@Debug BIT = 1

EXEC api.spDbo_Note_Get_Single
	@NoteID = @NoteID,
	@ScopeID = @ScopeID,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@BusinessEntityID = @BusinessEntityID,
	@Debug = @Debug
	
*/
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType

-- SELECT * FROM dbo.ErrorLog
-- SELECT * FROM dbo.InformationLog
-- =============================================
CREATE PROCEDURE [api].[spDbo_Note_Get_Single]
	@NoteID BIGINT,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT = NULL,
	@BusinessEntityID BIGINT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
	--DECLARE
	--	@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
	--	@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType)
		
	------------------------------------------------------------------------------------------------------------
	-- Set variables
	------------------------------------------------------------------------------------------------------------	
	
	-- debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod, 
			@NoteID AS NoteID, @ScopeID AS ScopeID, @BusinessEntityID AS BusinessEntityID,
			@Args AS Arguments
	END
	
	------------------------------------------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	------------------------------------------------------------------------------------------------------------
	IF @NoteID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to retrieve Note object. Object reference (@NoteID) is not set to an instance of an object. ' +
			'The @NoteID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	
	------------------------------------------------------------------------------------------------------------
	-- Retrieve Note object
	------------------------------------------------------------------------------------------------------------
	EXEC api.spDbo_Note_Get_List
		@NoteKey = @NoteID,
		@ScopeKey = @ScopeID,
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@BusinessEntityKey = @BusinessEntityID
	
			
END
