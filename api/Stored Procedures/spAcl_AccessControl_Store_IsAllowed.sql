﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/13/2014
-- Description:	Indicates if the request is allowed to access the control.
-- SAMPLE CALL:
/*
-- Denied request
EXEC api.spAcl_AccessControl_Store_IsAllowed
	@ApplicationCode = 'MPC',
	@NABP = '7878799',
	@AccessInspectionTypeCode = 'HOST',
	@AccessFilterTypeCode = 'IPADDR',
	@Filter = '192.168.1.7',
	@AccessDirectiveCode = 'A'

-- Denied request (inferred data elements)
EXEC api.spAcl_AccessControl_Store_IsAllowed
	@ApplicationCode = 'MPC',
	@NABP = '7878799',
	@Filter = '192.168.1.7'

-- Allowed request (inferred data elements)
EXEC api.spAcl_AccessControl_Store_IsAllowed
	@ApplicationCode = 'MPC',
	@NABP = '7878799',
	@Filter = '192.168.1.6'
	
*/

-- SELECT * FROM acl.vwStoreAccessControl
-- SELECT * FROM dbo.RequestLog ORDER BY RequestLogID DESC
-- SELECT * FROM dbo.RequestType
-- =============================================
CREATE PROCEDURE [api].[spAcl_AccessControl_Store_IsAllowed]
	@ApplicationCode VARCHAR(25),
	@NABP VARCHAR(MAX),
	@AccessInspectionTypeCode VARCHAR(25) = NULL,
	@AccessFilterTypeCode VARCHAR(25) = NULL,
	@Filter VARCHAR(256),
	@AccessDirectiveCode VARCHAR(25) = NULL,
	@RequestedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--------------------------------------------------------------
	-- Log request
	-- <Summary>
	-- Records the original access request for later inspecetion.
	-- </Summary>
	--------------------------------------------------------------
	SET @RequestedBy = COALESCE(@RequestedBy, SUSER_SNAME());
	
	DECLARE
		@RequestTypeID INT = dbo.fnGetRequestTypeID('CNTRLACC'),
		@SourceName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@Arguments VARCHAR(MAX) = 
			'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + '; ' +
			'@NABP=' + dbo.fnToStringOrEmpty(@NABP) + '; ' +
			'@AccessInspectionTypeCode=' + dbo.fnToStringOrEmpty(@AccessInspectionTypeCode) + '; ' +
			'@AccessFilterTypeCode=' + dbo.fnToStringOrEmpty(@AccessFilterTypeCode) + '; ' +
			'@Filter=' + dbo.fnToStringOrEmpty(@Filter) + '; ' +
			'@AccessDirectiveCode=' + dbo.fnToStringOrEmpty(@AccessDirectiveCode) + '; ';
	
	EXEC dbo.spRequestLog_Create
		@RequestTypeID = @RequestTypeID,
		@SourceName = @SourceName,
		@ApplicationKey = @ApplicationCode,
		@BusinessKey = @NABP,
		@RequesterKey = @RequestedBy,
		@CreatedBy = @RequestedBy,
		@Arguments = @Arguments
	
	
	--------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------
	-- Default to host inspection (i.e. Ip address, domain name, etc.)
	SET @AccessInspectionTypeCode = ISNULL(@AccessInspectionTypeCode, 'HOST')
	
	-- Default to Ip inspection
	SET @AccessFilterTypeCode = ISNULL(@AccessFilterTypeCode, 'IPADDR');
	
	-- Default to deny for the type of access.
	SET @AccessDirectiveCode = ISNULL(@AccessDirectiveCode, 'A');
	
	--------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------
	DECLARE
		@AccessInspectionTypeID INT,
		@AccessFilterTypeID INT,
		@AccessDirectiveID INT,
		@ErrorMessage VARCHAR(4000),
		@IsSuccess BIT = 1,
		@IsAllowed BIT = 1,
		@Message VARCHAR(4000) = 'Your provider has granted you access to the site.'	
	

	--------------------------------------------------------------
	-- Resolve access properties
	-- <Summary>
	-- Translates the access code type properties into their known
	-- surrogate Ids.
	-- </Summary>
	--------------------------------------------------------------	
	SET @AccessInspectionTypeID = acl.fnGetAccessInspectionTypeID(@AccessInspectionTypeCode);
	SET @AccessFilterTypeID = acl.fnGetAccessFilterTypeID(@AccessFilterTypeCode);
	SET @AccessDirectiveID = acl.fnGetAccessDirectiveID(@AccessDirectiveCode);
	
	--------------------------------------------------------------
	-- Temporary resources
	--------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpStoreList') IS NOT NULL
	BEGIN
		DROP TABLE #tmpStoreList;
	END
	
	CREATE TABLE #tmpStoreList (
		Idx INT IDENTITY(1,1),
		NABP VARCHAR(25)
	);
	
	IF OBJECT_ID('tempdb..#tmpResults') IS NOT NULL
	BEGIN
		DROP TABLE #tmpResults;
	END
	
	CREATE TABLE #tmpResults (
		Idx INT IDENTITY(1,1),
		StoreID BIGINT,
		NABP VARCHAR(25),
		AccessInspectionTypeCode VARCHAR(25),
		AccessFilterTypeCode VARCHAR(25),
		AccessDirectiveCode VARCHAR(25)
	);
	
	BEGIN TRY
	
		--------------------------------------------------------------
		-- Argument null validation
		-- <Summary>
		-- Validate required fields to ensure that null was not provided
		-- </Summary>
		--------------------------------------------------------------
		IF dbo.fnIsNullOrWhiteSpace(@ApplicationCode) = 1
		BEGIN
			SET @ErrorMessage = 'Unable to inspect filter. Exception: Object reference (@ApplicationCode) is not set to an instance ' +
				'of an object.'
				
			RAISERROR (
				@ErrorMessage, -- Message text.
				16, -- Severity.
				1 -- State.
			);
			
			RETURN;			
		END
		
		IF dbo.fnIsNullOrWhiteSpace(@NABP) = 1
		BEGIN
			SET @ErrorMessage = 'Unable to inspect filter. Exception: Object reference (@NABP) is not set to an instance ' +
				'of an object.'
				
			RAISERROR (
				@ErrorMessage, -- Message text.
				16, -- Severity.
				1 -- State.
			);
			
			RETURN;			
		END
	
		--------------------------------------------------------------
		-- Parse store identifiers.
		--------------------------------------------------------------
		INSERT INTO #tmpStoreList (
			NABP
		)
		SELECT 
			Value
		FROM dbo.fnSplit(@NABP, ',')
		WHERE ISNULL(Value, '') <> ''
		

		--------------------------------------------------------------
		-- Validate the store has any filters.
		-- <Summary>
		-- If the store does not have any specified filters, then
		-- it is allowing acccess from any location.  If one or more
		-- filters are specified then only those locations are allowed
		-- access.
		-- </Summary>
		--------------------------------------------------------------		
		IF NOT EXISTS (
			SELECT TOP 1 * 
			FROM acl.vwStoreAccessControl ac
				JOIN #tmpStoreList lst
					ON ac.NABP = lst.NABP
		)
		BEGIN		
			-- No data filtering files available.  Skip to the end.
			GOTO EOF;		
		END
		
		
		--------------------------------------------------------------
		-- Fetch results.
		--------------------------------------------------------------
		INSERT INTO #tmpResults (
			StoreID,
			NABP,
			AccessInspectionTypeCode,
			AccessFilterTypeCode,
			AccessDirectiveCode
		)
		SELECT
			ac.StoreID,
			ac.NABP,
			ac.AccessInspectionTypeCode,
			ac.AccessFilterTypeCode,
			ac.AccessDirectiveCode
		FROM acl.vwStoreAccessControl ac
			JOIN #tmpStoreList lst
				ON ac.NABP = lst.NABP
		WHERE ac.Filter = dbo.fnTrim(@Filter)
		

		-- Debug
		--SELECT * FROM #tmpResults
		
		--------------------------------------------------------------
		-- Interogate results
		-- <Summary>
		-- If a denied directive is found in the returned list then
		-- we need to deny all supplied stores their access.
		-- </Summary>
		--------------------------------------------------------------
		IF NOT EXISTS(SELECT TOP 1 * FROM #tmpResults WHERE AccessDirectiveCode = @AccessDirectiveCode)
		BEGIN
			SET @IsAllowed = 0;
			SET @Message = 'Your provider has denied you access to the site.  You were attempting access from ' + dbo.fnToStringOrEmpty(@Filter) + '. ' +
				'Reasons for restricted access could be due to ' +
				'your IP address, domain name, etc.  Please contact your site administrator to inquire more infomration ' +
				'regarding site access.'
		END
		
		--------------------------------------------------------------
		-- Send response
		-- <Summary>
		-- Create and send an XML response back to the caller.
		-- </Summary>
		--------------------------------------------------------------
		/*	
		DECLARE
			@Response XML
		
		SET @Response = 
			'<AccessResponse>' +
				dbo.fnXmlWriteElementString('IsAllowed', @IsAllowed) +
				dbo.fnXmlWriteElementString('Message', @Message) + 
			'</AccessResponse>'
		*/
	
	EOF:
	END TRY
	BEGIN CATCH
	
		EXEC spLogError;	
	
		SET @IsSuccess = 0;
		SET @Message = @ErrorMessage;
	
	END CATCH

	--------------------------------------------------------------
	-- Return results
	--------------------------------------------------------------		
	SELECT 
		@IsSuccess AS IsSuccess,
		@IsAllowed AS IsAllowed,
		@Message AS Message


	--------------------------------------------------------------
	-- Release resources
	--------------------------------------------------------------
	DROP TABLE #tmpStoreList;	
	DROP TABLE #tmpResults;
END
