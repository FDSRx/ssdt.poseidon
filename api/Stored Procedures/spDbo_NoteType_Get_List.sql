﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/21/2014
-- Description:	Returns a list of note types.
-- SAMPLE CALL: api.spDbo_NoteType_Get_List
-- =============================================
CREATE PROCEDURE [api].[spDbo_NoteType_Get_List]
	@NoteTypeID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		NoteTypeID,
		Code,
		Name,
		Description,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	FROM dbo.NoteType
	WHERE ( @NoteTypeID IS NULL OR NoteTypeID IN (SELECT Value FROM dbo.fnSplit(@NoteTypeID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	
END
