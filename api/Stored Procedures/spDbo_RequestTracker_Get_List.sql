﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/4/2016
-- Description:	Returns a list of requests that have been made.
-- SAMPLE CALL:
/*

DECLARE
	@ApplicationKey VARCHAR(50) = NULL,
	@RequestTypeKey VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL

EXEC api.spDbo_RequestTracker_Get_List
	@ApplicationKey = @ApplicationKey,
	@RequestTypeKey = @RequestTypeKey,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection

*/

-- SELECT * FROM dbo.RequestTracker
-- SELECT * FROM dbo.Application
-- SELECT * FROM dbo.RequestType
-- =============================================
CREATE PROCEDURE [api].[spDbo_RequestTracker_Get_List]
	@ApplicationKey VARCHAR(50) = NULL,
	@RequestTypeKey VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------------------------
	-- Result schema binding
	-- <Summary>
	-- Returns a schema of the intended result set.  This is a workaround to assist certain
	-- frameworks in determining the correct result sets (i.e. SSIS, Entity Framework, etc.).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------------------------
	IF (1 = 2)
	BEGIN
		DECLARE
			@Property VARCHAR(MAX) = NULL;

		SELECT 
			CONVERT(BIGINT, @Property) AS RequestTrackerID,
			CONVERT(VARCHAR(256), @Property) AS SourceName,
			CONVERT(INT, @Property) AS RequestTypeID,
			CONVERT(VARCHAR(50), @Property) AS RequestTypeCode,
			CONVERT(VARCHAR(50), @Property) AS RequestTypeName,
			CONVERT(VARCHAR(50), @Property) AS ApplicationKey,
			CONVERT(INT, @Property) AS ApplicationID,
			CONVERT(VARCHAR(50), @Property) AS ApplicationCode,
			CONVERT(VARCHAR(50), @Property) AS ApplicationName,
			CONVERT(VARCHAR(50), @Property) AS BusinessKey,
			CONVERT(BIGINT, @Property) AS BusinessEntityID,
			CONVERT(VARCHAR(50), @Property) AS Nabp,
			CONVERT(VARCHAR(256), @Property) AS BusinessName,
			CONVERT(VARCHAR(256), @Property) AS RequesterKey,
			CONVERT(BIGINT, @Property) AS RequesterID,
			CONVERT(INT, @Property) AS RequestCount,
			CONVERT(INT, @Property) AS RequestsRemaining,
			CONVERT(INT, @Property) AS MaxRequestsAllowed,
			CONVERT(BIT, @Property) AS IsLocked,
			CONVERT(DATETIME, @Property) AS DateExpires,
			CONVERT(VARCHAR(MAX), @Property) AS RequstData,
			CONVERT(VARCHAR(MAX), @Property) AS Arguments,
			CONVERT(UNIQUEIDENTIFIER, @Property) AS rowguid,
			CONVERT(DATETIME, @Property) AS DateCreated,
			CONVERT(DATETIME, @Property) AS DateModified,
			CONVERT(VARCHAR(256), @Property) AS CreatedBy,
			CONVERT(VARCHAR(256), @Property) AS ModifiedBy,
			CONVERT(BIGINT, @Property) AS TotalRecords
	END

	--------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------------------------
	DECLARE @ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@CommunicationMethodKey) + ';' +
		'@RequestTypeKey=' + dbo.fnToStringOrEmpty(@CommunicationMethodKey) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	--------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationKeyArray VARCHAR(MAX) = dbo.fnApplicationKeyTranslator(@ApplicationKey, DEFAULT)
	
	
	--------------------------------------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------------------------------------			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
		
	--------------------------------------------------------------------------------------------------------------------------
	-- Fetch results.
	--------------------------------------------------------------------------------------------------------------------------
	;WITH cteItems AS (
		SELECT
			ROW_NUMBER() OVER(ORDER BY
				CASE WHEN @SortField IN ('BusinessEntityID', 'BusinessID', 'StoreID') AND @SortDirection = 'asc'  THEN sto.BusinessEntityID END ASC, 
				CASE WHEN @SortField IN ('BusinessEntityID', 'BusinessID', 'StoreID') AND @SortDirection = 'desc'  THEN sto.BusinessEntityID END DESC,   
				CASE WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN sto.Name END ASC,
				CASE WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN sto.Name END DESC,  
				CASE WHEN @SortField IS NULL OR @SortField = '' THEN req.RequestTrackerID ELSE req.RequestTrackerID END ASC
			) AS RowNumber,
			req.RequestTrackerID,
			req.SourceName,
			req.RequestTypeID,
			typ.Code AS RequestTypeCode,
			typ.Name AS RequestTypeName,
			req.ApplicationKey,
			req.ApplicationID,
			app.Code AS ApplicationCode,
			app.Name AS ApplicationName,
			req.BusinessKey,
			req.BusinessEntityID,
			sto.Nabp AS Nabp,
			sto.Name AS BusinessName,
			req.RequesterKey,
			req.RequesterID,
			req.RequestCount,
			req.RequestsRemaining,
			req.MaxRequestsAllowed,
			req.IsLocked,
			req.DateExpires,
			req.RequestData,
			req.Arguments,
			req.rowguid,
			req.DateCreated,
			req.DateModified,
			req.CreatedBy,
			req.ModifiedBy
		--SELECT *
		FROM dbo.RequestTracker req
			LEFT JOIN dbo.RequestType typ
				ON typ.RequestTypeID = req.RequestTypeID
			LEFT JOIN dbo.Application app
				ON req.ApplicationID = app.ApplicationID
			LEFT JOIN dbo.Store sto
				ON req.BusinessEntityID = sto.BusinessEntityID
		WHERE (@ApplicationKey IS NULL OR ( @ApplicationKey IS NOT NULL AND req.ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationKeyArray, ',') WHERE ISNUMERIC(Value) = 1)))
	)

	SELECT
		RequestTrackerID,
		SourceName,
		RequestTypeID,
		RequestTypeCode,
		RequestTypeName,
		ApplicationKey,
		ApplicationID,
		ApplicationCode,
		ApplicationName,
		BusinessKey,
		BusinessEntityID,
		Nabp,
		BusinessName,
		RequesterKey,
		RequesterID,
		RequestCount,
		RequestsRemaining,
		MaxRequestsAllowed,
		IsLocked,
		DateExpires,
		RequestData,
		Arguments,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
	FROM cteItems
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.

END
