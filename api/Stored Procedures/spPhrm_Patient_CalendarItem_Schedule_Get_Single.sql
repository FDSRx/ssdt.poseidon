﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/17/2014
-- Description:	Gets the specified CalendarItem.
-- Change log:
-- 6/17/2016 - dhughes - Modified the procedure to accept the Application object parameters.

-- SAMPLE CALL;
/*

DECLARE
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = '7871787',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@PatientKey VARCHAR(50) = '21656',
	@PatientKeyType VARCHAR(50) = 'SRC',
	@NoteID BIGINT = 104,
	@Debug BIT = 1

EXEC api.spPhrm_Patient_CalendarItem_Schedule_Get_Single
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@NoteID = @NoteID,
	@Debug = @Debug

 */

-- SELECT * FROM phrm.fnGetPatientInformation(2) 
-- SELECT * FROM dbo.vwTask
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.CalendarItem
-- SELECT * FROM dbo.NoteSchedule

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_CalendarItem_Schedule_Get_Single]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@NoteID BIGINT = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey)  + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType)  + ';' +
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;



	-----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @PatientKeyType = ISNULL(@PatientKeyType, 'SRC');

	-----------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT,
		@PatientID BIGINT,
		@PersonID BIGINT,
		@ScopeID INT = dbo.fnGetScopeID('INTRN') -- only review internal notes
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Set local variables
	-----------------------------------------------------------------------------------------------------------------------------
	SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);

	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, @BusinessKeyType, @PatientKey, @PatientKeyType);
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');	

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Deubber On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeType,
			@BusinessID AS BusinessID, @NoteID AS NoteID
	END
	

	
	---------------------------------------------------------------------
	-- Return patient Task object.
	---------------------------------------------------------------------
	EXEC api.spDbo_CalendarItem_Schedule_Get_Single
		@NoteID = @NoteID,
		@ScopeID = @ScopeID,
		@BusinessEntityID = @PersonID,
		@Debug = @Debug
			
END
