﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/12/2014
-- Description:	Creates a single or collection of CredentialEntityBusinessEntity objects.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityBusinessEntityIDArray VARCHAR(MAX) = NULL
	
EXEC api.spCreds_CredentialEntityBusinessEntity_Chain_MergeRange 
	@CredentialKey = 959790,
	@ApplicationKey = 8,
	@BusinessKey = 3759,	
	@CreatedBy = 'dhughes',
	@CredentialEntityBusinessEntityIDArray = @CredentialEntityBusinessEntityIDArray OUTPUT

SELECT @CredentialEntityBusinessEntityIDArray AS CredentialEntityBusinessEntityIDArray

*/

-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntityBusinessEntity
-- SELECT * FROM dbo.Chain
-- SELECT * FROM dbo.BusinessEntityType
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialEntityBusinessEntity_Chain_MergeRange] 
	@CredentialKey VARCHAR(50),
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	@DeleteUnspecified BIT = 0,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@CredentialEntityBusinessEntityIDArray VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @CredentialEntityBusinessEntityIDArray = NULL;
	
	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE
		@CredentialEntityID BIGINT,
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessEntityTypeID INT = dbo.fnGetBusinessEntityTypeID('CHN')
	

	--------------------------------------------------------------------------------------------
	-- Interrogate credential key and parse accordingly.
	--------------------------------------------------------------------------------------------
	IF dbo.fnIsUniqueIdentifier(@CredentialKey) = 1
	BEGIN
		------------------------------------------------
		-- Validate token
		------------------------------------------------
		EXEC creds.spCredentialToken_Update 
			@Token = @CredentialKey OUTPUT,
			@CredentialEntityID = @CredentialEntityID OUTPUT
	END
	ELSE
	BEGIN
		-- Determine if id is an actual credential id.
		SET @CredentialEntityID = creds.fnGetCredentialEntityID(@CredentialKey);
	END

	--------------------------------------------------------------------------------------------
	-- Verify the roles that were passed are within the parameters of the provided arguments.
	--------------------------------------------------------------------------------------------	
	DECLARE @VerifiedChainArray VARCHAR(MAX) = (
		SELECT CONVERT(VARCHAR, BusinessEntityID) + ','
		--SELECT *
		FROM dbo.Chain
		WHERE BusinessEntityID IN (SELECT Value FROM dbo.fnSplit(@BusinessKey, ',') WHERE ISNUMERIC(Value) = 1)
		FOR XML PATH('')
	);
	
	--------------------------------------------------------------------------------------------
	-- Create new CredentialEntityBusinessEntity object(s).
	--------------------------------------------------------------------------------------------
	EXEC creds.spCredentialEntityBusinessEntity_MergeRange
		@CredentialEntityID = @CredentialEntityID,
		@ApplicationID = @ApplicationID,
		@BusinessEntityID = @VerifiedChainArray,
		@BusinessEntityTypeID = @BusinessEntityTypeID,
		@DeleteUnspecified = @DeleteUnspecified,
		@CreatedBy = @CreatedBy,
		@ModifiedBy = @ModifiedBy,
		@CredentialEntityBusinessEntityIDs = @CredentialEntityBusinessEntityIDArray OUTPUT
		
END
