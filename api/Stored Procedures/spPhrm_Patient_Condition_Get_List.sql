﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/10/2014
-- Description:	Returns a list of patient conditions.
-- SAMPLE CALL:
/*
EXEC api.spPhrm_Patient_Condition_Get_List
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@Skip = NULL,
	@Take = NULL
*/
-- SELECT * FROM phrm.Condition
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Condition_Get_List]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@ConditionKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of @ConditionKey object keys.
	@ConditionTypeKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of ConditionTye object keys.
	@CodeQualifierKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of CodeQualifier object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of OriginKey object keys.
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@ScopeID INT = dbo.fnGetScopeID('INTRN') -- only review internal notes
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');
	

	---------------------------------------------------------------------
	-- Return patient Condition objects.
	---------------------------------------------------------------------	
	EXEC api.spPhrm_Condition_Get_List
		@BusinessEntityKey = @PersonID,
		@ConditionTypeKey = @ConditionTypeKey,
		@CodeQualifierKey = @CodeQualifierKey,
		@ConditionKey = @ConditionKey,
		@OriginKey = @OriginKey,
		@Skip = @Skip,
		@Take = @Take,
		@SortCollection = @SortCollection
		
		
END
