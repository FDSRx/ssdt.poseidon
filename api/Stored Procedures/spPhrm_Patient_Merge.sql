﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/6/2014
-- Description:	Api wrapper to merge a patient record.
-- SAMPLE CALL:
/*

DECLARE
	--Business
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@StoreID INT = NULL,
	@Nabp VARCHAR(25) = NULL,
	@BusinessEntityID INT = NULL,
	--Patient
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@SourcePatientKey VARCHAR(50),
	--Person
	@PersonID INT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL, -- Required to try and create a unique person
	@LastName VARCHAR(75) = NULL, -- Required to try and create a unique person
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL, -- Required to try and create a unique person
	@GenderKey VARCHAR(25) = NULL,
	@GenderID INT  = NULL, -- Required to try and create a unique person
	@LanguageKey VARCHAR(25) = NULL,
	@LanguageID INT = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL, -- Required to try and create a unique person
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	-- MPC Pharmacy Service
	@Service_Mpc_ConnectionStatusKey VARCHAR(50) = NULL,
	@Service_Mpc_ConnectionStatusID INT = NULL,
	@Service_Mpc_ConnectionStatusCode VARCHAR(25) = NULL,
	@Service_Mpc_AccountKey VARCHAR(256) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	-- Error
	@ErrorLogID INT = NULL OUTPUT,
	-- User output 
	@PatientID INT = NULL OUTPUT,
	@PatientExists BIT = 0 OUTPUT,
	@Debug BIT = NULL

EXEC api.spPhrm_Patient_Merge
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@StoreID = @StoreID,
	@Nabp = @Nabp,
	@BusinessEntityID = @BusinessEntityID,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@SourcePatientKey = @SourcePatientKey,
	@PersonID = @PersonID,
	@Title = @Title,
	@FirstName = @FirstName,
	@LastName = @LastName,
	@MiddleName = @MiddleName,
	@Suffix = @Suffix,
	@BirthDate = @BirthDate,
	@GenderKey = @GenderKey,
	@GenderID = @GenderID,
	@LanguageKey = @LanguageKey,
	@LanguageID = @LanguageID,
	@AddressLine1 = @AddressLine1,
	@AddressLine2 = @AddressLine2,
	@City = @City,
	@State = @State,
	@PostalCode = @PostalCode,
	@PhoneNumber_Home = @PhoneNumber_Home,
	@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
	@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
	@PhoneNumber_Home_IsPreferred = @PhoneNumber_Home_IsPreferred,
	@PhoneNumber_Mobile = @PhoneNumber_Mobile,
	@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
	@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
	@PhoneNumber_Mobile_IsPreferred = @PhoneNumber_Mobile_IsPreferred,
	@EmailAddress_Primary = @EmailAddress_Primary,
	@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
	@EmailAddress_Primary_IsPreferred = @EmailAddress_Primary_IsPreferred,
	@EmailAddress_Alternate = @EmailAddress_Alternate,
	@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
	@EmailAddress_Alternate_IsPreferred = @EmailAddress_Alternate_IsPreferred,
	@Service_Mpc_ConnectionStatusKey = @Service_Mpc_ConnectionStatusKey,
	@Service_Mpc_ConnectionStatusID = @Service_Mpc_ConnectionStatusID,
	@Service_Mpc_ConnectionStatusCode = @Service_Mpc_ConnectionStatusCode,
	@Service_Mpc_AccountKey = @Service_Mpc_AccountKey,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@ErrorLogID = @ErrorLogID,
	@PatientID = @PatientID,
	@PatientExists = @PatientExists,
	@Debug = @Debug


*/

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Merge]
	--Business
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@StoreID INT = NULL,
	@Nabp VARCHAR(25) = NULL,
	@BusinessEntityID INT = NULL,
	--Patient
	@PatientKey VARCHAR(50) = NULL,
	@PatientKeyType VARCHAR(50) = NULL,
	@SourcePatientKey VARCHAR(50),
	--Person
	@PersonID INT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL, -- Required to try and create a unique person
	@LastName VARCHAR(75) = NULL, -- Required to try and create a unique person
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL, -- Required to try and create a unique person
	@GenderKey VARCHAR(25) = NULL,
	@GenderID INT  = NULL, -- Required to try and create a unique person
	@LanguageKey VARCHAR(25) = NULL,
	@LanguageID INT = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL, -- Required to try and create a unique person
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	-- MPC Pharmacy Service
	@Service_Mpc_ConnectionStatusKey VARCHAR(50) = NULL,
	@Service_Mpc_ConnectionStatusID INT = NULL,
	@Service_Mpc_ConnectionStatusCode VARCHAR(25) = NULL,
	@Service_Mpc_AccountKey VARCHAR(256) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	-- Error
	@ErrorLogID INT = NULL OUTPUT,
	-- User output 
	@PatientID INT = NULL OUTPUT,
	@PatientExists BIT = 0 OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	

	-----------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE(),
		@DebugMessage VARCHAR(MAX)

	DECLARE	@Args VARCHAR(MAX) = 
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@StoreID=' + dbo.fnToStringOrEmpty(@StoreID)  + ';' +
		'@Nabp=' + dbo.fnToStringOrEmpty(@Nabp)  + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID)  + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey)  + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKeyType)  + ';' +
		'@SourcePatientKey=' + dbo.fnToStringOrEmpty(@SourcePatientKey)  + ';' +
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID)  + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title)  + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName)  + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName)  + ';' +
		'@MiddleName=' + dbo.fnToStringOrEmpty(@MiddleName)  + ';' +
		'@Suffix=' + dbo.fnToStringOrEmpty(@Suffix)  + ';' +
		'@BirthDate=' + dbo.fnToStringOrEmpty(@BirthDate)  + ';' +
		'@GenderKey=' + dbo.fnToStringOrEmpty(@GenderKey)  + ';' +
		'@GenderID=' + dbo.fnToStringOrEmpty(@GenderID)  + ';' +
		'@LanguageKey=' + dbo.fnToStringOrEmpty(@LanguageKey)  + ';' +
		'@LanguageID=' + dbo.fnToStringOrEmpty(@LanguageID)  + ';' +
		'@AddressLine1=' + dbo.fnToStringOrEmpty(@AddressLine1)  + ';' +
		'@AddressLine2=' + dbo.fnToStringOrEmpty(@AddressLine2)  + ';' +
		'@City=' + dbo.fnToStringOrEmpty(@City)  + ';' +
		'@State=' + dbo.fnToStringOrEmpty(@State)  + ';' +
		'@PostalCode=' + dbo.fnToStringOrEmpty(@PostalCode)  + ';' +
		'@PhoneNumber_Home=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home)  + ';' +
		'@PhoneNumber_Home_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsCallAllowed)  + ';' +
		'@PhoneNumber_Home_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsTextAllowed)  + ';' +
		'@PhoneNumber_Home_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsPreferred)  + ';' +
		'@PhoneNumber_Mobile=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile)  + ';' +
		'@PhoneNumber_Mobile_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsCallAllowed)  + ';' +
		'@PhoneNumber_Mobile_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsTextAllowed)  + ';' +
		'@PhoneNumber_Mobile_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsPreferred)  + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary)  + ';' +
		'@EmailAddress_Primary_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsEmailAllowed)  + ';' +
		'@EmailAddress_Primary_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsPreferred)  + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate)  + ';' +
		'@EmailAddress_Alternate_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsEmailAllowed)  + ';' +
		'@EmailAddress_Alternate_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsPreferred)  + ';' +
		'@Service_Mpc_ConnectionStatusKey=' + dbo.fnToStringOrEmpty(@Service_Mpc_ConnectionStatusKey)  + ';' +
		'@Service_Mpc_ConnectionStatusID=' + dbo.fnToStringOrEmpty(@Service_Mpc_ConnectionStatusID)  + ';' +
		'@Service_Mpc_ConnectionStatusCode=' + dbo.fnToStringOrEmpty(@Service_Mpc_ConnectionStatusCode)  + ';' +
		'@Service_Mpc_AccountKey=' + dbo.fnToStringOrEmpty(@Service_Mpc_AccountKey)  + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy)  + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy)  + ';' +
		'@ErrorLogID=' + dbo.fnToStringOrEmpty(@ErrorLogID)  + ';' +
		'@PatientID=' + dbo.fnToStringOrEmpty(@PatientID)  + ';' +
		'@PatientExists=' + dbo.fnToStringOrEmpty(@PatientExists)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	-----------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------------
	SET @CreatedBy = COALESCE(@CreatedBy, @ModifiedBy, SUSER_NAME());
	SET @ModifiedBy = COALESCE(@ModifiedBy, @CreatedBy, SUSER_NAME());
	SET @Debug = ISNULL(@Debug, 0);	
		
			
	-----------------------------------------------------------------------------------------------------------------------
	-- Translate raw data keys into business objects.
	-----------------------------------------------------------------------------------------------------------------------
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PatientKey = @PatientKey,
		@PatientKeyType = @PatientKeyType,
		@BusinessID = @BusinessEntityID OUTPUT,
		@PatientID = @PatientID OUTPUT,
		@IsOutputOnly = 1
		

	-----------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------------	

	-----------------------------------------------------------------------------------------------------------------------
	-- <Remarks>
	-- Legacy method for discovering a BusinessID (BusinessEntityID)
	-- </Remarks>
	-----------------------------------------------------------------------------------------------------------------------
	IF @BusinessEntityID IS NULL
	BEGIN	
		-- Get BusinessEntityID (Store)
		SET @BusinessEntityID = 
			CASE 
				WHEN @BusinessEntityID IS NOT NULL THEN @BusinessEntityID
				WHEN @StoreID IS NOT NULL THEN dbo.fnGetStoreIDFromSourceID(@StoreID)
				WHEN ISNULL(@Nabp, '') <> '' THEN dbo.fnGetStoreIDByNABP(@Nabp)
				ELSE NULL
			END;
	END
	
	-- Get LanguageID
	SET @LanguageID = ISNULL(@LanguageID, dbo.fnGetLanguageID(@LanguageKey));
	
	-- Get GenderID
	SET @GenderID = ISNULL(@GenderID, dbo.fnGetGenderID(@GenderKey));
	
	-- Get MPC service connection status.
	SET @Service_Mpc_ConnectionStatusID = COALESCE(@Service_Mpc_ConnectionStatusID, dbo.fnGetConnectionStatusID(@Service_Mpc_ConnectionStatusCode),
		dbo.fnGetConnectionStatusID(@Service_Mpc_ConnectionStatusKey));


	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables.' AS ActionMethod,
			@BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType, @PatientKey AS PatientKey,
			@PatientKeyType AS PatientKeyType, @BusinessEntityID AS BusinessEntityID, 
			@Nabp AS Nabp, @SourcePatientKey AS SourcePatientKey
	END	
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Merge patient record.
	-- <Summary>
	-- The merge process will examine the record and determine if it is 
	-- considered an add or an update.
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------------
	EXEC Poseidon.phrm.spPatient_Merge
		--Business
		@BusinessEntityID = @BusinessEntityID,
		--Pharmacy Key
		@SourcePatientKey = @SourcePatientKey,
		--Person
		@Title = @Title,
		@FirstName = @FirstName,
		@LastName = @LastName,
		@MiddleName = @MiddleName,
		@Suffix = @Suffix,
		@BirthDate = @BirthDate,
		@GenderID = @GenderID,
		@LanguageID = @LanguageID,
		--Person Address
		@AddressLine1 = @AddressLine1,
		@AddressLine2 = @AddressLine2,
		@City = @City,
		@State = @State,
		@PostalCode = @PostalCode,
		--Person Phone
		--	Home
		@PhoneNumber_Home = @PhoneNumber_Home,
		@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
		@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
		@PhoneNumber_Home_IsPreferred = @PhoneNumber_Home_IsPreferred,
		-- Mobile
		@PhoneNumber_Mobile = @PhoneNumber_Mobile,
		@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
		@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
		@PhoneNumber_Mobile_IsPreferred = @PhoneNumber_Mobile_IsPreferred,
		--Person Email
		--	Primary
		@EmailAddress_Primary = @EmailAddress_Primary,
		@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
		@EmailAddress_Primary_IsPreferred = @EmailAddress_Primary_IsPreferred,
		--	Alternate
		@EmailAddress_Alternate = @EmailAddress_Alternate,
		@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
		@EmailAddress_Alternate_IsPreferred = @EmailAddress_Alternate_IsPreferred,
		-- MPC Pharmacy Service
		@Service_Mpc_ConnectionStatusID = @Service_Mpc_ConnectionStatusID,
		@Service_Mpc_AccountKey = @Service_Mpc_AccountKey,
		-- Caller
		@CreatedBy = @CreatedBy,
		@ModifiedBy = @ModifiedBy,
		--Output
		@PatientID = @PatientID OUTPUT,
		@PatientExists = @PatientExists OUTPUT,
		@ErrorLogID = @ErrorLogID OUTPUT,
		@Debug = @Debug
				
				
END
