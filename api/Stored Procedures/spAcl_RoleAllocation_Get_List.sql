﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/1/2014
-- Description:	Returns a list of roles that meet the specified criteria.
-- SAMPLE CALL:
/*

EXEC api.spAcl_RoleAllocation_Get_List
	@ApplicationKey = 8,
	@BusinessKey = 3759

EXEC api.spAcl_RoleAllocation_Get_List
	@ApplicationKey = 10,
	@BusinessKey = 10684
	
EXEC api.spAcl_RoleAllocation_Get_List
	@ApplicationKey = 10,
	@BusinessKey = 38

EXEC api.spAcl_RoleAllocation_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '2501624',
	@BusinessKeyType = 'NABP'	

*/

-- SELECT * FROM dbo.Application
-- SELECT * FROM acl.RoleAllocation
-- SELECT * FROM dbo.Chain WHERE Name LIKE '%Associated%'
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spAcl_RoleAllocation_Get_List]
	@ApplicationKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	@RoleKey VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	--Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@RoleKey=' + dbo.fnToStringOrEmpty(@RoleKey) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';' 
	
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@BusinessIdArray VARCHAR(MAX)

	--------------------------------------------------------------------------------------------
	-- Set variables.
	--------------------------------------------------------------------------------------------
	-- TODO: Need to create business and application key translators to support the passing of
	-- delimited list items.
	SET @BusinessIdArray = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);
	SET @ApplicationKey = dbo.fnGetApplicationID(@ApplicationKey);
				
	--------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'BusinessEntityID', 'BusinessID', 'Name') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;

	--------------------------------------------------------------------------------------------
	-- Build results
	--------------------------------------------------------------------------------------------
	;WITH cteRoles AS (	
		
		SELECT DISTINCT
			CASE 
				WHEN @SortField IN ('BusinessEntityID', 'BusinessID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY ra.BusinessEntityID ASC) 
				WHEN @SortField IN ('BusinessEntityID', 'BusinessID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY ra.BusinessEntityID DESC)     
				WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY r.Name ASC) 
				WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY r.Name DESC)  
				ELSE ROW_NUMBER() OVER (ORDER BY r.Name ASC)
			END AS RowNumber,
			ra.ApplicationID,
			app.Code AS ApplicationCode,
			app.Name AS ApplicationName,
			ra.BusinessEntityID AS BusinessID,
			COALESCE(sto.Name, chn.Name) AS BusinessName,
			ra.RoleID,
			r.Code AS RoleCode,
			r.Name AS RoleName,
			ISNULL(ra.Description, r.Description) AS RoleDescription,
			ra.DateCreated
		FROM acl.RoleAllocation ra
			JOIN acl.Roles r
				ON ra.RoleID = r.RoleID
			LEFT JOIN dbo.Application app
				ON ra.ApplicationID = app.ApplicationID
			LEFT JOIN dbo.Store sto
				ON ra.BusinessEntityID = sto.BusinessEntityID
			LEFT JOIN dbo.Chain chn
				ON ra.BusinessEntityID = chn.BusinessEntityID
		WHERE ( @ApplicationKey IS NULL OR ra.ApplicationID IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@ApplicationKey, ',')) )
			AND ( 
				( @BusinessKey IS NULL OR (
					ra.BusinessEntityID IS NULL OR 
					ra.BusinessEntityID IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@BusinessIdArray, ',')) )
				)
			)
			AND ( @RoleKey IS NULL OR ra.RoleID IN (SELECT CASE WHEN ISNUMERIC(Value) = 1 THEN Value ELSE NULL END FROM dbo.fnSplit(@RoleKey, ',')) )
	
	)

	-- Return a pageable result set.	
	SELECT 
		ApplicationID,
		ApplicationCode,
		ApplicationName,
		BusinessID,
		BusinessName,
		RoleID,
		RoleCode,
		RoleName,
		RoleDescription,
		(SELECT COUNT(*) FROM cteRoles) AS TotalRecords
	FROM cteRoles
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
	
	
	
END
