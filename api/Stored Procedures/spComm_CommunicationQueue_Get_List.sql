﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/21/2015
-- Description:	Returns a list of CommunicationQueue objects.
-- SAMPLE CALL:
/*

-- Business.
EXEC api.spComm_CommunicationQueue_Get_List
	@BusinessKey = '2501624',
	@BusinessKeyType = 'NABP'
	

*/
-- SELECT * FROM comm.CommunicationQueue
-- SELECT * FROM msg.MessageType
-- SELECT * FROM dbo.Application
-- SELECT * FROM dbo.Business
-- =============================================
CREATE PROCEDURE [api].[spComm_CommunicationQueue_Get_List]
	@CommunicationQueueKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@MessageTypeKey VARCHAR(MAX) = NULL,
	@CommunicationMethodKey VARCHAR(MAX) = NULL,
	@TargetAudienceSourceKey VARCHAR(MAX) = NULL,
	@RunOn DATETIME = NULL,
	@ExecutionStatusKey VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationQueueKey=' + dbo.fnToStringOrEmpty(@CommunicationQueueKey) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@MessageTypeKey=' + dbo.fnToStringOrEmpty(@MessageTypeKey) + ';' +
		'@CommunicationMethodKey=' + dbo.fnToStringOrEmpty(@CommunicationMethodKey) + ';' +
		'@TargetAudienceSourceKey=' + dbo.fnToStringOrEmpty(@TargetAudienceSourceKey) + ';' +
		'@RunOn=' + dbo.fnToStringOrEmpty(@RunOn) + ';' +
		'@ExecutionStatusKey=' + dbo.fnToStringOrEmpty(@ExecutionStatusKey) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------
	-- Parameter Sniffing Fix
	-- <Summary>
	-- Saves a copy of the global input variables to a local variable and then the local
	-- variable is used to perform all the necessary tasks within the procedure.  This allows
	-- the optimizer to use statistics instead of a cached plan that might not be optimized
	-- for the sparatic queries.
	-- </Summary>
	--------------------------------------------------------------------------------------------
	DECLARE
		@CommunicationQueueKey_Local VARCHAR(MAX) = @CommunicationQueueKey,
		@ApplicationKey_Local VARCHAR(MAX) = @ApplicationKey,
		@BusinessKey_Local VARCHAR(MAX) = @BusinessKey,
		@BusinessKeyType_Local VARCHAR(50) = @BusinessKeyType,
		@MessageTypeKey_Local VARCHAR(MAX) = @MessageTypeKey,
		@CommunicationMethodKey_Local VARCHAR(MAX) = @CommunicationMethodKey,
		@TargetAudienceSourceKey_Local VARCHAR(MAX) = @TargetAudienceSourceKey,
		@RunOn_Local DATETIME = @RunOn,
		@ExecutionStatusKey_Local VARCHAR(50) = @ExecutionStatusKey,
		@Skip_Local BIGINT = @Skip,
		@Take_Local BIGINT = @Take,
		@SortCollection_Local VARCHAR(MAX) = @SortCollection	


	-------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@CommuniationMethodIdArray VARCHAR(MAX),
		@BusinessID BIGINT = NULL,
		@ApplicationIdArray VARCHAR(MAX),
		@MessageTypeIdArray VARCHAR(MAX),
		@CommunicationMethodIdArray VARCHAR(MAX)

	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	SET @ApplicationIdArray = dbo.fnApplicationKeyTranslator(@ApplicationKey_Local, DEFAULT);
	SET @MessageTypeIdArray = comm.fnMessageTypeKeyTranslator(@MessageTypeKey_Local, DEFAULT);
	SET @CommunicationMethodIdArray = comm.fnCommunicationMethodKeyTranslator(@CommunicationMethodKey_Local, DEFAULT);
	--SET @BusinessID = dbo.fnBusinessIDTranslator(@BusinessKey_Local, @BusinessKeyType_Local);


	-- Debug
	--SELECT 
	--	@ServiceKey_Local AS ServiceKey, @ApplicationKey_Local AS Application, 
	--	@MessageTypeKey_Local AS MessageTypeKey, @CommunicationMethodKey_Local AS CommunicationMethodKey,
	--	@ServiceIdArray AS ServiceIdArray, @ApplicationIdArray AS ApplicationIdArray, 
	--	@MessageTypeIdArray AS MessageTypeIdArray, @CommunicationMethodIdArray AS CommunicationMethodIdArray	
	
	--------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------			
	-- Support paging
	SET @Skip_Local = ISNULL(@Skip_Local, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take_Local = ISNULL(@Take_Local, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
		
	-------------------------------------------------------------------------------------
	-- Fetch results.
	-------------------------------------------------------------------------------------
	;WITH cteItems AS (
		SELECT
			CASE 
				WHEN @SortField IN ('CommunicationQueueID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY q.CommunicationQueueID ASC) 
				WHEN @SortField IN ('CommunicationQueueID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY q.CommunicationQueueID DESC)     
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY q.DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY q.DateCreated DESC)   
				ELSE ROW_NUMBER() OVER (ORDER BY q.CommunicationQueueID ASC)
			END AS RowNumber,
			q.CommunicationQueueID,
			q.ApplicationID,
			app.Code AS ApplicationCode,
			app.Name AS ApplicationName,
			q.BusinessID,
			sto.Nabp,
			sto.Name AS StoreName,
			q.MessageTypeID,
			typ.Code AS MessageTypeCode,
			typ.Name AS MessageTypeName,
			q.CommunicationMethodID,
			meth.Code AS CommunicationMethodCode,
			meth.Name AS CommunicationMethodName,
			q.CommunicationName,
			q.TargetAudienceSourceID,
			aud.Code AS TargetAudienceSourceCode,
			aud.Name AS TargetAudienceSourceName,
			q.RunOn,
			q.DateStart,
			q.DateEnd,
			q.ExecutionStatusID,
			stat.Code AS ExecutionStatusCode,
			stat.Name AS ExecutionStatusName,
			tz.TimeZoneID,
			tz.Code AS TimeZoneCode,
			tz.Location AS TimeZoneName,
			tz.Prefix AS TimeZonePrefix,
			q.rowguid,
			q.DateCreated,
			q.DateModified,
			q.CreatedBy,
			q.ModifiedBy
		--SELECT TOP 1 *
		--SELECT *
		FROM comm.CommunicationQueue q
			LEFT JOIN dbo.Application app
				ON q.ApplicationID = app.ApplicationID
			LEFT JOIN dbo.Store sto
				ON q.BusinessID = sto.BusinessEntityID
			LEFT JOIN dbo.TimeZone tz
				ON sto.TimeZoneID = tz.TimeZoneID
			LEFT JOIN comm.MessageType typ
				ON q.MessageTypeID = typ.MessageTypeID
			LEFT JOIN comm.CommunicationMethod meth
				ON q.CommunicationMethodID = meth.CommunicationMethodID
			LEFT JOIN etl.ExecutionStatus stat
				ON q.ExecutionStatusID = stat.ExecutionStatusID
			LEFT JOIN comm.TargetAudienceSource aud
				ON q.TargetAudienceSourceID = aud.TargetAudienceSourceID
		WHERE ( (@BusinessKey_Local IS NULL OR @BusinessKey_Local = '' OR @BusinessKeyType_Local IS NULL OR @BusinessKeyType_Local = '') OR
			( (@BusinessKey_Local IS NOT NULL OR @BusinessKey_Local <> '' OR @BusinessKeyType_Local IS NOT NULL OR @BusinessKeyType_Local <> '')
				AND ( (@BusinessKeyType_Local IN ('ID', 'BusinessID', 'BusinessEntityID') AND q.BusinessID IN (SELECT Value FROM dbo.fnSplit(@BusinessKey_Local, ',') WHERE ISNUMERIC(Value) = 1))
					OR (@BusinessKeyType_Local IN ('NABP', 'NCPDPID') AND sto.Nabp IN (SELECT Value FROM dbo.fnSplit(@BusinessKey_Local, ','))))))
			AND ( @ApplicationKey_Local IS NULL OR ( @ApplicationKey_Local IS NOT NULL AND q.ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationIdArray, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @MessageTypeKey_Local IS NULL OR ( @MessageTypeKey_Local IS NOT NULL AND q.MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeIdArray, ',') WHERE ISNUMERIC(Value) = 1))) 
			AND ( @CommunicationMethodKey_Local IS NULL OR ( @CommunicationMethodKey_Local IS NOT NULL AND q.CommunicationMethodID IN (SELECT Value FROM dbo.fnSplit(@CommunicationMethodIdArray, ',') WHERE ISNUMERIC(Value) = 1))) 
				
	)

	SELECT
		CommunicationQueueID,
		ApplicationID,
		ApplicationCode,
		ApplicationName,
		BusinessID,
		Nabp,
		StoreName,
		MessageTypeID,
		MessageTypeCode,
		MessageTypeName,
		CommunicationMethodID,
		CommunicationMethodCode,
		CommunicationMethodName,
		CommunicationName,
		TargetAudienceSourceID,
		TargetAudienceSourceCode,
		TargetAudienceSourceName,
		RunOn,
		DateStart,
		DateEnd,
		ExecutionStatusID,
		ExecutionStatusCode,
		ExecutionStatusName,
		TimeZoneID,
		TimeZoneCode,
		TimeZoneName,
		TimeZonePrefix,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
	FROM cteItems
	WHERE RowNumber > @Skip_Local 
		AND RowNumber <= (@Skip_Local + @Take_Local)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
END
