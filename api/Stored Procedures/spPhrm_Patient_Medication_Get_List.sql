﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description:	Gets a list of Medication objects that are applicable to a patient.
-- SAMPLE CALL;
/*
EXEC api.spPhrm_Patient_Medication_Get_List
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@Skip = NULL,
	@Take = NULL
 */
-- SELECT * FROM phrm.fnGetPatientInformation(2) 
-- SELECT * FROM dbo.MedicationType
-- SELECT * FROM dbo.PrescriberType
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Medication_Get_List]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@MedicationKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of MedicationIDs
	@MedicationTypeKey VARCHAR(MAX) = NULL,  -- A single or comma delimited list of MedicationTypeIDs
	@PrescriberTypeKey VARCHAR(MAX) = NULL,  -- A single or comma delimited list of PrescriberTypeIDs
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');	

	
	---------------------------------------------------------------------
	-- Return patient Medication objects.
	---------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
		EXEC api.spPhrm_Medication_Get_List
			@MedicationKey = @MedicationKey,
			@PersonKey = @PersonID,
			@MedicationTypeKey = @MedicationTypeKey,
			@PrescriberTypeKey = @PrescriberTypeKey,
			@Skip = @Skip,
			@Take = @Take,
			@SortCollection = @SortCollection
	END
			
END
