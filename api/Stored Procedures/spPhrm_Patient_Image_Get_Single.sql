﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/17/2014
-- Description:	Returns a single image based on its unique key.
/*

EXEC api.spPhrm_Patient_Image_Get_Single
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@ImageID = 4

*/

-- SELECT * FROM dbo.Images
-- =============================================
CREATE PROCEDURE api.spPhrm_Patient_Image_Get_Single
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@ImageID BIGINT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@BusinessID BIGINT = NULL
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');
	SET @BusinessID = (SELECT BusinessEntityID FROM phrm.Patient WHERE PatientID = @PatientID);
	
	---------------------------------------------------------------------
	-- Fetch patient image records.
	---------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
		EXEC api.spDbo_Image_Get_List
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessID,
			@PersonKey = @PersonID,
			@ImageKey = @ImageID
	END
		
		
		
		
		
END
