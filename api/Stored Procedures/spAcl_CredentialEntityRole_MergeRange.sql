﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/2/2014
-- Description:	Creates a new set of CredentialEntityRole objects. The process will not fail if an object already exists.
--		The process will ignore the existing object and add the objects that do not exist.
-- SAMPLE CALL:
/*

DECLARE 
	@CredentialEntityRoleIDs VARCHAR(MAX) = NULL
	
EXEC api.spAcl_CredentialEntityRole_MergeRange
	@CredentialKey = 959790,
	@BusinessKey = 3759,
	@ApplicationKey = 'PARX',
	@RoleKey = '10,11,12,13',
	@CredentialEntityRoleIDs = @CredentialEntityRoleIDs OUTPUT,
	@DeleteUnspecified = 1,
	@CreatedBy = 'dhughes'

SELECT @CredentialEntityRoleIDs AS CredentialEntityRoleIDs

*/

-- SELECT * FROM acl.Roles
-- SElECT * FROM dbo.Application
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM acl.CredentialEntityRole WHERE CredentialEntityID = 959790
-- =============================================
CREATE PROCEDURE [api].[spAcl_CredentialEntityRole_MergeRange]
	@CredentialKey VARCHAR(50),
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	@RoleKey VARCHAR(MAX),
	@DeleteUnspecified BIT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CredentialEntityRoleIDs VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @CredentialEntityRoleIDs = NULL;
	SET @DeleteUnspecified = ISNULL(@DeleteUnspecified, 0);
	
	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE
		@CredentialEntityID BIGINT,
		@ApplicationID INT,
		@BusinessID BIGINT
	

	--------------------------------------------------------------------------------------------
	-- Translate system keys.
	--------------------------------------------------------------------------------------------
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialKey = @CredentialKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@CredentialToken = @CredentialKey OUTPUT,
		@IsOutputOnly = 1
	
	
	--------------------------------------------------------------------------------------------
	-- Verify the roles that were passed are within the parameters of the provided arguments.
	--------------------------------------------------------------------------------------------
	DECLARE @VerifiedRoleArray VARCHAR(MAX) = (
		SELECT CONVERT(VARCHAR, RoleID) + ','
		--SELECT *
		FROM acl.RoleAllocation
		WHERE ( @ApplicationKey IS NULL OR ApplicationID = @ApplicationID )
			AND RoleID IN ( SELECT Value FROM dbo.fnSplit(@RoleKey, ',') WHERE ISNUMERIC(Value) = 1 )
		FOR XML PATH('')
	);
		
	
	--------------------------------------------------------------------------------------------
	-- Create new Role object(s).
	--------------------------------------------------------------------------------------------
	EXEC acl.spCredentialEntityRole_MergeRange
		@CredentialEntityID = @CredentialEntityID,
		@ApplicationID = @ApplicationID,
		@BusinessEntityID = @BusinessID,
		@RoleID = @VerifiedRoleArray,
		@DeleteUnspecified = @DeleteUnspecified,
		@CreatedBy = @CreatedBy,
		@CredentialEntityRoleIDs = @CredentialEntityRoleIDs OUTPUT
	
	
END
