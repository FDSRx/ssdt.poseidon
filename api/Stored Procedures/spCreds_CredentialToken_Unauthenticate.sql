﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/16/2014
-- Description:	Unauthenticates an authenticated user (i.e. expires their authentication token, etc.).
/*

EXEC api.spCreds_CredentialToken_Unauthenticate
	@BusinessKey = '10684', 
	@Token = '7EE14C75-E6C9-4E04-B5DC-8A196C618E3E'

*/

-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC 
-- SELECT * FROM creds.CredentialToken WHERE Token = '7EE14C75-E6C9-4E04-B5DC-8A196C618E3E'
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialToken_Unauthenticate]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@Token VARCHAR(50),
	@IsBusinessVerificationRequired BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------
	SET @IsBusinessVerificationRequired = ISNULL(@IsBusinessVerificationRequired, 0);
	
	---------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------
	DECLARE
		@CredentialEntityID BIGINT,
		@ApplicationID INT,
		@PersonID BIGINT,
		@BusinessID BIGINT = NULL,
		@CredentialToken VARCHAR(50) = NULL,
		@IsSuccess BIT = 1,
		@Message VARCHAR(4000) = 'The token has been successfully unauthenticated from the system.'
	
	---------------------------------------------------------------------
	-- Set local variables.
	---------------------------------------------------------------------
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialKey = @Token,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@CredentialToken = @CredentialToken OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1
	

	---------------------------------------------------------------------
	-- Force expire the current credential token to unauthenticate the
	-- credential.
	---------------------------------------------------------------------
	IF @CredentialToken IS NOT NULL
	BEGIN
	
		EXEC creds.spCredentialToken_Update
			@Token = @CredentialToken,
			@ForceExpire = 1	
			
	END
	
	
	SELECT
		@IsSuccess AS IsSuccess,
		@Message AS Message
		
		
		
		
END
