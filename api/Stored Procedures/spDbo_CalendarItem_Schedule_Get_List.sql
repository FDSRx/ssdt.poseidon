﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/02/2014
-- Description:	Gets a list of CalendarItem schedule objects.
-- Change log:
-- 3/1/2016 - dhughes - Modified the procedure to better handle row numbering for faster performance. Added the ability to record
--					requests (when applicable).
-- 4/18/2016 - dhughes - Modified the procedure to return the "AdditionalData" and "CorrelationKey" properties.
-- 6/17/2016 - dhughes - Modified the procedure to accept the Application and Business object parameters.
-- 6/22/2016 - dhughes - Modified the procedure to use the Business object on the CalendarItem object to improve filter performance.

-- SAMPLE CALL:
/*

DECLARE 
	@ApplicationKey VARCHAR(50) = 'ENG',
	@BusinessKey VARCHAR(50) = '2501624',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Task object identifiers.
	@ScopeKey VARCHAR(MAX) = '2', -- NOTE: Key will only be used in absense of Id.
	@BusinessEntityKey VARCHAR(MAX) = '38', -- NOTE: A single or comma delimited list of BusinessEntity keys.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@Location VARCHAR(500) = NULL,
	@DateTimeFrameStart DATETIME = NULL,
	@DateTimeframeEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1
	
EXEC api.spDbo_CalendarItem_Schedule_Get_List
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@NoteKey = @NoteKey,
	@ScopeKey = @ScopeKey,
	@BusinessEntityKey = @BusinessEntityKey,
	@NotebookKey = @NotebookKey,
	@PriorityKey = @PriorityKey,
	@OriginKey = @OriginKey,
	@TagKey = @TagKey,
	@Location = @Location,
	@DateTimeFrameStart = @DateTimeFrameStart,
	@DateTimeframeEnd = @DateTimeframeEnd,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection,
	@Debug = @Debug

	
*/

-- SELECT * FROM dbo.CalendarItem
-- SELECT * FROM schedule.NoteSchedule
-- SELECT * FROM dbo.vwNote
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spDbo_CalendarItem_Schedule_Get_List]
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@NoteKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of Task object identifiers.
	@ScopeKey VARCHAR(MAX) = NULL, -- NOTE: Key will only be used in absense of Id.
	@BusinessEntityKey VARCHAR(MAX), -- NOTE: A single or comma delimited list of BusinessEntity keys.
	@NotebookKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Notebook object keys.
	@PriorityKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Priority object keys.
	@OriginKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Origin object keys.
	@TagKey VARCHAR(MAX) = NULL, -- Single or comma delimited list of Tag object keys.
	@Location VARCHAR(500) = NULL,
	@DateTimeFrameStart DATETIME = NULL,
	@DateTimeframeEnd DATETIME = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@NoteKey=' + dbo.fnToStringOrEmpty(@NoteKey)  + ';' +
		'@ScopeKey=' + dbo.fnToStringOrEmpty(@ScopeKey)  + ';' +
		'@BusinessEntityKey=' + dbo.fnToStringOrEmpty(@BusinessEntityKey)  + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey)  + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey)  + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey)  + ';' +
		'@TagKey=' + dbo.fnToStringOrEmpty(@TagKey)  + ';' +
		'@Location=' + dbo.fnToStringOrEmpty(@Location)  + ';' +
		'@DateTimeFrameStart=' + dbo.fnToStringOrEmpty(@DateTimeFrameStart)  + ';' +
		'@DateTimeframeEnd=' + dbo.fnToStringOrEmpty(@DateTimeframeEnd)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;



	-----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, 'NABP');


	-----------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@NoteTypeKey VARCHAR(25),
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType),
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey)
		
	-----------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------------------	
	SET @ScopeKey = dbo.fnGetScopeID(@ScopeKey);
	SET @NoteTypeKey = dbo.fnNoteTypeKeyTranslator('CI', DEFAULT);
	SET @NotebookKey = dbo.fnNotebookKeyTranslator(@NotebookKey, DEFAULT);
	SET @PriorityKey = dbo.fnPriorityKeyTranslator(@PriorityKey, DEFAULT);
	SET @OriginKey = dbo.fnOriginKeyTranslator(@OriginKey, DEFAULT);
	SET @TagKey = dbo.fnTagKeyTranslator(@TagKey, DEFAULT);
	
	-- debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables.' AS ActionMethod, 
			@ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeType,
			@BusinessID AS BusinessID, @NoteKey AS NoteKey, @NoteTypeKey AS NoteTypeKey, @NotebookKey AS NotebookKey, 
			@PriorityKey AS PriorityKey, @DateTimeFrameStart AS DateTimeFrameStart, @DateTimeFrameEnd AS DateTimeFrameEnd
	END


	-----------------------------------------------------------------------------------------------------------------------------
	-- Configure paging.
	-----------------------------------------------------------------------------------------------------------------------------		
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)

	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);

	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Create result set
	-----------------------------------------------------------------------------------------------------------------------------
	-- SELECT * FROM dbo.Note
	
	;WITH cteNotes AS (
	
		SELECT
			ROW_NUMBER() OVER (ORDER BY		
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'asc'  THEN n.Title END  ASC,
				CASE WHEN @SortField = 'Title' AND @SortDirection = 'desc'  THEN n.Title END DESC,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'asc'  THEN n.DateCreated END ASC ,
				CASE WHEN @SortField IN ('DateCreated', 'DateRequested', 'Date Created', 'Date Requested') AND @SortDirection = 'desc'  THEN n.DateCreated END DESC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'asc'  THEN n.DateModified END ASC ,
				CASE WHEN @SortField IN ('DateModified', 'StatusChanged', 'Date Modified', 'Status Changed') AND @SortDirection = 'desc'  THEN n.DateModified END DESC ,     
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'asc'  THEN n.CreatedBy END ASC,
				CASE WHEN @SortField IN ('CreatedBy', 'RequestedBy', 'Created By', 'Requested By') AND @SortDirection = 'desc'  THEN n.CreatedBy END DESC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'asc'  THEN n.ModifiedBy END ASC,
				CASE WHEN @SortField IN ('ModifiedBy', 'ChangedBy', 'Modified By', 'Changed By') AND @SortDirection = 'desc'  THEN n.ModifiedBy END  DESC,
				ns.DateStart DESC
			) AS RowNumber,
			--CASE    
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.DateCreated ASC) 
			--	WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.DateCreated DESC)
			--	WHEN @SortField = 'DateStart' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY ns.DateStart ASC) 
			--	WHEN @SortField = 'DateStart' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY ns.DateStart DESC)
			--	WHEN @SortField = 'Title' AND @SortDirection = 'asc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.Title ASC) 
			--	WHEN @SortField = 'Title' AND @SortDirection = 'desc'  
			--		THEN ROW_NUMBER() OVER (ORDER BY n.Title DESC)  
			--	ELSE ROW_NUMBER() OVER (ORDER BY ns.DateStart DESC)
			--END AS RowNumber,
			ns.NoteScheduleID,	
			ci.NoteID AS CalendarItemID,
			n.NoteTypeID AS NoteTypeID,
			nt.Code AS NoteTypeCode,
			nt.Name AS NoteTypeName,
			n.ScopeID AS ScopeID,
			scp.Code AS ScopeCode,
			scp.Name AS ScopeName,
			n.NotebookID AS NotebookID,
			nb.Code AS NotebookCode,
			nb.Name AS NotebookName,
			n.Title,
			n.Memo,
			--(
			--	SELECT t.TagID AS Id, t.Name AS Name
			--	FROM dbo.NoteTag ntag (NOLOCK)
			--		JOIN dbo.Tag t
			--			ON ntag.TagID = t.TagID
			--	WHERE ntag.NoteID = n.NoteID
			--	FOR XML PATH('Tag'), ROOT('Tags')
			--) AS Tags,
			n.PriorityID,
			p.Code AS PriorityCode,
			p.Name AS PriorityName,
			n.OriginID,
			ds.Code AS OriginCode,
			ds.Name AS OriginName,
			n.OriginDataKey,
			n.AdditionalData,
			n.CorrelationKey,
			ci.Location,
			freq.FrequencyTypeID,
			freq.Code AS FrequencyTypeCode,
			freq.Name AS FrequencyTypeName,
			ns.DateStart,
			ns.DateEnd,
			ns.IsAllDayEvent,
			ns.DateEffectiveStart,
			ns.DateEffectiveEnd,
			ci.DateCreated,
			ci.DateModified,
			ci.CreatedBy,
			ci.ModifiedBy			
		FROM dbo.CalendarItem ci (NOLOCK)
			JOIN dbo.Note n (NOLOCK)
				ON n.NoteID = ci.NoteID
			JOIN schedule.NoteSchedule ns (NOLOCK)
				ON ci.NoteID = ns.NoteID
			JOIN schedule.FrequencyType freq (NOLOCK)
				ON ns.FrequencyTypeID = freq.FrequencyTypeID
			LEFT JOIN dbo.NoteType nt (NOLOCK)
				ON n.NoteTypeID = nt.NoteTypeID
			LEFT JOIN dbo.Scope scp
				ON scp.ScopeID = n.ScopeID
			LEFT JOIN dbo.Priority p (NOLOCK)
				ON n.PriorityID = p.PriorityID
			LEFT JOIN dbo.Notebook nb (NOLOCK)
				ON n.NotebookID = nb.NotebookID	
			LEFT JOIN dbo.Origin ds (NOLOCK)
				ON n.OriginID = ds.OriginID
		WHERE ( @BusinessKey IS NULL OR (@BusinessKey IS NOT NULL AND ci.BusinessID = @BusinessID ))
			AND n.ScopeID = @ScopeKey
			AND ( @NoteKey IS NULL OR ( @NoteKey IS NOT NULL AND ci.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteKey, ',') WHERE ISNUMERIC(Value) = 1)))			
			AND ( @BusinessEntityKey IS NULL OR (@BusinessEntityKey IS NOT NULL AND n.BusinessEntityID IN (SELECT Value FROM dbo.fnSplit(@BusinessEntityKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @NoteTypeKey IS NULL OR (@NoteTypeKey IS NOT NULL AND n.NoteTypeID IN (SELECT Value FROM dbo.fnSplit(@NoteTypeKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @NotebookKey IS NULL OR (@NotebookKey IS NOT NULL AND n.NotebookID IN (SELECT Value FROM dbo.fnSplit(@NotebookKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @PriorityKey IS NULL OR (@PriorityKey IS NOT NULL AND n.PriorityID IN (SELECT Value FROM dbo.fnSplit(@PriorityKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( @OriginKey IS NULL OR (@OriginKey IS NOT NULL AND n.OriginID IN (SELECT Value FROM dbo.fnSplit(@OriginKey, ',') WHERE ISNUMERIC(Value) = 1)))
			AND ( NULLIF(@Location, '') IS NULL OR ( (NULLIF(@Location, '') IS NOT NULL AND ci.Location LIKE '%' + @Location + '%')))
			AND ( @DateTimeFrameStart IS NULL OR (
				@DateTimeFrameStart IS NOT NULL
				AND ns.DateStart >= @DateTimeFrameStart
				AND ns.DateEnd <= @DateTimeframeEnd
			))
			AND ( @TagKey IS NULL OR (@TagKey IS NOT NULL AND EXISTS(
				SELECT TOP 1 *
				FROM NoteTag tmp
				WHERE ci.NoteID = tmp.NoteID
					AND TagID IN(SELECT Value FROM dbo.fnSplit(@TagKey, ',') WHERE ISNUMERIC(Value) = 1))
			))

	)
	
	-- Fetch results from common table expression
	SELECT
		NoteScheduleID,
		CalendarItemID,
		NoteTypeID,
		NoteTypeCode,
		NoteTypeName,
		ScopeID,
		ScopeCode,
		ScopeName,
		NotebookID,
		NotebookCode,
		NotebookName,
		Title,
		Memo,
		CONVERT(XML, (
			SELECT t.TagID AS Id, t.Name AS Name
			FROM dbo.NoteTag ntag (NOLOCK)
				JOIN dbo.Tag t
					ON t.TagID = ntag.TagID
			WHERE ntag.NoteID = res.CalendarItemID
			FOR XML PATH('Tag'), ROOT('Tags')
		)) AS Tags,
		--CONVERT(XML, Tags) AS Tags,
		PriorityID,
		PriorityCode,
		PriorityName,
		OriginID,
		OriginCode,
		OriginName,
		OriginDataKey,
		AdditionalData,
		CorrelationKey,
		Location,
		FrequencyTypeID,
		FrequencyTypeCode,
		FrequencyTypeName,
		DateStart,
		DateEnd,
		IsAllDayEvent,
		DateEffectiveStart,
		DateEffectiveEnd,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteNotes) AS TotalRecordCount
	FROM cteNotes res
	WHERE RowNumber > @Skip AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
			
END
