﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/22/2015
-- Description:	Returns a list of MessageTypes and their applicable communication methods for the provided criteria.
-- SAMPLE CALL:
/*

-- Identifier.
EXEC api.spComm_CommunicationConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@CommunicationConfigurationKey = 1
	
-- Business.
EXEC api.spComm_CommunicationConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id'
	
-- Application.
EXEC api.spComm_CommunicationConfiguration_Get_List
	@ApplicationKey = 'ENG'
	
-- MessageTypes.
EXEC api.spComm_CommunicationConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@MessageTypeKey = 'WLCME'

-- Strict.
EXEC api.spComm_CommunicationConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@MessageTypeKey = 'HPPYBDAY',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 1
	
-- Strict.
EXEC api.spComm_CommunicationConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@MessageTypeKey = '2,3,4,5,6',
	@CommunicationMethodKey = 'VOICE',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 1

*/

-- SELECT * FROM comm.CommunicationConfiguration
-- SELECT * FROM comm.MessageType
-- SELECT * FROM comm.CommunicationMethod
-- =============================================
CREATE PROCEDURE [api].[spComm_CommunicationConfiguration_Get_List]
	@CommunicationConfigurationKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonTypeKey VARCHAR(50) = NULL,
	@MessageTypeKey VARCHAR(MAX) = NULL,
	@CommunicationMethodKey VARCHAR(MAX) = NULL,
	@EnforceApplicationSpecification BIT = NULl,
	@EnforceBusinessSpecification BIT = NULL,
	@EnforceEntitySpecification BIT = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Result schema binding
	-- <Summary>
	-- Returns a schema of the intended result set.  This is a workaround to assist certain
	-- frameworks in determining the correct result sets (i.e. SSIS, Entity Framework, etc.).
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF (1 = 2)
	BEGIN
		DECLARE
			@Property VARCHAR(MAX) = NULL;

		SELECT 
			CONVERT(BIGINT, @Property) AS CommunicationConfigurationID,
			CONVERT(INT, @Property) AS ApplicationID,
			CONVERT(BIGINT, @Property) AS BusinessID,
			CONVERT(BIGINT, @Property) AS EntityID,
			CONVERT(INT, @Property) AS MessageTypeID,
			CONVERT(VARCHAR(50), @Property) AS MessageTypeCode,
			CONVERT(VARCHAR(50), @Property) AS MessageTypeName,
			CONVERT(VARCHAR(1000), @Property) AS MessageTypeDescription,
			CONVERT(INT, @Property) AS CommunicationMethodID,
			CONVERT(VARCHAR(50), @Property) AS CommunicationMethodCode,
			CONVERT(VARCHAR(50), @Property) AS CommunicationMethodName,
			CONVERT(VARCHAR(1000), @Property) AS CommunicationMethodDescription,
			CONVERT(INT, @Property) AS MessagesPerMinutes,
			CONVERT(INT, @Property) AS MessagesNumberOfMinutes,
			CONVERT(INT, @Property) AS MessagesPerDays,
			CONVERT(INT, @Property) AS MessagesNumberOfDays,
			CONVERT(BIT, @Property) AS IsDisabled,
			CONVERT(UNIQUEIDENTIFIER, @Property) AS rowguid,
			CONVERT(DATETIME, @Property) AS DateCreated,
			CONVERT(DATETIME, @Property) AS DateModified,
			CONVERT(VARCHAR(256), @Property) AS CreatedBy,
			CONVERT(VARCHAR(256), @Property) AS ModifiedBy,
			CONVERT(BIGINT, @Property) AS TotalRecords
	END
	
	--------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@CommunicationConfigurationKey=' + dbo.fnToStringOrEmpty(@CommunicationConfigurationKey) + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PersonKey=' + dbo.fnToStringOrEmpty(@PersonKey) + ';' +
		'@PersonTypeKey=' + dbo.fnToStringOrEmpty(@PersonTypeKey) + ';' +
		'@MessageTypeKey=' + dbo.fnToStringOrEmpty(@MessageTypeKey) + ';' +
		'@CommunicationMethodKey=' + dbo.fnToStringOrEmpty(@CommunicationMethodKey) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @EnforceApplicationSpecification = ISNULL(@EnforceApplicationSpecification, 0);
	SET @EnforceBusinessSpecification = ISNULL(@EnforceBusinessSpecification, 0);
	SET @EnforceEntitySpecification = ISNULL(@EnforceEntitySpecification, 0);
	
		
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL,
		@PersonID BIGINT = NULL,
		@MessageTypeIdArray VARCHAR(MAX) = comm.fnMessageTypeKeyTranslator(@MessageTypeKey, DEFAULT),
		@CommunicationMethodIdArray VARCHAR(MAX) = comm.fnCommunicationMethodKeyTranslator(@CommunicationMethodKey, DEFAULT)
	
	-- Debug
	-- SELECT @MessageTypeIdArray AS MessageTypeIdArray, @CommunicationMethodIdArray AS CommunicationMethodIdArray

	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PersonKey = @PersonKey,
		@PersonTypeKey = @PersonTypeKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1	

	-- Debug
	--SELECT @ApplicationID AS AplicationID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
	--	@BusinessID AS BusinessID, @PersonKey AS PersonKey, @PersonTypeKey AS PersonTypeKey, @PersonID AS PersonID,
	--	@MessageTypeIdArray
		
		
	--------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	--------------------------------------------------------------------------------------------
	-- Fetch results.
	-- <Summary>
	-- Uses "bubble up" functionality to determine results (i.e. looks at specific criteria,
	-- to least specific criteria).
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF @EnforceApplicationSpecification = 0 AND @EnforceBusinessSpecification = 0
		AND @EnforceEntitySpecification = 0
	BEGIN
		
		;WITH cteItems AS (
			SELECT
				CASE 
					WHEN @SortField IN ('CommunicationConfigurationID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationConfigurationID ASC) 
					WHEN @SortField IN ('CommunicationConfigurationID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationConfigurationID DESC)
					WHEN @SortField IN ('CommunicationMethodID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationMethodID ASC) 
					WHEN @SortField IN ('CommunicationMethodID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationMethodID DESC)        
					WHEN @SortField = 'CommunicationMethodCode' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationMethodCode ASC) 
					WHEN @SortField = 'CommunicationMethodCode' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationMethodCode DESC) 
					WHEN @SortField = 'CommunicationMethodName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationMethodName ASC) 
					WHEN @SortField = 'CommunicationMethodName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY CommunicationMethodName DESC)
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeID ASC) 
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeID DESC) 				
					WHEN @SortField = 'MessageTypeCode' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeCode ASC) 
					WHEN @SortField = 'MessageTypeCode' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeCode DESC) 
					WHEN @SortField = 'MessageTypeName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeName ASC) 
					WHEN @SortField = 'MessageTypeName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeName DESC)
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated ASC) 
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated DESC)   
					ELSE ROW_NUMBER() OVER (ORDER BY MessageTypeName ASC)
				END AS RowNumber,
				CommunicationConfigurationID,
				ApplicationID,
				ApplicationCode,
				ApplicationName,
				BusinessID,
				EntityID,
				MessageTypeID,
				MessageTypeCode,
				MessageTypeName,
				MessageTypeDescription,
				CommunicationMethodID,
				CommunicationMethodCode,
				CommunicationMethodName,
				CommunicationMethodDescription,
				MessagesPerMinutes,
				MessagesNumberOfMinutes,
				MessagesPerDays,
				MessagesNumberOfDays,
				IsDisabled,
				rowguid,
				DateCreated,
				DateModified,
				CreatedBy,
				ModifiedBy
			--SELECT *
			FROM comm.fnGetCommunicationConfigurations(@ApplicationID, @BusinessID, @PersonID, DEFAULT) cfg
			WHERE ( @CommunicationConfigurationKey IS NULL OR CommunicationConfigurationID IN (
						SELECT Value FROM dbo.fnSplit(@CommunicationConfigurationKey, ',') WHERE ISNUMERIC(Value) =1) 
				)
				AND (@MessageTypeKey IS NULL OR cfg.MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeIdArray, ',') WHERE ISNUMERIC(Value) = 1))
				AND (@CommunicationMethodKey IS NULL OR cfg.CommunicationMethodID IN (SELECT Value FROM dbo.fnSplit(@CommunicationMethodIdArray, ',') WHERE ISNUMERIC(Value) = 1))
		)

		SELECT
			CommunicationConfigurationID,
			ApplicationID,
			ApplicationCode,
			ApplicationName,
			BusinessID,
			EntityID,
			MessageTypeID,
			MessageTypeCode,
			MessageTypeName,
			MessageTypeDescription,
			CommunicationMethodID,
			CommunicationMethodCode,
			CommunicationMethodName,
			CommunicationMethodDescription,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy,
			(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
		FROM cteItems
		WHERE RowNumber > @Skip 
			AND RowNumber <= (@Skip + @Take)
		ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.

	END	
	
	--------------------------------------------------------------------------------------------
	-- Fetch results.
	-- <Summary>
	-- Uses a more strict criterion based on the user's input.
	-- </Summary>
	--------------------------------------------------------------------------------------------	
	IF @EnforceApplicationSpecification = 1 OR @EnforceBusinessSpecification = 1
		OR @EnforceEntitySpecification = 1
	BEGIN

		;WITH cteItems AS (
			SELECT
				CASE 
					WHEN @SortField IN ('CommunicationConfigurationID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.CommunicationConfigurationID ASC) 
					WHEN @SortField IN ('CommunicationConfigurationID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.CommunicationConfigurationID DESC)
					WHEN @SortField IN ('CommunicationMethodID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.CommunicationMethodID ASC) 
					WHEN @SortField IN ('CommunicationMethodID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.CommunicationMethodID DESC)        
					WHEN @SortField = 'CommunicationMethodCode' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY meth.Code ASC) 
					WHEN @SortField = 'CommunicationMethodCode' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY meth.Code DESC) 
					WHEN @SortField = 'CommunicationMethodName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY meth.Name ASC) 
					WHEN @SortField = 'CommunicationMethodName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY meth.Name DESC)
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.MessageTypeID ASC) 
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.MessageTypeID DESC) 				
					WHEN @SortField = 'MessageTypeCode' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Code ASC) 
					WHEN @SortField = 'MessageTypeCode' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Code DESC) 
					WHEN @SortField = 'MessageTypeName' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Name ASC) 
					WHEN @SortField = 'MessageTypeName' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Name DESC)
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.DateCreated ASC) 
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.DateCreated DESC)   
					ELSE ROW_NUMBER() OVER (ORDER BY typ.Name ASC)
				END AS RowNumber,
				cfg.CommunicationConfigurationID,
				cfg.ApplicationID,
				app.Code AS ApplicationCode,
				app.Name AS ApplicationName,
				cfg.BusinessID,
				cfg.EntityID,
				cfg.MessageTypeID,
				typ.Code AS MessageTypeCode,
				typ.Name AS MessageTypeName,
				typ.Description AS MessageTypeDescription,
				cfg.CommunicationMethodID,
				meth.Code AS CommunicationMethodCode,
				meth.Name AS CommunicationMethodName,
				meth.Description AS CommunicationMethodDescription,
				cfg.MessagesPerMinutes,
				cfg.MessagesNumberOfMinutes,
				cfg.MessagesPerDays,
				cfg.MessagesNumberOfDays,
				cfg.IsDisabled,
				cfg.rowguid,
				cfg.DateCreated,
				cfg.DateModified,
				cfg.CreatedBy,
				cfg.ModifiedBy
			--SELECT *
			FROM comm.CommunicationConfiguration cfg
				LEFT JOIN comm.MessageType typ
					ON cfg.MessageTypeID = typ.MessageTypeID
				JOIN comm.CommunicationMethod meth
					ON cfg.CommunicationMethodID = meth.CommunicationMethodID
				LEFT JOIN dbo.Application app
					ON cfg.ApplicationID = app.ApplicationID
			WHERE ( @CommunicationConfigurationKey IS NULL OR CommunicationConfigurationID IN (
						SELECT Value FROM dbo.fnSplit(@CommunicationConfigurationKey, ',') WHERE ISNUMERIC(Value) =1) 
				)
				AND ( @EnforceApplicationSpecification = 0 OR cfg.ApplicationID = @ApplicationID )
				AND ( @EnforceBusinessSpecification = 0 OR cfg.BusinessID = @BusinessID )
				AND ( @EnforceEntitySpecification = 0 OR cfg.EntityID = @PersonID )
				AND (@MessageTypeKey IS NULL OR cfg.MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeIdArray, ',') WHERE ISNUMERIC(Value) = 1))
				AND (@CommunicationMethodKey IS NULL OR cfg.CommunicationMethodID IN (SELECT Value FROM dbo.fnSplit(@CommunicationMethodIdArray, ',') WHERE ISNUMERIC(Value) = 1))
		)

		SELECT
			CommunicationConfigurationID,
			ApplicationID,
			ApplicationCode,
			ApplicationName,
			BusinessID,
			EntityID,
			MessageTypeID,
			MessageTypeCode,
			MessageTypeName,
			MessageTypeDescription,
			CommunicationMethodID,
			CommunicationMethodCode,
			CommunicationMethodName,
			CommunicationMethodDescription,
			MessagesPerMinutes,
			MessagesNumberOfMinutes,
			MessagesPerDays,
			MessagesNumberOfDays,
			IsDisabled,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy,
			(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
		FROM cteItems
		WHERE RowNumber > @Skip 
			AND RowNumber <= (@Skip + @Take)
		ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.	
	
	
	END
	
	
END
