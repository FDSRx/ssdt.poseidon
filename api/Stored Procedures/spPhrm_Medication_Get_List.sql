﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description:	Returns a list of Medications.
-- SAMPLE CALL: 
/*
EXEC api.spPhrm_Medication_Get_List
	@PersonKey = 135590
*/
-- SELECT * FROM phrm.Medication
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Medication_Get_List]
	@MedicationKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of MedicationIDs
	@PersonKey VARCHAR(MAX) = NULL, -- A single or comma delimited list of PersonIDs
	@MedicationTypeKey VARCHAR(MAX) = NULL,  -- A single or comma delimited list of MedicationTypeIDs
	@PrescriberTypeKey VARCHAR(MAX) = NULL,  -- A single or comma delimited list of PrescriberTypeIDs
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)
		
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------	
	SET @MedicationTypeKey = phrm.fnMedicationTypeKeyTranslator(@MedicationTypeKey, DEFAULT);
	
	-- debug
	-- SELECT @MedicationTypeKey AS MedicationTypeKey
	
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	
	-- Scan and verify
	SET @SortField = CASE WHEN @SortField IN ('DateCreated', 'Title') THEN @SortField ELSE NULL END;
	SET @SortDirection = CASE WHEN @SortDirection IN ('asc', 'desc') THEN @SortDirection ELSE NULL END;


	--------------------------------------------------------------------------------------------
	-- Create result set
	--------------------------------------------------------------------------------------------
	-- SELECT * FROM phrm.Medication	
	;WITH cteMedications (
		RowNumber,
		MedicationID,
		PersonID,
		MedicationClassificationID,
		MedicationClassificationCode,
		MedicaitonClassificationName,
		MedicationTypeID,
		MedicationTypeCode,
		MedicationTypeName,
		Name,
		NDC,
		Quantity,
		Strength,
		Directions,
		Indication,
		DateFilled,
		PrescriberTypeID,
		PrescriberTypeCode,
		PrescriberTypeName,
		PrescriberFirstName,
		PrescriberLastName,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	) AS (
	
		SELECT
			CASE    
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY m.DateCreated ASC) 
				WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY m.DateCreated DESC)
				WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY m.Name ASC) 
				WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY m.Name DESC)  
				ELSE ROW_NUMBER() OVER (ORDER BY m.MedicationID DESC)
			END AS RowNumber,	
			m.MedicationID,
			m.PersonID,
			mt.MedicationClassificationID,
			mc.Code AS MedicationClassificationCode,
			mc.Name AS MedicationClassificationName,
			m.MedicationTypeID,
			mt.Code AS MedicationTypeCode,
			mt.Name AS MedicationTypeName,
			m.Name,
			m.NDC,
			m.Quantity,
			m.Strength,
			m.Directions,
			m.Indication,
			m.DateFilled,
			m.PrescriberTypeID,
			pt.Code AS PrescriberTypeCode,
			pt.Name AS PrescriberTypeName,
			m.PrescriberFirstName,
			m.PrescriberLastName,
			m.DateCreated,
			m.DateModified,
			m.CreatedBy,
			m.ModifiedBy
		FROM phrm.Medication m
			LEFT JOIN phrm.MedicationType mt
				ON m.MedicationTypeID = mt.MedicationTypeID
			LEFT JOIN phrm.MedicationClassification mc
				ON mt.MedicationClassificationID = mc.MedicationClassificationID
			LEFT JOIN phrm.PrescriberType pt
				ON m.PrescriberTypeID = pt.PrescriberTypeID
		WHERE ( @MedicationKey IS NULL OR m.MedicationID IN (SELECT Value FROM dbo.fnSplit(@MedicationKey, ',') WHERE ISNUMERIC(Value) = 1))
			AND ( @PersonKey IS NULL OR m.PersonID IN (SELECT Value FROM dbo.fnSplit(@PersonKey, ',') WHERE ISNUMERIC(Value) = 1)) 
			AND ( @MedicationTypeKey IS NULL OR m.MedicationTypeID IN (SELECT Value FROM dbo.fnSplit(@MedicationTypeKey, ',') WHERE ISNUMERIC(Value) = 1))
			AND ( @PrescriberTypeKey IS NULL OR m.PrescriberTypeID IN (SELECT Value FROM dbo.fnSplit(@PrescriberTypeKey, ',') WHERE ISNUMERIC(Value) = 1))
	)
	
	-- Fetch results from common table expression
	SELECT
		MedicationID,
		PersonID,
		MedicationClassificationID,
		MedicationClassificationCode,
		MedicaitonClassificationName,
		MedicationTypeID,
		MedicationTypeCode,
		MedicationTypeName,
		Name,
		NDC,
		Quantity,
		Strength,
		Directions,
		Indication,
		DateFilled,
		PrescriberTypeID,
		PrescriberTypeCode,
		PrescriberTypeName,
		PrescriberFirstName,
		PrescriberLastName,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		(SELECT COUNT(*) FROM cteMedications) AS TotalRecordCount
	FROM cteMedications
	WHERE RowNumber > @Skip AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.




END
