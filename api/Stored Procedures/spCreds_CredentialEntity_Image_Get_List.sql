﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/8/2014
-- Description:	Gets a list of images that are applicable to credential objects.
-- SAMPLE CALL:
/*

EXEC api.spCreds_CredentialEntity_Image_Get_List
	@ApplicationKey = 2,
	@BusinessKey = 10684,
	@BusinessKeyType = NULL,
	@CredentialKey = '801050'

*/

-- SELECT * FROM dbo.Images
-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialEntity WHERE CredentialEntityID = 801050	
-- =============================================
CREATE PROCEDURE api.spCreds_CredentialEntity_Image_Get_List
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@CredentialKey VARCHAR(50),
	@CredentialTypeKey VARCHAR(50) = NULL,
	@ImageKey VARCHAR(MAX) = NULL,
	@KeyName VARCHAR(MAX) = NULL,
	@LanguageKey VARCHAR(25) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------
	DECLARE
		@CredentialEntityID BIGINT,
		@ApplicationID INT,
		@PersonID BIGINT,
		@BusinessID BIGINT = NULL
		

	---------------------------------------------------------------------
	-- Set variables.
	---------------------------------------------------------------------
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialKey = @CredentialKey,
		@CredentialTypeKey = @CredentialTypeKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1

	-- Debug
	--SELECT @ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @CredentialEntityID AS CredentialEntityID, @PersonID AS PersonID
	
	
	---------------------------------------------------------------------
	-- Fetch credential image records.
	---------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
		EXEC api.spDbo_Image_Get_List
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessID,
			@PersonKey = @PersonID,
			@ImageKey = @ImageKey,
			@KeyName = @KeyName,
			@LanguageKey = @LanguageKey,
			@Skip = @Skip,
			@Take = @Take,
			@SortCollection = @SortCollection
	END
	
	
	
	
	
	
END
