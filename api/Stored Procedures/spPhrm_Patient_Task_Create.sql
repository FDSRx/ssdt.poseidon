﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/03/2014
-- Description:	Creates a new Task object for a patient.
-- Change log:
-- 3/4/2016 - dhughes - Modified the procedure to include additional input parameters.
-- 6/10/2016 - dhughes - Modified the procedure to accept the Application object parameter.

-- SAMPLE CALL:
/*

DECLARE 
	@NoteID BIGINT = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = '7871787',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@PatientKey VARCHAR(50) = '21656',
	@PatientKeyType VARCHAR(50) = 'SRC',
	@NotebookKey VARCHAR(50) = NULL,
	@Title VARCHAR(500) = 'Test API Stored Procedure Task On ' + CONVERT(VARCHAR, GETDATE()),
	@Memo VARCHAR(MAX) = 'This is a test task call from the stored procedure #: ' + CONVERT(VARCHAR, RAND()),
	@PriorityKey VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@OriginKey VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@DateDue DATETIME = NULL,
	@AssignedByID BIGINT = NULL,
	@AssignedByString VARCHAR(256) = NULL,
	@AssignedToID BIGINT = NULL,
	@AssignedToString VARCHAR(256) = NULL,
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusKey VARCHAR(50) = 'AUTO',
	@ParentTaskID BIGINT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@TaskID BIGINT = NULL,
	@Debug BIT = 1
	
EXEC api.spPhrm_Patient_Task_Create
	@NoteID = @NoteID,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@PatientKey = @PatientKey,
	@PatientKeyType = @PatientKeyType,
	@NotebookKey = @NotebookKey,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityKey = @PriorityKey,
	@Tags = @Tags,
	@OriginKey = @OriginKey,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@DateDue = @DateDue,
	@AssignedByID = @AssignedByID,
	@AssignedByString = @AssignedByString,
	@AssignedToID = @AssignedToID,
	@AssignedToString = @AssignedToString,
	@CompletedByID = @CompletedByID,
	@CompletedByString = @CompletedByString,
	@DateCompleted = @DateCompleted,
	@TaskStatusKey = @TaskStatusKey,
	@ParentTaskID = @ParentTaskID,
	@CreatedBy = @CreatedBy,
	@TaskID = @TaskID OUTPUT,
	@Debug = @Debug

SELECT @TaskID AS TaskID

 */
 
-- SELECT * FROM dbo.Note ORDER BY NoteID DESC
-- SELECT * FROM dbo.vwTask ORDER BY 1 DESC
-- SELECT * FROM dbo.Task ORDER BY 1 DESC
-- SELECT * FROM dbo.TaskStatus
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook
-- SELECT * FROM dbo.vwTask

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Task_Create]
	@NoteID BIGINT = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@NotebookKey VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX),
	@PriorityKey VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@OriginKey VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@DateDue DATETIME = NULL,
	@AssignedByID BIGINT = NULL,
	@AssignedByString VARCHAR(256) = NULL,
	@AssignedToID BIGINT = NULL,
	@AssignedToString VARCHAR(256) = NULL,
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusKey VARCHAR(50) = NULL,
	@ParentTaskID BIGINT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@TaskID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PatientKey=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		'@PatientKeyType=' + dbo.fnToStringOrEmpty(@PatientKey) + ';' +
		--'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		--'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode) + ';' +
		--'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		--'@NoteTypeKey=' + dbo.fnToStringOrEmpty(@NoteTypeKey) + ';' +
		'@NotebookKey=' + dbo.fnToStringOrEmpty(@NotebookKey) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityKey=' + dbo.fnToStringOrEmpty(@PriorityKey) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@OriginKey=' + dbo.fnToStringOrEmpty(@OriginKey) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@DateDue=' + dbo.fnToStringOrEmpty(@DateDue) + ';' +
		'@AssignedByID=' + dbo.fnToStringOrEmpty(@AssignedByID) + ';' +
		'@AssignedByString=' + dbo.fnToStringOrEmpty(@AssignedByString) + ';' +
		'@AssignedToID=' + dbo.fnToStringOrEmpty(@AssignedToID) + ';' +
		'@AssignedToString=' + dbo.fnToStringOrEmpty(@AssignedToString) + ';' +
		'@CompletedByID=' + dbo.fnToStringOrEmpty(@CompletedByID) + ';' +
		'@CompletedByString=' + dbo.fnToStringOrEmpty(@CompletedByString) + ';' +
		'@DateCompleted=' + dbo.fnToStringOrEmpty(@DateCompleted) + ';' +
		'@TaskStatusKey=' + dbo.fnToStringOrEmpty(@TaskStatusKey) + ';' +
		'@ParentTaskID=' + dbo.fnToStringOrEmpty(@ParentTaskID) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;	


	-----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------------------
	SET @TaskID = NULL;
	SET @Debug = ISNULL(@Debug, 0);
	SET @BusinesskeyType = ISNULL(@BusinessKeyType, 'NABP');
	SET @PatientKeyType = ISNULL(@PatientKeyType, 'SourcePatientKey');
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@BusinessID BIGINT,
		@PatientID BIGINT,
		@PersonID BIGINT,
		@ScopeID INT = dbo.fnGetScopeID('INTRN'), -- only create internal notes
		@NoteTypeID INT = dbo.fnGetNoteTypeID('TSK'),
		@NotebookID INT = dbo.fnGetNotebookID(@NotebookKey),
		@PriorityID INT = dbo.fnGetPriorityID(@PriorityKey),
		@OriginID INT = dbo.fnGetOriginID(@OriginKey),
		@TaskStatusID INT = dbo.fnGetTaskStatusID(@TaskStatusKey),
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey)
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------------------
	SET @BusinessID = CASE WHEN @BusinessKeyType IS NOT NULL THEN dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType) ELSE NULL END;

	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, @BusinessKeyType, @PatientKey, @PatientKeyType);
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');

	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@PatientID AS PatientID, @PersonID AS PersonID, @ScopeID AS ScopeID, @NoteTypeID AS NoteTypeID,
			@NotebookID AS NotebookID, @PriorityID AS PriorityID, @OriginID AS OriginID, @TaskStatusID AS TaskStatusID,
			@ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @PatientID AS PatientID, @PersonID AS PersonID		
	END	


	-----------------------------------------------------------------------------------------------------------------------------
	-- Create and return a patient Task object.
	-- <Summary>
	-- Creates a task that needs to be performed for a particular patient
	-- and returns the task identity.
	-- <Summary>
	-----------------------------------------------------------------------------------------------------------------------------	
	IF @PersonID IS NOT NULL
	BEGIN

		EXEC dbo.spTask_Create
			@NoteID = @NoteID,
			@ApplicationID = @ApplicationID,
			@BusinessID = @BusinessID,
			@ScopeID = @ScopeID,
			@BusinessEntityID = @PersonID,
			@NotebookID = @NotebookID,
			@PriorityID = @PriorityID,
			@Title = @Title,
			@Memo = @Memo,
			@Tags = @Tags,
			@OriginID = @OriginID,
			@OriginDataKey = @OriginDataKey,
			@AdditionalData = @AdditionalData,
			@CorrelationKey = @CorrelationKey,
			@DateDue = @DateDue,
			@AssignedByID = @AssignedByID,
			@AssignedByString = @AssignedByString,
			@AssignedToID = @AssignedToID,
			@AssignedToString = @AssignedToString,
			@CompletedByID = @CompletedByID,
			@CompletedByString = @CompletedByString,
			@DateCompleted = @DateCompleted,
			@TaskStatusID = @TaskStatusID,
			@ParentTaskID = @ParentTaskID,
			@CreatedBy = @CreatedBy,
			@TaskID = @TaskID OUTPUT,
			@Debug = @Debug
	END


			
END

