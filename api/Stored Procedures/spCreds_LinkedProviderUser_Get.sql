﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/15/2015
-- Description:	Returns the linked account that is associated with the credential and provider.
/*

DECLARE
	@LinkedProviderUserID BIGINT = NULL,
	@CredentialEntityID BIGINT,
	@CredentialProviderID INT,
	@UserKey VARCHAR(256)
	
EXEC api.spCreds_LinkedProviderUser_Get
	@ApplicationKey = 'ENG',
	@BusinessKey = 'ID_10684',
	@BusinessKeyType = NULL,
	@CredentialKey = '801050',
	@CredentialProviderKey = 'MRXA',
	@LinkedProviderUserID = @LinkedProviderUserID OUTPUT,
	@CredentialEntityID = @CredentialEntityID OUTPUT,
	@CredentialProviderID = @CredentialProviderID OUTPUT,
	@UserKey = @UserKey OUTPUT

SELECT @LinkedProviderUserID AS LinkedProviderUserID, @CredentialEntityID AS CredentialEntityID,
	@CredentialProviderID AS CredentialProviderID, @UserKey AS UserKey
	
*/

-- SELECT * FROM creds.vwExtranetUser WHERE CredentialEntityID = 801050
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE api.spCreds_LinkedProviderUser_Get
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(25) = NULL,
	@Username VARCHAR(256) = NULL,
	@Password VARCHAR(50) = NULL,
	@CredentialKey VARCHAR(50) = NULL,
	@CredentialEntityTypeKey VARCHAR(25) = NULL,
	@CredentialProviderKey VARCHAR(25) = NULL,
	@UserKey VARCHAR(256) = NULL OUTPUT,
	@LinkedProviderUserID BIGINT = NULL OUTPUT,
	@CredentialEntityID BIGINT = NULL OUTPUT,
	@CredentialProviderID INT = NULL OUTPUT,
	@IsOutputOnly BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-- Debug: Log request
	/*	
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@Password=' + dbo.fnToStringOrEmpty(@Password) + ';' +
		'@CredentialKey=' + dbo.fnToStringOrEmpty(@CredentialKey) + ';' +
		'@CredentialEntityTypeKey=' + dbo.fnToStringOrEmpty(@CredentialEntityTypeKey) + ';' +
		'@CredentialProviderKey=' + dbo.fnToStringOrEmpty(@CredentialProviderKey) + ';' +
		'@LinkedProviderUserID=' + dbo.fnToStringOrEmpty(@LinkedProviderUserID) + ';' +
		'@CredentialEntityID=' + dbo.fnToStringOrEmpty(@CredentialEntityID) + ';' +
		'@CredentialProviderID=' + dbo.fnToStringOrEmpty(@CredentialProviderID) + ';' +
		'@UserKey=' + dbo.fnToStringOrEmpty(@UserKey) + ';'
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	

	-------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------
	SET @UserKey = NULL;
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	SET @LinkedProviderUserID = NULL;
	SET @CredentialEntityID = NULL;
	SET @CredentialProviderID = NULL;

	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------	
	DECLARE 
		@ApplicationID INT,
		@BusinessID BIGINT = NULL,
		@CredentialEntityTypeID INT,
		@CredentialToken VARCHAR(50) = NULL
			
	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@CredentialTypeKey = @CredentialEntityTypeKey,
		@CredentialKey = @CredentialKey,
		@CredentialProviderKey = @CredentialProviderKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@CredentialEntityTypeID = @CredentialEntityTypeID OUTPUT,
		@CredentialEntityID = @CredentialEntityID OUTPUT,
		@CredentialProviderID = @CredentialProviderID OUTPUT,
		@IsOutputOnly = 1
	
	-- Debug
	--SELECT @ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @CredentialEntityID AS CredentialEntityID,
	--	@CredentialProviderID AS CredentialProviderID
		
	-------------------------------------------------------------------------------------------------
	-- Fetch results
	-------------------------------------------------------------------------------------------------
	-- If a user was discovered, then go ahead and fetch the results.
	IF @CredentialEntityID IS NOT NULL
	BEGIN
	
		EXEC creds.spLinkedProviderUser_Get
			@LinkedProviderUserID = @LinkedProviderUserID OUTPUT,
			@CredentialEntityID = @CredentialEntityID OUTPUT,
			@CredentialProviderID = @CredentialProviderID OUTPUT,
			@UserKey = @UserKey OUTPUT
	
	END
	
	-- Render the results if the "IsOutputOnly" flag is set to false (default value).
	IF @IsOutputOnly = 0
	BEGIN
		SELECT
			@LinkedProvideruserID AS LinkedProviderUserID,
			@CredentialEntityID AS CredentialEntityID,
			@CredentialProviderID AS CredentialProviderID,
			@UserKey AS UserKey
	END


END
