﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/5/2014
-- Description:	Returns a list of services.
-- Change log:
-- 1/4/2016 - dhughes - Modified the procedure to include additional properties in the result set.

/*

DECLARE
	@BusinessEntityServiceID VARCHAR(MAX) = NULL,
	@BusinessEntityID BIGINT = 38,
	@ServiceID VARCHAR(50) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC api.spDbo_BusinessEntityService_Get_List
	@BusinessEntityServiceID = @BusinessEntityServiceID,
	@BusinessEntityID = @BusinessEntityID,
	@ServiceID = @ServiceID,
	@Skip = @Skip,
	@Take = @Take,
	@SortCollection = @SortCollection

*/
-- SELECT * FROM dbo.Service	
-- SELECT * FROM dbo.BusinessEntityService WHERE BusinessEntityID = '10684'
-- SELECT * FROM dbo.vwStore WHERE Name LIKE '%Tommy%'
-- =============================================
CREATE PROCEDURE [api].[spDbo_BusinessEntityService_Get_List]
	@BusinessEntityServiceID VARCHAR(MAX) = NULL,
	@BusinessEntityID VARCHAR(MAX) = NULL,
	@ServiceID VARCHAR(MAX) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@BusinessEntityServiceID=' + dbo.fnToStringOrEmpty(@BusinessEntityServiceID)  + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID)  + ';' +
		'@ServiceID=' + dbo.fnToStringOrEmpty(@ServiceID)  + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip)  + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take)  + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);


	------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------


	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@BusinessEntityServiceID AS BusinessEntityServiceID, @BusinessEntityID AS BusinessEntityID, @ServiceID AS ServiceID
	END
	
	
	------------------------------------------------------------------------------------------------------------
	-- Paging.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10)
			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);

	
	------------------------------------------------------------------------------------------------------------
	-- Return results
	------------------------------------------------------------------------------------------------------------
	;WITH cteBusinessEntityServices AS 
	(
		SELECT
			ROW_NUMBER() OVER(ORDER BY
				CASE WHEN @SortField IN ('DateCreated') AND @SortDirection = 'asc'  THEN bes.DateCreated END ASC, 
				CASE WHEN @SortField IN ('DateCreated') AND @SortDirection = 'desc'  THEN bes.DateCreated END DESC,   
				CASE WHEN @SortField IN ('BusinessID', 'BusinessEntityID', 'Business') AND @SortDirection = 'asc'  THEN bes.BusinessEntityID END ASC,
				CASE WHEN @SortField IN ('BusinessID', 'BusinessEntityID', 'Business')  AND @SortDirection = 'desc'  THEN bes.BusinessEntityID END DESC,  
				bes.BusinessEntityID ASC,
				bes.ServiceID ASC
			) AS RowNumber,
			bes.BusinessEntityID,
			bes.ServiceID,
			srv.Code AS ServiceCode,
			srv.Name AS ServiceName,
			bes.AccountKey,
			bes.ConnectionStatusID,
			conn.Code AS ConnectionStatusCode,
			conn.Name AS ConnectionStatusName,
			ISNULL(conn.IsActive, 1) AS IsActive,
			ISNULL(conn.IsSelfOperated, 0) AS IsSelfOperated,
			bes.GoLiveDate,
			bes.DateTermed,
			bes.rowguid,
			bes.DateCreated,
			bes.DateModified,
			bes.CreatedBy,
			bes.ModifiedBy
		--SELECT *
		FROM dbo.BusinessEntityService bes
			JOIN dbo.Service srv
				ON bes.ServiceID = srv.ServiceID
			LEFT JOIN dbo.ConnectionStatus conn
				ON bes.ConnectionStatusID = conn.ConnectionStatusID
		WHERE ( @BusinessEntityID IS NOT NULL 
			AND bes.BusinessEntityID = @BusinessEntityID
			AND (@ServiceID IS NULL OR (@ServiceID IS NOT NULL AND bes.ServiceID IN (SELECT dbo.fnGetServiceID(Value) FROM dbo.fnSplit(@ServiceID, ','))))
		) 
		--OR (
		--	@BusinessEntityServiceID IS NOT NULL
		--	AND bes.BusinessEntityServiceID IN (SELECT Value FROM dbo.fnSplit(@BusinessEntityServiceID, ',') WHERE ISNUMERIC(Value) = 1)
		--)

	)
	
	SELECT
		BusinessEntityID,
		ServiceID,
		ServiceCode,
		ServiceName,
		AccountKey,
		ConnectionStatusID,
		ConnectionStatusCode,
		ConnectionStatusName,
		IsActive,
		IsSelfOperated,
		GoLiveDate,
		DateTermed,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	FROM cteBusinessEntityServices
	WHERE RowNumber > @Skip 
		AND RowNumber <= (@Skip + @Take)
	ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
	
	
END
