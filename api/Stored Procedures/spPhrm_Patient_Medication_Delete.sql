﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/21/2014
-- Description: Deletes a patient Medication object.
-- SAMPLE CALL:
/*
DECLARE 
	@MedicationID BIGINT = 53
	
EXEC api.spPhrm_Patient_Medication_Delete
	@BusinessKey = '7871787', 
	@PatientKey = '21656',
	@MedicationID = @MedicationID

SELECT @MedicationID AS MedicationID
	
*/
-- SELECT * FROM phrm.Medication
-- SELECT * FROM phrm.MedicationType
-- SELECT * FROM phrm.Provider
-- =============================================
CREATE PROCEDURE [api].[spPhrm_Patient_Medication_Delete]
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL,
	@PatientKey VARCHAR(50),
	@PatientKeyType VARCHAR(50) = NULL,
	@MedicationID BIGINT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------
	DECLARE
		@PatientID BIGINT,
		@PersonID BIGINT,
		@ErrorMessage VARCHAR(4000)
	
	---------------------------------------------------------------------
	-- Set local variables
	---------------------------------------------------------------------
	SET @PatientID = phrm.fnPatientIDTranslator(@BusinessKey, ISNULL(@BusinessKeyType, 'NABP'), 
						@PatientKey, ISNULL(@PatientKeyType, 'SourcePatientKey'));
	
	SET @PersonID = dbo.fnPersonIDTranslator(@PatientID, 'PatientID');

	---------------------------------------------------------------------------
	-- Argument resolution exceptions.
	-- <Summary>
	-- Validates resolved values.
	-- </Summary>
	---------------------------------------------------------------------------
	-- PersonID
	IF @PersonID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to delete Medication object. Object reference (@PatientKey) was not able to be translated. ' +
			'A valid @PatientKey value is required.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	---------------------------------------------------------------------
	-- Delete patient Medication object.
	---------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
		EXEC phrm.spMedication_Delete
			@MedicationID = @MedicationID,
			@PersonID = @PersonID
	END
	
		
	
END
