﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/21/2015
-- Description:	Returns a list of MessageType objects.
-- Change Log:
-- 7/22/2015 - dhughes - Modified the procedure to store the results into a temp table for returning either a standard
--							result set, or the ability to return an xml result set for cross-database referencing.
--							This allows for the data to be changed on Poseidon's front and allowing different platforms
--							to receive the data in Xml for parsing.
-- SAMPLE CALL:
/*

-- Identifier.
EXEC api.spComm_MessageTypeConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@MessageTypeConfigurationKey = 1
	
-- Business.
EXEC api.spComm_MessageTypeConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684'

-- Business as Xml.
EXEC api.spComm_MessageTypeConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@IsResultXml = 1

-- Business as Xml in output only mode.
EXEC api.spComm_MessageTypeConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@IsResultXml = 1,
	@IsOutputOnly = 1
	
-- Application.
EXEC api.spComm_MessageTypeConfiguration_Get_List
	@ApplicationKey = 'ENG'
	
-- MessageTypes.
EXEC api.spComm_MessageTypeConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@MessageTypeKey = 'WLCME,HPPYBDAY'


-- Strict.
EXEC api.spComm_MessageTypeConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '10684',
	@BusinessKeyType = 'Id',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 1
		
-- Strict.
EXEC api.spComm_MessageTypeConfiguration_Get_List
	@ApplicationKey = 'ENG',
	@BusinessKey = '38',
	@BusinessKeyType = 'Id',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 1

*/
-- SELECT * FROM comm.MessageTypeConfiguration
-- SELECT * FROM comm.MessageType
-- =============================================
CREATE PROCEDURE [api].[spComm_MessageTypeConfiguration_Get_List]
	@MessageTypeConfigurationKey VARCHAR(MAX) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@PersonKey VARCHAR(50) = NULL,
	@PersonTypeKey VARCHAR(50) = NULL,
	@MessageTypeKey VARCHAR(MAX) = NULL,
	@EnforceApplicationSpecification BIT = NULl,
	@EnforceBusinessSpecification BIT = NULL,
	@EnforceEntitySpecification BIT = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@IsOutputOnly BIT = NULL,
	@IsResultXml BIT = NULL,
	@MessageTypeConfigurationData XML = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------
	-- Result schema binding
	-- <Summary>
	-- Returns a schema of the intended result set.  This is a workaround to assist certain
	-- frameworks in determining the correct result sets (i.e. SSIS, Entity Framework, etc.).
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF (1 = 2)
	BEGIN
		DECLARE
			@Property VARCHAR(MAX) = NULL;
			
		SELECT 
			CONVERT(BIGINT, @Property) AS MessageTypeConfigurationID,
			CONVERT(INT, @Property) AS ApplicationID,
			CONVERT(BIGINT, @Property) AS BusinessID,
			CONVERT(BIGINT, @Property) AS EntityID,
			CONVERT(INT, @Property) AS MessageTypeID,
			CONVERT(VARCHAR(50), @Property) AS Code,
			CONVERT(VARCHAR(50), @Property) AS Name,
			CONVERT(VARCHAR(1000), @Property) AS Description,
			CONVERT(UNIQUEIDENTIFIER, @Property) AS rowguid,
			CONVERT(DATETIME, @Property) AS DateCreated,
			CONVERT(DATETIME, @Property) AS DateModified,
			CONVERT(VARCHAR(256), @Property) AS CreatedBy,
			CONVERT(VARCHAR(256), @Property) AS ModifiedBy,
			CONVERT(BIGINT, @Property) AS TotalRecordCount
	END
	
	--------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@PersonKey=' + dbo.fnToStringOrEmpty(@PersonKey) + ';' +
		'@PersonTypeKey=' + dbo.fnToStringOrEmpty(@PersonTypeKey) + ';' +
		'@MessageTypeKey=' + dbo.fnToStringOrEmpty(@MessageTypeKey) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @EnforceApplicationSpecification = ISNULL(@EnforceApplicationSpecification, 0);
	SET @EnforceBusinessSpecification = ISNULL(@EnforceBusinessSpecification, 0);
	SET @EnforceEntitySpecification = ISNULL(@EnforceEntitySpecification, 0);
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	SET @IsResultXml = ISNULL(@IsResultXml, 0);
	
		
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationID INT = NULL,
		@BusinessID BIGINT = NULL,
		@PersonID BIGINT = NULL,
		@MessageTypeIdArray VARCHAR(MAX) = comm.fnMessageTypeKeyTranslator(@MessageTypeKey, DEFAULT)


	--------------------------------------------------------------------------------------------
	-- Temporary Resources.
	--------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpMessageTypes') IS NOT NULL
	BEGIN
		DROP TABLE #tmpMessageTypes;
	END

	CREATE TABLE #tmpMessageTypes (
		MessageTypeConfigurationID BIGINT,
		ApplicationID INT,
		BusinessID BIGINT,
		EntityID BIGINT,
		MessageTypeID INT,
		Code VARCHAR(50),
		Name VARCHAR(50),
		Description VARCHAR(1000),
		rowguid UNIQUEIDENTIFIER,
		DateCreated DATETIME,
		DateModified DATETIME,
		CreatedBy VARCHAR(256),
		ModifiedBy VARCHAR(256),
		TotalRecordCount BIGINT
	);	

	--------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------
	-- Attempt to tranlate the BusinessKey object (using the system key translator) 
	-- into a BusinessID object using the supplied business key and key type.
	
	-- Translate system key variables using the system translator.
	EXEC api.spDbo_System_KeyTranslator
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@BusinessKeyType = @BusinessKeyType,
		@PersonKey = @PersonKey,
		@PersonTypeKey = @PersonTypeKey,
		@ApplicationID = @ApplicationID OUTPUT,
		@BusinessID = @BusinessID OUTPUT,
		@PersonID = @PersonID OUTPUT,
		@IsOutputOnly = 1	

	-- Debug
	--SELECT @ApplicationID AS AplicationID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
	--	@BusinessID AS BusinessID, @PersonKey AS PersonKey, @PersonTypeKey AS PersonTypeKey, @PersonID AS PersonID,
	--	@MessageTypeIdArray
		
		
	--------------------------------------------------------------------------------------------
	-- Paging
	--------------------------------------------------------------------------------------------			
	-- Support paging
	SET @Skip = ISNULL(@Skip, 0); -- if we didn't recieve a value then let's assign to 0
	SET @Take = ISNULL(@Take, 2147483647); -- if we didn't recieve a value then take as much as the int allows

	-- Create sorting collection values
	DECLARE @tblSortCollection AS TABLE (Idx INT, Value VARCHAR(20));
	
	-- Parse sort collection (only one item allowed right now.  Field|Direction is pipe delimited.
	-- Can be changed to be "Field,Direction|Field,Direction" like structure in the future.
	INSERT INTO @tblSortCollection (
		Idx,
		value
	)
	SELECT Idx, Value
	FROM dbo.fnSplit(@SortCollection, '|')
	
	-- Set sorting fields (Currently, Idx 1: Field; Idx 2: Direction
	SET @SortField = (SELECT Value FROM @tblSortCollection WHERE Idx = 1);
	SET @SortDirection = (SELECT Value FROM @tblSortCollection WHERE Idx = 2);
	

	--------------------------------------------------------------------------------------------
	-- Fetch results.
	-- <Summary>
	-- Uses "bubble up" functionality to determine results (i.e. looks at specific criteria,
	-- to least specific criteria).
	-- </Summary>
	--------------------------------------------------------------------------------------------
	IF @EnforceApplicationSpecification = 0 AND @EnforceBusinessSpecification = 0
		AND @EnforceEntitySpecification = 0
	BEGIN
			

		;WITH cteItems AS (
			SELECT
				CASE 
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeID ASC) 
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY MessageTypeID DESC)     
					WHEN @SortField = 'Code' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY Code ASC) 
					WHEN @SortField = 'Code' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY Code DESC) 
					WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY Name ASC) 
					WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY Name DESC)
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated ASC) 
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY DateCreated DESC)   
					ELSE ROW_NUMBER() OVER (ORDER BY Name ASC)
				END AS RowNumber,
				MessageTypeConfigurationID,
				ApplicationID,
				BusinessID,
				EntityID,
				MessageTypeID,
				Code,
				Name,
				Description,
				rowguid,
				DateCreated,
				DateModified,
				CreatedBy,
				ModifiedBy
			--SELECT *
			FROM comm.fnGetMessageTypeConfigurations(@ApplicationID, @BusinessID, @PersonID)
			WHERE ( @MessageTypeConfigurationKey IS NULL OR MessageTypeConfigurationID IN (
						SELECT Value FROM dbo.fnSplit(@MessageTypeConfigurationKey, ',') WHERE ISNUMERIC(Value) =1) 
				)
				AND (@MessageTypeKey IS NULL OR MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeIdArray, ',') WHERE ISNUMERIC(Value) = 1))
		)

		INSERT INTO #tmpMessageTypes (
			MessageTypeConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			Code,
			Name,
			Description,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy,
			TotalRecordCount
		)
		SELECT
			MessageTypeConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			Code,
			Name,
			Description,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy,
			(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
		FROM cteItems
		WHERE RowNumber > @Skip 
			AND RowNumber <= (@Skip + @Take)
		ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.

	END
	

	--------------------------------------------------------------------------------------------
	-- Fetch results.
	-- <Summary>
	-- Uses a more strict criterion based on the user's input.
	-- </Summary>
	--------------------------------------------------------------------------------------------	
	IF @EnforceApplicationSpecification = 1 OR @EnforceBusinessSpecification = 1
		OR @EnforceEntitySpecification = 1
	BEGIN

		;WITH cteItems AS (
			SELECT
				CASE 
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.MessageTypeID ASC) 
					WHEN @SortField IN ('MessageTypeID') AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY cfg.MessageTypeID DESC)     
					WHEN @SortField = 'Code' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Code ASC) 
					WHEN @SortField = 'Code' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Code DESC) 
					WHEN @SortField = 'Name' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Name ASC) 
					WHEN @SortField = 'Name' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY typ.Name DESC)
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'asc'  THEN ROW_NUMBER() OVER (ORDER BY typ.DateCreated ASC) 
					WHEN @SortField = 'DateCreated' AND @SortDirection = 'desc'  THEN ROW_NUMBER() OVER (ORDER BY typ.DateCreated DESC)   
					ELSE ROW_NUMBER() OVER (ORDER BY typ.Name ASC)
				END AS RowNumber,
				cfg.MessageTypeConfigurationID,
				cfg.ApplicationID,
				cfg.BusinessID,
				cfg.EntityID,
				cfg.MessageTypeID,
				typ.Code,
				typ.Name,
				typ.Description,
				cfg.rowguid,
				cfg.DateCreated,
				cfg.DateModified,
				cfg.CreatedBy,
				cfg.ModifiedBy
			--SELECT *
			FROM comm.MessageTypeConfiguration cfg (NOLOCK)
				JOIN comm.MessageType typ (NOLOCK)
					ON cfg.MessageTypeID = typ.MessageTypeID
				LEFT JOIN dbo.Application app (NOLOCK)
					ON cfg.ApplicationID = app.ApplicationID
			WHERE ( @MessageTypeConfigurationKey IS NULL OR cfg.MessageTypeConfigurationID IN (
						SELECT Value FROM dbo.fnSplit(@MessageTypeConfigurationKey, ',') WHERE ISNUMERIC(Value) =1) 
				)
				AND ( @EnforceApplicationSpecification = 0 OR cfg.ApplicationID = @ApplicationID )
				AND ( @EnforceBusinessSpecification = 0 OR cfg.BusinessID = @BusinessID )
				AND ( @EnforceEntitySpecification = 0 OR cfg.EntityID = @PersonID )
				AND (@MessageTypeKey IS NULL OR cfg.MessageTypeID IN (SELECT Value FROM dbo.fnSplit(@MessageTypeIdArray, ',') WHERE ISNUMERIC(Value) = 1))
		)

		INSERT INTO #tmpMessageTypes (
			MessageTypeConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			Code,
			Name,
			Description,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy,
			TotalRecordCount
		)
		SELECT
			MessageTypeConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			Code,
			Name,
			Description,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy,
			(SELECT COUNT(*) FROM cteItems) AS TotalRecordCount
		FROM cteItems
		WHERE RowNumber > @Skip 
			AND RowNumber <= (@Skip + @Take)
		ORDER BY RowNumber -- Performs sort.  Sort is handled in the CTE above.
			
	END



	--------------------------------------------------------------------------------------------
	-- Return results.
	-- <Summary>
	-- </Summary>
	--------------------------------------------------------------------------------------------	
	IF ISNULL(@IsResultXml, 0) = 0 AND ISNULL(@IsOutputOnly, 0) = 0
	BEGIN
	
		SELECT
			MessageTypeConfigurationID,
			ApplicationID,
			BusinessID,
			EntityID,
			MessageTypeID,
			Code,
			Name,
			Description,
			rowguid,
			DateCreated,
			DateModified,
			CreatedBy,
			ModifiedBy,
			TotalRecordCount
		FROM #tmpMessageTypes
	
	END
	ELSE 
	BEGIN
	
		SET @MessageTypeConfigurationData = (
			SELECT *
			FROM #tmpMessageTypes
			FOR XML PATH('MessageTypeConfiguration'), ROOT('MessageTypeConfigurations')
		);
		
		IF ISNULL(@IsOutputOnly, 0) = 0
		BEGIN
			SELECT @MessageTypeConfigurationData AS MessageTypeConfigurationData
		END
		
	END


	--------------------------------------------------------------------------------------------
	-- Release resources
	--------------------------------------------------------------------------------------------
	DROP TABLE #tmpMessageTypes;
	
		
END
