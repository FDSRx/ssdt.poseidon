﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/7/2014
-- Description:	Returns a list of InteractionType objects.
-- SAMPLE CALL: api.spDbo_InteractionType_Get_List
-- =============================================
CREATE PROCEDURE [api].[spDbo_InteractionType_Get_List]
	@InteractionTypeID VARCHAR(MAX) = NULL,
	@Code VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		InteractionTypeID,
		Code,
		Name,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy
	FROM dbo.InteractionType
	WHERE ( @InteractionTypeID IS NULL OR InteractionTypeID IN (SELECT Value FROM dbo.fnSplit(@InteractionTypeID, ',') WHERE ISNUMERIC(Value) = 1) )
		AND ( @Code IS NULL OR Code IN (SELECT Value FROM dbo.fnSplit(@Code, ',')) )
	ORDER BY Name
	
END
