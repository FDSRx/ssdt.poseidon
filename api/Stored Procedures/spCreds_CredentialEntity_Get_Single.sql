﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/3/2014
-- Description:	Returns a single credential (user) based on the specified criteria.
-- SAMPLE CALL:
/*
api.spCreds_CredentialEntity_Get_Single
	@ApplicationKey = '2',
	@BusinessKey = '10684',
	@CredentialKey = '801050'

api.spCreds_CredentialEntity_Get_Single
	@ApplicationKey = '8',
	@BusinessKey = '-1',
	@CredentialKey = 'A0BB064A-FB70-4DC0-ABEF-CA30DE3E274C',
	@EnforceApplicationSpecification = 1,
	@EnforceBusinessSpecification = 1,
	@EnforceCredentialSID = 1
*/

-- SELECT * FROM creds.vwExtranetUser
-- SELECT * FROM creds.CredentialToken ORDER BY CredentialTokenID DESC
-- SELECT * FROM creds.CredentialEntityApplication
-- SELECT * FROM dbo.Application
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [api].[spCreds_CredentialEntity_Get_Single]
	@ApplicationKey VARCHAR(MAX) = NULL,
	@BusinessKey VARCHAR(MAX) = NULL,
	@BusinessKeyType VARCHAR(25) = NULL,
	@CredentialKey VARCHAR(MAX) = NULL,
	@Username VARCHAR(256) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@Skip BIGINT = NULL,
	@Take BIGINT = NULL,
	@SortCollection VARCHAR(MAX) = NULL,
	@EnforceBusinessSpecification BIT = NULL,
	@EnforceApplicationSpecification BIT = NULL,
	@EnforceCredentialSID BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@CredentialKey=' + dbo.fnToStringOrEmpty(@CredentialKey) + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName) + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName) + ';' +
		'@Skip=' + dbo.fnToStringOrEmpty(@Skip) + ';' +
		'@Take=' + dbo.fnToStringOrEmpty(@Take) + ';' +
		'@SortCollection=' + dbo.fnToStringOrEmpty(@SortCollection) + ';' +
		'@EnforceBusinessSpecification=' + dbo.fnToStringOrEmpty(@EnforceBusinessSpecification) + ';' +
		'@EnforceApplicationSpecification=' + dbo.fnToStringOrEmpty(@EnforceApplicationSpecification) + ';' +
		'@EnforceCredentialSID=' + dbo.fnToStringOrEmpty(@EnforceCredentialSID) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @BusinessKeyType = ISNULL(@BusinessKeyType, NULLIF(@BusinessKeyType, ''));
	SET @EnforceBusinessSpecification = ISNULL(@EnforceBusinessSpecification, 1);
	SET @EnforceApplicationSpecification = ISNULL(@EnforceApplicationSpecification, 1);
	SET @EnforceCredentialSID = ISNULL(@EnforceCredentialSID, 0);
	
	--------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------
	DECLARE 
		@SortField VARCHAR(50),
		@SortDirection VARCHAR(10),
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = NULL,
		@CredentialEntityID BIGINT = NULL
	
	--------------------------------------------------------------------------------------------
	-- Interrogate credential information
	-- <Summary>
	-- If the call requires the credential information to be in its token form then we need to
	-- do a token validation.  Otherwise, someone has the appropriate permissions to pull the data.
	-- <Summary>
	--------------------------------------------------------------------------------------------
	-- If a credential token is required and the key is not in token (GUID) format
	-- then nullify the Id and exit the process.
	IF @EnforceCredentialSID = 1 AND dbo.fnIsUniqueIdentifier(@CredentialKey) = 0
	BEGIN
		SET @CredentialEntityID = NULL;	
	END
	ELSE
	BEGIN
	
		-- If the key is a unique identifier then set as the CredentialToken
		-- and attempt an update on said token.
		IF dbo.fnIsUniqueIdentifier(@CredentialKey) = 1
		BEGIN
				
			--------------------------------------------------------------------------------------------
			-- Validate token
			--------------------------------------------------------------------------------------------	
			EXEC creds.spCredentialToken_Update 
				@Token = @CredentialKey OUTPUT,
				@CredentialEntityID = @CredentialEntityID OUTPUT
			
		END
		
		-- If a CredentialEntityID was not resolved
		-- and the key is numeric, then assume it is a CredentialEntityID.
		IF ( @CredentialEntityID IS NULL ) 
			AND ISNUMERIC(@CredentialKey) = 1
		BEGIN
			SET @CredentialEntityID = @CredentialKey;
		END
	
	END
	
	-- Debug
	--SELECT @ApplicationKey AS ApplicationKey, @BusinessKey AS BusinessKey, @CredentialEntityID AS CredentialEntityID			
		
	--------------------------------------------------------------------------------------------
	-- Fetch credential (user) data.
	--------------------------------------------------------------------------------------------
	IF @CredentialEntityID IS NOT NULL
	BEGIN
	
		EXEC api.spCreds_CredentialEntity_Get_List
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessKey,
			@BusinessKeyType = @BusinessKeyType,
			@CredentialKey = @CredentialEntityID,
			@Take = 1,
			@EnforceApplicationSpecification = @EnforceApplicationSpecification,
			@EnforceBusinessSpecification = @EnforceBusinessSpecification
			
	END


	
	
END
