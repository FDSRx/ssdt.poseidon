﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Gets the Task object.
-- Change log:
-- 6/17/2016 - dhughes - Modified the procedure to accept the Application and Business object parameters.

-- SAMPLE CALL:
/*

DECLARE
	@NoteID BIGINT = 4,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT = NULL,
	@BusinessEntityID BIGINT = 135590,
	@Debug BIT = 1

EXEC api.spDbo_Task_Get_Single
	@NoteID = @NoteID,
	@ApplicationKey = @ApplicationKey,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@ScopeID = @ScopeID,
	@BusinessEntityID = @BusinessEntityID,
	@Debug = @Debug

	
*/
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Task

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [api].[spDbo_Task_Get_Single]
	@NoteID BIGINT,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT = NULL,
	@BusinessEntityID BIGINT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID)  + ';' +
		'@ApplicationKey=' + dbo.fnToStringOrEmpty(@ApplicationKey)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID)  + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;


	-----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------------------
	IF @NoteID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to retrieve Task object. Object reference (@NoteID) is not set to an instance of an object. ' +
			'The @NoteID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	

	-----------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@BusinessID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType),
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey)

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Deubber On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeType,
			@BusinessID AS BusinessID
	END	
		
	-----------------------------------------------------------------------------------------------------------------------------
	-- Retrieve Task object
	-----------------------------------------------------------------------------------------------------------------------------
	EXEC api.spDbo_Task_Get_List
		@NoteKey = @NoteID,
		@ScopeKey = @ScopeID,
		@BusinessEntityKey = @BusinessEntityID,
		@Debug = @Debug
	
			
END
