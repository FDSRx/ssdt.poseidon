﻿CREATE TABLE [secur].[KeyringDirection] (
    [KeyringDirectionID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]               VARCHAR (25)     NOT NULL,
    [Name]               VARCHAR (50)     NOT NULL,
    [Description]        VARCHAR (1000)   NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_KeyringDirection_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_KeyringDirection_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_KeyringDirection_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_KeyringDirection] PRIMARY KEY CLUSTERED ([KeyringDirectionID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_KeyringDirection_Code]
    ON [secur].[KeyringDirection]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_KeyringDirection_Name]
    ON [secur].[KeyringDirection]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_KeyringDirection_rowguid]
    ON [secur].[KeyringDirection]([rowguid] ASC);

