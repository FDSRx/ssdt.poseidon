﻿CREATE TABLE [secur].[EncryptionType] (
    [EncryptionTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]             VARCHAR (25)     NOT NULL,
    [Name]             VARCHAR (50)     NOT NULL,
    [Description]      VARCHAR (1000)   NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_EncryptionType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_EncryptionType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_EncryptionType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_EncryptionType] PRIMARY KEY CLUSTERED ([EncryptionTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EncryptionType_Code]
    ON [secur].[EncryptionType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EncryptionType_Name]
    ON [secur].[EncryptionType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EncryptionType_rowguid]
    ON [secur].[EncryptionType]([rowguid] ASC);

