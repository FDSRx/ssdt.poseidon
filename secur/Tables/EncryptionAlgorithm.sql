﻿CREATE TABLE [secur].[EncryptionAlgorithm] (
    [EncryptionAlgorithmID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]                  VARCHAR (25)     NOT NULL,
    [Name]                  VARCHAR (256)    NOT NULL,
    [Description]           VARCHAR (1000)   NULL,
    [rowguid]               UNIQUEIDENTIFIER CONSTRAINT [DF_EncryptionAlgorithm_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]           DATETIME         CONSTRAINT [DF_EncryptionAlgorithm_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]          DATETIME         CONSTRAINT [DF_EncryptionAlgorithm_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             VARCHAR (256)    NULL,
    [ModifiedBy]            VARCHAR (256)    NULL,
    CONSTRAINT [PK_EncryptionAlgorithm] PRIMARY KEY CLUSTERED ([EncryptionAlgorithmID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EncryptionAlgorithm_Code]
    ON [secur].[EncryptionAlgorithm]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EncryptionAlgorithm_Name]
    ON [secur].[EncryptionAlgorithm]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EncryptionAlgorithm_rowguid]
    ON [secur].[EncryptionAlgorithm]([rowguid] ASC);

