﻿CREATE TABLE [secur].[AsymmetricKey] (
    [AsymmetricKeyID]       BIGINT           IDENTITY (1, 1) NOT NULL,
    [EncryptionAlgorithmID] INT              NOT NULL,
    [Name]                  VARCHAR (256)    NOT NULL,
    [PublicKey]             VARCHAR (4096)   NULL,
    [PrivateKey]            VARCHAR (4096)   NULL,
    [KeySize]               INT              NULL,
    [Description]           VARCHAR (1000)   NULL,
    [rowguid]               UNIQUEIDENTIFIER CONSTRAINT [DF_AsymmetricKey_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]           DATETIME         CONSTRAINT [DF_AsymmetricKey_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]          DATETIME         CONSTRAINT [DF_AsymmetricKey_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             VARCHAR (256)    NULL,
    [ModifiedBy]            VARCHAR (256)    NULL,
    CONSTRAINT [PK_AsymmetricKey] PRIMARY KEY CLUSTERED ([AsymmetricKeyID] ASC),
    CONSTRAINT [FK_AsymmetricKey_EncryptionAlgorithm] FOREIGN KEY ([EncryptionAlgorithmID]) REFERENCES [secur].[EncryptionAlgorithm] ([EncryptionAlgorithmID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AsymmetricKey_rowguid]
    ON [secur].[AsymmetricKey]([rowguid] ASC);

