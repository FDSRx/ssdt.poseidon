﻿CREATE TABLE [secur].[SymmetricKey] (
    [SymmetricKeyID]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [EncryptionAlgorithmID] INT              NOT NULL,
    [Name]                  VARCHAR (256)    NOT NULL,
    [PrivateKey]            VARCHAR (4096)   NULL,
    [KeySize]               INT              NULL,
    [Description]           VARCHAR (1000)   NULL,
    [rowguid]               UNIQUEIDENTIFIER CONSTRAINT [DF_SymmetricKey_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]           DATETIME         CONSTRAINT [DF_SymmetricKey_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]          DATETIME         CONSTRAINT [DF_SymmetricKey_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             VARCHAR (256)    NULL,
    [ModifiedBy]            VARCHAR (256)    NULL,
    CONSTRAINT [PK_SymmetricKey] PRIMARY KEY CLUSTERED ([SymmetricKeyID] ASC),
    CONSTRAINT [FK_SymmetricKey_EncryptionAlgorithm] FOREIGN KEY ([EncryptionAlgorithmID]) REFERENCES [secur].[EncryptionAlgorithm] ([EncryptionAlgorithmID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_SymmetricKey_rowguid]
    ON [secur].[SymmetricKey]([rowguid] ASC);

