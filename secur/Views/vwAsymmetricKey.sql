﻿CREATE VIEW secur.vwAsymmetricKey AS
SELECT
	akey.AsymmetricKeyID,
	akey.EncryptionAlgorithmID,
	alg.Code AS EncryptionAlgorithmCode,
	alg.Name AS EncryptionAlgorithmName,
	akey.Name AS Name,
	akey.PublicKey,
	akey.PrivateKey,
	akey.KeySize,
	akey.Description,
	akey.rowguid,
	akey.DateCreated,
	akey.DateModified,
	akey.CreatedBy,
	akey.ModifiedBy
--SELECT *
FROM secur.AsymmetricKey akey
	JOIN secur.EncryptionAlgorithm alg
		ON alg.EncryptionAlgorithmID = akey.EncryptionAlgorithmID