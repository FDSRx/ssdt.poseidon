﻿CREATE VIEW secur.vwSymmetricKey AS
SELECT
	skey.SymmetricKeyID,
	skey.EncryptionAlgorithmID,
	alg.Code AS EncryptionAlgorithmCode,
	alg.Name AS EncryptionAlgorithmName,
	skey.Name AS Name,
	skey.PrivateKey,
	skey.KeySize,
	skey.Description,
	skey.rowguid,
	skey.DateCreated,
	skey.DateModified,
	skey.CreatedBy,
	skey.ModifiedBy
--SELECT *
FROM secur.SymmetricKey skey
	JOIN secur.EncryptionAlgorithm alg
		ON alg.EncryptionAlgorithmID = skey.EncryptionAlgorithmID
