﻿CREATE TABLE [cache].[Cache] (
    [CacheID]       BIGINT           IDENTITY (1, 1) NOT NULL,
    [CacheObjectID] INT              NOT NULL,
    [KeyName]       VARCHAR (256)    NOT NULL,
    [KeyValue]      VARCHAR (MAX)    NULL,
    [DataTypeID]    INT              NULL,
    [Arguments]     VARCHAR (MAX)    NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_Cache_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_Cache_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_Cache_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_Cache] PRIMARY KEY CLUSTERED ([CacheID] ASC),
    CONSTRAINT [FK_Cache_CacheObject] FOREIGN KEY ([CacheObjectID]) REFERENCES [cache].[CacheObject] ([CacheObjectID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Cache_rowguid]
    ON [cache].[Cache]([rowguid] ASC);

