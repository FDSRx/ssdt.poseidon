﻿CREATE TABLE [cache].[CacheObject] (
    [CacheObjectID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]          VARCHAR (50)     NOT NULL,
    [Name]          VARCHAR (256)    NOT NULL,
    [Namespace]     VARCHAR (256)    NULL,
    [Description]   VARCHAR (1000)   NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_CacheObject_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_CacheObject_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_CacheObject_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_CacheObject] PRIMARY KEY CLUSTERED ([CacheObjectID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CacheObject_Code]
    ON [cache].[CacheObject]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CacheObject_Name]
    ON [cache].[CacheObject]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CacheObject_rowguid]
    ON [cache].[CacheObject]([rowguid] ASC);

