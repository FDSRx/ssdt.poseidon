﻿CREATE TABLE [cache].[CacheObjectMetadata] (
    [CacheObjectMetadataID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [CacheObjectID]         INT              NOT NULL,
    [ColumnName]            VARCHAR (256)    NOT NULL,
    [DataTypeID]            INT              NOT NULL,
    [Description]           VARCHAR (1000)   NULL,
    [rowguid]               UNIQUEIDENTIFIER CONSTRAINT [DF_CacheObjectMetadata_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]           DATETIME         CONSTRAINT [DF_CacheObjectMetadata_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]          DATETIME         CONSTRAINT [DF_CacheObjectMetadata_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             VARCHAR (256)    NULL,
    [ModifiedBy]            VARCHAR (256)    NULL,
    CONSTRAINT [PK_CacheObjectMetadata] PRIMARY KEY CLUSTERED ([CacheObjectMetadataID] ASC),
    CONSTRAINT [FK_CacheObjectMetadata_CacheObject] FOREIGN KEY ([CacheObjectID]) REFERENCES [cache].[CacheObject] ([CacheObjectID])
);

