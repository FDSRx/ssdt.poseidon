﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/7/2015
-- Description:	Creates a new BatchExecution object.
-- SAMPLE CALL:
/*

DECLARE
	@BatchEntityID BIGINT = NULL,
	@TransactionDate DATETIME = GETDATE(),
	@ExecutionStatusID INT = etl.fnGetExecutionStatusID('CR'),
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC etl.spBatchExecution_Create
	@BatchEntityID = @BatchEntityID OUTPUT,
	@DateStart = @TransactionDate,
	@DateEnd = NULL,
	@ExecutionStatusID = @ExecutionStatusID,
	@ExitCode = NULL,
	@ExitMessage = NULL,
	@Result = NULL,
	@Message = NULL,
	@Arguments = NULL,
	@CreatedBy = @CreatedBy

SELECT @BatchEntityID AS BatchEntityID

*/

-- SELECT * FROM etl.BatchExecution
-- SELECT * FROM etl.BatchJobExecution
-- SELECT * FROM etl.BatchJobInstance
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- SELECT * FROM etl.ExecutionStatus
-- =============================================
CREATE PROCEDURE [etl].[spBatchExecution_Create]
	@BatchEntityID BIGINT = NULL OUTPUT, -- optional parameter. Object will be created it not provided.
	@BatchEntityTypeID INT = NULL,
	@BatchEntityTypeCode VARCHAR(25) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@ExecutionStatusID INT = NULL,
	@ExecutionStatusCode VARCHAR(25) = NULL,
	@ExitCode VARCHAR(256) = NULL,
	@ExitMessage VARCHAR(MAX) = NULL,
	@Result VARCHAR(MAX) = NULL,
	@Message VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@Parameters VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchEntityID=' + dbo.fnToStringOrEmpty(@BatchEntityID) + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart) + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd) + ';' +
		'@ExecutionStatusID=' + dbo.fnToStringOrEmpty(@ExecutionStatusID) + ';' +
		'@ExecutionStatusCode=' + dbo.fnToStringOrEmpty(@ExecutionStatusCode) + ';' +
		'@ExitCode=' + dbo.fnToStringOrEmpty(@ExitCode) + ';' +
		'@ExitMessage=' + dbo.fnToStringOrEmpty(@ExitMessage) + ';' +
		'@Result=' + dbo.fnToStringOrEmpty(@Result) + ';' +
		'@Message=' + dbo.fnToStringOrEmpty(@Message) + ';' +
		'@Arguments=' + dbo.fnToStringOrEmpty(@Arguments) + ';' +
		'@Parameters=' + dbo.fnToStringOrEmpty(@Parameters) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @Arguments = ISNULL(@Arguments, @Parameters);

	--------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------
	-- No declarations.

	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	-- Do nothing.

	--------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------

		--------------------------------------------------------------------------------------------------------
		-- Set variables.
		--------------------------------------------------------------------------------------------------------
		-- Create a new BatchEntity object if one was not provided.
		IF @BatchEntityID IS NULL
		BEGIN
			-- Retrieve a type of execution entity.
			IF @BatchEntityTypeID IS NULL AND @BatchEntityTypeCode IS NOT NULL
			BEGIN
				SET @BatchEntityTypeID = etl.fnGetBatchEntityTypeID(@BatchEntityTypeCode);
			END

			-- Set entity to unknown if unable to determine execution type.
			SET @BatchEntityTypeID = ISNULL(@BatchEntityTypeID, 'UNKNEXEC');

			EXEC etl.spBatchEntity_Create
				@BatchEntityTypeID = @BatchEntityTypeID,
				@CreatedBy = @CreatedBy,
				@BatchEntityID = @BatchEntityID OUTPUT
		END

		-- Retrieve an ExecutionStatus object if one is not provided.
		IF @ExecutionStatusID IS NULL AND @ExecutionStatusCode IS NOT NULL
		BEGIN
			SET @ExecutionStatusID = etl.fnGetExecutionStatusID(@ExecutionStatusCode);
		END

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Setting Variables' AS Method,
				@DateStart AS DateStart, @DateEnd AS DateEnd, @BatchEntityID AS BatchEntityID,
				@ExecutionStatusID AS ExecutionStatusID, @ExitCode AS ExitCode, @ExitMessage AS ExitMessage,
				@Result AS Result, @Message AS Message, @Arguments AS Arguments, @Parameters AS Params
		END

		--------------------------------------------------------------------------------------------------------
		-- Create object.
		--------------------------------------------------------------------------------------------------------
		INSERT INTO etl.BatchExecution(
			BatchEntityID,
			DateStart,
			DateEnd,
			ExecutionStatusID,
			ExitCode,
			ExitMessage,
			Result,
			Message,
			Arguments,
			CreatedBy
		)
		SELECT
			@BatchEntityID AS BatchEntityID,
			@DateStart AS DateStart,
			@DateEnd AS DateEnd,
			@ExecutionStatusID AS ExecutionStatusID,
			@ExitCode AS ExitCode,
			@ExitMessage AS ExitMessage,
			@Result AS Result,
			@Message AS Message,
			@Arguments AS Arguments,
			@CreatedBy AS CreatedBy

		--------------------------------------------------------------------------------------------------------
		-- Create object parameters.
		--------------------------------------------------------------------------------------------------------
		EXEC etl.spBatchExecutionParam_ParseAndCreate
			@BatchExecutionID = @BatchEntityID,
			@ParamString = @Parameters,
			@CreatedBy = @CreatedBy

 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH


END

