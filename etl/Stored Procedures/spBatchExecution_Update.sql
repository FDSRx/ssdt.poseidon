﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/11/2015
-- Description:	Updates a BatchExecution object.
-- SAMPLE CALL:
/*

DECLARE
	@BatchEntityID BIGINT = 22,
	@TransactionDate DATETIME = GETDATE(),
	@ExecutionStatusID INT = etl.fnGetExecutionStatusID('S'),
	@CreatedBy VARCHAR(256) = 'dhughes',
	@ModifiedBy VARCHAR(256) = 'dhughes'

EXEC etl.spBatchExecution_Update
	@BatchEntityID = @BatchEntityID OUTPUT,
	@DateEnd = @TransactionDate,
	@ExecutionStatusID = @ExecutionStatusID,
	@ExitCode = NULL,
	@ExitMessage = NULL,
	@Result = NULL,
	@Message = NULL,
	@ModifiedBy = @ModifiedBy,
	@Debug = 1

SELECT @BatchEntityID AS BatchEntityID

*/

-- SELECT * FROM etl.BatchExecution
-- SELECT * FROM etl.BatchJobExecution
-- SELECT * FROM etl.BatchJobInstance
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- SELECT * FROM etl.ExecutionStatus
-- =============================================
CREATE PROCEDURE [etl].[spBatchExecution_Update]
	@BatchEntityID BIGINT = NULL OUTPUT,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@ExecutionStatusID INT = NULL,
	@ExecutionStatusCode VARCHAR(25) = NULL,
	@ExitCode VARCHAR(256) = NULL,
	@ExitMessage VARCHAR(MAX) = NULL,
	@Result VARCHAR(MAX) = NULL,
	@Message VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@Parameters VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchEntityID=' + dbo.fnToStringOrEmpty(@BatchEntityID) + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart) + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd) + ';' +
		'@ExecutionStatusID=' + dbo.fnToStringOrEmpty(@ExecutionStatusID) + ';' +
		'@ExecutionStatusCode=' + dbo.fnToStringOrEmpty(@ExecutionStatusCode) + ';' +
		'@ExitCode=' + dbo.fnToStringOrEmpty(@ExitCode) + ';' +
		'@ExitMessage=' + dbo.fnToStringOrEmpty(@ExitMessage) + ';' +
		'@Result=' + dbo.fnToStringOrEmpty(@Result) + ';' +
		'@Message=' + dbo.fnToStringOrEmpty(@Message) + ';' +
		'@Arguments=' + dbo.fnToStringOrEmpty(@Arguments) + ';' +
		'@Parameters=' + dbo.fnToStringOrEmpty(@Parameters) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);

	--------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------
	-- No declarations.

	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	IF @BatchEntityID IS NULL
	BEGIN
		RETURN;
	END


	--------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	BEGIN TRY
	--SET @Trancount = @@TRANCOUNT;
	--IF @Trancount = 0
	--BEGIN
	--	BEGIN TRANSACTION;
	--END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------

		--------------------------------------------------------------------------------------------------------
		-- Set variables.
		--------------------------------------------------------------------------------------------------------
		-- Retrieve an ExecutionStatus object by code if one is not provided.
		IF @ExecutionStatusID IS NULL AND @ExecutionStatusCode IS NOT NULL
		BEGIN
			SET @ExecutionStatusID = etl.fnGetExecutionStatusID(@ExecutionStatusCode);
		END

		-- Data review
		-- SELECT * FROM etl.ExecutionStatus

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Setting Variables' AS Method,
				@DateEnd AS DateEnd, @BatchEntityID AS BatchEntityID,				 
				@DateStart AS DateStart, @DateEnd AS DateEnd, @ExecutionStatusID AS ExecutionStatusID, 
				@ExitCode AS ExitCode, @ExitMessage AS ExitMessage,	@Result AS Result, 
				@Message AS Message, @Arguments AS Arguments, @Parameters AS Params
		END

		--------------------------------------------------------------------------------------------------------
		-- Update object.
		--------------------------------------------------------------------------------------------------------
		UPDATE be
		SET
			be.DateStart = CASE WHEN @DateStart IS NULL THEN be.DateStart ELSE @DateStart END,
			be.DateEnd = CASE WHEN @DateEnd IS NULL THEN be.DateEnd ELSE @DateEnd END,
			be.ExecutionStatusID = CASE WHEN @ExecutionStatusID IS NULL THEN be.ExecutionStatusID ELSE @ExecutionStatusID END,
			be.ExitCode = CASE WHEN @ExitCode IS NULL THEN be.ExitCode ELSE @ExitCode END,
			be.ExitMessage = CASE WHEN @ExitMessage IS NULL THEN be.ExitMessage ELSE @ExitMessage END,
			be.Result = CASE WHEN @Result IS NULL THEN be.Result ELSE @Result END,
			be.Message = CASE WHEN @Message IS NULL THEN be.Message ELSE @Message END,
			be.DateModified = GETDATE(),
			be.ModifiedBy = CASE WHEN @ModifiedBy IS NULL THEN be.ModifiedBy ELSE @ModifiedBy END
		FROM etl.BatchExecution be
		WHERE BatchEntityID = @BatchEntityID


 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	--IF @Trancount = 0
	--BEGIN
	--	COMMIT TRANSACTION;
	--END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		--IF @Trancount = 0 AND @@TRANCOUNT > 0
		--BEGIN
		--	ROLLBACK TRANSACTION;
		--END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH


END

