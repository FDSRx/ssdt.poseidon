﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/12/2015
-- Description:	Creates a new BatchJobExecutionBusiness object.
-- SAMPLE CALL:
/*

DECLARE
	@BatchJobExecutionID BIGINT = NULL,
	@BatchJobExecutionParentID BIGINT = NULL,
	@BatchJobInstanceID BIGINT = 27,
	@TransactionDate DATETIME = GETDATE(),
	@ExecutionStatusID INT = etl.fnGetExecutionStatusID('CR'),
	@BusinessKey VARCHAR(50) = '2501624',
	@BusinessKeyType VARCHAR(50) = 'NABP',
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC etl.spBatchJobExecutionBusiness_Create
	@BatchJobExecutionID = @BatchJobExecutionID OUTPUT,
	@BatchJobExecutionParentID = @BatchJobExecutionParentID,
	@BatchJobInstanceID = @BatchJobInstanceID,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@DateStart = @TransactionDate,
	@DateEnd = NULL,
	@ExecutionStatusID = @ExecutionStatusID,
	@ExitCode = NULL,
	@ExitMessage = NULL,
	@Result = NULL,
	@Message = NULL,
	@Arguments = '@BusinessKey=2501624;@BusinessKeyType=Nabp',
	@Parameters = 'BusinessKey||2501624*||*BusinesKeyType||Nabp',
	@CreatedBy = @CreatedBy,
	@Debug = 1

SELECT @BatchJobExecutionID AS BatchJobExecutionID

-- ROLLBACK TRANSACTION;
*/

-- SELECT * FROM etl.BatchJobExecutionBusiness
-- SELECT * FROM etl.BatchExecution
-- SELECT * FROM etl.BatchJobExecution
-- SELECT * FROM etl.BatchExecutionParam
-- SELECT * FROM etl.BatchJobStep
-- SELECT * FROM etl.BatchJobInstance
-- SELECT * FROM etl.BatchStepInstance
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- SELECT * FROM etl.ExecutionStatus
-- =============================================
CREATE PROCEDURE [etl].[spBatchJobExecutionBusiness_Create]
	@BatchJobExecutionID BIGINT = NULL OUTPUT,
	@BatchJobExecutionParentID BIGINT,
	@BatchJobInstanceID BIGINT = NULL,
	@BatchJobInstanceCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(100) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@Notes VARCHAR(1000) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@ExecutionStatusID INT = NULL,
	@ExecutionStatusCode VARCHAR(25) = NULL,
	@ExitCode VARCHAR(256) = NULL,
	@ExitMessage VARCHAR(MAX) = NULL,
	@Result VARCHAR(MAX) = NULL,
	@Message VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@Parameters VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchJobExecutionID=' + dbo.fnToStringOrEmpty(@BatchJobExecutionID) + ';' +
		'@BatchJobExecutionParentID=' + dbo.fnToStringOrEmpty(@BatchJobExecutionParentID) + ';' +
		'@BatchJobInstanceID=' + dbo.fnToStringOrEmpty(@BatchJobInstanceID) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@Notes=' + dbo.fnToStringOrEmpty(@Notes) + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart) + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd) + ';' +
		'@ExecutionStatusID=' + dbo.fnToStringOrEmpty(@ExecutionStatusID) + ';' +
		'@ExitCode=' + dbo.fnToStringOrEmpty(@ExitCode) + ';' +
		'@ExitMessage=' + dbo.fnToStringOrEmpty(@ExitMessage) + ';' +
		'@Result=' + dbo.fnToStringOrEmpty(@Result) + ';' +
		'@Message=' + dbo.fnToStringOrEmpty(@Message) + ';' +
		'@Arguments=' + dbo.fnToStringOrEmpty(@Arguments) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);

	--------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@BatchExecutionExists BIT = NULL

	/*
	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	-- @BatchJobExecutionParentID
	IF @BatchJobExecutionParentID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @BatchJobExecutionParentID cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/

	--------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------

		--------------------------------------------------------------------------------------------------------
		-- Set variables.
		--------------------------------------------------------------------------------------------------------
		-- Data review.
		-- SELECT * FROM etl.BatchEntityType

		-- Create a new BatchEntity object if one was not provided.
		IF @BatchJobExecutionID IS NULL
		BEGIN
			EXEC etl.spBatchEntity_Create
				@BatchEntityTypeCode = 'JOBEXEC',
				@CreatedBy = @CreatedBy,
				@BatchEntityID = @BatchJobExecutionID OUTPUT
		END

		-- Retrieve an ExecutionStatus object if one is not provided.
		IF @ExecutionStatusID IS NULL AND @ExecutionStatusCode IS NOT NULL
		BEGIN
			SET @ExecutionStatusID = etl.fnGetExecutionStatusID(@ExecutionStatusCode);
		END

		-- Data review
		-- SELECT * FROM etl.ExecutionStatus

		-- If unable to retrieve an ExecutionStatus object, then defualt to "Created."
		SET @ExecutionStatusID = ISNULL(@ExecutionStatusID, etl.fnGetExecutionStatusID('CR'));

		-- If a parent object was not provided then make the local object the parent object.
		SET @BatchJobExecutionParentID = ISNULL(@BatchJobExecutionParentID, @BatchJobExecutionID);

		-- Translate the Business object if it was not provided.
		SET @BusinessID = ISNULL(@BusinessID, dbo.fnBusinessKeyTranslator(@BusinessKey, @BusinessKeyType));

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Setting Variables' AS Method,
				@BatchJobExecutionID AS BatchJobExecutionID, @BatchJobExecutionParentID AS BatchJobExecutionParentID,
				@BatchJobInstanceID AS BatchJobInstanceID, @DateStart AS DateStart, 
				@DateEnd AS DateEnd, @ExecutionStatusID AS ExecutionStatusID, 
				@ExitCode AS ExitCode, @ExitMessage AS ExitMessage,	@Result AS Result, 
				@Message AS Message, @Arguments AS Arguments, @Parameters AS Params
		END

		--------------------------------------------------------------------------------------------------------
		-- Create object.
		--------------------------------------------------------------------------------------------------------
		-- Data review.
		-- SELECT TOP 1 * FROM etl.BatchExecution
		-- SELECT TOP 1 * FROM etl.BatchJobExecution

		-- Create base object.
		EXEC etl.spBatchJobExecution_Create
			@BatchExecutionID = @BatchJobExecutionID,
			@BatchJobInstanceID = @BatchJobInstanceID,
			@BatchJobInstanceCode = @BatchJobInstanceCode,
			@DateStart = @DateStart,
			@DateEnd = @DateEnd,
			@ExecutionStatusID = @ExecutionStatusID,
			@ExitCode = @ExitCode,
			@ExitMessage = @ExitMessage,
			@Result = @Result,
			@Message = @Message,
			@Arguments = @Arguments,
			@Parameters = @Parameters,
			@CreatedBy = @CreatedBy,
			@Debug = @Debug

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Create BatchExecution object' AS Method,
				@BatchJobExecutionID AS BatchJobExecutionID, @BatchJobExecutionParentID AS BatchJobExecutionParentID,
				@ExecutionStatusID AS ExecutionStatusID
		END

		-- Debug
		-- SELECT * FROM etl.BatchExecution WHERE BatchEntityID = @BatchExecutionID
		-- SELECT * FROM etl.BatchJobExecution WHERE BatchExecutionID = @BatchExecutionID

		-- Data review
		-- SELECT TOP 1 * FROM etl.BatchJobExecutionBusiness

		-- Create a new BatchJobExecutionBusiness object.
		INSERT INTO etl.BatchJobExecutionBusiness (
			BatchJobExecutionID,
			BatchJobExecutionParentID,
			BusinessID,
			BusinessKey,
			BusinessKeyType,
			Notes,
			CreatedBy
		)
		SELECT
			@BatchJobExecutionID AS BatchJobExecutionID,
			@BatchJobExecutionParentID AS BatchJobExecutionParentID,
			@BusinessID AS BusinessID,
			@BusinessKey AS BusinessKey,
			@BusinessKeyType AS BusinessKeyType,
			@Notes AS Notes,
			@CreatedBy AS CreatedBy

 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH


END

