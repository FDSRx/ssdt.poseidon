﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/10/2015
-- Description:	Creates/Updates a BatchStepInstance object.
-- SAMPLE CALL:
/*

DECLARE
	@BatchStepInstanceID BIGINT = NULL,
	@Code VARCHAR(25) = 'MSDL',
	@Name VARCHAR(256) = 'Med. Sync Data Load',
	@CreatedBy VARCHAR(256) = 'dhughes',
	@Exists BIT = NULL

EXEC etl.spBatchStepInstance_Merge
	@BatchStepInstanceID = @BatchStepInstanceID OUTPUT,
	@Code = @Code,
	@Name = @Name,
	@CreatedBy = @CreatedBy,
	@Exists = @Exists OUTPUT

SELECT @BatchStepInstanceID AS BatchStepInstanceID, @Exists AS IsFound

*/

-- SELECT * FROM etl.BatchStepInstance
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- =============================================
CREATE PROCEDURE [etl].[spBatchStepInstance_Merge]
	@BatchStepInstanceID BIGINT = NULL OUTPUT,
	@Code VARCHAR(25) = NULL OUTPUT,
	@Name VARCHAR(256) = NULL OUTPUT,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchStepInstanceID=' + dbo.fnToStringOrEmpty(@BatchStepInstanceID) + ';' +
		'@Code=' + dbo.fnToStringOrEmpty(@Code) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@Version) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	-- Do nothing.


	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	-- Do nothing.

	--------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------
	-- No declarations.

	--------------------------------------------------------------------------------------------------------
	-- Set variables.
	--------------------------------------------------------------------------------------------------------
	-- Do nothing.

	--------------------------------------------------------------------------------------------------------
	-- Validate object existence.
	--------------------------------------------------------------------------------------------------------
	EXEC etl.spBatchStepInstance_Exists
		@BatchStepInstanceID = @BatchStepInstanceID OUTPUT,
		@Code = @Code,
		@Name = @Name,
		@Exists = @Exists OUTPUT

	--------------------------------------------------------------------------------------------------------
	-- Merge object.
	--------------------------------------------------------------------------------------------------------
	IF ISNULL(@Exists, 0) = 0
	BEGIN
		-- Object does not exists, let's create.
		EXEC etl.spBatchStepInstance_Create
			@BatchStepInstanceID = @BatchStepInstanceID,
			@Code = @Code,
			@Name = @Name,
			@CreatedBy = @CreatedBy

	END
	ELSE
	BEGIN
		SET @Exists = 1; -- <-- Used a a place holder.
	END


	





END

