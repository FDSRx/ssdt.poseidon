﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/10/2015
-- Description:	Creates a new BatchJobStep object.
-- SAMPLE CALL:
/*

DECLARE
	@BatchJobStepID BIGINT = NULL,
	@BatchJobInstanceID BIGINT = 1,
	@BatchStepInstanceID BIGINT = NULL,
	@BatchStepInstanceCode VARCHAR(25) = 'CMSOPPDL',
	@BatchStepInstanceName VARCHAR(256) = 'CMS Opportunities Data Load',
	@Owner VARCHAR(256) = 'Daniel Hughes',
	@Version VARCHAR(25) = '1.0.0.0',
	@Description VARCHAR(1000)= 'Loads CMS (e.g. 5-Star opportunities, etc.) opportunities from an mDM data feed.',
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC etl.spBatchJobStep_Create
	@BatchJobInstanceID = @BatchJobInstanceID,
	@BatchStepInstanceID = @BatchStepInstanceID,
	@BatchStepInstanceCode = @BatchStepInstanceCode,
	@BatchStepInstanceName = @BatchStepInstanceName,
	@Owner = @Owner,
	@Version = @Version,
	@Description = @Description,
	@CreatedBy = 'dhughes',
	@BatchJobStepID = @BatchJobStepID OUTPUT,
	@Debug = 0

SELECT @BatchJobStepID AS BatchJobStepID

*/

-- SELECT * FROM etl.BatchJobStep
-- SELECT * FROM etl.BatchJobInstance
-- SELECT * FROM etl.BatchStepInstance
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- =============================================
CREATE PROCEDURE [etl].[spBatchJobStep_Create]
	-- BatchJobInstance object properties.
	@BatchJobInstanceID BIGINT = NULL,
	@BatchJobInstanceCode VARCHAR(25) = NULL, -- Optional. Helps build a job if one is not provided.
	@BatchJobInstanceName VARCHAR(256) = NULL, -- Optional. Helps build a job if one is not provided.
	@BatchJobInstanceVersion VARCHAR(25) = NULL, -- Optional. Helps build a job if one is not provided.
	@BatchJobInstanceOwner VARCHAR(256) = NULL, -- Optional. Helps build a job if one is not provided.
	@BatchJobInstanceDescription VARCHAR(1000) = NULL, -- Optional. Helps build a job if one is not provided.
	-- BatchStepInstance object properties.
	@BatchStepInstanceID BIGINT = NULL OUTPUT,
	@BatchStepInstanceCode VARCHAR(25) = NULL, -- Optional. Helps build a step if one is not provided.
	@BatchStepInstanceName VARCHAR(256) = NULL, -- Optional. Helps build a step if one is not provided.
	-- BatchStepInstance extended properties.
	@Owner VARCHAR(256) = NULL,
	@Version VARCHAR(25) = NULL,
	@Description VARCHAR(1000) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	-- Output properties.
	@BatchJobStepID BIGINT = NULL OUTPUT,
	@Debug BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchJobInstanceID=' + dbo.fnToStringOrEmpty(@BatchJobInstanceID) + ';' +
		'@BatchJobInstanceCode=' + dbo.fnToStringOrEmpty(@BatchJobInstanceCode) + ';' +
		'@BatchJobInstanceName=' + dbo.fnToStringOrEmpty(@BatchJobInstanceName) + ';' +
		'@BatchJobInstanceVersion=' + dbo.fnToStringOrEmpty(@BatchJobInstanceVersion) + ';' +
		'@BatchJobInstanceOwner=' + dbo.fnToStringOrEmpty(@BatchJobInstanceOwner) + ';' +
		'@BatchJobInstanceDescription=' + dbo.fnToStringOrEmpty(@BatchJobInstanceDescription) + ';' +
		'@BatchStepInstanceID=' + dbo.fnToStringOrEmpty(@BatchStepInstanceID) + ';' +
		'@BatchStepInstanceCode=' + dbo.fnToStringOrEmpty(@BatchStepInstanceCode) + ';' +
		'@BatchStepInstanceName=' + dbo.fnToStringOrEmpty(@BatchStepInstanceName) + ';' +
		'@Owner=' + dbo.fnToStringOrEmpty(@Owner) + ';' +
		'@Version=' + dbo.fnToStringOrEmpty(@Version) + ';' +
		'@Description=' + dbo.fnToStringOrEmpty(@Description) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@BatchJobStepID=' + dbo.fnToStringOrEmpty(@BatchJobStepID) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @BatchJobStepID = NULL;
	SET @Version = ISNULL(@Version, '0.0.0.0');


	--------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@BatchStepInstanceID_Exists BIT = NULL

	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	-- @BatchJobInstanceID
	IF @BatchJobInstanceID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @BatchJobInstanceID cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END


	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------
		-- Set variables.
		--------------------------------------------------------------------------------------------------------
		-- Do nothing.

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debbuger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Setting Variables' AS Method, 
				@BatchJobInstanceID AS BatchJobInstanceID, @BatchJobInstanceCode AS BatchJobInstanceCode,
				@BatchJobInstanceName AS BatchJobInstanceName, @BatchJobInstanceVersion AS BatchJobInstanceVersion,
				@BatchJobInstanceOwner AS BatchJobInstanceOwner, @BatchJobInstanceDescription AS BatchJobInstanceDescription,
				@BatchStepInstanceID AS BatchStepInstanceID, @BatchStepInstanceCode AS BatchStepInstanceCode,
				@BatchStepInstanceName AS BatchStepInstanceName, @Version AS Version, @Owner AS Owner, @Description AS Description
		END


		--------------------------------------------------------------------------------------------------------
		-- Create new BatchStepInstance (if applicable).
		-- <Summary>
		-- Inspects the incoming BatchStepInstance argument to determine if a value was provided. If no value
		-- was provided then a new instance will be created or retrieved.
		-- </Summary>
		--------------------------------------------------------------------------------------------------------
		-- Data review
		-- SELECT * FROM etl.BatchStepInstance

		-- Create a new BatchStepInstance object if one was not provided.
		IF @BatchStepInstanceID IS NULL
		BEGIN
			-- Determine if the object exists.
			EXEC etl.spBatchStepInstance_Exists
				@BatchEntityID = @BatchStepInstanceID OUTPUT,
				@Code = @BatchStepInstanceCode,
				@Name = @BatchStepInstanceName,
				@Exists = @BatchStepInstanceID_Exists OUTPUT

			-- Debug
			IF @Debug = 1
			BEGIN
				SELECT 'Debbuger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Locating Step Instance' AS Method, 
					@BatchStepInstanceID AS BatchStepInstanceID, 
					@BatchStepInstanceID_Exists AS BatchStepInstanceID_Exists
			END

			-- Create new object if an object was not discovered.
			IF ISNULL(@BatchStepInstanceID_Exists, 0) = 0
			BEGIN
				EXEC etl.spBatchStepInstance_Create
					@BatchStepInstanceID = @BatchStepInstanceID OUTPUT,
					@Code = @BatchStepInstanceCode,
					@Name = @BatchStepInstanceName,
					@CreatedBy = @CreatedBy,
					@Debug = @Debug

				-- Debug
				IF @Debug = 1
				BEGIN
					SELECT 'Debbuger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Creating Step Instance' AS Method, 
						@BatchStepInstanceID AS BatchStepInstanceID
				END

			END

		END


		--------------------------------------------------------------------------------------------------------
		-- Create object.
		--------------------------------------------------------------------------------------------------------
		INSERT INTO etl.BatchJobStep (
			BatchJobInstanceID,
			BatchStepInstanceID,
			Owner,
			Version,
			Description,
			CreatedBy
		)
		SELECT
			@BatchJobInstanceID AS BatchJobInstanceID,
			@BatchStepInstanceID AS BatchStepInstanceID,
			@Owner AS Owner,
			@Version AS Version,
			@Description AS Description,
			@CreatedBy AS CreatedBy

		-- Retrieve object identity.
		SET @BatchJobStepID = SCOPE_IDENTITY();

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debbuger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Setting Object Identity' AS Method,
				@BatchJobStepID AS BatchJobStepID
		END


 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH



END

