﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/10/2015
-- Description:	Creates a new BatchStepInstance object.
-- SAMPLE CALL:
/*

DECLARE
	@BatchEntityID BIGINT = NULL,
	@Code VARCHAR(25) = 'MSOPPDL',
	@Name VARCHAR(256) = 'Med. Sync Opportunities Data Load',
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC etl.spBatchStepInstance_Create
	@BatchEntityID = @BatchEntityID OUTPUT,
	@Code = @Code,
	@Name = @Name,
	@CreatedBy = @CreatedBy

SELECT @BatchEntityID AS BatchEntityID

*/

-- SELECT * FROM etl.BatchStepInstance
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- =============================================
CREATE PROCEDURE [etl].[spBatchStepInstance_Create]
	@Code VARCHAR(25),
	@Name VARCHAR(256),
	@CreatedBy VARCHAR(256) = NULL,
	@BatchStepInstanceID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchEntityID=' + dbo.fnToStringOrEmpty(@BatchEntityID) + ';' +
		'@Code=' + dbo.fnToStringOrEmpty(@Code) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@Version) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @BatchStepInstanceID = NULL;


	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	-- @Code
	IF @Code IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @Code cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	-- @Name
	IF @Name IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @Name cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	


	--------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------
		-- Set variables.
		--------------------------------------------------------------------------------------------------------
		-- No settings.

		--------------------------------------------------------------------------------------------------------
		-- Create object.
		--------------------------------------------------------------------------------------------------------
		INSERT INTO etl.BatchStepInstance(
			Code,
			Name,
			CreatedBy
		)
		SELECT
			@Code AS Code,
			@Name AS Name,
			@CreatedBy AS CreatedBy

		-- Retrieve record identity.
		SET @BatchStepInstanceID = SCOPE_IDENTITY();


 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH



END

