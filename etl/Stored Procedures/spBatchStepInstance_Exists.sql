﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/10/2015
-- Description:	Determines if a BatchStepInstance exists.
-- SAMPLE CALL:
/*

DECLARE
	@BatchStepInstanceID BIGINT = NULL,
	@Code VARCHAR(25) = 'MSDL',
	@Exists BIT = NULL

EXEC etl.spBatchStepInstance_Exists
	@BatchStepInstanceID = @BatchStepInstanceID OUTPUT,
	@Code = @Code,
	@Exists = @Exists OUTPUT

SELECT @BatchStepInstanceID AS BatchStepInstanceID, @Exists AS IsFound

*/

-- SELECT * FROM etl.BatchStepInstance
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- =============================================
CREATE PROCEDURE [etl].[spBatchStepInstance_Exists]
	@BatchStepInstanceID BIGINT = NULL OUTPUT,
	@Code VARCHAR(25) = NULL OUTPUT,
	@Name VARCHAR(256) = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN

	-- SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED added to allow "dirty" reads during inserts.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchStepInstanceID=' + dbo.fnToStringOrEmpty(@BatchStepInstanceID) + ';' +
		'@Code=' + dbo.fnToStringOrEmpty(@Code) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@Version) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @Exists = 0;


	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	-- Do nothing.

	--------------------------------------------------------------------------------------------------------
	-- Set variables.
	--------------------------------------------------------------------------------------------------------
	-- If a BatchStepInstance object was provided then void the Code and Name values.
	IF @BatchStepInstanceID IS NOT NULL
	BEGIN
		SET @Code = NULL;
		SET @Name = NULL;
	END

	-- If a BatchStepInstance object was not provided but a Code was provided, then void the Name value.
	IF @BatchStepInstanceID IS NULL AND @Code IS NOT NULL
	BEGIN
		SET @Name = NULL;
	END

	--------------------------------------------------------------------------------------------------------
	-- Fetch object.
	--------------------------------------------------------------------------------------------------------
	SELECT TOP 1 
		@BatchStepInstanceID = BatchStepInstanceID,
		@Code = Code,
		@Name = Name
	FROM etl.BatchStepInstance bsi
	WHERE BatchStepInstanceID = @BatchStepInstanceID
		OR Code = @Code
		OR Name = @Name 


	IF @@ROWCOUNT = 0
	BEGIN
		SET @BatchStepInstanceID = NULL;
		SET @Code = NULL;
		SET @Name = NULL;
	END
	ELSE
	BEGIN
		SET @Exists = 1;
	END

	





END

