﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/7/2015
-- Description:	Creates a new BatchExecutionParam object.
-- SAMPLE CALL:
/*

DECLARE
	@BatchExecutionID BIGINT = NULL,
	@BatchExecutionParamID BIGINT = NULL,
	@TransactionDate DATETIME = GETDATE(),
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC etl.spBatchExecutionParam_Create
	@BatchExecutionParamID = @BatchExecutionParamID OUTPUT,
	@BatchExecutionID = @BatchExecutionID,
	@TypeOf = 'String',
	@Name = 'NABP',
	@ValueString = '2501624',
	@ValueLong = NULL,
	@ValueDate = NULL,
	@ValueDecimal = NULL,
	@ValueBoolean = NULL,
	@CreatedBy VARCHAR(256) = 'dhughes'

SELECT @BatchExecutionParamID AS BatchExecutionParamID

*/

-- SELECT * FROM etl.BatchExecutionParam
-- SELECT * FROM etl.BatchJobExecution
-- SELECT * FROM etl.BatchJobInstance
-- SELECT * FROM etl.BatchExecution
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- SELECT * FROM etl.ExecutionStatus
-- =============================================
CREATE PROCEDURE [etl].[spBatchExecutionParam_Create]
	@BatchExecutionParamID BIGINT = NULL OUTPUT,
	@BatchExecutionID BIGINT = NULL,
	@TypeOf VARCHAR(50) = NULL,
	@Name VARCHAR(256) = NULL,
	@ValueString VARCHAR(MAX) = NULL,
	@ValueLong BIGINT = NULL,
	@ValueDate DATETIME = NULL,
	@ValueDecimal DECIMAL(9,2) = NULL,
	@ValueBoolean BIT = NULL,
	@CreatedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	
	-------------------------------------------------------------------------------------------
	-- Record request.
	-------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchExecutionParamID=' + dbo.fnToStringOrEmpty(@BatchExecutionParamID) + ';' +
		'@BatchExecutionID=' + dbo.fnToStringOrEmpty(@BatchExecutionID) + ';' +
		'@TypeOf=' + dbo.fnToStringOrEmpty(@TypeOf) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@ValueString=' + dbo.fnToStringOrEmpty(@ValueString) + ';' +
		'@ValueLong=' + dbo.fnToStringOrEmpty(@ValueLong) + ';' +
		'@ValueDate=' + dbo.fnToStringOrEmpty(@ValueDate) + ';' +
		'@ValueDecimal=' + dbo.fnToStringOrEmpty(@ValueDecimal) + ';' +
		'@ValueBoolean=' + dbo.fnToStringOrEmpty(@ValueBoolean) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------
	SET @BatchExecutionParamID = NULL;


	-------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	-------------------------------------------------------------------------------------------
	-- @BatchExecutionID
	IF @BatchExecutionID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @BatchExecutionID cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	-------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------


	-------------------------------------------------------------------------------------------
	-- Create object.
	-------------------------------------------------------------------------------------------
	INSERT INTO etl.BatchExecutionParam (
		BatchExecutionID,
		TypeOf,
		Name,
		ValueString,
		ValueLong,
		ValueDate,
		ValueDecimal,
		ValueBoolean,
		CreatedBy
	)
	SELECT
		@BatchExecutionID AS BatchExecutionID,
		@TypeOf AS TypeOf,
		@Name AS Name,
		@ValueString AS ValueString,
		@ValueLong AS ValueLong,
		@ValueDate AS ValueDate,
		@ValueDecimal AS ValueDecimal,
		@ValueBoolean AS ValueBoolean,
		@CreatedBy AS CreatedBy

	-- Retrieve object identity.
	SET @BatchExecutionParamID = SCOPE_IDENTITY();



END

