﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/7/2015
-- Description:	Creates a new single or list of BatchExecutionParam objects from the provided parameter string.
-- SAMPLE CALL:
/*

DECLARE
	@BatchExecutionID BIGINT = NULL,
	@BatchExecutionParamID VARCHAR(MAX) = NULL,
	@TransactionDate DATETIME = GETDATE(),
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC etl.spBatchExecutionParam_ParseAndCreate
	@BatchExecutionParamID = @BatchExecutionParamID OUTPUT,
	@BatchExecutionID = @BatchExecutionID,
	@ParamString = 'BusinessKey||2501624||String*||*BusinessKeyType||Nabp||String'
	@CreatedBy VARCHAR(256) = 'dhughes'

SELECT @BatchExecutionParamID AS BatchExecutionParamID

*/

-- SELECT * FROM etl.BatchExecutionParam
-- SELECT * FROM etl.BatchJobExecution
-- SELECT * FROM etl.BatchJobInstance
-- SELECT * FROM etl.BatchExecution
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- SELECT * FROM etl.ExecutionStatus
-- =============================================
CREATE PROCEDURE [etl].[spBatchExecutionParam_ParseAndCreate]
	@BatchExecutionID BIGINT = NULL,
	@ParamString VARCHAR(MAX) = NULL,
	@CustomKeyValueDelimiter VARCHAR(10) = NULL,
	@CustomDelimeter VARCHAR(10) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BatchExecutionParamID VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	
	-------------------------------------------------------------------------------------------
	-- Record request.
	-------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchExecutionID=' + dbo.fnToStringOrEmpty(@BatchExecutionID) + ';' +
		'@ParamString=' + dbo.fnToStringOrEmpty(@ParamString) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@BatchExecutionParamID=' + dbo.fnToStringOrEmpty(@BatchExecutionParamID) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------
	SET @BatchExecutionParamID = NULL;


	-------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	-------------------------------------------------------------------------------------------
	-- @BatchExecutionID
	IF @BatchExecutionID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @BatchExecutionID cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	-------------------------------------------------------------------------------------------
	-- Short circuit logic.
	-------------------------------------------------------------------------------------------
	IF LEN(LTRIM(RTRIM(ISNULL(@ParamString, '')))) = 0
	BEGIN
		RETURN;
	END

	-------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------
	-- Do nothing.

	-------------------------------------------------------------------------------------------
	-- Temporary resources.
	-------------------------------------------------------------------------------------------
	DECLARE @tblOuput AS TABLE (
		BatchExecutionParamID BIGINT
	);

	-------------------------------------------------------------------------------------------
	-- Create object.
	-------------------------------------------------------------------------------------------
	INSERT INTO etl.BatchExecutionParam (
		BatchExecutionID,
		TypeOf,
		Name,
		ValueString,	
		CreatedBy
	)
	OUTPUT inserted.BatchExecutionParamID INTO @tblOuput(BatchExecutionParamID)
	SELECT
		@BatchExecutionID AS BatchExecutionID,
		TypeOf,
		Name,
		Value,
		@CreatedBy AS CreatedBy
	--SELECT *
	--SELECT TOP 1 *
	FROM etl.fnSplitParameterString(@ParamString)

	-- Retrieve record identities.
	IF @@ROWCOUNT > 0
	BEGIN
		SET @BatchExecutionParamID = (
			SELECT
				CONVERT(VARCHAR(25), BatchExecutionParamID) + ','
			FROM @tblOuput
			FOR XML PATH('')
		);

		SET @BatchExecutionParamID = LEFT(@BatchExecutionParamID, LEN(@BatchExecutionParamID) - 1);
	END





END

