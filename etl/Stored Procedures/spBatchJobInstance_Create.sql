﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/7/2015
-- Description:	Creates a new BatchJobInstance object.
-- SAMPLE CALL:
/*

DECLARE
	@BatchEntityID BIGINT = NULL

EXEC etl.spBatchJobInstance_Create
	@BatchEntityID = @BatchEntityID OUTPUT,
	@Code = 'MDMBUCDTL',
	@Name = 'mDM Batch Update Controller Detail',
	@Version = '1.0.0.0',
	@Description = 'Handles the mDM nightly data download for a business.',
	@CreatedBy = 'dhughes'

SELECT @BatchEntityID AS BatchEntityID

*/

-- SELECT * FROM etl.BatchJobInstance
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- =============================================
CREATE PROCEDURE [etl].[spBatchJobInstance_Create]
	@Code VARCHAR(25),
	@Name VARCHAR(256),
	@Owner VARCHAR(256) = NULL,
	@Version VARCHAR(25) = NULL,
	@Description VARCHAR(1000) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BatchJobInstanceID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	/*
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	
	DECLARE @Args VARCHAR(MAX) =
		'@BatchJobInstanceID=' + dbo.fnToStringOrEmpty(@BatchJobInstanceID) + ';' +
		'@Code=' + dbo.fnToStringOrEmpty(@Code) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@Version=' + dbo.fnToStringOrEmpty(@Version) + ';' +
		'@Description=' + dbo.fnToStringOrEmpty(@Description) + ';' +

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @Version = ISNULL(@Version, '0.0.0.0');


	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	-- @Code
	IF @Code IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @Code cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	-- @Name
	IF @Name IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @Name cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	


	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------
		-- Set variables.
		--------------------------------------------------------------------------------------------------------
		-- No settings.

		--------------------------------------------------------------------------------------------------------
		-- Create object.
		--------------------------------------------------------------------------------------------------------
		INSERT INTO etl.BatchJobInstance(
			Code,
			Name,
			Owner,
			Version,
			Description,
			CreatedBy
		)
		SELECT
			@Code AS Code,
			@Name AS Name,
			@Owner AS Owner,
			@Version AS Version,
			@Description AS Description,
			@CreatedBy AS CreatedBy

		-- Retrieve object identity.
		SEt @BatchJobInstanceID = SCOPE_IDENTITY();


 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH



END

