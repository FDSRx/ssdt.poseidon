﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/14/2014
-- Description:	Distrubutes business data to other db platforms.
-- SAMPLE CALL: etl.spBusinessEntity_Distribute
-- SAMPLE CALL: etl.spBusinessEntity_Distribute @BusinessEntityID = 14764
-- SELECT * FROM dbo.vwStore ORDER BY Name ASC
-- SELECT * FROM Athena.dbo.Business
-- =============================================
CREATE PROCEDURE [etl].[spBusinessEntity_Distribute]
	@BusinessEntityID VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------
	DECLARE
		@DataCatalogCode VARCHAR(10) = 'BIZ',
		@ErrorLogID BIGINT,
		@ErrorMessage VARCHAR(4000)
	
	------------------------------------------------------------
	-- Temporary resources
	------------------------------------------------------------
	-- Business id list.
	CREATE TABLE #tmpBizIDList (
		BusinessEntityID INT
	);

	------------------------------------------------------------
	-- Parse specified lists.
	-- <Summary>
	-- If the caller has specified certain business criteria then
	-- we need to parse and use that for our lookup.
	-- If no criteria is specified then lets retrieve all businesses.
	-- </Summary>
	------------------------------------------------------------
	
	----------------------------
	-- Parse business entity ids
	----------------------------
	INSERT INTO #tmpBizIDList (
		BusinessEntityID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@BusinessEntityID, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpBizIDList (BusinessEntityID) VALUES (NULL);
	END	


	------------------------------------------------------------
	-- Distribute data
	-- <Summary>
	-- Distrubutes the business data to the proper db channels.
	-- </Summary>
	------------------------------------------------------------
	
	------------------------------------------------------------
	-- Athena distribution
	------------------------------------------------------------
	-- Distrubute store information to Athena.
	INSERT INTO Athena.dbo.Business (
		BusinessEntityID,
		BusinessTypeID,
		Name
	)
	SELECT
		sto.BusinessEntityID,
		(SELECT TOP 1 BusinessTypeID FROM Athena.dbo.BusinessType WHERE Code = 'STRE') AS BusinessTypeID,
		sto.Name
	FROM dbo.Store sto
		JOIN #tmpBizIDList tmp
			ON sto.BusinessEntityID = ISNULL(tmp.BusinessEntityID, sto.BusinessEntityID)
	WHERE NOT EXISTS (
		SELECT TOP 1 *
		FROM Athena.dbo.Business
		WHERE BusinessEntityID = sto.BusinessEntityID
	)

	-- Distrubute chain information to Athena.
	INSERT INTO Athena.dbo.Business (
		BusinessEntityID,
		BusinessTypeID,
		Name
	)
	SELECT
		chn.BusinessEntityID,
		(SELECT TOP 1 BusinessTypeID FROM Athena.dbo.BusinessType WHERE Code = 'CHN') AS BusinessTypeID,
		chn.Name
	FROM dbo.Chain chn
		JOIN #tmpBizIDList tmp
			ON chn.BusinessEntityID = ISNULL(tmp.BusinessEntityID, chn.BusinessEntityID)
	WHERE NOT EXISTS (
		SELECT TOP 1 *
		FROM Athena.dbo.Business
		WHERE BusinessEntityID = chn.BusinessEntityID
	)	
	
	------------------------------------------------------------
	-- Apollo distribution
	------------------------------------------------------------
	-- Distrubute store information to Apollo.
	INSERT INTO Apollo.dbo.Business (
		BusinessEntityID,
		BusinessTypeID,
		Name
	)
	SELECT
		sto.BusinessEntityID,
		(SELECT TOP 1 BusinessTypeID FROM Apollo.dbo.BusinessType WHERE Code = 'STRE') AS BusinessTypeID,
		sto.Name
	FROM dbo.Store sto
		JOIN #tmpBizIDList tmp
			ON sto.BusinessEntityID = ISNULL(tmp.BusinessEntityID, sto.BusinessEntityID)
	WHERE NOT EXISTS (
		SELECT TOP 1 *
		FROM Apollo.dbo.Business
		WHERE BusinessEntityID = sto.BusinessEntityID
	)

	-- Distrubute chain information to Apollo.
	INSERT INTO Apollo.dbo.Business (
		BusinessEntityID,
		BusinessTypeID,
		Name
	)
	SELECT
		chn.BusinessEntityID,
		(SELECT TOP 1 BusinessTypeID FROM Apollo.dbo.BusinessType WHERE Code = 'CHN') AS BusinessTypeID,
		chn.Name
	FROM dbo.Chain chn
		JOIN #tmpBizIDList tmp
			ON chn.BusinessEntityID = ISNULL(tmp.BusinessEntityID, chn.BusinessEntityID)
	WHERE NOT EXISTS (
		SELECT TOP 1 *
		FROM Apollo.dbo.Business
		WHERE BusinessEntityID = chn.BusinessEntityID
	)					

	------------------------------------------------------------
	-- Release resources
	------------------------------------------------------------
	DROP TABLE #tmpBizIDList;
		
END
