﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/7/2015
-- Description:	Creates a new BatchJobExecution object.
-- SAMPLE CALL:
/*

DECLARE
	@BatchExecutionID BIGINT = NULL,
	@BatchJobInstanceID BIGINT = 1,
	@TransactionDate DATETIME = GETDATE(),
	@ExecutionStatusID INT = etl.fnGetExecutionStatusID('CR'),
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC etl.spBatchJobExecution_Create
	@BatchExecutionID = @BatchExecutionID OUTPUT,
	@BatchJobInstanceID = @BatchJobInstanceID,
	@DateStart = @TransactionDate,
	@DateEnd = NULL,
	@ExecutionStatusID = @ExecutionStatusID,
	@ExitCode = NULL,
	@ExitMessage = NULL,
	@Result = NULL,
	@Message = NULL,
	@Arguments = '@BusinessKey=2501624;@BusinessKeyType=Nabp',
	@Parameters = 'BusinessKey||2501624*||*BusinesKeyType||Nabp',
	@CreatedBy = @CreatedBy,
	@Debug = 1

SELECT @BatchExecutionID AS BatchExecutionID

-- ROLLBACK TRANSACTION;
*/

-- SELECT * FROM etl.BatchExecution
-- SELECT * FROM etl.BatchJobExecution
-- SELECT * FROM etl.BatchExecutionParam
-- SELECT * FROM etl.BatchJobStep
-- SELECT * FROM etl.BatchJobInstance
-- SELECT * FROM etl.BatchStepInstance
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- SELECT * FROM etl.ExecutionStatus
-- =============================================
CREATE PROCEDURE [etl].[spBatchJobExecution_Create]
	@BatchExecutionID BIGINT = NULL OUTPUT, -- optional parameter. Object will be created it not provided.
	@BatchJobInstanceID BIGINT = NULL,
	@BatchJobInstanceCode VARCHAR(50) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@ExecutionStatusID INT = NULL,
	@ExecutionStatusCode VARCHAR(25) = NULL,
	@ExitCode VARCHAR(256) = NULL,
	@ExitMessage VARCHAR(MAX) = NULL,
	@Result VARCHAR(MAX) = NULL,
	@Message VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@Parameters VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchExecutionID=' + dbo.fnToStringOrEmpty(@BatchExecutionID) + ';' +
		'@BatchJobInstanceID=' + dbo.fnToStringOrEmpty(@BatchJobInstanceID) + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart) + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd) + ';' +
		'@ExecutionStatusID=' + dbo.fnToStringOrEmpty(@ExecutionStatusID) + ';' +
		'@ExitCode=' + dbo.fnToStringOrEmpty(@ExitCode) + ';' +
		'@ExitMessage=' + dbo.fnToStringOrEmpty(@ExitMessage) + ';' +
		'@Result=' + dbo.fnToStringOrEmpty(@Result) + ';' +
		'@Message=' + dbo.fnToStringOrEmpty(@Message) + ';' +
		'@Arguments=' + dbo.fnToStringOrEmpty(@Arguments) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);

	--------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@BatchExecutionExists BIT = NULL


	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	-- @BatchJobInstanceID
	IF @BatchJobInstanceID IS NULL AND @BatchJobInstanceCode IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @BatchJobInstanceID or @BatchJobInstanceCode cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	--------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------

		--------------------------------------------------------------------------------------------------------
		-- Set variables.
		--------------------------------------------------------------------------------------------------------
		-- Create a new BatchEntity object if one was not provided.
		IF @BatchExecutionID IS NULL
		BEGIN
			EXEC etl.spBatchEntity_Create
				@BatchEntityTypeCode = 'JOBEXEC',
				@CreatedBy = @CreatedBy,
				@BatchEntityID = @BatchExecutionID OUTPUT
		END

		-- Retrieve an ExecutionStatus object if one is not provided.
		IF @ExecutionStatusID IS NULL AND @ExecutionStatusCode IS NOT NULL
		BEGIN
			SET @ExecutionStatusID = etl.fnGetExecutionStatusID(@ExecutionStatusCode);
		END

		-- Data review
		-- SELECT * FROM etl.ExecutionStatus

		-- If unable to retrieve an ExecutionStatus object, then defualt to "Created."
		SET @ExecutionStatusID = ISNULL(@ExecutionStatusID, etl.fnGetExecutionStatusID('CR'));

		-- If a BatchJobInstance object was not provided, then let's try and translate one.
		SET @BatchJobInstanceID = ISNULL(@BatchJobInstanceID, etl.fnGetBatchJobInstanceID(@BatchJobInstanceCode));

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Setting Variables' AS Method,
				@BatchExecutionID AS BatchExecutionID, @BatchJobInstanceID AS BatchJobInstanceID,
				@DateStart AS DateStart, @DateEnd AS DateEnd, @ExecutionStatusID AS ExecutionStatusID, 
				@ExitCode AS ExitCode, @ExitMessage AS ExitMessage,	@Result AS Result, 
				@Message AS Message, @Arguments AS Arguments, @Parameters AS Params
		END

		--------------------------------------------------------------------------------------------------------
		-- Create object.
		--------------------------------------------------------------------------------------------------------
		-- Determine if a BatchExeuction object has been created for the provided BatchEntity object.
		EXEC etl.spBatchExecution_Exists
			@BatchEntityID = @BatchExecutionID,
			@Exists = @BatchExecutionExists OUTPUT

		-- If a BatchExecution object does not exist, then let's create one.
		IF ISNULL(@BatchExecutionExists, 0) = 0
		BEGIN
			EXEC etl.spBatchExecution_Create
				@BatchEntityID = @BatchExecutionID,
				@DateStart = @DateStart,
				@DateEnd = @DateEnd,
				@ExecutionStatusID = @ExecutionStatusID,
				@ExitCode = @ExitCode,
				@ExitMessage = @ExitMessage,
				@Result = @Result,
				@Message = @Message,
				@Arguments = @Arguments,
				@Parameters = @Parameters,
				@CreatedBy = @CreatedBy,
				@Debug = @Debug
		END

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Check Existence and/or Create Batch Exec Object' AS Method,
				@BatchExecutionID AS BatchEntityID, @ExecutionStatusID AS ExecutionStatusID
		END

		-- Debug
		-- SELECT * FROM etl.BatchExecution WHERE BatchEntityID = @BatchEntityID

		-- Data review
		-- SELECT TOP 1 * FROM etl.BatchJobExecution

		-- Create a new BatchJobExecution object.
		INSERT INTO etl.BatchJobExecution(
			BatchExecutionID,
			BatchJobInstanceID,
			CreatedBy
		)
		SELECT
			@BatchExecutionID AS BatchExecutionID,
			@BatchJobInstanceID AS BatchJobInstanceID,
			@CreatedBy AS CreatedBy

 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH


END

