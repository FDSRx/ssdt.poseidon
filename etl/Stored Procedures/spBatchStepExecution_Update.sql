﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/7/2015
-- Description:	Updates a BatchStepExecution object.
-- SAMPLE CALL:
/*

DECLARE
	@BatchExecutionID BIGINT = 25,
	@TransactionDate DATETIME = GETDATE(),
	@ExecutionStatusID INT = etl.fnGetExecutionStatusID('S'),
	@CreatedBy VARCHAR(256) = 'dhughes',
	@ModifiedBy VARCHAR(256) = 'dhughes'

EXEC etl.spBatchStepExecution_Update
	@BatchExecutionID = @BatchExecutionID OUTPUT,
	@DateStart = @TransactionDate,
	@DateEnd = @TransactionDate,
	@ExecutionStatusID = @ExecutionStatusID,
	@CommitCount = 5,
	@WriteCount = 5,
	@ExitCode = NULL,
	@ExitMessage = NULL,
	@Result = NULL,
	@Message = 'The process has completed.',
	@Arguments = '@BusinessKey=2501624;@BusinessKeyType=Nabp',
	@Parameters = 'BusinessKey||2501624||String*||*BusinesKeyType||Nabp||String',
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@Debug = 1

SELECT @BatchExecutionID AS BatchExecutionID

*/

-- SELECT * FROM etl.BatchExecution
-- SELECT * FROM etl.BatchStepExecution
-- SELECT * FROM etl.BatchExecutionParam
-- SELECT * FROM etl.BatchJobExecution
-- SELECT * FROM etl.BatchJobInstance
-- SELECT * FROM etl.BatchStepInstance
-- SELECT * FROM etl.BatchEntity
-- SELECT * FROM etl.BatchEntityType
-- SELECT * FROM etl.ExecutionStatus
-- =============================================
CREATE PROCEDURE [etl].[spBatchStepExecution_Update]
	@BatchExecutionID BIGINT = NULL OUTPUT,
	--@BatchJobExecutionID BIGINT,
	--@BatchStepInstanceID BIGINT,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@ExecutionStatusID INT = NULL,
	@ExecutionStatusCode VARCHAR(25) = NULL,
	@CommitCount BIGINT = NULL,
	@ReadCount BIGINT = NULL,
	@FilterCount BIGINT = NULL,
	@WriteCount BIGINT = NULL,
	@ReadSkipCount BIGINT = NULL,
	@WriteSkipCount BIGINT = NULL,
	@ProcessSkipCount BIGINT = NULL,
	@RollbackCount BIGINT = NULL,
	@ExitCode VARCHAR(256) = NULL,
	@ExitMessage VARCHAR(MAX) = NULL,
	@Result VARCHAR(MAX) = NULL,
	@Message VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@Parameters VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchExecutionID=' + dbo.fnToStringOrEmpty(@BatchExecutionID) + ';' +
		--'@BatchJobExecutionID=' + dbo.fnToStringOrEmpty(@BatchJobExecutionID) + ';' +
		--'@BatchStepInstanceID=' + dbo.fnToStringOrEmpty(@BatchStepInstanceID) + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart) + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd) + ';' +
		'@ExecutionStatusID=' + dbo.fnToStringOrEmpty(@ExecutionStatusID) + ';' +
		'@ExitCode=' + dbo.fnToStringOrEmpty(@ExitCode) + ';' +
		'@ExitMessage=' + dbo.fnToStringOrEmpty(@ExitMessage) + ';' +
		'@Result=' + dbo.fnToStringOrEmpty(@Result) + ';' +
		'@Message=' + dbo.fnToStringOrEmpty(@Message) + ';' +
		'@Arguments=' + dbo.fnToStringOrEmpty(@Arguments) + ';' +
		'@Parameters=' + dbo.fnToStringOrEmpty(@Parameters) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@CommitCount=' + dbo.fnToStringOrEmpty(@CommitCount) + ';' +
		'@ReadCount=' + dbo.fnToStringOrEmpty(@ReadCount) + ';' +
		'@FilterCount=' + dbo.fnToStringOrEmpty(@FilterCount) + ';' +
		'@WriteCount=' + dbo.fnToStringOrEmpty(@WriteCount) + ';' +
		'@ReadSkipCount=' + dbo.fnToStringOrEmpty(@ReadSkipCount) + ';' +
		'@WriteSkipCount=' + dbo.fnToStringOrEmpty(@WriteSkipCount) + ';' +
		'@ProcessSkipCount=' + dbo.fnToStringOrEmpty(@ProcessSkipCount) + ';' +
		'@RollbackCount=' + dbo.fnToStringOrEmpty(@RollbackCount) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	-- Do nothing.

	--------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------
	-- No declarations.


	--------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	IF @BatchExecutionID IS NULL
	BEGIN
		RETURN;
	END

	--------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	--------------------------------------------------------------------------------------------------------
	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	--------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------

		--------------------------------------------------------------------------------------------------------
		-- Set variables.
		--------------------------------------------------------------------------------------------------------
		-- Retrieve an ExecutionStatus object by code if one is not provided.
		IF @ExecutionStatusID IS NULL AND @ExecutionStatusCode IS NOT NULL
		BEGIN
			SET @ExecutionStatusID = etl.fnGetExecutionStatusID(@ExecutionStatusCode);
		END

		-- Data review
		-- SELECT * FROM etl.ExecutionStatus

		-- Debug
		IF @Debug = 1
		BEGIN
			SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Setting Variables' AS Method,
				@BatchExecutionID AS BatchExecutionID, 
				@DateStart AS DateStart, @DateEnd AS DateEnd, @ExecutionStatusID AS ExecutionStatusID, 
				@ExitCode AS ExitCode, @ExitMessage AS ExitMessage,	@Result AS Result, 
				@Message AS Message, @Arguments AS Arguments, @Parameters AS Params
		END


		--------------------------------------------------------------------------------------------------------
		-- Update object.
		--------------------------------------------------------------------------------------------------------
		-- Update the base object.
		EXEC etl.spBatchExecution_Update
			@BatchEntityID = @BatchExecutionID,
			@DateStart = @DateStart,
			@DateEnd = @DateEnd,
			@ExecutionStatusID = @ExecutionStatusID,
			@ExecutionStatusCode = @ExecutionStatusCode,
			@ExitCode = @ExitCode,
			@ExitMessage = @ExitMessage,
			@Result = @Result,
			@Message = @Message,
			@Arguments = @Arguments,
			@Parameters = @Parameters,
			@CreatedBy = @CreatedBy,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug

		-- Update the inherited object
		UPDATE bse
		SET
			bse.CommitCount = CASE WHEN @CommitCount IS NULL THEN bse.CommitCount ELSE @CommitCount END,
			bse.ReadCount = CASE WHEN @ReadCount IS NULL THEN bse.ReadCount ELSE @ReadCount END,
			bse.FilterCount = CASE WHEN @FilterCount IS NULL THEN bse.FilterCount ELSE @FilterCount END,
			bse.WriteCount = CASE WHEN @WriteCount IS NULL THEN bse.WriteCount ELSE @WriteCount END,
			bse.ReadSkipCount = CASE WHEN @ReadSkipCount IS NULL THEN bse.ReadSkipCount ELSE @ReadSkipCount END,
			bse.WriteSkipCount = CASE WHEN @WriteSkipCount IS NULL THEN bse.WriteSkipCount ELSE @WriteSkipCount END,
			bse.ProcessSkipCount = CASE WHEN @ProcessSkipCount IS NULL THEN bse.ProcessSkipCount ELSE @ProcessSkipCount END,
			bse.RollbackCount = CASE WHEN @RollbackCount IS NULL THEN bse.RollbackCount ELSE @RollbackCount END,			
			bse.DateModified = GETDATE(),
			bse.ModifiedBy = CASE WHEN @ModifiedBy IS NULL THEN bse.ModifiedBy ELSE @ModifiedBy END
		FROM etl.BatchStepExecution bse
		WHERE BatchExecutionID = @BatchExecutionID


 	--------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-- Re-trhow the error.
		SET @ErrorMessage = ERROR_MESSAGE();
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );			  

	END CATCH


END

