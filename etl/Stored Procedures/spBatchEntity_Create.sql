﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/7/2015
-- Description:	Creates a new BatchEntity object.
-- SAMPLE CALL:
/*
DECLARE
	@BatchEntityID BIGINT = NULL

EXEC etl.BatchEntity
	@BatchEntityTypeCode = 'JOB',
	@CreatedBy = 'dhughes',
	@BatchEntityID = @BatchEntityID OUTPUT

SELECT @BatchEntityID AS BatchEntityID

*/

-- SELECT * FROM etl.BatchEntity
-- =============================================
CREATE PROCEDURE [etl].[spBatchEntity_Create]
	@BatchEntityTypeID INT = NULL,
	@BatchEntityTypeCode VARCHAR(25) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BatchEntityID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	/*
	-------------------------------------------------------------------------------------------
	-- Record request.
	-------------------------------------------------------------------------------------------
	-- Log incoming arguments
	
	DECLARE @Args VARCHAR(MAX) =
		'@BatchEntityTypeID=' + dbo.fnToStringOrEmpty(@BatchEntityTypeID) + ';' +
		'@BatchEntityTypeCode=' + dbo.fnToStringOrEmpty(@BatchEntityTypeCode) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @BatchEntityID = NULL;

	-------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	-------------------------------------------------------------------------------------------
	-- @BatchEntityTypeID, @BatchEntityTypeCode
	IF @BatchEntityTypeID IS NULL AND ISNULL(@BatchEntityTypeCode, '') = '' 
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @BatchEntityTypeID and @BatchEntityTypeCode cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	-------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------
	-- Try and retrieve a BusinessEntityType identifier if the code was provided.
	IF @BatchEntityTypeID IS NULL AND ISNULL(@BatchEntityTypeCode, '') <> ''
	BEGIN
		SET @BatchEntityTypeID = etl.fnGetBatchEntityTypeID(@BatchEntityTypeCode);
	END

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debbuger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Setting Variables' AS Method,
			@BatchEntityID AS BatchEntityID, @BatchEntityTypeID AS BatchEntityTypeID, 
			@BatchEntityTypeCode AS BatchEntityTypeCode, @CreatedBy AS CreatedBy
	END

	-------------------------------------------------------------------------------------------
	-- Create object.
	-------------------------------------------------------------------------------------------
	INSERT INTO etl.BatchEntity (
		BatchEntityTypeID,
		CreatedBy
	)
	SELECT
		@BatchEntityTypeID AS BatchEntityTypeID,
		@CreatedBy AS CreatedBy

	-- Retrieve object identity.
	SET @BatchEntityID = SCOPE_IDENTITY();

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debbuger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Setting Object Identity' AS Method,
			@BatchEntityID AS BatchEntityID
	END

END

