﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/10/2015
-- Description:	Determines if the BatchExecution object exists.
-- SAMPLE CALL:
/*

DECLARE
	@BatchEntityID BIGINT = 2,
	@TransactionDate DATETIME = GETDATE(),
	@Exists BIT = NULL

EXEC etl.spBatchExecution_Exists
	@BatchEntityID = @BatchEntityID OUTPUT,
	@Exists = @Exists OUTPUT

SELECT @BatchEntityID AS BatchEntityID, @Exists AS IsFound

*/

-- SELECT * FROM etl.BatchExecution
-- =============================================
CREATE PROCEDURE [etl].[spBatchExecution_Exists]
	@BatchEntityID BIGINT OUTPUT,
	@rowguid UNIQUEIDENTIFIER = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	
	-- SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED added to allow "dirty" reads during inserts.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BatchEntityID=' + dbo.fnToStringOrEmpty(@BatchEntityID) + ';' +
		'@rowguid=' + dbo.fnToStringOrEmpty(@rowguid) + ';' +
		'@Exists=' + dbo.fnToStringOrEmpty(@Exists) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	--------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------------------
	SET @Exists = 0;

	--------------------------------------------------------------------------------------------------------
	-- Set variables.
	--------------------------------------------------------------------------------------------------------
	-- Void rowguid if the primary identifier has been provided.
	IF @BatchEntityID IS NOT NULL
	BEGIN
		SET @rowguid = NULL;
	END

	--------------------------------------------------------------------------------------------------------
	-- Retrieve object.
	--------------------------------------------------------------------------------------------------------
	SELECT TOP 1
		@BatchEntityID = BatchEntityID
	FROM etl.BatchExecution be
	WHERE BatchEntityID = @BatchEntityID
		OR rowguid = @rowguid

	IF @@ROWCOUNT > 0
	BEGIN
		SET @Exists = 1;
	END
	ELSE
	BEGIN
		SET @Exists = 0;
		SET @BatchEntityID = NULL;
	END

END

