﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/7/2015
-- Description:	Creates a new BatchEntityType object.
-- SAMPLE CALL:
/*

DECLARE
	@BatchEntityTypeID INT = NULL

EXEC etl.spBatchEntityType_Create
	@Code = 'STPEXEC',
	@Name = 'Step Execution',
	@Description = 'A step being executed.',
	@CreatedBy = 'dhughes',
	@BatchEntityTypeID = @BatchEntityTypeID OUTPUT

SELECT @BatchEntityTypeID AS BatchEntityTypeID

*/

-- SELECT * FROM etl.BatchEntityType
-- =============================================
CREATE PROCEDURE [etl].[spBatchEntityType_Create]
	@Code VARCHAR(25),
	@Name VARCHAR(256),
	@Description VARCHAR(1000) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BatchEntityTypeID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL
	
	
	
	-------------------------------------------------------------------------------------------
	-- Record request.
	-------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@Code=' + dbo.fnToStringOrEmpty(@Code) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@Description=' + dbo.fnToStringOrEmpty(@Code) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@Code) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------
	SET @BatchEntityTypeID = NULL;

	-------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	-------------------------------------------------------------------------------------------
	-- @Code
	IF @Code IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @Code cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	-- @Name
	IF @Name IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The @Name cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	-------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------
	-- Do nothing.

	-------------------------------------------------------------------------------------------
	-- Create object.
	-------------------------------------------------------------------------------------------
	INSERT INTO etl.BatchEntityType (
		Code,
		Name,
		Description,
		CreatedBy
	)
	SELECT
		@Code AS Code,
		@Name AS Name,
		@Description AS Description,
		@CreatedBy AS CreatedBy


	-- Retrieve object identity.
	SET @BatchEntityTypeID = SCOPE_IDENTITY();

END

