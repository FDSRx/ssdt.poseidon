﻿



CREATE VIEW [etl].[vwBatchJobExecutionBusiness]
AS

SELECT
	bjeb.BatchJobExecutionID,
	bjeb.BatchJobExecutionParentID,
	bje.BatchJobInstanceID,
	bji.Code AS BatchJobInstanceCode,
	bji.Name AS BatchJobInstanceName,
	bji.Version AS BatchJobInstanceVersion,
	bji.Owner AS BatchJobInstanceOwner,
	bjeb.BusinessID,
	bjeb.BusinessKey,
	bjeb.BusinessKeyType,
	sto.Name AS BusinessName,
	bjeb.Notes,
	be.DateStart,
	be.DateEnd,
	be.ExecutionStatusID,
	es.Code AS ExecutionStatusCode,
	es.Name AS ExecutionStatusName,
	be.ExitCode,
	be.ExitMessage,
	be.Result,
	be.Message,
	be.Arguments,
	be.rowguid,
	be.DateCreated,
	be.DateModified,
	be.CreatedBy,
	be.ModifiedBy
--SELECT *
--SELECT TOP 1 *
FROM etl.BatchJobExecutionBusiness bjeb (NOLOCK)
	JOIN etl.BatchJobExecution bje (NOLOCK)
		ON bjeb.BatchJobExecutionID = bje.BatchExecutionID
	JOIN etl.BatchExecution be (NOLOCK)
		ON bje.BatchExecutionID = be.BatchEntityID
	JOIN etl.BatchJobInstance bji (NOLOCK)
		ON bje.BatchJobInstanceID = bji.BatchJobInstanceID
	LEFT JOIN etl.ExecutionStatus es (NOLOCK)
		ON be.ExecutionStatusID = es.ExecutionStatusID
	LEFT JOIN dbo.Store sto (NOLOCK)
		ON sto.BusinessEntityID = bjeb.BusinessID
			OR (
				bjeb.BusinessID IS NULL
				AND sto.Nabp = CASE WHEN bjeb.BusinessKeyType IN ('NABP', 'NCPDPID') THEN bjeb.BusinessKey ELSE NULL END
			)







