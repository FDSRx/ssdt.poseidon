﻿




CREATE VIEW [etl].[vwBatchStepExecution]
AS

SELECT
	bse.BatchExecutionID,
	bse.BatchJobExecutionID,
	bje.BatchJobInstanceID,
	bji.Code AS BatchJobInstanceCode,
	bji.Name AS BatchJobInstanceName,
	bji.Version AS BatchJobInstanceVersion,
	bji.Owner AS BatchJobInstanceOwner,
	bji.Description AS BatchJobInstanceDescription,
	bse.BatchStepInstanceID,
	bsi.Code AS BatchStepInstanceCode,
	bsi.Name AS BatchStepInstanceName,
	bjs.Owner AS BatchStepInstanceOwner,
	bjs.Version AS BatchStepInstanceVersion,
	bjs.Description AS BatchStepInstanceDescription,
	be.DateStart,
	be.DateEnd,
	be.ExecutionStatusID,
	es.Code AS ExecutionStatusCode,
	es.Name AS ExecutionStatusName,
	bse.ReadCount,
	bse.FilterCount,
	bse.WriteCount,
	bse.ReadSkipCount,
	bse.WriteSkipCount,
	bse.ProcessSkipCount,
	bse.RollbackCount,
	be.ExitCode,
	be.ExitMessage,
	be.Result,
	be.Message,
	be.Arguments,
	be.rowguid,
	be.DateCreated,
	be.DateModified,
	be.CreatedBy,
	be.ModifiedBy
--SELECT *
FROM etl.BatchStepExecution bse (NOLOCK)
	JOIN etl.BatchJobExecution bje (NOLOCK)
		ON bse.BatchJobExecutionID = bje.BatchExecutionID
	JOIN etl.BatchExecution be (NOLOCK)
		ON bse.BatchExecutionID = be.BatchEntityID
	JOIN etl.BatchJobInstance bji (NOLOCK)
		ON bje.BatchJobInstanceID = bji.BatchJobInstanceID
	JOIN etl.BatchStepInstance bsi (NOLOCK)
		ON bse.BatchStepInstanceID = bsi.BatchStepInstanceID
	LEFT JOIN etl.BatchJobStep bjs (NOLOCK)
		ON bje.BatchJobInstanceID = bjs.BatchJobInstanceID
			AND bse.BatchStepInstanceID = bjs.BatchStepInstanceID
	LEFT JOIN etl.ExecutionStatus es (NOLOCK)
		ON be.ExecutionStatusID = es.ExecutionStatusID






