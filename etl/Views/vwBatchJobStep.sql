﻿





CREATE VIEW [etl].vwBatchJobStep AS
SELECT
	js.BatchJobStepID,
	js.BatchJobInstanceID,
	job.Code AS JobCode,
	job.Name AS JobName,
	job.Owner AS JobOwner,
	job.Version AS JobVersion,
	job.Description AS JobDescription,
	stp.BatchStepInstanceID,
	stp.Code AS StepCode,
	stp.Name AS StepName,
	js.Owner AS StepOwner,
	js.Version AS StepVersion,
	js.Description AS StepDescription,
	js.DateCreated,
	js.DateModified,
	js.CreatedBy,
	js.ModifiedBy
--SELECT *
FROM etl.BatchJobStep js
	JOIN etl.BatchJobInstance job	 
		ON job.BatchJobInstanceID = js.BatchJobInstanceID
	JOIN etl.BatchStepInstance stp
		ON stp.BatchStepInstanceID = js.BatchStepInstanceID
