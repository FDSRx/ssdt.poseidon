﻿


CREATE VIEW [etl].[vwBatchExecution]
AS

SELECT
	be.BatchEntityID,
	be.DateStart,
	be.DateEnd,
	be.ExecutionStatusID,
	es.Code AS ExecutionStatusCode,
	es.Name AS ExecutionStatusName,
	be.ExitCode,
	be.ExitMessage,
	be.Result,
	be.Message,
	be.Arguments,
	be.rowguid,
	be.DateCreated,
	be.DateModified,
	be.CreatedBy,
	be.ModifiedBy
FROM etl.BatchExecution be (NOLOCK)
	LEFT JOIN etl.ExecutionStatus es (NOLOCK)
		ON be.ExecutionStatusID = es.ExecutionStatusID




