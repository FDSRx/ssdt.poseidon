﻿


CREATE VIEW [etl].[vwBatchJobExecution]
AS

SELECT
	bje.BatchExecutionID,
	bje.BatchJobInstanceID,
	bji.Code AS BatchJobInstanceCode,
	bji.Name AS BatchJobInstanceName,
	bji.Version AS BatchJobInstanceVersion,
	bji.Owner AS BatchJobInstanceOwner,
	be.DateStart,
	be.DateEnd,
	be.ExecutionStatusID,
	es.Code AS ExecutionStatusCode,
	es.Name AS ExecutionStatusName,
	be.ExitCode,
	be.ExitMessage,
	be.Result,
	be.Message,
	be.Arguments,
	be.rowguid,
	be.DateCreated,
	be.DateModified,
	be.CreatedBy,
	be.ModifiedBy
FROM etl.BatchJobExecution bje (NOLOCK)
	JOIN etl.BatchExecution be (NOLOCK)
		ON bje.BatchExecutionID = be.BatchEntityID
	JOIN etl.BatchJobInstance bji (NOLOCK)
		ON bje.BatchJobInstanceID = bji.BatchJobInstanceID
	LEFT JOIN etl.ExecutionStatus es (NOLOCK)
		ON be.ExecutionStatusID = es.ExecutionStatusID




