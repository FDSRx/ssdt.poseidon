﻿CREATE TABLE [etl].[BatchJobStep] (
    [BatchJobStepID]      BIGINT           IDENTITY (1, 1) NOT NULL,
    [BatchJobInstanceID]  BIGINT           NOT NULL,
    [BatchStepInstanceID] BIGINT           NOT NULL,
    [Owner]               VARCHAR (256)    NULL,
    [Version]             VARCHAR (25)     NULL,
    [Description]         VARCHAR (1000)   NULL,
    [rowguid]             UNIQUEIDENTIFIER CONSTRAINT [DF_BatchJobStep_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]         DATETIME         CONSTRAINT [DF_BatchJobStep_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]        DATETIME         CONSTRAINT [DF_BatchJobStep_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           VARCHAR (256)    NULL,
    [ModifiedBy]          VARCHAR (256)    NULL,
    CONSTRAINT [PK_BatchJobStep] PRIMARY KEY CLUSTERED ([BatchJobStepID] ASC),
    CONSTRAINT [FK_BatchJobStep_BatchJobInstance] FOREIGN KEY ([BatchJobInstanceID]) REFERENCES [etl].[BatchJobInstance] ([BatchJobInstanceID]),
    CONSTRAINT [FK_BatchJobStep_BatchStepInstance] FOREIGN KEY ([BatchStepInstanceID]) REFERENCES [etl].[BatchStepInstance] ([BatchStepInstanceID])
);

