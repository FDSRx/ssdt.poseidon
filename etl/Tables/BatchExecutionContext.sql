﻿CREATE TABLE [etl].[BatchExecutionContext] (
    [BatchExecutionID]  BIGINT           NOT NULL,
    [ShortContext]      VARCHAR (4000)   NULL,
    [SerializedContext] VARCHAR (MAX)    NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_BatchExecutionContext_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_BatchExecutionContext_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_BatchExecutionContext_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_BatchExecutionContext] PRIMARY KEY CLUSTERED ([BatchExecutionID] ASC),
    CONSTRAINT [FK_BatchExecutionContext_BatchExecution] FOREIGN KEY ([BatchExecutionID]) REFERENCES [etl].[BatchExecution] ([BatchEntityID])
);

