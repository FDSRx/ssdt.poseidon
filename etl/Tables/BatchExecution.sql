﻿CREATE TABLE [etl].[BatchExecution] (
    [BatchEntityID]     BIGINT           NOT NULL,
    [DateStart]         DATETIME         NULL,
    [DateEnd]           DATETIME         NULL,
    [ExecutionStatusID] INT              NULL,
    [ExitCode]          VARCHAR (256)    NULL,
    [ExitMessage]       VARCHAR (MAX)    NULL,
    [Result]            VARCHAR (MAX)    NULL,
    [Message]           VARCHAR (MAX)    NULL,
    [Arguments]         VARCHAR (MAX)    NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_BatchExecution_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_BatchExecution_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_BatchExecution_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_BatchExecution] PRIMARY KEY CLUSTERED ([BatchEntityID] ASC),
    CONSTRAINT [FK_BatchExecution_ExecutionStatus] FOREIGN KEY ([ExecutionStatusID]) REFERENCES [etl].[ExecutionStatus] ([ExecutionStatusID])
);

