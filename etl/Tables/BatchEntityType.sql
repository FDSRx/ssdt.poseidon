﻿CREATE TABLE [etl].[BatchEntityType] (
    [BatchEntityTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]              VARCHAR (25)     NOT NULL,
    [Name]              VARCHAR (50)     NOT NULL,
    [Description]       VARCHAR (1000)   NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_BatchEntityType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_BatchEntityType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_BatchEntityType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_BatchEntityType] PRIMARY KEY CLUSTERED ([BatchEntityTypeID] ASC)
);

