﻿CREATE TABLE [etl].[BatchStepInstance] (
    [BatchStepInstanceID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [Code]                VARCHAR (50)     NOT NULL,
    [Name]                VARCHAR (256)    NOT NULL,
    [rowguid]             UNIQUEIDENTIFIER CONSTRAINT [DF_BatchStepInstance_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]         DATETIME         CONSTRAINT [DF_BatchStepInstance_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]        DATETIME         CONSTRAINT [DF_BatchStepInstance_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           VARCHAR (256)    NULL,
    [ModifiedBy]          VARCHAR (256)    NULL,
    CONSTRAINT [PK_BatchStepInstance_1] PRIMARY KEY CLUSTERED ([BatchStepInstanceID] ASC)
);

