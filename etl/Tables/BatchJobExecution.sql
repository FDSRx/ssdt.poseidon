﻿CREATE TABLE [etl].[BatchJobExecution] (
    [BatchExecutionID]   BIGINT           NOT NULL,
    [BatchJobInstanceID] BIGINT           NOT NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_BatchJobExecution_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_BatchJobExecution_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_BatchJobExecution_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_BatchJobExecution] PRIMARY KEY CLUSTERED ([BatchExecutionID] ASC),
    CONSTRAINT [FK_BatchJobExecution_BatchExecution] FOREIGN KEY ([BatchExecutionID]) REFERENCES [etl].[BatchExecution] ([BatchEntityID]),
    CONSTRAINT [FK_BatchJobExecution_BatchJobInstance] FOREIGN KEY ([BatchJobInstanceID]) REFERENCES [etl].[BatchJobInstance] ([BatchJobInstanceID])
);

