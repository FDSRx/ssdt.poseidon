﻿CREATE TABLE [etl].[BatchJobInstance] (
    [BatchJobInstanceID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [Code]               VARCHAR (25)     NOT NULL,
    [Name]               VARCHAR (256)    NOT NULL,
    [Owner]              VARCHAR (256)    NULL,
    [Version]            VARCHAR (25)     NULL,
    [Description]        VARCHAR (1000)   NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_BatchJobInstance_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_BatchJobInstance_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_BatchJobInstance_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_BatchJobInstance_1] PRIMARY KEY CLUSTERED ([BatchJobInstanceID] ASC)
);

