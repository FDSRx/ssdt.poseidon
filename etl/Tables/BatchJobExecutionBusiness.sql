﻿CREATE TABLE [etl].[BatchJobExecutionBusiness] (
    [BatchJobExecutionID]       BIGINT           NOT NULL,
    [BatchJobExecutionParentID] BIGINT           NOT NULL,
    [BusinessID]                BIGINT           NULL,
    [BusinessKey]               VARCHAR (100)    NULL,
    [BusinessKeyType]           VARCHAR (50)     NULL,
    [Notes]                     VARCHAR (100)    NULL,
    [rowguid]                   UNIQUEIDENTIFIER CONSTRAINT [DF_BatchJobExecutionBusiness_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]               DATETIME         CONSTRAINT [DF_BatchJobExecutionBusiness_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]              DATETIME         CONSTRAINT [DF_BatchJobExecutionBusiness_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                 VARCHAR (256)    NULL,
    [ModifiedBy]                VARCHAR (256)    NULL,
    CONSTRAINT [PK_BatchJobExecutionBusiness] PRIMARY KEY CLUSTERED ([BatchJobExecutionID] ASC),
    CONSTRAINT [FK_BatchJobExecutionBusiness_BatchJobExecution] FOREIGN KEY ([BatchJobExecutionID]) REFERENCES [etl].[BatchJobExecution] ([BatchExecutionID]),
    CONSTRAINT [FK_BatchJobExecutionBusiness_BatchJobExecution1] FOREIGN KEY ([BatchJobExecutionParentID]) REFERENCES [etl].[BatchJobExecution] ([BatchExecutionID]),
    CONSTRAINT [FK_BatchJobExecutionBusiness_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID])
);

