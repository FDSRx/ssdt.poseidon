﻿CREATE TABLE [etl].[BatchEntity] (
    [BatchEntityID]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [BatchEntityTypeID] INT              NOT NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_BatchEntity_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_BatchEntity_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_BatchEntity_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_BatchEntity] PRIMARY KEY CLUSTERED ([BatchEntityID] ASC),
    CONSTRAINT [FK_BatchEntity_BatchEntityType] FOREIGN KEY ([BatchEntityTypeID]) REFERENCES [etl].[BatchEntityType] ([BatchEntityTypeID])
);

