﻿CREATE TABLE [etl].[BatchExecutionParam] (
    [BatchExecutionParamID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BatchExecutionID]      BIGINT           NOT NULL,
    [TypeOf]                VARCHAR (50)     NULL,
    [Name]                  VARCHAR (256)    NOT NULL,
    [ValueString]           VARCHAR (MAX)    NULL,
    [ValueLong]             BIGINT           NULL,
    [ValueDate]             DATETIME         NULL,
    [ValueDecimal]          DECIMAL (9, 2)   NULL,
    [ValueBoolean]          BIT              NULL,
    [rowguid]               UNIQUEIDENTIFIER CONSTRAINT [DF_BatchEntityExecutionParams_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]           DATETIME         CONSTRAINT [DF_BatchEntityExecutionParams_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]          DATETIME         CONSTRAINT [DF_BatchEntityExecutionParams_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             VARCHAR (256)    NULL,
    [ModifiedBy]            VARCHAR (256)    NULL,
    CONSTRAINT [PK_BatchEntityExecutionParams] PRIMARY KEY CLUSTERED ([BatchExecutionParamID] ASC),
    CONSTRAINT [FK_BatchExecutionParam_BatchExecution] FOREIGN KEY ([BatchExecutionID]) REFERENCES [etl].[BatchExecution] ([BatchEntityID])
);

