﻿CREATE TABLE [etl].[BatchStepExecution] (
    [BatchExecutionID]    BIGINT           NOT NULL,
    [BatchJobExecutionID] BIGINT           NOT NULL,
    [BatchStepInstanceID] BIGINT           NOT NULL,
    [CommitCount]         BIGINT           CONSTRAINT [DF_BatchStepExecution_CommitCount] DEFAULT ((0)) NOT NULL,
    [ReadCount]           BIGINT           CONSTRAINT [DF_BatchStepExecution_ReadCount] DEFAULT ((0)) NOT NULL,
    [FilterCount]         BIGINT           CONSTRAINT [DF_BatchStepExecution_FilterCount] DEFAULT ((0)) NOT NULL,
    [WriteCount]          BIGINT           CONSTRAINT [DF_BatchStepExecution_WriteCount] DEFAULT ((0)) NOT NULL,
    [ReadSkipCount]       BIGINT           CONSTRAINT [DF_BatchStepExecution_ReadSkipCount] DEFAULT ((0)) NOT NULL,
    [WriteSkipCount]      BIGINT           CONSTRAINT [DF_BatchStepExecution_WriteSkipCount] DEFAULT ((0)) NOT NULL,
    [ProcessSkipCount]    BIGINT           CONSTRAINT [DF_BatchStepExecution_ProcessSkipCount] DEFAULT ((0)) NOT NULL,
    [RollbackCount]       INT              CONSTRAINT [DF_BatchStepExecution_RollbackCount] DEFAULT ((0)) NOT NULL,
    [rowguid]             UNIQUEIDENTIFIER CONSTRAINT [DF_BatchStepExecution_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]         DATETIME         CONSTRAINT [DF_BatchStepExecution_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]        DATETIME         CONSTRAINT [DF_BatchStepExecution_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           VARCHAR (256)    NULL,
    [ModifiedBy]          VARCHAR (256)    NULL,
    CONSTRAINT [PK_BatchStepExecution] PRIMARY KEY CLUSTERED ([BatchExecutionID] ASC),
    CONSTRAINT [FK_BatchStepExecution_BatchExecution1] FOREIGN KEY ([BatchExecutionID]) REFERENCES [etl].[BatchExecution] ([BatchEntityID]),
    CONSTRAINT [FK_BatchStepExecution_BatchJobExecution2] FOREIGN KEY ([BatchJobExecutionID]) REFERENCES [etl].[BatchJobExecution] ([BatchExecutionID]),
    CONSTRAINT [FK_BatchStepExecution_BatchStepInstance] FOREIGN KEY ([BatchStepInstanceID]) REFERENCES [etl].[BatchStepInstance] ([BatchStepInstanceID])
);

