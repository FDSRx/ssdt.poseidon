﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/7/2015
-- Description:	Retrieve the BatchEntityType object identifier.
-- SAMPLE CALL: SELECT etl.fnGetBatchEntityTypeID(1);
-- SAMPLE CALL: SELECT etl.fnGetBatchEntityTypeID('STP');

-- SELECT * FROM etl.BatchEntityType
-- =============================================
CREATE FUNCTION [etl].[fnGetBatchEntityTypeID]
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT BatchEntityTypeID FROM etl.BatchEntityType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT BatchEntityTypeID FROM etl.BatchEntityType WHERE BatchEntityTypeID = @Key);
	END	

	-- Return the result of the function
	RETURN @Id;

END

