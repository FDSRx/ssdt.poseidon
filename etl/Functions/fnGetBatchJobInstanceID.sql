﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/12/2015
-- Description:	Retrieve the BatchJobInstance object identifier.
-- SAMPLE CALL: SELECT etl.fnGetBatchJobInstanceID(1);
-- SAMPLE CALL: SELECT etl.fnGetBatchJobInstanceID('CO');
-- SAMPLE CALL: SELECT etl.fnGetBatchJobInstanceID('MDMBUCMST');

-- SELECT * FROM etl.BatchJobInstance
-- =============================================
CREATE FUNCTION [etl].[fnGetBatchJobInstanceID]
(
	@Key VARCHAR(50)
)
RETURNS BIGINT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT BatchJobInstanceID FROM etl.BatchJobInstance WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT BatchJobInstanceID FROM etl.BatchJobInstance WHERE BatchJobInstanceID = @Key);
	END	

	-- Return the result of the function
	RETURN @Id;

END

