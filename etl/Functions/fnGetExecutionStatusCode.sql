﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/16/2015
-- Description:	Retrieve the ExecutionStatus object unique code.
-- SAMPLE CALL: SELECT etl.fnGetExecutionStatusCode(2);
-- SAMPLE CALL: SELECT etl.fnGetExecutionStatusCode('CO');

-- SELECT * FROM etl.ExecutionStatus
-- =============================================
CREATE FUNCTION [etl].[fnGetExecutionStatusCode]
(
	@Key VARCHAR(50)
)
RETURNS VARCHAR(25)
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Code VARCHAR(25)

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Code = (SELECT Code FROM etl.ExecutionStatus WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Code = (SELECT Code FROM etl.ExecutionStatus WHERE ExecutionStatusID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Code;

END

