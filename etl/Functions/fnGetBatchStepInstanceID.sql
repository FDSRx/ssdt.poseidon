﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/12/2015
-- Description:	Retrieve the BatchStepInstance object identifier.
-- SAMPLE CALL: SELECT etl.fnGetBatchStepInstanceID(1);
-- SAMPLE CALL: SELECT etl.fnGetBatchStepInstanceID('PATVALUPD');
-- SAMPLE CALL: SELECT etl.fnGetBatchStepInstanceID('MSOPPDL');

-- SELECT * FROM etl.BatchStepInstance
-- =============================================
CREATE FUNCTION [etl].[fnGetBatchStepInstanceID]
(
	@Key VARCHAR(50)
)
RETURNS BIGINT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT BatchStepInstanceID FROM etl.BatchStepInstance WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT BatchStepInstanceID FROM etl.BatchStepInstance WHERE BatchStepInstanceID = @Key);
	END	

	-- Return the result of the function
	RETURN @Id;

END

