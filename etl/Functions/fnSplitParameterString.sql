﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/11/2015
-- Description:	Parses the parameter list.
-- SAMPLE CALL:
/*

DECLARE 
	@Parameters VARCHAR(MAX) = NULL

SET @Parameters = etl.fnAppendParameter(@Parameters, 'BusinessKey', '1000006', 'String');
SET @Parameters = etl.fnAppendParameter(@Parameters, 'BusinessKeyType', 'NABP', 'String');
SET @Parameters = etl.fnAppendParameter(@Parameters, 'PatientKey', '000000', NULL);
SET @Parameters = etl.fnAppendParameter(@Parameters, 'PatientKeyType', NULL, NULL);

SELECT @Parameters AS Parameters

SELECT * FROM etl.fnSplitParameterString(@Parameters);

*/
-- =============================================
CREATE FUNCTION [etl].[fnSplitParameterString]
(
	@Str VARCHAR(MAX)
)
RETURNS @tblParams TABLE 
(		
	Idx INT IDENTITY(1,1),
	Name VARCHAR(256),
	Value NVARCHAR(MAX),
	TypeOf VARCHAR(50)
) 
AS  
BEGIN

	----------------------------------------------------------------------------------
	-- Local variables.
	----------------------------------------------------------------------------------
	DECLARE
		@KeyValueDelimiter VARCHAR(10) = '*||*',
		@Delimiter VARCHAR(10) = '||'

	----------------------------------------------------------------------------------
	-- Temporary resources.
	----------------------------------------------------------------------------------
	DECLARE @tblParamInfo AS TABLE (
		Idx INT IDENTITY(1,1),
		ParamString VARCHAR(MAX)
	);

	-- Declare temporary parts table.
	DECLARE @tblParamParts AS TABLE (
		Idx INT,
		Value VARCHAR(MAX)
	);

	----------------------------------------------------------------------------------
	-- Get parameter string info.
	----------------------------------------------------------------------------------
	INSERT INTO @tblParamInfo (
		ParamString
	)
	SELECT
		Value
	FROM dbo.fnSplitString(@Str, @KeyValueDelimiter)

	----------------------------------------------------------------------------------
	-- Parse param info.
	----------------------------------------------------------------------------------
	DECLARE
		@Idx INT = 0,
		@ParamString VARCHAR(MAX) = NULL

	SELECT TOP(1)
		@Idx = Idx,
		@ParamString = ParamString
	FROM @tblParamInfo
	WHERE Idx > @Idx
	ORDER BY Idx ASC

	----------------------------------------------------------------------------------
	-- Iterrate through records.
	----------------------------------------------------------------------------------	
	WHILE @@ROWCOUNT = 1
	BEGIN

		DECLARE
			@ParamName VARCHAR(256) = NULL,
			@ParamValue VARCHAR(MAX) = NULL,
			@TypeOf VARCHAR(50) = NULL

		-- Remove existing records.
		DELETE FROM @tblParamParts;

		-- Split parameter info
		INSERT INTO @tblParamParts (
			Idx,
			Value
		)
		SELECT
			Idx,
			Value
		FROM dbo.fnSplitString(@ParamString, @Delimiter);

		-- Assign parts
		SELECT
			@ParamName = CASE WHEN Idx = 1 THEN Value ELSE @ParamName END,
			@ParamValue = CASE WHEN Idx = 2 THEN Value ELSE @ParamValue END,
			@TypeOf = CASE WHEN Idx = 3 THEN Value ELSE @TypeOf END
		FROM @tblParamParts

		-- Debug
		-- SELECT @ParamName AS ParamName, @ParamValue AS ParamValue, @TypeOf AS TypeOf

		-- Finalize data.
		SET @ParamValue = 
			CASE
				WHEN @ParamValue = '<NULL>' THEN NULL
				ELSE @ParamValue
			END;

		-- Normalize parts.
		INSERT INTO @tblParams (
			Name,
			Value,
			TypeOf
		)
		SELECT 
			@ParamName AS ParamName, 
			@ParamValue AS ParamValue, 
			@TypeOf AS TypeOf 
		
		----------------------------------------------------------------------------------
		-- Programmer Note: No code beyond this point. It is used to fetch the next record.
		----------------------------------------------------------------------------------	
		SELECT TOP(1)
			@Idx = Idx,
			@ParamString = ParamString
		FROM @tblParamInfo
		WHERE Idx > @Idx
		ORDER BY Idx ASC

	END




	-- Return the result of the function
	RETURN;

END

