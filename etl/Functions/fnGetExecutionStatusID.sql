﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/16/2015
-- Description:	Retrieve the ExecutionStatus object identifier.
-- SAMPLE CALL: SELECT etl.fnGetExecutionStatusID(1);
-- SAMPLE CALL: SELECT etl.fnGetExecutionStatusID('CO');

-- SELECT * FROM etl.ExecutionStatus
-- =============================================
CREATE FUNCTION [etl].[fnGetExecutionStatusID]
(
	@Key VARCHAR(50)
)
RETURNS BIGINT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT ExecutionStatusID FROM etl.ExecutionStatus WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT ExecutionStatusID FROM etl.ExecutionStatus WHERE ExecutionStatusID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END

