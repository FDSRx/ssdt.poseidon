﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/11/2015
-- Description:	Appends the provided parameter to the parameter string.
-- SAMPLE CALL
/*

DECLARE 
	@Parameters VARCHAR(MAX) = NULL

SET @Parameters = etl.fnAppendParameter(@Parameters, 'BusinessKey', '1000006', 'String');
SET @Parameters = etl.fnAppendParameter(@Parameters, 'BusinessKeyType', 'NABP', 'String');
SET @Parameters = etl.fnAppendParameter(@Parameters, 'PatientKey', '000000', NULL);

SELECT @Parameters AS Parameters

*/
-- =============================================
CREATE FUNCTION [etl].[fnAppendParameter]
(
	@Str VARCHAR(MAX),
	@ParamName VARCHAR(256),
	@ParamValue VARCHAR(MAX) = NULL,
	@TypeOf VARCHAR(50) = NULL
)
RETURNS VARCHAr(MAX)
AS
BEGIN

	------------------------------------------------------------------------------------
	-- Short circuit the logic.
	------------------------------------------------------------------------------------
	-- If no parameter name was provided, the just return the string.
	IF LEN(RTRIM(LTRIM(ISNULL(@ParamName, '')))) = 0
	BEGIN
		RETURN @Str;
	END

	------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------
	SET @TypeOf =
		CASE
			WHEN LEN(LTRIM(RTRIM(ISNULL(@TypeOf, '')))) = '' THEN 'String'
			ELSE @TypeOf
		END;

	------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------
	DECLARE
		@KeyValueDelimiter VARCHAR(10) = '*||*',
		@Delimiter VARCHAR(10) = '||',
		@StrLength BIGINT = 0,
		@Item VARCHAR(MAX) = NULL


	------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------
	SET @Str = LTRIM(RTRIM(ISNULL(@Str, '')));
	SET @StrLength = LEN(@Str);
	SET @ParamValue = 
		CASE
			WHEN @ParamValue IS NULL THEN '<NULL>'
			ELSE @ParamValue
		END;
	SET @Item = @ParamName + @Delimiter + @ParamValue + @Delimiter + @TypeOf;

	------------------------------------------------------------------------------------
	-- Append string.
	------------------------------------------------------------------------------------
	IF @StrLength = 0
	BEGIN
		SET @Str = @Str + @Item;
	END
	ELSE
	BEGIN
		SET @Str = @Str + @KeyValueDelimiter + @Item;
	END
	
	

	-- Return the result of the function
	RETURN @Str;

END

