﻿CREATE TABLE [tmp].[Chain_Import] (
    [ChainImportID]    BIGINT        IDENTITY (1, 1) NOT NULL,
    [SourceChainID]    INT           NULL,
    [Name]             VARCHAR (255) NULL,
    [AddressLine1]     VARCHAR (255) NULL,
    [AddressLine2]     VARCHAR (255) NULL,
    [City]             VARCHAR (255) NULL,
    [State]            VARCHAR (255) NULL,
    [PostalCode]       VARCHAR (255) NULL,
    [PhoneNumber]      VARCHAR (255) NULL,
    [FaxNumber]        VARCHAR (255) NULL,
    [TimeZone]         VARCHAR (25)  NULL,
    [SupportDST]       BIT           CONSTRAINT [DF_Chain_Import_SupportDST] DEFAULT ((0)) NULL,
    [BusinessEntityID] INT           NULL,
    [IsMerged]         BIT           CONSTRAINT [DF_Chain_Import_IsMerged] DEFAULT ((0)) NULL,
    [ErrorLogID]       BIGINT        NULL,
    [DataCatalogCode]  VARCHAR (25)  NOT NULL,
    [Comments]         VARCHAR (MAX) NULL,
    [DateCreated]      DATETIME      CONSTRAINT [DF_Chain_Import_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME      CONSTRAINT [DF_Chain_Import_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Chain_Import] PRIMARY KEY CLUSTERED ([ChainImportID] ASC)
);

