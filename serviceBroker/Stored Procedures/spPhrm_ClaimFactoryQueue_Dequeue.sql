﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/14/2014
-- Description:	Dequeues an item from the initiator process.
-- =============================================
CREATE PROCEDURE serviceBroker.[spPhrm_ClaimFactoryQueue_Dequeue]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------
	-- Local variables
	---------------------------------------------
	DECLARE 
		@MessageTypeName NVARCHAR(255),
		@ConversationKey UNIQUEIDENTIFIER,
		@MessageXml XML,
		@MessageBinary VARBINARY(MAX)			
			
	---------------------------------------------
	-- Pop item from queue
	---------------------------------------------
	;WAITFOR
	(
		RECEIVE TOP (1) 
			@MessageBinary = message_body, 
			@ConversationKey = conversation_handle, 
			@MessageTypeName = message_type_name, 
			@MessageXml = CASE message_type_name WHEN 'X' 
								  THEN CAST(message_body AS NVARCHAR(MAX)) 
								  ELSE message_body 
								END
		--SELECT *
		FROM dbo.ClaimQueue	
	), TIMEOUT 2000;
	
	-- If i had a record then end the conversation
	IF @ConversationKey IS NOT NULL AND dbo.fnReverse(ISNULL(@MessageTypeName, ''), '/') = 'EndDialog'
	BEGIN
		END CONVERSATION @ConversationKey;
	END
	ELSE IF @ConversationKey IS NOT NULL AND dbo.fnReverse(ISNULL(@MessageTypeName, ''), '/') = 'Error'
	BEGIN
		END CONVERSATION @ConversationKey;
	END
	ELSE IF @ConversationKey IS NOT NULL AND @MessageTypeName = '//Poseidon/Claim/ItemAdded'
	BEGIN	
		END CONVERSATION @ConversationKey;
	END
	
END

