﻿

-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/14/2014
-- Description:	Writes a record to the queue. 
-- SAMPLE CALL: serviceBroker.spPhrm_ClaimQueue_Enqueue NULL
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- =============================================
CREATE PROCEDURE servicebroker.[spPhrm_ClaimQueue_Enqueue]
	@Record VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE 
		@DialogHandle UNIQUEIDENTIFIER,
		@Body NVARCHAR(1000),
		@Msg XML
  

	SET @Body = N'<?xml version="1.0"?>
      <ClaimMessage>
        <Record>' + ISNULL(@Record, '') + '</Record>
      </ClaimMessage>';
      
	SET @Msg = CAST(@body AS XML)
  
	BEGIN DIALOG CONVERSATION @DialogHandle
    FROM SERVICE 
      [//Poseidon/ClaimFactoryService]
    TO SERVICE
      '//Poseidon/ClaimService'
    ON CONTRACT
      [//Poseidon/Claim/AddItemContract];
      
	SEND ON CONVERSATION @DialogHandle
	MESSAGE TYPE [//Poseidon/Claim/AddItem] (@Msg);
	--END CONVERSATION @DialogHandle;
  
END












