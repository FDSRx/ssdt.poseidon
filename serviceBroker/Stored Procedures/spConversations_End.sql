﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/21/2014
-- Description:	Closes all the specified dialogs for the service broker.
-- SAMPLE CALL: serviceBroker.spConversations_End -- ends all convos
-- SAMPLE CALL: serviceBroker.spConversations_End @State = 'DI' -- Disconnected inbound
-- =============================================
CREATE PROCEDURE serviceBroker.spConversations_End
	@State VARCHAR(1000) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Handle UNIQUEIDENTIFIER

	-------------------------------------------------------------
	-- Iterate through dialogs and close if the criteria is met
	-------------------------------------------------------------
	DECLARE BrokerDialog CURSOR FOR

		SELECT 
			conversation_handle
		FROM sys.conversation_endpoints
		WHERE ( @State IS NULL OR state collate SQL_Latin1_General_CP1_CI_AS  IN (SELECT Value FROM dbo.fnSplit(@State, ',')) )
			AND state collate SQL_Latin1_General_CP1_CI_AS <> 'CO' -- Ignore closed transactions


	OPEN BrokerDialog;
	
	FETCH NEXT FROM BrokerDialog INTO @Handle;
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		END CONVERSATION @Handle WITH CLEANUP;
		FETCH NEXT FROM BrokerDialog INTO @Handle;
		
	END
	CLOSE BrokerDialog;
	DEALLOCATE BrokerDialog;


END

