﻿


-- select * from dbo.CalendarItem

CREATE VIEW [dbo].[vwCalendarItem]
AS
SELECT
  ci.NoteID,
  n.BusinessEntityID,
  enttyp.Name AS BusinessEntityTypeName,
  n.ScopeID,
  s.Code AS ScopeCode,
  s.Name AS ScopeName,
  n.NoteTypeID,
  typ.Code AS NoteTypeCode,
  typ.Name AS NoteTypeName,
  n.Title,
  n.Memo,
  n.NotebookID,
  nb.Code AS NotebookCode,
  nb.Name AS NotebookName,
  n.PriorityID,
  p.Code AS PriorityCode,
  p.Name AS PriorityName,
  ci.Location,
  o.OriginID,
  o.Code AS OriginCode,
  o.Name AS OriginName,
  n.OriginDataKey,
  CONVERT(VARCHAR(MAX), n.AdditionalData) AS AdditionalData,
  n.CorrelationKey,
  ci.rowguid,
  ci.DateCreated,
  ci.DateModified,
  ci.CreatedBy,
  ci.ModifiedBy
FROM dbo.CalendarItem AS ci
INNER JOIN dbo.Note AS n
  ON ci.NoteID = n.NoteID
LEFT OUTER JOIN dbo.Priority AS p
  ON n.PriorityID = p.PriorityID
LEFT OUTER JOIN dbo.Scope AS s
  ON n.ScopeID = s.ScopeID
LEFT OUTER JOIN dbo.Origin AS o
  ON n.OriginID = o.OriginID
LEFT OUTER JOIN dbo.Notebook AS nb
  ON n.NotebookID = nb.NotebookID
LEFT OUTER JOIN dbo.NoteType AS typ
  ON n.NoteTypeID = typ.NoteTypeID
INNER JOIN dbo.BusinessEntity AS ent
  ON n.BusinessEntityID = ent.BusinessEntityID
INNER JOIN dbo.BusinessEntityType AS enttyp
  ON ent.BusinessEntityTypeID = enttyp.BusinessEntityTypeID




