﻿









CREATE VIEW [dbo].[vwTask]
AS
SELECT
  tsk.NoteID,
	tsk.ApplicationID,
	app.Code AS ApplicationCode,
	app.Name AS ApplicationName,
	tsk.BusinessID AS BusinessID,
	biz.Name AS BusinessName,
  n.BusinessEntityID,
  enttyp.Name AS BusinessEntityTypeName,
  n.ScopeID,
  s.Code AS ScopeCode,
  s.Name AS ScopeName,
  n.NoteTypeID,
  typ.Code AS NoteTypeCode,
  typ.Name AS NoteTypeName,
  n.Title,
  n.Memo,
  n.NotebookID,
  nb.Code AS NotebookCode,
  nb.Name AS NotebookName,
  n.PriorityID,
  p.Code AS PriorityCode,
  p.Name AS PriorityName,
  tsk.AssignedToID,
  tsk.AssignedToString,
  tsk.AssignedByID,
  tsk.AssignedByString,
  tsk.DateDue,
  tsk.CompletedByID,
  tsk.CompletedByString,
  tsk.DateCompleted,
  o.OriginID,
  o.Code AS OriginCode,
  o.Name AS OriginName,
  n.OriginDataKey,
  n.AdditionalData,
  n.CorrelationKey,
  tsk.TaskStatusID,
  ts.Code AS TaskStatusCode,
  ts.Name AS TaskStatusName,
  tsk.ParentTaskID,
  tsk.rowguid,
  tsk.DateCreated,
  tsk.DateModified,
  tsk.CreatedBy,
  tsk.ModifiedBy
--SELECT *
FROM dbo.Task AS tsk
INNER JOIN dbo.Note AS n -- SELECT * FROM dbo.Note
  ON tsk.NoteID = n.NoteID
LEFT OUTER JOIN dbo.Priority AS p
  ON n.PriorityID = p.PriorityID
LEFT OUTER JOIN dbo.Scope AS s
  ON n.ScopeID = s.ScopeID
LEFT OUTER JOIN dbo.Origin AS o
  ON n.OriginID = o.OriginID
LEFT OUTER JOIN dbo.Notebook AS nb
  ON n.NotebookID = nb.NotebookID
LEFT OUTER JOIN dbo.NoteType AS typ
  ON n.NoteTypeID = typ.NoteTypeID
INNER JOIN dbo.BusinessEntity AS ent
  ON n.BusinessEntityID = ent.BusinessEntityID
INNER JOIN dbo.BusinessEntityType AS enttyp
  ON ent.BusinessEntityTypeID = enttyp.BusinessEntityTypeID
LEFT JOIN dbo.TaskStatus ts
  ON tsk.TaskStatusID = ts.TaskStatusID
LEFT JOIN dbo.Application app
	ON app.ApplicationID = tsk.ApplicationID
LEFT JOIN dbo.Business biz
	ON biz.BusinessEntityID = tsk.BusinessID 











GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tsk"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 201
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "n"
            Begin Extent = 
               Top = 6
               Left = 239
               Bottom = 125
               Right = 408
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 446
               Bottom = 125
               Right = 606
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 644
               Bottom = 125
               Right = 804
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "nb"
            Begin Extent = 
               Top = 6
               Left = 842
               Bottom = 125
               Right = 1002
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "typ"
            Begin Extent = 
               Top = 6
               Left = 1040
               Bottom = 125
               Right = 1200
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ent"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 0
         En', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwTask';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'd
         Begin Table = "enttyp"
            Begin Extent = 
               Top = 126
               Left = 269
               Bottom = 245
               Right = 462
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 26
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwTask';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwTask';

