﻿



CREATE VIEW [dbo].[vwApplication]   AS
SELECT
	a.ApplicationID,	
	a.Code,
	a.Name,
	a.ApplicationTypeID,
	at.Code as ApplicationTypeCode,
	at.Name as ApplicationTypeName,
	a.Description,
	a.LicenseName,
	a.ApplicationPath,
	a.rowguid,
	a.DateCreated,
	a.DateModified,
	a.CreatedBy, 
	a.ModifiedBy
--SELECT *
--SELECT TOP 1 *
FROM dbo.Application a
	INNER JOIN dbo.ApplicationType at
		ON a.ApplicationTypeID = at.ApplicationTypeID

