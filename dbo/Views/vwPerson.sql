﻿CREATE VIEW [dbo].[vwPerson]
AS
SELECT     psn.BusinessEntityID AS PersonID, psn.FirstName, psn.LastName, psn.MiddleName, psn.Title, psn.Suffix, psn.BirthDate, gdr.Name AS Gender, 
                      lang.Name AS Language, addr.AddressLine1, addr.AddressLine2, addr.City, addr.State, addr.PostalCode, hphone.PhoneNumber AS HomePhone, 
                      hphone.IsCallAllowed AS IsCallAllowedToHomePhone, hphone.IsTextAllowed AS IsTextAllowedToHomePhone, hphone.IsPreferred AS IsHomePhonePreferred, 
                      mphone.PhoneNumber AS MobilePhone, mphone.IsCallAllowed AS IsCallAllowedToMobilePhone, mphone.IsTextAllowed AS IsTextAllowedToMobilePhone, 
                      mphone.IsPreferred AS IsMobilePhonePreferred, pemail.EmailAddress AS PrimaryEmail, pemail.IsEmailAllowed AS IsEmailAllowedToPrimaryEmail, 
                      pemail.IsPreferred AS IsPrimaryEmailPreferred, aemail.EmailAddress AS AlternateEmail, aemail.IsEmailAllowed AS IsEmailAllowedToAlternateEmail, 
                      aemail.IsPreferred AS IsAlternateEmailPreferred, psn.DateCreated, psn.DateModified, psn.CreatedBy, psn.ModifiedBy
FROM         dbo.Person AS psn WITH (NOLOCK) LEFT OUTER JOIN
                      dbo.Gender AS gdr WITH (NOLOCK) ON psn.GenderID = gdr.GenderID LEFT OUTER JOIN
                      dbo.Language AS lang ON psn.LanguageID = lang.LanguageID LEFT OUTER JOIN
                          (SELECT     padr.BusinessEntityID AS PersonID, padr.AddressID, adr.AddressLine1, adr.AddressLine2, adr.City, sp.StateProvinceCode AS State, 
                                                   adr.PostalCode
                            FROM          dbo.BusinessEntityAddress AS padr WITH (NOLOCK) INNER JOIN
                                                   dbo.Address AS adr ON padr.AddressID = adr.AddressID AND padr.AddressTypeID =
                                                       (SELECT     AddressTypeID
                                                         FROM          dbo.AddressType WITH (NOLOCK)
                                                         WHERE      (Code = 'HME')) LEFT OUTER JOIN
                                                   dbo.StateProvince AS sp ON adr.StateProvinceID = sp.StateProvinceID) AS addr ON psn.BusinessEntityID = addr.PersonID LEFT OUTER JOIN
                          (SELECT     BusinessEntityID AS PersonID, PhoneNumber, IsCallAllowed, IsTextAllowed, IsPreferred
                            FROM          dbo.BusinessEntityPhone AS pp WITH (NOLOCK)
                            WHERE      (PhoneNumberTypeID =
                                                       (SELECT     PhoneNumberTypeID
                                                         FROM          dbo.PhoneNumberType WITH (NOLOCK)
                                                         WHERE      (Code = 'HME')))) AS hphone ON psn.BusinessEntityID = hphone.PersonID LEFT OUTER JOIN
                          (SELECT     BusinessEntityID AS PersonID, PhoneNumber, IsCallAllowed, IsTextAllowed, IsPreferred
                            FROM          dbo.BusinessEntityPhone AS pp WITH (NOLOCK)
                            WHERE      (PhoneNumberTypeID =
                                                       (SELECT     PhoneNumberTypeID
                                                         FROM          dbo.PhoneNumberType AS PhoneNumberType_1 WITH (NOLOCK)
                                                         WHERE      (Code = 'MBL')))) AS mphone ON psn.BusinessEntityID = mphone.PersonID LEFT OUTER JOIN
                          (SELECT     BusinessEntityID AS PersonID, EmailAddress, IsEmailAllowed, IsPreferred
                            FROM          dbo.BusinessEntityEmailAddress AS pe WITH (NOLOCK)
                            WHERE      (EmailAddressTypeID =
                                                       (SELECT     EmailAddressTypeID
                                                         FROM          dbo.EmailAddressType WITH (NOLOCK)
                                                         WHERE      (Code = 'PRIM')))) AS pemail ON psn.BusinessEntityID = pemail.PersonID LEFT OUTER JOIN
                          (SELECT     BusinessEntityID AS PersonID, EmailAddress, IsEmailAllowed, IsPreferred
                            FROM          dbo.BusinessEntityEmailAddress AS pe WITH (NOLOCK)
                            WHERE      (EmailAddressTypeID =
                                                       (SELECT     EmailAddressTypeID
                                                         FROM          dbo.EmailAddressType AS EmailAddressType_1 WITH (NOLOCK)
                                                         WHERE      (Code = 'ALT')))) AS aemail ON psn.BusinessEntityID = aemail.PersonID

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "psn"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gdr"
            Begin Extent = 
               Top = 6
               Left = 245
               Bottom = 125
               Right = 405
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "lang"
            Begin Extent = 
               Top = 6
               Left = 443
               Bottom = 125
               Right = 603
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "addr"
            Begin Extent = 
               Top = 6
               Left = 641
               Bottom = 125
               Right = 801
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hphone"
            Begin Extent = 
               Top = 6
               Left = 839
               Bottom = 125
               Right = 999
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mphone"
            Begin Extent = 
               Top = 6
               Left = 1037
               Bottom = 125
               Right = 1197
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pemail"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 198
            End
            DisplayFlags = 280
            TopColumn', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwPerson';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N' = 0
         End
         Begin Table = "aemail"
            Begin Extent = 
               Top = 126
               Left = 236
               Bottom = 245
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwPerson';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwPerson';

