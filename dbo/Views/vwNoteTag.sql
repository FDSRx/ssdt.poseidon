﻿






CREATE VIEW [dbo].[vwNoteTag] AS 
SELECT
	nt.NoteTagID,
	nt.NoteID,
	nte.ApplicationID,
	nte.BusinessID,
	nte.BusinessEntityID,
	nte.Title,
	nte.Memo,
	nt.TagID,
	tag.Name AS TagName,
	nt.rowguid,
	nt.DateCreated,
	--nt.DateModified,
	nt.CreatedBy
	--nt.ModifiedBy
FROM dbo.NoteTag nt
	JOIN dbo.Note nte
		ON nte.NoteID = nt.NoteID
	JOIN dbo.Tag tag
		ON tag.TagID = nt.TagID
