﻿


CREATE VIEW [dbo].[vwChainStore] AS
SELECT
	cs.ChainStoreID,
	cs.ChainID,
	chn.SourceChainID,
	bizchn.BusinessToken AS ChainToken,
	chn.Name AS ChainName,
	cs.StoreID,
	bizsto.BusinessToken AS StoreToken,
	sto.Nabp AS Nabp,
	sto.SourceStoreID,
	sto.Name AS StoreName
--SELECT *
--SELECT TOP 1 *
FROM dbo.ChainStore cs
	JOIN dbo.Chain chn
		ON chn.BusinessEntityID = cs.ChainID
	JOIN dbo.Store sto
		ON sto.BusinessEntityID = cs.StoreID
	JOIN dbo.Business bizchn
		ON bizchn.BusinessEntityID = chn.BusinessEntityID
	JOIN dbo.Business bizsto
		ON bizsto.BusinessentityId = sto.BusinessEntityID

