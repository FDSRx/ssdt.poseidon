﻿







CREATE VIEW [dbo].[vwConfiguration] AS 
		SELECT
			cfg.ConfigurationID,
			cfg.ApplicationID,
			app.Code AS ApplicationCode,
			app.Name AS ApplicationName,
			cfg.BusinessEntityID,
			sto.Nabp,
			sto.Name AS BusinessName,
			cfgkey.ConfigurationKeyID,
			cfg.KeyName,
			cfg.KeyValue,
			cfg.DataTypeID,
			typ.Name AS DataTypeName,
			cfg.DataSize,
			ISNULL(cfg.Description, cfgkey.Description) AS Description,
			cfg.rowguid,
			cfg.DateCreated,
			cfg.DateModified,
			cfg.CreatedBy,
			cfg.ModifiedBy
		--SELECT * 
		FROM dbo.Configuration cfg
			LEFT JOIN dbo.Application app
				ON cfg.ApplicationID = app.ApplicationID
			LEFT JOIN dbo.DataType typ
				ON cfg.DataTypeID = typ.DataTypeID
			LEFT JOIN dbo.Store sto
				ON cfg.BusinessEntityID = sto.BusinessEntityID
			JOIN config.ConfigurationKey cfgkey
				ON cfg.KeyName = cfgkey.Name


			

