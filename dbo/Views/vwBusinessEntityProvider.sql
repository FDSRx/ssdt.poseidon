﻿







CREATE VIEW dbo.vwBusinessEntityProvider AS
SELECT
	bp.BusinessEntityProviderID,
	bp.BusinessEntityID,
	biz.SourceSystemKey AS BusinessSourceSystemKey,
	sto.Nabp,
	ISNULL(sto.Name, biz.Name) AS BusinessName,
	bp.ProviderID,
	prov.Code AS ProviderCode,
	prov.Name AS ProviderName,
	bp.Username,
	bp.PasswordUnencrypted,
	bp.PasswordEncrypted,
	bp.rowguid,
	bp.DateCreated,
	bp.DateModified,
	bp.CreatedBy,
	bp.ModifiedBy
--SELECT *
FROM dbo.BusinessEntityProvider bp
	JOIN dbo.Business biz
		ON biz.BusinessEntityID = bp.BusinessEntityID
	LEFT JOIN dbo.Store sto
		ON sto.BusinessEntityID = bp.BusinessEntityID
	LEFT JOIN dbo.Provider prov
		ON prov.ProviderID = bp.ProviderID