﻿


CREATE VIEW dbo.vwRequestTracker AS
		SELECT
			req.RequestTrackerID,
			req.SourceName,
			req.RequestTypeID,
			typ.Code AS RequestTypeCode,
			typ.Name AS RequestTypeName,
			req.ApplicationKey,
			req.ApplicationID,
			app.Code AS ApplicationCode,
			app.Name AS ApplicationName,
			req.BusinessKey,
			req.BusinessEntityID,
			sto.Nabp AS Nabp,
			sto.Name AS BusinessName,
			req.RequesterKey,
			req.RequesterID,
			req.RequestCount,
			req.RequestsRemaining,
			req.MaxRequestsAllowed,
			req.IsLocked,
			req.DateExpires,
			req.RequestData,
			req.Arguments,
			req.rowguid,
			req.DateCreated,
			req.DateModified,
			req.CreatedBy,
			req.ModifiedBy
		--SELECT *
		FROM dbo.RequestTracker req
			LEFT JOIN dbo.RequestType typ
				ON typ.RequestTypeID = req.RequestTypeID
			LEFT JOIN dbo.Application app
				ON req.ApplicationID = app.ApplicationID
			LEFT JOIN dbo.Store sto
				ON req.BusinessEntityID = sto.BusinessEntityID