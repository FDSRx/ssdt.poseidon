﻿






CREATE VIEW [dbo].[vwHoliday] WITH SCHEMABINDING AS
SELECT
	hol.HolidayID,
	hol.HolidayTypeID,
	typ.Code AS HolidayTypeCode,
	typ.Name AS HolidayTypeName,
	hol.DayID AS DayID,
	wd.Code AS DayCode,
	wd.Name AS Weekday,
	hol.DateObserved,
	hol.Name,
	hol.WhereObserved,
	hol.rowguid,
	hol.DateCreated,
	hol.DateModified,
	hol.CreatedBy,
	hol.ModifiedBy
FROM dbo.Holiday hol
	JOIN dbo.HolidayType typ
		ON hol.HolidayTypeID = typ.HolidayTypeID
	JOIN dbo.Days wd
		ON hol.DayID = wd.DayID

GO
CREATE UNIQUE CLUSTERED INDEX [UIX_vwHoliday_HolidayID]
    ON [dbo].[vwHoliday]([HolidayID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_vwHoliday_DateObserved]
    ON [dbo].[vwHoliday]([DateObserved] ASC);

