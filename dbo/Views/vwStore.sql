﻿



/* SELECT * FROM dbo.vwStore*/
CREATE VIEW [dbo].[vwStore]
AS
SELECT
  sto.BusinessEntityID,
  sto.Name,
  sto.SourceStoreID,
  bizsto.BusinessToken,
  bizsto.BusinessNumber,
  bizsto.LicenseNumber,
  bizsto.SourceSystemKey,
  sto.NABP,
  chn.BusinessEntityID AS ChainID,
  chn.SourceChainID,
  chn.Name AS ChainName,
  adr_1.AddressLine1,
  adr_1.AddressLine2,
  adr_1.City,
  adr_1.State,
  adr_1.PostalCode,
  adr_1.Latitude,
  adr_1.Longitude,
  sto.Website,
  phn_1.PhoneNumber,
  eml_1.EmailAddress,
  fax.PhoneNumber AS FaxNumber,
  zne.Code AS TimeZoneCode,
  zne.Location AS TimeZoneLocation,
  zne.Offset AS TimeZoneOffSet,
  sto.SupportDST
FROM dbo.Store AS sto
INNER JOIN dbo.Business AS bizsto
  ON bizsto.BusinessEntityID = sto.BusinessEntityID
LEFT OUTER JOIN (SELECT
  baddr.BusinessEntityID,
  adr.AddressLine1,
  adr.AddressLine2,
  adr.City,
  sta.StateProvinceCode AS State,
  adr.PostalCode,
  adr.Latitude,
  adr.Longitude
FROM dbo.BusinessEntityAddress AS baddr
INNER JOIN dbo.Address AS adr
  ON baddr.AddressID = adr.AddressID
INNER JOIN dbo.AddressType AS typ
  ON baddr.AddressTypeID = typ.AddressTypeID
LEFT OUTER JOIN dbo.StateProvince AS sta
  ON adr.StateProvinceID = sta.StateProvinceID
WHERE (typ.Code = 'PRIM')) AS adr_1
  ON sto.BusinessEntityID = adr_1.BusinessEntityID
LEFT OUTER JOIN (SELECT
  phn.BusinessEntityID,
  phn.PhoneNumber
FROM dbo.BusinessEntityPhone AS phn
INNER JOIN dbo.PhoneNumberType AS typ
  ON phn.PhoneNumberTypeID = typ.PhoneNumberTypeID
WHERE (typ.Code = 'WRK')) AS phn_1
  ON sto.BusinessEntityID = phn_1.BusinessEntityID
LEFT OUTER JOIN (SELECT
  phn.BusinessEntityID,
  phn.PhoneNumber
FROM dbo.BusinessEntityPhone AS phn
INNER JOIN dbo.PhoneNumberType AS typ
  ON phn.PhoneNumberTypeID = typ.PhoneNumberTypeID
WHERE (typ.Code = 'FAX')) AS fax
  ON sto.BusinessEntityID = fax.BusinessEntityID
LEFT OUTER JOIN (SELECT
  eml.BusinessEntityID,
  eml.EmailAddress
FROM dbo.BusinessEntityEmailAddress AS eml
INNER JOIN dbo.EmailAddressType AS typ
  ON eml.EmailAddressTypeID = typ.EmailAddressTypeID
WHERE (typ.Code = 'PRIM')) AS eml_1
  ON sto.BusinessEntityID = eml_1.BusinessEntityID
LEFT OUTER JOIN dbo.TimeZone AS zne
  ON sto.TimeZoneID = zne.TimeZoneID
LEFT JOIN dbo.ChainStore cs -- SELECT * FROM dbo.ChainStore
	ON cs.StoreID = sto.BusinessEntityID
LEFT JOIN dbo.Chain chn -- SELECT * FROM dbo.Chain
	ON chn.BusinessEntityID = cs.ChainID




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sto"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "biz"
            Begin Extent = 
               Top = 96
               Left = 452
               Bottom = 215
               Right = 645
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "adr_1"
            Begin Extent = 
               Top = 6
               Left = 245
               Bottom = 125
               Right = 414
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "phn_1"
            Begin Extent = 
               Top = 6
               Left = 452
               Bottom = 95
               Right = 621
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "fax"
            Begin Extent = 
               Top = 6
               Left = 659
               Bottom = 95
               Right = 828
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "eml_1"
            Begin Extent = 
               Top = 6
               Left = 1064
               Bottom = 95
               Right = 1233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "zne"
            Begin Extent = 
               Top = 6
               Left = 866
               Bottom = 125
               Right = 1026
            End
            DisplayFlags = 280
            TopColumn = 0
 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwStore';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'        End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 22
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwStore';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwStore';

