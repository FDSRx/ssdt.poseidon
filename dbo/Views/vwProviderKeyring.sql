﻿




CREATE VIEW dbo.vwProviderKeyring AS 
SELECT
	pkey.ProviderKeyringID,
	pkey.ProviderID,
	prov.Code AS ProviderCode,
	prov.Name AS ProviderName,
	pkey.KeyringDirectionID,
	dir.Code AS KeyringDirectionCode,
	dir.Name AS KeyringDirectionName,
	pkey.AsymmetricKeyID,
	akey.EncryptionAlgorithmID AS AsymmetricKeyEncryptionAlgorithmID,
	akey.EncryptionAlgorithmCode AS AsymmetricKeyEncryptionAlgorithmCode,
	akey.EncryptionAlgorithmName AS AsymmetricKeyEncryptionAlgorithmName,
	akey.Name AS AsymmetricKeyName,
	akey.PublicKey AS AsymmetricKeyPublicKey,
	akey.PrivateKey AS AsymmetricKeyPrivateKey,
	akey.KeySize AS AsymmetricKeyKeySize,
	akey.Description AS AsymmetricKeyDescription,
	pkey.SymmetricKeyID,
	skey.EncryptionAlgorithmID AS SymmetricKeyEncryptionAlgorithmID,
	skey.EncryptionAlgorithmCode AS SymmetricKeyEncryptionAlgorithmCode,
	skey.EncryptionAlgorithmName AS SymmetricKeyEncryptionAlgorithmName,
	skey.Name AS SymmetricKeyName,
	skey.PrivateKey AS SymmetricKeyPrivateKey,
	skey.KeySize AS SymmetricKeyKeySize,
	skey.Description AS SymmetricKeyDescription,
	pkey.rowguid,
	pkey.DateCreated,
	pkey.DateModified,
	pkey.CreatedBy,
	pkey.ModifiedBy
--SELECT *
--SELECT TOP 1 *	
FROM dbo.ProviderKeyring pkey
	JOIN dbo.Provider prov
		ON prov.ProviderID = pkey.ProviderID
	JOIN secur.KeyringDirection dir
		ON dir.KeyringDirectionID = pkey.KeyringDirectionID
	LEFT JOIN secur.vwAsymmetricKey akey
		ON akey.AsymmetricKeyID = pkey.AsymmetricKeyID
	LEFT JOIN secur.vwSymmetricKey skey
		ON skey.SymmetricKeyID = pkey.SymmetricKeyID








		
		


