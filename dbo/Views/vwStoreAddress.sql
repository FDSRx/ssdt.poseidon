﻿




CREATE VIEW [dbo].[vwStoreAddress] AS
SELECT
	sto.BusinessEntityID AS BusinessID,
	sto.Nabp,
	sto.Name AS BusinessName,
	ba.AddressTypeID,
	at.Code AS AddressTypeCode,
	at.Name AS AddressTypeName,
	ba.AddressID,
	addr.AddressLine1,
	addr.AddressLine2,
	addr.City,
	sp.StateProvinceCode AS State,
	addr.StateProvinceID,
	sp.CountryRegionCode,
	sp.Name AS StateName,
	sp.IsOnlyStateProvince,
	sp.TimeZoneID,
	tz.Code AS TimeZoneCode,
	tz.Prefix AS TimeZonePrefix,
	tz.Location AS TimeZoneLocation,
	tz.GMT AS TimeZoneGMT,
	tz.Offset AS TimeZoneOffset,
	addr.rowguid,
	addr.DateCreated,
	addr.DateModified,
	addr.CreatedBy,
	addr.ModifiedBy
--SELECT *
FROM dbo.Store sto
	JOIN dbo.BusinessEntityAddress ba
		ON ba.BusinessEntityID = sto.BusinessEntityID
	JOIN dbo.AddressType at
		ON at.AddressTypeID = ba.AddressTypeID
	JOIN dbo.Address addr
		ON addr.AddressID = ba.AddressID
	LEFT JOIN dbo.StateProvince sp
		ON sp.StateProvinceID = addr.StateProvinceID
	LEFT JOIN dbo.TimeZone tz
		ON tz.TimeZoneID = sp.TimeZoneID
