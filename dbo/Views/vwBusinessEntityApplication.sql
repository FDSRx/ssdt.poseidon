﻿




CREATE VIEW dbo.vwBusinessEntityApplication AS
SELECT
	ba.BusinessEntityApplicationID,
	ba.BusinessEntityID,
	biz.BusinessEntityTypeID,
	biztyp.Code AS BusinessEntityTypeCode,
	biztyp.Name AS BusinessEntityTypeName,
	COALESCE(sto.Name, chn.Name, LTRIM(RTRIM(ISNULL(psn.FirstName, '') + ' ' + ISNULL(psn.LastName, ''))) ) AS BusinessEntityName,
	ba.ApplicationID,
	app.ApplicationTypeID,
	apptyp.Code AS ApplicationTypeCode,
	apptyp.Name AS ApplicationTypeName,
	app.Code AS ApplicationCode,
	app.Name AS ApplicationName,
	ba.ApplicationPath,
	ba.rowguid,
	ba.DateCreated,
	ba.DateModified,
	ba.CreatedBy,
	ba.ModifiedBy
--SELECT *
--SELECT TOP 1 *
FROM dbo.BusinessEntityApplication ba
	JOIN dbo.BusinessEntity biz
		ON ba.BusinessEntityID = biz.BusinessEntityID
	LEFT JOIN dbo.BusinessEntityType biztyp
		ON biztyp.BusinessEntityTypeID = biz.BusinessEntityTypeID
	JOIN dbo.Application app
		ON app.ApplicationID = ba.ApplicationID
	LEFT JOIN dbo.ApplicationType apptyp
		ON apptyp.ApplicationTypeID = app.ApplicationTypeID
	LEFT JOIN dbo.Store sto
		ON biztyp.Code = 'STRE'
			AND sto.BusinessEntityID = ba.BusinessEntityID
	LEFT JOIN dbo.Chain chn
		ON biztyp.Code = 'CHN'
			AND chn.BusinessEntityID = ba.BusinessEntityID
	LEFT JOIN dbo.Person psn
		ON biztyp.Code IN ('PRSN', 'PSN')
			AND psn.BusinessEntityID = ba.BusinessEntityID