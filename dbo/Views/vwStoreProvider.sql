﻿



CREATE VIEW dbo.vwStoreProvider AS
SELECT
	eprov.BusinessEntityProviderID,
	eprov.BusinessEntityID AS StoreID,
	sto.Nabp,
	sto.Name AS StoreName,
	eprov.ProviderID,
	prov.Code AS ProviderCode,
	prov.Name AS ProviderName,
	eprov.Username,
	eprov.PasswordUnencrypted,
	eprov.PasswordEncrypted,
	eprov.rowguid,
	eprov.DateCreated,
	eprov.DateModified,
	eprov.CreatedBy,
	eprov.ModifiedBy
FROM dbo.BusinessEntityProvider eprov
	JOIN dbo.Business biz
		ON biz.BusinessEntityID = eprov.BusinessEntityID
	JOIN dbo.Store sto
		ON sto.BusinessEntityID = eprov.BusinessEntityID
	JOIN dbo.Provider prov
		ON prov.ProviderID = eprov.ProviderID