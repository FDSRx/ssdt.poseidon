﻿






CREATE VIEW [dbo].[vwStoreConfiguration] AS
		SELECT
			cfg.ConfigurationID,
			cfg.ApplicationID,
			app.Code AS ApplicationCode,
			app.Name AS ApplicationName,
			cfg.BusinessEntityID,
			sto.Nabp,
			sto.SourceStoreID,
			sto.Name AS StoreName,
			cfg.KeyName,
			cfg.KeyValue,
			cfg.DataTypeID,
			typ.Name AS DataTypeName,
			cfg.DataSize,
			cfgkey.Description,
			cfg.rowguid,
			cfg.DateCreated,
			cfg.DateModified,
			cfg.CreatedBy,
			cfg.ModifiedBy
		--SELECT * 
		FROM dbo.Configuration cfg
			LEFT JOIN dbo.Application app
				ON cfg.ApplicationID = app.ApplicationID
			LEFT JOIN dbo.vwStore sto
				ON cfg.BusinessEntityID = sto.BusinessEntityID
			LEFT JOIN dbo.DataType typ
				ON cfg.DataTypeID = typ.DataTypeID
			JOIN config.ConfigurationKey cfgkey
				ON cfg.KeyName = cfgkey.Name


