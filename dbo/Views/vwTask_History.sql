﻿









CREATE VIEW [dbo].[vwTask_History]
AS
SELECT
  tsk.TaskHistoryID,
  tsk.NoteID,
  	tsk.ApplicationID,
	app.Code AS ApplicationCode,
	app.Name AS ApplicationName,
	tsk.BusinessID AS BusinessID,
	biz.Name AS BusinessName,
  n.BusinessEntityID,
  enttyp.Name AS BusinessEntityTypeName,
  n.ScopeID,
  s.Code AS ScopeCode,
  s.Name AS ScopeName,
  n.NoteTypeID,
  typ.Code AS NoteTypeCode,
  typ.Name AS NoteTypeName,
  n.Title,
  n.Memo,
  n.NotebookID,
  nb.Code AS NotebookCode,
  nb.Name AS NotebookName,
  n.PriorityID,
  p.Code AS PriorityCode,
  p.Name AS PriorityName,
  tsk.AssignedToID,
  tsk.AssignedToString,
  tsk.AssignedByID,
  tsk.AssignedByString,
  tsk.DateDue,
  tsk.CompletedByID,
  tsk.CompletedByString,
  tsk.DateCompleted,
  o.OriginID,
  o.Code AS OriginCode,
  o.Name AS OriginName,
  n.OriginDataKey,
  n.AdditionalData,
  n.CorrelationKey,
  tsk.TaskStatusID,
  tsk.ParentTaskID,
  ts.Code AS TaskStatusCode,
  ts.Name AS TaskStatusName,
  tsk.rowguid,
  tsk.DateCreated,
  tsk.DateModified,
  tsk.CreatedBy,
  tsk.ModifiedBy,
  tsk.AuditGuid,
  tsk.AuditAction,
  tsk.DateAudited
FROM dbo.Task_History AS tsk
INNER JOIN dbo.Note AS n
  ON tsk.NoteID = n.NoteID
LEFT OUTER JOIN dbo.Priority AS p
  ON n.PriorityID = p.PriorityID
LEFT OUTER JOIN dbo.Scope AS s
  ON n.ScopeID = s.ScopeID
LEFT OUTER JOIN dbo.Origin AS o
  ON n.OriginID = o.OriginID
LEFT OUTER JOIN dbo.Notebook AS nb
  ON n.NotebookID = nb.NotebookID
LEFT OUTER JOIN dbo.NoteType AS typ
  ON n.NoteTypeID = typ.NoteTypeID
INNER JOIN dbo.BusinessEntity AS ent
  ON n.BusinessEntityID = ent.BusinessEntityID
INNER JOIN dbo.BusinessEntityType AS enttyp
  ON ent.BusinessEntityTypeID = enttyp.BusinessEntityTypeID
LEFT JOIN dbo.TaskStatus ts
  ON tsk.TaskStatusID = ts.TaskStatusID
  LEFT JOIN dbo.Application app
	ON app.ApplicationID = tsk.ApplicationID
LEFT JOIN dbo.Business biz
	ON biz.BusinessEntityID = tsk.BusinessID 










