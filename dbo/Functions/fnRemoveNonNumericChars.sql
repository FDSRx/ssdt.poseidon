﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/15/2013
-- Description:	Remove non-numeric characters from string
-- SAMPLE CALL: SELECT dbo.fnRemoveNonNumericChars('555.555-555')
-- =============================================
CREATE FUNCTION [dbo].[fnRemoveNonNumericChars](
	@Text VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	IF ISNULL(@Text, '') = '' RETURN @Text;
	
    WHILE PATINDEX('%[^0-9]%', @Text) > 0
    BEGIN
        SET @Text = STUFF(@Text, PATINDEX('%[^0-9]%', @Text), 1, '')
    END
    
    RETURN @Text
END

