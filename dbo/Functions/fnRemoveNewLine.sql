﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/14/2013
-- Description:	Remove carriage returns and line feeds
-- SAMPLE CALL: SELECT dbo.fnRemoveNewLine('Hello World!');
-- SAMPLE CALL: 
/*
DECLARE @Line VARCHAR(MAX) = '
Hello 
World
';
SELECT @Line;
SELECT dbo.fnRemoveNewLine(@Line);
*/
-- =============================================
CREATE FUNCTION [dbo].[fnRemoveNewLine]
(
	@Str VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result VARCHAR(MAX);

	-- Add the T-SQL statements to compute the return value here
	SET @Result = REPLACE(REPLACE(@Str, CHAR(13), ''), CHAR(10), '')

	-- Return the result of the function
	RETURN @Result;

END

