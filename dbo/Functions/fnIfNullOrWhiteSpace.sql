﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/3/2013
-- Description:	if a value is null or has any white space then return a specified value in its place.
-- SAMPLE CALL: SELECT dbo.fnIfNullOrWhiteSpace('', 'hello world from empty');
-- SAMPLE CALL: SELECT dbo.fnIfNullOrWhiteSpace(null, 'hello world from null');
-- SAMPLE CALL: SELECT dbo.fnIfNullOrWhiteSpace('            ', 'hello world from whitespace');
-- =============================================
CREATE FUNCTION [dbo].[fnIfNullOrWhiteSpace]
(
	@Value VARCHAR(MAX),
	@ExceptionValue VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result VARCHAR(MAX);
	
	SET @Result = @Value;

	IF dbo.fnIsNullOrWhiteSpace(@Result) = 1
	BEGIN
		SET @Result = @ExceptionValue;
	END

	-- Return the result of the function
	RETURN @Result;

END

