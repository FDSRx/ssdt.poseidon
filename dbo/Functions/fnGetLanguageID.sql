﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/7/2013
-- Description:	Get language ID by code
-- SAMPLE CALL: SELECT dbo.fnGetLanguageID('en');
-- =============================================
CREATE FUNCTION [dbo].[fnGetLanguageID]
(
	@Key VARCHAR(15)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT LanguageID FROM dbo.Language WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT LanguageID FROM dbo.Language WHERE LanguageID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END

