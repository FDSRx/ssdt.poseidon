﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/20/2013
-- Description:	Decrypts encrypted string
-- SAMPLE CALL: SELECT dbo.fnDecrypt('010000001A40076C46D1C46C40B6E71F295B7B94500A0EE0280D523A')
-- =============================================
CREATE FUNCTION [dbo].[fnDataDecrypt]
(
	@Data VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	--------------------------------------------------
	-- Local variables
	--------------------------------------------------
	DECLARE @EncryptedBinary VARBINARY(MAX);
	DECLARE @EncryptionKey VARCHAR(255) = 'N0GU3SS1NGmyS3cr3tK3y0rM@ybeY0UW1llGue$$N0W@yt0t311';
	DECLARE @DecryptedData VARCHAR(MAX);

	SET @EncryptedBinary = CONVERT(VARBINARY(MAX), @Data, 2);
	
	SET @DecryptedData = DecryptByPassPhrase(@EncryptionKey, @EncryptedBinary);

	-- Return the result of the function
	RETURN @DecryptedData;

END

