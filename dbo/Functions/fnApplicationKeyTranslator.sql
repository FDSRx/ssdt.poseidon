﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/19/2014
-- Description: Gets the translated Application key or keys.
-- References: dbo.fnSlice
-- References: dbo.fnSplit

-- SAMPLE CALL: SELECT dbo.fnApplicationKeyTranslator(1, 'ApplicationID')
-- SAMPLE CALL: SELECT dbo.fnApplicationKeyTranslator('MPC', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnApplicationKeyTranslator(3, DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnApplicationKeyTranslator('1,2,3', 'ApplicationIDList')
-- SAMPLE CALL: SELECT dbo.fnApplicationKeyTranslator('MPC,PARX,PCG', 'CodeList')
-- SAMPLE CALL: SELECT dbo.fnApplicationKeyTranslator('MPC,PARX,PCG', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnApplicationKeyTranslator('1,2,3', DEFAULT)

-- SELECT * FROM dbo.Application
-- =============================================
CREATE FUNCTION [dbo].[fnApplicationKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END		
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'ApplicationID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ApplicationID) 
				FROM dbo.Application 
				WHERE ApplicationID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ApplicationID) 
				FROM dbo.Application 
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ApplicationID) 
				FROM dbo.Application 
				WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ApplicationID) 
				FROM dbo.Application 
				WHERE ApplicationID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ApplicationID) 
				FROM dbo.Application 
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'ApplicationIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ApplicationID), '') + ','
				FROM dbo.Application
				WHERE ApplicationID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'ApplicationCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ApplicationID), '') + ','
				FROM dbo.Application
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'ApplicationNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ApplicationID), '') + ','
				FROM dbo.Application
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ApplicationID), '') + ','
				FROM dbo.Application
				WHERE ApplicationID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ApplicationID), '') + ','
				FROM dbo.Application
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
