﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/23/2014
-- Description:	Gets the TagTypeID
-- SAMPLE CALL: SELECT dbo.fnGetTagTypeID('UD')
-- SAMPLE CALL: SELECT dbo.fnGetTagTypeID(1)
-- =============================================
CREATE FUNCTION [dbo].[fnGetTagTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT TagTypeID FROM dbo.TagType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT TagTypeID FROM dbo.TagType WHERE TagTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

