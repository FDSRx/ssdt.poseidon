﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/2/2013
-- Description:	Returns true if the string is null or the 
--			length is zero after triming the character.
-- SAMPLE CALL: SELECT dbo.fnIsNullOrWhiteSpace('     ');
-- SAMPLE CALL: SELECT dbo.fnIsNullOrWhiteSpace('true');
-- SAMPLE CALL: SELECT dbo.fnIsNullOrWhiteSpace(NULL);
-- SAMPLE CALL: SELECT dbo.fnIsNullOrWhiteSpace('');
-- =============================================
CREATE FUNCTION [dbo].[fnIsNullOrWhiteSpace]
(
	@Value VARCHAR(MAX)
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result BIT = 0;

	SET @Value = LTRIM(RTRIM(ISNULL(@Value, '')));

	IF LEN(@Value) = 0
	BEGIN
		SET @Result = 1;
	END

	-- Return the result of the function
	RETURN @Result;

END

