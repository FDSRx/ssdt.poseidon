﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/21/2015
-- Description:	Splits a delimited string.
-- Remarks: This is the newer version of the dbo.fnSplit function.  It does not exit the code if there is a null
--			or empty value provided.
-- SAMPLE CALL: SELECT * FROM dbo.fnSplitString('1,2,3,5,6,8,9,56666,324324,,64564564365543,4365,45,6435', ',')
-- SAMPLE CALL: SELECT * FROM dbo.fnSplitString(NULL, ',')
-- =============================================
CREATE FUNCTION [dbo].[fnSplitString]
(
	@String NVARCHAR(MAX),
	@Delimiter NVARCHAR(5)
)  
RETURNS @tblList TABLE 
(		
	Idx INT IDENTITY(1,1),
	Value NVARCHAR(MAX)
) 
AS  
BEGIN

	-- Local variables
	DECLARE @Idx INT       
    DECLARE @Slice NVARCHAR(MAX)  
    
    IF @Delimiter IS NULL
	BEGIN
		SET @Delimiter = ',' -- If the delimiter is null then just set it to a comma   
	END
	
	-- If a "space(' ')" is sent as the delimiter then format so that it can parse
	IF @Delimiter = ' '
	BEGIN
		SET @String = REPLACE(@String, ' ', '|');
		SET @Delimiter = '|';
	END  
      
    SET @Idx = 1           
      
    WHILE @Idx != 0       
    BEGIN       
        SET @Idx = CHARINDEX(@Delimiter, @String) 
              
        IF @Idx != 0    
		BEGIN   
            SET @Slice = LEFT(@String, @Idx - 1) 
		END      
        ELSE
		BEGIN       
            SET @Slice = @String 
		END                 

		-- Add the sliced item to the list.
        INSERT INTO @tblList(Value) VALUES(@Slice)       
  
        SET @String = SUBSTRING(@String, @Idx + LEN(@Delimiter), LEN(@String)) --RIGHT(@String, LEN(@String) - @Idx)  
             
        IF LEN(@String) = 0 
		BEGIN
			BREAK;
		END
		               
    END   


	RETURN;


END
