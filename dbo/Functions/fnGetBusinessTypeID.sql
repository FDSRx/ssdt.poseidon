﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/18/2013
-- Description:	Gets the business type id
-- SAMPLE CALL: SELECT dbo.fnGetBusinessTypeID('STRE')
-- SAMPLE CALL: SELECT dbo.fnGetBusinessTypeID('1')
-- SAMPLE CALL: SELECT dbo.fnGetBusinessTypeID('199')
-- =============================================
CREATE FUNCTION [dbo].[fnGetBusinessTypeID]
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT;

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT BusinessTypeID FROM dbo.BusinessType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @ID = (SELECT BusinessTypeID FROM dbo.BusinessType  WHERE BusinessTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @ID;

END

