﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/8/2013
-- Description:	Trims the left and right of a string.
-- =============================================
CREATE FUNCTION [dbo].[fnTrim]
(
	@Str VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	IF @Str IS NULL
	BEGIN
		RETURN @Str;
	END

	SET @Str = LTRIM(RTRIM(@Str));

	-- Return the result of the function
	RETURN @Str;

END

