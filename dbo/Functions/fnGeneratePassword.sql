﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Generate random passwords
-- SAMPLE CALL: SELECT dbo.fnGeneratePassword(10)
-- =============================================
CREATE FUNCTION [dbo].[fnGeneratePassword]
(
	@PasswordLength INT
)
RETURNS VARCHAR(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Password VARCHAR(50)	

	DECLARE @ValidCharacters   VARCHAR(100)
	DECLARE @PasswordIndex    INT
	DECLARE @CharacterLocation   INT

	SET @ValidCharacters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890'

	SET @PasswordIndex = 1
	SET @Password = ''


	WHILE @PasswordIndex <= @PasswordLength
	BEGIN
		
		SELECT @CharacterLocation = ABS(CAST(CAST(_NewID AS VARBINARY) AS INT)) % LEN(@ValidCharacters) + 1
		FROM dbo.vwNewID

		SET @Password = @Password + SUBSTRING(@ValidCharacters, @CharacterLocation, 1)

		SET @PasswordIndex = @PasswordIndex + 1
		
	END


	RETURN @Password

END

