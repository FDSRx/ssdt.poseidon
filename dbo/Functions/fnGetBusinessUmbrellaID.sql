﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/23/2013
-- Description:	Gets the business level (highest level) at which a business operates
-- SAMPLE CALL: SELECT dbo.fnGetBusinessUmbrellaID(5949, 'CUSDIR')
-- SAMPLE CALL: SELECT dbo.fnGetBusinessUmbrellaID(5949, 4)
-- SAMPLE CALL: SELECT dbo.fnGetBusinessUmbrellaID(6225, 4)
-- SAMPLE CALL: SELECT dbo.fnGetBusinessUmbrellaID('78BD319E-6721-47CF-9C5A-B38D4633B39E', 4)
-- =============================================
CREATE FUNCTION [dbo].[fnGetBusinessUmbrellaID] 
(
	@BusinessKey VARCHAR(50),
	@ServiceKey VARCHAR(50)
)
RETURNS BIGINT
AS
BEGIN

	-- Declare the return variable here
	DECLARE 
		@BusinessID BIGINT,
		@BusinessUmbrellaID BIGINT,
		@ServiceID INT
	

	-- Retrieve IDs
	SET @BusinessID = dbo.fnGetBusinessID(@BusinessKey);
	SET @ServiceID = dbo.fnGetServiceID(@ServiceKey);

	-- Retrieve business umbrella ID
	SELECT @BusinessUmbrellaID = BusinessEntityUmbrellaID 
	FROM dbo.BusinessEntityServiceUmbrella 
	WHERE BusinessEntityID = @BusinessID
		AND ServiceID = @ServiceID
	


	-- Return the result of the function
	RETURN @BusinessUmbrellaID;

END

