﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/7/2014
-- Description:	Gets the InteractionTypeID from a unique identifier.
-- SAMPLE CALL: SELECT dbo.fnGetInteractionTypeID('LC')
-- SAMPLE CALL: SELECT dbo.fnGetInteractionTypeID(5)
-- =============================================
CREATE FUNCTION [dbo].[fnGetInteractionTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT InteractionTypeID FROM dbo.InteractionType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT InteractionTypeID FROM dbo.InteractionType WHERE InteractionTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

