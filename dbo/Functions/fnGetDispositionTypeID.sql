﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/7/2014
-- Description:	Gets the InteractionTypeID from a unique identifier.
-- SAMPLE CALL: SELECT dbo.fnGetDispositionTypeID('S')
-- SAMPLE CALL: SELECT dbo.fnGetDispositionTypeID(5)
-- =============================================
CREATE FUNCTION [dbo].[fnGetDispositionTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT DispositionTypeID FROM dbo.DispositionType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT DispositionTypeID FROM dbo.DispositionType WHERE DispositionTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

