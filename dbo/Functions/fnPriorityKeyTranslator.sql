﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description: Gets the translated Priority key or keys.
-- References: dbo.fnSlice
-- SAMPLE CALL: SELECT dbo.fnPriorityKeyTranslator(1, 'PriorityID')
-- SAMPLE CALL: SELECT dbo.fnPriorityKeyTranslator('MED', 'Code')
-- SAMPLE CALL: SELECT dbo.fnPriorityKeyTranslator('CRIT', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnPriorityKeyTranslator(1, DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnPriorityKeyTranslator('1,2,3', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnPriorityKeyTranslator('NONE,LOW,MED', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnPriorityKeyTranslator('None,Critical,High', 'NameList')
-- SELECT * FROM dbo.Priority
-- =============================================
CREATE FUNCTION [dbo].[fnPriorityKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END		
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'PriorityID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, PriorityID) 
				FROM dbo.Priority 
				WHERE PriorityID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, PriorityID) 
				FROM dbo.Priority 
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, PriorityID) 
				FROM dbo.Priority WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, PriorityID) 
				FROM dbo.Priority 
				WHERE PriorityID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, PriorityID) 
				FROM dbo.Priority 
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'PriorityIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, PriorityID), '') + ','
				FROM dbo.Priority
				WHERE PriorityID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'PriorityCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, PriorityID), '') + ','
				FROM dbo.Priority
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'PriorityNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, PriorityID), '') + ','
				FROM dbo.Priority
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, PriorityID), '') + ','
				FROM dbo.Priority
				WHERE PriorityID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, PriorityID), '') + ','
				FROM dbo.Priority
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
