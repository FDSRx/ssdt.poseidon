﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Writes an xml element string
-- SAMPLE CALL: SELECT dbo.fnXmlWriteElementString('<Invoice>', '10.00')
-- SAMPLE CALL: SELECT dbo.fnXmlWriteElementString('InvoiceNumber', 'XYWX&123') -- special encoding test
-- SAMPLE CALL: SELECT dbo.fnXmlWriteElementString('Html', '<div>hello world & hello mars</div>') -- Html encoding test
-- =============================================
CREATE FUNCTION [dbo].[fnXmlWriteElementString]
(
	@Element VARCHAR(500),
	@Value VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN


	DECLARE 
		@ElementString VARCHAR(MAX)
	
	
	-------------------------------------
	-- Clone
	-------------------------------------
	SET @ElementString = @Element;
	
	-------------------------------------
	-- Clean element
	-------------------------------------
	SET @Element = REPLACE(@Element, '<', '');
	SET @Element = REPLACE(@Element, '>', '');
	SET @Element = REPLACE(@Element, '/', '');
	
	-------------------------------------
	-- Build element
	-------------------------------------
	SET @ElementString = 
		'<' + @Element + '>' +
		( SELECT  ISNULL(@Value, '') FOR XML PATH('') ) +
		'</' + @Element + '>'

	

	-- Return the result of the function
	RETURN @ElementString;

END

