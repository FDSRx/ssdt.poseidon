﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description: Gets the translated Notebook key or keys.
-- References: dbo.fnSlice
-- SAMPLE CALL: SELECT dbo.fnNotebookKeyTranslator(1, 'NotebookID')
-- SAMPLE CALL: SELECT dbo.fnNotebookKeyTranslator('INTRN', 'Code')
-- SAMPLE CALL: SELECT dbo.fnNotebookKeyTranslator('INTRN', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnNotebookKeyTranslator(1, DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnNotebookKeyTranslator('1,2,3', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnNotebookKeyTranslator('MNB,INTRN,TRSH', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnNotebookKeyTranslator('My Notebook,Internal,Trash', 'NameList')
-- SELECT * FROM dbo.Notebook
-- =============================================
CREATE FUNCTION [dbo].[fnNotebookKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END		
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'NotebookID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, NotebookID) 
				FROM dbo.Notebook 
				WHERE NotebookID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, NotebookID) 
				FROM dbo.Notebook 
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, NotebookID) 
				FROM dbo.Notebook WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, NotebookID) 
				FROM dbo.Notebook 
				WHERE NotebookID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, NotebookID) 
				FROM dbo.Notebook 
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'NotebookIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, NotebookID), '') + ','
				FROM dbo.Notebook
				WHERE NotebookID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'NotebookCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, NotebookID), '') + ','
				FROM dbo.Notebook
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'NotebookNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, NotebookID), '') + ','
				FROM dbo.Notebook
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, NotebookID), '') + ','
				FROM dbo.Notebook
				WHERE NotebookID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, NotebookID), '') + ','
				FROM dbo.Notebook
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
