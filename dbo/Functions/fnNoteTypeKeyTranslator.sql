﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description: Gets the translated NoteType key or keys.
-- References: dbo.fnSlice
-- SAMPLE CALL: SELECT dbo.fnNoteTypeKeyTranslator(1, 'NoteTypeID')
-- SAMPLE CALL: SELECT dbo.fnNoteTypeKeyTranslator('TSK', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnNoteTypeKeyTranslator(3, DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnNoteTypeKeyTranslator('1,2,3', 'NoteTypeIDList')
-- SAMPLE CALL: SELECT dbo.fnNoteTypeKeyTranslator('GNRL,TSK,STCKY', 'CodeList')
-- SAMPLE CALL: SELECT dbo.fnNoteTypeKeyTranslator('GNRL,TSK,STCKY', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnNoteTypeKeyTranslator('1,2,3', DEFAULT)
-- SELECT * FROM dbo.NoteType
-- =============================================
CREATE FUNCTION [dbo].[fnNoteTypeKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END		
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'NoteTypeID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, NoteTypeID) FROM dbo.NoteType WHERE NoteTypeID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (SELECT TOP 1 CONVERT(VARCHAR, NoteTypeID) FROM dbo.NoteType WHERE Code = @Key)
			WHEN @KeyType = 'Name' THEN (SELECT TOP 1 CONVERT(VARCHAR, NoteTypeID) FROM dbo.NoteType WHERE Name = @Key)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, NoteTypeID) FROM dbo.NoteType WHERE NoteTypeID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, NoteTypeID) FROM dbo.NoteType WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'NoteTypeIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, NoteTypeID), '') + ','
				FROM dbo.NoteType
				WHERE NoteTypeID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'NoteTypeCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, NoteTypeID), '') + ','
				FROM dbo.NoteType
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'NoteTypeNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, NoteTypeID), '') + ','
				FROM dbo.NoteType
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, NoteTypeID), '') + ','
				FROM dbo.NoteType
				WHERE NoteTypeID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, NoteTypeID), '') + ','
				FROM dbo.NoteType
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
