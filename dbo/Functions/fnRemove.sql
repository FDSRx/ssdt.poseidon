﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/15/2013
-- Description:	Replace values within the delimited list
-- SAMPLE CALL: SELECT dbo.fnRemove('555.555-5555', '.', '') --delimiter provided
-- =============================================
CREATE FUNCTION [dbo].[fnRemove]
(
	@Text VARCHAR(MAX),
	@RemoveTokens VARCHAR(MAX),
	@Delimiter VARCHAR(25)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	
	
	DECLARE @tblReplaceChrs AS TABLE (Idx INT IDENTITY(1,1), Token VARCHAR(MAX))
	
	-- if no delimiter was provided then treat the list as a single character
	IF ISNULL(@Delimiter, '') = ''
		INSERT INTO @tblReplaceChrs (Token) SELECT @RemoveTokens
	ELSE
	BEGIN
		-- if a delimiter was provided then parse the token list
		INSERT INTO @tblReplaceChrs (
			Token
		)
		SELECT DISTINCT Value
		FROM dbo.fnSplit(@RemoveTokens, @Delimiter)
	END
	
	-- set up loop counters
	DECLARE
		@CurIndex INT = 1,
		@LastIndex INT = (SELECT MAX(Idx) FROM @tblReplaceChrs)
	
	-- loop through token(s)
	WHILE @CurIndex <= @LastIndex
	BEGIN
		
		--get token to replace
		DECLARE @Token VARCHAR(MAX) = (SELECT Token FROM @tblReplaceChrs WHERE Idx = @CurIndex);
	
		-- replace token
		SET @Text = REPLACE(@Text, @Token, '');
		
		SET @CurIndex = @CurIndex + 1; -- increment loop
	END

	-- Return the result of the function
	RETURN @Text;

END

