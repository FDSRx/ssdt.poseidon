﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/16/2013
-- Description:	Gets the default store ID
-- SAMPLE CALL: SELECT dbo.fnGetStoreDefaultID()
-- =============================================
CREATE FUNCTION [dbo].[fnGetStoreDefaultID]
(
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @StoreID INT;

	-- Add the T-SQL statements to compute the return value here
	SET @StoreID = CONVERT(INT, dbo.fnGetConfigValue('DFLTSTREID'));

	-- Return the result of the function
	RETURN @StoreID;

END

