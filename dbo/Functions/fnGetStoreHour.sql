﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/15/2013
-- Description:	Get store hour
-- SAMPLE CALL: SELECT dbo.fnGetStoreHour('78BD319E-6721-47CF-9C5A-B38D4633B39E', 'STRE', 1, NULL)
-- SAMPLE CALL: SELECT dbo.fnGetStoreHour('78BD319E-6721-47CF-9C5A-B38D4633B39E', 'STRE', 'MON', NULL)
-- SAMPLE CALL: SELECT dbo.fnGetStoreHour('78BD319E-6721-47CF-9C5A-B38D4633B39E', 'STRE', 1, 'TimeStart')
-- =============================================
CREATE FUNCTION [dbo].[fnGetStoreHour]
(
	@StoreKey VARCHAR(50),
	@HourTypeCode VARCHAR(15),
	@DayKey VARCHAR(15),
	@OutputField VARCHAR(20)
)
RETURNS VARCHAR(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Time VARCHAR(50)
	
	-----------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------
	DECLARE
		@StoreID INT,
		@DayID INT

	-----------------------------------------------------------------
	-- Use smart filter to determine if ID or if token was passed.  
	-- If token, retrieve ID
	-----------------------------------------------------------------
	IF ISNUMERIC(@StoreKey) = 0
	BEGIN
		SET @StoreID = dbo.fnGetBusinessID(@StoreKey);
	END
	ELSE
	BEGIN
		SET @StoreID = CONVERT(INT, @StoreKey);
	END
	
	-----------------------------------------------------------------
	-- Use smart filter to determine if ID or day code was passed
	-----------------------------------------------------------------
	IF ISNUMERIC(@DayKey) = 0
	BEGIN
		SET @DayID = (SELECT DayID FROM dbo.Days WHERE Code = @DayKey);
	END
	ELSE
	BEGIN
		SET @DayID = CONVERT(INT, @DayKey);
	END
	
	-----------------------------------------------------------------
	-- Get store hour
	-----------------------------------------------------------------
	SELECT 
		@Time =CASE
			WHEN @OutputField = 'TimeStart' THEN CONVERT(VARCHAR, sto.TimeStart)
			WHEN @OutputField = 'TimeEnd' THEN CONVERT(VARCHAR, sto.TimeEnd)
			WHEN @OutputField = 'TimeText' THEN sto.TimeText
			ELSE sto.TimeText
		END
	--SELECT *
	FROM dbo.StoreHours sto
		JOIN dbo.HourType typ
			ON sto.HourTypeID = typ.HourTypeID
		JOIN dbo.Days d
			ON sto.HourTypeID = typ.HourTypeID
	WHERE sto.StoreID = @StoreID
		AND sto.DayID = @DayID
		AND typ.Code = @HourTypeCode
		
		
		

	-- Return the result of the function
	RETURN @Time;

END

