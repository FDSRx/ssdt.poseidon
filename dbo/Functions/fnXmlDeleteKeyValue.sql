﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/19/2013
-- Description:	Deletes an XML key value pair
-- =============================================
CREATE FUNCTION [dbo].[fnXmlDeleteKeyValue]
(
	@Xml XML,
	@Key NVARCHAR(255)
)
RETURNS XML
AS
BEGIN

	-- Declare the return variable here
	DECLARE @XmlOut NVARCHAR(MAX);
	
	SET @XmlOut = CONVERT(VARCHAR(MAX), @Xml);
	
	--------------------------------------------------------
	-- Xml structure tags
	--------------------------------------------------------
	DECLARE
		@RootStartTag NVARCHAR(255) = '<KeyValueList>',
		@RootEndTag NVARCHAR(255) = '</KeyValueList>',
		@KeyValueStartTag NVARCHAR(255) = '<KeyValue>',
		@KeyValueEndTag NVARCHAR(255) = '</KeyValue>',
		@KeyStartTag NVARCHAR(255) = '<Key>',
		@KeyEndTag NVARCHAR(255) = '</Key>',
		@ValueStartTag NVARCHAR(255) = '<Value>',
		@ValueEndTag NVARCHAR(255) = '</Value>',
		@CDataStartTag NVARCHAR(255) = '<![CDATA[',
		@CDataEndTag NVARCHAR(255) = ']]>'
	
	
	-- Get the value associated with the key
	DECLARE @KeyValue VARCHAR(MAX) = dbo.fnXmlGetKeyValue(@Xml, @Key);	
	
	--------------------------------------------------------
	-- Build key/value pair
	--------------------------------------------------------
	DECLARE @KeyValuePair VARCHAR(MAX) = @KeyValueStartTag + -- start key/value node
		@KeyStartTag + @CDataStartTag + @Key + @CDataEndTag + @KeyEndTag + -- add key element
		@ValueStartTag + @CDataStartTag + ISNULL(@KeyValue, '') + @CDataEndTag + @ValueEndTag + -- add value element
		@KeyValueEndTag -- end key/value node
	
	-- Convert raw key/value to xml format
	DECLARE @KeyValuePairXml XML = CONVERT(XML, @KeyValuePair);
	
	-- Convert key value pair back to string format so that character representation will be the same when comparing string lines
	SET @KeyValuePair = CONVERT(VARCHAR(MAX), @KeyValuePairXml);	
	
	--------------------------------------------------------
	-- Find xml line item and remove
	--------------------------------------------------------
	SET @XmlOut = REPLACE(@XmlOut, @KeyValuePair, '');
	
	-- Return Xml object
	RETURN CONVERT(XML, @XmlOut);

END

