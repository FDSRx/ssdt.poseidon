﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/11/2013
-- Description:	Escapes special characters in a search string
-- SAMPLE CALL: SELECT dbo.fnEscapeSpecialChars('Hellow_World');
-- =============================================
CREATE FUNCTION [dbo].[fnEscapeSpecialChars]
(
	@String VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	
	SET @String = REPLACE(REPLACE(REPLACE(@String, '[', '[[]'), '%', '[%]'), '_', '[_]');

	-- Return the result of the function
	RETURN @String;

END

