﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/14/2014
-- Description:	Get the LogEntryType object record identifier.

-- SAMPLE CALL: SELECT dbo.fnGetLogEntryTypeID('INFO');
-- SAMPLE CALL: SELECT dbo.fnGetLogEntryTypeID(2);
-- =============================================
CREATE FUNCTION [dbo].fnGetLogEntryTypeID
(
	@Key VARCHAR(15)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT LogEntryTypeID FROM dbo.LogEntryType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT LogEntryTypeID FROM dbo.LogEntryType WHERE LogEntryTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END

