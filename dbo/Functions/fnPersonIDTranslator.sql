﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description: Gets the PersonID.
-- SAMPLE CALL: SELECT dbo.fnPersonIDTranslator(66969, 'Id')
-- SAMPLE CALL: SELECT dbo.fnPersonIDTranslator(800000, DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnPersonIDTranslator(135590, 'PatientID')
-- SELECT * FROM dbo.Person
-- =============================================
CREATE FUNCTION [dbo].[fnPersonIDTranslator]
(
	@Key VARCHAR(50),
	@KeyType VARCHAR(50) = NULL
)
RETURNS BIGINT
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Id BIGINT		
	
	SET @Id = 
		CASE
			WHEN @KeyType IN ('Id', 'PersonID') AND ISNUMERIC(@Key) = 1 THEN (
				SELECT TOP 1 BusinessEntityID 
				FROM dbo.Person 
				WHERE BusinessEntityID = CONVERT(BIGINT, @Key)
			)
			WHEN @KeyType = 'PatientID' AND ISNUMERIC(@Key) = 1 THEN (
				SELECT TOP 1 PersonID 
				FROM phrm.Patient 
				WHERE PatientID = CONVERT(BIGINT, @Key)
			)			
			WHEN @KeyType = 'CredentialEntityID' AND ISNUMERIC(@Key) = 1 THEN (
				SELECT TOP 1 ISNULL(PersonID, CredentialEntityID) 
				FROM creds.CredentialEntity 
				WHERE CredentialEntityID = CONVERT(BIGINT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 THEN (
				SELECT TOP 1 BusinessEntityID 
				FROM dbo.Person 
				WHERE BusinessEntityID = CONVERT(BIGINT, @Key)
			)
			ELSE NULL
		END	

	-- Return the result of the function
	RETURN @Id;

END
