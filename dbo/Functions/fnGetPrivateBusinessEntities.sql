﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/19/2014
-- Description:	Returns a list of Business Entities that are considered private to the
--		provided criteria.
-- SAMPLE CALL: SELECT * FROM dbo.fnGetPrivateBusinessEntities(NULL, 5949, 3);
-- SELECT * FROM dbo.BusinessEntityPrivate
-- =============================================
CREATE FUNCTION [dbo].[fnGetPrivateBusinessEntities]
(
	@ApplicationKey VARCHAR(25) = NULL,
	@BusinessKey VARCHAR(25) = NULL,
	@ServiceKey VARCHAR(25) = NULL
)
RETURNS @tblPrivateEntities TABLE (
	Idx BIGINT IDENTITY(1,1),
	BusinessEntityID BIGINT
)
AS
BEGIN

	------------------------------------------------------------------------
	-- Declare and translate keys to their applicable Ids.
	------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessID BIGINT = dbo.fnGetBusinessID(@BusinessKey),
		@ServiceID INT = dbo.fnGetServiceID(@ServiceKey)
	
	
	------------------------------------------------------------------------
	-- Retrieve private entities.
	------------------------------------------------------------------------
	INSERT INTO @tblPrivateEntities (
		BusinessEntityID
	)
	SELECT
		BusinessEntityID
	FROM dbo.BusinessEntityPrivate
	WHERE ( @ApplicationID IS NULL OR ApplicationID = @ApplicationID )
		AND ( @BusinessID IS NULL OR BusinessID = @BusinessID )
		AND ( @ServiceID IS NULL OR ServiceID = @ServiceID )
		
	
	RETURN;
	
END
