﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/7/2013
-- Description:	Formats the data to null if it is empty or has whitespace.
-- SAMPLE CALL: SELECT dbo.fnToNullIfEmptyOrWhiteSpace('       ');
-- SAMPLE CALL: SELECT dbo.fnToNullIfEmptyOrWhiteSpace('       test');
-- SAMPLE CALL: SELECT dbo.fnTrimAll(NULL);
-- =============================================
CREATE FUNCTION [dbo].[fnToNullIfEmptyOrWhiteSpace]
(
	@Str VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	-- Watered down version.
	IF LTRIM(RTRIM(ISNULL(@Str, ''))) = ''
	BEGIN
		SET @Str = NULL;
	END
	
	-- Note: Does not perform well on large result sets due to recursion algorithm.
	--IF (dbo.fnTrimAll(@Str) = '')
	--BEGIN
	--	SET @Str = NULL;
	--END

	-- Return the result of the function
	RETURN @Str;

END

