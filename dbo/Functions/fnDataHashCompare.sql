﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/19/2013
-- Description:	Compares a value to a hash value to determine if they are the same
-- SAMPLE CALL: SELECT dbo.[fnDataHashCompare]('This is a sample hash', 0x7D4508E1B5EC5E08AA13978C9D5E9CD60E84FF52);
-- =============================================
CREATE FUNCTION [dbo].[fnDataHashCompare]
(
	@Value VARCHAR(300),
	@ValueHash VARBINARY(300)
)
RETURNS BIT
AS
BEGIN
-- Declare the return variable here
	DECLARE 
		@IsValid BIT = 0

	-- Add the T-SQL statements to compute the return value here
	DECLARE 
		@Hash VARBINARY(300)
	
	SET @Hash = dbo.fnDataHash(@Value);
	
	IF @Hash = @ValueHash
		SET @IsValid = 1;

	-- Return the result of the function
	RETURN @IsValid;

END

