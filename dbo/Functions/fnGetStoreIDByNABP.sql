﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/26/2013
-- Description:	Get store id from source id
-- SAMPLE CALL: SELECT dbo.[fnGetStoreIDByNABP](4228688)
-- =============================================
CREATE FUNCTION [dbo].[fnGetStoreIDByNABP]
(
	@NABP VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @BusinessEntityID INT

	-- Add the T-SQL statements to compute the return value here
	SELECT @BusinessEntityID = BusinessEntityID
	FROM dbo.Store
	WHERE NABP = @NABP

	-- Return the result of the function
	RETURN @BusinessEntityID;

END

