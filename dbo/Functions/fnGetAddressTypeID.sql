﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/12/2013
-- Description:	Gets an address type id 
-- SAMPLE CALL: SELECT dbo.fnGetAddressTypeID('HME')
-- SELECT * FROM dbo.AddressType
-- =============================================
CREATE FUNCTION [dbo].[fnGetAddressTypeID]
(
	@Key VARCHAR(25)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @AddressTypeID INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @AddressTypeID = (SELECT AddressTypeID FROM dbo.AddressType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @AddressTypeID = (SELECT AddressTypeID FROM dbo.AddressType WHERE AddressTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @AddressTypeID;

END

