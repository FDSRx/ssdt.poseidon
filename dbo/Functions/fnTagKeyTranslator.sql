﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/18/2014
-- Description: Gets the translated Tag key or keys.
-- References: dbo.fnSlice
-- SAMPLE CALL: SELECT dbo.fnTagKeyTranslator(1, 'TagID')
-- SAMPLE CALL: SELECT dbo.fnTagKeyTranslator('Med. Sync', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnTagKeyTranslator('1,2,3', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnTagKeyTranslator('Med. Therapy Management,Complete Med. Review,Targeted Intervention Program', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnTagKeyTranslator('Med. Sync, Med Sync, Med. Sync Fill, Med Sync Fill, Med Sync Reminder, Med. Sync Reminder', DEFAULT);
-- SELECT * FROM dbo.Tag
-- =============================================
CREATE FUNCTION [dbo].[fnTagKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END		
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'TagID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TagID) 
				FROM dbo.Tag 
				WHERE TagID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TagID) 
				FROM dbo.Tag 
				WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TagID) 
				FROM dbo.Tag 
				WHERE TagID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TagID) 
				FROM dbo.Tag 
				WHERE Name = @Key
			)
			WHEN @KeyType IN ('IdList', 'TagIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TagID), '') + ','
				FROM dbo.Tag
				WHERE TagID IN (SELECT LTRIM(RTRIM(Value)) FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'TagNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TagID), '') + ','
				FROM dbo.Tag
				WHERE Name IN (SELECT LTRIM(RTRIM(Value)) FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TagID), '') + ','
				FROM dbo.Tag
				WHERE TagID IN (SELECT LTRIM(RTRIM(Value)) FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TagID), '') + ','
				FROM dbo.Tag
				WHERE Name IN (SELECT LTRIM(RTRIM(Value)) FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
