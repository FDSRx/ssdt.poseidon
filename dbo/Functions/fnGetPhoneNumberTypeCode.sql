﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/13/2015
-- Description:	Gets a PhoneNumberType object code.

-- SAMPLE CALL: SELECT dbo.[fnGetPhoneNumberTypeCode]('HME')
-- SAMPLE CALL: SELECT dbo.[fnGetPhoneNumberTypeCode](1)

-- SELECT * FROM dbo.PhoneNumberType
-- =============================================
CREATE FUNCTION [dbo].[fnGetPhoneNumberTypeCode]
(
	@Key VARCHAR(25)
)
RETURNS VARCHAR(50)
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Code VARCHAR(50)

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Code = (SELECT Code FROM dbo.PhoneNumberType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Code = (SELECT Code FROM dbo.PhoneNumberType WHERE PhoneNumberTypeID = @Key);
	END	

	-- Return the result of the function
	RETURN @Code;

END

