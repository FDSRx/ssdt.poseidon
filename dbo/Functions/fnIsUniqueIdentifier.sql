﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/02/2013
-- Description:	Determines if the string is a unique identifier
-- SAMPLE CALL: SELECT dbo.fnIsUniqueIdentifier(NEWID()) -- good
-- SAMPLE CALL: SELECT dbo.fnIsUniqueIdentifier('testguid') -- bad
-- =============================================
CREATE FUNCTION [dbo].[fnIsUniqueIdentifier]
(
	@Str VARCHAR(50)
)
RETURNS BIT
AS
BEGIN

	DECLARE @IsGuid BIT = 0;
	
	SET @IsGuid = 
		CASE WHEN @Str LIKE '[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0‌​-9a-fA-F]-[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]-[0-9a-fA-F][0-9a-fA-F][0-9‌​a-fA-F][0-9a-fA-F]-[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]-[0-9a-fA-F][0-9a-‌​fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F‌​][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]' THEN 1 ELSE 0 END;
	
	RETURN @IsGuid;

END

