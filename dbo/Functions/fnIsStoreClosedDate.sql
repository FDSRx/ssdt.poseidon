﻿
-- =============================================
-- Author:		John Kim
-- Create date: 10/10/2014
-- Description:	Determines whether the specified date falls on store's holiday/weekend schedule. 
-- SAMPLE CALL:
/*
DECLARE 
	@StoreKey VARCHAR(50) = '3374662',
	@StoreID BIGINT

SET @StoreID = Poseidon.dbo.fnGetStoreIDByNABP(@StoreKey);

SELECT dbo.fnIsStoreClosedDate(@StoreID, '2016-1-14');
SELECT dbo.fnIsStoreClosedDate(@StoreID, '2016-1-17');

*/

-- SELECT * FROM dbo.StoreHours
-- SELECT * FROM dbo.Store WHERE Nabp = '3374662'
-- =============================================
CREATE FUNCTION [dbo].[fnIsStoreClosedDate] 
(
	@StoreID VARCHAR(50),
	@CheckDate DATE
)
RETURNS BIT
AS
BEGIN

	DECLARE 
		@IsClosed BIT,
		@DatePart TINYINT,
		@NumClosedDays TINYINT

	DECLARE @StoreClosedDayIDTable TABLE (
		RowNumber INT IDENTITY,
		DayID INT
	)

	SET @IsClosed = 0;

	SELECT @DatePart = ((DATEPART(DW, @checkDate) + @@DATEFIRST - 2) % 7 + 1)


	INSERT INTO @StoreClosedDayIDTable (
		DayID
	)
	SELECT 
		DayID 
	FROM dbo.fnGetStoreClosedDays(@StoreID)

	SELECT 
		@NumClosedDays = COUNT(*) 
	FROM @StoreClosedDayIDTable 
	WHERE DayID = @DatePart;

	IF @NumClosedDays > 0
	BEGIN
		SET @IsClosed = 1;
	END

	RETURN @IsClosed;
END

