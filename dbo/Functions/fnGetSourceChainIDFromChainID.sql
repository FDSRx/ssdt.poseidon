﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/28/2014
-- Description:	Gets the source chain ID 
-- SAMPLE CALL: SELECT dbo.fnGetSourceChainIDFromChainID(113)
-- SELECT * FROM dbo.Chain 
-- SELECT * FROM dbo.BusinessEntity WHERE BusinessEntityID = 109
-- =============================================
CREATE FUNCTION [dbo].[fnGetSourceChainIDFromChainID]
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@ChainID INT,
		@SourceChainID INT

	SET @ChainID = dbo.fnGetBusinessID(@key);
	
	SELECT TOP 1
		@SourceChainID = SourceChainID
	FROM dbo.Chain
	WHERE BusinessEntityID = @ChainID

	-- Return the result of the function
	RETURN @SourceChainID;

END

