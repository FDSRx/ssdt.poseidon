﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/20/2013
-- Description:	Encrypts a string
-- SAMPLE CALL: SELECT dbo.fnEncrypt('test')
-- =============================================
CREATE FUNCTION [dbo].[fnDataEncrypt]
(
	@Data VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result VARCHAR(MAX);
	
	--------------------------------------------------
	-- Local variables
	--------------------------------------------------
	DECLARE @EncryptedBinary VARBINARY(MAX);
	DECLARE @EncryptionKey VARCHAR(255) = 'N0GU3SS1NGmyS3cr3tK3y0rM@ybeY0UW1llGue$$N0W@yt0t311';
	
	-- Binary representation
	SET @EncryptedBinary = EncryptByPassPhrase(@EncryptionKey, @Data);
	
	-- VARCHAR representation
	SET @Result = CONVERT(VARCHAR(MAX), @EncryptedBinary, 2);
	

	-- Return the result of the function
	RETURN @Result;

END

