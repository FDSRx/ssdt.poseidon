﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/5/2013
-- Description:	Gets the business config value for the supplied key
-- SAMPLE CALL: SELECT dbo.fnGetAppBusinessConfigValue(NULL, NULL, 'CredentialLockOutAttempts')
-- SAMPLE CALL: SELECT dbo.fnGetAppBusinessConfigValue(2, NULL, 'CredTokenDurationMin')

-- SELECT * FROM dbo.Configuration
-- =============================================
CREATE FUNCTION [dbo].[fnGetAppBusinessConfigValue]
(
	@ApplicationKey VARCHAR(50),
	@BusinessEntityKey VARCHAR(50),
	@Key VARCHAR(255)
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE
		@ApplicationID INT, 
		@BusinessEntityID INT,
		@Value VARCHAR(255)
		
	SET @BusinessEntityID = dbo.fnGetBusinessID(@BusinessEntityKey);
	SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationKey);

	-- Add the T-SQL statements to compute the return value here
	SELECT
		@Value = KeyValue
	--SELECT *
	FROM dbo.Configuration
	WHERE ( @ApplicationKey IS NULL OR ApplicationID = @ApplicationID )
		AND ( @BusinessEntityKey IS NULL OR BusinessEntityID = @BusinessEntityID )
		AND KeyName = @Key

	-- Return the result of the function
	RETURN @Value

END

