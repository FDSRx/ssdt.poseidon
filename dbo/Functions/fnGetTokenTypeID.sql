﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2014
-- Description:	Gets the TokenTypeID from the inquiry.
-- SAMPLE CALL: SELECT dbo.fnGetTokenTypeID('OTR')
-- SAMPLE CALL: SELECT dbo.fnGetTokenTypeID(1)

-- SELECT * FROM dbo.TokenType
-- =============================================
CREATE FUNCTION dbo.fnGetTokenTypeID 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT TokenTypeID FROM dbo.TokenType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT TokenTypeID FROM dbo.TokenType WHERE TokenTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

