﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/13/2013
-- Description:	if a value is null or empty then return a specified value in its place.
-- SAMPLE CALL: SELECT dbo.fnIfNullorEmpty('', 'hello world from empty');
-- SAMPLE CALL: SELECT dbo.fnIfNullorEmpty(null, 'hello world from null');
-- SAMPLE CALL: SELECT dbo.fnIfNullorEmpty('hi', 'hello world from null'); -- no exception
-- =============================================
CREATE FUNCTION [dbo].[fnIfNullOrEmpty]
(
	@Value VARCHAR(MAX),
	@ExceptionValue VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result VARCHAR(MAX);
	
	SET @Result = @Value;

	IF dbo.fnIsNullOrEmpty(@Result) = 1
	BEGIN
		SET @Result = @ExceptionValue;
	END

	-- Return the result of the function
	RETURN @Result;

END

