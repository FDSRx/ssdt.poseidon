﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/6/2014
-- Description:	Get person type id.
-- SAMPLE CALL: SELECT dbo.[fnGetPersonTypeID]('PAT');
-- SAMPLE CALL: SELECT dbo.[fnGetPersonTypeID](1);
-- =============================================
CREATE FUNCTION [dbo].[fnGetPersonTypeID]
(
	@Key VARCHAR(25)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT PersonTypeID FROM dbo.PersonType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT PersonTypeID FROM dbo.PersonType WHERE PersonTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
