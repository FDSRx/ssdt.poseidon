﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description: Gets the translated NoteType key or keys.
-- SAMPLE CALL: SELECT dbo.fnNoteTypeIDTranslator(1, 'NoteTypeID')
-- SAMPLE CALL: SELECT dbo.fnNoteTypeIDTranslator('TSK', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnNoteTypeIDTranslator(3, DEFAULT)
-- SELECT * FROM dbo.NoteType
-- =============================================
CREATE FUNCTION dbo.[fnNoteTypeIDTranslator]
(
	@Key VARCHAR(50),
	@KeyType VARCHAR(50) = NULL
)
RETURNS BIGINT
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Id BIGINT		
	
	SET @Id = 
		CASE
			WHEN @KeyType IN ('Id', 'NoteTypeID') AND ISNUMERIC(@Key) = 1 THEN (
				SELECT TOP 1 NoteTypeID FROM dbo.NoteType WHERE NoteTypeID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (SELECT TOP 1 NoteTypeID FROM dbo.NoteType WHERE Code = @Key)
			WHEN @KeyType = 'Name' THEN (SELECT TOP 1 NoteTypeID FROM dbo.NoteType WHERE Name = @Key)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 THEN (
				SELECT TOP 1 NoteTypeID FROM dbo.NoteType WHERE NoteTypeID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 THEN (
				SELECT TOP 1 NoteTypeID FROM dbo.NoteType WHERE Code = @Key
			)
			ELSE NULL
		END	

	-- Return the result of the function
	RETURN @Id;

END
