﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/17/2013
-- Description:	Returns a FileCategoryID based on the key provided.
-- SAMPLE CALL: SELECT dbo.fnGetFileCategoryID(8)
-- SAMPLE CALL: SELECT dbo.fnGetFileCategoryID('PRVCYPLCY')
-- =============================================
CREATE FUNCTION [dbo].[fnGetFileCategoryID]
(
	@Key VARCHAR(25)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @ID INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT FileCategoryID FROM doc.FileCategory WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @ID = (SELECT FileCategoryID FROM doc.FileCategory WHERE FileCategoryID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @ID;

END

