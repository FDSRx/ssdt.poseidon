﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/24/2103
-- Description:	Creates/Appends xml key value pairs
/*
SAMPLE CALL:
DECLARE @KeyValueList VARCHAR(MAX)
SET @KeyValueList = dbo.fnAddKeyValueXml(@KeyValueList, '<*FN*>', 'Daniel')
SET @KeyValueList = dbo.fnAddKeyValueXml(@KeyValueList, '<*LN*>', 'Hughes')
SET @KeyValueList = dbo.fnAddKeyValueXml(@KeyValueList, '<*MI*>', 'R')
SELECT @KeyValueList
DECLARE @Xml XML = @KeyValueList
SELECT @Xml AS XML -- test if structure is valid
*/
-- =============================================
CREATE FUNCTION [dbo].[fnXmlAddKeyValue]
(
	@Xml XML,
	@Key NVARCHAR(255),
	@Value NVARCHAR(MAX)
)
RETURNS XML
AS
BEGIN
	-- Declare the return variable here
	DECLARE @XmlOut NVARCHAR(MAX)
	
	
	--------------------------------------------------------
	-- Xml structure tags
	--------------------------------------------------------
	DECLARE
		@RootStartTag NVARCHAR(255) = '<KeyValueList>',
		@RootEndTag NVARCHAR(255) = '</KeyValueList>',
		@EmptyRootTag NVARCHAR(255) = '<KeyValueList/>',
		@KeyValueStartTag NVARCHAR(255) = '<KeyValue>',
		@KeyValueEndTag NVARCHAR(255) = '</KeyValue>',
		@KeyStartTag NVARCHAR(255) = '<Key>',
		@KeyEndTag NVARCHAR(255) = '</Key>',
		@ValueStartTag NVARCHAR(255) = '<Value>',
		@ValueEndTag NVARCHAR(255) = '</Value>',
		@CDataStartTag NVARCHAR(255) = '<![CDATA[',
		@CDataEndTag NVARCHAR(255) = ']]>'
		
		
		
	
	--------------------------------------------------------
	-- Assign input string to out string
	--------------------------------------------------------
	SET @XmlOut = CONVERT(VARCHAR(MAX), @Xml)
	
	--------------------------------------------------------
	-- Remove root start and end tag (will append later)
	--------------------------------------------------------
	SET @XmlOut = REPLACE(@XmlOut, @RootStartTag, '')
	SET @XmlOut = REPLACE(@XmlOut, @RootEndTag, '')
	
	--------------------------------------------------------
	-- Append key/value pair
	--------------------------------------------------------
	SET @XmlOut = ISNULL(@XmlOut, '') + 
		@KeyValueStartTag + -- start key/value node
		@KeyStartTag + @CDataStartTag + @Key + @CDataEndTag + @KeyEndTag + -- add key element
		@ValueStartTag + @CDataStartTag + ISNULL(@Value, '') + @CDataEndTag + @ValueEndTag + -- add value element
		@KeyValueEndTag -- end key/value node
	
	-- Remove any empty root tags that may have been created.
	SET @XmlOut = REPLACE(@XmlOut, @EmptyRootTag, '');
		
	--------------------------------------------------------
	-- Attach start and end root tags
	--------------------------------------------------------
	SET @XmlOut = @RootStartTag + ISNULL(@XmlOut, '') + @RootEndTag
	


	-- Return the result of the function
	RETURN CONVERT(XML, @XmlOut)

END

