﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 5/4/2015
-- Description:	Returns the preference value for the specified business object.
-- SAMPLE CALL: SELECT dbo.fnGetBusinessEntityPreferenceValue('135590', 'DNC', DEFAULT);
-- SAMPLE CALL: SELECT dbo.fnGetBusinessEntityPreferenceValue('135590', 'DNC', 'String');
-- SAMPLE CALL: SELECT dbo.fnGetBusinessEntityPreferenceValue('135590', 'DNC', 'Bool');

-- SELECT * FROM dbo.BusinessEntityPreference
-- SELECT * FROM dbo.Preference
-- =============================================
CREATE FUNCTION dbo.fnGetBusinessEntityPreferenceValue
(
	@BusinessEntityKey VARCHAR(50),
	@PreferenceKey VARCHAR(25),
	@ValueTypeKey VARCHAR(10) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	----------------------------------------------------------------------------------
	-- Local variables.
	----------------------------------------------------------------------------------
	DECLARE
		@BusinessEntityID BIGINT = dbo.fnGetBusinessEntityID(@BusinessEntityKey),
		@PreferenceID INT = dbo.fnGetPreferenceID(@PreferenceKey),
		@Value VARCHAR(MAX)
		

	----------------------------------------------------------------------------------
	-- Retrieve result.
	----------------------------------------------------------------------------------
	SELECT
		@Value =
			CASE
				WHEN @ValueTypeKey IN ('S', 'String', 'Varchar') THEN CONVERT(VARCHAR(MAX), ValueString)
				WHEN @ValueTypeKey IN ('B', 'Boolean', 'Bool', 'Bit') THEN CONVERT(VARCHAR(MAX), ValueBoolean)
				WHEN @ValueTypeKey IN ('N', 'Numeric', 'Number', 'Decimal', 'Int', 'BigInt', 'Float') THEN CONVERT(VARCHAR(MAX), ValueNumeric)
				ELSE COALESCE(CONVERT(VARCHAR(MAX), ValueString), CONVERT(VARCHAR(MAX), ValueBoolean), CONVERT(VARCHAR(MAX), ValueNumeric))
			END
	FROM dbo.BusinessEntityPreference
	WHERE BusinessEntityID = @BusinessEntityID
		AND PreferenceID = @PreferenceID

	----------------------------------------------------------------------------------
	-- Return result.
	----------------------------------------------------------------------------------
	RETURN @Value;

END
