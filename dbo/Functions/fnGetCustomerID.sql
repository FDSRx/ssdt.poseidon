﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/29/2013
-- Description:	Gets a Customer ID
-- SAMPLE CALL: SELECT dbo.fnGetCustomerID(5949, 8);
-- =============================================
CREATE FUNCTION [dbo].[fnGetCustomerID]
(
	@BusinessKey INT,
	@PersonID INT
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@CustomerID INT,
		@BusinessEntityID INT
		
	SET @BusinessEntityID = dbo.fnGetBusinessID(@BusinessKey);

	SELECT
		@CustomerID = CustomerID
	FROM dbo.Customer
	WHERE BusinessEntityID = @BusinessEntityID
		AND PersonID = @PersonID

	-- Return the result of the function
	RETURN @CustomerID;

END

