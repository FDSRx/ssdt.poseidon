﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/20/2014
-- Description:	Calculates a persons age
-- SAMPLE CALL: SELECT dbo.fnGetAge('11/26/1996', NULL)
-- =============================================
CREATE function [dbo].[fnGetAge](
	@BirthDate AS DATETIME = NULL,
	@Now AS DATETIME
)
RETURNS INT

AS

BEGIN

	--Declare output variable
	DECLARE @Age INT = 99999;
	
	--Set Now to Now if a date was not passed
	IF @Now IS NULL
	BEGIN
		SET @Now = GETDATE();
	END

	SET @Age = DATEDIFF(yyyy, @BirthDate, @Now) - CASE WHEN DATEPART(dy, @BirthDate ) > DATEPART(dy, @Now) THEN 1 ELSE 0 END;
	
	RETURN @Age

END