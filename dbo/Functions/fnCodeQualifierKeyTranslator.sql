﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/10/2014
-- Description: Gets the translated CodeQualifier key or keys.
-- References: dbo.fnSlice
-- SAMPLE CALL: SELECT dbo.fnCodeQualifierKeyTranslator(1, 'CodeQualifierID')
-- SAMPLE CALL: SELECT dbo.fnCodeQualifierKeyTranslator('ICD9', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnCodeQualifierKeyTranslator(3, DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnCodeQualifierKeyTranslator('1,2,3', 'CodeQualifierIDList')
-- SAMPLE CALL: SELECT dbo.fnCodeQualifierKeyTranslator('ICD9,ICD10,UNKN', 'CodeList')
-- SAMPLE CALL: SELECT dbo.fnCodeQualifierKeyTranslator('ICD9,ICD10,UNKN', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnCodeQualifierKeyTranslator('1,2,3', DEFAULT)
-- SELECT * FROM dbo.CodeQualifier
-- =============================================
CREATE FUNCTION [dbo].[fnCodeQualifierKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END		
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'CodeQualifierID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, CodeQualifierID) 
				FROM dbo.CodeQualifier 
				WHERE CodeQualifierID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, CodeQualifierID) 
				FROM dbo.CodeQualifier 
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, CodeQualifierID) 
				FROM dbo.CodeQualifier 
				WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, CodeQualifierID) 
				FROM dbo.CodeQualifier 
				WHERE CodeQualifierID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, CodeQualifierID) 
				FROM dbo.CodeQualifier 
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'CodeQualifierIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, CodeQualifierID), '') + ','
				FROM dbo.CodeQualifier
				WHERE CodeQualifierID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'CodeQualifierCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, CodeQualifierID), '') + ','
				FROM dbo.CodeQualifier
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'CodeQualifierNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, CodeQualifierID), '') + ','
				FROM dbo.CodeQualifier
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, CodeQualifierID), '') + ','
				FROM dbo.CodeQualifier
				WHERE CodeQualifierID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, CodeQualifierID), '') + ','
				FROM dbo.CodeQualifier
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
