﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/29/2013
-- Description:	Get store id from source id
-- SAMPLE CALL: SELECT dbo.fnGetStoreIDFromSourceID(14661)
-- =============================================
CREATE FUNCTION [dbo].[fnGetStoreIDFromSourceID]
(
	@SourceStoreID INT
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @BusinessEntityID INT

	-- Add the T-SQL statements to compute the return value here
	SELECT @BusinessEntityID = BusinessEntityID
	FROM dbo.Store
	WHERE SourceStoreID = @SourceStoreID

	-- Return the result of the function
	RETURN @BusinessEntityID;

END

