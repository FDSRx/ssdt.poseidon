﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/24/2013
-- Description:	Formats phone number to XXX-XXX-XXXX
-- SAMPLE CALL: SELECT dbo.[fnFormatPhoneNumber]('555-555-5555') -- already formated. should come back same
-- SAMPLE CALL: SELECT dbo.[fnFormatPhoneNumber]('555555555') -- no format
-- SAMPLE CALL: SELECT dbo.[fnFormatPhoneNumber]('555.555.5555') -- formatted but not in our desired way
-- =============================================
CREATE FUNCTION [dbo].[fnFormatPhoneNumber](
	@PhoneNumber VARCHAR(25)
)
RETURNS VARCHAR(25)
BEGIN

	DECLARE @StrippedPhone VARCHAR(25) = dbo.fnRemoveNonNumericChars(@PhoneNumber)
	
	IF LEN(@StrippedPhone) <> 10 
		RETURN @PhoneNumber; -- return unformatted phonenumber as it was improperly stored or presented
	
	SET @PhoneNumber = 	SUBSTRING(@StrippedPhone, 1, 3) + '-' + SUBSTRING(@StrippedPhone, 4, 3) + '-' + SUBSTRING(@StrippedPhone, 7, 4);
   
   
    RETURN @PhoneNumber;
    
END

