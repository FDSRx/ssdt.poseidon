﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Trims all carriage returns, whitespace, etc. from the left, up until the first true character.
-- SAMPLE CALL:
/*
DECLARE @Line VARCHAR(MAX) = '    
hello world.
		The brown fox 
	Jumped over the fence...
	 and somehow did not hurt	himself.	 
	 Thank goodness.';
SELECT dbo.fnLTrimAll(@Line);	 
*/	
-- =============================================
CREATE FUNCTION [dbo].[fnLTrimAll]
(
	@String VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	
	DECLARE @Result VARCHAR(MAX) = @String;
	
	-- LTrim all
	;WITH CTE_LTrim AS (
		SELECT 1 AS i, @Result AS String
		UNION ALL
		SELECT i + 1, String = SUBSTRING(@Result, i, LEN(@Result))
		FROM CTE_LTrim
		WHERE LEFT(String, 1) IN (CHAR(10), CHAR(13), CHAR(10) + CHAR(13), CHAR(9), CHAR(32))
			AND i <=  LEN(@Result)
	)

	SELECT TOP(1) @Result = String
	FROM CTE_LTrim
	ORDER BY i DESC
	OPTION (MAXRECURSION 0);
	
	RETURN @Result;

END

