﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/5/2013
-- Description:	Gets a value from the opposite side of the string based on a delimiter
-- SAMPLE CALL: SELECT dbo.fnReverse('https://www.mypharmacyconnect.com/PharmacyAdmin/StoreFiles/labels/default/label.png', '/')
-- =============================================
CREATE FUNCTION [dbo].[fnReverse]
(
	@Text VARCHAR(MAX),
	@Delimiter VARCHAR(100)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Slice VARCHAR(MAX),
		@Index INT

	SET @Index = CHARINDEX(@Delimiter, REVERSE(@Text));
	
	IF @Index > 0
	BEGIN
		SET @Slice = REVERSE(SUBSTRING(REVERSE(@Text), 1, @Index - 1));
	END

	-- Return the result of the function
	RETURN @Slice;

END

