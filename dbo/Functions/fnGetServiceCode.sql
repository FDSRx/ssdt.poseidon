﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/19/2013
-- Description:	Gets the service code
-- SAMPLE CALL: SELECT dbo.fnGetServiceCode('MPC')
-- SAMPLE CALL: SELECT dbo.fnGetServiceCode(2)
-- =============================================
CREATE FUNCTION [dbo].[fnGetServiceCode]
(
	@Key VARCHAR(20)
)
RETURNS VARCHAR(25)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Code VARCHAR(25);

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Code = (SELECT Code FROM dbo.Service WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Code = (SELECT Code FROM dbo.Service WHERE ServiceID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Code;
	

END

