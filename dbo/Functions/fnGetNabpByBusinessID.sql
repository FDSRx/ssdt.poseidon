﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/18/2015
-- Description:	Retrieves the nabp by the business id.
-- SAMPLE CALL: SELECT  dbo.fnGetNabpByBusinessID(33);

-- SELECT * FROM dbo.Store
-- =============================================
CREATE FUNCTION dbo.fnGetNabpByBusinessID 
(
	@Id BIGINT
)
RETURNS VARCHAR(25)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Nabp VARCHAR(25)


	SELECT
		@Nabp = Nabp
	FROM dbo.Store
	WHERE BusinessEntityID = @Id

	-- Return the result of the function
	RETURN @Nabp;

END
