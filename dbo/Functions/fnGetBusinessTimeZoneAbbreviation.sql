﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 5/4/2015
-- References:
--		dbo.fnGetBusinessTimeZone
-- Description:	Returns the TimeZone abbreviation for a particular store.
-- SAMPLE CALL: SELECT dbo.fnGetBusinessTimeZoneAbbreviation('7871787', 'NABP');
-- SAMPLE CALL: SELECT dbo.fnGetBusinessTimeZoneAbbreviation('10684', 'NABP');

-- SElECT * FROM dbo.vwStore WHERE BusinessEntityID = 10684
-- SELECT * FROM dbo.TimeZone
-- SELECT * FROM dbo.StateProvince
-- =============================================
CREATE FUNCTION [dbo].[fnGetBusinessTimeZoneAbbreviation]
(
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(10)
AS
BEGIN


	---------------------------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------------------------
	DECLARE	
		@BusinessEntityID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType),
		@TimeZoneID BIGINT = NULL,
		@SupportDST INT = NULL,
		@Prefix VARCHAR(10) = NULL,
		@Abbrevation VARCHAR(25) = NULL,
		@IsInDaylightSavingsTime BIT = 0,
		@CurrentDate DATETIME = GETDATE()

	-- Debug
	-- SELECT @BusinessEntityID AS BusinessEntityID

	---------------------------------------------------------------------------------------------
	-- Retrieve TimeZoneID
	---------------------------------------------------------------------------------------------
	SELECT 
		@TimeZoneID = TimeZoneID,
		@SupportDST = SupportDST
	--SELECT *
	--SELECT TOP 1 *
	FROM dbo.Store
	WHERE BusinessEntityID = @BusinessEntityID
	
	-- If a time zone was unable to be discovered via a store, then let's fetch the time zone
	-- of the store's state.
	IF @TimeZoneID IS NULL
	BEGIN
	
		-- SELECT * FROM dbo.AddressType
		
		DECLARE 
			@AddressTypeID INT = dbo.fnGetAddressTypeID('MOFF'),
			@StateProvinceID INT = NULL,
			@AddressID BIGINT = NULL
		
		-- Retrieve the address of the business.
		SELECT 
			@AddressID = AddressID
		--SELECT *	
		--SELECT TOP 1 *
		FROM dbo.BusinessEntityAddress
		WHERE BusinessEntityID = @BusinessEntityID
			AND AddressTypeID = @AddressTypeID	
		
		-- Debug
		-- SELECT @AddressID AS AddressID		
		
		-- Use the address to determine the State/Province of the business.
		SELECT
			@StateProvinceID = StateProvinceID
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.Address
		WHERE AddressID = @AddressID
		
		-- Debug
		-- SELECT @StateProvinceID AS StateProvinceID
		
		-- Use the State/Province to determine the Time Zone in which the state resides.
		SELECT
			@TimeZoneID = TimeZoneID
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.StateProvince
		WHERE StateProvinceID = @StateProvinceID
	
	END
	
	-- Retrieve the Time Zone prefix	
	SELECT
		@Prefix = Prefix
	--SELECT *
	--SELECT TOP 1 *
	FROM dbo.TimeZone
	WHERE TimeZoneID = @TimeZoneID

	-- Debug
	-- SELECT @TimeZoneID AS TimeZoneID, @SupportDST AS SupportDST, @TimeZonePrefix AS TimeZonePrefix	
	
	-- Determine if we are in daylight savings time.
	SET @IsInDaylightSavingsTime = dbo.fnIsInDaylightSavingsTime(DEFAULT);
	
	-- Debug
	-- SELECT @IsInDaylightSavingsTime AS IsInDaylightSavingsTime
	
	-- Build the time zone abbreviation.
	SET @Abbrevation = 
		@Prefix +
		CASE
			WHEN ISNULL(@SupportDST, 0) = 1 AND ISNULL(@IsInDaylightSavingsTime, 0) = 1 THEN 'DT'
			WHEN ISNULL(@SupportDST, 0) = 1 AND ISNULL(@IsInDaylightSavingsTime, 0) = 0 THEN 'ST'
			WHEN ISNULL(@SupportDST, 0) = 0 THEN 'ST'
		END

	-- Debug
	-- SELECT @Abbrevation AS Abbrevation
	
	-- Return the result of the function
	RETURN @Abbrevation;

END
