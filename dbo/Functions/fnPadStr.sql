﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/24/2013
-- Description:	Pads string with specified character and the amount of times
-- SAMPLE CALL SELECT dbo.fnPadStr('765555', '0', '6')
-- =============================================
CREATE FUNCTION [dbo].[fnPadStr]
(
	@Text VARCHAR(4000),
	@PadChar VARCHAR(10),
	@TextLength INT
	
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @PaddedText VARCHAR(MAX)

	SELECT @PaddedText = STUFF(@Text, 1, 0, REPLICATE(@PadChar, @TextLength - LEN(@Text)))

	-- Return the result of the function
	RETURN @PaddedText;

END

