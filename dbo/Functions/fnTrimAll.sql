﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/14/2013
-- Description:	Trims all spaces, tabs, and carriage returns before and after characters.
-- SAMPLE CALL:
/*
DECLARE @Line VARCHAR(MAX) = ' 
	hello world.
		The brown fox 
	Jumped over the fence...
	 and some how did not hurt	himself.	 ';
SELECT dbo.fnTrimAll(@Line);	 
*/
-- =============================================
CREATE FUNCTION [dbo].[fnTrimAll] 
(
	@Str VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result VARCHAR(MAX) = @Str;

	-- LTrim all whitespace, carriage returns, etc.
	SET @Result = dbo.fnLTrimAll(@Result);
	
	-- RTrim all whitespace, carriage returns, etc.
	SET @Result = dbo.fnRTrimAll(@Result);

	-- Return the result of the function
	RETURN @Result;

END

