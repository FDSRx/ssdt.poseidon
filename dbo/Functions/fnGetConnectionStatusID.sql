﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/10/2014
-- Description:	Gets the ConnectionStatusID property value.
-- SAMPLE CALL: SELECT dbo.fnGetConnectionStatusID('NONE')
-- SAMPLE CALL: SELECT dbo.fnGetConnectionStatusID(1)
-- =============================================
CREATE FUNCTION [dbo].[fnGetConnectionStatusID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT ConnectionStatusID FROM dbo.ConnectionStatus WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT ConnectionStatusID FROM dbo.ConnectionStatus WHERE ConnectionStatusID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

