﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/9/2014
-- Description:	Gets the EventTypeID
-- SAMPLE CALL: SELECT dbo.fnGetEventTypeID('CRED')
-- SAMPLE CALL: SELECT dbo.fnGetEventTypeID(1)

-- SELECT * FROM dbo.EventType
-- =============================================
CREATE FUNCTION [dbo].[fnGetEventTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT EventTypeID FROM dbo.EventType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT EventTypeID FROM dbo.EventType WHERE EventTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

