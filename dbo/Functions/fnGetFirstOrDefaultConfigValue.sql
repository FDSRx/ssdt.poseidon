﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/9/2014
-- Description:	Gets the first applicable configuration value for the provided credentials.
-- SAMPLE CALL: SELECT dbo.fnGetFirstOrDefaultConfigValue(1, NULL, 'CredentialLockOutTimeMin');
-- SAMPLE CALL: SELECT dbo.fnGetFirstOrDefaultConfigValue(1, -1, 'CredentialLockOutMaxAttempts');

-- SELECT * FROM dbo.Configuration
-- =============================================
CREATE FUNCTION [dbo].[fnGetFirstOrDefaultConfigValue]
(
	@ApplicationKey VARCHAR(25) = NULL,
	@BusinessKey VARCHAR(25) = NULL,
	@Key VARCHAR(255)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Result VARCHAR(MAX) = NULL,
		@ApplicationID INT = dbo.fnGetApplicationID(@ApplicationKey),
		@BusinessEntityID BIGINT = dbo.fnGetBusinessID(@BusinessKey)


	-- Retrieve value by Application, Business, and Key
	SELECT TOP 1
		@Result = KeyValue
	FROM dbo.Configuration
	WHERE ApplicationID = @ApplicationID
		AND BusinessEntityID = @BusinessEntityID
		AND KeyName = @Key
	
	-- If a value was not found, search by Application and Key
	IF @Result IS NULL
	BEGIN
		SELECT TOP 1
			@Result = KeyValue
		FROM dbo.Configuration
		WHERE ApplicationID = @ApplicationID
			AND KeyName = @Key
	END
	
	-- If a value was not found, search by Key (global value across all apps)
	IF @Result IS NULL
	BEGIN
		SELECT TOP 1
			@Result = KeyValue
		FROM dbo.Configuration
		WHERE KeyName = @Key
	END

	-- Return the result of the function
	RETURN @Result;

END

