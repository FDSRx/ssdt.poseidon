﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/21/2014
-- Description:	Gets person ID
-- SAMPLE CALL: SELECT dbo.fnGetPersonID(66969);
-- SAMPLE CALL: SELECT dbo.fnGetPersonID(NULL);
-- =============================================
CREATE FUNCTION [dbo].[fnGetPersonID]
(
	@Key VARCHAR(50)
)
RETURNS BIGINT
AS
BEGIN

	------------------------------------------------------------------------
	-- Short circut.
	------------------------------------------------------------------------
	IF @Key IS NULL
	BEGIN
		RETURN NULL;
	END
	
	
	-- Declare the return variable here
	DECLARE @PersonID BIGINT

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0 AND dbo.fnIsUniqueIdentifier(@Key) = 1
	BEGIN
		SET @Id = (SELECT BusinessEntityID FROM dbo.Person WHERE rowguid = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT BusinessEntityID FROM dbo.Person WHERE BusinessEntityID = CONVERT(BIGINT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
