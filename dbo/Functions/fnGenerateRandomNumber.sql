﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/24/2013
-- Description:	Generate random number between
-- SAMPLE CALL: SELECT dbo.fnGenerateRandomNumber(RAND(), 0, 1000)
-- =============================================
CREATE FUNCTION [dbo].[fnGenerateRandomNumber]
(
	@Rand FLOAT, --used to circumvent the invalid use of a side operator.
	@MinNumber BIGINT,
	@MaxNumber BIGINT
)
RETURNS INT
AS
BEGIN
	
	DECLARE 
		@RandomNumber BIGINT

	SELECT @RandomNumber = CAST(((@MaxNumber + 1) - @MinNumber) * @Rand + @MinNumber AS BIGINT)
	
	RETURN @RandomNumber;

END

