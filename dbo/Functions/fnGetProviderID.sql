﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/18/2015
-- Description:	Gets the identifier of the Provider object.

-- SAMPLE CALL: SELECT dbo.fnGetProviderID('LPS')
-- SAMPLE CALL: SELECT dbo.fnGetProviderID(1)

-- SELECT * FROM dbo.Provider
-- =============================================
CREATE FUNCTION [dbo].[fnGetProviderID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT ProviderID FROM dbo.Provider WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT ProviderID FROM dbo.Provider WHERE ProviderID = @Key);
	END

	-- Return the result of the function
	RETURN @ID;

END

