﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/9/2013
-- Description:	Get default business entity id
-- SAMPLE CALL: SELECT dbo.fnGetBusinessDefaultID()
-- SELECT * FROM dbo.Configuration
-- =============================================
CREATE FUNCTION [dbo].[fnGetBusinessDefaultID]
(

)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT;

	-- Add the T-SQL statements to compute the return value here
	SET @ID = CONVERT(INT, dbo.fnGetConfigValue('DFLTBIZID'));

	-- Return the result of the function
	RETURN @ID;

END


