﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/23/2015
-- Description:	Get's the DataSource object identifier.
-- SAMPLE CALL: SELECT dbo.fnGetDataSourceID('1')
-- SAMPLE CALL: SELECT dbo.fnGetDataSourceID('EZDW')

-- SELECT * FROM dbo.DataSource
-- =============================================
CREATE FUNCTION [dbo].[fnGetDataSourceID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@ID INT;

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT TOP 1 DataSourceID FROM dbo.DataSource WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @ID = (SELECT TOP 1 DataSourceID FROM dbo.DataSource WHERE DataSourceID = @Key);
	END

	-- Return the result of the function
	RETURN @ID;

END
