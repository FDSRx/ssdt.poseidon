﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/24/2013

-- Description:	Gets service id from service code
-- SAMPLE CALL: SELECT dbo.[fnGetServiceID]('MPC')
-- SAMPLE CALL: SELECT dbo.[fnGetServiceID](1)
-- SAMPLE CALL: SELECT dbo.[fnGetServiceID]('UNKN')

-- SELECT * FROM dbo.Service
-- =============================================
CREATE FUNCTION [dbo].[fnGetServiceID]
(
	@ServiceKey VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ServiceID INT;

	-- Smart filter
	IF ISNUMERIC(@ServiceKey) = 0
	BEGIN
		SET @ServiceID = (SELECT ServiceID FROM dbo.Service WHERE Code = @ServiceKey);
	END
	ELSE
	BEGIN
		SET @ServiceID = (SELECT ServiceID FROM dbo.Service WHERE ServiceID = CONVERT(INT, @ServiceKey));
	END	

	-- Return the result of the function
	RETURN @ServiceID;

END

