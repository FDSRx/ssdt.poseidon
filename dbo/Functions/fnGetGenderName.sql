﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/4/2013
-- Description:	Gets the name of the gender.
-- SAMPLE CALL: SELECT dbo.fnGetGenderName('M');
-- SAMPLE CALL: SELECT dbo.fnGetGenderName(2);
-- SAMPLE CALL: SELECT dbo.fnGetGenderName(3);
-- =============================================
CREATE FUNCTION [dbo].[fnGetGenderName]
(
	@Key VARCHAR(25)
)
RETURNS VARCHAR(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Name VARCHAR(50)

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Name = (SELECT Name FROM dbo.Gender WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Name = (SELECT Name FROM dbo.Gender WHERE GenderID = CONVERT(INT, @Key));
	END	


	-- Return the result of the function
	RETURN @Name;

END

