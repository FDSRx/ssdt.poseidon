﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/7/2013
-- Description:	Get language ID by code
-- SAMPLE CALL: SELECT dbo.fnGetLanguageDefaultID();
-- =============================================
CREATE FUNCTION [dbo].[fnGetLanguageDefaultID]
(
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @LanguageID INT;
	
	DECLARE @DefaultLangCode VARCHAR(10) = ISNULL(dbo.fnGetConfigValue('DEFAULTLANG'), 'en');

	SELECT
		@LanguageID = LanguageID
	FROM dbo.Language
	WHERE Code = @DefaultLangCode 

	-- Return the result of the function
	RETURN @LanguageID;

END

