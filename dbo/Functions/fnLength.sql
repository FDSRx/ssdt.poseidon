﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/7/2013
-- Description:	Hack used to include whitespaces to get actual width of string.
-- =============================================
CREATE FUNCTION [dbo].[fnLength]
(
	@Str VARCHAR(MAX)
)
RETURNS BIGINT
AS
BEGIN

	DECLARE @Length INT = 0;
	
	SELECT @Length = LEN(ISNULL(@Str, '') + '_') - 1

	-- Return the result of the function
	RETURN @Length;

END

