﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description:	Creates a record hash key.
-- SAMPLE CALL: SELECT dbo.fnCreateRecordHashKey('Hello World');
-- =============================================
CREATE FUNCTION dbo.fnCreateRecordHashKey
(
	@String VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@HashKey VARCHAR(MAX)

	SET @HashKey = dbo.fnBase64Encode(HASHBYTES('SHA1', dbo.fnToHashableString(@String)));

	-- Return the result of the function
	RETURN @HashKey;

END
