﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/30/2015
-- Note: Replaces the "dbo.fnGetConfigValue" method.
-- Description:	Gets the value for the supplied criteria.
-- SAMPLE CALL: SELECT dbo.fnGetConfigurationValue(NULL, NULL, NULL, 'CredentialLockOutMaxAttempts')
-- SAMPLE CALL: SELECT dbo.fnGetConfigurationValue(2, NULL, NULL, 'CredTokenDurationMin')

-- SELECT * FROM dbo.Configuration
-- =============================================
CREATE FUNCTION [dbo].[fnGetConfigurationValue]
(
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@Key VARCHAR(255)
)
RETURNS VARCHAR(MAX)
AS
BEGIN


	---------------------------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------------------------
	DECLARE
		@ApplicationID INT, 
		@BusinessEntityID INT,
		@Value VARCHAR(255)
	
	---------------------------------------------------------------------------------------
	-- Set variables.
	---------------------------------------------------------------------------------------	
	SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationKey);	
	SET @BusinessEntityID = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType);	


	---------------------------------------------------------------------------------------
	-- Use a hierarchical approach to find the value.
	-- <Summary>
	-- Follows a series of steps to acquire the value.
	-- </Summary>
	---------------------------------------------------------------------------------------	
	-- Retrieves the value based on an exact match.
	SELECT
		@Value = KeyValue
	--SELECT *
	FROM dbo.Configuration
	WHERE ApplicationID = @ApplicationID
		AND BusinessEntityID = @BusinessEntityID
		AND KeyName = @Key		

	-- Retrieves the value based on a business match.
	IF @Value IS NULL
	BEGIN	
		
		SELECT
			@Value = KeyValue
		--SELECT *
		FROM dbo.Configuration
		WHERE ApplicationID IS NULL
			AND BusinessEntityID = @BusinessEntityID
			AND KeyName = @Key
			
	END
	
	-- Retrieves the value based on an application match.
	IF @Value IS NULL
	BEGIN
	
		SELECT
			@Value = KeyValue
		--SELECT *
		FROM dbo.Configuration
		WHERE ApplicationID = @ApplicationID
			AND BusinessEntityID IS NULL
			AND KeyName = @Key
		
	END
	
	-- Retrieves the value based on a global match
	IF @Value IS NULL
	BEGIN
	
		SELECT
			@Value = KeyValue
		--SELECT *
		FROM dbo.Configuration
		WHERE ApplicationID IS NULL
			AND BusinessEntityID IS NULL
			AND KeyName = @Key
	
	END
		
		

	-- Return the result of the function
	RETURN @Value;

END

