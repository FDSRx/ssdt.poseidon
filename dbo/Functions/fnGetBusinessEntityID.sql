﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/29/2014
-- Description:	Get business id from business token
-- SAMPLE CALL: SELECT dbo.fnGetBusinessEntityID('78BD319E-6721-47CF-9C5A-B38D4633B39E')
-- SAMPLE CALL: SELECT dbo.fnGetBusinessEntityID(6225)
-- SELECT * FROM dbo.BusinessEntity
-- =============================================
CREATE FUNCTION [dbo].[fnGetBusinessEntityID]
(
	@Key VARCHAR(50)
)
RETURNS BIGINT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID BIGINT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT BusinessEntityID FROM dbo.BusinessEntity WHERE rowguid = @Key);
	END
	ELSE
	BEGIN
		SET @ID = (SELECT BusinessEntityID FROM dbo.BusinessEntity WHERE BusinessEntityID = CONVERT(BIGINT, @Key));
	END	

	-- Return the result of the function
	RETURN @ID;

END

