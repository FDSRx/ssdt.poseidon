﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/10/2014
-- Description:	Gets the ConnectionStatus Code property value.
-- SAMPLE CALL: SELECT dbo.fnGetConnectionStatusCode('NONE')
-- SAMPLE CALL: SELECT dbo.fnGetConnectionStatusCode(1)
-- =============================================
CREATE FUNCTION [dbo].[fnGetConnectionStatusCode] 
(
	@Key VARCHAR(50)
)
RETURNS VARCHAR(25)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Code VARCHAR(25)

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Code = (SELECT Code FROM dbo.ConnectionStatus WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @Code = (SELECT Code FROM dbo.ConnectionStatus WHERE ConnectionStatusID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @Code;

END

