﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/24/2013
-- Description:	Returns a list of business entities that fall under the business umbrella
-- SAMPLE CALL: SELECT * FROM dbo.[tblStoresUnderUmbrella](277, 'PHRM')
-- =============================================
CREATE FUNCTION [dbo].[tblStoresUnderUmbrella]
(	
	@BusinessEntityUmbrellaID BIGINT,
	@ServiceKey VARCHAR(50)
)
RETURNS @tblStoreList TABLE 
(		
	Idx INT IDENTITY(1,1),
	BusinessEntityID INT,
	BusinessToken VARCHAR(50),	
	BusinessNumber VARCHAR(50),
	Name VARCHAR(150)
) 
BEGIN

	DECLARE
		@ServiceID INT
		
	SET @ServiceID = dbo.fnGetServiceID(@ServiceKey);
	
	INSERT INTO @tblStoreList (
		BusinessEntityID,
		BusinessToken,
		BusinessNumber,
		Name
	)
	SELECT DISTINCT 
		sto.BusinessEntityID,
		biz.BusinessToken,
		biz.BusinessNumber,
		sto.Name
	FROM dbo.BusinessEntityServiceUmbrella umb
		JOIN dbo.Business biz
			ON umb.BusinessEntityID = biz.BusinessEntityID
		JOIN dbo.Store sto
			ON biz.BusinessEntityID = sto.BusinessEntityID
	WHERE BusinessEntityUmbrellaID = @BusinessEntityUmbrellaID
	
	RETURN;

END
