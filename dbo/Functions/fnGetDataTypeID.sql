﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/09/2014
-- Description:	Gets the DataTypeID.
-- SAMPLE CALL: SELECT dbo.fnGetDataTypeID('varchar');

-- SELECT * FROM dbo.DataType
-- =============================================
CREATE FUNCTION dbo.fnGetDataTypeID
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT DataTypeID FROM dbo.DataType WHERE TypeOf = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT DataTypeID FROM dbo.DataType WHERE DataTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END
