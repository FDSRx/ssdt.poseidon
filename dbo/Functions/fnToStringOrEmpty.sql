﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Writes a value to string or empty
-- SAMPLE CALL: SELECT dbo.fnToStringOrEmpty('hello world');
-- SAMPLE CALL: SELECT dbo.fnToStringOrEmpty(1234);
-- SAMPLE CALL: SELECT dbo.fnToStringOrEmpty(1234.5);
-- SAMPLE CALL: SELECT dbo.fnToStringOrEmpty(NULL);
-- =============================================
CREATE FUNCTION [dbo].[fnToStringOrEmpty]
(
	@Var VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result VARCHAR(MAX);

	SET @Result = ISNULL(CONVERT(VARCHAR(MAX), @Var), '');

	-- Return the result of the function
	RETURN @Result;

END

