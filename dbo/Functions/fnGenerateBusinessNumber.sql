﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/24/2013
-- Description:	Generate public business identifier
-- SAMPLE CALL: SELECT dbo.fnCreateBusinessNumber(RAND())
-- =============================================
CREATE FUNCTION [dbo].[fnGenerateBusinessNumber]
(
	@Rand FLOAT
)
RETURNS VARCHAR(25)
AS
BEGIN

	DECLARE 
		@BusinessNumber VARCHAR(25),
		@NewID UNIQUEIDENTIFIER,
		@MOD BIGINT,
		@NumBoundry BIGINT
	
	
	-- Get new unique key
	SELECT @NewID = _NewID FROM dbo.vwNewID;
	
	-- Number to mod by	
	SET @MOD = dbo.fnGenerateRandomNumber(@Rand, 1, 9999999);
	
	
	-- randomize the mod to flucuate numbers
	SELECT @BusinessNumber = CAST(CAST(@NewID AS VARBINARY(5)) AS BIGINT) % @MOD
	
	-- define the business number as a 7 digit with leading zeros where applicable
	-- 7 digits give you millions of combinations for stores, chains, and networks
	SET @BusinessNumber = dbo.fnPadStr(@BusinessNumber, '0', 7)

	RETURN @BusinessNumber;

END

