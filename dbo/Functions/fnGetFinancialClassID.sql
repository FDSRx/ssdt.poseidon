﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/22/2014
-- Description:	Returns the FinancialClassID.
-- SAMPLE CALL: SELECT dbo.fnGetFinancialClassID(1);
-- SAMPLE CALL: SELECT dbo.fnGetFinancialClassID('UT01');
-- SELECT * FROM dbo.FinancialClass
-- =============================================
CREATE FUNCTION dbo.fnGetFinancialClassID
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT FinancialClassID FROM dbo.FinancialClass WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT FinancialClassID FROM dbo.FinancialClass WHERE FinancialClassID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
