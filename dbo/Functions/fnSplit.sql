﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/4/2013
-- Description:	splits a list
-- Change Log:
-- 11/13/2015 - dhughes - Fixes the infite loop issue caused when a space (' ') is presented as the delimiter.

-- SAMPLE CALL: SELECT * FROM dbo.fnSplit('1,2,3,5,6,8,9,56666,324324,,64564564365543,4365,45,6435', ',')
-- SAMPLE CALL: SELECT * FROM dbo.fnSplit('Now is the time for all good men to come to the aid of their country', ' ')
-- =============================================
CREATE FUNCTION [dbo].[fnSplit]
(
	@String NVARCHAR(MAX),
	@Delimiter NVARCHAR(5)
)  
RETURNS @tblList TABLE 
(		
	Idx INT IDENTITY(1,1),
	Value nvarchar(MAX)
) 
AS  
BEGIN

	-- Local variables
	DECLARE @Idx int       
    DECLARE @Slice NVARCHAR(MAX)
	DECLARE @Space VARCHAR(5) = '^*|*^' 
    
    IF @Delimiter IS NULL
	BEGIN
		SET @Delimiter = ',' -- If the delimiter is null then just set it to a comma 
	END

	-- Handle space
	IF @Delimiter = ' '
	BEGIN
		SET @String = REPLACE(@String, ' ', @Space);
		SET @Delimiter = @Space;
	END
	 
	-- If there is nothing to parse, then exit the function to save processing time.      
    IF LEN(@String) < 1 OR @String IS NULL  
	BEGIN
		RETURN;
	END  
	
	SET @Idx = 1;     
      
    WHILE @Idx != 0       
    BEGIN       
        SET @Idx = CHARINDEX(@Delimiter, @String) 
              
        IF @Idx != 0       
            SET @Slice = LEFT(@String, @Idx - 1)       
        ELSE       
            SET @Slice = @String           
                
        --IF(LEN(@Slice) > 0)  
            INSERT INTO @tblList(Value) VALUES(@Slice)       
  
        SET @String = SUBSTRING(@String, @Idx + LEN(@Delimiter), LEN(@String)) --RIGHT(@String, LEN(@String) - @Idx)  
             
        IF LEN(@String) = 0 BREAK;
               
    END   


	RETURN;


END
