﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/5/2013
-- Description:	Gets the business config value for the supplied key
-- SAMPLE CALL: SELECT dbo.[fnGetBusinessConfigValue](5949, 'SecurityQuestionCount')
-- SAMPLE CALL: SELECT dbo.[fnGetBusinessConfigValue]('F459B0E6-8A21-4BC8-AE74-15CEA3A3A27A', 'SecurityQuestionCount')
-- SELECT * FROM Poseidon.dbo.BusinessEntity WHERE BusinessEntityID = 277
-- =============================================
CREATE FUNCTION [dbo].[fnGetBusinessConfigValue]
(
	@BusinessEntityKey VARCHAR(50),
	@Key VARCHAR(255)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE
		@BusinessEntityID INT,
		@Value VARCHAR(255)
		
	SET @BusinessEntityID = dbo.fnGetBusinessID(@BusinessEntityKey);

	-- Add the T-SQL statements to compute the return value here
	SELECT
		@Value = KeyValue
	--SELECT *
	FROM dbo.Configuration
	WHERE ApplicationID IS NULL
		AND BusinessEntityID = @BusinessEntityID
		AND KeyName = @Key

	-- Return the result of the function
	RETURN @Value

END

