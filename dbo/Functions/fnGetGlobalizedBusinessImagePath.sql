﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/15/2013
-- Description:	Gets globalized image from business file directory
-- SAMPLE CALL: SELECT dbo.fnGetGlobalizedBusinessImagePath('78BD319E-6721-47CF-9C5A-B38D4633B39E', 'STREDSKTPLOGO', 'es')
-- SAMPLE CALL: SELECT dbo.fnGetGlobalizedBusinessImagePath('78BD319E-6721-47CF-9C5A-B38D4633B39E', 'STREDSKTPLOGO', 1)
-- =============================================
CREATE FUNCTION [dbo].[fnGetGlobalizedBusinessImagePath]
(
	@BusinessKey VARCHAR(50),
	@CategoryCode VARCHAR(25),
	@LanguageKey VARCHAR(10)
)
RETURNS VARCHAR(1000)
AS
BEGIN

	DECLARE 
		@ImagePath VARCHAR(1000),
		@LanguageID INT,
		@DefaultLanguageID INT
		

	DECLARE 
		@BusinessEntityID INT,
		@BusinessName VARCHAR(255)
	
	-----------------------------------------------------------------
	-- Use smart filter to determine if ID or if token was passed.  
	-- If token, retrieve ID
	-----------------------------------------------------------------
	SET @BusinessEntityID = dbo.fnGetBusinessID(@BusinessKey);
	
	----------------------------------------------------------
	-- Set language default
	----------------------------------------------------------
	SET @LanguageID = ISNULL(dbo.fnGetLanguageID(@LanguageKey), dbo.fnGetLanguageID(dbo.fnGetLanguageDefaultID()));
		
	SET @DefaultLanguageID = dbo.fnGetLanguageDefaultID();
	
	----------------------------------------------------------
	-- Create temporary holding table
	----------------------------------------------------------	
	DECLARE @tblFiles AS TABLE (
		BusinessEntityID INT, 
		FileID INT, FileName VARCHAR(1000), 
		FilePath VARCHAR(1000), 
		LanguageID INT
	)
	
	----------------------------------------------------------
	-- Obtain list of files
	----------------------------------------------------------
	INSERT INTO @tblFiles (
		BusinessEntityID,
		FileID,
		FileName,
		FilePath,
		LanguageID
	)
	SELECT
		bfile.BusinessEntityID,
		fil.FileID,
		fil.FileName,
		fil.FilePath,
		fil.LanguageID
	--SELECT *
	FROM dbo.BusinessEntityFile bfile
		JOIN doc.Files fil
			ON bfile.FileID = fil.FileID
		JOIN doc.FileType typ
			ON fil.FileTypeID = typ.FileTypeID
		JOIN doc.FileCategory cat
			ON fil.FileCategoryID = cat.FileCategoryID
	WHERE bfile.BusinessEntityID = @BusinessEntityID
		AND typ.Code = 'IMG'
		AND cat.Code = @CategoryCode		
		
	

	----------------------------------------------------------
	-- STEP 1:
	-- Look for exact match
	----------------------------------------------------------
	SELECT
		@ImagePath = FilePath
	FROM @tblFiles
	WHERE LanguageID = @LanguageID
	
	
	----------------------------------------------------------
	-- STEP 2:
	-- Look for store image in english version
	----------------------------------------------------------
	IF ISNULL(@ImagePath, '') = ''
	BEGIN
		SELECT
			@ImagePath = FilePath
		FROM @tblFiles
		WHERE LanguageID = @DefaultLanguageID
	END


	-- Return the result of the function
	RETURN @ImagePath
	

END

