﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Gets the ScopeID
-- SAMPLE CALL: SELECT dbo.fnGetScopeID('UNIV')
-- SAMPLE CALL: SELECT dbo.fnGetScopeID(1)
-- =============================================
CREATE FUNCTION [dbo].[fnGetScopeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT ScopeID FROM dbo.Scope WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT ScopeID FROM dbo.Scope WHERE ScopeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

