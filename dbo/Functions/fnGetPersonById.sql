﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 5/13/2014
-- Description:	Returns a person record by their Id
-- SAMPLE CALL: SELECT * FROM dbo.fnGetPersonById(66969)
-- SELECT * FROM dbo.Person
-- =============================================
CREATE FUNCTION [dbo].[fnGetPersonById]
(
	@Id BIGINT
)
RETURNS TABLE
/*
RETURNS @tblOutput TABLE 
(
	PersonID BIGINT, 
	FirstName VARCHAR(75),  
	LastName VARCHAR(75), 
	MiddleName VARCHAR(75), 
	Title VARCHAR(25), 
	Suffix VARCHAR(25), 
	BirthDate DATE, 
	Gender VARCHAR(25), 
	Language VARCHAR(25), 
	AddressLine1 VARCHAR(150), 
	AddressLine2 VARCHAR(150), 
	City VARCHAR(75), 
	State VARCHAR(75), 
	PostalCode VARCHAR(25), 
	HomePhone VARCHAR(25), 
	MobilePhone VARCHAR(25), 
	PrimaryEmail VARCHAR(256), 
	AlternateEmail VARCHAR(256), 
	DateModified DATETIME
)
AS

BEGIN
*/

AS RETURN

/*
	INSERT INTO @tblOutput (	
		PersonID, 
		FirstName,  
		LastName, 
		MiddleName, 
		Title, 
		Suffix, 
		BirthDate, 
		Gender, 
		Language, 
		AddressLine1, 
		AddressLine2, 
		City, 
		State, 
		PostalCode, 
		HomePhone, 
		MobilePhone, 
		PrimaryEmail, 
		AlternateEmail, 
		DateModified 	
	)
*/
	SELECT     
		psn.BusinessEntityID AS PersonID, 
		psn.FirstName, 
		psn.LastName, 
		psn.MiddleName, 
		psn.Title, 
		psn.Suffix, 
		psn.BirthDate, 
		gdr.Name AS Gender, 
		lang.Name AS Language, 
		addr.AddressLine1, 
		addr.AddressLine2, 
		addr.City, 
		addr.State, 
		addr.PostalCode, 
		hphone.PhoneNumber AS HomePhone, 
		mphone.PhoneNumber AS MobilePhone, 
		pemail.EmailAddress AS PrimaryEmail, 
		aemail.EmailAddress AS AlternateEmail, 
		psn.DateModified
	FROM dbo.Person psn (NOLOCK) 
		LEFT JOIN dbo.Gender gdr (NOLOCK) 
			ON psn.GenderID = gdr.GenderID 
		LEFT JOIN dbo.Language lang 
			ON psn.LanguageID = lang.LanguageID 
		LEFT OUTER JOIN (
			SELECT     
				padr.BusinessEntityID AS PersonID, 
				padr.AddressID, adr.AddressLine1, 
				adr.AddressLine2, adr.City, 
				sp.StateProvinceCode AS State, 
				adr.PostalCode
			FROM dbo.BusinessEntityAddress padr (NOLOCK) 
				JOIN dbo.Address adr 
					ON padr.AddressID = adr.AddressID 
						AND padr.AddressTypeID = (SELECT AddressTypeID FROM dbo.AddressType (NOLOCK) WHERE Code = 'HME') 
				LEFT JOIN dbo.StateProvince sp 
					ON adr.StateProvinceID = sp.StateProvinceID
			WHERE padr.BusinessEntityID = @Id
		) addr 
			ON psn.BusinessEntityID = addr.PersonID 
		LEFT JOIN (
			SELECT     
				BusinessEntityID AS PersonID, 
				PhoneNumber
			FROM dbo.BusinessEntityPhone pp WITH (NOLOCK)
			WHERE PhoneNumberTypeID = (SELECT PhoneNumberTypeID FROM dbo.PhoneNumberType (NOLOCK) WHERE Code = 'HME')
				AND pp.BusinessEntityID = @Id
		) hphone 
			ON psn.BusinessEntityID = hphone.PersonID 
		LEFT JOIN (
			SELECT BusinessEntityID AS PersonID, PhoneNumber
			FROM dbo.BusinessEntityPhone pp WITH (NOLOCK)
			WHERE PhoneNumberTypeID = (SELECT PhoneNumberTypeID FROM dbo.PhoneNumberType (NOLOCK) WHERE Code = 'MBL') 
				AND pp.BusinessEntityID = @Id
		) mphone 
			ON psn.BusinessEntityID = mphone.PersonID 
		LEFT JOIN (
			SELECT 
				BusinessEntityID AS PersonID, 
				EmailAddress
			FROM dbo.BusinessEntityEmailAddress pe WITH (NOLOCK)
			WHERE EmailAddressTypeID = (SELECT EmailAddressTypeID FROM dbo.EmailAddressType (NOLOCK) WHERE Code = 'PRIM')
				AND pe.BusinessEntityID = @Id
		) pemail 
			ON psn.BusinessEntityID = pemail.PersonID 
		LEFT JOIN (
			SELECT BusinessEntityID AS PersonID, EmailAddress
			FROM dbo.BusinessEntityEmailAddress pe WITH (NOLOCK)
			WHERE EmailAddressTypeID = (SELECT EmailAddressTypeID FROM dbo.EmailAddressType (NOLOCK) WHERE Code = 'ALT')
				AND pe.BusinessEntityID = @Id
		) aemail 
			ON psn.BusinessEntityID = aemail.PersonID
	WHERE psn.BusinessEntityID = @Id
	                                                         

/*		
	RETURN;
END
*/