﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Creates a password salt
-- SAMPLE CALL: SELECT dbo.fnPasswordSalt(Rand())
-- =============================================
CREATE FUNCTION [dbo].[fnPasswordSalt]
(
	@Rand FLOAT
)
RETURNS VARBINARY(10)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Salt VARBINARY(10)

	-- Add the T-SQL statements to compute the return value here
	SET @Salt = CONVERT(VARBINARY(10), @Rand);

	-- Return the result of the function
	RETURN @Salt;

END

