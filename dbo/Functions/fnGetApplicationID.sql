﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Gets the application ID
-- SAMPLE CALL: SELECT dbo.fnGetApplicationID('MPC')
-- SAMPLE CALL: SELECT dbo.fnGetApplicationID(1)
-- =============================================
CREATE FUNCTION [dbo].[fnGetApplicationID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT ApplicationID FROM dbo.Application WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT ApplicationID FROM dbo.Application WHERE ApplicationID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

