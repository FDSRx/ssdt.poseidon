﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Hash a password
-- SAMPLE CALL: SELECT dbo.fnPasswordHash('Password')
-- =============================================
CREATE FUNCTION [dbo].[fnPasswordHash]
(
	@Password VARCHAR(128)
)
RETURNS VARBINARY(128)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Hash VARBINARY(128)

	-- Add the T-SQL statements to compute the return value here
	SET @Hash = HASHBYTES('SHA1', @Password);

	-- Return the result of the function
	RETURN @Hash;

END

