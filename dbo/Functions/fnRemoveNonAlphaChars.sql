﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/29/2014
-- Description:	Removes non-alpha characters.
-- SAMPLE CALL: SELECT dbo.fnRemoveNonAlphaChars('Guilbeau''s Pharmacy + Grocery');
-- =============================================
CREATE FUNCTION [dbo].[fnRemoveNonAlphaChars]
(
	@Str VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	--Short circut procedure if no text in string.
	IF ISNULL(@Str, '') = '' RETURN @Str;

    DECLARE 
		@KeepValues AS VARCHAR(50)
		
    SET @KeepValues = '%[^a-z]%';
    
    WHILE PATINDEX(@KeepValues, @Str) > 0
	BEGIN
        SET @Str = Stuff(@Str, PatIndex(@KeepValues, @Str), 1, '');
    END

    RETURN @Str;
END


