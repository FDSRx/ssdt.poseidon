﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/18/2013
-- Description: Get the time based on the businesses location
-- SAMPLE CALL: SELECT dbo.fnConvertToBusinessLocalTime('4D1A6C2C-8632-4CB3-A665-05F533FE9CE4', GETDATE())
-- SELECT * FROM dbo.Store
-- SELECT * FROM dbo.BusinessEntity WHERE BusinessEntityID = 9 -- Eastern
-- SELECT * FROM dbo.TimeZone
-- =============================================
CREATE FUNCTION [dbo].[fnConvertToBusinessLocalTime]
(
	@BusinessKey VARCHAR(50),
	@DateTime DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here
	DECLARE @BizTime DATETIME;

	DECLARE
		@BusinessEntityID INT = dbo.fnGetBusinessID(@BusinessKey),
		@IsDST BIT,
		@LocalOffset INT,
		@LocalSupportsDST BIT,
		@BizOffset INT,
		@BizSupportsDST BIT,
		@Offset INT
	
	-- Determine if in daylight savings time
	SELECT @IsDST = CASE WHEN DaylightSavingsTimeID > 1 THEN 1 ELSE 0 END 
	FROM DayLightSavingsTime 
	WHERE @DateTime BETWEEN DateStart AND DateEnd
	
	-- Get local (server) time zone settings
	SELECT 
		@LocalOffset = zne.Offset, 
		@LocalSupportsDST = sto.SupportDST
	FROM dbo.Store sto
		JOIN dbo.TimeZone zne 
			ON sto.TimeZoneID = zne.TimeZoneID
	WHERE sto.BusinessEntityID = dbo.fnGetStoreDefaultID()

	-- Get business TimeZone settings
	SELECT 
		@BizOffset = zne.Offset, 
		@BizSupportsDST = sto.SupportDST
	FROM dbo.Store sto
		JOIN dbo.TimeZone zne 
			ON sto.TimeZoneID = zne.TimeZoneID
	WHERE sto.BusinessEntityID = @BusinessEntityID
	
	-- Adjust From for DST if is appropriate time of year and the store is located where they support DST
	IF @IsDST IS NOT NULL AND @LocalSupportsDST = 1  
	  SELECT @LocalOffset = @LocalOffset + 1

	-- Adjust To for DST if is appropriate time of year and the store is located where they support DST
	IF @IsDST IS NOT NULL AND @BizSupportsDST = 1  
	  SELECT @BizOffset = @BizOffset + 1    

	-- Determine true offset
	SET @Offset = ABS(@LocalOffset) - ABS(@BizOffset)

	-- Adjust to new time
	SET @BizTime = DATEADD(hh, @Offset, @DateTime)

	-- Return the result of the function
	RETURN @BizTime;

END

