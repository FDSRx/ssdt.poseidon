﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/13/2013
-- Description:	if a value is null or empty then return a specified value in its place.
-- SAMPLE CALL: SELECT dbo.fnIsNullorEmpty('');
-- SAMPLE CALL: SELECT dbo.fnIsNullorEmpty(null);
-- SAMPLE CALL: SELECT dbo.fnIsNullorEmpty('hello world');
-- SAMPLE CALL: SELECT dbo.fnIsNullorEmpty(1);
-- =============================================
CREATE FUNCTION [dbo].[fnIsNullOrEmpty]
(
	@Value VARCHAR(MAX)
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result BIT = 0;

	IF (NULLIF(@Value, '')) IS NULL
	BEGIN
		SET @Result = 1;
	END

	-- Return the result of the function
	RETURN @Result;

END

