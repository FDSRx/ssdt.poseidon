﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/26/2014
-- Description: Gets a single or comma delimited list of person ids.
-- SAMPLE CALL: SELECT dbo.fnPersonKeyTranslator(66969, 'Id')
-- SAMPLE CALL: SELECT dbo.fnPersonKeyTranslator(800000, DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnPersonKeyTranslator(135590, 'PatientID')

-- SELECT * FROM dbo.Person
-- =============================================
CREATE FUNCTION [dbo].[fnPersonKeyTranslator]
(
	@Key VARCHAR(50),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)		
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END	
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'PersonID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, BusinessEntityID)
				FROM dbo.Person 
				WHERE BusinessEntityID = CONVERT(BIGINT, @Key)
			)
			WHEN @KeyType = 'PatientID' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, PersonID)
				FROM phrm.Patient 
				WHERE PatientID = CONVERT(BIGINT, @Key)
			)
			WHEN @KeyType IN ('PatientIDList', 'PatientKeyList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, PersonID), '') + ','
				FROM phrm.Patient 
				WHERE PatientID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)			
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, BusinessEntityID)
				FROM dbo.Person 
				WHERE BusinessEntityID = CONVERT(BIGINT, @Key)
			)
			WHEN @KeyType IN ('PersonIDList', 'PersonKeyList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, BusinessEntityID), '') + ','
				FROM dbo.Person
				WHERE BusinessEntityID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)	
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
