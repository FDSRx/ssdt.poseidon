﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/4/2013
-- Description:	Determine if email address is valid
-- SAMPLE CALL: SELECT dbo.fnIsEmailValid('dhughes@hcc-care.com') -- valid
-- SAMPLE CALL: SELECT dbo.fnIsEmailValid('dhughes@hcc-care') -- invalid
-- SAMPLE CALL: SELECT dbo.fnIsEmailValid('') -- empty string
-- SAMPLE CALL: SELECT dbo.fnIsEmailValid(NULL) -- null
-- =============================================
CREATE FUNCTION [dbo].[fnIsEmailValid]
(
	@Email VARCHAR(255)
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @IsValid BIT
	
	IF ISNULL(@Email, '') = ''
		RETURN 0;

	IF ISNULL(@Email, '') <> '' AND @Email NOT LIKE '_%@__%.__%'
		SET @IsValid = 0;  -- Invalid
	ELSE 
		SET @IsValid = 1;   -- Valid

	-- Return the result of the function
	RETURN @IsValid;

END

