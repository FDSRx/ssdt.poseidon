﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/10/2014
-- Description:	Translates a BusinessID from a raw key.
-- SAMPLE CALL:
/*

SELECT dbo.fnBusinessIDTranslator(44, DEFAULT);
SELECT dbo.fnBusinessIDTranslator(261, 'SourceStoreKey');
SELECT dbo.fnBusinessIDTranslator(4609004, 'Nabp');
SELECT dbo.fnBusinessIDTranslator('7529B357-5D54-4759-A384-F636835507FA', DEFAULT);
SELECT dbo.fnBusinessIDTranslator('ID_44', DEFAULT);
SELECT dbo.fnBusinessIDTranslator('NABP_4609004', DEFAULT);
SELECT dbo.fnBusinessIDTranslator('NABP_', DEFAULT);

*/

-- SELECT * FROM dbo.Store
-- SELECT * FROM dbo.Business
-- =============================================
CREATE FUNCTION [dbo].[fnBusinessIDTranslator]
(
	@Key VARCHAR(50),
	@KeyType VARCHAR(50) = NULL
)
RETURNS BIGINT
AS
BEGIN
	-- Declare the return variable here
	DECLARE
		@Id BIGINT,
		@BusinessKeyIdentifier VARCHAR(10) = NULL
	
	
	--------------------------------------------------------------------
	-- Determine if the user is identifying the type of business within
	-- the business key.
	-- <Summary>
	-- Parses the key string into a key type and key (if applicable).
	-- </Summary>
	--------------------------------------------------------------------
	DECLARE @IdentifierIndex INT = CHARINDEX('_', @Key);
	
	SET @BusinessKeyIdentifier = 
		CASE 
			WHEN @IdentifierIndex = 0 THEN NULL 
			ELSE SUBSTRING(@Key, 1, @IdentifierIndex - 1) 
		END;
	
	SET @Key = 
		CASE 
			WHEN @IdentifierIndex = 0 THEN @Key 
			ELSE NULLIF(SUBSTRING(@Key, @IdentifierIndex + 1, LEN(@Key) - @IdentifierIndex ), '')
		END;


	--------------------------------------------------------------------
	-- Interrogate data and asisgn identifier.
	--------------------------------------------------------------------
	SET @Id =
		CASE
			WHEN @KeyType IN ('Nabp', 'NCPDP', 'NCPDPID') OR @BusinessKeyIdentifier IN ('NABP', 'N', 'NBP', 'NCPDP', 'NCPDPID') THEN (
				SELECT TOP 1 BusinessEntityID 
				FROM dbo.Store 
				WHERE NABP = CONVERT(VARCHAR(50), @Key)
			)
			WHEN ( @KeyType IN ('BusinessToken', 'StoreToken', 'ChainToken') OR @BusinessKeyIdentifier IN ('GUID', 'BT', 'ST', 'CT') )
					AND dbo.fnIsUniqueIdentifier(@Key) = 1 THEN (
				SELECT TOP 1 BusinessEntityID 
				FROM dbo.Business 
				WHERE BusinessToken = CONVERT(UNIQUEIDENTIFIER, @Key)
			)
			WHEN ( @KeyType IN ('BusinessID', 'BusinessEntityID', 'ID')  OR @BusinessKeyIdentifier IN ('BID', 'ID') )
					AND ISNUMERIC(@Key) = 1 THEN (
				SELECT TOP 1 BusinessEntityID 
				FROM dbo.Business 
				WHERE BusinessEntityID = CONVERT(BIGINT, @Key)
			)
			WHEN ( @KeyType IN ('SourceStoreID', 'SourceStoreKey')  OR @BusinessKeyIdentifier IN ('SSK', 'SSID') )
					AND ISNUMERIC(@Key) = 1 THEN (
				SELECT TOP 1 BusinessEntityID 
				FROM dbo.Store 
				WHERE SourceStoreID = CONVERT(INT, @Key)
			)
			WHEN ( @KeyType IN ('SourceChainID', 'SourceChainKey')  OR @BusinessKeyIdentifier IN ('SCID', 'SCK') )
					AND ISNUMERIC(@Key) = 1 THEN (
				SELECT TOP 1 BusinessEntityID 
				FROM dbo.Chain 
				WHERE SourceChainID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND dbo.fnIsUniqueIdentifier(@Key) = 1 THEN (
				SELECT TOP 1 BusinessEntityID
				FROM dbo.Business 
				WHERE BusinessToken = CONVERT(UNIQUEIDENTIFIER, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 THEN (
				SELECT TOP 1 BusinessEntityID 
				FROM dbo.Business 
				WHERE BusinessEntityID = CONVERT(BIGINT, @Key)
			)
			ELSE NULL
		END;

	-- Return the result of the function
	RETURN @Id;

END
