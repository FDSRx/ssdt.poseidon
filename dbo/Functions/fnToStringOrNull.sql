﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Writes a value to string or null
-- SAMPLE CALL: SELECT dbo.fnToStringOrNull('hello world');
-- SAMPLE CALL: SELECT dbo.fnToStringOrNull(1234);
-- SAMPLE CALL: SELECT dbo.fnToStringOrNull(1234.5);
-- SAMPLE CALL: SELECT dbo.fnToStringOrNull(NULL);
-- =============================================
CREATE FUNCTION [dbo].[fnToStringOrNull]
(
	@Var VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result VARCHAR(MAX);

	SET @Result = CONVERT(VARCHAR(MAX), @Var);

	-- Return the result of the function
	RETURN @Result;

END

