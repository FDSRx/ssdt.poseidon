﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/20/2015
-- Description:	Gets the identifier for the Preference object.
-- SAMPLE CALL: SELECT dbo.fnGetPreferenceID('RIC')
-- SAMPLE CALL: SELECT dbo.fnGetPreferenceID(1)
-- =============================================
CREATE FUNCTION [dbo].[fnGetPreferenceID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT PreferenceID FROM dbo.Preference WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT PreferenceID FROM dbo.Preference WHERE PreferenceID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

