﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/19/2013
-- Description:	Get Gender ID by code
-- SAMPLE CALL: SELECT dbo.[fnGetGenderID]('M');
-- SAMPLE CALL: SELECT dbo.[fnGetGenderID](1);
-- =============================================
CREATE FUNCTION [dbo].[fnGetGenderID]
(
	@Key VARCHAR(15)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT GenderID FROM dbo.Gender WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT GenderID FROM dbo.Gender WHERE GenderID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
