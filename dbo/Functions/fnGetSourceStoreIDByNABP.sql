﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/23/2013
-- Description:	Get's the source ID of a store from a business entity ID
-- SAMPLE CALL: SELECT dbo.[fnGetSourceStoreIDByNABP](4228688)
-- SELECT * FROM dbo.Store
-- =============================================
CREATE FUNCTION [dbo].[fnGetSourceStoreIDByNABP]
(
	@NABP VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@SourceStoreID INT,
		@BusinessEntityID INT
	
	-- Get business entity ID
	SET @BusinessEntityID = dbo.fnGetStoreIDByNABP(@NABP);
	
	-- Get source store ID
	SELECT
		@SourceStoreID = SourceStoreID
	--SELECT TOP 1 *
	--SELECT *
	FROM dbo.Store
	WHERE BusinessEntityID = @BusinessEntityID	

	-- Return the result of the function
	RETURN @SourceStoreID

END

