﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/18/2013
-- Description:	Gets the business entity id
-- SAMPLE CALL: SELECT dbo.fnGetBusinessEntityTypeID('PSN')
-- SAMPLE CALL: SELECT dbo.fnGetBusinessEntityTypeID('1')
-- SAMPLE CALL: SELECT dbo.fnGetBusinessEntityTypeID('4')
-- =============================================
CREATE FUNCTION [dbo].[fnGetBusinessEntityTypeID]
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT;

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT BusinessEntityTypeID FROM dbo.BusinessEntityType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @ID = (SELECT BusinessEntityTypeID FROM dbo.BusinessEntityType  WHERE BusinessEntityTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @ID;

END

