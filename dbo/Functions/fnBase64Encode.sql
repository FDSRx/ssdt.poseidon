﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description:	Encodes data as a Base64 string.
-- SAMPLE CALL: SELECT dbo.fnBase64Encode('Hello World');
-- =============================================
CREATE FUNCTION dbo.fnBase64Encode
(
    @String VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
BEGIN

  RETURN (
        SELECT
            CAST(N'' AS XML).value(
                  'xs:base64Binary(xs:hexBinary(sql:column("bin")))'
                , 'VARCHAR(MAX)'
            ) AS  Base64Encoding
        FROM (
            SELECT CAST(@String AS VARBINARY(MAX)) AS bin
        ) AS bin_sql_server_temp
    )
END
