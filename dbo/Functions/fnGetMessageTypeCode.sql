﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/18/2013
-- Description:	Gets the message type code
-- SAMPLE CALL: SELECT dbo.fnGetMessageTypeCode('SMS')
-- =============================================
CREATE FUNCTION [dbo].[fnGetMessageTypeCode]
(
	@Key VARCHAR(50)
)
RETURNS VARCHAR(25)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Code VARCHAR(25);

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Code = (SELECT Code FROM msg.MessageType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Code = (SELECT Code FROM msg.MessageType  WHERE MessageTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Code;

END

