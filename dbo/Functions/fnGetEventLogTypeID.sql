﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/9/2014
-- Description:	Gets the EventLogTypeID
-- SAMPLE CALL: SELECT dbo.fnGetEventLogTypeID('CRED')
-- SAMPLE CALL: SELECT dbo.fnGetEventLogTypeID(1)

-- SELECT * FROM dbo.EventLogType
-- =============================================
CREATE FUNCTION [dbo].[fnGetEventLogTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT EventLogTypeID FROM dbo.EventLogType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT EventLogTypeID FROM dbo.EventLogType WHERE EventLogTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

