﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/03/2014
-- Description:	Gets the OriginID
-- SAMPLE CALL: SELECT dbo.[fnGetOriginID]('ENGMS');
-- SAMPLE CALL: SELECT dbo.[fnGetOriginID](2);
-- =============================================
CREATE FUNCTION [dbo].[fnGetOriginID]
(
	@Key VARCHAR(15)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT OriginID FROM dbo.Origin WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT OriginID FROM dbo.Origin WHERE OriginID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
