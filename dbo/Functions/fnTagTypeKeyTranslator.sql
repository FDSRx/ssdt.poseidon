﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description: Gets the translated Priority key or keys.
-- References: dbo.fnSlice
-- SAMPLE CALL: SELECT dbo.fnTagTypeKeyTranslator(1, 'TagTypeID')
-- SAMPLE CALL: SELECT dbo.fnTagTypeKeyTranslator('UD', 'Code')
-- SAMPLE CALL: SELECT dbo.fnTagTypeKeyTranslator('UD', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnTagTypeKeyTranslator(1, DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnTagTypeKeyTranslator('1,2,3', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnTagTypeKeyTranslator('UD,PHRM', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnTagTypeKeyTranslator('User-defined', 'NameList')
-- SELECT * FROM dbo.Priority
-- =============================================
CREATE FUNCTION [dbo].[fnTagTypeKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END		
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'TagTypeId') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TagTypeID) 
				FROM dbo.TagType 
				WHERE TagTypeID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TagTypeID) 
				FROM dbo.TagType 
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TagTypeID) 
				FROM dbo.TagType 
				WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TagTypeId) 
				FROM dbo.TagType 
				WHERE TagTypeID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TagTypeID) 
				FROM dbo.TagType 
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'TagTypeIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TagTypeID), '') + ','
				FROM dbo.TagType
				WHERE TagTypeID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'TagTypeCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TagTypeID), '') + ','
				FROM dbo.TagType
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'TagTypeNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TagTypeID), '') + ','
				FROM dbo.TagType
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TagTypeID), '') + ','
				FROM dbo.TagType
				WHERE TagTypeID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TagTypeID), '') + ','
				FROM dbo.TagType
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
