﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/24/2013
-- Description:	Get value from key from xml dataset
-- SAMPLE CALL: SELECT dbo.fnGetKeyvalueXml('<KeyValueList><KeyValue><Key>TestKey</Key><Value>TestValue</Value></KeyValue></KeyValueList>', 'TestKey') -- valid call
-- SAMPLE CALL: SELECT dbo.fnGetKeyValueXml('<test>test</test>', 'test') -- invalid call
-- SAMPLE CALL: SELECT dbo.fnGetKeyvalueXml('<KeyValueList><KeyValue><Key>TestValue</Key></KeyValue></KeyValueList>', 'TestKey') -- Invalid Call
-- =============================================
CREATE FUNCTION [dbo].[fnXmlGetKeyValue]
(
	@Xml XML,
	@Key VARCHAR(255)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Value NVARCHAR(MAX)
	
	----------------------------------------------------------------
	-- Validate xml
	----------------------------------------------------------------
	-- Ideally, we would expect the key/value schema to be passed into the function
	-- but just to be sure, there are a few tags we can check for to ensure it is a well formed
	-- key/value xml schema. It is not fool proof because a try-catch cannot be used in a function.
	
	DECLARE 
		@IsValidXml BIT = 1,
		@XmlText VARCHAR(MAX) = CONVERT(VARCHAR(MAX), @Xml)
	
	-- All tags must be present in the xml string to be a file key/value pair
	SET @IsValidXml = CASE WHEN ISNULL(@XmlText, '') <> '' THEN @IsValidXml ELSE 0 END	
	SET @IsValidXml = CASE WHEN CHARINDEX('<KeyValueList>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('</KeyValueList>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('<KeyValue>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('</KeyValue>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('<Key>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('</Key>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('<Value>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('</Value>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	
	IF @IsValidXml = 0
	BEGIN
		SET @Value = NULL
		
		-- Exit function if not valid xml
		RETURN @Value;
	END
	

	-- Valid xml
	IF @IsValidXml = 1
	BEGIN
	
		SELECT @Value = Value
		FROM dbo.fnXmlKeyValueList(@Xml)
		WHERE KeyName = @Key
		
	END


	-- Return the result of the function
	RETURN @Value

END

