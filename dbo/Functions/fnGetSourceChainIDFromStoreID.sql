﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/28/2014
-- Description:	Gets the source chain ID 
-- SAMPLE CALL: SELECT dbo.fnGetSourceChainIDFromStoreID(1)
-- SELECT * FROM dbo.Store 
-- SELECT * FROM dbo.Chain 
-- SELECT * FROM dbo.ChainStore
-- SELECT * FROM dbo.BusinessEntity WHERE BusinessEntityID = 109
-- =============================================
CREATE FUNCTION [dbo].[fnGetSourceChainIDFromStoreID]
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@StoreID INT,
		@SourceChainID INT

	SET @StoreID = dbo.fnGetBusinessID(@key);
	
	SELECT TOP 1
		@SourceChainID = SourceChainID
	FROM dbo.ChainStore cs
		JOIN dbo.Chain chn
			ON cs.ChainID = chn.BusinessEntityID
	WHERE StoreID = @StoreID

	-- Return the result of the function
	RETURN @SourceChainID;

END

