﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/14/2013
-- Description:	Retrieves the applications associated ServiceID

-- SAMPLE CALL: SELECT dbo.fnGetApplicationServiceID(1);
-- SAMPLE CALL: SELECT dbo.fnGetApplicationServiceID('MPC');

-- SELECT * FROM dbo.Service
-- SELECT * FROM dbo.Application
-- =============================================
CREATE FUNCTION [dbo].[fnGetApplicationServiceID]
(
	@ApplicationKey VARCHAR(25)
)
RETURNS INT
AS
BEGIN
	
	DECLARE
		@ServiceID INT = dbo.fnGetServiceID('UNKN');

	RETURN @ServiceID;

END

