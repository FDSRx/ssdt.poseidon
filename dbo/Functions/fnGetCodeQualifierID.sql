﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/08/2014
-- Description:	Gets a CodeQualierID from a CodeQualifier identifier.
-- SAMPLE CALL: SELECT dbo.[fnGetCodeQualifierID]('ICD9');
-- SAMPLE CALL: SELECT dbo.[fnGetCodeQualifierID](1);
-- =============================================
CREATE FUNCTION [dbo].[fnGetCodeQualifierID]
(
	@Key VARCHAR(15)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT CodeQualifierID FROM dbo.CodeQualifier WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT CodeQualifierID FROM dbo.CodeQualifier WHERE CodeQualifierID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END
