﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 5/13/2014
-- Description:	Parses a set of key value pairs sent in the form of <key>:<value>,<key>:<value>.
-- SAMPLE CALL: SELECT * FROM dbo.fnGetKeyValuePairs('First:Daniel,Last:Hughes', ',', ':');
-- =============================================
CREATE FUNCTION [dbo].[fnGetKeyValuePairs] ( 
	@Pairs VARCHAR(MAX),
	@Delimiter VARCHAR(10) = NULL,
	@KeyValueDelimiter VARCHAR(10) = NULL
) 
RETURNS @OutTable TABLE (
	Id INT IDENTITY(1,1),
	Name VARCHAR(MAX), 
	Value VARCHAR(MAX)
)
AS
BEGIN
	
	SET @Delimiter = ISNULL(@Delimiter, ','); --','
	SET @KeyValueDelimiter = ISNULL(@KeyValueDelimiter, ':'); --':'
	
	DECLARE @tblCollection AS TABLE (
		Idx INT,
		KeyValuePair VARCHAR(MAX)
	);
	
	INSERT INTO @tblCollection (
		Idx,
		KeyValuePair
	)
	SELECT
		Idx,
		Value
	FROM dbo.fnSplit(@Pairs, @Delimiter)
	
	DECLARE
		@KeyValuePair VARCHAR(MAX),
		@Index INT = 0
	
	-----------------------------------------------------------------------------
	-- Start cursor emulator
	-----------------------------------------------------------------------------
	SELECT TOP(1)
		@Index = Idx,
		@KeyValuePair = KeyValuePair
	FROM @tblCollection
	WHERE Idx > @Index
	ORDER BY Idx ASC
	
	WHILE @@ROWCOUNT = 1
	BEGIN
	
		DECLARE @tblValues AS TABLE (
			Idx INT,
			Value VARCHAR(MAX)
		);
		
		DELETE FROM @tblValues;
		
		INSERT INTO @tblValues (
			Idx,
			Value
		)
		SELECT
			Idx,
			Value
		FROM dbo.fnSplit(@KeyValuePair, @KeyValueDelimiter);
		
		INSERT @OutTable (
			Name,
			Value
		)
		SELECT
			LTRIM(RTRIM((SELECT TOP 1 Value FROM @tblValues WHERE Idx = 1))) AS Name,
			(SELECT TOP 1 Value FROM @tblValues WHERE Idx = 2) AS Value
			
	
		-----------------------------------------------------------------------------
		-- End cursor emulator
		-----------------------------------------------------------------------------
		SELECT TOP(1)
			@Index = Idx,
			@KeyValuePair = KeyValuePair
		FROM @tblCollection
		WHERE Idx > @Index
		ORDER BY Idx ASC
	END

	

	RETURN;
	
END


/*
-- Deprecated code
DECLARE 
		@separator CHAR(1), 
		@keyValueSeperator CHAR(1)

	SET @separator = ISNULL(@Delimiter, ','); --','
	SET @keyValueSeperator = ISNULL(@KeyValueDelimiter, ':'); --':'

	DECLARE 
		@separator_position INT , 
		@keyValueSeperatorPosition INT

	DECLARE 
		@match VARCHAR(MAX) 

	SET @Pairs = @Pairs + @separator;

	WHILE PATINDEX('%' + @separator + '%' , @Pairs) <> 0 
	BEGIN

		SELECT @separator_position =  PATINDEX('%' + @separator + '%' , @Pairs);

		SELECT @match = LEFT(@Pairs, @separator_position - 1);

		IF @match <> '' 
		BEGIN			
			
			SELECT @keyValueSeperatorPosition = PATINDEX('%' + @keyValueSeperator + '%' , @match);
	
			IF @keyValueSeperatorPosition <> -1 
			BEGIN
			
				DECLARE @tblValues AS TABLE (
					Idx INT,
					Value VARCHAR(MAX)
				);
				
				DELETE FROM @tblValues;
				
				INSERT INTO @tblValues (
					Idx,
					Value
				)
				SELECT
					Idx,
					Value
				FROM dbo.fnSplit(@match, @keyValueSeperator);
				
				INSERT @OutTable (
					Name,
					Value
				)
				SELECT
					LTRIM(RTRIM((SELECT TOP 1 Value FROM @tblValues WHERE Idx = 1))) AS Name,
					(SELECT TOP 1 Value FROM @tblValues WHERE Idx = 2) AS Value
				
				/*	
				INSERT @OutTable (
					Name,
					Value
				)
				VALUES (LTRIM(RTRIM(LEFT(@match,@keyValueSeperatorPosition -1))), RIGHT(@match,LEN(@match) - @keyValueSeperatorPosition))
				*/
				
			END
			
		END	
			
		SELECT @Pairs = STUFF(@Pairs, 1, @separator_position, '');
		
	END
*/