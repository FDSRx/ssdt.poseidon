﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/7/2014
-- Description:	Gets the ContactTypeID
-- SAMPLE CALL: SELECT dbo.fnGetContactTypeID('UNKN')
-- SAMPLE CALL: SELECT dbo.fnGetContactTypeID(1)
-- =============================================
CREATE FUNCTION [dbo].[fnGetContactTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT ContactTypeID FROM dbo.ContactType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT ContactTypeID FROM dbo.ContactType WHERE ContactTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

