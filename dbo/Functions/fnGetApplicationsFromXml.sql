﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/25/2015
-- Description:	Parses Application objects from the provided XML data.
-- SAMPLE CALL:
/*
DECLARE @Xml XML = '
<Applications>
  <Application>
    <Id>2</Id>
    <Code>ESMT</Code>
    <Name>External Store Management Tool</Name>
  </Application>
  <Application>
    <Id>10</Id>
    <Code>ENG</Code>
    <Name>Engage</Name>
  </Application>
</Applications>'

SELECT *
FROM dbo.fnGetApplicationsFromXml(@Xml);

*/

-- SELECT * FROM dbo.CredentialEntityApplications
-- =============================================
CREATE FUNCTION [dbo].[fnGetApplicationsFromXml]
(	
	@Xml XML
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT
		ISNULL(objs.value('data(ApplicationID)[1]', 'INT'), objs.value('data(Id)[1]', 'INT')) AS Id,
		objs.value('data(Code)[1]', 'VARCHAR(50)') AS Code,
		objs.value('data(Name)[1]', 'VARCHAR(256)') AS Name,
		objs.value('data(Description)[1]', 'VARCHAR(1000)') AS Description
	FROM @Xml.nodes('*:Applications/*:Application') AS obj(objs)
)
