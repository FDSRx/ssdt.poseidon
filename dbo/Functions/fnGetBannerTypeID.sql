﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/22/2015
-- Description:	Gets the BannerType object identifier.
-- SAMPLE CALL: SELECT dbo.fnGetBannerTypeID('SYSALRT')
-- SAMPLE CALL: SELECT dbo.fnGetBannerTypeID(1)

-- SELECT * FROM dbo.BannerType
-- =============================================
CREATE FUNCTION [dbo].fnGetBannerTypeID 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT BannerTypeID FROM dbo.BannerType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT BannerTypeID FROM dbo.BannerType WHERE BannerTypeID = @Key);
	END

	-- Return the result of the function
	RETURN @ID;

END

