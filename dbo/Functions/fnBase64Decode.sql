﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description:	Decodes base64 data.
-- SAMPLE CALL: SELECT dbo.fnBase64Decode('SGVsbG8gV29ybGQ=');
-- SELECT dbo.fnBase64Encode('Hello World');
-- =============================================
CREATE FUNCTION dbo.fnBase64Decode
(
    @String VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
BEGIN

  RETURN (
		SELECT CAST(
			CAST(N'' AS XML).value(
				'xs:base64Binary(sql:column("bin"))'
			  , 'VARBINARY(MAX)'
			) 
			AS VARCHAR(MAX)
		) AS Base64Decoded 
		FROM (
			SELECT CAST(@String AS VARCHAR(MAX)) AS bin
		) AS bin_sql_server_temp
    );
    
END
