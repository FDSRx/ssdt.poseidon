﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description: Gets the translated Service key or keys.
-- References: dbo.fnSlice
-- SAMPLE CALL: SELECT dbo.fnServiceKeyTranslator(1, 'ServiceID')
-- SAMPLE CALL: SELECT dbo.fnServiceKeyTranslator('MPC', 'Code')
-- SAMPLE CALL: SELECT dbo.fnServiceKeyTranslator('PHRM', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnServiceKeyTranslator(3, DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnServiceKeyTranslator('1,2,3', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnServiceKeyTranslator('MPC,PHRM,RWRD', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnServiceKeyTranslator('myPharmacyConnect,Pharmacy Services,Loyalty Program', 'NameList')

-- SELECT * FROM dbo.Service
-- =============================================
CREATE FUNCTION [dbo].[fnServiceKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX) = NULL,
		@IsArray BIT = NULL
	
	-- Sanitize the key.
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END	
	
	-- Determine if the input is an array/list
	SET @IsArray = CASE WHEN CHARINDEX(',', @Key) > 0 THEN 1 ELSE 0 END;	
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'ServiceID') AND ISNUMERIC(@Key) = 1 AND @IsArray = 0 THEN (
				SELECT TOP 1 
					CONVERT(VARCHAR, ServiceID) 
				FROM dbo.Service 
				WHERE ServiceID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 
					CONVERT(VARCHAR, ServiceID) 
				FROM dbo.Service 
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 
					CONVERT(VARCHAR, ServiceID) 
				FROM dbo.Service 
				WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND @IsArray = 0 THEN (
				SELECT TOP 1 
					CONVERT(VARCHAR, ServiceID) 
				FROM dbo.Service 
				WHERE ServiceID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND @IsArray = 0 THEN (
				SELECT TOP 1 
					CONVERT(VARCHAR, ServiceID) 
				FROM dbo.Service 
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'ServiceIDList') THEN (
				SELECT 
					ISNULL(CONVERT(VARCHAR, ServiceID), '') + ','
				FROM dbo.Service
				WHERE ServiceID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'ServiceCodeList') THEN (
				SELECT 
					ISNULL(CONVERT(VARCHAR, ServiceID), '') + ','
				FROM dbo.Service
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'ServiceNameList') THEN (
				SELECT 
					ISNULL(CONVERT(VARCHAR, ServiceID), '') + ','
				FROM dbo.Service
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT 
					ISNULL(CONVERT(VARCHAR, ServiceID), '') + ','
				FROM dbo.Service
				WHERE ServiceID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT 
					ISNULL(CONVERT(VARCHAR, ServiceID), '') + ','
				FROM dbo.Service
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
