﻿

-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/16/2014
-- Description:	Gets a TaskStatusID from a TaskStatus identifier.
-- SAMPLE CALL: SELECT dbo.[fnGetTaskStatusID]('INV');
-- SAMPLE CALL: SELECT dbo.[fnGetTaskStatusID](1);

-- SELECT * FROM dbo.TaskStatus
-- =============================================
CREATE FUNCTION [dbo].[fnGetTaskStatusID]
(
	@Key VARCHAR(15)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Id INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Id = (SELECT TaskStatusID FROM dbo.TaskStatus WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Id = (SELECT TaskStatusID FROM dbo.TaskStatus WHERE TaskStatusID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Id;

END

