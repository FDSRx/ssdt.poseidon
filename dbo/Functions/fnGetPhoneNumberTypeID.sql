﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/17/2013
-- Description:	Gets an phone number type id 
-- SAMPLE CALL: SELECT dbo.[fnGetPhoneNumberTypeID]('HME')
-- SELECT * FROM dbo.PhoneNumberType
-- =============================================
CREATE FUNCTION [dbo].[fnGetPhoneNumberTypeID]
(
	@Key VARCHAR(25)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @PhoneNumberTypeID INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @PhoneNumberTypeID = (SELECT PhoneNumberTypeID FROM dbo.PhoneNumberType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @PhoneNumberTypeID = (SELECT PhoneNumberTypeID FROM dbo.PhoneNumberType WHERE PhoneNumberTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @PhoneNumberTypeID;

END

