﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Gets the PriorityID
-- SAMPLE CALL: SELECT dbo.fnGetPriorityID('CRIT')
-- SAMPLE CALL: SELECT dbo.fnGetPriorityID(1)
-- =============================================
CREATE FUNCTION [dbo].[fnGetPriorityID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT PriorityID FROM dbo.Priority WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT PriorityID FROM dbo.Priority WHERE PriorityID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

