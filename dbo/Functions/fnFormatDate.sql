﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/28/2013
-- Description:	Formats date
-- SAMPLE CALL: SELECT dbo.fnFormatDate(GETDATE(), 'MM/DD/YYYY');
-- SAMPLE CALL: SELECT dbo.fnFormatDate('12/31/2013', 'MMDDYYYY');
-- SAMPLE CALL: SELECT dbo.fnFormatDate('2012-09-17 00:00:00.000', 'MM/DD/YYYY HH:MI:SS tt')
-- =============================================
CREATE FUNCTION [dbo].[fnFormatDate]
(
	@DateTime VARCHAR(100),
	@FormatMask VARCHAR(50) = NULL
)
RETURNS VARCHAR(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@IsDate BIT,
		@WorkingDate DATETIME
	
	SET @IsDate = ISDATE(@DateTime)	
	
	IF @IsDate = 0
	BEGIN
		RETURN @DateTime;
	END
	ELSE
	BEGIN
		SET @WorkingDate = CONVERT(DATETIME, @DateTime)
	END
	

	-- Declare the return variable here

	DECLARE 
		@StringDate VARCHAR(32),
		@AMPM VARCHAR(2),
		@PostTimeChar VARCHAR(2) = 'TT'

	SET @StringDate = @FormatMask

	IF (CHARINDEX ('AMPM',@StringDate) > 0) 
	BEGIN
		SET @AMPM = RIGHT(CONVERT(VARCHAR(32), @WorkingDate, 109), 2);
		SET @StringDate = REPLACE(@StringDate, 'AMPM', @PostTimeChar);
	END  

	IF (CHARINDEX ('AM',@StringDate) > 0) 
	BEGIN
		SET @AMPM = RIGHT(CONVERT(VARCHAR(32), @WorkingDate, 109), 2);
		SET @StringDate = REPLACE(@StringDate, 'AM', @PostTimeChar);
	END

	IF (CHARINDEX ('PM',@StringDate) > 0) 
	BEGIN
		SET @AMPM = RIGHT(CONVERT(VARCHAR(32), @WorkingDate, 109), 2);
		SET @StringDate = REPLACE(@StringDate, 'PM', @PostTimeChar);
	END
	
	IF (CHARINDEX ('TT',@StringDate) > 0) 
	BEGIN
		SET @AMPM = RIGHT(CONVERT(VARCHAR(32), @WorkingDate, 109), 2);
	END    

	IF (CHARINDEX ('HH',@StringDate) > 0) 
	BEGIN
		DECLARE @Hours INT = DATEPART(HH, @WorkingDate);
		DECLARE @HoursText VARCHAR(2) = RIGHT('0' + CONVERT(VARCHAR(2), CASE WHEN @Hours > 12 THEN @Hours - 12 ELSE @Hours END), 2);

		SET @StringDate = REPLACE(@StringDate, 'HH', @HoursText);
	END 

	IF (CHARINDEX ('MI',@StringDate) > 0) 
	BEGIN
		SET @StringDate = REPLACE(@StringDate, 'MI', RIGHT('0'+CONVERT(VARCHAR,DATEPART(MI, @WorkingDate)),2));
	END 
	
	IF (CHARINDEX ('SS',@StringDate) > 0) 
	BEGIN
		SET @StringDate = REPLACE(@StringDate, 'SS', RIGHT('0'+CONVERT(VARCHAR,DATEPART(SS, @WorkingDate)),2));
	END   

	IF (CHARINDEX ('YYYY',@StringDate) > 0)
	BEGIN
		SET @StringDate = REPLACE(@StringDate, 'YYYY', DATENAME(YY, @WorkingDate));
	END

	IF (CHARINDEX ('YY',@StringDate) > 0)
	BEGIN
		SET @StringDate = REPLACE(@StringDate, 'YY', RIGHT(DATENAME(YY, @WorkingDate),2));
	END

	IF (CHARINDEX ('Month',@StringDate) > 0)
	BEGIN
		SET @StringDate = REPLACE(@StringDate, 'Month', DATENAME(MM, @WorkingDate));
	END

	IF (CHARINDEX ('MON',@StringDate COLLATE SQL_Latin1_General_CP1_CS_AS)>0)
	BEGIN
		SET @StringDate = REPLACE(@StringDate, 'MON', LEFT(UPPER(DATENAME(MM, @WorkingDate)),3));
	END

	IF (CHARINDEX ('Mon', @StringDate) > 0)
	BEGIN
		SET @StringDate = REPLACE(@StringDate, 'Mon', LEFT(DATENAME(MM, @WorkingDate),3));
	END

	IF (CHARINDEX ('MM', @StringDate) > 0)
	BEGIN
		SET @StringDate = REPLACE(@StringDate, 'MM', RIGHT('0'+CONVERT(VARCHAR,DATEPART(MM, @WorkingDate)),2));
	END

	IF (CHARINDEX ('M', @StringDate) > 0)
	BEGIN
		SET @StringDate = REPLACE(@StringDate, 'M', CONVERT(VARCHAR,DATEPART(MM, @WorkingDate)));
	END

	IF (CHARINDEX ('DD',@StringDate) > 0)
	BEGIN
		SET @StringDate = REPLACE(@StringDate, 'DD', RIGHT('0'+DATENAME(DD, @WorkingDate),2));
	END

	IF (CHARINDEX ('D',@StringDate) > 0) 
	BEGIN
		SET @StringDate = REPLACE(@StringDate, 'D', DATENAME(DD, @WorkingDate)) ;
	END 

	IF (CHARINDEX (@PostTimeChar, @StringDate) > 0) 
	BEGIN
		SET @StringDate =  REPLACE(@StringDate, @PostTimeChar, @AMPM) ;
	END   


	-- Return the result of the function
	RETURN @StringDate;


END

