﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/28/2014
-- Description:	Indicates whether the person ID being requested exists in our person data.
-- SAMPLE CALL: SELECT dbo.fnIsValidPersonID(66969);
-- SAMPLE CALL: SELECT dbo.fnIsValidPersonID(NULL);
-- SAMPLE CALL: SELECT dbo.fnIsValidPersonID(0);
-- =============================================
CREATE FUNCTION [dbo].[fnIsValidPersonID]
(
	@ID BIGINT
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@IsValid BIT,
		@PersonID BIGINT

	SET @PersonID = (
		SELECT BusinessEntityID
		FROM dbo.Person
		WHERE BusinessEntityID = @ID
	);
	
	SET @IsValid = CASE WHEN @PersonID IS NOT NULL THEN 1 ELSE 0 END;

	-- Return the result of the function
	RETURN @IsValid;

END

