﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/24/2013
-- Description:	Get the name of the business entity id
-- SAMPLE CALL: SELECT dbo.fnGetBusinessName(5949)
-- SAMPLE CALL: SELECT dbo.fnGetBusinessName('78BD319E-6721-47CF-9C5A-B38D4633B39E')
-- =============================================
CREATE FUNCTION [dbo].[fnGetBusinessName]
(
	@Key VARCHAR(50)
)
RETURNS VARCHAR(255)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@BusinessID BIGINT,
		@BusinessName VARCHAR(255)
	
	SET @BusinessID = dbo.fnGetBusinessID(@Key);

	-- Try to lookup a store name first
	SELECT @BusinessName = Name FROM dbo.Store WHERE BusinessEntityID = @BusinessID
	
	-- If you can't find a store then see if the business is a chain
	IF ISNULL(@BusinessName, '') = ''
	BEGIN
		SELECT @BusinessName = Name FROM dbo.Chain WHERE BusinessEntityID = @BusinessID
	END

	-- Return the result of the function
	RETURN @BusinessName;

END

