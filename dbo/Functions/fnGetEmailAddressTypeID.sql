﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/14/2013
-- Description:	Gets an email address type id 
-- SAMPLE CALL: SELECT dbo.[fnGetEmailAddressTypeID]('PRIM')
-- SELECT * FROM dbo.EmailAddressType
-- =============================================
CREATE FUNCTION [dbo].[fnGetEmailAddressTypeID]
(
	@Key VARCHAR(25)
)
RETURNS INT
AS
BEGIN

	-- Declare the return variable here
	DECLARE @EmailAddressTypeID INT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @EmailAddressTypeID = (SELECT EmailAddressTypeID FROM dbo.EmailAddressType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @EmailAddressTypeID = (SELECT EmailAddressTypeID FROM dbo.EmailAddressType WHERE EmailAddressTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @EmailAddressTypeID;

END

