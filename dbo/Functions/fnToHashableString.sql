﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2013
-- Description:	Returns a standardized hashable string
-- SAMPLE CALL: SELECT dbo.fnToHashableString('Hello World');
-- SAMPLE CALL: SELECT dbo.fnToHashableString(1);
-- SAMPLE CALL: SELECT dbo.fnToHashableString(NULL);
-- SAMPLE CALL: SELECT dbo.fnToHashableString('');
-- =============================================
CREATE FUNCTION [dbo].[fnToHashableString]
(
	@Data VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE
		@String VARCHAR(MAX)

	SET @String = LOWER(ISNULL(CONVERT(VARCHAR(MAX), @Data), '<NULL>'));

	-- Return the result of the function
	RETURN @String;

END
