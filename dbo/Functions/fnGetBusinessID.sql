﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/24/2013
-- Description:	Get business id from business token
-- SAMPLE CALL: SELECT dbo.fnGetBusinessID('78BD319E-6721-47CF-9C5A-B38D4633B39E')
-- SAMPLE CALL: SELECT dbo.fnGetBusinessID(6225)
-- =============================================
CREATE FUNCTION [dbo].[fnGetBusinessID]
(
	@Key VARCHAR(50)
)
RETURNS BIGINT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID BIGINT

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT BusinessEntityID FROM dbo.Business WHERE BusinessToken = @Key);
	END
	ELSE
	BEGIN
		SET @ID = (SELECT BusinessEntityID FROM dbo.Business WHERE BusinessEntityID = CONVERT(BIGINT, @Key));
	END	

	-- Return the result of the function
	RETURN @ID;

END

