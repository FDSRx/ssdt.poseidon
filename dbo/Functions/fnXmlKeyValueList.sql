﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/24/2013
-- Description:	Parses XMl key value structure
-- SAMPLE CALL:
/*


DECLARE 
	@KeyValueList XML,
	@Xml XML = NULL

SET @KeyValueList = dbo.fnXmlAddKeyValue(@KeyValueList, '<*FN*>', 'Daniel');
SET @KeyValueList = dbo.fnXmlAddKeyValue(@KeyValueList, '<*LN*>', 'Hughes');
SET @KeyValueList = dbo.fnXmlAddKeyValue(@KeyValueList, '<*MI*>', 'R');

-- SELECT @KeyValueList

SELECT * FROM dbo.fnXmlKeyValueList(@KeyValueList)



*/
-- =============================================
CREATE FUNCTION [dbo].[fnXmlKeyValueList]
(
	@Xml XML
)
RETURNS  @TblKeyValue 
TABLE (
	[KeyName] NVARCHAR(255),
	Value NVARCHAR(MAX)
)
AS
BEGIN


	----------------------------------------------------------------
	-- Validate xml
	----------------------------------------------------------------
	-- Ideally, we would expect the key/value schema to be passed into the function
	-- but just to be sure, there are a few tags we can check for to ensure it is a well formed
	-- key/value xml schema. It is not fool proof because a try-catch cannot be used in a function.
	
	DECLARE 
		@IsValidXml BIT = 1,
		@XmlText VARCHAR(MAX) = CONVERT(VARCHAR(MAX), @Xml)
	
	-- All tags must be present in the xml string to be a file key/value pair
	SET @IsValidXml = CASE WHEN ISNULL(@XmlText, '') <> '' THEN @IsValidXml ELSE 0 END	
	SET @IsValidXml = CASE WHEN CHARINDEX('<KeyValueList>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('</KeyValueList>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('<KeyValue>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('</KeyValue>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('<Key>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('</Key>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('<Value>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	SET @IsValidXml = CASE WHEN CHARINDEX('</Value>', @XmlText) > 0 THEN @IsValidXml ELSE 0 END
	
	IF @IsValidXml = 0
	BEGIN
		-- Invalid schema; return nothing
		RETURN;
	END
	
	
	-- If xml is a valid key/value schema then parse data and return key/value table
	IF @IsValidXml = 1
	BEGIN
	
		--------------------------------------------------------
		-- Bind xml string to xml variable for validation
		--------------------------------------------------------
		DECLARE @x XML
		
		SET @x = @Xml
		
		--------------------------------------------------------
		-- Parse xml structure into a key/value paired table
		--------------------------------------------------------
		INSERT INTO @TblKeyValue (
			KeyName,
			Value
		)
		SELECT
			kvp.value('(Key)[1]', 'NVARCHAR(MAX)') AS [KeyName], 
			kvp.value('(Value)[1]', 'NVARCHAR(MAX)') AS Value
		FROM @x.nodes('KeyValueList/KeyValue') as KV(kvp)
		
	END
	
	
	
	RETURN 
END
