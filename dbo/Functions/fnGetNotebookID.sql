﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/16/2014
-- Description:	Gets the NotebookID
-- SAMPLE CALL: SELECT dbo.fnGetNotebookID('INTRN')
-- SAMPLE CALL: SELECT dbo.fnGetNotebookID(1)
-- =============================================
CREATE FUNCTION [dbo].[fnGetNotebookID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT NotebookID FROM dbo.Notebook WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT NotebookID FROM dbo.Notebook WHERE NotebookID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

