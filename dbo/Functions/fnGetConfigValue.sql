﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/30/2013
-- NOTE: Deprecated method.  Use the "dbo.fnGetConfigurationValue" method.
-- Description:	Get a configuration value for the config key
-- SAMPLE CALL: 
-- SELECT dbo.[fnGetConfigValue]('SERVERTIMEZONE');

-- SELECT * FROM dbo.Configuration
-- =============================================
CREATE FUNCTION [dbo].[fnGetConfigValue]
(
	@Key VARCHAR(255)
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Value VARCHAR(255)

	-- Add the T-SQL statements to compute the return value here
	SELECT
		@Value = KeyValue
	--SELECT *
	FROM dbo.Configuration
	WHERE KeyName = @Key
		AND ApplicationID IS NULL
		AND BusinessEntityID IS NULL

	-- Return the result of the function
	RETURN @Value

END

