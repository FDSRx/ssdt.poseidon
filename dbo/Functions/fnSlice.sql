﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/4/2013
-- Description: Simulates a slice concept of an array for a list (0 indexed based)
-- SAMPLE CALL: SELECT dbo.fnSlice('1,2,3,500,9000,300', ',', 5)
-- =============================================
CREATE FUNCTION [dbo].[fnSlice]
(
    @Text VARCHAR(MAX),
    @Delimiter VARCHAR(100),
    @Index INT
)
RETURNS VARCHAR(MAX)
AS BEGIN
    DECLARE @A TABLE (ID INT IDENTITY, V VARCHAR(MAX));
    
    DECLARE @R VARCHAR(MAX);
    
    WITH CTE AS
    (
		SELECT 0 A, 1 B
		UNION ALL
		SELECT B, CONVERT(INT,CHARINDEX(@Delimiter, @Text, B) + LEN(@Delimiter))
		FROM CTE
		WHERE B > A
    )
    
    INSERT @A(V)
    SELECT SUBSTRING(@Text,A,CASE WHEN B > LEN(@Delimiter) THEN B-A-LEN(@Delimiter) ELSE LEN(@Text) - A + 1 END) VALUE      
    FROM CTE WHERE A > 0

    SELECT @R = V
    FROM @A
    WHERE ID = @Index + 1
    
    RETURN @R
END

