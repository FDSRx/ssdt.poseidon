﻿
-- =============================================
-- Author:		John Kim
-- Create date: 10/10/2014
-- Description:	Determines whether the specified date falls on store's holiday/weekend schedule. 
-- Change Log:
-- 1/14/2016 - dhughes - Modified the procedure to check for a "00:00:00.0000000" time stamp in both the start and end fields as this
--				is also a valid "store closed" entry.
-- 3/1/2016 - dhughes - Modified the procedure to determine which set of hours to use when attempting to determine a closed scenario.
--				Added a set of default hours if an appropriate hour (store or pharmacy) could not be determined. The pharmacy hours
--				always try and "win" over store hours in this scenario.

-- SAMPLE CALL:
/*

DECLARE
	@StoreID VARCHAR(50) = 3

SELECT 
	DayID 
FROM dbo.fnGetStoreClosedDays(@StoreID)

*/

-- SELECT * FROM dbo.StoreHours WHERE StoreID = 15852
-- SELECT * FROM dbo.Store WHERE Nabp = '3374662'
-- =============================================
CREATE FUNCTION [dbo].[fnGetStoreClosedDays]
(
	@StoreID VARCHAR(50)
)
RETURNS @StoreClosedDays TABLE (
	RowID INT IDENTITY,
	DayID INT
)
AS
BEGIN

	---------------------------------------------------------------------------------------------------------------
	-- Local variables.
	---------------------------------------------------------------------------------------------------------------
	DECLARE
		@ResultHourTypeCode VARCHAR(25),
		@IsPharmacyHoursValid BIT = NULL,
		@IsStoreHoursValid BIT = NULL


	---------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	---------------------------------------------------------------------------------------------------------------
	DECLARE @tblStoreHours AS TABLE (
		Idx INT IDENTITY(1,1),
		StoreID BIGINT,
		HourTypeCode VARCHAR(25),
		DayID INT,
		TimeStart TIME,
		TimeEnd TIME,
		TimeText VARCHAR(100)	
	);

	---------------------------------------------------------------------------------------------------------------
	-- Create default hours.
	---------------------------------------------------------------------------------------------------------------
	INSERT INTO @tblStoreHours (
		StoreID,
		HourTypeCode,
		DayID,
		TimeStart,
		TimeEnd,
		TimeText
	)
	SELECT @StoreID, 'DFLT', 1, '09:00', '19:00', '9:00 AM - 7:00 PM'
	UNION
	SELECT @StoreID, 'DFLT', 2, '09:00', '19:00', '9:00 AM - 7:00 PM'
	UNION
	SELECT @StoreID, 'DFLT', 3, '09:00', '19:00', '9:00 AM - 7:00 PM'
	UNION
	SELECT @StoreID, 'DFLT', 4, '09:00', '19:00', '9:00 AM - 7:00 PM'
	UNION
	SELECT @StoreID, 'DFLT', 5, '09:00', '19:00', '9:00 AM - 7:00 PM'
	UNION
	SELECT @StoreID, 'DFLT', 6, NULL, NULL, 'Closed' -- Closed Saturday.
	UNION
	SELECT @StoreID, 'DFLT', 7, NULL, NULL, 'Closed' -- Closed Sunday.	

	-- Debug
	--SELECT 'Debugger On' AS DebugMode, 'Review hours (default)' AS ActionMethod, * FROM @tblStoreHours

	---------------------------------------------------------------------------------------------------------------
	-- Fetch store hours.
	---------------------------------------------------------------------------------------------------------------
	INSERT INTO @tblStoreHours (
		StoreID,
		HourTypeCode,
		DayID,
		TimeStart,
		TimeEnd,
		TimeText
	)
	SELECT
		StoreID,
		'STRE' AS HourTypeCode,
		DayID,
		TimeStart,
		TimeEnd,
		TimeText
	--SELECT *
	FROM dbo.StoreHours
	WHERE StoreID = @StoreID 
		AND HourTypeID = (SELECT TOP 1 HourTypeID FROM dbo.HourType WHERE Code = 'STRE')

	-- Debug
	--SELECT 'Debugger On' AS DebugMode, 'Review hours (store)' AS ActionMethod, * FROM @tblStoreHours

	---------------------------------------------------------------------------------------------------------------
	-- Fetch pharmacy hours.
	---------------------------------------------------------------------------------------------------------------
	INSERT INTO @tblStoreHours (
		StoreID,
		HourTypeCode,
		DayID,
		TimeStart,
		TimeEnd,
		TimeText
	)
	SELECT
		StoreID,
		'PHRM' AS HourTypeCode,
		DayID,
		TimeStart,
		TimeEnd,
		TimeText
	--SELECT *
	FROM dbo.StoreHours
	WHERE StoreID = @StoreID 
		AND HourTypeID = (SELECT TOP 1 HourTypeID FROM dbo.HourType WHERE Code = 'PHRM')

	-- Debug
	--SELECT 'Debugger On' AS DebugMode, 'Review hours (pharmacy)' AS ActionMethod, * FROM @tblStoreHours



	---------------------------------------------------------------------------------------------------------------
	-- Set variables.
	---------------------------------------------------------------------------------------------------------------
	SET @IsPharmacyHoursValid = 
		CASE
			-- No hours.
			WHEN (SELECT COUNT(*) FROM @tblStoreHours WHERE HourTypeCode = 'PHRM') = 0 THEN 0 
			-- Every day is closed.
			WHEN (SELECT COUNT(*) FROM @tblStoreHours WHERE HourTypeCode = 'PHRM' AND ( TimeStart IS NULL AND TimeEnd IS NULL )) = 7 THEN 0
			ELSE 1
		END

	SET @IsStoreHoursValid = 
		CASE
			-- No hours.
			WHEN (SELECT COUNT(*) FROM @tblStoreHours WHERE HourTypeCode = 'STRE') = 0 THEN 0
			-- Every day is closed.
			WHEN (SELECT COUNT(*) FROM @tblStoreHours WHERE HourTypeCode = 'STRE' AND ( TimeStart IS NULL AND TimeEnd IS NULL )) = 7 THEN 0
			ELSE 1
		END

	-- Determine the set of hours to use when determining if the store is closed.
	SET @ResultHourTypeCode = 
		CASE 
			WHEN @IsPharmacyHoursValid = 1 THEN 'PHRM'
			WHEN @IsStoreHoursValid = 1 THEN 'STRE'
			ELSE 'DFLT'
		END;

	-- Debug
	--SELECT 'Debugger On' AS DebugMode, 'Review hours (store)' AS ActionMethod, 
	--	@IsPharmacyHoursValid AS IsPharmacyHoursvalid, @IsStoreHoursValid AS IsStoreHoursValid,
	--	@ResultHourTypeCode AS ResultHourTypeCode


	---------------------------------------------------------------------------------------------------------------
	-- Fetch store closed days.
	---------------------------------------------------------------------------------------------------------------
	INSERT INTO @StoreClosedDays (
		DayID
	)
	SELECT DISTINCT
		DayID 
	--SELECT *
	FROM dbo.StoreHours 
	WHERE StoreID = @StoreID
		AND HourTypeID = (SELECT TOP 1 HourTypeID FROM dbo.HourType WHERE Code = @ResultHourTypeCode)
		AND ( (TimeStart IS NULL AND TimeEnd IS NULL) OR (TimeStart = '00:00:00.0000000' AND TimeEnd = '00:00:00.0000000') )

		

	RETURN;
END



/*
----------------------------------------------------------------------------------------------
-- 3/1/2016 - dhughes - Deprecated logic 
----------------------------------------------------------------------------------------------

INSERT INTO @StoreClosedDays (
	DayID
)
SELECT DISTINCT
	DayID 
FROM dbo.StoreHours 
WHERE StoreID = @StoreID
	AND (
		TimeStart IS NULL AND TimeEnd IS NULL AND TimeText = 'Closed' 
			AND HourTypeID IN (SELECT HourTypeID from poseidon.dbo.HourType WHERE Code IN ('PHRM', 'STRE'))
		OR (TimeStart = '00:00:00.0000000' AND TimeEnd = '00:00:00.0000000')
	)

*/