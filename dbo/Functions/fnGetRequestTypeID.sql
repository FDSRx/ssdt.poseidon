﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/8/2014
-- Description:	Gets the request type ID
-- SAMPLE CALL: SELECT dbo.fnGetRequestTypeID('CNTRLACC')
-- SAMPLE CALL: SELECT dbo.fnGetRequestTypeID(1)
-- =============================================
CREATE FUNCTION [dbo].fnGetRequestTypeID
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT RequestTypeID FROM dbo.RequestType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT RequestTypeID FROM dbo.RequestType WHERE RequestTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

