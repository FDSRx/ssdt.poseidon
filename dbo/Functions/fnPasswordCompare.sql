﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Compare passwords
-- Sample Call: SELECT dbo.fnPasswordCompare('Password?±óœ”5ƒL', 0x55746361F1DE51BD3A31D2DA1969898F2B8EF28C)
-- =============================================
CREATE FUNCTION [dbo].[fnPasswordCompare]
(
	@Password VARCHAR(128),
	@PasswordHash VARBINARY(128)
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@IsValid BIT = 0

	-- Add the T-SQL statements to compute the return value here
	DECLARE 
		@Hash VARBINARY(128)
	
	SET @Hash = dbo.fnPasswordHash(@Password);
	
	IF @Hash = @PasswordHash
		SET @IsValid = 1;

	-- Return the result of the function
	RETURN @IsValid;

END

