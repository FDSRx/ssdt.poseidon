﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/18/2013
-- Description:	Gets the message type id
-- SAMPLE CALL: SELECT dbo.fnGetMessageTypeID('SMS')
-- =============================================
CREATE FUNCTION [dbo].[fnGetMessageTypeID]
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @MessageTypeID INT;

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @MessageTypeID = (SELECT MessageTypeID FROM msg.MessageType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @MessageTypeID = (SELECT MessageTypeID FROM msg.MessageType  WHERE MessageTypeID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @MessageTypeID;

END

