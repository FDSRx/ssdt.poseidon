﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/26/2015
-- Description:	Parses Business objects from the provided XML data.
-- SAMPLE CALL:
/*
DECLARE @Xml XML = '
<Businesses>
  <Business>
    <BusinessID>38</BusinessID>
    <SourceStoreKey>7812</SourceStoreKey>
    <Nabp>2501624</Nabp>
    <Name>City Drug Store</Name>
  </Business>
</Businesses>'

SELECT *
FROM dbo.fnGetBusinessesFromXml(@Xml);

*/

-- SELECT * FROM dbo.CredentialEntityBusinessEntity
-- =============================================
CREATE FUNCTION [dbo].[fnGetBusinessesFromXml]
(	
	@Xml XML
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT
		ISNULL(objs.value('data(BusinessID)[1]', 'BIGINT'), objs.value('data(Id)[1]', 'BIGINT')) AS BusinessID,
		objs.value('data(Nabp)[1]', 'VARCHAR(50)') AS Nabp,
		objs.value('data(SourceStoreKey)[1]', 'VARCHAR(256)') AS SourceStoreKey,
		objs.value('data(Name)[1]', 'VARCHAR(1000)') AS Name
	FROM @Xml.nodes('*:Businesses/*:Business') AS obj(objs)
)
