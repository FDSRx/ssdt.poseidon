﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/24/2013
-- Description:	Get the public business number of a business
-- SAMPLE CALL: SELECT dbo.fnGetBusinessNumber(5949)
-- SAMPLE CALL: SELECT dbo.fnGetBusinessNumber('78BD319E-6721-47CF-9C5A-B38D4633B39E')
-- =============================================
CREATE FUNCTION [dbo].[fnGetBusinessNumber] 
(
	@BusinessKey VARCHAR(50)
)
RETURNS VARCHAR(25)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@BusinessEntityID INT,
		@BusinessNumber VARCHAR(25)

	SET @BusinessEntityID = dbo.fnGetBusinessID(@BusinessKey);
	
	-- Get business number
	SELECT @BusinessNumber = BusinessNumber 
	FROM dbo.Business 
	WHERE BusinessEntityID = @BusinessEntityID

	-- Return the result of the function
	RETURN @BusinessNumber;

END

