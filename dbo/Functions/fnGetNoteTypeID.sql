﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Gets the NoteTypeID
-- SAMPLE CALL: SELECT dbo.fnGetNoteTypeID('TSK')
-- SAMPLE CALL: SELECT dbo.fnGetNoteTypeID(1)
-- =============================================
CREATE FUNCTION [dbo].[fnGetNoteTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT NoteTypeID FROM dbo.NoteType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT NoteTypeID FROM dbo.NoteType WHERE NoteTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

