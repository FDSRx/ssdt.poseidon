﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/18/2013
-- Description:	Determine if a phone number is valid
-- SAMPLE CALL: SELECT dbo.fnIsPhoneValid('555555555') -- 9 digits; invalid
-- SAMPLE CALL: SELECT dbo.fnIsPhoneValid('4105555555') -- 10 digits; valid
-- =============================================
CREATE FUNCTION [dbo].[fnIsPhoneValid]
(
	@Text VARCHAR(255)
)
RETURNS BIT
AS
BEGIN
	
	SET @Text = dbo.fnRemoveNonNumericChars(@Text);
	
	-- Empty phone number; not valid
	IF ISNULL(@Text, '') = ''
		RETURN 0;
	
	-- Non-existent area code; not valid	
	IF LEFT(@Text, 3) = '000'
		RETURN 0;
	
	-- Length is not 10 digits; not valid
	IF LEN(@Text) <> 10
		RETURN 0;
		
	
	-- Passed all incorrect scenarios; return as valid
	RETURN 1;


END

