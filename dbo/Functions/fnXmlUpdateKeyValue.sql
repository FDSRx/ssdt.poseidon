﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/19/2013
-- Description:	Updates the value of the key
-- =============================================
CREATE FUNCTION [dbo].[fnXmlUpdateKeyValue]
(
	@Xml XML,
	@Key VARCHAR(255),
	@Value VARCHAR(MAX)
)
RETURNS XML
AS
BEGIN
	
	-- Remove current key line
	SET @Xml = dbo.fnXmlDeleteKeyValue(@Xml, @Key);
	
	-- Add new key line
	SET @Xml = dbo.fnXmlAddKeyValue(@Xml, @Key, @Value);
	

	-- Return Xml object
	RETURN @Xml;

END

