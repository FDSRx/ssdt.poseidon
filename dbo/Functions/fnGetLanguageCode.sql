﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/13/2013
-- Description:	Get language code
-- SAMPLE CALL: SELECT dbo.fnGetLanguageCode('en');
-- SAMPLE CALL: SELECT dbo.fnGetLanguageCode(1);
-- =============================================
CREATE FUNCTION [dbo].[fnGetLanguageCode]
(
	@Key VARCHAR(15)
)
RETURNS VARCHAR(25)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Code VARCHAR(25)

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Code = (SELECT Code FROM dbo.Language WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Code = (SELECT Code FROM dbo.Language WHERE LanguageID = CONVERT(INT, @Key));
	END	

	-- Return the result of the function
	RETURN @Code;

END


