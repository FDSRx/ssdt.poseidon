﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/24/2013
-- Description:	Returns a list of business entities that fall under the business umbrella
-- SAMPLE CALL: SELECT * FROM dbo.[tblBusinessesUnderUmbrella](277, 'MPC')
-- =============================================
CREATE FUNCTION [dbo].[tblBusinessesUnderUmbrella]
(	
	@BusinessEntityUmbrellaID BIGINT,
	@ServiceKey VARCHAR(50)
)
RETURNS @tblStoreList TABLE 
(		
	Idx BIGINT IDENTITY(1,1),
	BusinessEntityID BIGINT,
	BusinessToken VARCHAR(50),	
	BusinessNumber VARCHAR(50),
	BusinessTypeCode VARCHAR(15),
	BusinessType VARCHAR(50),
	Name VARCHAR(150)
) 
BEGIN

	DECLARE 
		@ServiceID INT
		
	SET @ServiceID = dbo.fnGetServiceID(@ServiceKey);
	
	-- Find stores under the umbrella
	INSERT INTO @tblStoreList (
		BusinessEntityID,
		BusinessToken,
		BusinessNumber,
		BusinessTypeCode,
		BusinessType,
		Name
	)
	SELECT DISTINCT 
		sto.BusinessEntityID,
		biz.BusinessToken,
		biz.BusinessNumber,
		typ.Code,
		typ.Name,
		sto.Name
	FROM dbo.BusinessEntityServiceUmbrella umb
		JOIN dbo.BusinessEntity bizent
			ON umb.BusinessEntityID = bizent.BusinessEntityID
		JOIN dbo.BusinessEntityType typ
			ON bizent.BusinessEntityTypeID = typ.BusinessEntityTypeID
		JOIN dbo.Business biz
			ON umb.BusinessEntityID = biz.BusinessEntityID
		JOIN dbo.Store sto
			ON biz.BusinessEntityID = sto.BusinessEntityID

	WHERE BusinessEntityUmbrellaID = @BusinessEntityUmbrellaID
	
	UNION
	
	-- Find chains under the umbrella
	SELECT DISTINCT 
		chn.BusinessEntityID,
		biz.BusinessToken,
		biz.BusinessNumber,
		typ.Code,
		typ.Name,
		chn.Name
	FROM dbo.BusinessEntityServiceUmbrella umb
		JOIN dbo.BusinessEntity bizent
			ON umb.BusinessEntityID = bizent.BusinessEntityID
		JOIN dbo.BusinessEntityType typ
			ON bizent.BusinessEntityTypeID = typ.BusinessEntityTypeID
		JOIN dbo.Business biz
			ON umb.BusinessEntityID = biz.BusinessEntityID
		JOIN dbo.Chain chn
			ON biz.BusinessEntityID = chn.BusinessEntityID
	WHERE BusinessEntityUmbrellaID = @BusinessEntityUmbrellaID
	
	RETURN;

END
