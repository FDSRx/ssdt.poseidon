﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 5/4/2015
-- Description:	Returns the TimeZone object identifier for a particular store.
-- SAMPLE CALL: SELECT dbo.fnGetBusinessTimeZoneID('10684', 'NABP');

-- SElECT * FROM dbo.vwStore WHERE BusinessEntityID
-- SELECT * FROM dbo.TimeZone
-- SELECT * FROM dbo.StateProvince
-- =============================================
CREATE FUNCTION dbo.fnGetBusinessTimeZoneID
(
	@BusinessKey VARCHAR(50),
	@BusinessKeyType VARCHAR(50) = NULL
)
RETURNS INT
AS
BEGIN


	---------------------------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------------------------
	DECLARE	
		@BusinessEntityID BIGINT = dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType),
		@TimeZoneID BIGINT = NULL,
		@SupportDST INT = NULL


	---------------------------------------------------------------------------------------------
	-- Retrieve TimeZoneID
	---------------------------------------------------------------------------------------------
	SELECT 
		@TimeZoneID = TimeZoneID,
		@SupportDST = SupportDST
	--SELECT *
	--SELECT TOP 1 *
	FROM dbo.Store
	WHERE BusinessEntityID = @BusinessEntityID
	
	IF @TimeZoneID IS NULL
	BEGIN
	
		-- SELECT * FROM dbo.AddressType
		
		DECLARE 
			@AddressTypeID INT = dbo.fnGetAddressTypeID('MOFF'),
			@StateProvinceID INT = NULL,
			@AddressID BIGINT = NULL
		
		-- Retrieve the address of the business.
		SELECT 
			@AddressID = AddressID
		--SELECT *	
		--SELECT TOP 1 *
		FROM dbo.BusinessEntityAddress
		WHERE BusinessEntityID = @BusinessEntityID
			AND AddressTypeID = @AddressTypeID			
		
		-- Use the address to determine the State/Province of the business.
		SELECT
			@StateProvinceID = StateProvinceID
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.Address
		
		-- Use the State/Province to determine the Time Zone in which the state resides.
		SELECT
			@TimeZoneID = TimeZoneID
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.StateProvince
		WHERE StateProvinceID = @StateProvinceID
	
	END
	
	

	-- Return the result of the function
	RETURN @TimeZoneID

END
