﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/18/2013
-- Description:	Hash value
-- SAMPLE CALL: SELECT dbo.fnDataHash('This is a sample hash');
-- =============================================
CREATE FUNCTION [dbo].[fnDataHash]
(
	@Value VARCHAR(300)
)
RETURNS VARBINARY(300)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Hash VARBINARY(300)

	-- Add the T-SQL statements to compute the return value here
	SET @Hash = HASHBYTES('SHA1', @Value);

	-- Return the result of the function
	RETURN @Hash;

END

