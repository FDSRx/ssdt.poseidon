﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/13/2015
-- Description:	Gets an EmailAddressType object code.

-- SAMPLE CALL: SELECT dbo.[fnGetEmailAddressTypeCode]('PRIM')
-- SAMPLE CALL: SELECT dbo.[fnGetEmailAddressTypeCode](2)

-- SELECT * FROM dbo.EmailAddressType
-- =============================================
CREATE FUNCTION [dbo].[fnGetEmailAddressTypeCode]
(
	@Key VARCHAR(25)
)
RETURNS VARCHAR(50)
AS
BEGIN

	-- Declare the return variable here
	DECLARE @Code VARCHAR(50)

	-- Smart filter
	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @Code = (SELECT Code FROM dbo.EmailAddressType WHERE Code = @Key);
	END
	ELSE
	BEGIN
		SET @Code = (SELECT Code FROM dbo.EmailAddressType WHERE EmailAddressTypeID = @Key);
	END	

	-- Return the result of the function
	RETURN @Code;

END

