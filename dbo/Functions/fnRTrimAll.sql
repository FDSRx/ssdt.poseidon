﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Trims all carriage returns, whitespace, etc. from the right, up until the first true character.
-- SAMPLE CALL:
/*
DECLARE @Line VARCHAR(MAX) = '    
hello world.
		The brown fox 
	Jumped over the fence...
	 and some how did not hurt	himself.	 ';
SELECT dbo.fnRTrimAll(@Line);	 
*/	 
-- =============================================
CREATE FUNCTION [dbo].[fnRTrimAll]
(
	@String VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	
	DECLARE @Result VARCHAR(MAX) = @String;
	
	-- RTrim all
	;WITH CTE_RTrim AS (
		SELECT 1 AS i, @Result AS String
		UNION ALL
		SELECT i + 1, String = SUBSTRING(@Result, 1, LEN(@Result) - i)
		FROM CTE_RTrim
		WHERE RIGHT(String, 1) IN (CHAR(10), CHAR(13), CHAR(10) + CHAR(13), CHAR(9), CHAR(32))
			AND i <=  LEN(@Result)
	)
	
	SELECT TOP(1) @Result = String
	FROM CTE_RTrim
	ORDER BY i DESC
	OPTION (MAXRECURSION 0);
	
	RETURN @Result;

END

