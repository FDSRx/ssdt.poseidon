﻿

-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/23/2013
-- Description:	Get an app specific configuration value for the config key
-- SAMPLE CALL: SELECT dbo.fnGetAppConfigValue('MPC', 'RequestKeyExpirationMin')
-- SAMPLE CALL: SELECT dbo.fnGetAppConfigValue('MPC', 'PrivacyPolicyUrl')
-- =============================================
CREATE FUNCTION [dbo].[fnGetAppConfigValue]
(
	@ApplicationKey VARCHAR(50),
	@Key VARCHAR(255)
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE 
		@ApplicationID INT,
		@Value VARCHAR(255)
		
	SET @ApplicationID = dbo.fnGetApplicationID(@ApplicationKey);

	-- Add the T-SQL statements to compute the return value here
	SELECT
		@Value = KeyValue
	--SELECT *
	FROM dbo.Configuration
	WHERE ApplicationID = @ApplicationID
		AND BusinessEntityID IS NULL
		AND KeyName = @Key

	-- Return the result of the function
	RETURN @Value

END


