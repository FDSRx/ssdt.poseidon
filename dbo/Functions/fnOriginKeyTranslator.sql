﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/3/2014
-- Description: Gets the translated Origin key or keys.
-- References: dbo.fnSlice
-- SAMPLE CALL: SELECT dbo.fnOriginKeyTranslator(1, 'OriginID')
-- SAMPLE CALL: SELECT dbo.fnOriginKeyTranslator('ENG', 'Code')
-- SAMPLE CALL: SELECT dbo.fnOriginKeyTranslator('ENGMS', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnOriginKeyTranslator(1, DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnOriginKeyTranslator('1,2,3', DEFAULT)
-- SELECT * FROM dbo.Priority
-- =============================================
CREATE FUNCTION [dbo].[fnOriginKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END		
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'OriginID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, OriginID) 
				FROM dbo.Origin 
				WHERE OriginID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, OriginID) 
				FROM dbo.Origin 
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, OriginID) 
				FROM dbo.Origin 
				WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, OriginID) 
				FROM dbo.Origin 
				WHERE OriginID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, OriginID) 
				FROM dbo.Origin 
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'OriginIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, OriginID), '') + ','
				FROM dbo.Origin
				WHERE OriginID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'OriginCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, OriginID), '') + ','
				FROM dbo.Origin
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'OriginNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, OriginID), '') + ','
				FROM dbo.Origin
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, OriginID), '') + ','
				FROM dbo.Origin
				WHERE OriginID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, OriginID), '') + ','
				FROM dbo.Origin
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
