﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 5/5/2014
-- Description:	True to indicate daylight savings time is in effect; otherwise false.
-- SAMPLE CALL: SELECT dbo.fnIsInDaylightSavingsTime(DEFAULT)

-- SELECT * FROM dbo.TimeZone
-- =============================================
CREATE FUNCTION dbo.fnIsInDaylightSavingsTime
(
	@DateTime DATETIME = NULL
)
RETURNS BIT
AS
BEGIN

	----------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------
	SET @DateTime = ISNULL(@DateTime, GETDATE());

	----------------------------------------------------------------------------
	-- Local variables.
	----------------------------------------------------------------------------	
	DECLARE
		@IsInDST BIT = 0


	----------------------------------------------------------------------------
	-- Determine if the date is in daylight savings time.
	----------------------------------------------------------------------------	
	-- Determine if we are in daylight savings time.
	SELECT
		@IsInDST = CASE WHEN DaylightSavingsTimeID IS NOT NULL THEN 1 ELSE 0 END
	--SELECT *
	--SELECT TOP 1 *
	FROM dbo.DayLightSavingsTime
	WHERE DateStart <= @DateTime
		AND DateEnd >= @DateTime

	-- Return the result of the function
	RETURN @IsInDST;

END
