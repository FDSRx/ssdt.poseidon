﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/22/2015
-- Description: Gets the translated TaskStatus key or keys.
-- References: dbo.fnSlice

-- SAMPLE CALL: SELECT dbo.fnTaskStatusKeyTranslator(1, 'TaskStatusID')
-- SAMPLE CALL: SELECT dbo.fnTaskStatusKeyTranslator('NEW', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnTaskStatusKeyTranslator('1,2,3', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnTaskStatusKeyTranslator('NEW,INPROG,CMPL', DEFAULT)
-- SAMPLE CALL: SELECT dbo.fnTaskStatusKeyTranslator('NEW,CMPL,INV', DEFAULT)

-- SELECT * FROM dbo.TaskStatus
-- =============================================
CREATE FUNCTION [dbo].[fnTaskStatusKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END		
	
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'TaskStatusID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TaskStatusID) FROM dbo.TaskStatus WHERE TaskStatusID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (SELECT TOP 1 CONVERT(VARCHAR, TaskStatusID) FROM dbo.TaskStatus WHERE Code = @Key)
			WHEN @KeyType = 'Name' THEN (SELECT TOP 1 CONVERT(VARCHAR, TaskStatusID) FROM dbo.TaskStatus WHERE Name = @Key)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TaskStatusID) FROM dbo.TaskStatus WHERE TaskStatusID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, TaskStatusID) FROM dbo.TaskStatus WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'TaskStatusIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TaskStatusID), '') + ','
				FROM dbo.TaskStatus
				WHERE TaskStatusID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'TaskStatusCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TaskStatusID), '') + ','
				FROM dbo.TaskStatus
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'TaskStatusNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TaskStatusID), '') + ','
				FROM dbo.TaskStatus
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TaskStatusID), '') + ','
				FROM dbo.TaskStatus
				WHERE TaskStatusID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, TaskStatusID), '') + ','
				FROM dbo.TaskStatus
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
