﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/13/2014
-- Description:	Indicates whether the specified tag object exists.
-- SAMPLE CALL:
/*
DECLARE @TagID BIGINT, @Exists BIT

EXEC dbo.spTag_Exists
	@Name = 'High Risk Medications',
	@Exists = @Exists OUTPUT,
	@TagID = @TagID OUTPUT
	
SELECT @TagID AS TagID, @Exists AS IsFound
*/
-- =============================================
CREATE PROCEDURE [dbo].[spTag_Exists]
	@TagID BIGINT = NULL OUTPUT,
	@Name VARCHAR(500) = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------
	SET @Exists = NULL;
	
	--------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------
	DECLARE @ErrorMessage VARCHAR(4000)
	
	--------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	--------------------------------------------------------------
	IF @TagID IS NULL AND ISNULL(@Name, '') = ''
	BEGIN
		SET @ErrorMessage = 'Unable to verify Tag object existence. Object references (@TagID or @Name) is not set to an instance of an object.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	--------------------------------------------------------------
	-- Clear name property if the primary key was provided.
	-- <Remarks>
	-- The primary key always wins when attempting to perform a
	-- lookup.
	-- </Summary>
	--------------------------------------------------------------
	SET @Name = CASE WHEN @TagID IS NOT NULL THEN NULL ELSE @Name END;
	
	--------------------------------------------------------------
	-- Determine if tag exists
	--------------------------------------------------------------
	SET @TagID = (
		SELECT TOP 1
			TagID
		FROM dbo.Tag
		WHERE (TagID = @TagID OR Name = @Name)
	);
	
	-- Set the exists flag based on the lookup.
	SET @Exists = CASE WHEN @TagID IS NOT NULL THEN 1 ELSE 0 END;
	
	
	
	
END
