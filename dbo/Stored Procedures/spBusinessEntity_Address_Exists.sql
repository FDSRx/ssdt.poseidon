﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/12/1982
-- Description:	Indicates whether the business entity address exists.
-- SAMPLE CALL:
/*
DECLARE 
	@Exists BIT = 0, 
	@BusinessEntityAddressID BIGINT = NULL,
	@AddressID BIGINT = NULL

---- Surrogate validation
--EXEC [spBusinessEntity_Address_Exists]
--	@BusinessEntityAddressID = @BusinessEntityAddressID OUTPUT,
--	@BusinessEntityID = -1,
--	@AddressTypeID = -1,
--	@Exists = @Exists OUTPUT

-- Unique key validation
EXEC [spBusinessEntity_Address_Exists]
	@BusinessEntityAddressID = @BusinessEntityAddressID OUTPUT,
	@BusinessEntityID = 1,
	@AddressTypeID = 5,
	@Exists = @Exists OUTPUT,
	@AddressID = @AddressID OUTPUT
	
SELECT @Exists AS IsFound, @BusinessEntityAddressID AS BusinessEntityAddressID, @AddressID AS AddressID
		
*/
-- SELECT * FROM dbo.BusinessEntityAddress
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Address_Exists]
	@BusinessEntityAddressID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@AddressTypeID INT = NULL,
	@AddressID BIGINT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);
	SET @AddressID = NULL;
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidateSurrogateKey BIT = 0
		

	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------	
	IF @BusinessEntityAddressID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @AddressTypeID = NULL;
		
		SET @ValidateSurrogateKey = 1;
	END
	
	----------------------------------------------------------------------
	-- Argument Null Exceptions
	----------------------------------------------------------------------
	----------------------------------------------------------------------
	-- Surrogate key validation.
	-- <Summary>
	-- If a surrogate key was provided, then that will be the only lookup
	-- performed.
	-- <Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityAddressID, 0) = 0 AND @ValidateSurrogateKey = 1
	BEGIN			
		
		SET @ErrorMessage = 'Object reference not set to an instance of an object. The object, @BusinessEntityAddressID, cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	----------------------------------------------------------------------
	-- Unique row key validation.
	-- <Summary>
	-- If a surrogate key was not provided then we will try and perform the
	-- lookup based on the records unique key.
	-- </Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0 AND ISNULL(@AddressTypeID, 0) = 0 AND @ValidateSurrogateKey = 0
	BEGIN
		
		SET @ErrorMessage = 'Object references not set to an instance of an object. The objects, @BusinessEntityID and @AddressTypeID cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END
	
	
	--------------------------------------------------------------------
	-- Determine if business entity service exists
	-- <Remarks>
	-- Legacy implementation to perform lookup.  The logic can be
	-- modified at a later date to be more compacted.
	-- </Remarks>
	--------------------------------------------------------------------
	IF @BusinessEntityAddressID IS NOT NULL AND @ValidateSurrogateKey = 1
	BEGIN
		SELECT TOP 1 
			@BusinessEntityAddressID = BusinessEntityAddressID,
			@AddressID = AddressID
		FROM dbo.BusinessEntityAddress
		WHERE BusinessEntityAddressID = @BusinessEntityAddressID
	END
	ELSE
	BEGIN
		SELECT TOP 1 
			@BusinessEntityAddressID = BusinessEntityAddressID,
			@AddressID = AddressID
		FROM dbo.BusinessEntityAddress
		WHERE BusinessEntityID = @BusinessEntityID
			AND AddressTypeID = @AddressTypeID
	END
	
	-- If no results were returned then let's ensure the unique identifier and address identifier
	-- cannot be present.
	IF @@ROWCOUNT = 0
	BEGIN
		SET @BusinessEntityAddressID = NULL;
		SET @AddressID = NULL;
	END
	
	
	-- Set the exists flag to properly represent the finding.
	SET @Exists = CASE WHEN @BusinessEntityAddressID IS NOT NULL THEN 1 ELSE 0 END;



	
END
