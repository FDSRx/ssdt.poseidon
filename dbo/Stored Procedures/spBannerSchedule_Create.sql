﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/15/2016
-- Description:	Updates a Banner object.
-- SAMPLE CALL:
/*

DECLARE
	@ApplicationID VARCHAR(MAX) = '10',
	@BusinessID VARCHAR(MAX) = '38,10864',
	@PersonID VARCHAR(MAX) = NULL,
	@BannerID VARCHAR(MAX) = '1,2,3,4',
	@BannerTypeID INT = NULL,
	@BannerTypeCode VARCHAR(50) = NULL,
	@Name VARCHAR(256) = NULL,
	@Header VARCHAR(MAX) = NULL,
	@Body VARCHAR(MAX) = NULL,
	@Footer VARCHAR(MAX) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(MAX) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@SortOrder DECiMAL(9,2),
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@BannerScheduleID VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC dbo.spBannerSchedule_Create
	@ApplicationID = @ApplicationID,
	@BusinessID = @BusinessID,
	@PersonID = @PersonID,
	@BannerID = @BannerID,
	@BannerTypeID = @BannerTypeID,
	@BannerTypeCode = @BannerTypeCode,
	@Name = @Name,
	@Header = @Header,
	@Body = @Body,
	@Footer = @Footer,
	@LanguageID = @LanguageID,
	@LanguageCode = @LanguageCode,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@BannerScheduleID = @BannerScheduleID OUTPUT,
	@Debug = @Debug

SELECT @BannerScheduleID AS BannerScheduleID


*/

-- SELECT * FROM dbo.Banner
-- SELECT * FROM dbo.BannerSchedule
-- =============================================
CREATE PROCEDURE [dbo].[spBannerSchedule_Create]
	@ApplicationID VARCHAR(MAX) = NULL,
	@BusinessID VARCHAR(MAX) = NULL,
	@PersonID VARCHAR(MAX) = NULL,
	@BannerID VARCHAR(MAX) = NULL,
	@BannerTypeID INT = NULL,
	@BannerTypeCode VARCHAR(50) = NULL,
	@Name VARCHAR(256) = NULL,
	@Header VARCHAR(MAX) = NULL,
	@Body VARCHAR(MAX) = NULL,
	@Footer VARCHAR(MAX) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(MAX) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@SortOrder DECiMAL(9,2) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@BannerScheduleID VARCHAR(MAX) = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	----------------------------------------------------------------------------------------------------
	-- Instance variables.
	----------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@BannerTypeID=' + dbo.fnToStringOrEmpty(@BannerTypeID) + ';' +
		'@BannerTypeCode=' + dbo.fnToStringOrEmpty(@BannerTypeCode) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@Header=' + dbo.fnToStringOrEmpty(@Header) + ';' +
		'@Body=' + dbo.fnToStringOrEmpty(@Body) + ';' +
		'@Footer=' + dbo.fnToStringOrEmpty(@Footer) + ';' +
		'@LanguageID=' + dbo.fnToStringOrEmpty(@LanguageID) + ';' +
		'@LanguageCode=' + dbo.fnToStringOrEmpty(@LanguageCode) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	----------------------------------------------------------------------------------------------------
	-- Log request.
	----------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	----------------------------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @ModifiedBy = COALESCE(@ModifiedBy, SUSER_NAME());
	SET @BannerScheduleID = NULL

	----------------------------------------------------------------------------------------------------
	-- Set variables.
	----------------------------------------------------------------------------------------------------
	SET @BannerTypeID = ISNULL(dbo.fnGetBannerTypeID(@BannerTypeID), dbo.fnGetBannerTypeID(@BannerTypeCode));
	SET @LanguageID = ISNULL(dbo.fnGetLanguageID(@LanguageID), dbo.fnGetLanguageID(@LanguageCode));

	-- Debug	
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @PersonID AS PersonID, @BannerID AS BannerID
	END	

	----------------------------------------------------------------------------------------------------
	-- Temporary resources.
	----------------------------------------------------------------------------------------------------
	-- Banners
	IF OBJECT_ID('tempdb..#tmpBanners') IS NOT NULL
	BEGIN
		DROP TABLE #tmpBanners;
	END

	CREATE TABLE #tmpBanners (
		Idx BIGINT IDENTITY(1,1),
		BannerID BIGINT
	);

	-- Applications
	IF OBJECT_ID('tempdb..#tmpApplications') IS NOT NULL
	BEGIN
		DROP TABLE #tmpApplications;
	END

	CREATE TABLE #tmpApplications (
		Idx BIGINT IDENTITY(1,1),
		ApplicationID BIGINT
	);

	-- Businesses
	IF OBJECT_ID('tempdb..#tmpBusinesses') IS NOT NULL
	BEGIN
		DROP TABLE #tmpBusinesses;
	END

	CREATE TABLE #tmpBusinesses (
		Idx BIGINT IDENTITY(1,1),
		BusinessID BIGINT
	);

	-- Persons
	IF OBJECT_ID('tempdb..#tmpPersons') IS NOT NULL
	BEGIN
		DROP TABLE #tmpPersons;
	END

	CREATE TABLE #tmpPersons (
		Idx BIGINT IDENTITY(1,1),
		PersonID BIGINT
	);

	-- Output
	IF OBJECT_ID('tempdb..#tmpOutput') IS NOT NULL
	BEGIN
		DROP TABLE #tmpOutput;
	END

	CREATE TABLE #tmpOutput (
		Idx BIGINT IDENTITY(1,1),
		BannerScheduleID BIGINT
	);


	----------------------------------------------------------------------------------------------------
	-- Parse objects.
	----------------------------------------------------------------------------------------------------
	-- Applications
	INSERT INTO #tmpApplications (
		ApplicationID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@ApplicationID, ',')
	WHERE ISNUMERIC(Value) = 1

	--Seed global value.
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpApplications(ApplicationID) VALUES(NULL);
	END

	-- Businesses
	INSERT INTO #tmpBusinesses (
		BusinessID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@BusinessID, ',')
	WHERE ISNUMERIC(Value) = 1

	--Seed global value.
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpBusinesses(BusinessID) VALUES(NULL);
	END

	-- Persons
	INSERT INTO #tmpPersons (
		PersonID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@PersonID, ',')
	WHERE ISNUMERIC(Value) = 1

	--Seed global value.
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpPersons(PersonID) VALUES(NULL);
	END

	-- Banners
	INSERT INTO #tmpBanners (
		BannerID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@BannerID, ',')
	WHERE ISNUMERIC(Value) = 1


	----------------------------------------------------------------------------------------------------
	-- Create object.
	----------------------------------------------------------------------------------------------------
	BEGIN TRY
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	----------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	----------------------------------------------------------------------------------------------------
	
		----------------------------------------------------------------------------------------------------
		-- Create Banner object (if applicable).
		-- <Summary>
		-- Determine if the Banner object was provided.  If true, then ignore the creation process;
		-- otherwise, create a new banner object to map to provided criteria.
		-- </Summary>
		----------------------------------------------------------------------------------------------------
		IF @BannerID IS NULL
		BEGIN

			EXEC dbo.spBanner_Create
				@BannerTypeID = @BannerTypeID,
				@BannerTypeCode = @BannerTypeCode,
				@Name = @Name,
				@Header = @Header,
				@Body = @Body,
				@Footer = @Footer,
				@LanguageID = @LanguageID,
				@LanguageCode = @LanguageCode,
				@CreatedBy = @CreatedBy,
				@ModifiedBy = @ModifiedBy,
				@BannerID = @BannerID OUTPUT,
				@Debug = @Debug

			INSERT INTO #tmpBanners (
				BannerID
			)
			SELECT
				@BannerID AS BannerID

		END

		----------------------------------------------------------------------------------------------------
		-- Create BannerSchedule objects.
		----------------------------------------------------------------------------------------------------
		INSERT INTO dbo.BannerSchedule (
			ApplicationID,
			BusinessID,
			PersonID,
			BannerID,
			DateStart,
			DateEnd,
			SortOrder,
			CreatedBy
		)
		OUTPUT inserted.BannerScheduleID INTO #tmpOutput(BannerScheduleID)
		SELECT
			app.ApplicationID,
			biz.BusinessID,
			per.PersonID,
			b.BannerID,
			@DateStart AS DateStart,
			@DateEnd AS DateEnd,
			ISNULL(@SortOrder, 0) AS SortOrder,
			@CreatedBy AS CreatedBy
		FROM #tmpBanners b
			CROSS APPLY #tmpApplications app
			CROSS APPLY #tmpBusinesses biz
			CROSS APPLY #tmpPersons per		


		-- Retrieve object identity (or identifiers)
		IF @@ROWCOUNT = 1
		BEGIN
			SET @BannerScheduleID = SCOPE_IDENTITY();
		END
		ELSE IF @@ROWCOUNT > 1
		BEGIN
			SET @BannerScheduleID = (
				SELECT
					ISNULL(CONVERT(VARCHAR, BannerScheduleID), '') + ','
				FROM #tmpOutput
				FOR XML PATH('')
			);
		END


	----------------------------------------------------------------------------------------------------
	-- End data transaction.
	----------------------------------------------------------------------------------------------------		
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args

		SET @ErrorMessage = ERROR_MESSAGE();
	
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);		

	END CATCH


	----------------------------------------------------------------------------------------------------
	-- Release resources.
	----------------------------------------------------------------------------------------------------
	DROP TABLE #tmpBanners;
	DROP TABLE #tmpApplications;
	DROP TABLE #tmpBusinesses;
	DROP TABLE #tmpPersons;







END