﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/23/2014
-- Description:	Updates a Task object.
-- 11/9/2015 - dhughes - Modified the procedure to receive a list of Note objects vs. a single object so that it is able
--					to perform batch updates (when applicable).
-- 11/13/15 - dhughes/ssimmons - Modified the update to have better speed and performance.
-- 3/11/2016 - dhughes - Modified the procedure to accept more inputs to be more versatile.

-- SAMPLE CALL:
/*

DECLARE
	@NoteID VARCHAR(MAX) = 21094, 
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(50) = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = '<div>MedSync fill for <a href="javascript:urlRedirect(''MED_SYNC'',30466 )">VIRGINIA SIMMONS (Daniel)</a> on 01-07-2015</div><div>CARB/LEVO    TAB 25-250MG (2/6.00).</div>',
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@DateDue DATETIME = NULL,
	@AssignedByID BIGINT = NULL,
	@AssignedByString VARCHAR(256) = NULL,
	@AssignedToID BIGINT = NULL,
	@AssignedToString VARCHAR(256) = NULL,
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusID INT = NULL,
	@TaskStatusCode VARCHAR(50) = NULL,
	@ParentTaskID BIGINT = NULL,
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@IsDateDueNullificationAllowed BIT = NULL,
	@IsAssignedByIDNullificationAllowed BIT = NULL,
	@IsAssignedByStringNullificationAllowed BIT = NULL,
	@IsAssignedToIDNullificationAllowed BIT = NULL,
	@IsAssignedToStringNullificationAllowed BIT = NULL,
	@IsCompletedByIDNullificationAllowed BIT = NULL,
	@IsCompletedByStringNullificationAllowed BIT = NULL,
	@IsDateCompletedNullificationAllowed BIT = NULL,
	@IsParentTaskIDNullificationAllowed BIT = NULL,
	@Debug BIT = 1
	
EXEC dbo.spTask_Update
	@NoteID = @NoteID,
	@ApplicationID = @ApplicationID,
	@ApplicationCode = @ApplicationCode,
	@BusinessID = @BusinessID,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@BusinessEntityID = @BusinessEntityID,
	@ScopeID = @ScopeID,
	@ScopeCode = @ScopeCode,
	@NoteTypeID = @NoteTypeID,
	@NoteTypeCode = @NoteTypeCode,
	@NotebookID = @NotebookID,
	@NotebookCode = @NotebookCode,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityID = @PriorityID,
	@PriorityCode = @PriorityCode,
	@Tags = @Tags,
	@OriginID = @OriginID,
	@OriginCode = @OriginCode,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@DateDue = @DateDue,
	@AssignedByID = @AssignedByID,
	@AssignedByString = @AssignedByString,
	@AssignedToID = @AssignedToID,
	@AssignedToString = @AssignedToString,
	@CompletedByID = @CompletedByID,
	@CompletedByString = @CompletedByString,
	@DateCompleted = @DateCompleted,
	@TaskStatusID = @TaskStatusID,
	@TaskStatusCode = @TaskStatusCode,
	@ParentTaskID = @ParentTaskID,
	@ModifiedBy = @ModifiedBy,
	@IsDateDueNullificationAllowed = @IsDateDueNullificationAllowed,
	@IsAssignedByIDNullificationAllowed = @IsAssignedByIDNullificationAllowed,
	@IsAssignedByStringNullificationAllowed = @IsAssignedByStringNullificationAllowed,
	@IsAssignedToIDNullificationAllowed = @IsAssignedToIDNullificationAllowed,
	@IsAssignedToStringNullificationAllowed = @IsAssignedToStringNullificationAllowed,
	@IsCompletedByIDNullificationAllowed = @IsCompletedByIDNullificationAllowed,
	@IsCompletedByStringNullificationAllowed = @IsCompletedByStringNullificationAllowed,
	@IsDateCompletedNullificationAllowed = @IsDateCompletedNullificationAllowed,
	@IsParentTaskIDNullificationAllowed = @IsParentTaskIDNullificationAllowed,
	@Debug = @Debug

SELECT @NoteID AS NoteID


*/

-- SELECT * FROM dbo.Task ORDER BY 1 DESC
-- SELECT * FROM dbo.vwTask WHERE NoteID = 21094
-- SELECT * FROM dbo.Note ORDER BY 1 DESC
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.NoteBook
-- SELECT * FROM dbo.vwTask WHERE NoteID = 21094
-- SELECT * FROM dbo.Note WHERE NoteID = 21094

-- SELECT TOP 10 * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spTask_Update]
	@NoteID VARCHAR(MAX),
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(50) = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@DateDue DATETIME = NULL,
	@AssignedByID BIGINT = NULL,
	@AssignedByString VARCHAR(256) = NULL,
	@AssignedToID BIGINT = NULL,
	@AssignedToString VARCHAR(256) = NULL,
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusID INT = NULL,
	@TaskStatusCode VARCHAR(50) = NULL,
	@ParentTaskID BIGINT = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@IsDateDueNullificationAllowed BIT = NULL,
	@IsAssignedByIDNullificationAllowed BIT = NULL,
	@IsAssignedByStringNullificationAllowed BIT = NULL,
	@IsAssignedToIDNullificationAllowed BIT = NULL,
	@IsAssignedToStringNullificationAllowed BIT = NULL,
	@IsCompletedByIDNullificationAllowed BIT = NULL,
	@IsCompletedByStringNullificationAllowed BIT = NULL,
	@IsDateCompletedNullificationAllowed BIT = NULL,
	@IsParentTaskIDNullificationAllowed BIT = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE(),
		@ErrorMessage VARCHAR(4000) = NULL

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode) + ';' +
		'@NoteTypeID=' + dbo.fnToStringOrEmpty(@NoteTypeID) + ';' +
		'@NoteTypeCode=' + dbo.fnToStringOrEmpty(@NoteTypeCode) + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID) + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID) + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID) + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@DateDue=' + dbo.fnToStringOrEmpty(@DateDue) + ';' +
		'@AssignedByID=' + dbo.fnToStringOrEmpty(@AssignedByID) + ';' +
		'@AssignedByString=' + dbo.fnToStringOrEmpty(@AssignedByString) + ';' +
		'@AssignedToID=' + dbo.fnToStringOrEmpty(@AssignedToID) + ';' +
		'@AssignedToString=' + dbo.fnToStringOrEmpty(@AssignedToString) + ';' +
		'@CompletedByID=' + dbo.fnToStringOrEmpty(@CompletedByID) + ';' +
		'@CompletedByString=' + dbo.fnToStringOrEmpty(@CompletedByString) + ';' +
		'@DateCompleted=' + dbo.fnToStringOrEmpty(@DateCompleted) + ';' +
		'@TaskStatusID=' + dbo.fnToStringOrEmpty(@TaskStatusID) + ';' +
		'@TaskStatusCode=' + dbo.fnToStringOrEmpty(@TaskStatusCode) + ';' +
		'@ParentTaskID=' + dbo.fnToStringOrEmpty(@ParentTaskID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@IsDateDueNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsDateDueNullificationAllowed) + ';' +
		'@IsAssignedByIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedByIDNullificationAllowed) + ';' +
		'@IsAssignedByStringNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedByStringNullificationAllowed) + ';' +
		'@IsAssignedToIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedToIDNullificationAllowed) + ';' +
		'@IsAssignedToStringNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsAssignedToStringNullificationAllowed) + ';' +
		'@IsCompletedByIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsCompletedByIDNullificationAllowed) + ';' +
		'@IsCompletedByStringNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsCompletedByStringNullificationAllowed) + ';' +
		'@IsDateCompletedNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsDateCompletedNullificationAllowed) + ';' +
		'@IsParentTaskIDNullificationAllowed=' + dbo.fnToStringOrEmpty(@IsParentTaskIDNullificationAllowed) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;



	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	-- Log incoming arguments	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	
	*/	
		
	-----------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------------
	SET @IsDateDueNullificationAllowed = ISNULL(@IsDateDueNullificationAllowed, 0);
	SET @IsAssignedByIDNullificationAllowed = ISNULL(@IsAssignedByIDNullificationAllowed, 0);
	SET @IsAssignedByStringNullificationAllowed = ISNULL(@IsAssignedByStringNullificationAllowed, 0);
	SET @IsAssignedToIDNullificationAllowed = ISNULL(@IsAssignedToIDNullificationAllowed, 0);
	SET @IsAssignedToStringNullificationAllowed = ISNULL(@IsAssignedToStringNullificationAllowed, 0);
	SET @IsCompletedByIDNullificationAllowed = ISNULL(@IsCompletedByIDNullificationAllowed, 0);
	SET @IsCompletedByStringNullificationAllowed = ISNULL(@IsCompletedByStringNullificationAllowed, 0);
	SET @IsDateCompletedNullificationAllowed = ISNULL(@IsDateCompletedNullificationAllowed, 0);
	SET @IsParentTaskIDNullificationAllowed = ISNULL(@IsParentTaskIDNullificationAllowed, 0);
	SET @Debug = ISNULL(@Debug, 0);

	-----------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------------------------
	-- No local properties defined.

	-----------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------------
	SET @TaskStatusID = ISNULL(dbo.fnGetTaskStatusiD(@TaskStatusID), dbo.fnGetTaskStatusID(@TaskStatusCode));

	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables.' AS ActionMethod,
			@NoteID AS NoteID, @Args AS Arguments, @TaskStatusID AS TaskStatusID, @TaskStatusCode AS TaskStatusCode
	END


	-----------------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE @tblNotes AS TABLE (
		Idx INT IDENTITY(1,1),
		NoteID BIGINT
	);

	------------------------------------------------------------------------------------------------------------------------
	-- Parse notes.
	------------------------------------------------------------------------------------------------------------------------
	INSERT INTO @tblNotes (
		NoteID
	)
	SELECT 
		Value 
	FROM dbo.fnSplit(@NoteID, ',') 
	WHERE ISNUMERIC(Value) = 1


	-----------------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------------		
	BEGIN TRY
	-----------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	-----------------------------------------------------------------------------------------------------------------------	
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		-----------------------------------------------------------------------------------------------------------------------
		-- Update parent Note object.
		-----------------------------------------------------------------------------------------------------------------------	
		EXEC dbo.spNote_Update
			@NoteID = @NoteID,
			@BusinessEntityID = @BusinessEntityID,
			@ScopeID = @ScopeID,
			@ScopeCode = @ScopeCode,
			@NoteTypeID = @NoteTypeID,
			@NoteTypeCode = @NoteTypeCode,
			@NotebookID = @NotebookID,
			@NotebookCode = @NotebookCode,
			@Title = @Title,
			@Memo = @Memo,
			@PriorityID = @PriorityID,
			@PriorityCode = @PriorityCode,
			@Tags = @Tags,
			@OriginID = @OriginID,
			@OriginCode = @OriginCode,
			@OriginDataKey = @OriginDataKey,
			@AdditionalData = @AdditionalData,
			@CorrelationKey = @CorrelationKey,
			@ModifiedBy = @ModifiedBy


		-----------------------------------------------------------------------------------------------------------------------
		-- Update Task object.
		-----------------------------------------------------------------------------------------------------------------------
		UPDATE tsk
		SET
			tsk.DateDue = 
				CASE 
					WHEN @DateDue IS NULL AND ISNULL(@IsDateDueNullificationAllowed, 0) = 0 THEN tsk.DateDue 
					ELSE @DateDue 
				END,
			tsk.AssignedByID = 
				CASE 
					WHEN @AssignedByID IS NULL AND ISNULL(@IsAssignedByIDNullificationAllowed, 0) = 0 THEN tsk.AssignedByID 
					ELSE @AssignedByID 
				END,
			tsk.AssignedByString = 
				CASE 
					WHEN @AssignedByString IS NULL AND ISNULL(@IsAssignedByStringNullificationAllowed, 0) = 0 THEN tsk.AssignedByString 
					ELSE @AssignedByString 
				END,
			tsk.AssignedToID = 
				CASE 
					WHEN @AssignedToID IS NULL AND ISNULL(@IsAssignedToIDNullificationAllowed, 0) = 0 THEN tsk.AssignedToID 
					ELSE @AssignedToID 
				END,
			tsk.AssignedToString = 
				CASE 
					WHEN @AssignedToString IS NULL AND ISNULL(@IsAssignedToStringNullificationAllowed, 0) = 0 THEN tsk.AssignedToString 
					ELSE @AssignedToString 
				END,
			tsk.CompletedByID = 
				CASE 
					WHEN @CompletedByID IS NULL AND ISNULL(@IsCompletedByIDNullificationAllowed, 0) = 0 THEN tsk.CompletedByID 
					ELSE @CompletedByID 
				END,
			tsk.CompletedByString = 
				CASE 
					WHEN @CompletedByString IS NULL AND ISNULL(@IsCompletedByStringNullificationAllowed, 0) = 0 THEN tsk.CompletedByString 
					ELSE @CompletedByString 
				END,
			tsk.DateCompleted = 
				CASE 
					WHEN @DateCompleted IS NULL AND ISNULL(@IsDateCompletedNullificationAllowed, 0) = 0 THEN tsk.DateCompleted 
					ELSE @DateCompleted 
				END,
			tsk.TaskStatusID = 
				CASE 
					WHEN @TaskStatusID IS NULL THEN tsk.TaskStatusID 
					ELSE @TaskStatusID 
				END,
			tsk.ParentTaskID = 
				CASE 
					WHEN @ParentTaskID IS NULL AND ISNULL(@IsParentTaskIDNullificationAllowed, 0) = 0 THEN tsk.ParentTaskID 
					ELSE @ParentTaskID 
				END,
			tsk.ModifiedBy = 
				CASE 
					WHEN @ModifiedBy IS NULL THEN tsk.ModifiedBy 
					ELSE @ModifiedBy 
				END,
			tsk.DateModified = GETDATE()
		--SELECT *
		FROM @tblNotes tmp 
			JOIN dbo.Task tsk
				ON tmp.NoteID = tsk.NoteID

		-- 11/13/2015 - dhughes - Deprecated code. Removed to help increase performance.  
		--	JOIN dbo.Note n
		--		ON tsk.NoteID = n.NoteID
		--WHERE ( @BusinessEntityID IS NULL AND tsk.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteID, ',') WHERE ISNUMERIC(Value) = 1) )
		--		OR ( @BusinessEntityID IS NOT NULL AND ( tsk.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteID, ',') WHERE ISNUMERIC(Value) = 1)  
		--			AND n.BusinessEntityID = @BusinessEntityID ) )
			
			
	-----------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	-----------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		-----------------------------------------------------------------------------------------------------------------------
		-- Set error variables
		-----------------------------------------------------------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);
		
	END CATCH




END
