﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/9/2014
-- Description:	Creates an EventLog object.
-- SAMPLE CALL:
/*
	DECLARE 
		@EventLogID BIGINT,
		@EventLogTypeID INT = dbo.fnGetEventLogTypeID('CRED')
	
	EXEC dbo.spEventLog_Create
		@EventLogTypeID = @EventLogTypeID,
		@EventID = 1,
		@EventSource = 'spEventLog_Create',
		@EventDataString = 'CredentialEntityID: 80108',
		@EventLogID = @EventLogID OUTPUT,
		@CreatedBy = 'dhughes'
	
	SELECT @EventLogID AS EventLogID
*/

-- SELECT * FROM dbo.EventLog
-- =============================================
CREATE PROCEDURE [dbo].[spEventLog_Create]
	@EventLogTypeID INT,
	@EventID INT,
	@EventSource VARCHAR(256) = NULL,
	@EventDataXml XML = NULL,
	@EventDataString VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@EventLogID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------
	SET @EventLogID = NULL;	
	
	------------------------------------------------------------------------------
	-- Create a new EventLog object.
	-- <Summary>
	-- Creates a super-type EventLog object that can be further defined
	-- by a sub-type EventLog object (where applicable).
	-- </Summary>
	------------------------------------------------------------------------------
	INSERT INTO dbo.EventLog (
		EventLogTypeID,
		EventID,
		EventSource,
		EventDataXml,
		EventDataString,
		CreatedBy
	)
	SELECT
		@EventLogTypeID AS EventLogTypeID,
		@EventID AS EventID,
		@EventSource AS EventSource,
		@EventDataXml AS EventDataXml,
		@EventDataString AS EventDataString,
		@CreatedBy AS CreatedBy
	
	-- Retrieve record identity.
	SET @EventLogID = SCOPE_IDENTITY();
	
	
END
