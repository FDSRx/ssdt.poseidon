﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/8/2015
-- Description:	Updates a CalendarItem object.
-- Change log:
-- 2/29/2016 - dhuhges - Modified the procedure to accept additional inputs.

-- SAMPLE CALL:

/*

DECLARE
	@NoteID BIGINT= '2641',
	@BusinessEntityID BIGINT = NULL,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(50) = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = '<div><a href="javascript:urlRedirect(''MTM'',21656 )">CMR</a> completed on 10/05/2014.</div>',
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@Location VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = 1

	
EXEC dbo.spCalendarItem_Update
	@NoteID = @NoteID,
	@BusinessEntityID = @BusinessEntityID,
	@ScopeID = @ScopeID,
	@ScopeCode = @ScopeCode,
	@NoteTypeID = @NoteTypeID,
	@NoteTypeCode = @NoteTypeCode,
	@NotebookID = @NotebookID,
	@NotebookCode = @NotebookCode,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityID = @PriorityID,
	@PriorityCode = @PriorityCode,
	@Tags = @Tags,
	@OriginID = @OriginID,
	@OriginCode = @OriginCode,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@Location = @Location,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/


-- SELECT * FROM dbo.Note ORDER BY 1 DESC
-- SELECT * FROM dbo.CalendarItem ORDER BY 1 DESC
-- SELECT * FROM dbo.CalendarItem_History ORDER BY 1 DESC

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spCalendarItem_Update]
	@NoteID VARCHAR(MAX),
	@BusinessEntityID BIGINT = NULL,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(50) = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@Location VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------	
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000),
		@ErrorSeverity INT = NULL,
		@ErrorState INT = NULL,
		@ErrorNumber INT = NULL,
		@ErrorLine INT = NULL,
		@ErrorProcedure  NVARCHAR(200) = NULL


	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode) + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID) + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID) + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@Location=' + dbo.fnToStringOrEmpty(@Location) + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID) + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);


	-----------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ScopeID AS ScopeID, @ScopeCode AS ScopeCode, @NotebookID AS NotebookID, @NotebookCode AS NotebookCode, @Title AS Title, 
			@Memo AS Memo, @PriorityID AS PriorityID, @PriorityCode AS PriorityCode, @Tags AS Tags, @CorrelationKey AS CorrelationKey, 
			@AdditionalData AS AdditionalData, @Location AS Location, @OriginID AS OriginID, @OriginCode AS OriginCode, 
			@OriginDataKey AS OriginDataKey, @BusinessEntityID AS BusinessEntityID
	END	



	BEGIN TRY
	-----------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	-----------------------------------------------------------------------------------------------------------------------	
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	
		-----------------------------------------------------------------------------------------------------------------------
		-- Update CalendarItem object.
		-----------------------------------------------------------------------------------------------------------------------
		UPDATE ci
		SET
			ci.Location = CASE WHEN @Location IS NULL THEN ci.Location ELSE @Location END,
			ci.DateModified = GETDATE(),
			ci.ModifiedBy = @ModifiedBy
		--SELECT *
		FROM dbo.CalendarItem ci
			JOIN dbo.Note n
				ON ci.NoteID = n.NoteID
			JOIN (
				SELECT
					Value AS NoteID
				FROM dbo.fnSplit(@NoteID, ',')
				WHERE ISNUMERIC(Value) = 1
			) tmp
				ON tmp.NoteID = ci.NoteID
		WHERE (@BusinessEntityID IS NULL OR ( @BusinessEntityID IS NOT NULL AND n.BusinessEntityID = @BusinessEntityID ))
			
		
		-----------------------------------------------------------------------------------------------------------------------
		-- Update Note object.
		-----------------------------------------------------------------------------------------------------------------------
		EXEC dbo.spNote_Update
			@NoteID = @NoteID,
			@BusinessEntityID = @BusinessEntityID,
			@ScopeID = @ScopeID,
			@ScopeCode = @ScopeCode,
			@NoteTypeID = @NoteTypeID,
			@NoteTypeCode = @NoteTypeCode,
			@NotebookID = @NotebookID,
			@NotebookCode = @NotebookCode,
			@Title = @Title,
			@Memo = @Memo,
			@PriorityID = @PriorityID,
			@PriorityCode = @PriorityCode,
			@Tags = @Tags,
			@OriginID = @OriginID,
			@OriginCode = @OriginCode,
			@OriginDataKey = @OriginDataKey,
			@AdditionalData = @AdditionalData,
			@CorrelationKey = @CorrelationKey,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug


	-----------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	-----------------------------------------------------------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		-----------------------------------------------------------------------------------------------------------------------
		-- Set error variables
		-----------------------------------------------------------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);
		
	END CATCH	
	
	
	
		
END
