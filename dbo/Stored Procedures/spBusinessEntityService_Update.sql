﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/14/2014
-- Description:	Updates a business entity service record.
-- SELECT * FROM dbo.BusinessEntityService
-- SAMPLE CALL:
/*

DECLARE
	@BusinessEntityServiceID INT = NULL OUTPUT,
	@BusinessEntityID INT = NULL,
	@ServiceID INT = NULL,
	@AccountKey VARCHAR(256) = NULL,
	@ConnectionStatusID INT = NULL,
	@GoLiveDate DATETIME = NULL,
	@DateTermed DATETIME = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL

EXEC dbo.spBusinessEntityService_Update
	@BusinessEntityServiceID = @BusinessEntityServiceID,
	@BusinessEntityID = @BusinessEntityID,
	@ServiceID = @ServiceID,
	@AccountKey = @AccountKey,
	@ConnectionStatusID = @ConnectionStatusID,
	@GoLiveDate = @GoLiveDate,
	@DateTermed = @DateTermed,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug
*/
-- SELECT * FROM dbo.BusinessEntityService WHERE BusinessEntityID = 135590
-- SELECT * FROM dbo.ConnectionStatus
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityService_Update]
	@BusinessEntityServiceID INT = NULL OUTPUT,
	@BusinessEntityID INT = NULL,
	@ServiceID INT = NULL,
	@AccountKey VARCHAR(256) = NULL,
	@ConnectionStatusID INT = NULL,
	@GoLiveDate DATETIME = NULL,
	@DateTermed DATETIME = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	-------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
			
	-------------------------------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------------------------------
	DECLARE
		@HashKey VARCHAR(256),
		@ValidateSurrogateKey BIT = 0
	
	-------------------------------------------------------------------------------------------------------
	-- Set variables
	-------------------------------------------------------------------------------------------------------
	-- Create hash key.
	SET @HashKey = dbo.fnCreateRecordHashKey(
		dbo.fnToHashableString(@BusinessEntityID) +
		dbo.fnToHashableString(@ServiceID) +
		dbo.fnToHashableString(@AccountKey) +
		dbo.fnToHashableString(@ConnectionStatusID) +
		dbo.fnToHashableString(@GoLiveDate) +
		dbo.fnToHashableString(@DateTermed)
	);
	
	IF @BusinessEntityServiceID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @ServiceID = NULL;
		
		SET @ValidateSurrogateKey = 1;
	END
				
	-------------------------------------------------------------------------------------------------------
	-- Argument Null Exceptions
	-------------------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------------------
	-- Surrogate key validation.
	-- <Summary>
	-- If a surrogate key was provided, then that will be the only lookup
	-- performed.
	-- <Summary>
	-------------------------------------------------------------------------------------------------------
	IF ISNULL(@BusinessEntityServiceID, 0) = 0 AND @ValidateSurrogateKey = 1
	BEGIN			
		
		SET @ErrorMessage = 'Object reference not set to an instance of an object. The object, @BusinessEntityServiceID, cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	-------------------------------------------------------------------------------------------------------
	-- Unique row key validation.
	-- <Summary>
	-- If a surrogate key was not provided then we will try and perform the
	-- lookup based on the records unique key.
	-- </Summary>
	-------------------------------------------------------------------------------------------------------
	IF ISNULL(@ServiceID, 0) = 0 AND ISNULL(@BusinessEntityID, 0) = 0 AND @ValidateSurrogateKey = 0
	BEGIN
		
		SET @ErrorMessage = 'Object references not set to an instance of an object. The objects, @BusinessEntityID and @ServiceID cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END
	
	-------------------------------------------------------------------------------------------------------
	-- Temporary resources
	-------------------------------------------------------------------------------------------------------
	DECLARE
		@tblOutput AS TABLE (BusinessEntityServiceID BIGINT)
	
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@BusinessEntityServiceID AS BusinessEntityServiceID, @BusinessEntityID AS BusinessEntityID, 
			@ServiceID AS ServiceID, @ConnectionStatusID AS ConnectionStatusID, @GoLiveDate AS GoLiveDate, 
			@DateTermed AS DateTermed, @ModifiedBy AS ModifiedBy, @HashKey AS HashKey
	END
	
	-------------------------------------------------------------------------------------------------------
	-- Update new business service record.
	-------------------------------------------------------------------------------------------------------
	UPDATE bs
	SET
		AccountKey = CASE WHEN @AccountKey IS NOT NULL THEN @AccountKey ELSE AccountKey END,
		ConnectionStatusID = CASE WHEN @ConnectionStatusID IS NOT NULL THEN @ConnectionStatusID ELSE ConnectionStatusID END,
		GoLiveDate = ISNULL(@GoLiveDate, bs.GoLiveDate),
		DateTermed = ISNULL(@DateTermed, bs.DateTermed),
		HashKey = @HashKey,
		DateModified = GETDATE(),
		ModifiedBy = CASE WHEN @ModifiedBy IS NOT NULL THEN @ModifiedBy ELSE ModifiedBy END
	OUTPUT inserted.BusinessEntityServiceID INTO @tblOutput(BusinessEntityServiceID)
	FROM dbo.BusinessEntityService bs
	WHERE ( BusinessEntityServiceID = @BusinessEntityServiceID
		OR (
			BusinessEntityID = @BusinessEntityID
				AND ServiceID = @ServiceID
		)
	)
		AND ISNULL(HashKey, '') <> @HashKey
	
	
	
	-- Retrieve record identity.
	SELECT @BusinessEntityServiceID = (SELECT TOP 1 BusinessEntityServiceID FROM @tblOutput);
	

	
	
	
	
	
	
	
	
	
	
	
	
END
