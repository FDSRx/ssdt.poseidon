﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Get password
-- SAMPLE CALL:
/* 
DECLARE @Password VARBINARY(128), @Salt VARBINARY(10)
EXEC spPassword_Get 1, 1, @Password OUTPUT, @Salt OUTPUT
SELECT @Password, @Salt
*/
-- =============================================
CREATE PROCEDURE spPassword_Get
	@PersonID INT,
	@ServiceID INT,
	@PasswordHash VARBINARY(128) = NULL OUTPUT,
	@PasswordSalt VARBINARY(10) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		@PasswordHash = PasswordHash,
		@PasswordSalt = PasswordSalt
	FROM dbo.Password
	WHERE PersonID = @PersonID
		AND ServiceID = @ServiceID
		
END
