﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/19/2015
-- Description:	Deletes a calendar item.

-- SELECT * FROM dbo.CalendarItem
-- =============================================
CREATE PROCEDURE [dbo].[spCalendarItem_Delete]
	@NoteID BIGINT,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT;
	
	-------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		
	-------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy
		

	-------------------------------------------------------------------------------
	-- Begin data transaction.
	-------------------------------------------------------------------------------	
	BEGIN TRY
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	
		-------------------------------------------------------------------------------
		-- Remove calendar item record.
		-------------------------------------------------------------------------------
		DELETE ci
		--SELECT *
		FROM dbo.CalendarItem ci
		WHERE NoteID = @NoteID

		-------------------------------------------------------------------------------
		-- Remove calendar item from schedule.
		-------------------------------------------------------------------------------	
		EXEC schedule.spNoteSchedule_Delete
			@NoteID = @NoteID,
			@ModifiedBy = @ModifiedBy	

		-------------------------------------------------------------------------------
		-- Remove note item.
		-------------------------------------------------------------------------------	
		EXEC dbo.spNote_Delete
			@NoteID = @NoteID,
			@ModifiedBy = @ModifiedBy


	-------------------------------------------------------------------------------
	-- End data transaction.
	-------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);
		
	END CATCH


END
