﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/12/1982
-- Description:	Adds or updates the business entity address record.
-- Change Log:
-- DRH - 6/30/2015 - Modified the update section to only update the record if it is not considered a "void" or "empty"
--					record.  This will leave the existing record in tact until a more complete required is provided.
--					In the future, we could have a force null property that allows the address to be overriden with a
--					"void" or "empty" record.
-- SAMPLE CALL:
/*

spBusinessEntity_Address_Merge
	@BusinessEntityAddressID  = NULL,
	@BusinessEntityID  = 1,
	@AddressTypeID  = 5,
	@AddressID  = 250, --257 -- original
	@ModifiedBy  = 'dhughes@rxlps.com'

spBusinessEntity_Address_Merge
	@BusinessEntityAddressID  = 1,
	@BusinessEntityID  = 1,
	@AddressTypeID  = 5,
	@AddressID  = 257, --257 -- original
	@ModifiedBy  = 'dhughes@rxlps.com'
		
*/
-- SELECT * FROM dbo.BusinessEntityAddress
-- SELECT * FROM dbo.Address
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Address_Merge]
	@BusinessEntityAddressID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@AddressTypeID INT = NULL,
	@AddressID INT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidateSurrogateKey BIT = 0
		

	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------	
	IF @BusinessEntityAddressID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @AddressTypeID = NULL;
		
		SET @ValidateSurrogateKey = 1;
	END
	
	----------------------------------------------------------------------
	-- Argument Null Exceptions
	----------------------------------------------------------------------
	----------------------------------------------------------------------
	-- Surrogate key validation.
	-- <Summary>
	-- If a surrogate key was provided, then that will be the only lookup
	-- performed.
	-- <Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityAddressID, 0) = 0 AND @ValidateSurrogateKey = 1
	BEGIN			
		
		SET @ErrorMessage = 'Object reference not set to an instance of an object. The object, @BusinessEntityAddressID, cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	----------------------------------------------------------------------
	-- Unique row key validation.
	-- <Summary>
	-- If a surrogate key was not provided then we will try and perform the
	-- lookup based on the records unique key.
	-- </Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0 AND ISNULL(@AddressTypeID, 0) = 0 AND @ValidateSurrogateKey = 0
	BEGIN
		
		SET @ErrorMessage = 'Object references not set to an instance of an object. The objects, @BusinessEntityID and @AddressTypeID cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	--------------------------------------------------------------------
	-- Determine if record exists
	--------------------------------------------------------------------
	EXEC dbo.spBusinessEntity_Address_Exists
		@BusinessEntityAddressID = @BusinessEntityAddressID OUTPUT,
		@BusinessEntityID = @BusinessEntityID,
		@AddressTypeID = @AddressTypeID,
		@Exists = @Exists OUTPUT
		
	----------------------------------------------------------------------
	-- Merge data
	-- <Summary>
	-- If the record does not exist, then add the record; otherwise,
	-- update the record
	-- <Summary>
	----------------------------------------------------------------------	
	IF ISNULL(@Exists, 0) = 0
	BEGIN
		-- Add record
		EXEC dbo.spBusinessEntity_Address_Create
			@BusinessEntityID = @BusinessEntityID,
			@AddressTypeID = @AddressTypeID,
			@AddressID = @AddressID,
			@CreatedBy = @CreatedBy,
			@BusinessEntityAddressID = @BusinessEntityAddressID OUTPUT
	END
	ELSE
	BEGIN
	
		DECLARE
			@AddressLine1 VARCHAR(100) = NULL,
			@AddressLine2 VARCHAR(100) = NULL,
			@City VARCHAR(50) = NULL,
			@State VARCHAR(10) = NULL,
			@StateProvinceID INT = NULL,
			@PostalCode VARCHAR(25) = NULL,
			@SpatialLocation GEOGRAPHY = NULL,
			@Latitude DECIMAL(9,6) = NULL,
			@Longitude DECIMAL(9,6) = NULL
		
		-- Retrieve the address that is going to replace the existing record address.
		EXEC dbo.spAddress_Get
			@AddressID = @AddressID,
			@AddressLine1 = @AddressLine1 OUTPUT,
			@AddressLine2 = @AddressLine2 OUTPUT,
			@City = @City OUTPUT,
			@State = @State OUTPUT,
			@StateProvinceID = @StateProvinceID OUTPUT,
			@PostalCode = @PostalCode OUTPUT,
			@SpatialLocation = @SpatialLocation OUTPUT,
			@Latitude = @Latitude OUTPUT,
			@Longitude = @Longitude OUTPUT
		
		-- If the address is "void" or "empty" then lets not update the current address.
		-- We will leave in place for the time being until a better set of rules can be determine.
		IF 
			@AddressLine1 IS NULL 
			AND @AddressLine2 IS NULL 
			AND @City IS NULL 
			AND @State IS NULL 
			AND @StateProvinceID IS NULL
			AND @PostalCode IS NULL
			AND @SpatialLocation IS NULL
			AND @Latitude IS NULL
			AND @Longitude IS NULL
		BEGIN
			RETURN;
		END
			
		-- Update record
		EXEC dbo.spBusinessEntity_Address_Update
			@BusinessEntityAddressID = @BusinessEntityAddressID OUTPUT,
			@BusinessEntityID = @BusinessEntityID,
			@AddressTypeID = @AddressTypeID,
			@AddressID = @AddressID,
			@ModifiedBy = @ModifiedBy

	END


	
END
