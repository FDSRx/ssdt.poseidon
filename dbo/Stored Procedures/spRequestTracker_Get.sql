﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/3/2014
-- Description:	Retrieves a RequestTracker object.
-- SAMPLE CALL:
/*

DECLARE 
	@RequestTrackerID BIGINT,
	@RequestsRemaining INT, 
	@RequestCount INT,
	@IsLocked BIT, 
	@DateExpires DATETIME,
	@MaxRequestsAllowed INT
	
EXEC dbo.spRequestTracker_Get
	@RequestTrackerID = @RequestTrackerID OUTPUT,
	@ApplicationKey = 'MPC',
	@BusinessKey = '000000',
	@RequesterKey = 'dhughes',
	@RequestKey = 'MPC-000000-dhughes',
	@RequestCount = @RequestCount OUTPUT,
	@RequestsRemaining = @RequestsRemaining OUTPUT,
	@MaxRequestsAllowed = @MaxRequestsAllowed OUTPUT,
	@IsLocked = @IsLocked OUTPUT,
	@DateExpires = @DateExpires OUTPUT

SELECT
	@RequestTrackerID AS RequestTrackerID,
	@RequestCount AS RequestCount,
	@RequestsRemaining AS RequestRemaining,
	@MaxRequestsAllowed AS MaxRequestsAllowed,
	@IsLocked AS IsLocked,
	@DateExpires AS DateExpires
	
*/

-- SELECT * FROM dbo.RequestTracker
-- =============================================
CREATE PROCEDURE [dbo].[spRequestTracker_Get]
	@RequestTrackerID BIGINT = NULL OUTPUT,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@RequesterKey VARCHAR(256) = NULL,
	@RequestKey VARCHAR(255) = NULL,
	@RequestCount INT = NULL OUTPUT,
	@RequestsRemaining INT = NULL OUTPUT,
	@MaxRequestsAllowed INT = NULL OUTPUT,
	@IsLocked BIT = NULL OUTPUT,
	@DateExpires DATETIME = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------
	SET @RequestCount = NULL;
	SET @RequestsRemaining = NULL;
	SET @MaxRequestsAllowed = NULL;
	SET @IsLocked = 0;
	SET @DateExpires = '12/31/9999';

	---------------------------------------------------------------------------------------------
	-- Determine which row identifier should be used to look up the record.
	---------------------------------------------------------------------------------------------
	IF @RequestTrackerID IS NOT NULL
	BEGIN
		SET @ApplicationKey = NULL;
		SET @BusinessKey = NULL;
		SET @RequesterKey = NULL;
		SET @RequestKey = NULL;
	END	
	
	-- Debug
	--SELECT @RequestTrackerID AS RequestTrackerID, @ApplicationKey AS ApplicationKey, @BusinessKey AS BusinessKey, @RequesterKey AS RequesterKey, @RequestKey AS RequestKey

	---------------------------------------------------------------------------------------------
	-- Retrieve the sepcified record.
	---------------------------------------------------------------------------------------------
	SELECT
		@RequestTrackerID = RequestTrackerID,
		@RequestCount = RequestCount,
		@RequestsRemaining = RequestsRemaining,
		@MaxRequestsAllowed = MaxRequestsAllowed,
		@IsLocked = IsLocked,
		@DateExpires = DateExpires
	FROM dbo.RequestTracker
	WHERE RequestTrackerID = @RequestTrackerID 
		-- Includes: ApplicationKey, BusinessKey, RequesterKey, and RequestKey
		OR (
			( ApplicationKey = @ApplicationKey )
			AND ( BusinessKey = @BusinessKey )
			AND ( RequesterKey = @RequesterKey )
			AND RequestKey = @RequestKey
		)
		-- Includes: BusinessKey, RequesterKey, and RequestKey
		-- Excludes: ApplicationKey
		OR (
			ApplicationKey IS NULL
			AND ( BusinessKey = @BusinessKey )
			AND ( RequesterKey = @RequesterKey )
			AND RequestKey = @RequestKey
		)
		-- Includes: RequesterKey, and RequestKey
		-- Excludes: ApplicationKey, BusinessKey
		OR (
			ApplicationKey IS NULL
			AND BusinessKey IS NULL
			AND ( RequesterKey = @RequesterKey )
			AND RequestKey = @RequestKey
		)
		-- Includes: RequestKey
		-- Excludes: ApplicationKey, BusinessKey, RequesterKey
		OR (
			ApplicationKey IS NULL
			AND BusinessKey IS NULL
			AND RequesterKey IS NULL
			AND RequestKey = @RequestKey
		)
		-- Includes: ApplicationKey, BusinessKey, RequestKey
		-- Excludes: RequesterKey
		OR (
			ApplicationKey = @ApplicationKey
			AND BusinessKey = @BusinessKey
			AND RequesterKey IS NULL
			AND RequestKey = @RequestKey
		)
		-- Includes: ApplicationKey, RequestKey
		-- Excludes: BusinessKey, RequesterKey
		OR (
			ApplicationKey = @ApplicationKey
			AND BusinessKey IS NULL
			AND RequesterKey IS NULL
			AND RequestKey = @RequestKey
		)
		-- Includes: BusinessKey, RequestKey
		-- Excludes: ApplicationKey, RequesterKey
		OR (
			ApplicationKey IS NULL
			AND BusinessKey = @BusinessKey
			AND RequesterKey IS NULL
			AND RequestKey = @RequestKey
		)
		
	
	IF @@ROWCOUNT = 0
	BEGIN
		SET @RequestTrackerID = NULL;
		SET @ApplicationKey = NULL;
		SET @BusinessKey = NULL;
		SET @RequesterKey = NULL;
		SET @RequestKey = NULL;
	END	
		
		
END
