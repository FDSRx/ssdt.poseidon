﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/14/2014
-- Description:	Add or updates the provided record.
/*

DECLARE 
	@BusinessEntityServiceUmbrellaID BIGINT = NULL,
	@Exists BIT = NULL
	
EXEC dbo.spBusinessEntityServiceUmbrella_Merge
	@BusinessEntityServiceUmbrellaID = @BusinessEntityServiceUmbrellaID OUTPUT,
	@BusinessEntityID = 1,
	@ServiceID = 4,
	@BusinessEntityUmbrellaID = 5032,
	@CreatedBy = 'dhughes',
	@ModifiedBy = 'dhughes',
	@Exists = @Exists OUTPUT

SELECT @BusinessEntityServiceUmbrellaID AS BusinessEntityServiceUmbrellaID, @Exists AS IsFound

*/
-- SELECT * FROM dbo.BusinessEntityServiceUmbrella
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityServiceUmbrella_Merge]
	@BusinessEntityServiceUmbrellaID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT,
	@ServiceID INT,
	@BusinessEntityUmbrellaID BIGINT,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = 0	OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);
	SET @ModifiedBy = ISNULL(@ModifiedBy, @CreatedBy);
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidateUniqueRowKey BIT = 1

	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------	
	IF @BusinessEntityServiceUmbrellaID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @ServiceID = NULL;
		
		SET @ValidateUniqueRowKey = 1;
	END
				
	--------------------------------------------------------------------
	-- Argument Null Exceptions
	--------------------------------------------------------------------
	
	IF @BusinessEntityServiceUmbrellaID IS NULL AND
		@BusinessEntityID IS NULL AND
		@ServiceID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object references (@BusinessEntityServiceUmbrellaID, @BusinessEntityID, @ServiceID) are not set to an instance ' +
			'of an object. The objects (@BusinessEntityServiceUmbrellaID, @BusinessEntityID, @ServiceID) cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;	
	END
	
	--------------------------------------------------------------------
	-- Business Entity Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null business entity ID
	-- was passed
	--------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0 AND @ValidateUniqueRowKey = 1
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@BusinessEntityID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;	
	END	
	
	--------------------------------------------------------------------
	-- Service Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null service ID
	-- was passed
	--------------------------------------------------------------------
	IF ISNULL(@ServiceID, 0) = 0 AND @ValidateUniqueRowKey = 1
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@ServiceID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	-- Debug
	--SELECT @BusinessEntityServiceUmbrellaID AS BusinessEntityServiceUmbrellaID, @BusinessEntityID AS BusinessEntityID, @ServiceID AS ServiceID
	
	
	----------------------------------------------------------------------
	-- Determine if record exists.
	----------------------------------------------------------------------
	EXEC dbo.spBusinessEntityServiceUmbrella_Exists
		@BusinessEntityServiceUmbrellaID = @BusinessEntityServiceUmbrellaID OUTPUT,
		@BusinessEntityID = @BusinessEntityID,
		@ServiceID = @ServiceID,
		@Exists = @Exists OUTPUT
	
	
	----------------------------------------------------------------------
	-- Merge data
	-- <Summary>
	-- If the record does not exist, then add the record; otherwise,
	-- update the record
	-- <Summary>
	----------------------------------------------------------------------	
	IF ISNULL(@Exists, 0) = 0
	BEGIN
	
		-- Add record
		EXEC dbo.spBusinessEntityServiceUmbrella_Create
			@BusinessEntityID = @BusinessEntityID,
			@ServiceID = @ServiceID,
			@BusinessEntityUmbrellaID = @BusinessEntityUmbrellaID,
			@CreatedBy = @CreatedBy,
			@BusinessEntityServiceUmbrellaID = @BusinessEntityServiceUmbrellaID OUTPUT
	END
	ELSE
	BEGIN
	
		-- Update record
		EXEC dbo.spBusinessEntityServiceUmbrella_Update
			@BusinessEntityServiceUmbrellaID = @BusinessEntityServiceUmbrellaID OUTPUT,
			@BusinessEntityID = @BusinessEntityID,
			@ServiceID = @ServiceID,
			@BusinessEntityUmbrellaID = @BusinessEntityUmbrellaID,
			@ModifiedBy = @ModifiedBy,
			@Exists = @Exists OUTPUT
				
	END
	
	
	
	
	
	
	
	
	
	
	
END
