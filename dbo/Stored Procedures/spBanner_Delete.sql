﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/01/2016
-- Description:	Deletes a Banner object(s).
-- SAMPLE CALL:
/*

DECLARE
	@BannerID BIGINT = 21,
	@BannerTypeID INT = NULL,
	@BannerTypeCode VARCHAR(50) = NULL,
	@Name VARCHAR(256) = NULL,
	@Header VARCHAR(MAX) = NULL,
	@Body VARCHAR(MAX) = NULL,
	@Footer VARCHAR(MAX) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL

EXEC dbo.spBanner_Delete
	@BannerID = @BannerID,
	@BannerTypeID = @BannerTypeID,
	@BannerTypeCode = @BannerTypeCode,
	@Name = @Name,
	@Header = @Header,
	@Body = @Body,
	@Footer = @Footer,
	@LanguageID = @LanguageID,
	@LanguageCode = @LanguageCode,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

SELECT @BannerID AS BannerID


*/

-- SELECT * FROM dbo.Banner
-- =============================================
CREATE PROCEDURE [dbo].[spBanner_Delete]
	@BannerID VARCHAR(MAX) = NULL,
	@BannerTypeID INT = NULL,
	@BannerTypeCode VARCHAR(50) = NULL,
	@Name VARCHAR(256) = NULL,
	@Header VARCHAR(MAX) = NULL,
	@Body VARCHAR(MAX) = NULL,
	@Footer VARCHAR(MAX) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	----------------------------------------------------------------------------------------------------
	-- Instance variables.
	----------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@BannerID=' + dbo.fnToStringOrEmpty(@BannerID) + ';' +
		'@BannerTypeID=' + dbo.fnToStringOrEmpty(@BannerTypeID) + ';' +
		'@BannerTypeCode=' + dbo.fnToStringOrEmpty(@BannerTypeCode) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@Header=' + dbo.fnToStringOrEmpty(@Header) + ';' +
		'@Body=' + dbo.fnToStringOrEmpty(@Body) + ';' +
		'@Footer=' + dbo.fnToStringOrEmpty(@Footer) + ';' +
		'@LanguageID=' + dbo.fnToStringOrEmpty(@LanguageID) + ';' +
		'@LanguageCode=' + dbo.fnToStringOrEmpty(@LanguageCode) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	----------------------------------------------------------------------------------------------------
	-- Log request.
	----------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	----------------------------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @ModifiedBy = COALESCE(@ModifiedBy, SUSER_NAME());

	----------------------------------------------------------------------------------------------------
	-- Set variables.
	----------------------------------------------------------------------------------------------------
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' ActionMethod,
			@BannerID AS BannerID
	END


	----------------------------------------------------------------------------------------------------
	-- Temporary resources.
	----------------------------------------------------------------------------------------------------
	-- Banners
	IF OBJECT_ID('tempdb..#tmpBanners') IS NOT NULL
	BEGIN
		DROP TABLE #tmpBanners;
	END

	CREATE TABLE #tmpBanners (
		Idx BIGINT IDENTITY(1,1),
		BannerID BIGINT
	);

	----------------------------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	----------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy

	----------------------------------------------------------------------------------------------------
	-- Parse banners.
	----------------------------------------------------------------------------------------------------
	INSERT INTO #tmpBanners (
		BannerID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@BannerID, ',')
	WHERE ISNUMERIC(Value) = 1

	----------------------------------------------------------------------------------------------------
	-- Create object.
	----------------------------------------------------------------------------------------------------
	BEGIN TRY
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	----------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	----------------------------------------------------------------------------------------------------

		-- Delete banner schedule(s).
		DELETE trgt
		--SELECT *
		FROM dbo.BannerSchedule trgt
			JOIN #tmpBanners tmp
				ON tmp.BannerID = trgt.BannerID

		-- Delete banner(s).
		DELETE trgt
		--SELECT *
		FROM dbo.Banner trgt
			JOIN #tmpBanners tmp
				ON tmp.BannerID = trgt.BannerID

	----------------------------------------------------------------------------------------------------
	-- End data transaction.
	----------------------------------------------------------------------------------------------------		
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args

		SET @ErrorMessage = ERROR_MESSAGE();
	
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);		

	END CATCH

END