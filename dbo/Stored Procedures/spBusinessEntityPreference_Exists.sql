﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/20/2015
-- Description:	Updates a BusinessEntityPreference object.
-- SAMPLE CALL;
/*

DECLARE @Exists BIT = NULL

EXEC dbo.spBusinessEntityPreference_Exists
	@BusinessEntityID = 135590,
	@PreferenceID = 1,
	@Exists = @Exists OUTPUT

SELECT @Exists AS IsFound

*/

-- SELECT * FROM dbo.BusinessEntityPreference
-- =============================================
CREATE PROCEDURE spBusinessEntityPreference_Exists
	@BusinessEntityPreferenceID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@PreferenceID INT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------
	SET @Exists = 0;
	
	
	-----------------------------------------------------------------------------------------------
	-- Determine if the BusinessEntityPreference object exists.
	-----------------------------------------------------------------------------------------------
	-- The record identity has primary search priority.
	IF @BusinessEntityPreferenceID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @PreferenceID = NULL;
	END
	
	SELECT TOP 1
		@BusinessEntityPreferenceID = BusinessEntityPreferenceID,
		@BusinessEntityID = BusinessEntityID,
		@PreferenceID = @PreferenceID,
		@Exists = 1
	FROM dbo.BusinessEntityPreference
	WHERE BusinessEntityPreferenceID = @BusinessEntityPreferenceID
		OR (
			BusinessEntityID = @BusinessEntityID
			AND PreferenceID = @PreferenceID
		) 
	
	-- If no records were discovered then void the output data.
	IF @@ROWCOUNT = 0
	BEGIN
		SET @BusinessEntityPreferenceID = NULL;
		SET @BusinessEntityID = NULL;
		SET @PreferenceID = NULL;
		SET @Exists = 0;
	END
	
END
