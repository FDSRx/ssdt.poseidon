﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/18/2016
-- Description:	Deletes a note item and all of its references
-- SAMPLE CALL:
/*

DECLARE
	@NoteID VARCHAR(MAX) = '70963',
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@Debug BIT = 1

EXEC dbo.spNote_Delete_AllReferences
	@NoteID = @NoteID,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/

-- ROLLBACK TRANSACTION;

-- SELECT * FROM dbo.Note ORDER BY 1 DESC
-- SELECT * FROM dbo.CalendarItem
-- SELECT * FROM dbo.Task
-- SELECT * FROM dbo.NoteTag
-- SELECT * FROM dbo.NoteInteraction

-- SELECT TOP 10 * FROM dbo.Note ORDER BY 1 DESC
-- SELECT * FROM dbo.vwNoteTag ORDER BY 1 DESC

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spNote_Delete_AllReferences]
	@NoteID VARCHAR(MAX),
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID)  + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;
		

	----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------------------------------------------------
	SET @NoteID = NULLIF(@NoteID, '');

	----------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	----------------------------------------------------------------------------------------------------------------------------	

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS procedureName, 'Get/Set variables.' AS ActionMethod,
			@NoteID AS NoteID, @ModifiedBy AS ModifiedBy
	END


	----------------------------------------------------------------------------------------------------------------------------
	-- Short circuit.
	----------------------------------------------------------------------------------------------------------------------------
	IF @NoteID IS NULL
	BEGIN
		RETURN;
	END

	----------------------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	----------------------------------------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpNoteBaseAndInherited') IS NOT NULL
	BEGIN
		DROP TABLE #tmpNoteBaseAndInherited;
	END

	CREATE TABLE #tmpNoteBaseAndInherited (
		Idx BIGINT IDENTITY(1,1),
		NoteID BIGINT
	);

	----------------------------------------------------------------------------------------------------------------------------
	-- Parse objects.
	----------------------------------------------------------------------------------------------------------------------------
	INSERT INTO #tmpNoteBaseAndInherited (
		NoteID
	)
	SELECT DISTINCT
		Value
	FROM dbo.fnSplit(@NoteID, ',')
	WHERE ISNUMERIC(Value) = 1
			
	----------------------------------------------------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	----------------------------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy
		

	----------------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	----------------------------------------------------------------------------------------------------------------------------	
	BEGIN TRY
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	
		----------------------------------------------------------------------------------------------------------------------------
		-- Remove NoteSchedule object(s).
		----------------------------------------------------------------------------------------------------------------------------
		DELETE trgt
		FROM Schedule.NoteSchedule trgt
		WHERE NoteID IN (SELECT NoteID FROM #tmpNoteBaseAndInherited) 	

		----------------------------------------------------------------------------------------------------------------------------
		-- Remove CalendarItem object(s).
		----------------------------------------------------------------------------------------------------------------------------
		DELETE trgt
		FROM dbo.CalendarItem trgt
		WHERE NoteID IN (SELECT NoteID FROM #tmpNoteBaseAndInherited)	

		----------------------------------------------------------------------------------------------------------------------------
		-- Remove Task object(s).
		----------------------------------------------------------------------------------------------------------------------------
		DELETE trgt
		FROM dbo.Task trgt
		WHERE NoteID IN (SELECT NoteID FROM #tmpNoteBaseAndInherited) 

		----------------------------------------------------------------------------------------------------------------------------
		-- Remove NoteInteraction object(s).
		----------------------------------------------------------------------------------------------------------------------------
		DELETE trgt
		FROM dbo.NoteInteraction trgt
		WHERE NoteID IN (SELECT NoteID FROM #tmpNoteBaseAndInherited)
			OR CommentNoteID IN (SELECT NoteID FROM #tmpNoteBaseAndInherited)

		----------------------------------------------------------------------------------------------------------------------------
		-- Remove existing note tags.
		----------------------------------------------------------------------------------------------------------------------------
		EXEC dbo.spNoteTag_Delete
			@NoteID = @NoteID,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug
		
		----------------------------------------------------------------------------------------------------------------------------
		-- Remove note item record.
		----------------------------------------------------------------------------------------------------------------------------
		DELETE n
		--SELECT *
		FROM dbo.Note n
		WHERE NoteID IN (SELECT NoteID FROM #tmpNoteBaseAndInherited)

	----------------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	----------------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args

		SET @ErrorMessage = ERROR_MESSAGE();
	
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		
	END CATCH


	----------------------------------------------------------------------------------------------------------------------------
	-- Dispose of resources.
	----------------------------------------------------------------------------------------------------------------------------
	DROP TABLE #tmpNoteBaseAndInherited;

END
