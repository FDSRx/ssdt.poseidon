﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/14/2013
-- Description:	Updates a business entity's email address
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Email_Update] 
	@BusinessEntityEmailAddressID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@EmailAddress VARCHAR(255) = NULL,
	@EmailAddressTypeID INT = NULL,
	@IsEmailAllowed BIT = NULL,
	@IsPreferred BIT = NULL,
	@ModifiedBy VARCHAR(256) = NULL	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-----------------------------------------------------------------------------
	-- Clean data
	-----------------------------------------------------------------------------
	SET @EmailAddress = CASE WHEN LTRIM(RTRIM(ISNULL(@EmailAddress, ''))) = '' THEN NULL ELSE @EmailAddress END;
	
	--------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------
	DECLARE @HashKey VARCHAR(256)

	--------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------	
	-- Create hash key.
	SET @HashKey = dbo.fnCreateRecordHashKey( 
		dbo.fnToHashableString(@BusinessEntityID) + 
		dbo.fnToHashableString(@EmailAddressTypeID) +
		dbo.fnToHashableString(@EmailAddress) +
		dbo.fnToHashableString(@IsEmailAllowed) +
		dbo.fnToHashableString(@IsPreferred)
	);
	
	-----------------------------------------------------------------------------
	-- Temporary resources
	-----------------------------------------------------------------------------
	DECLARE
		@tblOutput AS TABLE (BusinessEntityEmailAddressID BIGINT)

	-----------------------------------------------------------------------------
	-- Update data.
	-----------------------------------------------------------------------------	
	-- If the surrogate record identifier was provided then use the row identifer; otherwise, use the
	-- BusinessEntityID/EmailAddressType combination.
	IF @BusinessEntityEmailAddressID IS NOT NULL
	BEGIN
	
		UPDATE TOP(1) addr
		SET
			addr.EmailAddress = CASE WHEN @EmailAddress IS NOT NULL THEN @EmailAddress ELSE addr.EmailAddress END,
			addr.EmailAddressTypeID = CASE WHEN @EmailAddressTypeID IS NOT NULL THEN @EmailAddressTypeID ELSE addr.EmailAddressTypeID END,
			addr.IsEmailAllowed = CASE WHEN @IsEmailAllowed IS NOT NULL THEN @IsEmailAllowed ELSE addr.IsEmailAllowed END,
			addr.IsPreferred = CASE WHEN @IsPreferred IS NOT NULL THEN @IsPreferred ELSE addr.IsPreferred END,
			addr.HashKey = @HashKey,
			addr.ModifiedBy = CASE WHEN @ModifiedBy IS NOT NULL THEN @ModifiedBy ELSE addr.ModifiedBy END,
			addr.DateModified = GETDATE()
		OUTPUT inserted.BusinessEntityEmailAddressID INTO @tblOutput(BusinessEntityEmailAddressID)
		--SELECT *
		FROM dbo.BusinessEntityEmailAddress addr
		WHERE addr.BusinessEntityEmailAddressID = @BusinessEntityEmailAddressID
			AND ISNULL(HashKey, '') <> @HashKey
	
	END
	ELSE -- caller wishes to initiate a strict update (wants to ensure correct business entity)
	BEGIN
	
		UPDATE TOP(1) addr
		SET
			addr.EmailAddress = CASE WHEN @EmailAddress IS NOT NULL THEN @EmailAddress ELSE addr.EmailAddress END,
			addr.EmailAddressTypeID = CASE WHEN @EmailAddressTypeID IS NOT NULL THEN @EmailAddressTypeID ELSE addr.EmailAddressTypeID END,
			addr.IsEmailAllowed = CASE WHEN @IsEmailAllowed IS NOT NULL THEN @IsEmailAllowed ELSE addr.IsEmailAllowed END,
			addr.IsPreferred = CASE WHEN @IsPreferred IS NOT NULL THEN @IsPreferred ELSE addr.IsPreferred END,
			addr.HashKey = @HashKey,
			addr.ModifiedBy = CASE WHEN @ModifiedBy IS NOT NULL THEN @ModifiedBy ELSE addr.ModifiedBy END,
			addr.DateModified = GETDATE()
		OUTPUT inserted.BusinessEntityEmailAddressID INTO @tblOutput(BusinessEntityEmailAddressID)
		--SELECT *
		FROM dbo.BusinessEntityEmailAddress addr
		WHERE addr.BusinessEntityID = @BusinessEntityID
			AND addr.BusinessEntityEmailAddressID = @BusinessEntityEmailAddressID
			AND ISNULL(HashKey, '') <> @HashKey
	
	END

	-----------------------------------------------------------------------------
	-- Retrieve record information.
	----------------------------------------------------------------------------- 
	SET @BusinessEntityEmailAddressID = (SELECT TOP 1 BusinessEntityEmailAddressID FROM @tblOutput);
	
	
	
	
END
