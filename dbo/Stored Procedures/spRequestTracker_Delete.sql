﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/3/2014
-- Description:	Deletes expired requests or a specified request.
-- SAMPLE CALL: 
/*

EXEC dbo.spRequestTracker_Delete 
	@RequestTrackerID = 2


*/

-- SELECT * FROM dbo.RequestTracker
-- =============================================
CREATE PROCEDURE [dbo].[spRequestTracker_Delete]
	@RequestTrackerID BIGINT = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@RequesterKey VARCHAR(256) = NULL,
	@RequestKey VARCHAR(255) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------
	-- Always remove expired keys via every delete request.
	---------------------------------------------------------------------------------------------
	DELETE
	FROM dbo.RequestTracker
	WHERE DateExpires < GETDATE()

	---------------------------------------------------------------------------------------------
	-- Determine which row identifier should be used to look up record.
	---------------------------------------------------------------------------------------------
	IF @RequestTrackerID IS NOT NULL
	BEGIN
		SET @ApplicationKey = NULL;
		SET @BusinessKey = NULL;
		SET @RequesterKey = NULL;
		SET @RequestKey = NULL;
	END
	
	---------------------------------------------------------------------------------------------
    -- Delete specific key.
    ---------------------------------------------------------------------------------------------
    DELETE
    FROM dbo.RequestTracker
	WHERE RequestTrackerID = @RequestTrackerID 
		OR (
			@RequestTrackerID IS NULL
			AND ApplicationKey = @ApplicationKey
			AND BusinessKey = @BusinessKey
			AND RequesterKey = @RequesterKey
			AND RequestKey = @RequestKey
		)
		OR (
			@RequestTrackerID IS NULL
			AND ApplicationKey IS NULL
			AND BusinessKey = @BusinessKey
			AND RequesterKey = @RequesterKey
			AND RequestKey = @RequestKey
		)
		OR (
			@RequestTrackerID IS NULL
			AND ApplicationKey IS NULL
			AND BusinessKey IS NULL
			AND RequesterKey = @RequesterKey
			AND RequestKey = @RequestKey
		)
		OR (
			@RequestTrackerID IS NULL
			AND ApplicationKey IS NULL
			AND BusinessKey IS NULL
			AND RequesterKey IS NULL
			AND RequestKey = @RequestKey
		)
    
END
