﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/14/2015
-- Description:	Retrieves a Note object.
-- SAMPLE CALL:
/*

DECLARE
	@NoteID BIGINT = 451,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(25) = NULL,
	@ScopeName VARCHAR(50) = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(25) = NULL,
	@NoteTypeName VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(25) = NULL,
	@NotebookName VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(25) = NULL,
	@PriorityName VARCHAR(50) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(25) = NULL,
	@OriginName VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL

EXEC dbo.spNote_Get
	@NoteID = @NoteID,
	@ApplicationID = @ApplicationID OUTPUT,
	@ApplicationCode = @ApplicationCode,
	@BusinessID = @BusinessID OUTPUT,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@BusinessEntityID = @BusinessEntityID OUTPUT,
	@ScopeID = @ScopeID OUTPUT,
	@ScopeCode = @ScopeCode OUTPUT,
	@ScopeName = @ScopeName OUTPUT,
	@NoteTypeID = @NoteTypeID OUTPUT,
	@NoteTypeCode = @NoteTypeCode OUTPUT,
	@NoteTypeName = @NoteTypeName OUTPUT,
	@NotebookID = @NotebookID OUTPUT,
	@NotebookCode = @NotebookCode OUTPUT,
	@NotebookName = @NotebookName OUTPUT,
	@Title = @Title OUTPUT,
	@Memo = @Memo OUTPUT,
	@PriorityID = @PriorityID OUTPUT,
	@PriorityCode = @PriorityCode OUTPUT,
	@PriorityName = @PriorityName OUTPUT,
	@OriginID = @OriginID OUTPUT,
	@OriginCode = @OriginCode OUTPUT,
	@OriginName = @OriginName OUTPUT,
	@OriginDataKey = @OriginDataKey OUTPUT,
	@AdditionalData = @AdditionalData OUTPUT,
	@CorrelationKey = @CorrelationKey OUTPUT


SELECT 
	@NoteID AS NoteID, @BusinessEntityID AS BusinessEntityID, @ScopeID AS ScopeID, @ScopeCode AS ScopeCode, @ScopeName AS ScopeName,
	@NoteTypeID AS NoteTypeID, @NoteTypeCode AS NoteTypeCode, @NoteTypeName AS NoteTypeName, @NotebookID AS NotebookID, 
	@NotebookCode AS NotebookCode, @NotebookName AS NotebookName, @Title AS Title, @Memo AS Memo, @PriorityID AS PriorityID,
	@PriorityCode AS PriorityCode, @PriorityName AS PriorityName, @OriginID AS OriginID, @OriginCode AS OriginCode, @OriginName AS OriginName,
	@OriginDataKey AS OriginDataKey, @AdditionalData AS AdditionalData, @CorrelationKey AS CorrelationKey,
	@ApplicationID AS ApplicationID, @BusinessID AS BusinessID


*/

-- SELECT TOP 10 * FROM dbo.Note
-- =============================================
CREATE PROCEDURE [dbo].[spNote_Get]
	@NoteID BIGINT,
	@ApplicationID INT = NULL OUTPUT,
	@ApplicationCode VARCHAR(50) = NULL OUTPUT,
	@BusinessID BIGINT = NULL OUTPUT,
	@BusinessKey VARCHAR(50) = NULL OUTPUT,
	@BusinessKeyType VARCHAR(50) = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@ScopeID INT = NULL OUTPUT,
	@ScopeCode VARCHAR(25) = NULL OUTPUT,
	@ScopeName VARCHAR(50) = NULL OUTPUT,
	@NoteTypeID INT = NULL OUTPUT,
	@NoteTypeCode VARCHAR(25) = NULL OUTPUT,
	@NoteTypeName VARCHAR(50) = NULL OUTPUT,
	@NotebookID INT = NULL OUTPUT,
	@NotebookCode VARCHAR(25) = NULL OUTPUT,
	@NotebookName VARCHAR(50) = NULL OUTPUT,
	@Title VARCHAR(1000) = NULL OUTPUT,
	@Memo VARCHAR(MAX) = NULL OUTPUT,
	@PriorityID INT = NULL OUTPUT,
	@PriorityCode VARCHAR(25) = NULL OUTPUT,
	@PriorityName VARCHAR(50) = NULL OUTPUT,
	@OriginID INT = NULL OUTPUT,
	@OriginCode VARCHAR(25) = NULL OUTPUT,
	@OriginName VARCHAR(50) = NULL OUTPUT,
	@OriginDataKey VARCHAR(256) = NULL OUTPUT,
	@AdditionalData XML = NULL OUTPUT,
	@CorrelationKey VARCHAR(256) = NULL OUTPUT
AS
BEGIN	

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	-- No sanitization required.

	------------------------------------------------------------------------------------------------------------
	-- Retrieve object.
	------------------------------------------------------------------------------------------------------------
	SELECT TOP 1
		@NoteID = nte.NoteID,
		@ApplicationID = nte.ApplicationID,
		@ApplicationCode = app.Code,
		@BusinessID = @BusinessID,
		@BusinessEntityID = nte.BusinessEntityID,
		@ScopeID = nte.ScopeID,
		@ScopeCode = scp.Code,
		@ScopeName = scp.Name,
		@NoteTypeID = nte.NoteTypeID,
		@NoteTypeCode = nt.Code,
		@NoteTypeName = nt.Name,
		@NotebookID = nte.NotebookID,
		@NotebookCode = nb.Code,
		@NotebookName = nb.Name,
		@Title = nte.Title,
		@Memo = nte.Memo,
		@PriorityID = nte.PriorityID,
		@PriorityCode = pri.Code,
		@PriorityName = pri.Name,
		@OriginID = nte.OriginID,
		@OriginCode = org.Code,
		@OriginName = org.Name,
		@OriginDataKey = nte.OriginDataKey,
		@AdditionalData = nte.AdditionalData,
		@CorrelationKey = nte.CorrelationKey
	FROM dbo.Note nte
		LEFT JOIN dbo.Scope scp
			ON scp.ScopeID = nte.ScopeID
		LEFT JOIN dbo.NoteType nt
			ON nt.NoteTypeID = nte.NoteTypeID
		LEFT JOIN dbo.Notebook nb
			ON nb.NotebookID = nte.NotebookID
		LEFT JOIN dbo.Priority pri
			ON pri.PriorityID = nte.PriorityID
		LEFT JOIN dbo.Origin org
			ON org.OriginID = nte.OriginID
		LEFT JOIN dbo.Application app
			ON app.ApplicationID = nte.ApplicationID
	WHERE nte.NoteID = @NoteID


END
