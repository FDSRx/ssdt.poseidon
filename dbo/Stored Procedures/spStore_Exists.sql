﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/18/2015
-- Description:	Determines if a Store object exists.
-- SAMPLE CALL:
/*

DECLARE
	@BusinessEntityID BIGINT = NULL,
	@Nabp VARCHAR(50) = '2501624',
	@Exists BIT = NULL

EXEC dbo.spStore_Exists
	@BusinessEntityID = @BusinessEntityID OUTPUT,
	@Nabp = @Nabp OUTPUT,
	@Exists = @Exists OUTPUT


SELECT @BusinessEntityID AS BusinessEntityID, @Nabp AS Nabp, @Exists AS IsFound

*/

-- SELECT * FROM dbo.Store
-- =============================================
CREATE PROCEDURE [dbo].[spStore_Exists]
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@Nabp VARCHAR(50) = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-------------------------------------------------------------------------------------------------------------
	-- Instance variables
	-------------------------------------------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ErrorMessage VARCHAR(4000)
	
	-------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------------
	SET @Exists = 0;
	
	-------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------------------------
	-- No declarations required.
		
	
	-------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------------------------
	IF @BusinessEntityID IS NOT NULL
	BEGIN
		SET @Nabp = NULL;
	END
	
	-------------------------------------------------------------------------------------------------------------
	-- Locate store.
	-------------------------------------------------------------------------------------------------------------
	SELECT TOP(1)
		@BusinessEntityID = BusinessEntityID,
		@Nabp = Nabp,
		@Exists = 1
	--SELECT *
	FROM dbo.Store
	WHERE (@BusinessEntityID IS NOT NULL AND BusinessEntityID = BusinessEntityID)
		OR (@BusinessEntityID IS NULL AND Nabp = @Nabp)

	IF @@ROWCOUNT = 0
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @Nabp = NULL;
		SET @Exists = 0;
	END

	
	
END
