﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/30/2015
-- Description:	Retrieves the specified address.
/*

DECLARE 
	@AddressID BIGINT = NULL,
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@StateProvinceID INT = NULL,
	@PostalCode VARCHAR(25) = NULL,
	@SpatialLocation GEOGRAPHY = NULL,
	@Latitude DECIMAL(9,6) = NULL,
	@Longitude DECIMAL(9,6) = NULL

EXEC dbo.spAddress_Get
	@AddressID = @AddressID OUTPUT,
	@AddressLine1 = @AddressLine1 OUTPUT,
	@AddressLine2 = @AddressLine2 OUTPUT,
	@City = @City OUTPUT,
	@State = @State OUTPUT,
	@StateProvinceID = @StateProvinceID OUTPUT,
	@PostalCode = @PostalCode OUTPUT,
	@SpatialLocation = @SpatialLocation OUTPUT,
	@Latitude = @Latitude OUTPUT,
	@Longitude = @Longitude OUTPUT

SELECT @AddressID AS AddressID, @AddressLine1 AS AddressLine1, @AddressLine2 AS AddressLine2,
	@City AS City, @State AS State, @StateProvinceID AS StateProvinceID, @PostalCode AS PostalCode,
	@SpatialLocation AS SpatialLocation, @Latitude AS Latitude, @Longitude AS Longitude

*/

-- SELECT TOP 10 * FROM dbo.Address ORDER BY AddressID DESC
-- SELECT * FROM dbo.Address WHERE AddressID = 658353
-- SELECT * FROM dbo.Address WHERE AddressLine1 IS NULL AND StateProvinceID IS NULL
-- =============================================
CREATE PROCEDURE [dbo].[spAddress_Get]
	@AddressID BIGINT = NULL OUTPUT,
	@AddressLine1 VARCHAR(100) = NULL OUTPUT,
	@AddressLine2 VARCHAR(100) = NULL OUTPUT,
	@City VARCHAR(50) = NULL OUTPUT,
	@State VARCHAR(10) = NULL OUTPUT,
	@StateProvinceID INT = NULL OUTPUT,
	@PostalCode VARCHAR(25) = NULL OUTPUT,
	@SpatialLocation GEOGRAPHY = NULL OUTPUT,
	@Latitude DECIMAL(9,6) = NULL OUTPUT,
	@Longitude DECIMAL(9,6) = NULL OUTPUT
	
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------	
	
	------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------
		
	------------------------------------------------------------------------------------
	-- Perform necessary lookups where applicable.
	------------------------------------------------------------------------------------
	SET @StateProvinceID = 
		CASE
			WHEN @StateProvinceID IS NOT NULL THEN @StateProvinceID
			ELSE (SELECT StateProvinceID FROM StateProvince WHERE StateProvinceCode = @State)
		END;
	
	------------------------------------------------------------------------------------
	-- Retrieve the address object.
	------------------------------------------------------------------------------------
	SELECT TOP 1 
		@AddressID = AddressID,
		@AddressLine1 = AddressLine1,
		@AddressLine2 = AddressLine2,
		@City = City,
		@State = (SELECT StateProvinceCode FROM dbo.StateProvince WHERE StateProvinceID = addr.StateProvinceID),
		@StateProvinceID = StateProvinceID,
		@PostalCode = PostalCode,
		@SpatialLocation = SpatialLocation,
		@Latitude = Latitude,
		@Longitude = Longitude
	FROM dbo.Address addr (NOLOCK)
	WHERE AddressID = @AddressID
		OR ( @AddressID IS NULL AND 
			( ( @AddressLine1 IS NULL AND AddressLine1 IS NULL ) OR AddressLine1 = @AddressLine1 )
			AND ( ( @AddressLine2 IS NULL AND AddressLine2 IS NULL )  OR AddressLine2 = @AddressLine2 )
			AND ( ( @City IS NULL AND City IS NULL )  OR City = @City )
			AND ( ( @StateProvinceID IS NULL AND StateProvinceID IS NULL )  OR StateProvinceID = @StateProvinceID )
			AND ( ( @PostalCode IS NULL AND PostalCode IS NULL )  OR PostalCode = @PostalCode )
		)
	
	-- Void any provided data if no results were returned.
	IF @@ROWCOUNT = 0
	BEGIN
		SET @AddressID = NULL;
		SET @AddressLine1 = NULL;
		SET @AddressLine2 = NULL;
		SET @City = NULL;
		SET @State = NULL;
		SET @StateProvinceID = NULL;
		SET @PostalCode = NULL;
		SET @SpatialLocation = NULL;
		SET @Latitude = NULL;
		SET @Longitude = NULL;
	END		
			
			
END
