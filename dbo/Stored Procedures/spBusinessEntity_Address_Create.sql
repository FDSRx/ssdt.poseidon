﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/25/2013
-- Description:	Create new BusinessEntityAddress object.
-- Change Log:
-- 2/23/2016 - dhughes - Modified the procedure to include all Address object properties so that an identifier is not required if
--			not known.

-- SAMPLE CALL:
/*

DECLARE
	@BusinessEntityID BIGINT = 10684,
	@AddressTypeID INT = NULL,
	@AddressTypeCode VARCHAR(50) = 'SHIP',
	@AddressID BIGINT = NULL,
	@AddressLine1 VARCHAR(100) = '8850 Stanford Blvd.',
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(75) = 'Columbia',
	@State VARCHAR(10) = 'MD',
	@PostalCode VARCHAR(25) = '21044',
	@SpatialLocation GEOGRAPHY = NULL,
	@Latitude DECIMAL(9,6) = NULL,
	@Longitude DECIMAL(9,6) = NULL,
	@CreatedBy VARCHAR(256) = 'dhughes',
	@BusinessEntityAddressID BIGINT = NULL

EXEC dbo.spBusinessEntity_Address_Create
	@BusinessEntityID = @BusinessEntityID,
	@AddressTypeID = @AddressTypeID,
	@AddressTypeCode = @AddressTypeCode,
	@AddressID = @AddressID OUTPUT,
	@AddressLine1 = @AddressLine1,
	@AddressLine2 = @AddressLine2,
	@City = @City,
	@State = @State,
	@PostalCode = @PostalCode,
	@SpatialLocation = @SpatialLocation,
	@Latitude = @Latitude,
	@Longitude = @Longitude,
	@CreatedBy = @CreatedBy,
	@BusinessEntityAddressID = @BusinessEntityAddressID OUTPUT

SELECT @BusinessEntityAddressID AS BusinessEntityAddressID, @AddressID AS AddressID

*/

-- SELECT * FROM dbo.BusinessEntityAddress WHERE BusinessEntityID = 10684
-- SELECT * FROM dbo.vwBusinessEntityAddress
-- SELECT * FROM dbo.Address
-- SELECT * FROM dbo.AddressType
-- SELECT * FROM dbo.Store WHERE Name LIKE '%Tommy''s Corner%'
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Address_Create]
	@BusinessEntityID BIGINT = NULL,
	@AddressTypeID INT = NULL,
	@AddressTypeCode VARCHAR(50) = NULL,
	@AddressID BIGINT = NULL OUTPUT,
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(75) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(25) = NULL,
	@SpatialLocation GEOGRAPHY = NULL,
	@Latitude DECIMAL(9,6) = NULL,
	@Longitude DECIMAL(9,6) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityAddressID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-----------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------
	SET @BusinessEntityAddressID = NULL;

	-----------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------------------
	DECLARE
		@CreateObject BIT = 0

	-----------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------
	SET @AddressTypeID = COALESCE(dbo.fnGetAddressTypeID(@AddressTypeID), dbo.fnGetAddressTypeID(@AddressTypeCode), dbo.fnGetAddressTypeID('PRIM'));

	-----------------------------------------------------------------------------------------------------------------
	-- Determine if the primary object is able to be created and/or if the Address object needs to be
	-- created or discovered.
	-----------------------------------------------------------------------------------------------------------------
	-- If a BusinessEntity object and Address object was supplied that does not already exist, then allow the BusinessEntityAddress object
	-- to be created.
	IF @BusinessEntityID IS NOT NULL 
		AND @AddressTypeID IS NOT NULL 
		AND @AddressID IS NOT NULL
		AND NOT EXISTS (SELECT TOP 1 * FROM BusinessEntityAddress WHERE BusinessEntityID = @BusinessEntityID AND AddressTypeID = @AddressTypeID)
	BEGIN

		SET @CreateObject = 1;

	END
	-- If a BusinessEntity object and Address object properties were provided, then determine if the Address needs to be created and then
	-- create the BusinessEntityAddress object.
	ELSE IF @BusinessEntityID IS NOT NULL 
		AND @AddressID IS NULL
	BEGIN

		SET @CreateObject = 1;

		-- Create new Address object (if applicable).
		EXEC dbo.spAddress_Create
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			@SpatialLocation = @SpatialLocation,
			@Latitude = @Latitude,
			@Longitude = @Longitude,
			@CreatedBy = @CreatedBy,
			@AddressID = @AddressID OUTPUT
	END

	-----------------------------------------------------------------------------------------------------------------
	-- Create BusinessEntityAddress object.
	-----------------------------------------------------------------------------------------------------------------
	IF @CreateObject = 1
	BEGIN
		INSERT INTO dbo.BusinessEntityAddress (
			BusinessEntityID,
			AddressTypeID,
			AddressID,
			CreatedBy
		)
		SELECT
			@BusinessEntityID,
			@AddressTypeID,
			@AddressID,
			@CreatedBy
		
		-- Retrieve unique record identifier.
		SET @BusinessEntityAddressID = SCOPE_IDENTITY();
	END
	
END
