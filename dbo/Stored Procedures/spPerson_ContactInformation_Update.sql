﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/9/2014
-- Description:	Updates a person demographics (i.e. personal information, address, phone records, etc.).
-- SAMPLE CALL:
/*

EXEC dbo.spPerson_ContactInformation_Update
	@PersonID = 135624,
	@FirstName = 'Daniel',
	@LastName = 'Hughes',
	@PhoneNumber_Mobile = '4109379354'

*/

-- SELECT * FROM dbo.Person WHERE BusinessEntityID = 135624
-- SELECT * FROM dbo.vwPerson WHERE PersonID = 135624
-- =============================================
CREATE PROCEDURE [dbo].[spPerson_ContactInformation_Update]
	--Person
	@PersonID INT = NULL OUTPUT,
	@PersonTypeID INT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75),
	@LastName VARCHAR(75),
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderID INT = NULL,
	@LanguageID INT = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	--Caller
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	-- Error
	@ErrorLogID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	-------------------------------------------------------------------------------------------
	-- Instance variables
	-------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID) + ';' +
		'@PersonTypeID=' + dbo.fnToStringOrEmpty(@PersonTypeID) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName) + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName) + ';' +
		'@MiddleName=' + dbo.fnToStringOrEmpty(@MiddleName) + ';' +
		'@Suffix=' + dbo.fnToStringOrEmpty(@Suffix) + ';' +
		'@BirthDate=' + dbo.fnToStringOrEmpty(@BirthDate) + ';' +
		'@GenderID=' + dbo.fnToStringOrEmpty(@GenderID) + ';' +
		'@LanguageID=' + dbo.fnToStringOrEmpty(@LanguageID) + ';' +
		'@AddressLine1=' + dbo.fnToStringOrEmpty(@AddressLine1) + ';' +
		'@AddressLine2=' + dbo.fnToStringOrEmpty(@AddressLine2) + ';' +
		'@City=' + dbo.fnToStringOrEmpty(@City) + ';' +
		'@State=' + dbo.fnToStringOrEmpty(@State) + ';' +
		'@PostalCode=' + dbo.fnToStringOrEmpty(@PostalCode) + ';' +
		'@PhoneNumber_Home=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home) + ';' +
		'@PhoneNumber_Home_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsCallAllowed) + ';' +
		'@PhoneNumber_Home_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsTextAllowed) + ';' +
		'@PhoneNumber_Home_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsPreferred) + ';' +
		'@PhoneNumber_Mobile=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile) + ';' +
		'@PhoneNumber_Mobile_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsCallAllowed) + ';' +
		'@PhoneNumber_Mobile_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsTextAllowed) + ';' +
		'@PhoneNumber_Mobile_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsPreferred) + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary) + ';' +
		'@EmailAddress_Primary_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsEmailAllowed) + ';' +
		'@EmailAddress_Primary_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsPreferred) + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate) + ';' +
		'@EmailAddress_Alternate_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsEmailAllowed) + ';' +
		'@EmailAddress_Alternate_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsPreferred) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' 
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	
	-------------------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------------------
	DECLARE
		@AddressID INT,
		@ErrorMessage VARCHAR(4000)


	BEGIN TRY	
	-------------------------------------------------------------------------------------------
	-- Begin data transaction.
	-------------------------------------------------------------------------------------------	
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	
	
		-------------------------------------------------------------------------------------------
		-- Update the person demographic data.
		-------------------------------------------------------------------------------------------
		EXEC dbo.spPerson_Update
			@PersonID = @PersonID,
			@PersonTypeID = @PersonTypeID,
			@Title = @Title,
			@FirstName = @FirstName,
			@LastName = @LastName,
			@MiddleName = @MiddleName,
			@Suffix = @Suffix,
			@BirthDate = @BirthDate,
			@GenderID = @GenderID,
			@LanguageID = @LanguageID,
			@ModifiedBy = @ModifiedBy
		
		-------------------------------------------------------------------------------------------
		-- Create a new address or get a known address
		-------------------------------------------------------------------------------------------
		EXEC dbo.spAddress_Create
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			@AddressID = @AddressID OUTPUT
		
		-- Get the address ID for home address
		DECLARE @AddressTypeID_Home INT = dbo.fnGetAddressTypeID('HME')
		
		-------------------------------------------------------------------------------------------
		-- Merge the person's home address.
		-------------------------------------------------------------------------------------------
		EXEC spBusinessEntity_Address_Merge
			@BusinessEntityID = @PersonID,
			@AddressTypeID = @AddressTypeID_Home,
			@AddressID = @AddressID,
			@CreatedBy = @CreatedBy,
			@ModifiedBy = @ModifiedBy
		
		-------------------------------------------------------------------------------------------
		-- Update person email.
		-------------------------------------------------------------------------------------------
		DECLARE 
			@EmailAddressTypeID_Primary INT = dbo.fnGetEmailAddressTypeID('PRIM'),
			@EmailAddressTypeID_Alternate INT = dbo.fnGetEmailAddressTypeID('ALT')
		
		-- Create primary email address
		EXEC spBusinessEntity_Email_Merge
			@BusinessEntityID = @PersonID,
			@EmailAddressTypeID = @EmailAddressTypeID_Primary,
			@EmailAddress = @EmailAddress_Primary,
			@IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
			@IsPreferred = @EmailAddress_Primary_IsPreferred,
			@CreatedBy = @CreatedBy,
			@ModifiedBy = @ModifiedBy


		-- Create alternate email address
		EXEC spBusinessEntity_Email_Merge
			@BusinessEntityID = @PersonID,
			@EmailAddressTypeID = @EmailAddressTypeID_Alternate,
			@EmailAddress = @EmailAddress_Alternate,
			@IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
			@IsPreferred = @EmailAddress_Alternate_IsPreferred,
			@CreatedBy = @CreatedBy,
			@ModifiedBy = @ModifiedBy

		-------------------------------------------------------------------------------------------
		-- Create new person phone number
		-------------------------------------------------------------------------------------------
		DECLARE
			@PhoneNumberTypeID_Home INT = dbo.fnGetPhoneNumberTypeID('HME'),
			@PhoneNumberTypeID_Mobile INT = dbo.fnGetPhoneNumberTypeID('MBL')
			
		-- Create new home phone
		EXEC spBusinessEntity_Phone_Merge
			@BusinessEntityID = @PersonID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Home,
			@PhoneNumber = @PhoneNumber_Home,
			@IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
			@IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
			@IsPreferred = @PhoneNumber_Home_IsPreferred,
			@CreatedBy = @CreatedBy,
			@ModifiedBy = @ModifiedBy
		
		-- Create new mobile phone
		EXEC spBusinessEntity_Phone_Merge
			@BusinessEntityID = @PersonID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Mobile,
			@PhoneNumber = @PhoneNumber_Mobile,
			@IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
			@IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
			@IsPreferred = @PhoneNumber_Mobile_IsPreferred,
			@CreatedBy = @CreatedBy,
			@ModifiedBy = @ModifiedBy
			

	-------------------------------------------------------------------------------------------
	-- End data transaction.
	-------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError] @ErrorLogID OUTPUT;
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);
		
	END CATCH
	
				
END
