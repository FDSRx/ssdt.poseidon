﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/14/2014
-- Description:	Creates a BusinessEntityService object.
/*

DECLARE
	@BusinessEntityID BIGINT,
	@ServiceID INT,
	@AccountKey VARCHAR(256) = NULL,
	@ConnectionStatusID INT = NULL,
	@GoLiveDate DATETIME = NULL,
	@DateTermed DATETIME = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityServiceID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL

EXEC dbo.spBusinessEntityService_Create
	@BusinessEntityID = @BusinessEntityID,
	@ServiceID = @ServiceID,
	@AccountKey = @AccountKey,
	@ConnectionStatusID = @ConnectionStatusID,
	@GoLiveDate = @GoLiveDate,
	@DateTermed = @DateTermed,
	@CreatedBy = @CreatedBy,
	@BusinessEntityServiceID = @BusinessEntityServiceID,
	@Debug = @Debug
		
*/

-- SELECT * FROM dbo.BusinessEntityService WHERE BusinessEntityID = 10684

-- SELECT * FROM dbo.InformationLog
-- SELECT * FROM dbo.ErrorLog
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityService_Create]
	@BusinessEntityID BIGINT,
	@ServiceID INT,
	@AccountKey VARCHAR(256) = NULL,
	@ConnectionStatusID INT = NULL,
	@GoLiveDate DATETIME = NULL,
	@DateTermed DATETIME = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityServiceID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	--------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------
	SET @BusinessEntityServiceID = NULL;
	SET @Debug = ISNULL(@Debug, 0);
	
	--------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------
	DECLARE
		@HashKey VARCHAR(256)
	
	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------
	-- Create hash key.
	SET @HashKey = dbo.fnCreateRecordHashKey(
		dbo.fnToHashableString(@BusinessEntityID) +
		dbo.fnToHashableString(@ServiceID) +
		dbo.fnToHashableString(@AccountKey) +
		dbo.fnToHashableString(@ConnectionStatusID) +
		dbo.fnToHashableString(@GoLiveDate) +
		dbo.fnToHashableString(@DateTermed)
	);
	
	--------------------------------------------------------------------
	-- Argument Null Exceptions
	--------------------------------------------------------------------
	--------------------------------------------------------------------
	-- Business Entity Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null business entity ID
	-- was passed
	--------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@BusinessEntityID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;	

	END	

	
	--------------------------------------------------------------------
	-- Service Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null service ID
	-- was passed
	--------------------------------------------------------------------
	IF ISNULL(@ServiceID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@ServiceID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@BusinessEntityID AS BusinessEntityID, @ServiceID AS ServiceID, @AccountKey AS AccountKey, 
			@ConnectionStatusID AS ConnectionStatusID, @CreatedBy AS CreatedBy
	END
	
	--------------------------------------------------------------------
	-- Create new business service umbrella record.
	--------------------------------------------------------------------
	INSERT INTO dbo.BusinessEntityService (
		BusinessEntityID,
		ServiceID,
		AccountKey,
		ConnectionStatusID,
		GoLiveDate,
		DateTermed,
		HashKey,
		CreatedBy
	)
	SELECT
		@BusinessEntityID AS BusinessEntityID,
		@ServiceID AS ServiceID,
		@AccountKey AS AccountKey,
		@ConnectionStatusID AS ConnectionStatusID,
		@GoLiveDate AS GoLiveDate,
		@DateTermed AS DateTermed,
		@HashKey AS HashKey,
		@CreatedBy
		
	
	-- Return record identity.
	SET @BusinessEntityServiceID = SCOPE_IDENTITY();
	
	
	
	
	
	
	
	
	
	
	
	
END
