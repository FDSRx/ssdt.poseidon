﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/3/2015
-- Description:	Deletes a BusinessEntityService object.
-- SAMPLE CALL:
/*
	EXEC spBusinessEntityService_Delete
		@BusinessEntityID = 10684,
		@ServiceID = 5,
		@ModifiedBy = 'dhughes'
*/

-- SELECT * FROM dbo.BusinessEntityService WHERE BusinessEntityID = 10684
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityService_Delete]
	@BusinessEntityServiceID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@ServiceID INT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID)
			
	--------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------------------------------------------------------
	-- No sanitation required.
	
	--------------------------------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------------------------------
	-- No declarations required.	
	
	--------------------------------------------------------------------------------------------------------------------
	-- Argument Null Exceptions
	--------------------------------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------------------------------
	-- Business Entity Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null business entity ID
	-- was passed
	--------------------------------------------------------------------------------------------------------------------
	IF @BusinessEntityServiceID IS NULL AND ISNULL(@BusinessEntityID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@BusinessEntityID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;	
	END	

	
	--------------------------------------------------------------------------------------------------------------------
	-- Service Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null service ID
	-- was passed
	--------------------------------------------------------------------------------------------------------------------
	IF @BusinessEntityServiceID IS NULL AND ISNULL(@ServiceID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@ServiceID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END
	
	-- Debug
	--SELECT @BusinessEntityServiceID AS BusinessEntityServiceID, @BusinessEntityID AS BusinessEntityID, @ServiceID AS ServiceID

	--------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	--------------------------------------------------------------------------------------------------------------------
	IF @BusinessEntityServiceID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @ServiceID = NULL;
	END

	--------------------------------------------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	--------------------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy
			
	--------------------------------------------------------------------------------------------------------------------
	-- Delete object.
	--------------------------------------------------------------------------------------------------------------------
	DELETE obj
	FROM dbo.BusinessEntityService obj
	WHERE obj.BusinessEntityServiceID = @BusinessEntityServiceID
		OR (
			obj.BusinessEntityID = @BusinessEntityID
			AND obj.ServiceID = @ServiceID
		)
	
	
	
	
	
	
	
	
	
	
	
	
END
