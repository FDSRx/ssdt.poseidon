﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/25/2013
-- Description:	Create new business entity email address
-- SAMPLE CALL:
/*
	EXEC dbo.spBusinessEntity_Email_Create
		@BusinessEntityID = 10684,
		@EmailAddressTypeID = 2,
		@EmailAddress = 'dhughes@fdsrx.com',
		@CreatedBy = 'dhughes'
*/

-- SELECT * FROM dbo.BusinessEntityEmailAddress
-- SELECT * FROM dbo.BusinessEntityEmailAddress_History ORDER BY 1 DESC
-- SELECT * FROM dbo.EmailAddressType
-- SELECT * FROM dbo.BusinessEntityEmailAddress WHERE BusinessEntityID = 10684
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Email_Create]
	@BusinessEntityID INT = NULL,
	@EmailAddressTypeID INT = NULL,
	@EmailAddress VARCHAR(255) = NULL,
	@IsEmailAllowed BIT = 0,
	@IsPreferred BIT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityEmailAddressID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--------------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------------
	SET @BusinessEntityEmailAddressID = NULL;
	SET @IsEmailAllowed = ISNULL(@IsEmailAllowed, 0);
	SET @IsPreferred = ISNULL(@IsPreferred, 0);
	
	--------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------
	DECLARE @HashKey VARCHAR(256)

	--------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------	
	-- Create hash key.
	SET @HashKey = dbo.fnCreateRecordHashKey( 
		dbo.fnToHashableString(@BusinessEntityID) + 
		dbo.fnToHashableString(@EmailAddressTypeID) +
		dbo.fnToHashableString(@EmailAddress) +
		dbo.fnToHashableString(@IsEmailAllowed) +
		dbo.fnToHashableString(@IsPreferred)
	);

	--------------------------------------------------------------------------
	-- Create record.
	--------------------------------------------------------------------------
    -- Check to see if the email address does not already exist and ensure it is a valid email format.
	IF @BusinessEntityID IS NOT NULL 
		AND @EmailAddressTypeID IS NOT NULL 
		AND @EmailAddress IS NOT NULL
		AND (SELECT COUNT(*) FROM BusinessEntityEmailAddress WHERE BusinessEntityID = @BusinessEntityID AND EmailAddressTypeID = @EmailAddressTypeID) = 0
		AND dbo.fnIsEmailValid(@EmailAddress) = 1
	BEGIN
		INSERT INTO BusinessEntityEmailAddress (
			BusinessEntityID,
			EmailAddressTypeID,
			EmailAddress,
			IsEmailAllowed,
			IsPreferred,
			HashKey,
			CreatedBy
		)
		SELECT
			@BusinessEntityID,
			@EmailAddressTypeID,
			@EmailAddress,
			@IsEmailAllowed,
			@IsPreferred,
			@HashKey,
			@CreatedBy
		
		-- Retrieve record identifier.
		SET @BusinessEntityEmailAddressID = SCOPE_IDENTITY();
	END
END
