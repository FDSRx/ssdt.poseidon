﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/21/2014
-- Description:	Indicates if an image exists within the image repository.
/*
DECLARE 
	@ImageID BIGINT,
	@Exists BIT
	
EXEC spImage_Exists
	@ApplicationID = 2,
	@BusinessID = '10684',
	@PersonID = '135590',
	@KeyName = 'ProfilePictureTiny',
	@ImageID = @ImageID OUTPUT,
	@Exists = @Exists OUTPUT

SELECT @ImageID AS ImageID, @Exists AS IsFound
*/

-- SELECT * FROM dbo.Images
-- SELECT * FROM dbo.vwPerson WHERE LastName LIKE '%Simmons%' AND FirstName LIKE '%Steve%'
-- =============================================
CREATE PROCEDURE [dbo].[spImage_Exists]
	@ImageID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@BusinessID INT = NULL,
	@PersonID BIGINT = NULL,
	@KeyName VARCHAR(256) = NULL,
	@LanguageID INT = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------
	SET @Exists = 0;
	
		
	------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
	
	------------------------------------------------------------
	-- Set variables
	------------------------------------------------------------
	SET @LanguageID = ISNULL(@LanguageID, dbo.fnGetLanguageID('en')); -- default to english.
	
	------------------------------------------------------------
	-- Argument null exception
	------------------------------------------------------------
	IF ISNULL(@KeyName, '') = '' AND @ImageID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object references (@KeyName and @ImageID) are not set to an instance of an object. ' + 
			'One of the objects cannot be null or empty.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	------------------------------------------------------------
	-- Retrieve ImageID
	-- <Summary>
	-- Attempts to retrieve an image ID for image validation.
	-- If an image ID is found, then the image exists within
	-- the repository.
	-- </Summary>
	------------------------------------------------------------
	IF @ImageID IS NOT NULL
	BEGIN
	
		SET @ImageID = (
			SELECT TOP 1
				ImageID
			FROM dbo.Images
			WHERE ImageID = @ImageID
		);
			
	END
	ELSE
	BEGIN
	
		SELECT TOP 1
			@ImageID = ImageID
		FROM dbo.Images
		WHERE ( @ApplicationID IS NULL OR ApplicationID = @ApplicationID )
			AND ( @BusinessID IS NULL OR BusinessID = @BusinessID )
			AND ( @PersonID IS NULL OR PersonID = @PersonID )
			AND KeyName = @KeyName
			AND ( @LanguageID IS NULL OR LanguageID = @LanguageID )
	
	END
	
	-- Set existence flag.
	SET @Exists = CASE WHEN @ImageID IS NOT NULL THEN 1 ELSE 0 END;
	
	
	

	
	
	
END

