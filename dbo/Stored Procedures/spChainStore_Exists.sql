﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/28/2014
-- Description:	Indicates whether a chain store already exists in the system.
-- SAMPLE CALL: 
/*
DECLARE @ChainStoreID INT
EXEC spChainStore_Exists 
	@StoreID = 9737, 
	@ChainStoreID = @ChainStoreID OUTPUT
SELECT @ChainStoreID AS ChainStoreID
*/
-- =============================================
CREATE PROCEDURE spChainStore_Exists
	@ChainID INT = NULL,
	@StoreID INT,
	@ChainStoreID INT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------
	SET @ChainStoreID = NULL;
	SET @Exists = 0;
	

	-----------------------------------------------------------
	-- Determine if a chain store exists.
	-----------------------------------------------------------
	SET @ChainStoreID = (
		SELECT TOP 1
			ChainStoreID
		FROM dbo.ChainStore
		WHERE StoreID = @StoreID
			--AND ChainID = @ChainID
	);
	
	-- Set the exists flag.
	SET @Exists = CASE WHEN @ChainStoreID IS NOT NULL THEN 1 ELSE 0 END;
	
	
		
END
