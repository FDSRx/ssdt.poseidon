﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Creates a new note tag item.
-- SAMPLE CALL:
/*
DECLARE @NoteTagID BIGINT

EXEC dbo.spNoteTag_Merge
	@NoteID = 1,
	@TagID = 1,
	@NoteTagID = @NoteTagID OUTPUT

SELECT @NoteTagID AS NoteTagID

*/
-- SELECT * FROM dbo.NoteTag
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Tag
-- =============================================
CREATE PROCEDURE dbo.spNoteTag_Merge
	@NoteID BIGINT,
	@TagID BIGINT = NULL,
	@TagName VARCHAR(500) = NULL,
	@NoteTagID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @NoteTagID = NULL;

	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
		
	----------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	----------------------------------------------------------------------
	-- A NoteTag object was required to be provided.
	IF ISNULL(@NoteID, 0) = 0
	BEGIN
		SET @ErrorMessage = 'Unable to create NoteTag object.  Object reference (@NoteID) is not set to an instance of an object. ' +
			'The @NoteID parameter cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	
	--------------------------------------------------------------
	-- Determine NoteTag object existence.
	-- <Summary>
	-- Determine if the NoteTag object exists.  If the NoteTag object already exists,
	-- then exit the merge process and return the found NoteTagID.
	-- <Summary>
	-- <Remarks>
	-- NoteTags are always adds.  They are NEVER updates.
	-- </Summary>
	--------------------------------------------------------------
	EXEC dbo.spNoteTag_Exists
		@NoteID = @NoteID,
		@TagID = @TagID,
		@TagName = @TagName,
		@NoteTagID = @NoteTagID OUTPUT
	
	-- debug
	--SELECT @NoteTagID AS NoteTagID
	
	-- If a NoteTag object was found, then exit the code.
	IF @NoteTagID IS NOT NULL
	BEGIN
		RETURN;
	END	
	
	--------------------------------------------------------------
	-- Retrieve Tag object.
	-- <Summary>
	-- Determines the Tag object existence and retrieves the Tag object ID.
	-- </Summary>
	--------------------------------------------------------------
	EXEC dbo.spTag_Exists
		@TagID = @TagID OUTPUT,
		@Name = @TagName
	
	-- debug
	--SELECT @NoteID AS NoteID, @TagID AS TagID
		
	--------------------------------------------------------------
	-- Create new NoteTag object.
	-- <Summary>
	-- Creates a new NoteTag entry and returns its unique row identifier.
	-- </Summary>
	--------------------------------------------------------------
	EXEC dbo.spNoteTag_Create
		@NoteID = @NoteID,
		@TagID = @TagID,
		@NoteTagID = @NoteTagID OUTPUT
	
END
