﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/25/2013
-- Description:	Creates a new Business object.
-- Change log:
-- 2/22/2016 - dhughes - Added the "SourceSystemKey" property to the procedure.

-- SAMPLE CALL:
/*

DECLARE
	@BusinessEntityTypeID INT,
	@BusinessEntityTypeCode VARCHAR(50),
	@BusinessTypeID INT = NULL,
	@BusinessTypeCode VARCHAR(50) = NULL,
	@Name VARCHAR(256) = NULL,
	@SourceSystemKey VARCHAR(256) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@Debug BIT = 1

EXEC [dbo].[spBusiness_Create]
	@BusinessEntityTypeID = @BusinessEntityTypeID,
	@BusinessEntityTypeCode = @BusinessEntityTypeCode,
	@BusinessTypeID = @BusinessTypeID,
	@BusinessTypeCode = @BusinessTypeCode,
	@Name = @Name,
	@SourceSystemKey = @SourceSystemKey,
	@CreatedBy = @CreatedBy,
	@BusinessEntityID = @BusinessEntityID OUTPUT,
	@Debug = @Debug

*/

-- SELECT * FROM dbo.Business
-- SELECT * FROM dbo.BusinessEntityType
-- =============================================
CREATE PROCEDURE [dbo].[spBusiness_Create]
	@BusinessEntityTypeID INT,
	@BusinessEntityTypeCode VARCHAR(50) = NULL,
	@BusinessTypeID INT = NULL,
	@BusinessTypeCode VARCHAR(50) = NULL,
	@Name VARCHAR(256) = NULL,
	@SourceSystemKey VARCHAR(256) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------------------	
	SET @BusinessEntityID = NULL;
	SET @BusinessTypeID = ISNULL(@BusinessTypeID, dbo.fnGetBusinessTypeID('STRE'));
	SET @Debug = ISNULL(@Debug, 0);
	
	------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------------------	
	DECLARE 
		@BusinessNumber VARCHAR(25),
		@LoopCounter INT = 0,
		@LoopTerminator INT = 10,
		@ErrorMessage VARCHAR(4000)
		
		
	------------------------------------------------------------------------------------------------------------------------
	-- Generate new business number.
	------------------------------------------------------------------------------------------------------------------------
	SET @BusinessNumber = dbo.fnGenerateBusinessNumber(RAND());
	
	WHILE EXISTS(SELECT BusinessNumber FROM dbo.Business WHERE BusinessNumber = @BusinessNumber)
		AND @LoopCounter <= @LoopTerminator
	BEGIN
		SET @BusinessNumber = dbo.fnGenerateBusinessNumber(RAND());
	END
	
	------------------------------------------------------------------------------------------------------------------------
	-- Fail process if unable to generate a new card.
	------------------------------------------------------------------------------------------------------------------------
	IF @LoopCounter = @LoopTerminator
	BEGIN
		
		SET @ErrorMessage = 'Process failed creating a new BUSINESS.'
			+ ' Unable to generate a unique business number.  Business number generator was attempted ' + CONVERT(VARCHAR, @LoopCounter) + ' time(s).';
				
		RAISERROR (@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
	END

		
	------------------------------------------------------------------------------------------------------------------------
	-- Create new BusinessEntity object.
	------------------------------------------------------------------------------------------------------------------------
	EXEC dbo.spBusinessEntity_Create
		@BusinessEntityTypeID = @BusinessEntityTypeID,
		@CreatedBy = @CreatedBy,
		@BusinessEntityID = @BusinessEntityID OUTPUT

	------------------------------------------------------------------------------------------------------------------------
	-- Create new Business object.
	------------------------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.Business (
		BusinessEntityID,
		BusinessTypeID,
		BusinessNumber,
		Name,
		SourceSystemKey,
		CreatedBy
	)
	SELECT
		@BusinessEntityID,
		@BusinessTypeID,
		@BusinessNumber,
		@Name,
		@SourceSystemKey,
		@CreatedBy
	
	

		
	
END
