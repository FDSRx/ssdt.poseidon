﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/14/2014
-- Description:	Creates a new ApplicationLog object entry.
-- SAMPLE CALL:
/*

DECLARE
	@ApplicationLogID BIGINT = NULL
	
EXEC dbo.spApplicationLog_Create
	@ApplicationID = 2,
	@LogEntryTypeID = 1,
	@SourceName = 'dbo.spApplicationLog_Create',
	@UserName = 'dhughes',
	@Message = 'Sample informational log entry.',
	@ApplicationLogID = @ApplicationLogID OUTPUT

SELECT @ApplicationLogID AS ApplicationLogID

*/

-- SELECT * FROM dbo.ApplicationLog
-- =============================================
CREATE PROCEDURE [dbo].[spApplicationLog_Create]
	@ApplicationID INT = NULL,
	@LogEntryTypeID INT = NULL,
	@MachineName VARCHAR(256) = NULL,
	@ServiceName VARCHAR(256) = NULL,
	@SourceName VARCHAR(256) = NULL,
	@EventName VARCHAR(128) = NULL,
	@UserName VARCHAR(256) = NULL,
	@StartTime DATETIME = NULL,
	@EndTime DATETIME = NULL,
	@Message VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@StackTrace VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ApplicationLogID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------
	SET @ApplicationLogID = NULL;
	SET @StartTime = ISNULL(@StartTime, GETDATE());
	SET @EndTime = ISNULL(@EndTime, GETDATE());

	---------------------------------------------------------------------------------------
	-- Set variables.
	---------------------------------------------------------------------------------------	
	SET @ApplicationID = ISNULL(dbo.fnGetApplicationID(@ApplicationID), dbo.fnGetApplicationID('UNKNOWN'));
	SET @LogEntryTypeID = ISNULL(dbo.fnGetLogEntryTypeID(@LogEntryTypeID), dbo.fnGetLogEntryTypeID('INFO'));
	
	-- Debug
	--SELECT @ApplicationID AS ApplicationID, @LogEntryTypeID AS LogEntryTypeID
	
	---------------------------------------------------------------------------------------
	-- Create new ApplicationLog object.
	---------------------------------------------------------------------------------------
	INSERT INTO ApplicationLog (
		ApplicationID,
		LogEntryTypeID,
		MachineName,
		ServiceName,
		SourceName,
		EventName,
		UserName,
		StartTime,
		EndTime,
		Message,
		Arguments,
		StackTrace,
		CreatedBy
	)
	SELECT
		@ApplicationID,
		@LogEntryTypeID,
		@MachineName,
		@ServiceName,
		@SourceName,
		@EventName,
		@UserName,
		@StartTime,
		@EndTime,
		@Message,
		@Arguments,
		@StackTrace,
		@CreatedBy
	
	-- Retrieve object unique identifier.
	SET @ApplicationLogID = SCOPE_IDENTITY();
	
	
END
