﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/6/2014
-- Description:	Creates a new CalendarItem object.
-- Change log:
-- 2/26/2016 - dhughes - Modified the procedure to accept more inputs. Modified the procedure to call the Note object create method with
--				more inputs.
-- 6/15/2016 - dhughes - Modified the procedure accept the Application and Business object inputs.

-- SAMPLE CALL:
/*

DECLARE 
	@NoteID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@ScopeID INT,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@CorrelationKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@Location VARCHAR(500) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CalendarItemID BIGINT = NULL,
	@Debug BIT = 1

EXEC dbo.spCalendarItem_Create
	@NoteID = @NoteID,
	@ApplicationID = @ApplicationID,
	@ApplicationCode = @ApplicationCode,
	@BusinessID = @BusinessID,
	@ScopeID = @ScopeID,
	@ScopeCode = @ScopeCode,
	@BusinessEntityID = @BusinessEntityID,
	@NotebookID = @NotebookID,
	@NotebookCode = @NotebookCode,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityID = @PriorityID,
	@PriorityCode = @PriorityCode,
	@Tags = @Tags,
	@CorrelationKey = @CorrelationKey,
	@AdditionalData = @AdditionalData,
	@Location = @Location,
	@OriginID = @OriginID,
	@OriginCode = @OriginCode,
	@OriginDataKey = @OriginDataKey,
	@CreatedBy = @CreatedBy,
	@CalendarItemID = @CalendarItemID OUTPUT,
	@Debug = @Debug


SELECT @CalendarItemID AS CalendarItemID

*/

-- SELECT * FROM dbo.CalendarItem
-- SELECT * FROM schedule.NoteSchedule
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Tag
-- SELECT * FROM dbo.NoteTag
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook
-- SELECT * FROM dbo.Priority
-- SELECT TOP 10 * FROM phrm.vwPatient WHERE LastName LIKE '%Simmons%'

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spCalendarItem_Create]
	@NoteID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@Businesskey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@CorrelationKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@Location VARCHAR(500) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CalendarItemID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID)  + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID)  + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode)  + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID)  + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode)  + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID)  + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID)  + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode)  + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title)  + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo)  + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID)  + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode)  + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags)  + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey)  + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData))  + ';' +
		'@Location=' + dbo.fnToStringOrEmpty(@Location)  + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID)  + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode)  + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey)  + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy)  + ';' +
		'@CalendarItemID=' + dbo.fnToStringOrEmpty(@CalendarItemID)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;


	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	
	------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------------------------------------------------------
	SET @ScopeID = ISNULL(@ScopeID, dbo.fnGetScopeID(@ScopeCode));
	SET @NotebookID = COALESCE(@NotebookID, dbo.fnGetNotebookID(@NotebookCode), dbo.fnGetNotebookID('MCIS'));	
	SET @PriorityID = ISNULL(@PriorityID, dbo.fnGetPriorityID(@PriorityCode));
	SET @OriginID = ISNULL(@OriginID, dbo.fnGetOriginID(@OriginCode));

	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
	DECLARE
		@NoteTypeID INT = dbo.fnGetNoteTypeID('CI')


	------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------		
	SET @ApplicationID = ISNULL(dbo.fnGetApplicationID(@ApplicationID), dbo.fnGetApplicationID(@ApplicationCode));
	SET @BusinessID = CASE WHEN @BusinessID IS NOT NULL THEN @BusinessID ELSE dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType) END;

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ApplicationCode AS ApplicationCode, @ApplicationID AS ApplicationID, @BusinessKey AS BusinessKey,
			@BusinessKeyType AS BusinessKeyType, @BusinessID AS BusinessID, @ScopeCode AS ScopeCode, @ScopeID AS ScopeID,
			@NotebookCode AS NotebookCode, @NotebookID AS NotebookID, @PriorityCode AS PriorityCode,
			@OriginCode AS OriginCode, @OriginID AS OriginID, @NoteTypeID AS NoteTypeID
	END
	

	------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. If the request is unable to be processed, all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	------------------------------------------------------------------------------------------------------------	
	BEGIN TRY
	
		------------------------------------------------------------------------------------------------------------
		-- Null argument validation
		-- <Summary>
		-- Validates if any of the necessary arguments were provided with
		-- data.
		-- </Summary>
		------------------------------------------------------------------------------------------------------------
		-- @Title 
		IF dbo.fnIsNullOrWhiteSpace(@Title) = 1
		BEGIN
			SET @ErrorMessage = 'Unable to create CalendarItem object. Object reference (@Title) is not set to an instance of an object. ' +
				'The @Title parameter cannot be null or empty.';
			
			RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			  
			 RETURN;
		END	
		
	------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END

	
		------------------------------------------------------------------------------------------------------------
		-- Create Note object.
		-- <Summary>
		-- If a Note object was not provided, then create a new Note object;
		-- otherwise, we will create a CalendarItem on the provided Note.
		-- <Summary>
		-- <Remarks>
		-- CalendarItems are no more than a derived object of Note.
		-- </Remarks> 
		------------------------------------------------------------------------------------------------------------
		IF @NoteID IS NULL
		BEGIN
			EXEC dbo.spNote_Create
				@ApplicationID = @ApplicationID,
				@BusinessID = @BusinessID,
				@ScopeID = @ScopeID,
				@BusinessEntityID = @BusinessEntityID,
				@NoteTypeID = @NoteTypeID,
				@NotebookID = @NotebookID,
				@PriorityID = @PriorityID,
				@Title = @Title,
				@Memo = @Memo,
				@Tags = @Tags,
				@CorrelationKey = @CorrelationKey,
				@AdditionalData = @AdditionalData,
				@OriginID = @OriginID,
				@OriginDataKey = @OriginDataKey,
				@CreatedBy = @CreatedBy,
				@NoteID = @NoteID OUTPUT
		END

		------------------------------------------------------------------------------------------------------------
		-- Create CalendarItem object.
		------------------------------------------------------------------------------------------------------------		
		INSERT INTO dbo.CalendarItem (
			NoteID,
			ApplicationID,
			BusinessID,
			BusinessEntityID,
			Location,
			CreatedBy
		)
		SELECT
			@NoteID,
			@ApplicationID,
			@BusinessID,
			@BusinessEntityID,
			@Location,
			@CreatedBy
		
		
		-- Retrieve the record identity.
		SET @CalendarItemID = @NoteID;			
				
	
	------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args
		
		SET @ErrorMessage = ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		
	END CATCH		

END
