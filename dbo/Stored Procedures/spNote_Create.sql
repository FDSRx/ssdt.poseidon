﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/13/2014
-- Description:	Creates a new note object.
-- Change log:
-- 3/4/2016 - dhughes - Modified the procedure to include additional input parameters.

-- SAMPLE CALL:
/*

DECLARE
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT,
	@ScopeCode VARCHAR(50) = 2,
	@BusinessEntityID BIGINT = 135590,
	@NoteTypeID INT = 1,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = 2,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(500) = 'Poseidon Test Note On ' + CONVERT(VARCHAR, GETDATE()),
	@Memo VARCHAR(MAX) = 'This is a test note #: ' + CONVERT(VARCHAR, RAND()),
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = 'Test,Test Note,My Note,Internal', -- A comma separated list of tags that identify a note.
	@CorrelationKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@OriginID INT = 4,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@CreatedBy VARCHAR(256) = 'Poseidon/dhughes',
	@NoteID BIGINT = NULL,
	@Debug BIT = 1

EXEC dbo.spNote_Create
	@ApplicationID = @ApplicationID,
	@ApplicationCode = @ApplicationCode,
	@BusinessID = @BusinessID,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@ScopeID = @ScopeID,
	@ScopeCode = @ScopeCode,
	@BusinessEntityID = @BusinessEntityID,
	@NoteTypeID = @NoteTypeID,
	@NoteTypeCode = @NoteTypeCode,
	@NotebookID = @NotebookID,
	@NotebookCode = @NotebookCode,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityID = @PriorityID,
	@PriorityCode = @PriorityCode,
	@Tags = @Tags,
	@CorrelationKey = @CorrelationKey,
	@AdditionalData = @AdditionalData,
	@OriginID = @OriginID,
	@OriginCode = @OriginCode,
	@OriginDataKey = @OriginDataKey,
	@CreatedBy = @CreatedBy,
	@NoteID = @NoteID OUTPUT,
	@Debug = @Debug


SELECT @NoteID AS NoteID

*/


-- SELECT * FROM dbo.Note ORDER BY 1 DESC
-- SELECT * FROM dbo.Tag
-- SELECT * FROM dbo.NoteTag
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook
-- SELECT * FROM dbo.Priority
-- SELECT * FROM dbo.Origin
-- SELECT TOP 10 * FROM phrm.vwPatient WHERE LastName LIKE '%Simmons%'

-- SELECT * FROM dbo.ErrorLog
-- SELECT * FROM dbo.InformationLog
-- =============================================
CREATE PROCEDURE [dbo].[spNote_Create]
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@CorrelationKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@NoteID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@NoteTypeID=' + dbo.fnToStringOrEmpty(@NoteTypeID) + ';' +
		'@NoteTypeCode=' + dbo.fnToStringOrEmpty(@NoteTypeCode) + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID) + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID) + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID) + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------------------------------------------------------------
	SET @NoteID = NULL;
	SET @ScopeID = ISNULL(dbo.fnGetScopeID(@ScopeID), dbo.fnGetScopeID(@ScopeCode));
	SET @NoteTypeID = COALESCE(dbo.fnGetNoteTypeID(@NoteTypeID), dbo.fnGetNoteTypeID(@NoteTypeCode), dbo.fnGetNoteTypeID('GNRL'));
	SET @NotebookID = COALESCE(dbo.fnGetNotebookID(@NotebookID), dbo.fnGetNotebookID(@NotebookCode), dbo.fnGetNotebookID('MNB'));
	SET @PriorityID = COALESCE(dbo.fnGetPriorityID(@PriorityID), dbo.fnGetPriorityID(@PriorityCode), dbo.fnGetPriorityID('NONE'));
	SET @OriginID = COALESCE(dbo.fnGetOriginID(@OriginID), dbo.fnGetOriginID(@OriginCode), dbo.fnGetOriginID('UNKN'));
	--SET @CreatedBy = ISNULL(@CreatedBy, SUSER_SNAME());
	SET @Debug = ISNULL(@Debug, 0);

	-----------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------------------------------------------------------------
	-- No local declarations required.

	-----------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------------------
	SET @ApplicationID = ISNULL(dbo.fnGetApplicationID(@ApplicationID),dbo.fnGetApplicationID(@ApplicationCode));

	SET @BusinessID = 
		CASE 
			WHEN @BusinessID IS NOT NULL THEN @BusinessID
			ELSE dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType)
		END;

	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ScopeID AS ScopeID, @NoteTypeID AS NoteTypeID, @ApplicationID AS ApplicationID, @BusinessID AS BusinessID,
			@NotebookID AS NotebookID, @PriorityID AS PriorityID, @OriginID AS OriginID

	END
			
	-----------------------------------------------------------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Inpsects the necessary arguments and determines if they are valid for processing.
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------------------
	IF dbo.fnIsNullOrWhiteSpace(@Title) = 1 AND dbo.fnIsNullOrWhiteSpace(@Memo) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to create Note object. Object references (@Title or @Memo) are not set to an instance of an object. ' +
			'The parameters, @Memo or @Title, cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
		
	-----------------------------------------------------------------------------------------------------------------------------
	-- Create Note object
	-- <Summary>
	-- Creates a new note entry and returns the note's unique identifier.
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.Note (
		ApplicationID,
		BusinessID,
		ScopeID,
		BusinessEntityID,
		NoteTypeID,
		NotebookID,
		Title,
		Memo,
		AdditionalData,
		CorrelationKey,
		PriorityID,
		OriginID,
		OriginDataKey,
		CreatedBy
	)
	SELECT
		@ApplicationID,
		@BusinessID,
		@ScopeID,
		@BusinessEntityID,
		@NoteTypeID,
		@NotebookID,
		@Title,
		@Memo,
		@AdditionalData,
		@CorrelationKey,
		@PriorityID,
		@OriginID,
		@OriginDataKey,
		@CreatedBy
	
	-- Retrieve row unique identifier.
	SET @NoteID = SCOPE_IDENTITY();

	-- Data review.
	-- SELECT * FROM dbo.Note

	-----------------------------------------------------------------------------------------------------------------------------
	-- Create NoteTag objects (if applicable)
	-- <Summary>
	-- Creates a collection of NoteTag objects that help identify the note.
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------------------	
	IF @NoteID IS NOT NULL AND dbo.fnIsNullOrWhiteSpace(@Tags) = 0
	BEGIN
		EXEC dbo.spNoteTag_MergeRange
			@NoteID = @NoteID,
			@TagNames = @Tags
	END
	
	
	
	
	
	
	
	
	
	
END

