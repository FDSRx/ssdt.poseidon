﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/20/2015
-- Description:	Creates a new BusinessEntityPreference object.
-- SAMPLE CALL;
/*

EXEC dbo.spBusinessEntityPreference_Create
	@BusinessEntityID = 135590,
	@PreferenceID = 1,
	@ValueBoolean = 1

*/

-- SELECT * FROM dbo.BusinessEntityPreference
-- =============================================
CREATE PROCEDURE spBusinessEntityPreference_Create
	@BusinessEntityID BIGINT,
	@PreferenceID INT,
	@ValueString VARCHAR(MAX) = NULL,
	@ValueBoolean BIT = NULL,
	@ValueNumeric DECIMAL(19, 8) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityPreferenceID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------
	SET @BusinessEntityPreferenceID = NULL;
	
	-----------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000) = NULL
	
	-----------------------------------------------------------------------------------------------
	-- Argument validation.
	-- <Summary>
	-- Validates critical arguments.
	-- </Summary>
	-----------------------------------------------------------------------------------------------		
	
	--@BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
			
		SET @ErrorMessage = 'Unable to create record. Object reference (@BusinessEntityID) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END	
	
	--@PreferenceID
	IF @PreferenceID IS NULL
	BEGIN
			
		SET @ErrorMessage = 'Unable to create record. Object reference (@PreferenceID) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END
	
	
	-----------------------------------------------------------------------------------------------
	-- Create BusinessEntityPreference object.
	-----------------------------------------------------------------------------------------------
	INSERT INTO dbo.BusinessEntityPreference (
		BusinessEntityID,
		PreferenceID,
		ValueString,
		ValueBoolean,
		ValueNumeric,
		CreatedBy
	)
	SELECT
		@BusinessEntityID,
		@PreferenceID,
		@ValueString,
		@ValueBoolean,
		@ValueNumeric,
		@CreatedBy
	
	-- Retrieve the record identity.
	SET @BusinessEntityPreferenceID = SCOPE_IDENTITY();
	
END
