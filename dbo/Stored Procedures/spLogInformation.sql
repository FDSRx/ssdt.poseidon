﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/10/2013
-- Description:	Logs informational messages
-- =============================================
CREATE PROCEDURE [dbo].[spLogInformation] 
	@UserName VARCHAR(255) = NULL,
	@InformationProcedure NVARCHAR(126) = NULL,
	@Message VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@InformationID INT = NULL OUTPUT,
	@ErrorLogID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------
	-- Clean input
	-----------------------------------------------------------
	SET @InformationID = NULL;
	
	-----------------------------------------------------------
	-- Record information
	-----------------------------------------------------------
	BEGIN TRY
	
		SET @UserName = ISNULL(@UserName, CONVERT(sysname, CURRENT_USER));
		
		INSERT INTO dbo.InformationLog (
			UserName,
			InformationProcedure,
			Message,
			Arguments
		)
		SELECT
			@UserName,
			@InformationProcedure,
			@Message,
			@Arguments
		
		SET @InformationID = SCOPE_IDENTITY();
	
	END TRY
	BEGIN CATCH
	
		PRINT 'An error occurred in stored procedure spLogInformation: ';
        
        EXECUTE [dbo].[spLogError] @ErrorLogID OUTPUT;
        
        RETURN -1;
	
	END CATCH
	
END








