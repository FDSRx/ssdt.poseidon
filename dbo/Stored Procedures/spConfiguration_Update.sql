﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/6/2015
-- Description:	Updates the provided configuration object.
-- SAMPLE CALL:
/*
DECLARE
	@ConfigurationID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@KeyName VARCHAR(256) = NULL,
	@KeyValue VARCHAR(MAX) = NULL,
	@DataTypeID INT= NULL,
	@DataSize INT = NULL,
	@Description VARCHAR(1000) = NULL,
	@ModifiedBy VARCHAR(256) = NULL

EXEC dbo.spConfiguration_Update
	@ConfigurationID = @ConfigurationID,
	@ApplicationID = @ApplicationID,
	@BusinessEntityID = @BusinessEntityID,
	@KeyName = @KeyName,
	@KeyValue = @KeyValue,
	@DataTypeID = @DataTypeID,
	@DataSize = @DataSize,
	@Description = @Description,
	@ModifiedBy = @ModifiedBy

SELECT @ConfigurationID AS ConfigurationID

*/

-- SELECT * FROM dbo.Configuration
-- =============================================
CREATE PROCEDURE dbo.spConfiguration_Update
	@ConfigurationID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@KeyName VARCHAR(256) = NULL,
	@KeyValue VARCHAR(MAX) = NULL,
	@DataTypeID INT= NULL,
	@DataSize INT = NULL,
	@Description VARCHAR(1000) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		

	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ConfigurationID=' + dbo.fnToStringOrEmpty(@ConfigurationID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@KeyName=' + dbo.fnToStringOrEmpty(@KeyName) + ';' +
		'@KeyValue=' + dbo.fnToStringOrEmpty(@KeyValue) + ';' +
		'@DataTypeID=' + dbo.fnToStringOrEmpty(@DataTypeID) + ';' +
		'@DataSize=' + dbo.fnToStringOrEmpty(@DataSize) + ';' +
		'@Description=' + dbo.fnToStringOrEmpty(@Description) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' 
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------------
	SET @ModifiedBy = ISNULL(@ModifiedBy, SUSER_NAME());


	-----------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------------------------
	-- No declarations.

	-----------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------------

	-- Debug
	SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Getter/Setter Methods.' AS ActionMethod,
		@ModifiedBy AS ModifiedBy, @KeyValue AS KeyValue

	-----------------------------------------------------------------------------------------------------------------------
	-- Update object.
	-----------------------------------------------------------------------------------------------------------------------
	UPDATE TOP(1) trgt
	SET
		trgt.KeyValue = ISNULL(@KeyValue, trgt.KeyValue),
		trgt.DataTypeID = ISNULL(@DataTypeID, trgt.DataTypeID),
		trgt.DataSize = ISNULL(@DataSize, trgt.DataSize),
		trgt.Description = CASE WHEN ISNULL(@Description, '') <> '' THEN @Description ELSE trgt.Description END,
		trgt.ModifiedBy = ISNULL(@ModifiedBy, ModifiedBy),
		trgt.DateModified = GETDATE()
	FROM dbo.Configuration trgt
	WHERE ConfigurationID = @ConfigurationID



END
