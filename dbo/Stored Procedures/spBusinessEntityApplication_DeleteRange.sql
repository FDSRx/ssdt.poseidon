﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/3/2015
-- Description:	Removes a single or collection of BusinessEntityApplication objects based on the specified criteria.
-- SAMPLE CALL:
/*


EXEC dbo.BusinessEntityApplication_DeleteRange
	@BusinessEntityApplication = 10684,
	@ApplicationID = 8,
	@ModifiedBy = 'dhughes'

*/

-- SELECT * FROM dbo.BusinessEntityApplication
-- SELECT * FROM dbo.BusinessEntityApplication_History
-- SElECT * FROM dbo.Application
-- SELECT * FROM dbo.BusinessEntityApplication
-- =============================================
CREATE PROCEDURE dbo.[spBusinessEntityApplication_DeleteRange]
	@BusinessEntityApplicationID VARCHAR(MAX) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@ApplicationID VARCHAR(MAX) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID)

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @BusinessEntityApplicationID, @BusinessEntityID, @ApplicationID
	IF dbo.fnIsNullOrWhiteSpace(@BusinessEntityApplicationID) = 1 
		AND @BusinessEntityID IS NULL
		AND dbo.fnISNullOrWhiteSpace(@ApplicationID) = 1 
	BEGIN
		SET @ErrorMessage = 'Unable to delete BusinessEntityApplication object. Object references ' +
			'(@BusinessEntityApplicationID or @BusinessEntityID and @ApplicationID) ' +
			'are not set to an instance of an object. ' +
			'The parameters, @BusinessEntityApplicationID or @BusinessEntityID and @ApplicationID, cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @BusinessEntityApplicationID AND @BusinessEntityID.
	IF dbo.fnIsNullOrWhiteSpace(@BusinessEntityApplicationID) = 1  AND 
		@BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to delete BusinessEntityApplication object. Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END		
	
	-- @BusinessEntityApplicationID AND @ApplicationID.
	IF dbo.fnIsNullOrWhiteSpace(@BusinessEntityApplicationID) = 1 
		AND @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to delete BusinessEntityApplication object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy
			
	-------------------------------------------------------------------------------
	-- Delete object(s).
	-- <Summary>
	-- Removes a single or collection of objects.
	-- </Summary>
	-------------------------------------------------------------------------------
	DELETE obj
	FROM dbo.BusinessEntityApplication obj
	WHERE obj.BusinessEntityApplicationID IN (SElECT Value FROM dbo.fnSplit(@BusinessEntityApplicationID, ',') WHERE ISNUMERIC(Value) = 1)
		OR (
			obj.BusinessEntityID = @BusinessEntityID
			AND obj.ApplicationID IN (SELECT Value FROM dbo.fnSplit(@ApplicationID, ',') WHERE ISNUMERIC(Value) = 1)	
		)
		
END
