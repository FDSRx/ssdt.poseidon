﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/12/2015
-- Description:	Removes a Business from the system (if applicable).
-- SAMPLE CALL:
/*

DECLARE
	@BusinessEntityID BIGINT = 1267632,
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@Debug BIT = NULL

EXEC dbo.spStore_Delete
	@BusinessEntityID = @BusinessEntityID,
	@ModifiedBy = @ModifiedBy

*/

-- SELECT * FROM dbo.vwStore WHERE BusinessEntityID = 1267632
-- SELECT * FROM dbo.Store WHERE BusinessEntityID = 1267632
-- =============================================
CREATE PROCEDURE [dbo].[spStore_Delete]
	@BusinessEntityID BIGINT = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000),
		@ErrorSeverity INT = NULL,
		@ErrorState INT = NULL,
		@ErrorNumber INT = NULL,
		@ErrorLine INT = NULL,
		@ErrorProcedure  NVARCHAR(200) = NULL

	-----------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	--Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ; 
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args	
	*/


	-----------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------
	SET @ModifiedBy = ISNULL(@ModifiedBy, SUSER_NAME());
	SET @Debug = ISNULL(@Debug, 1);


	-----------------------------------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------
	-- @BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	-----------------------------------------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	-----------------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy


	-----------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. IF the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------
	BEGIN TRY
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	

		-- Remove Address object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM BusinessEntityAddress obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove State Province object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM BusinessEntityStateProvince obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove Application object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM BusinessEntityApplication obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove Contact object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM BusinessEntityContact obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove EmailAddress object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM BusinessEntityEmailAddress obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove File object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM BusinessEntityFile obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove Phone object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM BusinessEntityPhone obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove Provider object(s).
		DELETE obj
		FROM BusinessEntityProvider obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove Service object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM BusinessEntityService obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove Service Umbrella object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM BusinessEntityServiceUmbrella obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove Customer object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM Customer obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove ChainStore object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM ChainStore obj
		WHERE obj.ChainID = @BusinessEntityID
			OR obj.StoreID = @BusinessEntityID

		-- Remove Chain object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM Chain obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove Store object(s).
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM Store obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove Business object.
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM Business obj
		WHERE obj.BusinessEntityID = @BusinessEntityID

		-- Remove BusinessEntity object.
		DELETE obj
		--SELECT TOP 1 *
		--SELECT *
		FROM BusinessEntity obj
		WHERE obj.BusinessEntityID = @BusinessEntityID


	-------------------------------------------------------------------------------
	-- End data transaction.
	-------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);
		
	END CATCH

END
