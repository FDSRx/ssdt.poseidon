﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/9/2014
-- Description:	Gets a configuration value based on the specified parameters.
-- Change log:
-- 6/13/2016 - dhughes - Modified the procedure to better handle configuration value lookups. Fixed bug where default values
--					were being returned in place of specific values.
-- SAMPLE CALL:
/*

DECLARE
	@ConfigurationID BIGINT = NULL, 
	@ApplicationID INT = 10,
	@BusinessEntityID BIGINT = 10713,
	@KeyName VARCHAR(256) = 'IsMedSyncBinVerificationQueueEnabled',
	@KeyValue VARCHAR(MAX) = NULL,
	@DataTypeID INT = NULL,
	@TypeOf VARCHAR(256) = NULL,
	@Description VARCHAR(1000) = NULL,
	@Debug BIT = 1
	
EXEC dbo.spConfiguration_Get
	@ConfigurationID = @ConfigurationID,
	@ApplicationID = @ApplicationID,
	@BusinessEntityID = @BusinessEntityID,
	@KeyName = @KeyName,
	@KeyValue = @KeyValue OUTPUT,
	@DataTypeID = @DataTypeID,
	@TypeOf = @TypeOf,
	@Description = @Description,
	@Debug = @Debug

SELECT @KeyValue AS KeyValue

*/

-- SELECT * FROM dbo.Configuration
-- SELECT * FROM dbo.DataType
-- =============================================
CREATE PROCEDURE [dbo].[spConfiguration_Get]
	@ConfigurationID BIGINT = NULL OUTPUT, 
	@ApplicationID INT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@KeyName VARCHAR(256),
	@KeyValue VARCHAR(MAX) = NULL OUTPUT,
	@DataTypeID INT = NULL OUTPUT,
	@TypeOf VARCHAR(256) = NULL OUTPUT,
	@Description VARCHAR(1000) = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ConfigurationID=' + dbo.fnToStringOrEmpty(@ConfigurationID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@KeyName=' + dbo.fnToStringOrEmpty(@KeyName) + ';' +
		'@KeyValue=' + dbo.fnToStringOrEmpty(@KeyValue) + ';' +
		'@DataTypeID=' + dbo.fnToStringOrEmpty(@DataTypeID) + ';' +
		'@TypeOf=' + dbo.fnToStringOrEmpty(@TypeOf) + ';' +
		'@Description=' + dbo.fnToStringOrEmpty(@Description) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;


	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	SET @KeyValue = NULL;
	SET @DataTypeID = NULL
	SET @Description = NULL
	SET @TypeOf = NULL;

	------------------------------------------------------------------------------------------------------------
	-- Argument null validation.
	------------------------------------------------------------------------------------------------------------
	IF @ConfigurationID IS NULL AND @KeyName IS NULL
	BEGIN
		RETURN;
	END
	
	------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------
	-- If the unique surrogate key was provided then void the unique row key and/or
	-- the provided key name.
	IF @ConfigurationID IS NOT NULL
	BEGIN
		SET @ApplicationID = NULL;
		SET @BusinessEntityID = NULL;
		SET @KeyName = NULL;
	END
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debug Mode On' AS DebuggerMode, @ProcedureName AS ProcedureName, 'Get/Set variables.' AS ActionMethod,
			@ConfigurationID AS ConfigurationID, @ApplicationID AS ApplicationID, @BusinessEntityID AS BusinessEntityID, 
			@KeyName AS KeyName
	END


	------------------------------------------------------------------------------------------------------------
	-- Locate Configuration object.
	------------------------------------------------------------------------------------------------------------
	-- Locate in Application and Business/Entity level 
	IF @ConfigurationID IS NULL
	BEGIN
		SELECT TOP 1
			@ConfigurationID = ConfigurationID
		FROM dbo.Configuration config
		WHERE ApplicationID = @ApplicationID
			AND BusinessEntityID = @BusinessEntityID
			AND config.KeyName = @KeyName
	END

	-- Locate in Application level.
	IF @ConfigurationID IS NULL
	BEGIN
		SELECT TOP 1
			@ConfigurationID = ConfigurationID
		FROM dbo.Configuration config
		WHERE ApplicationID = @ApplicationID
			AND BusinessEntityID IS NULL
			AND config.KeyName = @KeyName
	END

	-- Locate in Business level.
	IF @ConfigurationID IS NULL
	BEGIN
		SELECT TOP 1
			@ConfigurationID = ConfigurationID
		FROM dbo.Configuration config
		WHERE ApplicationID IS NULL
			AND BusinessEntityID = @BusinessEntityID
			AND config.KeyName = @KeyName
	END

	-- Locate in Global scope.
	IF @ConfigurationID IS NULL
	BEGIN
		SELECT TOP 1
			@ConfigurationID = ConfigurationID
		FROM dbo.Configuration config
		WHERE ApplicationID IS NULL
			AND BusinessEntityID IS NULL
			AND config.KeyName = @KeyName
	END

	------------------------------------------------------------------------------------------------------------
	-- Fetch object data.
	------------------------------------------------------------------------------------------------------------
	SELECT TOP 1
		@ConfigurationID = ConfigurationID,
		@ApplicationID = ApplicationID,
		@BusinessEntityID = BusinessEntityID,
		@KeyValue = config.KeyValue,
		@DataTypeID = config.DataTypeID,
		@TypeOf = typ.TypeOf,
		@Description = config.Description
	FROM dbo.Configuration config
		LEFT JOIN dbo.DataType typ
			ON config.DataTypeID = typ.DataTypeID
	WHERE config.ConfigurationID = @ConfigurationID

	
	IF @@ROWCOUNT = 0
	BEGIN
		SET @ConfigurationID = NULL;
		SET @ApplicationID = NULL;
		SET @BusinessEntityID = NULL;
		SET @KeyName = NULL;
	END
	
		
END

