﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/7/2015
-- Description:	Retrieves a Task object.

-- SAMPLE CALL:
/*

DECLARE
	@NoteID BIGINT = 59832,
	@BusinessEntityID BIGINT = NULL,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(25) = NULL,
	@ScopeName VARCHAR(50) = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(25) = NULL,
	@NoteTypeName VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(25) = NULL,
	@NotebookName VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(25) = NULL,
	@PriorityName VARCHAR(50) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(25) = NULL,
	@OriginName VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@TaskStatusID INT = NULL,
	@TaskStatusCode VARCHAR(50) = NULL

EXEC dbo.spTask_Get
	@NoteID = @NoteID,
	@BusinessEntityID = @BusinessEntityID OUTPUT,
	@ScopeID = @ScopeID OUTPUT,
	@ScopeCode = @ScopeCode OUTPUT,
	@ScopeName = @ScopeName OUTPUT,
	@NoteTypeID = @NoteTypeID OUTPUT,
	@NoteTypeCode = @NoteTypeCode OUTPUT,
	@NoteTypeName = @NoteTypeName OUTPUT,
	@NotebookID = @NotebookID OUTPUT,
	@NotebookCode = @NotebookCode OUTPUT,
	@NotebookName = @NotebookName OUTPUT,
	@Title = @Title OUTPUT,
	@Memo = @Memo OUTPUT,
	@PriorityID = @PriorityID OUTPUT,
	@PriorityCode = @PriorityCode OUTPUT,
	@PriorityName = @PriorityName OUTPUT,
	@OriginID = @OriginID OUTPUT,
	@OriginCode = @OriginCode OUTPUT,
	@OriginName = @OriginName OUTPUT,
	@OriginDataKey = @OriginDataKey OUTPUT,
	@AdditionalData = @AdditionalData OUTPUT,
	@CorrelationKey = @CorrelationKey OUTPUT,
	@TaskStatusID = @TaskStatusID OUTPUT,
	@TaskStatusCode = @TaskStatusCode OUTPUT

SELECT @NoteID AS NoteID, @BusinessEntityID AS BusinessEntityID, @ScopeID AS ScopeID, @ScopeCode AS ScopeCode, @ScopeName AS ScopeName,
	@NoteTypeID AS NoteTypeID, @NoteTypeCode AS NoteTypeCode, @NoteTypeName AS NoteTypeName, @NotebookID AS NotebookID, 
	@NotebookCode AS NotebookCode, @NotebookName AS NotebookName, @Title AS Title, @Memo AS Memo, @PriorityID AS PriorityID,
	@PriorityCode AS PriorityCode, @PriorityName AS PriorityName, @OriginID AS OriginID, @OriginCode AS OriginCode, @OriginName AS OriginName,
	@OriginDataKey AS OriginDataKey, @AdditionalData AS AdditionalData, @CorrelationKey AS CorrelationKey, @TaskStatusID AS TaskStatusID,
	@TaskStatusCode AS TaskStatusCode


*/

-- SELECT * FROM dbo.Note ORDER BY 1 DESC
-- SELECT * FROM dbo.Task ORDER BY 1 DESC
-- SELECT * FROM dbo.vwTask ORDER BY 1 DESC

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spTask_Get]
	@NoteID BIGINT,
	@ApplicationID INT = NULL OUTPUT,
	@ApplicationCode VARCHAR(50) = NULL OUTPUT,
	@BusinessID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@ScopeID INT = NULL OUTPUT,
	@ScopeCode VARCHAR(50) = NULL OUTPUT,
	@ScopeName VARCHAR(50) = NULL OUTPUT,
	@NoteTypeID INT = NULL OUTPUT,
	@NoteTypeCode VARCHAR(50) = NULL OUTPUT,
	@NoteTypeName VARCHAR(50) = NULL OUTPUT,
	@NotebookID INT = NULL OUTPUT,
	@NotebookCode VARCHAR(50) = NULL OUTPUT,
	@NotebookName VARCHAR(50) = NULL OUTPUT,
	@Title VARCHAR(MAX) = NULL OUTPUT,
	@Memo VARCHAR(MAX) = NULL OUTPUT,
	@PriorityID INT = NULL OUTPUT,
	@PriorityCode VARCHAR(50) = NULL OUTPUT,
	@PriorityName VARCHAR(50) = NULL OUTPUT,
	@OriginID INT = NULL OUTPUT,
	@OriginCode VARCHAR(50) = NULL OUTPUT,
	@OriginName VARCHAR(50) = NULL OUTPUT,
	@OriginDataKey VARCHAR(256) = NULL OUTPUT,
	@AdditionalData XML = NULL OUTPUT,
	@CorrelationKey VARCHAR(256) = NULL OUTPUT,
	@DateDue DATETIME = NULL OUTPUT,
	@AssignedByID BIGINT = NULL OUTPUT,
	@AssignedByString VARCHAR(256) = NULL OUTPUT,
	@AssignedToID BIGINT = NULL OUTPUT,
	@AssignedToString VARCHAR(256) = NULL OUTPUT,
	@CompletedByID BIGINT = NULL OUTPUT,
	@CompletedByString VARCHAR(256) = NULL OUTPUT,
	@DateCompleted DATETIME = NULL OUTPUT,
	@TaskStatusID INT = NULL OUTPUT,
	@TaskStatusCode VARCHAR(50) = NULL OUTPUT,
	@TaskStatusName VARCHAR(50) = NULL OUTPUT,
	@ParentTaskID BIGINT = NULL OUTPUT,
	@IsOutputOnly BIT = 0
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode) + ';' +
		'@NoteTypeID=' + dbo.fnToStringOrEmpty(@NoteTypeID) + ';' +
		'@NoteTypeCode=' + dbo.fnToStringOrEmpty(@NoteTypeCode) + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID) + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID) + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode) + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID) + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@DateDue=' + dbo.fnToStringOrEmpty(@DateDue) + ';' +
		'@AssignedByID=' + dbo.fnToStringOrEmpty(@AssignedByID) + ';' +
		'@AssignedByString=' + dbo.fnToStringOrEmpty(@AssignedByString) + ';' +
		'@AssignedToID=' + dbo.fnToStringOrEmpty(@AssignedToID) + ';' +
		'@AssignedToString=' + dbo.fnToStringOrEmpty(@AssignedToString) + ';' +
		'@CompletedByID=' + dbo.fnToStringOrEmpty(@CompletedByID) + ';' +
		'@CompletedByString=' + dbo.fnToStringOrEmpty(@CompletedByString) + ';' +
		'@DateCompleted=' + dbo.fnToStringOrEmpty(@DateCompleted) + ';' +
		'@TaskStatusID=' + dbo.fnToStringOrEmpty(@TaskStatusID) + ';' +
		'@TaskStatusCode=' + dbo.fnToStringOrEmpty(@TaskStatusCode) + ';' +
		'@ParentTaskID=' + dbo.fnToStringOrEmpty(@ParentTaskID) + ';' +
		'@IsOutputOnly=' + dbo.fnToStringOrEmpty(@IsOutputOnly) + ';' ;
		
	
	---------------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	---------------------------------------------------------------------------------------------------------------------------------
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	---------------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------------------------------------------------------------------
	SET @IsOutputOnly = ISNULL(@IsOutputOnly, 0);
	SET @ScopeID = NULL;
	SET @ScopeCode = NULL;
	SET @NoteTypeID = NULL;
	SET @NoteTypeCode = NULL;
	SET @NotebookID = NULL;
	SET @NotebookCode = NULL;
	SET @Title = NULL;
	SET @Memo = NULL;
	SET @PriorityID = NULL;
	SET @PriorityCode = NULL;
	SET @OriginID = NULL;
	SET @OriginCode = NULL;
	SET @OriginDataKey = NULL;
	SET @AdditionalData = NULL;
	SET @CorrelationKey = NULL;
	SET @DateDue = NULL;
	SET @AssignedByID = NULL;
	SET @AssignedByString = NULL;
	SET @AssignedToID = NULL;
	SET @AssignedToString = NULL;
	SET @CompletedByID = NULL;
	SET @CompletedByString = NULL;
	SET @DateCompleted = NULL;
	SET @TaskStatusID = NULL;
	SET @TaskStatusCode = NULL;
	SET @ParentTaskID = NULL;

	---------------------------------------------------------------------------------------------------------------------------------
	-- Assign properties
	---------------------------------------------------------------------------------------------------------------------------------
	SELECT
		@NoteID = nte.NoteID,
		@ApplicationID = nte.ApplicationID,
		@ApplicationCode = app.Code,
		@BusinessID = @BusinessID,
		@BusinessEntityID = nte.BusinessEntityID,
		@ScopeID = nte.ScopeID,
		@ScopeCode = scp.Code,
		@ScopeName = scp.Name,
		@NoteTypeID = nte.NoteTypeID,
		@NoteTypeCode = nt.Code,
		@NoteTypeName = nt.Name,
		@NotebookID = nte.NotebookID,
		@NotebookCode = nb.Code,
		@NotebookName = nb.Name,
		@Title = nte.Title,
		@Memo = nte.Memo,
		@PriorityID = nte.PriorityID,
		@PriorityCode = pri.Code,
		@PriorityName = pri.Name,
		@OriginID = nte.OriginID,
		@OriginCode = org.Code,
		@OriginName = org.Name,
		@OriginDataKey = nte.OriginDataKey,
		@AdditionalData = nte.AdditionalData,
		@CorrelationKey = nte.CorrelationKey,
		@DateDue = tsk.DateDue,
		@AssignedByID = tsk.AssignedByID,
		@AssignedByString = tsk.AssignedByString,
		@AssignedToID = tsk.AssignedToID,
		@AssignedToString = tsk.AssignedToString,
		@CompletedByID = tsk.CompletedByID,
		@CompletedByString = tsk.CompletedByString,
		@DateCompleted = tsk.DateCompleted,
		@TaskStatusID = tsk.TaskStatusID,
		@TaskStatusCode = tskstat.Code,
		@TaskStatusName = tskstat.Name,
		@ParentTaskID = tsk.ParentTaskID
	FROM dbo.Note nte
		JOIN dbo.Task tsk
			ON nte.NoteID = tsk.NoteID
		LEFT JOIN dbo.Scope scp
			ON scp.ScopeID = nte.ScopeID
		LEFT JOIN dbo.NoteType nt
			ON nt.NoteTypeID = nte.NoteTypeID
		LEFT JOIN dbo.Notebook nb
			ON nb.NotebookID = nte.NotebookID
		LEFT JOIN dbo.Priority pri
			ON pri.PriorityID = nte.PriorityID
		LEFT JOIN dbo.Origin org
			ON org.OriginID = nte.OriginID
		LEFT JOIN dbo.TaskStatus tskstat
			ON tskstat.TaskStatusID = tsk.TaskStatusID
		LEFT JOIN dbo.Application app
			ON app.ApplicationID = nte.ApplicationID
	WHERE ( @BusinessEntityID IS NULL AND nte.NoteID = @NoteID )
		OR ( @BusinessEntityID IS NOT NULL AND nte.NoteID = @NoteID AND nte.BusinessEntityID = @BusinessEntityID )
		

	---------------------------------------------------------------------------------------------------------------------------------
	-- Retrieve data.
	---------------------------------------------------------------------------------------------------------------------------------
	IF ISNULL(@IsOutputOnly, 0) = 0
	BEGIN
	
		SELECT
			@NoteID AS NoteID,
			@ApplicationID AS ApplicationID,
			@ApplicationCode AS ApplicationCode,
			@BusinessID AS BusinessID,
			@BusinessEntityID AS BusinessEntityID,
			@ScopeID AS ScopeID,
			@ScopeCode AS ScopeCode,
			@NoteTypeID AS NoteTypeID,
			@NoteTypeCode AS NoteTypeCode,
			@NotebookID AS NotebookID,
			@NotebookCode AS NotebookCode,
			@Title AS Title,
			@Memo AS Memo,
			@PriorityID AS PriorityID,
			@PriorityCode AS PriorityCode,
			@OriginID AS OriginID,
			@OriginCode AS OriginCode,
			@OriginDataKey AS OriginDataKey,
			@AdditionalData AS AdditionalData,
			@CorrelationKey AS CorrelationKey,
			@DateDue AS DateDue,
			@AssignedByID AS AssignedByID,
			@AssignedByString AS AssignedByString,
			@AssignedToID AS AssignedToID,
			@AssignedToString AS AssignedToString,
			@CompletedByID AS CompletedByID,
			@CompletedByString AS CompletedByString,
			@DateCompleted AS DateCompleted,
			@TaskStatusID AS TaskStatusID,
			@TaskStatusCode AS TaskStatusCode,
			@ParentTaskID AS ParentTaskID
	
	END
		
		
END
