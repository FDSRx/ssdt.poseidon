﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/25/2013
-- Description:	Create new business entity
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Create]
	@BusinessEntityTypeID INT,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------
	-- Sanitize input
	---------------------------------------------------	
	SET @BusinessEntityID = NULL;
	
	---------------------------------------------------
	-- Create new business enity
	---------------------------------------------------
	INSERT INTO dbo.BusinessEntity (
		BusinessEntityTypeID,
		CreatedBy
	)
	SELECT
		@BusinessEntityTypeID,
		@CreatedBy
		
	SET @BusinessEntityID = SCOPE_IDENTITY();
	

		
	
END
