﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/04/2015
-- Description:	Determines if the Configuration object exists.
-- SAMPLE CALL:

/*
DECLARE
	@ConfigurationID BIGINT,
	@ApplicationID INT = 10,
	@BusinessEntityID BIGINT = 38,
	@KeyName VARCHAR(256) = 'IsEngageMessagingEnabled',
	@Exists BIT = NULL

EXEC dbo.spConfiguration_Exists
	@ApplicationID = @ApplicationID, -- "ESMT" (Engage/Store management tool)
	@BusinessEntityID = @BusinessEntityID,
	@KeyName = @KeyName,
	@ConfigurationID = @ConfigurationID OUTPUT,
	@Exists = @Exists OUTPUT

SELECT @ConfigurationID AS ConfigurationID, @Exists AS IsFound

*/
-- SELECT * FROM dbo.Configuration
-- =============================================
CREATE PROCEDURE [dbo].[spConfiguration_Exists]
	@ConfigurationID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@KeyName VARCHAR(256) = NULL OUTPUT,
	@KeyValue VARCHAR(MAX) = NULL OUTPUT,
	@DataTypeID INT= NULL OUTPUT,
	@DataSize INT = NULL OUTPUT,
	@Description VARCHAR(1000) = NULL OUTPUT,
	@CreatedBy VARCHAR(256) = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------
	SET @KeyValue = NULL;
	SET @DataSize = NULL;
	SET @DataTypeID = NULL;
	SET @Description = NULL;
	SET @CreatedBy = NULL;
	SET @Exists = ISNULL(@Exists, 0);

	-------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)		

	-------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------
	IF @ConfigurationID IS NOT NULL
	BEGIN
		SET @ApplicationID = NULL;
		SET @BusinessEntityID = NULL;
		SET @KeyName = NULL;
	END
	
	-------------------------------------------------------------------------------------
	-- Find object.
	-------------------------------------------------------------------------------------
	SELECT TOP 1
		@ConfigurationID = ConfigurationID,
		@ApplicationID = ApplicationID,
		@BusinessEntityID = BusinessEntityID,
		@KeyName = KeyName,
		@KeyValue = KeyValue,
		@DataTypeID = DataTypeID,
		@DataSize = DataSize,
		@Description = Description,
		@CreatedBy = CreatedBy
	FROM dbo.Configuration
	WHERE (@ConfigurationID IS NOT NULL AND ConfigurationID = @ConfigurationID)
		OR (@ConfigurationID IS NULL
			AND (
				(@ApplicationID IS NULL OR ApplicationID = @ApplicationID)
				AND (@BusinessEntityID IS NULL OR BusinessEntityID = @BusinessEntityID)
				AND KeyName = @KeyName
			)
		)

	-- If an object was not discovered then void the data.
	IF @@ROWCOUNT = 0
	BEGIN
		SET @ConfigurationID = NULL;
		SET @ApplicationID = NULL;
		SET @BusinessEntityID = NULL;
		SET @KeyName = NULL;
	END
	ELSE
	BEGIN
		SET @Exists = 1;
	END
	

	
	
END
