﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 03/17/2015
-- Description:	Updates a Chain object.
-- Change log:
-- 8/3/2016 - dhughes - Modified the procedure to only update the object if the Chain object has changed.
-- SAMPLE CALL:
/*

DECLARE
	@BusinessEntityID BIGINT = 122,	
	@SourceChainID INT = NULL,
	@Name VARCHAR(255) = 'Demo User Pharmacy',
	@TimeZoneID INT = NULL,
	@SupportDST BIT = 1,
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(75) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(25) = NULL,
	@PhoneNumber_Work VARCHAR(25) = NULL,
	@PhoneNumber_Fax VARCHAR(25) = NULL,
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@ErrorLogID INT = NULL,
	@Exists BIT = NULL,
	@Debug BIT = 1

EXEC dbo.spChain_Update
	@BusinessEntityID = @BusinessEntityID,
	@SourceChainID = @SourceChainID,
	@Name = @Name,
	@TimeZoneID = @TimeZoneID,
	@SupportDST = @SupportDST,
	@AddressLine1 = @AddressLine1,
	@AddressLine2 = @AddressLine2,
	@City = @City,
	@State = @State,
	@PostalCode = @PostalCode,
	@PhoneNumber_Work = @PhoneNumber_Work,
	@PhoneNumber_Fax = @PhoneNumber_Fax,
	@EmailAddress_Primary = @EmailAddress_Primary,
	@EmailAddress_Alternate = @EmailAddress_Alternate,
	@ModifiedBy = @ModifiedBy,
	@ErrorLogID = @ErrorLogID,
	@Exists = @Exists,
	@Debug = @Debug


*/


-- SELECT * FROM dbo.Chain
-- SELECT * FROM dbo.Chain_History ORDER BY 1 DESC
-- SELECT * FROM dbo.Chain_History WHERE BusinessEntityID = 122 ORDER BY 1 DESC
-- SELECT * FROM dbo.BusinessEntityType
-- SELECT * FROM dbo.BusinessType

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spChain_Update]
	@BusinessEntityID BIGINT,	
	-- Chain
	@SourceChainID INT = NULL,
	@Name VARCHAR(255) = NULL,
	@TimeZoneID INT = NULL,
	@SupportDST BIT = 1,
	-- Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(75) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(25) = NULL,
	--Business Phone (Work)
	@PhoneNumber_Work VARCHAR(25) = NULL,
	@PhoneNumber_Fax VARCHAR(25) = NULL,
	--Business Email (Primary and alternate)
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	-- Caller
	@ModifiedBy VARCHAR(256) = NULL,
	-- Output
	@ErrorLogID INT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------
	-- Instance variables
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT

	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID)  + ';' +
		'@SourceChainID=' + dbo.fnToStringOrEmpty(@SourceChainID)  + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name)  + ';' +
		'@TimeZoneID=' + dbo.fnToStringOrEmpty(@TimeZoneID)  + ';' +
		'@SupportDST=' + dbo.fnToStringOrEmpty(@SupportDST)  + ';' +
		'@AddressLine1=' + dbo.fnToStringOrEmpty(@AddressLine1)  + ';' +
		'@AddressLine2=' + dbo.fnToStringOrEmpty(@AddressLine2)  + ';' +
		'@City=' + dbo.fnToStringOrEmpty(@City)  + ';' +
		'@State=' + dbo.fnToStringOrEmpty(@State)  + ';' +
		'@PostalCode=' + dbo.fnToStringOrEmpty(@PostalCode)  + ';' +
		'@PhoneNumber_Work=' + dbo.fnToStringOrEmpty(@PhoneNumber_Work)  + ';' +
		'@PhoneNumber_Fax=' + dbo.fnToStringOrEmpty(@PhoneNumber_Fax)  + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary)  + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate)  + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy)  + ';' +
		'@ErrorLogID=' + dbo.fnToStringOrEmpty(@ErrorLogID)  + ';' +
		'@Exists=' + dbo.fnToStringOrEmpty(@Exists)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;


	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------------
	SET @ErrorLogID = NULL;
	SET @Exists = 0;
	SET @Debug = ISNULL(@Debug, 0);
	SET @ModifiedBy = ISNULL(@ModifiedBy, SUSER_NAME());
	
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE
		@AddressID BIGINT
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------------
	
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set Variables' AS ActionMethod,
			@BusinessEntityID AS BusinessEntityID, @SourceChainID AS SourceChainID, @Name AS Name
	END

	-----------------------------------------------------------------------------------------------------------------------
	-- Infer a timezone.
	-----------------------------------------------------------------------------------------------------------------------
	-- If a time zone is not known during the initial pass then look one up by state
	IF ISNULL(@TimeZoneID, 0) = 0
	BEGIN
		SET @TimeZoneID = (SELECT TimeZoneID FROM StateProvince WHERE StateProvinceCode = @State);
		
		-- If a state was not provided then set the store to eastern time.  It can be updated later.
		IF ISNULL(@TimeZoneID, 0) = 0
		BEGIN
			SET @TimeZoneID = (SELECT TimeZoneID FROM dbo.TimeZone WHERE Code = 'EST');
		END
	END
	
	-- If "support daylight savings time" comes through as null then set to 1
	-- We will assume the store wants to support DST
	IF ISNULL(@SupportDST, 0) = 0
	BEGIN
		SET @SupportDST = 1;
	END
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Check for null arguments
	-- <Summary>
	-- Checks critical inputs for null argument exceptions.
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------------
	IF @BusinessEntityID IS NULL
	BEGIN
		
		SET @ErrorMessage = 'Unable to modify Chain. Exception: Object reference (@BusinessEntityID) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;	
	END
	
		
	-----------------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. If the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	-----------------------------------------------------------------------------------------------------------------------	
	BEGIN TRY
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	-----------------------------------------------------------------------------------------------------------------------	
	-- Begin data transaction.
	-----------------------------------------------------------------------------------------------------------------------

		-----------------------------------------------------------------------------------------------------------------------
		-- Update chain.
		-- <Summary>
		-- Modifies the chain object.
		-- </Summary>
		-----------------------------------------------------------------------------------------------------------------------
		-- Update business.
		UPDATE trgt
		SET
			trgt.Name = CASE WHEN @Name IS NULL THEN trgt.Name ELSE @Name END
		--SELECT *
		FROM dbo.Business trgt
		WHERE trgt.BusinessEntityID = @BusinessEntityID
			AND (
				@Name IS NOT NULL
				AND trgt.Name <> @Name
			)
		
		-- Update chain.
		UPDATE trgt
		SET
			trgt.Name = CASE WHEN @Name IS NULL THEN trgt.Name ELSE @Name END,
			trgt.DateModified = GETDATE(),
			trgt.ModifiedBy = CASE WHEN @ModifiedBy IS NULL THEN trgt.ModifiedBy ELSE @ModifiedBy END
		--SELECT *
		FROM dbo.Chain trgt
		WHERE trgt.BusinessEntityID = @BusinessEntityID
			AND (
				@Name IS NOT NULL
				AND trgt.Name <> @Name
			)
	
		-----------------------------------------------------------------------------------------------------------------------
		-- Create new chain address.
		-----------------------------------------------------------------------------------------------------------------------	
		-- Create a new address entry (if applicable)
		EXEC spAddress_Create 
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			@CreatedBy = @ModifiedBy,
			@AddressID = @AddressID OUTPUT
		
		-- Get the address ID for primary address
		DECLARE @AddressTypeID_Primary INT = dbo.fnGetAddressTypeID('PRIM');
		
		-- Create business address
		EXEC spBusinessEntity_Address_Merge
			@BusinessEntityID = @BusinessEntityID,
			@AddressTypeID = @AddressTypeID_Primary,
			@AddressID = @AddressID,
			@CreatedBy = @ModifiedBy
			
		-----------------------------------------------------------------------------------------------------------------------
		-- Create new chain email.
		-----------------------------------------------------------------------------------------------------------------------
		DECLARE 
			@EmailAddressTypeID_Primary INT = dbo.fnGetEmailAddressTypeID('PRIM'),
			@EmailAddressTypeID_Alternate INT = dbo.fnGetEmailAddressTypeID('ALT')
		
		-- Create primary email address
		EXEC spBusinessEntity_Email_Merge	
			@BusinessEntityID = @BusinessEntityID,
			@EmailAddressTypeID = @EmailAddressTypeID_Primary,
			@EmailAddress = @EmailAddress_Primary,
			@CreatedBy = @ModifiedBy


		-- Create alternate email address
		EXEC spBusinessEntity_Email_Merge
			@BusinessEntityID = @BusinessEntityID,
			@EmailAddressTypeID = @EmailAddressTypeID_Alternate,
			@EmailAddress = @EmailAddress_Alternate,
			@CreatedBy = @ModifiedBy
		
		-----------------------------------------------------------------------------------------------------------------------
		-- Create new chain phone number.
		-----------------------------------------------------------------------------------------------------------------------
		DECLARE
			@PhoneNumberTypeID_Work INT = dbo.fnGetPhoneNumberTypeID('WRK'),
			@PhoneNumberTypeID_Fax INT = dbo.fnGetPhoneNumberTypeID('FAX')
			
		-- Create new work phone
		EXEC spBusinessEntity_Phone_Merge
			@BusinessEntityID = @BusinessEntityID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Work,
			@PhoneNumber = @PhoneNumber_Work,
			@CreatedBy = @ModifiedBy
		
		-- Create new fax number
		EXEC spBusinessEntity_Phone_Merge
			@BusinessEntityID = @BusinessEntityID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Fax,
			@PhoneNumber = @PhoneNumber_Fax,
			@CreatedBy = @ModifiedBy
			
	-----------------------------------------------------------------------------------------------------------------------	
	-- End data transaction.
	-----------------------------------------------------------------------------------------------------------------------			
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
			
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args

		SET @ErrorMessage = ERROR_MESSAGE();
	
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);		
			
	END CATCH
		
END
