﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/2/2015
-- Description:	Deletes a BusinessEntityAppliation object.
-- SAMPLE CALL:
/*

DECLARE 
	@BusinessEntityAppliationID BIGINT = NULL
	
EXEC dbo.spBusinessEntityApplication_Delete
	@BusinessEntityAppliationID = @BusinessEntityAppliationID,
	@BusinessEntityID = 959790,
	@ApplicationID = 8
	

SELECT @BusinessEntityAppliationID AS BusinessEntityAppliationID

*/

-- SELECT * FROM dbo.BusinessEntityApplication
-- SElECT * FROM dbo.Application
-- SELECT * FROM dbo.BusinessEntity
-- =============================================
CREATE PROCEDURE dbo.[spBusinessEntityApplication_Delete]
	@BusinessEntityApplicationID BIGINT = NULL,
	@BusinessEntityID BIGINT= NULL,
	@ApplicationID INT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID)
		
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------


	/*
	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplication object. Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @ApplicationID
	IF @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplication object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/
	
	-------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy
		
	-------------------------------------------------------------------------------
	-- Delete object.
	-------------------------------------------------------------------------------
	DELETE obj
	FROM dbo.BusinessEntityApplication obj
	WHERE obj.BusinessEntityApplicationID = @BusinessEntityApplicationID
		OR (
			obj.BusinessEntityID = @BusinessEntityID
			AND obj.ApplicationID = @ApplicationID
		)
		
		
END
