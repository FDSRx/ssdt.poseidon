﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/17/2013
-- Description:	Determines if a business entity phone number exists.
-- SAMPLE CALL:
/*
DECLARE 
	@Exists BIT = 0, 
	@BusinessEntityPhoneNumberID BIGINT = NULL

---- Surrogate validation
--EXEC dbo.spBusinessEntity_Phone_Exists
--	@BusinessEntityPhoneNumberID = @BusinessEntityPhoneNumberID OUTPUT,
--	@BusinessEntityID = -1,
--	@PhoneNumberTypeID = -1,
--	@Exists = @Exists OUTPUT

-- Unique key validation
EXEC dbo.spBusinessEntity_Phone_Exists
	@BusinessEntityPhoneNumberID = @BusinessEntityPhoneNumberID OUTPUT,
	@BusinessEntityID = 1,
	@PhoneNumberTypeID = 3,
	@Exists = @Exists OUTPUT
	
SELECT @Exists AS IsFound, @BusinessEntityPhoneNumberID AS BusinessEntityPhoneNumberID
		
*/
-- SELECT * FROM dbo.BusinessEntityPhone
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Phone_Exists]
	@BusinessEntityPhoneNumberID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@PhoneNumberTypeID INT = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidateSurrogateKey BIT = 0
		

	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------	
	IF @BusinessEntityPhoneNumberID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @PhoneNumberTypeID = NULL;
		
		SET @ValidateSurrogateKey = 1;
	END
	
	----------------------------------------------------------------------
	-- Argument Null Exceptions
	----------------------------------------------------------------------
	----------------------------------------------------------------------
	-- Surrogate key validation.
	-- <Summary>
	-- If a surrogate key was provided, then that will be the only lookup
	-- performed.
	-- <Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityPhoneNumberID, 0) = 0 AND @ValidateSurrogateKey = 1
	BEGIN			
		
		SET @ErrorMessage = 'Object reference not set to an instance of an object. The object, @BusinessEntityPhoneNumberID, cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	----------------------------------------------------------------------
	-- Unique row key validation.
	-- <Summary>
	-- If a surrogate key was not provided then we will try and perform the
	-- lookup based on the records unique key.
	-- </Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0 AND ISNULL(@PhoneNumberTypeID, 0) = 0 AND @ValidateSurrogateKey = 0
	BEGIN
		
		SET @ErrorMessage = 'Object references not set to an instance of an object. The objects, @BusinessEntityID and @PhoneNumberTypeID cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END
	
	
	--------------------------------------------------------------------
	-- Determine if business entity service exists
	--------------------------------------------------------------------
	SET @BusinessEntityPhoneNumberID = 
		CASE 
			WHEN @BusinessEntityPhoneNumberID IS NOT NULL AND @ValidateSurrogateKey = 1 THEN (
				SELECT TOP 1 BusinessEntityPhoneNumberID 
				FROM dbo.BusinessEntityPhone
				WHERE BusinessEntityPhoneNumberID = @BusinessEntityPhoneNumberID
			)
			ELSE (
				SELECT TOP 1 BusinessEntityPhoneNumberID
				FROM dbo.BusinessEntityPhone
				WHERE BusinessEntityID = @BusinessEntityID
					AND PhoneNumberTypeID = @PhoneNumberTypeID
			)
		END;
	
	-- Set the exists flag to properly represent the finding.
	SET @Exists = CASE WHEN @BusinessEntityPhoneNumberID IS NOT NULL THEN 1 ELSE 0 END;
	
END
