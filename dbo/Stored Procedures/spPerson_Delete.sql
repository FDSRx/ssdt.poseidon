﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/16/2016
-- Description:	Deletes a Person object and all of its related dependencies.
-- SAMPLE CALL:
/*

DECLARE
	@PersonID VARCHAR(MAX),
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL

EXEC dbo.spPerson_Delete
	@PersonID = @PersonID,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/
-- =============================================
CREATE PROCEDURE spPerson_Delete
	@PersonID VARCHAR(MAX),
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @ModifiedBy = ISNULL(@ModifiedBy, SUSER_NAME());

	------------------------------------------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------------------------------------------
		
	------------------------------------------------------------------------------------------------------------
	-- Set variables
	------------------------------------------------------------------------------------------------------------	
	
	-- debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod, 
			@PersonID AS PersonID, @ModifiedBy AS ModifiedBy
	END


	-----------------------------------------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-----------------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy


	BEGIN TRY
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	-----------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	-----------------------------------------------------------------------------------------------------------------

		-- utl.spFindTable 'PersonID'

		-----------------------------------------------------------------------------------------------------------------
		-- Banner object.
		-----------------------------------------------------------------------------------------------------------------
		-- BannerSchedule object.
		DELETE trgt
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.BannerSchedule trgt
		WHERE PersonID = @PersonID

		-- Patient object.
		DELETE trgt
		--SELECT *
		--SELECT TOP 1 *
		FROM phrm.Patient trgt
		WHERE PersonID = @PersonID

		-- Vaccination object.
		DELETE trgt
		--SELECT *
		--SELECT TOP 1 *
		FROM vaccine.Vaccination trgt
		WHERE PersonID = @PersonID

		-- Images object.
		DELETE trgt
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.Images trgt
		WHERE PersonID = @PersonID

		-- Password object.
		DELETE trgt
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.Password trgt
		WHERE PersonID = @PersonID

		-----------------------------------------------------------------------------------------------------------------
		-- Remove credential.
		-----------------------------------------------------------------------------------------------------------------
		DECLARE
			@CredentialEntityID BIGINT = (SELECT TOP 1 CredentialEntityID FROM creds.CredentialEntity WHERE PersonID = @PersonID)

		-- CredentialEntity object.
		EXEC creds.spCredentialEntity_Delete
			@CredentialentityID = @CredentialEntityID,
			@ModifiedBy = @ModifiedBy

		-- Medication object.
		DELETE trgt
		--SELECT *
		--SELECT TOP 1 *
		FROM phrm.Medication trgt
		WHERE PersonID = @PersonID

		-- PersonMessage object.
		DELETE trgt
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.PersonMessage trgt
		WHERE PersonID = @PersonID

		-- BusinessEntityContact object.
		DELETE trgt
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.BusinessEntityContact trgt
		WHERE PersonID = @PersonID

		-- Customer object.
		DELETE trgt
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.Customer trgt
		WHERE PersonID = @PersonID

		-----------------------------------------------------------------------------------------------------------------
		-- Patient object.
		-----------------------------------------------------------------------------------------------------------------
		-- SELECT * FROM phrm.Patient
		DECLARE
			@PatientID BIGINT = (SELECT TOP 1 PatientID FROM phrm.Patient WHERE PersonID = @PersonID)

		EXEC phrm.spPatient_Delete
			@PatientID = @PatientID


		-----------------------------------------------------------------------------------------------------------------
		-- Person object.
		-----------------------------------------------------------------------------------------------------------------
		-- Person object.
		DELETE trgt
		--SELECT *
		--SELECT TOP 1 *
		FROM dbo.Person trgt
		WHERE BusinessEntityID = @PersonID




	-----------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	-----------------------------------------------------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		SET @ErrorMessage = ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			16,
			1
		);	
	
	END CATCH


END