﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Merges a range of Tags.
-- SAMPLE CALL:
/*
DECLARE @TagIDs VARCHAR(MAX)

EXEC dbo.spTag_MergeRange
	@TagTypeID = 'PHRM',
	@Names = 'High Risk Medications, 5-Star, Med Sync, MTM',
	@CreatedBy = 'dhughes',
	@TagIDs = @TagIDs OUTPUT

SELECT @TagIDs AS TagIDs
*/
-- SELECT * FROM dbo.Tag
-- SELECT * FROM dbo.TagTagType
-- =============================================
CREATE PROCEDURE [dbo].[spTag_MergeRange]
	@TagTypeID VARCHAR(MAX) = NULL,
	@Names VARCHAR(MAX), -- A comma delimited list of tag names.
	@CreatedBy VARCHAR(256) = NULL,
	@TagIDs VARCHAR(MAX) = NULL OUTPUT -- A comma delimited list of the created/existing TagID's.
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------
	SET @TagIDs = NULL;

	--------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000) = NULL

	--------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------
	SET @TagTypeID = ISNULL(@TagTypeID, dbo.fnGetTagTypeID('UD'));
		
	--------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	--------------------------------------------------------------
	-- A tag name is required to be created.
	IF LTRIM(RTRIM(ISNULL(@Names, ''))) = ''
	BEGIN
	
		SET @ErrorMessage = 'Unable to create Tag entries.  Object reference (@Names) is not set to an instance of an object. ' +
			'The tag name list cannot be null or empty.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END

	--------------------------------------------------------------
	-- Temporary resources.
	--------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpTag_Names') IS NOT NULL
	BEGIN
		DROP TABLE #tmpTag_Names;
	END
	
	CREATE TABLE #tmpTag_Names (
		Name VARCHAR(500)
	);
	
	IF OBJECT_ID('tempdb..#tmpTag_Output') IS NOT NULL
	BEGIN
		DROP TABLE #tmpTag_Output;
	END
	
	CREATE TABLE #tmpTag_Output (
		TagID BIGINT,
		Name VARCHAR(500)
	);
	
	
	BEGIN TRY
	
		--------------------------------------------------------------
		-- Parse tag range.
		-- <Summary>
		-- Splits the comma delimited string of tag names into a table list.
		-- </Summary>
		--------------------------------------------------------------
		INSERT INTO #tmpTag_Names (
			Name
		)
		SELECT
			LTRIM(RTRIM(Value)) AS Value
		FROM dbo.fnSplit(@Names, ',')
		WHERE LTRIM(RTRIM(ISNULL(Value, ''))) <> ''
		
		-- Validate list
		IF NOT EXISTS(SELECT TOP 1 * FROM #tmpTag_Names)
		BEGIN
			SET @ErrorMessage = 'Unable to create Tag object entries.  Object reference (@Names) is not set to an instance of an object. ' +
				'The @Names list does not contain a list of items or is not formatted correctly.';
				
			RAISERROR (@ErrorMessage, -- Message text.
					   16, -- Severity.
					   1 -- State.
					   );
			
			RETURN;		
		END
				
		--------------------------------------------------------------
		-- Create tag entries.
		-- <Summary>
		-- Adds the range of tags into the tag collection.
		-- </Summary>
		--------------------------------------------------------------
		-- Find existing tags
		INSERT INTO #tmpTag_Output (
			TagID,
			Name
		)
		SELECT
			trgt.TagID,
			trgt.Name
		--SELECT *
		FROM #tmpTag_Names tmp
			JOIN dbo.Tag trgt
				ON trgt.Name = tmp.Name
		
		-- Create new tags
		INSERT INTO dbo.Tag (
			Name,
			CreatedBy
		)
		OUTPUT inserted.TagID, inserted.Name INTO #tmpTag_Output(TagID, Name)
		SELECT
			tmp.Name,
			@CreatedBy
		--SELECT *
		FROM #tmpTag_Names tmp
			LEFT JOIN dbo.Tag trgt
				ON tmp.Name = trgt.Name
		WHERE trgt.TagID IS NULL

		--------------------------------------------------------------
		-- Build tag output string.
		-- <Summary>
		-- Creates a comma delimited string of existing and created
		-- TagIDs.
		-- </Summary>
		--------------------------------------------------------------
		SET @TagIDs = (
			SELECT CONVERT(VARCHAR(MAX), TagID) + ','
			FROM #tmpTag_Output
			FOR XML PATH('')
		);
		
		-- Remove trailing comma.
		SET @TagIDs = LEFT(@TagIDs, LEN(@TagIDs) - 1);
		
		--------------------------------------------------------------
		-- Create new Tag/TagType objects.
		-- <Summary>
		-- Creates a new Tag/TagType object reference(s).
		-- </Summary>
		--------------------------------------------------------------			
		IF @TagTypeID IS NOT NULL
		BEGIN
			SET @TagTypeID = dbo.fnTagTypeKeyTranslator(@TagTypeID, DEFAULT);
		END
		
		SET @TagTypeID = ISNULL(@TagTypeID, dbo.fnGetTagTypeID('UD'));
		
		-- Create new Tag/TagType objects.
		INSERT INTO dbo.TagTagType (
			TagID,
			TagTypeID,
			CreatedBy
		)
		SELECT
			tags.TagID AS TagID,
			Value AS TagTypeID,
			@CreatedBy
		FROM dbo.fnSplit(@TagTypeID, ',') typs
			CROSS APPLY #tmpTag_Output tags
			LEFT JOIN dbo.TagTagType tt
				ON tt.TagID = tags.TagID
					AND tt.TagTypeID = typs.Value
		WHERE ISNUMERIC(typs.Value) = 1
			AND tt.TagTagTypeID IS NULL
		
		
	
	END TRY
	BEGIN CATCH
	
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);	
	
	END CATCH
	
		
	--------------------------------------------------------------
	-- Dispose of temporary resources.
	--------------------------------------------------------------	
	DROP TABLE #tmpTag_Output;	
	DROP TABLE #tmpTag_Names;
END
