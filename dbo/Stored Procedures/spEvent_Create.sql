﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/9/2014
-- Description:	Creates a new Event object.
-- SAMPLE CALL:
/*

DECLARE
	@EventTypeID INT = dbo.fnGetEventTypeID('CRED'),
	@EventID INT = NULL
	
EXEC dbo.spEvent_Create
	@EventTypeID = @EventTypeID,
	@CreatedBy = 'dhughes',
	@EventID = @EventID OUTPUT

SELECT @EventID AS EventID

*/

-- SELECT * FROM dbo.Event
-- =============================================
CREATE PROCEDURE dbo.spEvent_Create
	@EventTypeID INT,
	@CreatedBy VARCHAR(256) = NULL,
	@EventID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------
	SET @EventID = NULL;
	
	-------------------------------------------------------------------------
	-- Create Event object.
	-- <Summary>
	-- Creates an Event object super-type that is used to define a sub-type
	-- object.
	-- </Summary>
	-------------------------------------------------------------------------	
	INSERT INTO dbo.Event (
		EventTypeID,
		CreatedBy
	)
	SELECT 
		@EventTypeID AS EventTypeID,
		@CreatedBy AS CreatedBy
	
	-- Retrieve identity
	SET @EventID = SCOPE_IDENTITY();
	

END
