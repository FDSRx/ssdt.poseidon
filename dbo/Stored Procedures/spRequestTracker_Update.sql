﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/3/2014
-- Description:	Updates a RequestTracker object.
-- SAMPLE CALL:
/*

DECLARE 
	@RequestTrackerID BIGINT,
	@IsLocked BIT,
	@DateExpires DATETIME,
	@RequestsRemaining INT

EXEC dbo.spRequestTracker_Update
	@RequestTypeID = 2,
	@SourceName = 'dbo.spRequestTracker_Create',
	@ApplicationKey = 'MPC',
	@BusinessKey = '000000',
	@RequesterKey = 'dhughes',
	@RequestKey = 'MPC-000000-dhughes',
	@IsLocked = @IsLocked OUTPUT,
	@DateExpires = @DateExpires OUTPUT,
	@RequestsRemaining = @RequestsRemaining OUTPUT,
	@RequestData = 'Test Call Update',
	@ModifiedBy = 'dhughes',
	@RequestTrackerID = @RequestTrackerID OUTPUT

SELECT @RequestTrackerID AS RequestTrackerID, @IsLocked AS IsLocked, @DateExpires AS DateExpires, @RequestsRemaining AS RequestsRemaining

*/

-- SELECT * FROM dbo.RequestTracker ORDER BY RequestTrackerID DESC
-- TRUNCATE TABLE dbo.RequestTracker 
-- =============================================
CREATE PROCEDURE [dbo].[spRequestTracker_Update]
	@RequestTrackerID BIGINT = NULL OUTPUT,
	@RequestTypeID INT = NULL,
	@SourceName VARCHAR(256) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@RequesterKey VARCHAR(256) = NULL,
	@RequestKey VARCHAR(256) = NULL,
	@RequestCount INT = NULL OUTPUT,
	@RequestsRemaining INT = NULL OUTPUT,
	@MaxRequestsAllowed INT = NULL,
	@IsLocked BIT = NULL OUTPUT,
	@DateExpires DATETIME = NULL OUTPUT,
	@ApplicationID INT = NULL OUTPUT,
	@BusinessEntityID INT = NULL OUTPUT,
	@RequesterID INT = NULL OUTPUT,
	@RequestData VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------

	
	---------------------------------------------------------------------------------------------
	-- Temporary resources.
	---------------------------------------------------------------------------------------------
	DECLARE @tblOutput AS TABLE (
		RequestTrackerID INT,
		RequestCount INT,
		RequestsRemaining INT,
		IsLocked INT,
		DateExpires DATETIME,
		ApplicationID INT,
		BusinessEntityID INT,
		RequesterID INT
	);	
		
	
	---------------------------------------------------------------------------------------------
	-- Determine which row identifier should be used to look up record.
	---------------------------------------------------------------------------------------------
	IF @RequestTrackerID IS NOT NULL
	BEGIN
		SET @ApplicationKey = NULL;
		SET @BusinessKey = NULL;
		SET @RequesterKey = NULL;
		SET @RequestKey = NULL;
	END
	
	
	-- Debug
	--SELECT @RequestTrackerID AS RequestTrackerID, @ApplicationKey AS ApplicationKey, @BusinessKey AS Businesskey, @RequesterKey AS RequesterKey,
	--	@RequestKey AS RequestKey, @RequestCount AS RequestCount, @RequestsRemaining AS RequestsRemaining, @MaxRequestsAllowed AS MaxRequestsAllowed,
	--	@IsLocked AS IsLocked, @DateExpires AS DateExpires
		
	---------------------------------------------------------------------------------------------
	-- Create/Track request.
	---------------------------------------------------------------------------------------------
	UPDATE rt
	SET
		rt.RequestTypeID =
			CASE
				WHEN @RequestTypeID IS NOT NULL THEN @RequestTypeID
				ELSE rt.RequestTypeID
			END,
		rt.RequestCount = 
			CASE
				WHEN @RequestCount IS NOT NULL THEN @RequestCount
				ELSE rt.RequestCount + 1
			END,
		rt.RequestsRemaining =
			CASE
				WHEN @RequestsRemaining IS NOT NULL THEN @RequestsRemaining
				WHEN rt.RequestsRemaining > 0 THEN rt.RequestsRemaining - 1
				ELSE rt.RequestsRemaining
			END,
		rt.MaxRequestsAllowed =
			CASE
				WHEN @MaxRequestsAllowed IS NOT NULL THEN @MaxRequestsAllowed
				WHEN rt.MaxRequestsAllowed IS NULL THEN 2147483647
				ELSE rt.MaxRequestsAllowed
			END,
		rt.IsLocked =
			CASE 
				WHEN @IsLocked IS NOT NULL THEN @IsLocked
				WHEN rt.RequestsRemaining <= 0 THEN 1
				ELSE rt.IsLocked
			END,
		rt.DateExpires = 
			CASE 
				WHEN @DateExpires IS NOT NULL THEN @DateExpires
				WHEN @IsLocked IS NOT NULL THEN DATEADD(MI, 20, GETDATE()) -- default to a 20 minute lockout.
				WHEN rt.RequestsRemaining <= 0 THEN DATEADD(MI, 20, GETDATE()) -- default to a 20 minute lockout.
				ELSE rt.DateExpires
			END,
		rt.ApplicationID = 
			CASE
				WHEN @ApplicationID IS NOT NULL THEN @ApplicationID
				ELSE rt.ApplicationID
			END,
		rt.BusinessEntityID = 
			CASE
				WHEN @BusinessEntityID IS NOT NULL THEN @BusinessEntityID
				ELSE rt.BusinessEntityID
			END,
		rt.RequesterID = 
			CASE
				WHEN @RequesterID IS NOT NULL THEN @RequesterID
				ELSE rt.RequesterID
			END,
		rt.RequestData = @RequestData,
		rt.Arguments = @Arguments,
		rt.DateModified = GETDATE(),
		rt.ModifiedBy = @ModifiedBy
	OUTPUT
		inserted.RequestTrackerID,
		inserted.RequestCount,
		inserted.RequestsRemaining,
		inserted.IsLocked,
		inserted.DateExpires,
		inserted.ApplicationID,
		inserted.BusinessEntityID,
		inserted.RequesterID
	INTO @tblOutput (
		RequestTrackerID,
		RequestCount,
		RequestsRemaining,
		IsLocked,
		DateExpires,
		ApplicationID,
		BusinessEntityID,
		RequesterID
	)
	--SELECT TOP 1 *	
	--SELECT *
	FROM dbo.RequestTracker rt
	WHERE RequestTrackerID = @RequestTrackerID 
		OR (
			( ApplicationKey = @ApplicationKey )
			AND ( BusinessKey = @BusinessKey )
			AND ( RequesterKey = @RequesterKey )
			AND RequestKey = @RequestKey
		)
		OR (
			ApplicationKey IS NULL
			AND ( BusinessKey = @BusinessKey )
			AND ( RequesterKey = @RequesterKey )
			AND RequestKey = @RequestKey
		)
		OR (
			ApplicationKey IS NULL
			AND BusinessKey IS NULL
			AND ( RequesterKey = @RequesterKey )
			AND RequestKey = @RequestKey
		)
		OR (
			ApplicationKey IS NULL
			AND BusinessKey IS NULL
			AND RequesterKey IS NULL
			AND RequestKey = @RequestKey
		)
	
	-- Retrieve record information.
	SELECT
		@RequestTrackerID = RequestTrackerID,
		@RequestCount = RequestCount,
		@RequestsRemaining = RequestsRemaining,
		@IsLocked = IsLocked,
		@DateExpires = DateExpires,
		@ApplicationID = ApplicationID,
		@BusinessEntityID = BusinessEntityID,
		@RequesterID = RequesterID
	FROM @tblOutput
	
	
END
