﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/19/2014
-- Description:	Indicates whether the provided address information already exists within the system.
/*

DECLARE 
	@AddressID BIGINT

EXEC dbo.spAddress_Exists
	--@City = 'N',
	--@PostalCode = '10021',
	@AddressID = @AddressID OUTPUT

SELECT @AddressID AS AddressID

*/

-- SELECT TOP 10 * FROM dbo.Address ORDER BY AddressID DESC
-- SELECT * FROM dbo.Address WHERE AddressID = 658353
-- SELECT * FROM dbo.Address WHERE AddressLine1 IS NULL AND StateProvinceID IS NULL
-- =============================================
CREATE PROCEDURE [dbo].[spAddress_Exists]
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@StateProvinceID INT = NULL,
	@PostalCode VARCHAR(25) = NULL,
	@SpatialLocation GEOGRAPHY = NULL,
	@Latitude DECIMAL(9,6) = NULL,
	@Longitude DECIMAL(9,6) = NULL,
	@Exists BIT = NULL,
	@AddressID BIGINT = NULL OUTPUT
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------
	SET @AddressID = NULL;
	SET @Exists = 0;
	
	------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------
	
	------------------------------------------------------------------------------------
	-- Perform necessary lookups where applicable.
	------------------------------------------------------------------------------------
	SET @StateProvinceID = 
		CASE
			WHEN @StateProvinceID IS NOT NULL THEN @StateProvinceID
			ELSE (SELECT StateProvinceID FROM StateProvince WHERE StateProvinceCode = @State)
		END;
	
	------------------------------------------------------------------------------------
	-- Determine if an existing address exists.
	------------------------------------------------------------------------------------
	SELECT TOP 1 
		@AddressID = AddressID
	FROM dbo.Address (NOLOCK)
	WHERE ( ( @AddressLine1 IS NULL AND AddressLine1 IS NULL ) OR AddressLine1 = @AddressLine1 )
			AND ( ( @AddressLine2 IS NULL AND AddressLine2 IS NULL )  OR AddressLine2 = @AddressLine2 )
			AND ( ( @City IS NULL AND City IS NULL )  OR City = @City )
			AND ( ( @StateProvinceID IS NULL AND StateProvinceID IS NULL )  OR StateProvinceID = @StateProvinceID )
			AND ( ( @PostalCode IS NULL AND PostalCode IS NULL )  OR PostalCode = @PostalCode )
			
			
	------------------------------------------------------------------------------------
	-- Set the exists flag.
	------------------------------------------------------------------------------------
	SET @Exists = CASE WHEN @AddressID IS NOT NULL THEN 1 ELSE 0 END;		
			
END
