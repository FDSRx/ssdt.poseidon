﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/26/2013
-- Description:	Creates a new chain entry.

-- SELECT * FROM dbo.Chain
-- SELECT * FROM dbo.BusinessEntityType
-- SELECT * FROM dbo.BusinessType
-- =============================================
CREATE PROCEDURE [dbo].[spChain_Create]
	-- Chain
	@SourceChainID INT,
	@Name VARCHAR(255),
	@TimeZoneID INT = NULL,
	@SupportDST BIT = 1,
	-- Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(75) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(25) = NULL,
	--Business Phone (Work)
	@PhoneNumber_Work VARCHAR(25) = NULL,
	@PhoneNumber_Fax VARCHAR(25) = NULL,
	--Business Email (Primary and alternate)
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Output
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@ErrorLogID INT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Instance variables
	----------------------------------------------------------------------
	DECLARE @Trancount INT = @@TRANCOUNT;
	
	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @BusinessEntityID = NULL;
	SET @ErrorLogID = NULL;
	SET @Exists = 0;
	
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@BusinessEntityTypeID INT = NULL,
		@BusinessTypeID INT = NULL
	
	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------
	SET @BusinessEntityTypeID = dbo.fnGetBusinessEntityTypeID('CHN');
	SET @BusinessTypeID = dbo.fnGetBusinessTypeID('CHN');
	
	----------------------------------------------------------------------
	-- Check for null arguments
	-- <Summary>
	-- Checks critical inputs for null argument exceptions.
	-- The following pieces are required to properly create
	-- an Extranet user
	-- </Summary>
	----------------------------------------------------------------------
	IF @SourceChainID IS NULL
	BEGIN
		
		SET @ErrorMessage = 'Unable to create Chain. Exception: Object reference (@SourceChainID) is not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;	
	END
	
	----------------------------------------------------------------------
	-- Check to see if the chain already exists
	-- <Summary>
	-- A validation step used to bypass chain creation
	-- if the chain already exists.
	-- </Summary>
	----------------------------------------------------------------------
	SET @BusinessEntityID = (SELECT TOP 1 BusinessEntityID FROM dbo.Chain WHERE SourceChainID = @SourceChainID);
	
	IF ISNULL(@BusinessEntityID, 0) <> 0
	BEGIN
	
		SET @Exists = 1;
		
		RETURN;
		
	END	
		
	
	BEGIN TRY
	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		DECLARE 
			@BusinessNumber VARCHAR(25),
			@AddressID INT
			
		----------------------------------------------------------------------
		-- Create new business enity
		-- <Summary>
		-- Creates a new unique identifier for the Chain item.
		-- </Summary>
		----------------------------------------------------------------------
		EXEC spBusiness_Create
			@BusinessEntityTypeID = @BusinessEntityTypeID,
			@BusinessTypeID = @BusinessTypeID,
			@Name = @Name,
			@CreatedBy = @CreatedBy,
			@BusinessEntityID = @BusinessEntityID OUTPUT
		
		----------------------------------------------------------------------
		-- Infer a timezone.
		----------------------------------------------------------------------
		-- If a time zone is not known during the initial pass then look one up by state
		IF ISNULL(@TimeZoneID, 0) = 0
		BEGIN
			SET @TimeZoneID = (SELECT TimeZoneID FROM StateProvince WHERE StateProvinceCode = @State);
			
			-- If a state was not provided then set the store to eastern time.  It can be updated later.
			IF ISNULL(@TimeZoneID, 0) = 0
				SET @TimeZoneID = (SELECT TimeZoneID FROM dbo.TimeZone WHERE Code = 'EST')
		END
		
		-- If "support daylight savings time" comes through as null then set to 1
		-- We will assume the store wants to support DST
		IF ISNULL(@SupportDST, 0) = 0
		BEGIN
			SET @SupportDST = 1;
		END
		
		----------------------------------------------------------------------
		-- Create chain
		-- <Summary>
		-- Adds the chain data to the chain table
		-- </Summary>
		-- <Remarks>
		-- Time zone information is currently not relevant for a chain, but may
		-- become relevant in the future.
		-- </Remarks>
		----------------------------------------------------------------------
		INSERT INTO dbo.Chain (
			BusinessEntityID,
			SourceChainID,
			Name,
			CreatedBy
		)
		SELECT
			@BusinessEntityID,
			@SourceChainID,
			@Name,
			@CreatedBy
	
	
		----------------------------------------------------------------------
		-- Create new chain address
		----------------------------------------------------------------------	
		-- Create a new address entry (if applicable)
		EXEC spAddress_Create 
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			@CreatedBy = @CreatedBy,
			@AddressID = @AddressID OUTPUT
		
		-- Get the address ID for primary address
		DECLARE @AddressTypeID_Primary INT = dbo.fnGetAddressTypeID('PRIM');
		
		-- Create business address
		EXEC spBusinessEntity_Address_Create
			@BusinessEntityID = @BusinessEntityID,
			@AddressTypeID = @AddressTypeID_Primary,
			@AddressID = @AddressID,
			@CreatedBy = @CreatedBy
			
		-----------------------------------------------------------------------
		-- Create new chain email
		-----------------------------------------------------------------------
		DECLARE 
			@EmailAddressTypeID_Primary INT = dbo.fnGetEmailAddressTypeID('PRIM'),
			@EmailAddressTypeID_Alternate INT = dbo.fnGetEmailAddressTypeID('ALT')
		
		-- Create primary email address
		EXEC spBusinessEntity_Email_Create
			@BusinessEntityID = @BusinessEntityID,
			@EmailAddressTypeID = @EmailAddressTypeID_Primary,
			@EmailAddress = @EmailAddress_Primary,
			@CreatedBy = @CreatedBy


		-- Create alternate email address
		EXEC spBusinessEntity_Email_Create
			@BusinessEntityID = @BusinessEntityID,
			@EmailAddressTypeID = @EmailAddressTypeID_Alternate,
			@EmailAddress = @EmailAddress_Alternate,
			@CreatedBy = @CreatedBy
		
		-----------------------------------------------------------------------
		-- Create new chain phone number
		-----------------------------------------------------------------------
		DECLARE
			@PhoneNumberTypeID_Work INT = dbo.fnGetPhoneNumberTypeID('WRK'),
			@PhoneNumberTypeID_Fax INT = dbo.fnGetPhoneNumberTypeID('FAX')
			
		-- Create new work phone
		EXEC spBusinessEntity_Phone_Create
			@BusinessEntityID = @BusinessEntityID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Work,
			@PhoneNumber = @PhoneNumber_Work,
			@CreatedBy = @CreatedBy
		
		-- Create new fax number
		EXEC spBusinessEntity_Phone_Create
			@BusinessEntityID = @BusinessEntityID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Fax,
			@PhoneNumber = @PhoneNumber_Fax,
			@CreatedBy = @CreatedBy
			
			
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	
	END TRY
	BEGIN CATCH
			
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		EXECUTE [dbo].[spLogError] @ErrorLogID OUTPUT;
		
			
	END CATCH
		
END
