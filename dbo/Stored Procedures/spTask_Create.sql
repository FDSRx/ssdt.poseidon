﻿


-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/2/2014
-- Description:	Creates a new Task object.
-- Change log:
-- 3/4/2016 - dhughes - Modified the procedure to include additional input parameters.

-- SAMPLE CALL:
/*

DECLARE 
	@NoteID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT = 2,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = 135590,
	@NotebookID INT = 4,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(500) = 'Test Poseidon Task: ' + CONVERT(VARCHAR, GETDATE()),
	@Memo VARCHAR(MAX) = 'This is a test task from the task stored procedure #: ' + CONVERT(VARCHAR, RAND()),
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@OriginID INT = 4,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@Tags VARCHAR(MAX) = 'High Risk Meds', -- A comma separated list of tags that identify a note.
	@DateDue DATETIME = NULL,
	@AssignedByID BIGINT = NULL,
	@AssignedByString VARCHAR(256) = NULL,
	@AssignedToID BIGINT = NULL,
	@AssignedToString VARCHAR(256) = NULL,
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusID INT = NULL,
	@TaskStatusCode VARCHAR(50) = NULL,
	@ParentTaskID BIGINT = NULL,
	@CreatedBy VARCHAR(256) = 'Poseidon/dhughes',
	@TaskID BIGINT = NULL,
	@Debug BIT = 1

EXEC dbo.spTask_Create
	@NoteID = @NoteID,
	@ApplicationID = @ApplicationID,
	@ApplicationCode = @ApplicationCode,
	@BusinessID = @BusinessID,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@ScopeID = @ScopeID,
	@ScopeCode = @ScopeCode,
	@BusinessEntityID = @BusinessEntityID,
	@NotebookID = @NotebookID,
	@NotebookCode = @NotebookCode,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityID = @PriorityID,
	@PriorityCode = @PriorityCode,
	@OriginID = @OriginID,
	@OriginCode = @OriginCode,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@Tags = @Tags,
	@DateDue = @DateDue,
	@AssignedByID = @AssignedByID,
	@AssignedByString = @AssignedByString,
	@AssignedToID = @AssignedToID,
	@AssignedToString = @AssignedToString,
	@CompletedByID = @CompletedByID,
	@CompletedByString = @CompletedByString,
	@DateCompleted = @DateCompleted,
	@TaskStatusID = @TaskStatusID,
	@TaskStatusCode = @TaskStatusCode,
	@ParentTaskID = @ParentTaskID,
	@CreatedBy = @CreatedBy,
	@TaskID = @TaskID OUTPUT,
	@Debug = @Debug


SELECT @TaskID AS TaskID

*/

-- SELECT * FROM dbo.Task ORDER BY 1 DESC
-- SELECT * FROM dbo.vwTask ORDER BY 1 DESC
-- SELECT * FROM dbo.TaskStatus
-- SELECT * FROM dbo.Note ORDER BY 1 DESC
-- SELECT * FROM dbo.Tag
-- SELECT * FROM dbo.NoteTag
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook
-- SELECT * FROM dbo.Priority
-- SELECT TOP 10 * FROM phrm.vwPatient WHERE LastName LIKE '%Simmons%'

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spTask_Create]
	@NoteID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@DateDue DATETIME = NULL,
	@AssignedByID BIGINT = NULL,
	@AssignedByString VARCHAR(256) = NULL,
	@AssignedToID BIGINT = NULL,
	@AssignedToString VARCHAR(256) = NULL,
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusID INT = NULL,
	@TaskStatusCode VARCHAR(50) = NULL,
	@ParentTaskID BIGINT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@TaskID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		--'@NoteTypeID=' + dbo.fnToStringOrEmpty(@NoteTypeID) + ';' +
		--'@NoteTypeCode=' + dbo.fnToStringOrEmpty(@NoteTypeCode) + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID) + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID) + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID) + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@DateDue=' + dbo.fnToStringOrEmpty(@DateDue) + ';' +
		'@AssignedByID=' + dbo.fnToStringOrEmpty(@AssignedByID) + ';' +
		'@AssignedByString=' + dbo.fnToStringOrEmpty(@AssignedByString) + ';' +
		'@AssignedToID=' + dbo.fnToStringOrEmpty(@AssignedToID) + ';' +
		'@AssignedToString=' + dbo.fnToStringOrEmpty(@AssignedToString) + ';' +
		'@CompletedByID=' + dbo.fnToStringOrEmpty(@CompletedByID) + ';' +
		'@CompletedByString=' + dbo.fnToStringOrEmpty(@CompletedByString) + ';' +
		'@DateCompleted=' + dbo.fnToStringOrEmpty(@DateCompleted) + ';' +
		'@TaskStatusID=' + dbo.fnToStringOrEmpty(@TaskStatusID) + ';' +
		'@TaskStatusCode=' + dbo.fnToStringOrEmpty(@TaskStatusCode) + ';' +
		'@ParentTaskID=' + dbo.fnToStringOrEmpty(@ParentTaskID) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;	


	-----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
		
	-----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------------------
	SET @NotebookID = COALESCE(dbo.fnGetNotebookID(@NotebookID), dbo.fnGetNotebookID(@NotebookCode), dbo.fnGetNotebookID('MTSKS'));
	SET @TaskStatusID = COALESCE(dbo.fnGetTaskStatusID(@TaskStatusID), dbo.fnGetTaskStatusID(@TaskStatusCode), dbo.fnGetTaskStatusID('NEW'));
	SET @ApplicationID = ISNULL(dbo.fnGetApplicationID(@ApplicationID), dbo.fnGetApplicationID(@ApplicationCode));
	SET @BusinessID = CASE WHEN @BusinessID IS NOT NULL THEN @BusinessID ELSE dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType) END;

	-----------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@NoteTypeID INT = dbo.fnGetNoteTypeID('TSK')
		
	-----------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------------------
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables.' AS ActionMethod,
			@ScopeID AS ScopeID, @NoteTypeID AS NoteTypeID, @ApplicationID AS ApplicationID, @ApplicationCode AS ApplicationCode,
			@BusinessID AS BusinessID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@NotebookID AS NotebookID, @PriorityID AS PriorityID, @OriginID AS OriginID, @TaskStatusID AS TaskStatusID			
	END		
	
	BEGIN TRY
	
		-----------------------------------------------------------------------------------------------------------------------------
		-- Null argument validation
		-- <Summary>
		-- Validates if any of the necessary arguments were provided with
		-- data.
		-- </Summary>
		-----------------------------------------------------------------------------------------------------------------------------
		IF dbo.fnIsNullOrWhiteSpace(@Title) = 1
		BEGIN
			SET @ErrorMessage = 'Unable to create Task object. Object reference (@Title) is not set to an instance of an object. ' +
				'The parameter, @Title, cannot be null or empty.';
			
			RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			  
			 RETURN;
		END	
		
	-----------------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	-----------------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END

	
		-----------------------------------------------------------------------------------------------------------------------------
		-- Create Note object.
		-- <Summary>
		-- If a Note object was not provided, then create a new Note object;
		-- otherwise, we will create a task on the provided Note.
		-- <Summary>
		-- <Remarks>
		-- Tasks are no more than an derived object of Note.
		-- </Remarks> 
		-----------------------------------------------------------------------------------------------------------------------------
		IF @NoteID IS NULL
		BEGIN
			EXEC dbo.spNote_Create
				@ApplicationID = @ApplicationID,
				@ApplicationCode = @ApplicationCode,
				@BusinessID = @BusinessID,
				@BusinessKey = @BusinessKey,
				@BusinessKeyType = @BusinessKeyType,
				@ScopeID = @ScopeID,
				@ScopeCode = @ScopeCode,
				@BusinessEntityID = @BusinessEntityID,
				@NoteTypeID = @NoteTypeID,
				--@NoteTypeCode = @NoteTypeCode,
				@NotebookID = @NotebookID,
				@NotebookCode = @NotebookCode,
				@PriorityID = @PriorityID,
				@PriorityCode = @PriorityCode,
				@Title = @Title,
				@Memo = @Memo,
				@Tags = @Tags,
				@OriginID = @OriginID,
				@OriginCode = @OriginCode,
				@OriginDataKey = @OriginDataKey,
				@AdditionalData = @AdditionalData,
				@CorrelationKey = @CorrelationKey,
				@CreatedBy = @CreatedBy,
				@NoteID = @NoteID OUTPUT,
				@Debug = @Debug
		END
		
		INSERT INTO dbo.Task (
			NoteID,
			ApplicationID,
			BusinessID,
			BusinessEntityID,
			DateDue,
			AssignedByID,
			AssignedByString,
			AssignedToID,
			AssignedToString,
			CompletedByID,
			CompletedByString,
			TaskStatusID,
			ParentTaskID,
			CreatedBy
		)
		SELECT
			@NoteID,
			@ApplicationID,
			@BusinessID,
			@BusinessEntityID,
			@DateDue,
			@AssignedByID,
			@AssignedByString,
			@AssignedToID,
			@AssignedToString,
			@CompletedByID,
			@CompletedByString,
			@TaskStatusID,
			@ParentTaskID,
			@CreatedBy
		
		
		-- Retrieve the record identity.
		SET @TaskID = @NoteID;	
		
		-- Data review
		-- SELECT TOP 1 * FROM dbo.Task		
	
	
	
	-----------------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	-----------------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		-----------------------------------------------------------------------------------------------------------------------------
		-- Set error variables
		-----------------------------------------------------------------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);
		
	END CATCH		

END



