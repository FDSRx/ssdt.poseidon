﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/18/2013
-- Description:	Creates a customer for a business entity.  It is a wrapper around the core person
--				create method.
-- DRH 3/7/2013 - Added phone and email logic for allows (i.e. Allow calls, texts, emails). 
-- =============================================
CREATE PROCEDURE [dbo].[spCustomer_Create]
	--Store
	@BusinessEntityID BIGINT,
	--Person
	@PersonID BIGINT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderID INT = NULL,
	@LanguageID INT = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Alternate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	-- Initiator
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	-- Error
	@ErrorLogID INT = NULL OUTPUT,
	-- Customer 
	@CustomerID BIGINT = NULL OUTPUT,
	@CustomerExists BIT = 0 OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------
	-- Instance variables
	------------------------------------------------------------------------
	DECLARE @Trancount INT = @@TRANCOUNT;
	
	------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------
	SET @ModifiedBy = ISNULL(@ModifiedBy, @CreatedBy);
	SET @CreatedBy = ISNULL(@CreatedBy, @ModifiedBy);
	
		
	------------------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ServiceID INT = dbo.fnGetServiceID('CUSDIR'),
		@PassedPersonID INT = @PersonID -- Reassign person ID
	
	------------------------------------------------------------------------
	-- Set defaults
	------------------------------------------------------------------------
	SET @CustomerID = NULL;
	SET @CustomerExists = 0;	
	SET @PersonID = NULL; -- Need to reset to null (we reassigned above)
	
	
	
	BEGIN TRY
	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		-- DRH - 1/22/2013 - Code block moved to base person create procedure.
		-- Base person will specify the minimum amount of data required to create a person.
		--------------------------------------------------------------------
		-- Determine if the customer data meets the minimum requirements
		--------------------------------------------------------------------
		--IF ISNULL(@FirstName, '') = '' OR ISNULL(@LastName, '') = '' 
		--	OR ISNULL(@BirthDate, '1/1/1900') = '1/1/1900' OR ISNULL(@PostalCode, '') = ''
		--BEGIN
		--	-- insufficient person information to create a customer. A first, last, dob, and postal code is the minimum amount of inormation necessary
		--	-- to create a customer entity
		--	SET @ErrorMessage = 'Insufficient person data collected to create a customer.  A minimum of first and last name, birth date, and postal code is required to create a customer entity. ' +
		--		'Data = BusinessEntityID: ' + CONVERT(VARCHAR, @BusinessEntityID) + '; First Name: ' + ISNULL(@FirstName, '<EMPTY>') +
		--		'; Last Name: ' + ISNULL(@LastName, '<EMPTY>') + '; Birth Date: ' + ISNULL(CONVERT(VARCHAR, @BirthDate, 101), '<EMPTY>') +
		--		'; Postal Code: ' + ISNULL(@PostalCode, '<EMPTY>');
				
		--	RAISERROR (@ErrorMessage, -- Message text.
		--			   16, -- Severity.
		--			   1 -- State.
		--			   );
					  
		--END
		
		--------------------------------------------------------------------
		-- Business Entity Validation
		-- Before we can begin the create process, make sure a non-null BusinessEntityID
		-- was passed
		--------------------------------------------------------------------
		IF ISNULL(@BusinessEntityID, 0) = 0
		BEGIN
			
			SET @ErrorMessage = 'Cannot initialize CUSTOMER create process. A business entity was not supplied.'
				
			RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );	
		END	
	
		--------------------------------------------------------------------
		-- Determine if customer arleady exists
		--------------------------------------------------------------------		
		EXEC spCustomer_Exists
			@BusinessEntityID = @BusinessEntityID,
			@PersonID = @PassedPersonID,
			@FirstName = @FirstName,
			@LastName = @LastName,
			@BirthDate = @BirthDate,
			@GenderID = @GenderID,
			@PostalCode = @PostalCode,
			@CustomerID = @CustomerID OUTPUT,
			@CustomerExists = @CustomerExists OUTPUT
		
		-- Verify a customer does not already exist
		IF ISNULL(@CustomerID, 0) > 0
		BEGIN		
			
			SET @CustomerExists = 1;
			
			--SET @ErrorMessage = CONVERT(VARCHAR, @PersonCnt) + ' instance(s) of this customer were found.  Unable to process request. ' 
			--	+ @FirstName + ' ' + @LastName + ' ' + CONVERT(VARCHAR(10), @BirthDate, 101) + ' ' + @PostalCode
				
			---- Customer exists multiple times.  Do not load them again
			--RAISERROR (@ErrorMessage, -- Message text.
			--		   16, -- Severity.
			--		   1 -- State.
			--		   );
			
			-- Transaction is complete at this point.  Commit and exit code.	
			IF @Trancount = 0
			BEGIN
				COMMIT TRANSACTION;	   
			END
			
			RETURN; -- exit code if we do not need to add a new customer			
		END
		
		--------------------------------------------------------------------
		-- If a person does not already exist and a specific person was passed
		-- then let's add the person ID as the customer instead of creating a new one.
		--------------------------------------------------------------------
		IF ISNULL(@PassedPersonID, 0) > 0
		BEGIN

			-- Verify the person is not already a customer
			SET @CustomerID = (SELECT CustomerID FROM dbo.Customer WHERE BusinessEntityID = @BusinessEntityID AND PersonID = @PassedPersonID);
			
			IF ISNULL(@CustomerID, 0) = 0
			BEGIN
						
				INSERT INTO dbo.Customer (
					BusinessEntityID,
					PersonID,
					CreatedBy
				)
				SELECT 
					@BusinessEntityID,
					@PassedPersonID,
					@CreatedBy
					
				SET @CustomerID = SCOPE_IDENTITY();
				
				--------------------------------------------------------------------
				-- Attach customer service to person
				--------------------------------------------------------------------				
				EXEC dbo.spBusinessEntityService_Merge
					@BusinessEntityID = @PassedPersonID,
					@ServiceID = @ServiceID,
					@CreatedBy = @CreatedBy,
					@ModifiedBy = @ModifiedBy			

			END
			ELSE
			BEGIN						
				SET @CustomerExists = 1;			
			END
			
			-- Transaction is complete at this point.  Commit and exit code.	
			IF @Trancount = 0
			BEGIN
				COMMIT TRANSACTION;	   
			END
			
			RETURN; -- exit code if we do not need to add a new customer	
			-- We exit the code because it can only be one or the other (ID or demographic data) for person creation	
		END
		
	
		--------------------------------------------------------------------
		-- If we have made it this far then let us
		-- create a person entity
		--------------------------------------------------------------------
		EXEC spPerson_Create
			@Title = @Title,
			@FirstName = @FirstName,
			@LastName = @LastName,
			@MiddleName = @MiddleName,
			@Suffix = @Suffix,
			@BirthDate = @BirthDate,
			@GenderID = @GenderID,
			@LanguageID = @LanguageID,
			--Person Address
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			--Person Phone
			--	Home
			@PhoneNumber_Home = @PhoneNumber_Home,
			@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
			@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
			@PhoneNumber_Home_IsPreferred = @PhoneNumber_Home_IsPreferred,
			-- Mobile
			@PhoneNumber_Mobile = @PhoneNumber_Mobile,
			@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
			@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
			@PhoneNumber_Mobile_IsPreferred = @PhoneNumber_Mobile_IsPreferred,
			--Person Email
			--	Primary
			@EmailAddress_Primary = @EmailAddress_Primary,
			@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
			@EmailAddress_Primary_IsPreferred = @EmailAddress_Primary_IsPreferred,
			--	Alternate
			@EmailAddress_Alternate = @EmailAddress_Alternate,
			@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
			@EmailAddress_Alternate_IsPreferred = @EmailAddress_Alternate_IsPreferred,
			--Output
			@PersonID = @PersonID OUTPUT,
			@ErrorLogID = @ErrorLogID OUTPUT
		
		-- If an error was encountered when creating a person then fail
		IF ISNULL(@ErrorLogID, 0) > 0
		BEGIN
			--bubble error
			SELECT @ErrorMessage = ErrorMessage FROM ErrorLog (NOLOCK) WHERE ErrorLogID = @ErrorLogID
			
			SET @ErrorMessage = 'Cannot create CUSTOMER record for BusinessEntityID: ' + ISNULL(CONVERT(VARCHAR, @BusinessEntityID), '<EMPTY>') + '. Reason: '
				+ @ErrorMessage;
				
			RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );		
		END
		
		--PRINT 'PERSON: ' + CONVERT(VARCHAR, ISNULL(@PersonID, 0)) + ' ' + 'ERROR: ' + CONVERT(VARCHAR, ISNULL(@ErrorLogID, 0))
		--------------------------------------------------------------------
		-- Create customer
		--------------------------------------------------------------------
		-- If no errors were encountered when creating a person then add that person to the business entity customer directory
		IF ISNULL(@PersonID, 0) > 0 AND ISNULL(@ErrorLogID, 0) = 0
		BEGIN				
			INSERT INTO dbo.Customer (
				BusinessEntityID,
				PersonID,
				CreatedBy
			)
			SELECT 
				@BusinessEntityID,
				@PersonID,
				@CreatedBy
				
			SET @CustomerID = SCOPE_IDENTITY();				
		END
		
		--------------------------------------------------------------------
		-- Attach customer service to person
		--------------------------------------------------------------------
		EXEC spBusinessEntityService_Merge
			@BusinessEntityID = @PersonID,
			@ServiceID = @ServiceID,
			@CreatedBy = @CreatedBy,
			@ModifiedBy = @ModifiedBy
	
	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END
	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END

		EXECUTE [dbo].[spLogError] @ErrorLogID OUTPUT;
		
	END CATCH;
	
END
