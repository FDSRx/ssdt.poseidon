﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/2/2015
-- Description:	Creates a new BusinessEntityApplication object.
-- SAMPLE CALL:
/*

DECLARE 
	@BusinessEntityID BIGINT,
	@ApplicationID INT,
	@ApplicationPath VARCHAR(512) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityApplicationID BIGINT = NULL,
	@Debug BIT = 1
	
EXEC dbo.spBusinessEntityApplication_Create
	@BusinessEntityID BIGINT = @BusinessEntityID,
	@ApplicationID INT = @ApplicationID,
	@ApplicationPath = @ApplicationPath,
	@CreatedBy = @CreatedBy,
	@BusinessEntityApplicationID = @BusinessEntityApplicationID,
	@Debug = @Debug

SELECT @BusinessEntityApplicationID AS BusinessEntityApplicationID

*/

-- SELECT * FROM dbo.BusinessEntityApplication
-- SElECT * FROM dbo.Application
-- SELECT * FROM dbo.BusinessEntity
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityApplication_Create]
	@BusinessEntityID BIGINT,
	@ApplicationID INT,
	@ApplicationPath VARCHAR(512) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityApplicationID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000)

	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityApplicationID=' + dbo.fnToStringOrEmpty(@BusinessEntityApplicationID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationPath=' + dbo.fnToStringOrEmpty(@ApplicationPath) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	-------------------------------------------------------------------------------------------------------
	-- Record request.
	-------------------------------------------------------------------------------------------------------
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------
	SET @BusinessEntityApplicationID = NULL;
	SET @Debug = ISNULL(@Debug, 0);
	SET @CreatedBy = ISNULL(@CreatedBy, SUSER_NAME());
	
	-------------------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------------------------------
	-- @BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplication object. Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @ApplicationID
	IF @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplication object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
		
	-------------------------------------------------------------------------------------------------------
	-- Create new object.
	-------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.BusinessEntityApplication (		
		BusinessEntityID,
		ApplicationID,
		ApplicationPath,
		CreatedBy
	)
	SELECT
		@BusinessEntityID AS BusinessEntityID,
		@ApplicationID AS ApplicationID,
		@ApplicationPath AS ApplicationPath,
		@CreatedBy AS CreatedBy
	
	-- Retrieve record identity value.
	SET @BusinessEntityApplicationID = SCOPE_IDENTITY();
		
END
