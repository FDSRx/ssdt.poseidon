﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/13/2014
-- Description:	Creates a new tag object.
-- SAMPLE CALL:
/*
DECLARE @TagID BIGINT

EXEC dbo.spTag_Create
	@TagTypeID = 'PHRM,UD',
	@Name = 'PDC - High Risk Medications',
	@CreatedBy = 'dhughes',
	@TagID = @TagID OUTPUT

SELECT @TagID AS TagID
*/
-- SELECT * FROM dbo.Tag
-- SELECT * FROM dbo.TagTagType
-- =============================================
CREATE PROCEDURE [dbo].[spTag_Create]
	@TagTypeID VARCHAR(MAX) = NULL,
	@Name VARCHAR(256),
	@CreatedBy VARCHAR(256) = NULL,
	@TagID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------
	SET @TagID = NULL;
	
	--------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000) = NULL

	--------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------	
	
	--------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	--------------------------------------------------------------
	-- A tag name is required to be created.
	IF LTRIM(RTRIM(ISNULL(@Name, ''))) = ''
	BEGIN
	
		SET @ErrorMessage = 'Unable to create a Tag entry.  Object reference (@Name) is not set to an instance of an object. ' +
			'A tag name cannot be null or empty.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	
	BEGIN TRY
		
		--------------------------------------------------------------
		-- Create new tag object.
		-- <Summary>
		-- Creates a new tag entry and returns its unique row identifier.
		-- </Summary>
		--------------------------------------------------------------
		INSERT INTO dbo.Tag (
			Name,
			CreatedBy
		)
		SELECT
			@Name,
			@CreatedBy
		
		-- Retrieve record identity.
		SET @TagID = SCOPE_IDENTITY();

		--------------------------------------------------------------
		-- Create new Tag/TagType objects.
		-- <Summary>
		-- Creates a new Tag/TagType object reference(s).
		-- </Summary>
		--------------------------------------------------------------	
		IF @TagTypeID IS NOT NULL
		BEGIN
			SET @TagTypeID = dbo.fnTagTypeKeyTranslator(@TagTypeID, DEFAULT);
		END
		
		SET @TagTypeID = ISNULL(@TagTypeID, dbo.fnGetTagTypeID('UD'));

		INSERT INTO dbo.TagTagType (
			TagID,
			TagTypeID,
			CreatedBy
		)
		SELECT
			@TagID AS TagID,
			Value AS TagTypeID,
			@CreatedBy
		FROM dbo.fnSplit(@TagTypeID, ',')
		WHERE ISNUMERIC(Value) = 1
		
	END TRY
	BEGIN CATCH
	
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);
	
	END CATCH
	

	
END
