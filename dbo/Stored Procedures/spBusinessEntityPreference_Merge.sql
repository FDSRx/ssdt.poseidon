﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/20/2015
-- Description:	Updates a BusinessEntityPreference object.
-- SAMPLE CALL;
/*

DECLARE 
	@Exists BIT = NULL

EXEC dbo.spBusinessEntityPreference_Merge
	@BusinessEntityID = 135590,
	@PreferenceID = 1,
	@ValueBoolean = 1,
	@ModifiedBy = 'dhughes',
	@Exists = @Exists OUTPUT

SELECT @Exists AS IsFound

*/

-- SELECT * FROM dbo.BusinessEntityPreference
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityPreference_Merge]
	@BusinessEntityPreferenceID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@PreferenceID INT = NULL OUTPUT,
	@ValueString VARCHAR(MAX) = NULL,
	@ValueBoolean BIT = NULL,
	@ValueNumeric DECIMAL(19, 8) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------
	SET @Exists = 0;
	SET @CreatedBy = ISNULL(@CreatedBy, @ModifiedBy);
	SET @ModifiedBy = ISNULL(@ModifiedBy, @CreatedBy);
	
	
	-----------------------------------------------------------------------------------------------
	-- Determine if the BusinessEntityPreference object exists.
	-----------------------------------------------------------------------------------------------
	EXEC dbo.spBusinessEntityPreference_Exists
		@BusinessEntityPreferenceID = @BusinessEntityPreferenceID OUTPUT,
		@BusinessEntityID = @BusinessEntityID,
		@PreferenceID = @PreferenceID,
		@Exists = @Exists OUTPUT
	
	-----------------------------------------------------------------------------------------------
	-- Merge (create or update) the record.
	-----------------------------------------------------------------------------------------------	
	-- If the object does not exist, then let's create a new one.
	IF @BusinessEntityPreferenceID IS NULL
	BEGIN
	
		EXEC dbo.spBusinessEntityPreference_Create
			@BusinessEntityID = @BusinessEntityID,
			@PreferenceID = @PreferenceID,
			@ValueString = @ValueString,
			@ValueBoolean = @ValueBoolean,
			@ValueNumeric = @ValueNumeric,
			@CreatedBy = @CreatedBy,
			@BusinessEntityPreferenceID = @BusinessEntityPreferenceID OUTPUT
			
	END
	-- If object does exist, then let's update its data.
	ELSE
	BEGIN
	
		EXEC dbo.spBusinessEntityPreference_Update
			@BusinessEntityPreferenceID = @BusinessEntityPreferenceID,
			@BusinessEntityID = @BusinessEntityID,
			@PreferenceID = @PreferenceID,
			@ValueString = @ValueString,
			@ValueBoolean = @ValueBoolean,
			@ValueNumeric = @ValueNumeric,
			@ModifiedBy = @ModifiedBy
	
	END
	
	
	
	

END
