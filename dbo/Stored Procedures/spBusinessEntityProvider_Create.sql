﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/21/2015
-- Description:	Creates a new BusienssEntityServiceProvider object.
-- SAMPLE CALL:
/*

DECLARE
	@BusinessEntityProviderID BIGINT = NULL,
	@BusinessEntityID BIGINT = 38,
	@ProviderID = 1,
	@CreatedBy VARCHAR(256) = 'dhughes'

EXEC dbo.spBusinessEntityProvider_Create
	@BusinessEntityID = @BusinessEntityID,
	@ProviderID = @ProviderID,
	@CreatedBy = @CreatedBy,
	@BusinessEntityProviderID = @BusinessEntityProviderID OUTPUT

SELECT @BusinessEntityProviderID AS BusinessEntityProviderID

*/

-- SELECT * FROM dbo.BusinessEntityProvider
-- =============================================
CREATE PROCEDURE dbo.spBusinessEntityProvider_Create
	@BusinessEntityID BIGINT,
	@ProviderID INT = NULL,
	@ProviderCode VARCHAR(25) = NULL,
	@Username VARCHAR(256) = NULL,
	@PasswordUnencrypted VARChAR(256) = NULL,
	@PasswordEncrypted NVARCHAR(256) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityProviderID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	----------------------------------------------------------------------------------------------------------
	-- Instance variables.
	----------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	----------------------------------------------------------------------------------------------------------
	-- Log request.
	----------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@ProviderID=' + dbo.fnToStringOrEmpty(@ProviderID) + ';' +
		'@ProviderCode=' + dbo.fnToStringOrEmpty(@ProviderCode) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@PasswordUnencrypted=' + dbo.fnToStringOrEmpty(@PasswordUnencrypted) + ';' +
		'@PasswordEncrypted=' + dbo.fnToStringOrEmpty(@PasswordEncrypted) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@PasswordEncrypted) + ';' +
		'@BusinessEntityProviderID=' + dbo.fnToStringOrEmpty(@BusinessEntityProviderID) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	----------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------------------------------
	SET @BusinessEntityProviderID = NULL;


	----------------------------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	----------------------------------------------------------------------------------------------------------
	-- @MessageTypeID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @CommunicationMethodID
	IF @ProviderID IS NULL AND ( @ProviderCode IS NULL OR @ProviderCode = '' )
	BEGIN
		SET @ErrorMessage = 'Object reference (@ProviderID) is not set to an instance of an object. ' +
			'The parameter, @ProviderID or @ProviderCode, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	


	----------------------------------------------------------------------------------------------------------
	-- Set variables.
	----------------------------------------------------------------------------------------------------------
	-- ProviderID
	SET @ProviderID = dbo.fnGetApplicationID(@ProviderID);
	SET @ProviderID = CASE WHEN @ProviderID IS NOT NULL THEN @ProviderID ELSE dbo.fnGetApplicationID(@ProviderCode) END;


	----------------------------------------------------------------------------------------------------------
	-- Create object.
	----------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.BusinessEntityProvider (
		BusinessEntityID,
		ProviderID,
		Username,
		PasswordUnencrypted,
		PasswordEncrypted,
		CreatedBy
	)
	SELECT
		@BusinessEntityID AS BusinessEntityID,
		@ProviderID AS ProviderID,
		@Username AS Username,
		@PasswordUnencrypted AS PasswordUnencrypted,
		@PasswordEncrypted AS PasswordEncrypted,
		@CreatedBy AS CreatedBy

	-- Retrieve object identity.
	SET @BusinessEntityProviderID = SCOPE_IDENTITY();

END
