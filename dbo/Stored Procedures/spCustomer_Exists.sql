﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/15/2013
-- Description:	Determines if a customer already exists
-- SAMPLE CALL: 
/*

DECLARE @PersonID INT, @CustomerID INT, @CustomerExists BIT

EXEC spCustomer_Exists
	@BusinessEntityID = 277,
	--@PersonID = '60753',
	@FirstName = 'Daniel', 
	@LastName = 'Hughes', 
	@BirthDate = '3/14/1982', 
	@CustomerExists = @CustomerExists OUTPUT,
	@CustomerID = @CustomerID OUTPUT
	
SELECT @CustomerID AS CustomerID, @CustomerExists AS CustomerExists

*/

-- =============================================
CREATE PROCEDURE [dbo].[spCustomer_Exists]
	@BusinessEntityID BIGINT,
	@PersonID BIGINT = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@PostalCode VARCHAR(25) = NULL,
	@BirthDate DATE = NULL,
	@GenderID INT = NULL,
	@CustomerID INT = NULL OUTPUT,
	@CustomerExists BIT = 0 OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------
	-- Local variables
	---------------------------------------------
	DECLARE
		@PersonCnt INT = 0
	
	---------------------------------------------
	-- Set Defaults
	---------------------------------------------
	SET @CustomerID = NULL;
	SET @CustomerExists = 0;

		
	
	----------------------------------------------------------------------
	-- Verification process
	-- Rules engine:
	-- If a PersonID was supplied then we need to validate that the person id is not already
	--		being used in the customer table
	-- If a PersonID was supplied then we need to validate that it is a real person
	--		and use the credentials from the person ID to determine if the person already exists
	--		under another ID
	-- If no person ID was supplied then we use the demographic data passed
	-- Perform routine lookup on customer data to determine if the customer already exists
	----------------------------------------------------------------------
	
	----------------------------------------------------------------------
	-- Determine if a customer already exists under the current business
	-- and person
	----------------------------------------------------------------------
	SELECT
		@CustomerID = CustomerID
	--SELECT TOP 1 *
	FROM dbo.Customer
	WHERE BusinessEntityID = @BusinessEntityID
		AND PersonID = @PersonID
	
	
	IF ISNULL(@CustomerID, 0) > 0
	BEGIN

		SET @CustomerExists = 1;
		
		-- Customer already exists.  We have all the data (output values) we need.  We can exit the program.
		RETURN;
	END
	
	
	----------------------------------------------------------------------
	-- If a customer was not found then we need to see if we need to use
	-- the demographic data passed or the person ID passed.
	-- Person ID wins over demographic
	----------------------------------------------------------------------
	IF @PersonID IS NOT NULL
	BEGIN
		
		IF EXISTS(SELECT TOP 1 * FROM dbo.Person WHERE BusinessEntityID = @PersonID)
		BEGIN

			-- destroy any passed variables as an ID wins every time
			SET @FirstName = NULL;
			SET @LastName = NULL;
			SET @PostalCode = NULL;
			SET @BirthDate = NULL;
			SET @GenderID = NULL;
			
			-- Let's go ahead and pull the person data
			SELECT
				@FirstName = FirstName,
				@LastName = LastName,
				@BirthDate = BirthDate,
				@PostalCode = PostalCode,
				@GenderID = (SELECT GenderID FROM dbo.Gender WHERE Name = psn.Gender)
			FROM dbo.vwPerson psn
			WHERE PersonID = @PersonID

		END
		ELSE
		BEGIN
			-- An invalid person ID was passed. We need to exit the code.  We cannot guarentee the demographic data passed
			-- is enough.  It should be only one or other other.
			
			SET @CustomerExists = 0;
			
			RETURN;
			
		END
		
	END
	
	-- Debug
	--SELECT @FirstName, @LastName, @PostalCode, @BirthDate, @GenderID 
	
	--------------------------------------------------------------	
	-- Perform routine cusotmer lookup based on demographic data
	-- Currently, customer only looks at First, Last, Birthdate
	--------------------------------------------------------------
	SELECT TOP 1
		@CustomerID = cus.CustomerID,
		@PersonID = psn.BusinessEntityID
	FROM dbo.Customer cus
		JOIN dbo.Person psn
			ON cus.PersonID = psn.BusinessEntityID
	WHERE cus.BusinessEntityID = @BusinessEntityID
		AND psn.FirstName = @FirstName
		AND psn.LastName = @LastName
		AND psn.BirthDate = @BirthDate
		--AND adr.PostalCode = @PostalCode
	
	-- Determine if the customer exists
	IF ISNULL(@CustomerID, 0) > 0
	BEGIN		
		
		SET @CustomerExists = 1;		
 
		RETURN; -- exit code if we do not need to add a new customer			
	END
	
	
END
