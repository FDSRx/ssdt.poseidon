﻿

-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/14/2012
-- Description:	Track user requests and determine if they have tried to many attempts.
-- SAMPLE CALL: 
/*

DECLARE
	@IsLocked BIT,
	@DateExpires DATETIME,
	@RequestCount INT,
	@RequestsRemaining INT
	
EXEC dbo.spRequestTracker_TrackWithLockOut
	@RequestTypeID = 2,
	@SourceName = 'dbo.spRequestTracker_Create',
	@ApplicationKey = 'MPC',
	@BusinessKey = '000000',
	@RequesterKey = 'dhughes',
	@RequestKey = 'MPC-000000-dhughes',
	@RequestsRemaining = @RequestsRemaining OUTPUT, 
	@RequestCount = @RequestCount OUTPUT,
	@IsLocked = @IsLocked OUTPUT,
	@DateExpires = @DateExpires OUTPUT,
	@CreatedBy = 'dhughes'

SELECT @IsLocked AS IsLocked, @DateExpires AS DateExpires, @RequestCount AS RequestCount, @RequestsRemaining AS RequestsRemaining
	
*/
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- SELECT * FROM dbo.RequestTracker
-- TRUNCATE TABLE dbo.RequestTracker
-- =============================================
CREATE PROCEDURE [dbo].[spRequestTracker_TrackWithLockOut]
	@RequestTypeID INT = NULL,
	@SourceName VARCHAR(256) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@RequesterKey VARCHAR(50) = NULL,
	@RequestKey VARCHAR(255),
	@MaxRequestsAllowed INT = NULL OUTPUT,
	@RequestCount INT = NULL OUTPUT,
	@RequestsRemaining INT = NULL OUTPUT,
	@IsLocked BIT = NULL OUTPUT,
	@DateExpires DATETIME = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@RequesterID BIGINT = NULL,
	@RequestData VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@RequestTrackerID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug (raw request)
	--SELECT @ApplicationKey AS ApplicationKey, @BusinessKey AS BusinessKey, @RequesterKey AS RequesterKey,
	--	@RequestKey AS RequestKey, @ApplicationID AS ApplicationID, @BusinessEntityID AS BusinessEntityID,
	--	@RequesterID AS RequesterID, @CreatedBy AS CreatedBy, @ModifiedBy AS ModifiedBy
		
	--------------------------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------------------------
	SET @RequestTrackerID = NULL;
	SET @RequestCount = NULL;
	SET @IsLocked = 0;
	SET @DateExpires = NULL;
	SET @RequestsRemaining = NULL;
	SET @ModifiedBy = ISNULL(@ModifiedBy, @CreatedBy);
	SET @CreatedBy = ISNULL(@CreatedBy, @ModifiedBy);
	
		
	--------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------
	DECLARE 
		@IsKeyFound BIT = 0,
		@ExpirationDurationMinutes INT
	
	
	BEGIN TRY
	
	--------------------------------------------------------------------------------------------
	-- Set variables/defaults
	--------------------------------------------------------------------------------------------
	-- incase default is not found set to 20 minutes.
	SET @ExpirationDurationMinutes = ISNULL(dbo.fnGetAppBusinessConfigValue(@ApplicationID, @BusinessEntityID, 'RequestKeyExpirationMin'), 20); 
	SET @RequestKey = LTRIM(RTRIM(@RequestKey))
	-- Get defined max attempts
	SET @MaxRequestsAllowed = ISNULL(dbo.fnGetAppBusinessConfigValue(@ApplicationID, @BusinessEntityID, 'RequestAttemptsMax'), 3); 
	
	-- Debug
	--SELECT 'Set Variables/Defaults' AS ExecutionStep, @ExpirationDurationMinutes AS ExpirationDurationMinutes, 
	--	@MaxRequestsAllowed AS MaxRequestsAllowed
	
	--------------------------------------------------------------------------------------------
	-- Remove all expired keys.
	-- <Summary>
	-- Removes all requests that have expired.
	-- </Summary.
	--------------------------------------------------------------------------------------------
	EXEC spRequestTracker_Delete;
	
	--------------------------------------------------------------------------------------------
	-- Try and grab the key.
	-- <Summary>
	-- Trys to locate the supplied "key" (unique row) and return its request information.
	-- </Summary>
	--------------------------------------------------------------------------------------------
	DECLARE @MaxRequestsAllowed_Out INT
	
	EXEC dbo.spRequestTracker_Get
		@RequestTrackerID = @RequestTrackerID OUTPUT,
		@ApplicationKey = @ApplicationKey,
		@BusinessKey = @BusinessKey,
		@RequesterKey = @RequesterKey,
		@RequestKey = @RequestKey,
		@IsLocked = @IsLocked OUTPUT,
		@DateExpires = @DateExpires OUTPUT,
		@RequestsRemaining = @RequestsRemaining OUTPUT,
		@RequestCount = @RequestCount OUTPUT,
		@MaxRequestsAllowed = @MaxRequestsAllowed_Out OUTPUT
	
	-- Indicate whether the key was successfully found.
	SET @IsKeyFound = CASE WHEN @RequestTrackerID IS NOT NULL THEN 1 ELSE 0 END;
	-- If a pre-defined MaxRequest value was found, then used the pre-defined version;
	-- otherwise, use our definition of max requests.
	SET @MaxRequestsAllowed = ISNULL(@MaxRequestsAllowed_Out, @MaxRequestsAllowed);
	
	-- Debug
	--SELECT @RequestTrackerID AS RequestTrackerID, @IsKeyFound AS IsKeyFound, @IsLocked AS IsLocked, 
	--	@DateExpires AS DateExpires, @RequestCount AS RequestCount, @RequestsRemaining AS RequestsRemaining, 
	--	@MaxRequestsAllowed AS MaxRequestsAllowed
	
	IF ISNULL(@IsKeyFound, 0) > 0
	BEGIN
	----------------------------------------------------- Key found setion start ------------------------------------------------------------
	
		-- Increment request attempts
		SET @RequestCount = @RequestCount + 1

		IF @IsLocked = 1
		BEGIN
		----------------------------------------------------- locked setion start ------------------------------------------------------------
			
			-- Add seconds to the clock incase a bot is peppering the site.
			SET @DateExpires = DATEADD(ss, 30, ISNULL(@DateExpires, GETDATE()))
			
			-- Debug
			--SELECT 'Track & Lock' AS ExecutionStep,  @RequestTrackerID AS RequestTrackerID, @IsLocked AS IsLocked, @DateExpires AS DateExpires,
			--	@NumberOfAttempts AS RequestCount, @RequestsRemaining AS RequestsRemaining, @MaxRequestsAllowed AS MaxRequestsAllowed 
			
			-- Update core data.
			EXEC dbo.spRequestTracker_Update
				@RequestTrackerID = @RequestTrackerID,
				@RequestCount = @RequestCount,
				@DateExpires = @DateExpires,
				@ModifiedBy = @ModifiedBy
		
		
		----------------------------------------------------- locked setion end --------------------------------------------------------------
		END
		ELSE
		BEGIN
		----------------------------------------------------- update setion start ------------------------------------------------------------
						
			-- Determine if request has breached threshold
			SET @IsLocked = CASE WHEN @RequestCount >= @MaxRequestsAllowed THEN 1 ELSE 0 END;
			SET @RequestsRemaining = CASE WHEN @RequestsRemaining <= 0 THEN @RequestsRemaining ELSE @RequestsRemaining - 1 END;
			SET @DateExpires = DATEADD(mi, @ExpirationDurationMinutes, GETDATE());
			
			-- Debug
			--SELECT 'Track' AS ExecutionStep, @RequestTrackerID AS RequestTrackerID, @IsLocked AS IsLocked, @DateExpires AS DateExpires,
			--	@NumberOfAttempts AS RequestCount, @RequestsRemaining AS RequestsRemaining, @MaxRequestsAllowed AS MaxRequestsAllowed 
			
			-- Update core data.
			EXEC dbo.spRequestTracker_Update
				@RequestTrackerID = @RequestTrackerID,
				@RequestCount = @RequestCount,
				@RequestsRemaining = @RequestsRemaining,
				@IsLocked = @IsLocked,
				@DateExpires = @DateExpires,
				@ModifiedBy = @ModifiedBy			
	
			
		----------------------------------------------------- update setion end ------------------------------------------------------------	
		END	
	----------------------------------------------------- Key found setion end ------------------------------------------------------------	
	END
	ELSE
	BEGIN
	----------------------------------------------------- Not found setion start ------------------------------------------------------------
		
		SET @RequestsRemaining = @MaxRequestsAllowed - 1;
		
		-- First time creating a key. Let the caller know the request has not been locked.
		SET @RequestCount = 1;
		SET @IsLocked = 0;
		SET @DateExpires = DATEADD(mi, @ExpirationDurationMinutes, GETDATE());
		
		-- Debug
		--SELECT 'Create' AS ExecutionStep, @RequestTrackerID AS RequestTrackerID, @IsLocked AS IsLocked, @DateExpires AS DateExpires,
		--		@NumberOfAttempts AS RequestCount, @RequestsRemaining AS RequestsRemaining, @MaxRequestsAllowed AS MaxRequestsAllowed 
			
			
		-- Create RequestTracker object.
		EXEC dbo.spRequestTracker_Create
			@RequestTypeID = @RequestTypeID,
			@SourceName = @SourceName,
			@ApplicationKey = @ApplicationKey,
			@BusinessKey = @BusinessKey,
			@RequesterKey = @RequesterKey,
			@RequestKey = @RequestKey,
			@RequestCount = @RequestCount,
			@RequestsRemaining = @RequestsRemaining,
			@MaxRequestsAllowed = @MaxRequestsAllowed,
			@IsLocked = @IsLocked,
			@DateExpires = @DateExpires,
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessEntityID,
			@RequesterID = @RequesterID,
			@RequestData = @RequestData,
			@Arguments = @Arguments,
			@CreatedBy = @CreatedBy,
			@RequestTrackerID = @RequestTrackerID OUTPUT

		
	----------------------------------------------------- Not found setion end --------------------------------------------------------------
	END
	
	END TRY
	BEGIN CATCH
	
		EXECUTE spLogError;
	
	END CATCH
	
END









