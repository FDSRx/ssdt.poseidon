﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/3/2015
-- Description:	Deletes a single or list of CalendarItem objects.
-- SAMPLE CALL:
/*

DECLARE
	@NoteID VARCHAR(MAX) = '59799', -- A single or comma delimited list of Note object identifiers.
	@ModifiedBy VARCHAR(256) = 'dhughes'

EXEC dbo.spCalendarItem_DeleteRange
	@NoteID = @NoteID,
	@ModifiedBy = @ModifiedBy

*/

-- SELECT * FROM dbo.CalendarItem ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spCalendarItem_DeleteRange]
	@NoteID VARCHAR(MAX), -- A single or comma delimited list of Note object identifiers.
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);


	------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';'
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	

	
	------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)


	------------------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	------------------------------------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpItems') IS NOT NULL
	BEGIN
		DROP TABLE #tmpItems;
	END
	
	CREATE TABLE #tmpItems (
		NoteID BIGINT
	);
	
			
	------------------------------------------------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	------------------------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy



	------------------------------------------------------------------------------------------------------------------------
	-- Parse task item keys.
	------------------------------------------------------------------------------------------------------------------------
	INSERT INTO #tmpItems (
		NoteID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@NoteID, ',')
	WHERE ISNUMERIC(Value) = 1


	------------------------------------------------------------------------------------------------------------------------
	-- Prepare cursor to walk through task records.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@NoteID_Cur BIGINT = NULL
		
	DECLARE curObjs CURSOR FAST_FORWARD
	FOR 
	
		SELECT 
			DISTINCT NoteID
		FROM #tmpItems
		ORDER BY NoteID ASC

	OPEN curObjs

	FETCH NEXT FROM curObjs
	INTO 
		@NoteID_Cur

	WHILE @@FETCH_STATUS = 0
	BEGIN
	------------------------------------------------------------------------------------------------------------------------
					

		------------------------------------------------------------------------------------------------------------------------
		-- Begin data transaction.
		------------------------------------------------------------------------------------------------------------------------	
		BEGIN TRY
		IF @Trancount = 0
		BEGIN
			BEGIN TRANSACTION;
		END	
		
			------------------------------------------------------------------------------------------------------------------------
			-- Remove calendar item.
			------------------------------------------------------------------------------------------------------------------------	
			EXEC dbo.spCalendarItem_Delete
				@NoteID = @NoteID_Cur,
				@ModifiedBy = @ModifiedBy


		------------------------------------------------------------------------------------------------------------------------
		-- End data transaction.
		------------------------------------------------------------------------------------------------------------------------	
		IF @Trancount = 0
		BEGIN
			COMMIT TRANSACTION;
		END	
		END TRY
		BEGIN CATCH
		
			-- Rollback any active or uncommittable transactions before
			-- inserting information in the ErrorLog
			IF @Trancount = 0 AND @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION;
			END
			
			-- Log transaction error.	
			EXECUTE [dbo].[spLogError];
			
			DECLARE
				@ErrorSeverity INT = NULL,
				@ErrorState INT = NULL,
				@ErrorNumber INT = NULL,
				@ErrorLine INT = NULL,
				@ErrorProcedure  NVARCHAR(200) = NULL

			------------------------------------------------------------------------------------------------------------------------
			-- Set error variables
			------------------------------------------------------------------------------------------------------------------------
			SELECT 
				@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
				@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
				@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
				@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
				@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
				@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

			
			SELECT @ErrorMessage = 
				N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
					'Message: '+ ERROR_MESSAGE();

			RAISERROR (
				@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState,               
				@ErrorNumber,    -- parameter: original error number.
				@ErrorSeverity,  -- parameter: original error severity.
				@ErrorState,     -- parameter: original error state.
				@ErrorProcedure, -- parameter: original error procedure name.
				@ErrorLine       -- parameter: original error line number.
			);
			
		END CATCH


	------------------------------------------------------------------------------------------------------------------------
	-- Retrieve next record object.
	------------------------------------------------------------------------------------------------------------------------
	FETCH NEXT FROM curObjs
	INTO 
		@NoteID_Cur		
	END 
	
	-- Dispose of cursor.
	CLOSE curObjs;
	DEALLOCATE curObjs;
	------------------------------------------------------------------------------------------------------------------------


	------------------------------------------------------------------------------------------------------------------------
	-- Dispose of objects.
	------------------------------------------------------------------------------------------------------------------------
	DROP TABLE #tmpItems;
	
	
	
END
