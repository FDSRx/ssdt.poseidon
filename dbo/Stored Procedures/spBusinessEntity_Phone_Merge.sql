﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/12/1982
-- Description:	Adds or updates the business entity address record.
-- SAMPLE CALL:
/*

spBusinessEntity_Phone_Merge
	@BusinessEntityPhoneNumberID = NULL,
	@BusinessEntityID = 135790,
	@PhoneNumberTypeID = 1,		
	@PhoneNumber = '555-555-5555', -- 9738654117 -- Orginal number
	@IsCallAllowed = 0,
	@IsTextAllowed = 0,
	@ModifiedBy = 'dhughes@rxlps.com'

spBusinessEntity_Phone_Merge
	@BusinessEntityPhoneNumberID = 71095,
	@BusinessEntityID = 135790,
	@PhoneNumberTypeID = 1,		
	@PhoneNumber = '9738654117', -- 9738654117 -- Orginal number
	@IsCallAllowed = 0,
	@IsTextAllowed = 1,
	@ModifiedBy = 'dhughes@rxlps.com'
		
*/
-- SELECT * FROM dbo.BusinessEntityPhone WHERE BusinessEntityPhoneNumberID = 71095
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Phone_Merge]
	@BusinessEntityPhoneNumberID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@PhoneNumberTypeID INT = NULL,
	@PhoneNumber VARCHAR(255) = NULL,
	@IsCallAllowed BIT = 0,
	@IsTextAllowed BIT = 0,
	@IsPreferred BIT = 0,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);
	SET @IsCallAllowed = ISNULL(@IsCallAllowed, 0);
	SET @IsTextAllowed = ISNULL(@IsTextAllowed, 0);
	SET @IsPreferred = ISNULL(@IsPreferred, 0);
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidateSurrogateKey BIT = 0
		

	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------	
	IF @BusinessEntityPhoneNumberID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @PhoneNumberTypeID = NULL;
		
		SET @ValidateSurrogateKey = 1;
	END
	
	----------------------------------------------------------------------
	-- Argument Null Exceptions
	----------------------------------------------------------------------
	----------------------------------------------------------------------
	-- Surrogate key validation.
	-- <Summary>
	-- If a surrogate key was provided, then that will be the only lookup
	-- performed.
	-- <Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityPhoneNumberID, 0) = 0 AND @ValidateSurrogateKey = 1
	BEGIN			
		
		SET @ErrorMessage = 'Object reference not set to an instance of an object. The object, @BusinessEntityPhoneNumberID, cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	----------------------------------------------------------------------
	-- Unique row key validation.
	-- <Summary>
	-- If a surrogate key was not provided then we will try and perform the
	-- lookup based on the records unique key.
	-- </Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0 AND ISNULL(@PhoneNumberTypeID, 0) = 0 AND @ValidateSurrogateKey = 0
	BEGIN
		
		SET @ErrorMessage = 'Object references not set to an instance of an object. The objects, @BusinessEntityID and @PhoneNumberTypeID cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END
	
	--------------------------------------------------------------------
	-- Determine if record exists
	--------------------------------------------------------------------
	EXEC dbo.spBusinessEntity_Phone_Exists
		@BusinessEntityPhoneNumberID = @BusinessEntityPhoneNumberID OUTPUT,
		@BusinessEntityID = @BusinessEntityID,
		@PhoneNumberTypeID = @PhoneNumberTypeID,
		@Exists = @Exists OUTPUT
		
	----------------------------------------------------------------------
	-- Merge data
	-- <Summary>
	-- If the record does not exist, then add the record; otherwise,
	-- update the record
	-- <Summary>
	----------------------------------------------------------------------	
	IF ISNULL(@Exists, 0) = 0
	BEGIN
		-- Add record
		EXEC dbo.spBusinessEntity_Phone_Create
			@BusinessEntityID = @BusinessEntityID,
			@PhoneNumberTypeID = @PhoneNumberTypeID,
			@PhoneNumber = @PhoneNumber,
			@IsCallAllowed = @IsCallAllowed,
			@IsTextAllowed = @IsTextAllowed,
			@IsPreferred = @IsPreferred,
			@CreatedBy = @CreatedBy,
			@BusinessEntityPhoneNumberID = @BusinessEntityPhoneNumberID OUTPUT
	END
	ELSE
	BEGIN
		-- Update record
		EXEC dbo.spBusinessEntity_Phone_Update
			@BusinessEntityPhoneNumberID = @BusinessEntityPhoneNumberID OUTPUT,
			@BusinessEntityID = @BusinessEntityID,
			@PhoneNumberTypeID = @PhoneNumberTypeID,		
			@PhoneNumber = @PhoneNumber,
			@IsCallAllowed = @IsCallAllowed,
			@IsTextAllowed = @IsTextAllowed,
			@IsPreferred = @IsPreferred,
			@ModifiedBy = @ModifiedBy
	END


	
END
