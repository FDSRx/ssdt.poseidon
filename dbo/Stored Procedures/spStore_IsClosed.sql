﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 5/18/2016
-- Description:	Determines if the store is cloesd is on the provided date.
-- SAMPLE CALL:
/*

DECLARE
	@BusinessID BIGINT = 38,
	@Date DATETIME = '6/6/2016',
	@IsClosed BIT = NULL,
	@ResultSet VARCHAR(25) = NULL,
	@Debug BIT = NULL

EXEC dbo.spStore_IsClosed
	@BusinessID = @BusinessID,
	@Date = @Date,
	@IsClosed = @IsClosed OUTPUT,
	@ResultSet = @ResultSet,
	@Debug = @Debug


SELECT @IsClosed AS IsClosed

*/

-- SELECT * FROM dbo.Store WHERE BusinessEntityID = 38
-- =============================================
CREATE PROCEDURE [dbo].[spStore_IsClosed]
	@BusinessID BIGINT,
	@Date DATETIME,
	@IsClosed BIT = NULL OUTPUT,
	@ResultSet VARCHAR(25) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-------------------------------------------------------------------------------------------------------------
	-- Instance variables
	-------------------------------------------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ErrorMessage VARCHAR(4000)

	
	-------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @IsClosed = 0;
	SET @ResultSet = ISNULL(@ResultSet, 'OUT');
	
	-------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------------------------
	-- No declarations required.
		
	
	-------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------------------------
	-- No initialization required.
	
	-------------------------------------------------------------------------------------------------------------
	-- Determine if the store is open on the provided day.
	-------------------------------------------------------------------------------------------------------------
	SET @IsClosed = dbo.fnIsStoreClosedDate(@BusinessID, @Date);


	-------------------------------------------------------------------------------------------------------------
	-- Determine if the store is open on the provided day.
	-------------------------------------------------------------------------------------------------------------
	IF @ResultSet IN ('SINGLE', 'FULL')
	BEGIN
		SELECT @IsClosed AS IsClosed
	END
	

	
	
END
