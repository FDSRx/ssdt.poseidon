﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/15/2013
-- Description: Throws a log error
-- =============================================
CREATE PROCEDURE [dbo].[spTryThrowUncommitableWriteError]
	@ErrorMessage NVARCHAR(4000) = NULL,
	@ErrorSeverity INT = NULL,
	@ErrorState INT = NULL,
	@ErrorNumber INT = NULL,
	@ErrorLine INT = NULL,
	@ErrorProcedure  NVARCHAR(200) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------
	DECLARE
		@InnerException VARCHAR(4000),
		@SourceName VARCHAR(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	
	--------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------
	SELECT 
		@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
		@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
		@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
		@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
		@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
		@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');
		

	--------------------------------------------------------------------
	-- Raise Error
	-- <Summary>
	-- Check the XACT_STATE() of the transaction to determine if the error
	-- needs to be thrown. 
	-- </Summary>
	--------------------------------------------------------------------
	IF XACT_STATE() = -1
	BEGIN
	
		SET @InnerException = 'The current transaction cannot be committed and cannot support ' +
			'operations that write to the log file. Roll back the transaction.'
			
		SET @ErrorMessage = 'Error Pocedure: ' + ISNULL(@ErrorProcedure, '') + '.		' +
			CASE
				WHEN ISNULL(@ErrorMessage, '') = @InnerException THEN @ErrorMessage
				WHEN ISNULL(@ErrorMessage, '') <> '' THEN @ErrorMessage + '		Inner Exception: ' + @InnerException
				ELSE @ErrorMessage
			END
			
	RAISERROR 
		(
		@ErrorMessage, 
		@ErrorSeverity, 
		@ErrorState,               
		@ErrorNumber,    -- parameter: original error number.
		@ErrorSeverity,  -- parameter: original error severity.
		@ErrorState,     -- parameter: original error state.
		@ErrorProcedure, -- parameter: original error procedure name.
		@ErrorLine       -- parameter: original error line number.
		);
		
	END

END
