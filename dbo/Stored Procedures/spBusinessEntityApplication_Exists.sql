﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/2/2015
-- Description:	Determines if the BusinessEntityApplication object exists.
-- SAMPLE CALL:
/*

DECLARE 
	@BusinessEntityApplicationID BIGINT = NULL,
	@Exists BIT = NULL
	
	
EXEC dbo.spBusinessEntityApplication_Exists
	@BusinessEntityID = 10684,
	@ApplicationID = 10,
	@BusinessEntityApplicationID = @BusinessEntityApplicationID OUTPUT,
	@Exists = @Exists OUTPUT

SELECT @BusinessEntityApplicationID AS BusinessEntityApplicationID, @Exists AS IsFound

*/

-- SElECT * FROM dbo.Application
-- SELECT * FROM dbo.BusinessEntity
-- SELECT * FROM dbo.BusinessEntityApplication
-- =============================================
CREATE PROCEDURE dbo.[spBusinessEntityApplication_Exists]
	@BusinessEntityID BIGINT,
	@ApplicationID INT,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityApplicationID BIGINT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------
	DECLARE @ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';'
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';'
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';'
		'@BusinessEntityApplicationID=' + dbo.fnToStringOrEmpty(@BusinessEntityApplicationID) + ';'
		'@Exists=' + dbo.fnToStringOrEmpty(@Exists) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	--SET @BusinessEntityApplicationID = NULL;
	SET @Exists = 0;
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)

	/*
	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplicationID object. Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @ApplicationID
	IF @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplicationID object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/

	-------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------
	IF @BusinessEntityApplicationID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @ApplicationID = NULL;		
	END
			
	-------------------------------------------------------------------------------
	-- Determine if the object exists.
	-------------------------------------------------------------------------------
	SELECT TOP 1
		@BusinessEntityID = BusinessEntityID,
		@ApplicationID = ApplicationID,
		@BusinessEntityApplicationID = BusinessEntityApplicationID
	FROM dbo.BusinessEntityApplication obj
	WHERE BusinessEntityApplicationID = @BusinessEntityApplicationID
		OR (
			BusinessEntityID = @BusinessEntityID
			AND ApplicationID = @ApplicationID
		)
	
	IF @@ROWCOUNT = 1
	BEGIN		
		SET @Exists = 1;		
	END
	ELSE
	BEGIN
		SET @BusinessEntityApplicationID = NULL;
	END
		
		

		
END
