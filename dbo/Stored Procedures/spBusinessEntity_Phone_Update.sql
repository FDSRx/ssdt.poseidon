﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/17/2013
-- Description:	Updates a business entity's phone information
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Phone_Update]
	@BusinessEntityPhoneNumberID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@PhoneNumber VARCHAR(25) = NULL,
	@PhoneNumberTypeID INT = NULL,
	@IsCallAllowed BIT = NULL,
	@IsTextAllowed BIT = NULL,
	@IsPreferred BIT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------
	-- Sanitize input.
	--------------------------------------------------------------------------
	SET @PhoneNumber = CASE WHEN LTRIM(RTRIM(ISNULL(dbo.fnRemoveNonNumericChars(@PhoneNumber), ''))) = '' THEN NULL ELSE dbo.fnRemoveNonNumericChars(@PhoneNumber) END;

	--------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------
	DECLARE @HashKey VARCHAR(256)

	--------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------	
	-- Create hash key.
	SET @HashKey = dbo.fnCreateRecordHashKey( 
		dbo.fnToHashableString(@BusinessEntityID) + 
		dbo.fnToHashableString(@PhoneNumberTypeID) +
		dbo.fnToHashableString(@PhoneNumber) +
		dbo.fnToHashableString(@IsCallAllowed) +
		dbo.fnToHashableString(@IsTextAllowed) +
		dbo.fnToHashableString(@IsPreferred)
	);	

	-----------------------------------------------------------------------------
	-- Temporary resources
	-----------------------------------------------------------------------------
	DECLARE
		@tblOutput AS TABLE (BusinessEntityPhoneNumberID BIGINT)
		
	--------------------------------------------------------------------------
	-- Update data.
	--------------------------------------------------------------------------			
	-- If the surrogate record identifier was provided then use the row identifer; otherwise, use the
	-- BusinessEntityID/EmailAddressType combination.
	IF @BusinessEntityPhoneNumberID IS NOT NULL
	BEGIN
	
		UPDATE TOP(1) phn
		SET
			phn.PhoneNumber = CASE WHEN @PhoneNumber IS NOT NULL THEN @PhoneNumber ELSE phn.PhoneNumber END,
			phn.PhoneNumberTypeID = CASE WHEN @PhoneNumberTypeID IS NOT NULL THEN @PhoneNumberTypeID ELSE phn.PhoneNumberTypeID END,
			phn.IsCallAllowed = CASE WHEN @IsCallAllowed IS NOT NULL THEN @IsCallAllowed ELSE phn.IsCallAllowed END,
			phn.IsTextAllowed = CASE WHEN @IsTextAllowed IS NOT NULL THEN @IsTextAllowed ELSE phn.IsTextAllowed END,
			phn.IsPreferred = CASE WHEN @IsPreferred IS NOT NULL THEN @IsPreferred ELSE phn.IsPreferred END,
			HashKey = @HashKey,
			phn.ModifiedBy = CASE WHEN @ModifiedBy IS NOT NULL THEN @ModifiedBy ELSE phn.ModifiedBy END,
			phn.DateModified = GETDATE()
		OUTPUT inserted.BusinessEntityPhoneNumberID INTO @tblOutput(BusinessEntityPhoneNumberID)
		--SELECT *
		FROM dbo.BusinessEntityPhone phn
		WHERE phn.BusinessEntityPhoneNumberID = @BusinessEntityPhoneNumberID
			AND ISNULL(HashKey, '') <> @HashKey
	
	END
	ELSE -- caller wishes to initiate a strict update (wants to ensure correct business entity).
	BEGIN
	
		UPDATE TOP(1) phn
		SET
			phn.PhoneNumber = CASE WHEN @PhoneNumber IS NOT NULL THEN @PhoneNumber ELSE phn.PhoneNumber END,
			phn.PhoneNumberTypeID = CASE WHEN @PhoneNumberTypeID IS NOT NULL THEN @PhoneNumberTypeID ELSE phn.PhoneNumberTypeID END,
			phn.IsCallAllowed = CASE WHEN @IsCallAllowed IS NOT NULL THEN @IsCallAllowed ELSE phn.IsCallAllowed END,
			phn.IsTextAllowed = CASE WHEN @IsTextAllowed IS NOT NULL THEN @IsTextAllowed ELSE phn.IsTextAllowed END,
			phn.IsPreferred = CASE WHEN @IsPreferred IS NOT NULL THEN @IsPreferred ELSE phn.IsPreferred END,
			HashKey = @HashKey,
			phn.ModifiedBy = CASE WHEN @ModifiedBy IS NOT NULL THEN @ModifiedBy ELSE phn.ModifiedBy END,
			phn.DateModified = GETDATE()
		OUTPUT inserted.BusinessEntityPhoneNumberID INTO @tblOutput(BusinessEntityPhoneNumberID)
		--SELECT *
		FROM dbo.BusinessEntityPhone phn
		WHERE phn.BusinessEntityID = @BusinessEntityID 
			AND phn.PhoneNumberTypeID = @PhoneNumberTypeID
			AND ISNULL(HashKey, '') <> @HashKey
	
	END


	-----------------------------------------------------------------------------
	-- Retrieve record information.
	----------------------------------------------------------------------------- 
	SET @BusinessEntityPhoneNumberID = (SELECT TOP 1 BusinessEntityPhoneNumberID FROM @tblOutput);
		
END
