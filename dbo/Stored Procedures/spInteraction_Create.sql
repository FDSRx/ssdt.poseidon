﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/7/2014
-- Description:	Creates a new Interaction item.
-- SAMPLE CALL:

/*
DECLARE
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@InteractionTypeID INT,
	@InteractionTypeCode VARCHAR(50) = NULL,
	@DispositionTypeID INT,
	@DispositionTypeCode VARCHAR(50) = NULL,
	-- Initiated By properties
	@InitiatedByID BIGINT = NULL,
	@InitiatedByString VARCHAR(256) = NULL,
	-- Interacted With properties
	@InteractedWithID BIGINT = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeID INT = NULL,
	@ContactTypeCode VARCHAR(50) = NULL,
	-- Comment Note properties
	@CommentNoteID BIGINT = NULL,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identifies a note.
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(50) = NULL,
	-- Record properties
	@CreatedBy VARCHAR(256) = NULL,
	@InteractionID BIGINT = NULL,
	@Debug BIT = 1

EXEC dbo.spInteraction_Create
	@ApplicationID = @ApplicationID,
	@ApplicationCode = @ApplicationCode,
	@BusinessID = @BusinessID,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@InteractionTypeID = @InteractionTypeID,
	@InteractionTypeCode = @InteractionTypeCode,
	@DispositionTypeID = @DispositionTypeID,
	@DispositionTypeCode = @DispositionTypeCode,
	@InitiatedByID = @InitiatedByID,
	@InitiatedByString = @InitiatedByString,
	@InteractedWithID = @InteractedWithID,
	@InteractedWithString = @InteractedWithString,
	@ContactTypeID = @ContactTypeID,
	@ContactTypeCode = @ContactTypeCode,
	@CommentNoteID = @CommentNoteID,
	@ScopeID = @ScopeID,
	@ScopeCode = @ScopeCode,
	@BusinessEntityID = @BusinessEntityID,
	@NoteTypeID = @NoteTypeID,
	@NoteTypeCode = @NoteTypeCode,
	@NotebookID = @NotebookID,
	@NotebookCode = @NotebookCode,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityID = @PriorityID,
	@PriorityCode = @PriorityCode,
	@Tags = @Tags,
	@OriginID = @OriginID,
	@OriginCode = @OriginCode,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@CreatedBy = @CreatedBy,
	@InteractionID = @InteractionID,
	@Debug = @Debug


SELECT @InteractionID AS InteractionID

*/

-- SELECT * FROM dbo.Interaction
-- SELECT * FROM dbo.InteractionType
-- SELECT * FROM dbo.DispositionType

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spInteraction_Create]
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@InteractionTypeID INT,
	@InteractionTypeCode VARCHAR(50) = NULL,
	@DispositionTypeID INT,
	@DispositionTypeCode VARCHAR(50) = NULL,
	-- Initiated By properties
	@InitiatedByID BIGINT = NULL,
	@InitiatedByString VARCHAR(256) = NULL,
	-- Interacted With properties
	@InteractedWithID BIGINT = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeID INT = NULL,
	@ContactTypeCode VARCHAR(50) = NULL,
	-- Comment Note properties
	@CommentNoteID BIGINT = NULL OUTPUT,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identifies a note.
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	-- Record properties
	@CreatedBy VARCHAR(256) = NULL,
	@InteractionID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@InteractionTypeID=' + dbo.fnToStringOrEmpty(@InteractionTypeID) + ';' +
		'@InteractionTypeCode=' + dbo.fnToStringOrEmpty(@InteractionTypeCode) + ';' +
		'@DispositionTypeID=' + dbo.fnToStringOrEmpty(@DispositionTypeID) + ';' +
		'@DispositionTypeCode=' + dbo.fnToStringOrEmpty(@DispositionTypeCode) + ';' +
		'@InitiatedByID=' + dbo.fnToStringOrEmpty(@InitiatedByID) + ';' +
		'@InitiatedByString=' + dbo.fnToStringOrEmpty(@InitiatedByString) + ';' +
		'@InteractedWithID=' + dbo.fnToStringOrEmpty(@InteractedWithID) + ';' +
		'@InteractedWithString=' + dbo.fnToStringOrEmpty(@InteractedWithString) + ';' +
		'@ContactTypeID=' + dbo.fnToStringOrEmpty(@ContactTypeID) + ';' +
		'@ContactTypeCode=' + dbo.fnToStringOrEmpty(@ContactTypeCode) + ';' +
		'@CommentNoteID=' + dbo.fnToStringOrEmpty(@CommentNoteID) + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@NoteTypeID=' + dbo.fnToStringOrEmpty(@NoteTypeID) + ';' +
		'@NoteTypeCode=' + dbo.fnToStringOrEmpty(@NoteTypeCode) + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID) + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID) + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID) + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@InteractionID=' + dbo.fnToStringOrEmpty(@InteractionID) + ';' ;


	

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = NULL,
		@Arguments = @Args
	*/
	
	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	SET @InteractionID = NULL;

	------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------
	SET @ApplicationID = ISNULL(dbo.fnGetApplicationID(@ApplicationID), dbo.fnGetApplicationID(@ApplicationCode));
	SET @BusinessID = 
		CASE 
			WHEN @BusinessID IS NOT NULL THEN @BusinessID
			ELSE dbo.fnBusinessIDTranslator(@BusinessKey, @BusinessKeyType)
		END;
	SET @InteractionTypeID = ISNULL(dbo.fnGetInteractionTypeID(@InteractionTypeID), dbo.fnGetInteractionTypeID(@InteractionTypeCode));
	SET @DispositionTypeID = ISNULL(dbo.fnGetDispositionTypeID(@DispositionTypeID), dbo.fnGetDispositionTypeID(@DispositionTypeCode));
	SET @ContactTypeID = ISNULL(dbo.fnGetContactTypeID(@ContactTypeID), dbo.fnGetContactTypeID(@ContactTypeCode));
	
	------------------------------------------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	------------------------------------------------------------------------------------------------------------
	-- @InteractionTypeID 
	IF @InteractionTypeID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Interaction object. Object reference (@InteractionTypeID) is not set to an instance of an object. ' +
			'The @InteractionTypeID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @DispositionTypeID
	IF @DispositionTypeID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create Interaction object. Object reference (@DispositionTypeID) is not set to an instance of an object. ' +
			'The @DispositionTypeID parameter cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	

	BEGIN TRY
	
	------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
		
		------------------------------------------------------------------------------------------------------------
		-- Create supplemental note (if applicable).
		-- <Summary>
		-- If a supplemental Note object was not provided, but it looks
		-- like the user wants to comment on the interaction, then try and
		-- create a new Note object.
		-- </Summary>
		------------------------------------------------------------------------------------------------------------		
		IF @CommentNoteID IS NULL
		BEGIN
		
			IF @BusinessEntityID IS NOT NULL AND
				(dbo.fnIsNullOrWhiteSpace(@Title) = 0 OR dbo.fnIsNullOrWhiteSpace(@Memo) = 0)
			BEGIN
				
				EXEC dbo.spNote_Create
					@ScopeID = @ScopeID,
					@ScopeCode = @ScopeCode,
					@ApplicationID = @ApplicationID,
					@BusinessID = @BusinessID,
					@BusinessEntityID = @BusinessEntityID,
					@NoteTypeID = @NoteTypeID,
					@NoteTypeCode = @NoteTypeCode,
					@NotebookID = @NotebookID,
					@NotebookCode = @NotebookCode,
					@Title = @Title,
					@Memo = @Memo,
					@PriorityID = @PriorityID,
					@PriorityCode = @PriorityCode,
					@Tags = @Tags,
					@OriginID = @OriginID,
					@OriginCode = @OriginCode,
					@OriginDataKey = @OriginDataKey,
					@AdditionalData = @AdditionalData,
					@CorrelationKey = @CorrelationKey,
					@CreatedBy = @CreatedBy,
					@NoteID = @CommentNoteID OUTPUT
			
			END
		
		END
			
				
		------------------------------------------------------------------------------------------------------------
		-- Create new interaction item
		------------------------------------------------------------------------------------------------------------
		INSERT INTO dbo.Interaction (
			InteractionTypeID,
			DispositionTypeID,
			InitiatedByID,
			InitiatedByString,
			InteractedWithID,
			InteractedWithString,
			ContactTypeID,
			CommentNoteID,
			CreatedBy
		)
		SELECT
			@InteractionTypeID,
			@DispositionTypeID,
			@InitiatedByID,
			@InitiatedByString,
			@InteractedWithID,
			@InteractedWithString,
			@ContactTypeID,
			@CommentNoteID,
			@CreatedBy
		
		
		-- Retrieve record identity
		SET @InteractionID = SCOPE_IDENTITY();
	
	------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		------------------------------------------------------------------------------------------------------------
		-- Set error variables
		------------------------------------------------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);
		
	END CATCH		
		
END
