﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/21/2014
-- Description:	Creates/Updates an image.
/*
DECLARE 
	@ImageID BIGINT,
	@Exists BIT
	
EXEC spImage_Set
	@ApplicationID = 2,
	@BusinessID = '10684',
	@PersonID = '135590',
	@KeyName = 'ProfilePictureTiny',
	@Caption = 'This is a really tiny profile image.',
	@ImageID = @ImageID OUTPUT,
	@Exists = @Exists OUTPUT,
	@ModifiedBy = 'dhughes'

SELECT @ImageID AS ImageID, @Exists AS IsFound

*/

-- SELECT * FROM dbo.Images
-- SELECT TOP 10 * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- =============================================
CREATE PROCEDURE [dbo].[spImage_Set]
	@ImageID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@PersonID BIGINT = NULL,
	@KeyName VARCHAR(256) = NULL,
	@Name VARCHAR(256) = NULL,
	@Caption VARCHAR(4000) = NULL,
	@Description VARCHAR(MAX) = NULL,
	@FileName VARCHAR(256) = NULL,
	@FilePath VARCHAR(1000) = NULL,
	@FileDataString VARCHAR(MAX) = NULL,
	@FileDataBinary VARBINARY(MAX) = NULL,
	@IsDelete BIT = 0,
	@LanguageID INT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------
	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ImageID=' + dbo.fnToStringOrEmpty(@ImageID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID) + ';' +
		'@KeyName=' + dbo.fnToStringOrEmpty(@KeyName) + ';' +
		'@LanguageID=' + dbo.fnToStringOrEmpty(@LanguageID) + ';'
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------
	SET @IsDelete = ISNULL(@IsDelete, 0);
	SET @Exists = 0;
	SET @ModifiedBy = ISNULL(@ModifiedBy, @CreatedBy);
		
	------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ImageID_Clone INT = @ImageID

	------------------------------------------------------------
	-- Set variables
	------------------------------------------------------------
	SET @LanguageID = ISNULL(@LanguageID, dbo.fnGetLanguageID('en')); -- default to english.
	
	------------------------------------------------------------
	-- Argument null exception
	------------------------------------------------------------
	IF ISNULL(@KeyName, '') = '' AND @ImageID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object references (@KeyName and @ImageID) are not set to an instance of an object. ' + 
			'One of the objects cannot be null or empty.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	------------------------------------------------------------
	-- Retrieve ImageID
	------------------------------------------------------------
	EXEC dbo.spImage_Exists
		@ImageID = @ImageID OUTPUT,
		@ApplicationID = @ApplicationID,
		@BusinessID = @BusinessID,
		@PersonID = @PersonID,
		@KeyName = @KeyName,
		@LanguageID = @LanguageID,
		@Exists = @Exists OUTPUT

	------------------------------------------------------------
	-- Quick image ID verification
	-- <Summary>
	-- If an image ID was supplied but was not found in our 
	-- image repository then we need to just exit the program
	-- as the image ID truncates the unique key lookup process.
	-- </Summary>
	------------------------------------------------------------
	IF @ImageID_Clone IS NOT NULL
	BEGIN
	
		IF @ImageID_Clone <> ISNULL(@ImageID, -1999999)
		BEGIN
			RETURN; -- Exit code; bogus image ID supplied.
		END
		
	END
	
		
	------------------------------------------------------------
	-- Create/Update image record.
	-- <Summary>
	-- If the image ID was not able to be located then we will
	-- consider the image as an "add" and create a new image
	-- record. If the image id was located, then we will update
	-- the image contents.
	------------------------------------------------------------
	IF ISNULL(@Exists, 0) = 0
	BEGIN
	
		-- Create new image record.
		EXEC dbo.spImage_Create
			@ApplicationID = @ApplicationID,
			@BusinessID = @BusinessID,
			@PersonID = @PersonID,
			@KeyName = @KeyName,
			@Name = @Name,
			@Caption = @Caption,
			@Description = @Description,
			@FileName = @FileName,
			@FilePath = @FilePath,
			@FileDataString = @FileDataString,
			@FileDataBinary = @FileDataBinary,
			@LanguageID = @LanguageID,
			@CreatedBy = @CreatedBy,
			@ImageID = @ImageID OUTPUT
	
	END
	ELSE
	BEGIN
	
		-- Update exists image record.
		EXEC dbo.spImage_Update
			@ImageID = @ImageID,
			@ApplicationID = @ApplicationID,
			@BusinessID = @BusinessID,
			@PersonID = @PersonID,
			@KeyName = @KeyName,
			@Name = @Name,
			@Caption = @Caption,
			@Description = @Description,
			@FileName = @FileName,
			@FilePath = @FilePath,
			@FileDataString = @FileDataString,
			@FileDataBinary = @FileDataBinary,
			@IsDelete = @IsDelete,
			@ModifiedBy = @ModifiedBy,
			@LanguageID = @LanguageID
	
	END
	

	
	
	
END

