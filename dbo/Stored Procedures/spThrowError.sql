﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/15/2013
-- Description: Throws a stored procedure error
-- =============================================
CREATE PROCEDURE [dbo].[spThrowError]
	@ErrorMessage NVARCHAR(4000) = NULL,
	@ErrorSeverity INT = NULL,
	@ErrorState INT = NULL,
	@ErrorNumber INT = NULL,
	@ErrorLine INT = NULL,
	@ErrorProcedure  NVARCHAR(200) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------
	SELECT 
		@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
		@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
		@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
		@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
		@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
		@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

	IF @ErrorNumber IS NULL
		RETURN;
		
	SELECT @ErrorMessage = 
		N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
			'Message: '+ ERROR_MESSAGE();

	RAISERROR 
		(
		@ErrorMessage, 
		@ErrorSeverity, 
		@ErrorState,               
		@ErrorNumber,    -- parameter: original error number.
		@ErrorSeverity,  -- parameter: original error severity.
		@ErrorState,     -- parameter: original error state.
		@ErrorProcedure, -- parameter: original error procedure name.
		@ErrorLine       -- parameter: original error line number.
		);

END







