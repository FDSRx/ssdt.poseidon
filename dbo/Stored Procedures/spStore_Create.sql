﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/25/2013
-- Description:	Creates a store
-- Change log:
-- 2/23/2016 - dhughes - Modified the procedure to include the addition of the business provider and additional properties that assist
--					with a more detailed store creation.

-- SAMPLE CALL: 
/*

DECLARE
	-- Business properties.
	@SourceSystemKey VARCHAR(256) = NULL,
	-- Store properties.
	@SourceStoreID INT, -- Legacy operation.
	@StoreName VARCHAR(255),
	@NABP VARCHAR(50) = NULL,
	@TimeZoneID INT = NULL,
	@TimeZoneCode VARCHAR(50) = NULL,
	@SupportDST BIT = NULL,
	@Demographics XML = NULL,
	@Website VARCHAR(256) = NULL,
	-- Provider properties
	@ProviderID INT,
	@ProviderCode VARCHAR(50),
	@ProviderUsername VARCHAR(256),
	@ProviderPasswordUnencrypted VARCHAR(256) = NULL,
	@ProviderPasswordEncrypted VARCHAR(256) = NULL,
	-- Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(75) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(25) = NULL,
	--Business Phone (Work & Fax)
	@PhoneNumber_Work VARCHAR(25) = NULL,
	@PhoneNumber_Fax VARCHAR(25) = NULL,
	--Business Email (Primary & alternate)
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Output
	@BusinessEntityID BIGINT = NULL,
	@Exists BIT = 0,
	-- Error
	@ErrorLogID BIGINT = NULL,
	@ThrowCaughtException BIT = NULL,
	-- Debug
	@Debug BIT = NULL

EXEC dbo.spStore_Create
	-- Business properties.
	@SourceSystemKey = @SourceSystemKey,
	-- Store properties.
	@SourceStoreID = @SourceStoreID, -- Legacy operation.
	@StoreName = @StoreName,
	@NABP = @NABP,
	@TimeZoneID = @TimeZoneID,
	@TimeZoneCode = @TimeZoneCode,
	@SupportDST = @SupportDST,
	@Demographics = @Demographics,
	@Website = @Website,
	-- Provider properties
	@ProviderID = @ProviderID,
	@ProviderCode = @ProviderCode,
	@ProviderUsername = @ProviderUsername,
	@ProviderPasswordUnencrypted = @ProviderPasswordUnencrypted,
	@ProviderPasswordEncrypted = @ProviderPasswordEncrypted,
	-- Address
	@AddressLine1 = @AddressLine1,
	@AddressLine2 = @AddressLine2,
	@City = @City,
	@State = @State,
	@PostalCode = @PostalCode,
	--Business Phone (Work & Fax)
	@PhoneNumber_Work = @PhoneNumber_Work,
	@PhoneNumber_Fax = @PhoneNumber_Fax,
	--Business Email (Primary & alternate)
	@EmailAddress_Primary = @EmailAddress_Primary,
	@EmailAddress_Alternate = @EmailAddress_Alternate,
	-- Caller
	@CreatedBy = @CreatedBy,
	-- Output
	@BusinessEntityID = @BusinessEntityID OUTPUT,
	@Exists = @Exists,
	-- Error
	@ErrorLogID = @ErrorLogID OUTPUT,
	@ThrowCaughtException = @ThrowCaughtException,
	-- Debug
	@Debug = @Debug

SELECT @BusinessEntityID AS BusinessEntityID

*/

-- SELECT * FROM dbo.Business
-- SELECT * FROM dbo.Store
-- SELECT * FROM dbo.Store ORDER BY SourceStoreID ASC
-- SELECT * FROM dbo.Provider
-- SELECT * FROM dbo.BusinessEntityProvider
-- SELECT * FROM dbo.TimeZone

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spStore_Create]
	-- Business properties.
	@SourceSystemKey VARCHAR(256) = NULL,
	-- Store properties.
	@SourceStoreID INT, -- Legacy operation.
	@StoreName VARCHAR(255),
	@NABP VARCHAR(50) = NULL,
	@TimeZoneID INT = NULL,
	@TimeZoneCode VARCHAR(50) = NULL,
	@TimeZonePrefix VARCHAR(10) = NULL,
	@SupportDST BIT = NULL,
	@Demographics XML = NULL,
	@Website VARCHAR(256) = NULL,
	-- Provider properties
	@ProviderID INT = NULL,
	@ProviderCode VARCHAR(50) = NULL,
	@ProviderUsername VARCHAR(256) = NULL,
	@ProviderPasswordUnencrypted VARCHAR(256) = NULL,
	@ProviderPasswordEncrypted VARCHAR(256) = NULL,
	-- Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(75) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(25) = NULL,
	--Business Phone (Work & Fax)
	@PhoneNumber_Work VARCHAR(25) = NULL,
	@PhoneNumber_Fax VARCHAR(25) = NULL,
	--Business Email (Primary & alternate)
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Output
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@Exists BIT = NULL,
	-- Error
	@ErrorLogID BIGINT = NULL OUTPUT,
	@ThrowCaughtException BIT = NULL,
	-- Debug
	@Debug BIT = NULL
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@ErrorCode VARCHAR(50) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	------------------------------------------------------------------------------------------------------------------------
	-- Construct argument list.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE @Args VARCHAR(MAX) =
		'@SourceStoreID=' + dbo.fnToStringOrEmpty(@SourceStoreID) + ';' +
		'@SourceSystemKey=' + dbo.fnToStringOrEmpty(@SourceSystemKey) + ';' +
		'@StoreName=' + dbo.fnToStringOrEmpty(@StoreName) + ';' +
		'@NABP=' + dbo.fnToStringOrEmpty(@NABP) + ';' +
		'@TimeZoneID=' + dbo.fnToStringOrEmpty(@TimeZoneID) + ';' +
		'@TimeZoneCode=' + dbo.fnToStringOrEmpty(@TimeZoneCode) + ';' +
		'@TimeZonePrefix=' + dbo.fnToStringOrEmpty(@TimeZonePrefix) + ';' +
		'@SupportDST=' + dbo.fnToStringOrEmpty(@SupportDST) + ';' +
		'@Demographics=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @Demographics)) + ';' +
		'@Website=' + dbo.fnToStringOrEmpty(@Website) + ';' +
		'@ProviderID=' + dbo.fnToStringOrEmpty(@ProviderID) + ';' +
		'@ProviderCode=' + dbo.fnToStringOrEmpty(@ProviderCode) + ';' +
		'@ProviderUsername=' + dbo.fnToStringOrEmpty(@ProviderUsername) + ';' +
		'@ProviderPasswordUnencrypted=' + dbo.fnToStringOrEmpty(@ProviderPasswordUnencrypted) + ';' +
		'@ProviderPasswordEncrypted=' + dbo.fnToStringOrEmpty(@ProviderPasswordEncrypted) + ';' +
		'@AddressLine1=' + dbo.fnToStringOrEmpty(@AddressLine1) + ';' +
		'@AddressLine2=' + dbo.fnToStringOrEmpty(@AddressLine2) + ';' +
		'@City=' + dbo.fnToStringOrEmpty(@City) + ';' +
		'@State=' + dbo.fnToStringOrEmpty(@State) + ';' +
		'@PostalCode=' + dbo.fnToStringOrEmpty(@PostalCode) + ';' +
		'@PhoneNumber_Work=' + dbo.fnToStringOrEmpty(@PhoneNumber_Work) + ';' +
		'@PhoneNumber_Fax=' + dbo.fnToStringOrEmpty(@PhoneNumber_Fax) + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary) + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@Exists=' + dbo.fnToStringOrEmpty(@Exists) + ';' +
		'@ErrorLogID=' + dbo.fnToStringOrEmpty(@ErrorLogID) + ';' +
		'@ThrowCaughtException=' + dbo.fnToStringOrEmpty(@ThrowCaughtException) + ';' ;

	------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/	


	------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------------------
	SET @BusinessEntityID = NULL;
	SET @ErrorLogID = NULL;
	SET @Exists = 0;
	SET @ThrowCaughtException = ISNULL(@ThrowCaughtException, 0);
	SET @SupportDST = ISNULL(@SupportDST, 1);
	SET @Debug = ISNULL(@Debug, 0);

	-- Set the SourceStoreID property for legacy purposes.
	SET @SourceStoreID = 
		CASE
			-- If a SourceStoreID was provided, then let's use it.
			WHEN @SourceStoreID IS NOT NULL THEN @SourceStoreID 
			-- If a SourceStoreID was not provided, and we already have an identifier below the 0 threshold, then take the current lowest
			-- identifier and increment.
			WHEN @SourceStoreID IS NULL AND (SELECT MIN(SourceStoreID) FROM dbo.Store) < 0
				THEN (SELECT MIN(SourceStoreID) FROM dbo.Store) + -1
			-- Create initial seed for unknown StoreStoreIDs
			ELSE -1000 
		END;

	SET @SourceSystemKey = ISNULL(@SourceSystemKey, @SourceStoreID);
	
	------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@BusinessEntityTypeID INT,
		@BusinessTypeID INT
		
	
	------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------------------
	SET @BusinessEntityTypeID = dbo.fnGetBusinessEntityTypeID('STRE');
	SET @BusinessTypeID = dbo.fnGetBusinessTypeID('STRE');
	SET @ProviderID = ISNULL(dbo.fnGetProviderID(@ProviderID), dbo.fnGetProviderID(@ProviderCode));

	------------------------------------------------------------------------------------------------------------------------
	-- Determine the time zone.
	------------------------------------------------------------------------------------------------------------------------
	-- If a time zone is not known during the initial pass then look one up by state
	IF @TimeZoneID IS NULL
	BEGIN
		SET @TimeZoneID = (SELECT TimeZoneID FROM StateProvince WHERE StateProvinceCode = @State);
			
		-- If a state was not provided then set the store to eastern time.  It can be updated later.
		IF @TimeZoneID IS NULL
		BEGIN
			SET @TimeZoneID = (SELECT TimeZoneID FROM dbo.TimeZone WHERE Code = 'EST')
		END
	END	
	
	-- Look up by NABP.
	SET @BusinessEntityID = ISNULL(@BusinessEntityID, (SELECT BusinessEntityID FROM dbo.Store WHERE Nabp = @Nabp));

	------------------------------------------------------------------------------------------------------------------------
	-- Check to see if the store already exists based on the source system criteria.
	-- <Summary>
	-- Use the type of store, the source system key, and the store's provider to determine if the store might
	-- already exist based on its source data.
	-- </Summary>
	-- <Remarks>
	-- This is a legacy operation that can most likely be removed.
	-- </Remarks>
	------------------------------------------------------------------------------------------------------------------------
	SET @BusinessEntityID = ISNULL(@BusinessEntityID, (
		SELECT 
			BusinessEntityID
		--SELECT *
		--SELECT TOP 1 * 
		FROM dbo.Business biz
		WHERE BusinessTypeID = @BusinessTypeID
			AND SourceSystemKey = @SourceSystemKey
			AND EXISTS (
				SELECT TOP 1 *
				FROM dbo.BusinessEntityProvider bp
				WHERE bp.BusinessEntityID = biz.BusinessEntityID
					AND bp.ProviderID = @ProviderID
			)
	));

	-- Debug
	IF @Debug = 1
	BEGIN 
		SELECT 'DebuggerOn' AS DebugMode, @ProcedureName AS procedureName, 'Getter/Setter methods.' AS ActionMethod,
			@BusinessEntityTypeID AS BusinessEntityTypeID, @BusinessTypeID AS BusinessTypeID, 
			@ProviderID AS ProviderID, @ProviderCode AS ProviderCode, @TimeZoneID AS TimeZoneID, @TimeZoneCode AS TimeZoneCode,
			@TimeZonePrefix AS TimeZonePrefix,
			@SourceSystemKey AS SourceSystemKey, @BusinessEntityID AS BusinessID, @CreatedBy AS CreatedBy
	END

	-- If a business was discovered, then let's exit the code.
	IF @BusinessEntityID IS NOT NULL
	BEGIN
	
		SET @Exists = 1;
				
		RETURN;
	END	

	------------------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. If the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	------------------------------------------------------------------------------------------------------------------------
	BEGIN TRY	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	------------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	------------------------------------------------------------------------------------------------------------------------
		
		DECLARE 
			@BusinessNumber VARCHAR(25),
			@AddressID INT
			
		------------------------------------------------------------------------------------------------------------------------
		-- Create new business enity
		------------------------------------------------------------------------------------------------------------------------
		EXEC dbo.spBusiness_Create
			@BusinessEntityTypeID = @BusinessEntityTypeID,
			@BusinessTypeID = @BusinessTypeID,
			@Name = @StoreName,
			@SourceSystemKey = @SourceSystemKey,
			@CreatedBy = @CreatedBy,
			@BusinessEntityID = @BusinessEntityID OUTPUT	
		
		
		------------------------------------------------------------------------------------------------------------------------
		-- Create new Store object.
		------------------------------------------------------------------------------------------------------------------------
		INSERT INTO dbo.Store (
			BusinessEntityID,
			SourceStoreID,
			NABP,
			Name,
			TimeZoneID,
			SupportDST,
			Demographics,
			Website,
			CreatedBy
		)
		SELECT
			@BusinessEntityID,
			@SourceStoreID,
			@NABP,
			@StoreName,
			@TimeZoneID,
			@SupportDST,
			@Demographics,
			@Website,
			@CreatedBy


		------------------------------------------------------------------------------------------------------------------------
		-- Create new BusinessEntityProvider object (if applicable).
		------------------------------------------------------------------------------------------------------------------------
		IF @ProviderID IS NOT NULL
		BEGIN
			EXEC dbo.spBusinessEntityProvider_Merge
				@BusinessEntityID = @BusinessEntityID,
				@ProviderID = @ProviderID,
				@ProviderCode = @ProviderCode,
				@Username = @ProviderUsername,
				@PasswordUnencrypted = @ProviderPasswordUnencrypted,
				@PasswordEncrypted = @ProviderPasswordEncrypted,
				@CreatedBy = @CreatedBy,
				@ModifiedBy = @CreatedBy,
				@Debug = @Debug		
		END		
		
		------------------------------------------------------------------------------------------------------------------------
		-- Create new Address & Store Address object (if applicable).
		------------------------------------------------------------------------------------------------------------------------	
		-- Create a new address entry (if applicable)
		EXEC spAddress_Create 
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			@CreatedBy = @CreatedBy,
			@AddressID = @AddressID OUTPUT
		
		-- Get the address ID for home address
		DECLARE @AddressTypeID_Work INT = dbo.fnGetAddressTypeID('PRIM');
		
		-- Create new BusinessEntityAddress (store address) object.
		EXEC spBusinessEntity_Address_Create
			@BusinessEntityID = @BusinessEntityID,
			@AddressTypeID = @AddressTypeID_Work,
			@AddressID = @AddressID,
			@CreatedBy = @CreatedBy
			
		------------------------------------------------------------------------------------------------------------------------
		-- Create new store email.
		------------------------------------------------------------------------------------------------------------------------
		DECLARE 
			@EmailAddressTypeID_Primary INT = dbo.fnGetEmailAddressTypeID('PRIM'),
			@EmailAddressTypeID_Alternate INT = dbo.fnGetEmailAddressTypeID('ALT')
		
		-- Create primary email address
		EXEC spBusinessEntity_Email_Create
			@BusinessEntityID = @BusinessEntityID,
			@EmailAddressTypeID = @EmailAddressTypeID_Primary,
			@EmailAddress = @EmailAddress_Primary,
			@CreatedBy = @CreatedBy  


		-- Create alternate email address
		EXEC spBusinessEntity_Email_Create
			@BusinessEntityID = @BusinessEntityID,
			@EmailAddressTypeID = @EmailAddressTypeID_Alternate,
			@EmailAddress = @EmailAddress_Alternate,
			@CreatedBy = @CreatedBy
		
		------------------------------------------------------------------------------------------------------------------------
		-- Create new store phone number
		------------------------------------------------------------------------------------------------------------------------
		DECLARE
			@PhoneNumberTypeID_Work INT = dbo.fnGetPhoneNumberTypeID('WRK'),
			@PhoneNumberTypeID_Fax INT = dbo.fnGetPhoneNumberTypeID('FAX')
			
		-- Create new work phone
		EXEC spBusinessEntity_Phone_Create
			@BusinessEntityID = @BusinessEntityID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Work,
			@PhoneNumber = @PhoneNumber_Work,
			@CreatedBy = @CreatedBy
		
		-- Create new fax number
		EXEC spBusinessEntity_Phone_Create
			@BusinessEntityID = @BusinessEntityID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Fax,
			@PhoneNumber = @PhoneNumber_Fax,
			@CreatedBy = @CreatedBy

	------------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	------------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0 
	BEGIN	
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- void business entity
		SET @BusinessEntityID = NULL;
		
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		EXECUTE dbo.spLogException
			@ErrorLogID = @ErrorLogID OUTPUT,
			@Arguments = @Args

		-- If the caller wishes the exception to be thrown, then re-throw the exception.
		IF @ThrowCaughtException = 1
		BEGIN
			SET @ErrorMessage = 'Unable to create Store object. Exeception: ' + ERROR_MESSAGE();
				
			RAISERROR (
				@ErrorMessage, -- Message text.
				16, -- Severity.
				1 -- State.
			);	
		END
	
	END CATCH;
	
	
END
