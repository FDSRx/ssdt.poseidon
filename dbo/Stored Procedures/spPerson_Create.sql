﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/15/2013
-- Description:	Creates a person within the person model.  This is the core person create SP.  
--			It collects basic person elements to create a fully functioning person.
--			Meaning, it can collect to phone numbers (home and mobile), a home address, two emails (primary and alternate).

-- Change log:
-- DRH 3/7/2013 - Added phone and email logic for allows (i.e. Allow calls, texts, emails).
-- DRH 9/10/2014 
--		- Added the ability to send in an existing BusinessEntityID to extend as a person.
--		- Added the ability to re-throw the handled exception so newer peices of code can treat this
--				stored procedure as a true core method that throws an error on failure.
-- DRH 7/7/2015 - Added the IsPreferred flag for the main communication devices.
-- 2/24/2016 - dhughes - Modified the procedure to include type property codes so that the input is more versatile.
 
-- SAMPLE CALL:

/*
DECLARE
	--Person
	@PersonID BIGINT = NULL,
	@PersonTypeID INT = NULL,
	@PersonTypeCode VARCHAR(50) = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75),
	@LastName VARCHAR(75),
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderID INT = NULL,
	@GenderCode VARCHAR(50) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(50) = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Error
	@ErrorLogID INT = NULL,
	@ThrowCaughtException BIT = NULL,
	-- Business Entity
	@BusinessEntityID BIGINT = NULL,
	-- Debug
	@Debug BIT = NULL


EXEC dbo.spPerson_Create
	@PersonID = @PersonID OUTPUT,
	@PersonTypeID = @PersonTypeID,
	@PersonTypeCode = @PersonTypeCode,
	@Title = @Title,
	@FirstName = @FirstName,
	@LastName = @LastName,
	@MiddleName = @MiddleName,
	@Suffix = @Suffix,
	@BirthDate = @BirthDate,
	@GenderID = @GenderID,
	@GenderCode = @GenderCode,
	@LanguageID = @LanguageID,
	@LanguageCode = @LanguageCode,
	@AddressLine1 = @AddressLine1,
	@AddressLine2 = @AddressLine2,
	@City = @City,
	@State = @State,
	@PostalCode = @PostalCode,
	@PhoneNumber_Home = @PhoneNumber_Home,
	@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
	@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
	@PhoneNumber_Home_IsPreferred = @PhoneNumber_Home_IsPreferred,
	@PhoneNumber_Mobile = @PhoneNumber_Mobile,
	@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
	@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
	@PhoneNumber_Mobile_IsPreferred = @PhoneNumber_Mobile_IsPreferred,
	@EmailAddress_Primary = @EmailAddress_Primary,
	@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
	@EmailAddress_Primary_IsPreferred = @EmailAddress_Primary_IsPreferred,
	@EmailAddress_Alternate = @EmailAddress_Alternate,
	@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
	@EmailAddress_Alternate_IsPreferred = @EmailAddress_Alternate_IsPreferred,
	@CreatedBy = @CreatedBy,
	@ErrorLogID = @ErrorLogID,
	@ThrowCaughtException = @ThrowCaughtException,
	@BusinessEntityID = @BusinessEntityID OUTPUT,
	@Debug = @Debug

SELECT @BusinessEntityID AS BusinessEntityID, @PersonID AS PersonID

*/

-- SELECT * FROM dbo.Person
-- SELECT * FROM dbo.Address

-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- =============================================
CREATE PROCEDURE [dbo].[spPerson_Create]
	--Person
	@PersonID BIGINT = NULL OUTPUT,
	@PersonTypeID INT = NULL,
	@PersonTypeCode VARCHAR(50) = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75),
	@LastName VARCHAR(75),
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderID INT = NULL,
	@GenderCode VARCHAR(50) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(50) = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	-- Error
	@ErrorLogID INT = NULL OUTPUT,
	-- Validation
	@IgnorePersonValidation BIT = NULL,
	@ThrowCaughtException BIT = NULL,
	-- Business Entity
	@BusinessEntityID BIGINT = NULL OUTPUT,
	-- Debug
	@Debug BIT = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@Trancount INT = @@TRANCOUNT,
		@ErrorMessage VARCHAR(4000),
		@DebugMessage VARCHAR(4000)

	------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID) + ';' +
		'@PersonTypeID=' + dbo.fnToStringOrEmpty(@PersonTypeID) + ';' +
		'@PersonTypeCode=' + dbo.fnToStringOrEmpty(@PersonTypeCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName) + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName) + ';' +
		'@MiddleName=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @MiddleName)) + ';' +
		'@Suffix=' + dbo.fnToStringOrEmpty(@Suffix) + ';' +
		'@BirthDate=' + dbo.fnToStringOrEmpty(@BirthDate) + ';' +
		'@GenderID=' + dbo.fnToStringOrEmpty(@GenderID) + ';' +
		'@GenderCode=' + dbo.fnToStringOrEmpty(@GenderCode) + ';' +
		'@LanguageID=' + dbo.fnToStringOrEmpty(@LanguageID) + ';' +
		'@LanguageCode=' + dbo.fnToStringOrEmpty(@LanguageCode) + ';' +
		'@AddressLine1=' + dbo.fnToStringOrEmpty(@AddressLine1) + ';' +
		'@AddressLine2=' + dbo.fnToStringOrEmpty(@AddressLine2) + ';' +
		'@City=' + dbo.fnToStringOrEmpty(@City) + ';' +
		'@State=' + dbo.fnToStringOrEmpty(@State) + ';' +
		'@PostalCode=' + dbo.fnToStringOrEmpty(@PostalCode) + ';' +
		'@PhoneNumber_Home=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home) + ';' +
		'@PhoneNumber_Home_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsCallAllowed) + ';' +
		'@PhoneNumber_Home_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsTextAllowed) + ';' +
		'@PhoneNumber_Home_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsPreferred) + ';' +
		'@PhoneNumber_Mobile=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile) + ';' +
		'@PhoneNumber_Mobile_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsCallAllowed) + ';' +
		'@PhoneNumber_Mobile_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsTextAllowed) + ';' +
		'@PhoneNumber_Mobile_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsPreferred) + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary) + ';'  +
		'@EmailAddress_Primary_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsEmailAllowed) + ';' +
		'@EmailAddress_Primary_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsPreferred) + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate) + ';' +
		'@EmailAddress_Alternate_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsEmailAllowed) + ';' +
		'@EmailAddress_Alternate_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsPreferred) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ErrorLogID=' + dbo.fnToStringOrEmpty(@ErrorLogID) + ';' +
		'@ThrowCaughtException=' + dbo.fnToStringOrEmpty(@ThrowCaughtException) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------------------
	SET @ErrorLogID = NULL; -- never have a user pass an error log ID.  They should only have one returned.
	SET @ThrowCaughtException = ISNULL(@ThrowCaughtException, 0);
	SET @IgnorePersonValidation = ISNULL(@IgnorePersonValidation, 0);
		
	------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@AddressID INT,
		@HashKey VARCHAR(256)

	
	------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------------------
	-- Data review.
	-- SELECT * FROM dbo.PersonType
	-- SELECT * FROM dbo.Language
	-- SELECT * FROM dbo.Gender

	SET @PersonTypeID = COALESCE(dbo.fnGetPersonTypeID(@PersonTypeID), dbo.fnGetPersonTypeID(@PersonTypeCode), dbo.fnGetPersonTypeID('IN'));
	SET @LanguageID = COALESCE(dbo.fnGetLanguageID(@LanguageID), dbo.fnGetLanguageID(@LanguageCode), dbo.fnGetLanguageID('EN'));
	SET @GenderID = COALESCE(dbo.fnGetGenderID(@GenderID), dbo.fnGetGenderID(@GenderCode), NULL);

	-- Create hash key.
	SET @HashKey = dbo.fnCreateRecordHashKey(
		dbo.fnToHashableString(@PersonTypeID) +
		dbo.fnToHashableString(@Title) +
		dbo.fnToHashableString(@FirstName) +
		dbo.fnToHashableString(@LastName) +
		dbo.fnToHashableString(@MiddleName) +
		dbo.fnToHashableString(@Suffix) +
		dbo.fnToHashableString(@BirthDate) +
		dbo.fnToHashableString(@GenderID) +
		dbo.fnToHashableString(@LanguageID)
	);
	

	------------------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. If the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	------------------------------------------------------------------------------------------------------------------------	
	BEGIN TRY
	------------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	------------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	
		------------------------------------------------------------------------------------------------------------------------
		-- Format data
		------------------------------------------------------------------------------------------------------------------------ 
		SET @PhoneNumber_Home = dbo.fnRemoveNonNumericChars(@PhoneNumber_Home)
		SET @PhoneNumber_Mobile = dbo.fnRemoveNonNumericChars(@PhoneNumber_Mobile)
		       

		------------------------------------------------------------------------------------------------------------------------
		-- Validate data to determine if a person can be created.
		-- <Summary>
		-- Validate that the minimum required fields are available to create a person (unless the caller specifies otherwise).
		-- </Summary>
		------------------------------------------------------------------------------------------------------------------------
		IF @IgnorePersonValidation = 0
		BEGIN
			-- Base criteria required to create a person
			IF ISNULL(@FirstName, '') = '' 
				OR ISNULL(@LastName, '') = '' 
				OR ISNULL(@BirthDate, '1/1/0001') = '1/1/0001' 
				--OR ISNULL(@PostalCode, '') = ''
			BEGIN
				-- insufficient person information to create a person. A first, last, and dob is the minimum amount of information necessary
				-- to create a person entity
				SET @ErrorMessage = 'Unable to create a PERSON entity.  A minimum of first and last name, and birth date is required to create a person entity. '
					+ 'Data = '
					+ 'First Name: ' + ISNULL(@FirstName, '<EMPTY>')
					+ '; Last Name: ' + ISNULL(@LastName, '<EMPTY>')
					+ '; Birth Date: ' + ISNULL(CONVERT(VARCHAR, @BirthDate, 101), '<EMPTY>')
					--+ '; Postal Code: ' + ISNULL(@PostalCode, '<EMPTY>');
				
				RAISERROR (
					@ErrorMessage, -- Message text.
					16, -- Severity.
					1 -- State.
				);


				GOTO EOF;
								  
			END	
		END

		------------------------------------------------------------------------------------------------------------------------
		-- Create a new BusinessEntity 0bject.
		-- <Summary>
		-- If a BusinessEntityID (PersonID) was not provided, then create
		-- a new entity to be used as a Person object.
		-- </Summary>
		------------------------------------------------------------------------------------------------------------------------
		-- If a PersonID was provided then set as the BusinessEntityID.
		-- ** The caller wishes to extend an existing object as a person.
		SET @BusinessEntityID = @PersonID	
	
		DECLARE @BusinessEntityTypeID INT = dbo.fnGetBusinessEntityTypeID('PRSN');
		
		-- If a BusinessEntityID was not provided via a PersonID, then create a new BusinessEntityID.
		IF @BusinessEntityID IS NULL
		BEGIN
		
			EXEC dbo.spBusinessEntity_Create
				@BusinessEntityTypeID = @BusinessEntityTypeID,
				@BusinessEntityID = @BusinessEntityID OUTPUT
		END
			
		------------------------------------------------------------------------------------------------------------------------
		-- Create new Person object.
		------------------------------------------------------------------------------------------------------------------------
		INSERT INTO Person (
			BusinessEntityID,
			PersonTypeID,
			Title,
			FirstName,
			LastName,
			MiddleName,
			Suffix,
			BirthDate,
			GenderID,
			LanguageID,
			HashKey,
			CreatedBy
		)
		SELECT
			@BusinessEntityID,
			@PersonTypeID,
			@Title,
			@FirstName,
			@LastName,
			@MiddleName,
			@Suffix,
			@BirthDate,
			@GenderID,
			@LanguageID,
			@HashKey,
			@CreatedBy
			
		-- Reassign the BusinessEntityID as the PersonID.
		SET @PersonID = @BusinessEntityID;

		------------------------------------------------------------------------------------------------------------------------
		-- Create new person address.
		------------------------------------------------------------------------------------------------------------------------	
		-- Create a new address entry (if applicable)
		EXEC spAddress_Create 
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			@CreatedBy = @CreatedBy,
			@AddressID = @AddressID OUTPUT
		
		-- Get the address ID for home address
		DECLARE @AddressTypeID_Home INT = dbo.fnGetAddressTypeID('HME')
		
		-- Create person address
		EXEC spBusinessEntity_Address_Create
			@BusinessEntityID = @PersonID,
			@AddressTypeID = @AddressTypeID_Home,
			@AddressID = @AddressID,
			@CreatedBy = @CreatedBy


		------------------------------------------------------------------------------------------------------------------------
		-- Create new person email.
		------------------------------------------------------------------------------------------------------------------------
		DECLARE 
			@EmailAddressTypeID_Primary INT = dbo.fnGetEmailAddressTypeID('PRIM'),
			@EmailAddressTypeID_Alternate INT = dbo.fnGetEmailAddressTypeID('ALT')
		
		-- Create primary email address
		EXEC spBusinessEntity_Email_Create
			@BusinessEntityID = @PersonID,
			@EmailAddressTypeID = @EmailAddressTypeID_Primary,
			@EmailAddress = @EmailAddress_Primary,
			@IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
			@IsPreferred = @EmailAddress_Primary_IsPreferred,
			@CreatedBy = @CreatedBy


		-- Create alternate email address
		EXEC spBusinessEntity_Email_Create
			@BusinessEntityID = @PersonID,
			@EmailAddressTypeID = @EmailAddressTypeID_Alternate,
			@EmailAddress = @EmailAddress_Alternate,
			@IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
			@IsPreferred = @EmailAddress_Alternate_IsPreferred,
			@CreatedBy = @CreatedBy

		------------------------------------------------------------------------------------------------------------------------
		-- Create new person phone number.
		------------------------------------------------------------------------------------------------------------------------
		DECLARE
			@PhoneNumberTypeID_Home INT = dbo.fnGetPhoneNumberTypeID('HME'),
			@PhoneNumberTypeID_Mobile INT = dbo.fnGetPhoneNumberTypeID('MBL')
			
		-- Create new home phone
		EXEC spBusinessEntity_Phone_Create
			@BusinessEntityID = @PersonID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Home,
			@PhoneNumber = @PhoneNumber_Home,
			@IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
			@IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
			@IsPreferred = @PhoneNumber_Home_IsPreferred,
			@CreatedBy = @CreatedBy
		
		-- Create new mobile phone
		EXEC spBusinessEntity_Phone_Create
			@BusinessEntityID = @PersonID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Mobile,
			@PhoneNumber = @PhoneNumber_Mobile,
			@IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
			@IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
			@IsPreferred = @PhoneNumber_Mobile_IsPreferred,
			@CreatedBy = @CreatedBy


	------------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	------------------------------------------------------------------------------------------------------------------------
	EOF:	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Void person ID
		SET @PersonID = NULL;
		
		EXECUTE [dbo].[spLogError] @ErrorLogID OUTPUT;
		
		------------------------------------------------------------------------------------------------------------------------
		-- Throw caught exception (if applicable)
		-- <Summary>
		-- Re-throws the exception if specified by the caller.
		-- </Summary>
		------------------------------------------------------------------------------------------------------------------------
		IF @ThrowCaughtException = 1
		BEGIN
		
			-- re-throw error
			SET @ErrorMessage = ERROR_MESSAGE();
			
			RAISERROR (
				@ErrorMessage, -- Message text.
				16, -- Severity.
				1 -- State.
			);	
		
		END

	END CATCH;

END
