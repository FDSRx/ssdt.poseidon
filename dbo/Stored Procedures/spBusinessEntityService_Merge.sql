﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/14/2014
-- Description:	Add or updates the provided record.
/*

DECLARE
	@BusinessEntityServiceID BIGINT = NULL,
	@BusinessEntityID BIGINT = 135590,
	@ServiceID INT = 1,
	@AccountKey VARCHAR(256) = 'stephen.simmons@gmail.com',
	@ConnectionStatusID INT = 2,
	@GoLiveDate DATETIME = GETDATE(),
	@DateTermed DATETIME = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@Exists BIT = 0,
	@Debug BIT = 1

EXEC dbo.spBusinessEntityService_Merge
	@BusinessEntityServiceID = @BusinessEntityServiceID,
	@BusinessEntityID = @BusinessEntityID,
	@ServiceID = @ServiceID,
	@AccountKey = @AccountKey,
	@ConnectionStatusID = @ConnectionStatusID,
	@GoLiveDate = @GoLiveDate,
	@DateTermed = @DateTermed,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@Exists = @Exists,
	@Debug = @Debug

*/

-- SELECT * FROM dbo.BusinessEntityService WHERE BusinessEntityServiceID = 69458
-- SELECT * FROM dbo.BusinessEntityService WHERE BusinessEntityID = 135590

-- SELECT * FROM dbo.InformationLog
-- SELECT * FROM dbo.ErrorLog
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityService_Merge]
	@BusinessEntityServiceID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT,
	@ServiceID INT,
	@AccountKey VARCHAR(256) = NULL,
	@ConnectionStatusID INT = NULL,
	@GoLiveDate DATETIME = NULL,
	@DateTermed DATETIME = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = 0	OUTPUT,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);
	SET @ModifiedBy = ISNULL(@ModifiedBy, @CreatedBy);
	SET @CreatedBy = ISNULL(@CreatedBy, @ModifiedBy);
	SET @Debug = ISNULL(@Debug, 0);
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidateSurrogateKey BIT = 0

	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------	
	IF @BusinessEntityServiceID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @ServiceID = NULL;
		
		SET @ValidateSurrogateKey = 1;
	END
				
	----------------------------------------------------------------------
	-- Argument Null Exceptions
	----------------------------------------------------------------------
	----------------------------------------------------------------------
	-- Surrogate key validation.
	-- <Summary>
	-- If a surrogate key was provided, then that will be the only lookup
	-- performed.
	-- <Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityServiceID, 0) = 0 AND @ValidateSurrogateKey = 1
	BEGIN			
		
		SET @ErrorMessage = 'Object reference not set to an instance of an object. The object, @BusinessEntityServiceID, cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	----------------------------------------------------------------------
	-- Unique row key validation.
	-- <Summary>
	-- If a surrogate key was not provided then we will try and perform the
	-- lookup based on the records unique key.
	-- </Summary>
	----------------------------------------------------------------------
	IF ISNULL(@ServiceID, 0) = 0 AND ISNULL(@BusinessEntityID, 0) = 0 AND @ValidateSurrogateKey = 0
	BEGIN
		
		SET @ErrorMessage = 'Object references not set to an instance of an object. The objects, @BusinessEntityID and @ServiceID cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END
	
	
	----------------------------------------------------------------------
	-- Determine if record exists.
	----------------------------------------------------------------------
	EXEC dbo.spBusinessEntityService_Exists
		@BusinessEntityServiceID = @BusinessEntityServiceID OUTPUT,
		@BusinessEntityID = @BusinessEntityID,
		@ServiceID = @ServiceID,
		@Exists = @Exists OUTPUT
	
	
	----------------------------------------------------------------------
	-- Merge data
	-- <Summary>
	-- If the record does not exist, then add the record; otherwise,
	-- update the record
	-- <Summary>
	----------------------------------------------------------------------	
	IF ISNULL(@Exists, 0) = 0
	BEGIN
		-- Add record
		EXEC dbo.spBusinessEntityService_Create
			@BusinessEntityID = @BusinessEntityID,
			@ServiceID = @ServiceID,
			@AccountKey = @AccountKey,
			@ConnectionStatusID = @ConnectionStatusID,
			@GoLiveDate = @GoLiveDate,
			@DateTermed = @DateTermed,
			@CreatedBy = @CreatedBy,
			@BusinessEntityServiceID = @BusinessEntityServiceID OUTPUT,
			@Debug = @Debug
	END
	ELSE
	BEGIN
		-- Update record
		EXEC dbo.spBusinessEntityService_Update
			@BusinessEntityServiceID = @BusinessEntityServiceID OUTPUT,
			@BusinessEntityID = @BusinessEntityID,
			@ServiceID = @ServiceID,
			@AccountKey = @AccountKey,
			@ConnectionStatusID = @ConnectionStatusID,
			@GoLiveDate = @GoLiveDate,
			@DateTermed = @DateTermed,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug		
	END
	
	
	
	
	
	
	
	
	
	
	
END
