﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Resets a password with a random password or a provided password
-- SAMPLE CALL:
/*
DECLARE @Password VARCHAR(128), @PasswordOut VARCHAR(128)
--SET @Password = 'Password2'
EXEC spPassword_Reset 1, 1, @Password, @PasswordOut OUTPUT
SELECT @PasswordOut
*/
-- SELECT * FROM dbo.Password
-- =============================================
CREATE PROCEDURE spPassword_Reset
	@PersonID INT,
	@ServiceID INT,
	@Password VARCHAR(128) = NULL,
	@NewPassword VARCHAR(128) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	------------------------------------------------------
	-- Local variables
	------------------------------------------------------
	DECLARE
		@Hash VARBINARY(128),
		@Salt VARBINARY(10),
		@PwdLength INT = 8
		
	------------------------------------------------------
	-- Retrieve new password to store
	------------------------------------------------------
	IF ISNULL(LTRIM(RTRIM(@Password)), '') <> ''
	BEGIN
		SET @NewPassword = @Password;
	END
	ELSE
	BEGIN
		SET @NewPassword = dbo.fnGeneratePassword(@PwdLength);
	END
	
	------------------------------------------------------
	-- Hash and salt new password
	------------------------------------------------------
	SET @Salt = dbo.fnPasswordSalt(RAND());
	SET @Hash = dbo.fnPasswordHash(@Password + CONVERT(VARCHAR(MAX), @Salt));
	
	------------------------------------------------------
	-- Update new password
	------------------------------------------------------
	EXEC spPassword_Update
		@PersonID = @PersonID,
		@ServiceID = @ServiceID,
		@Password = @NewPassword
	
	
END
