﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/12/2014
-- Description:	Creates a new Note object with Interaction object (if applicable).
-- Change log:
-- 6/9/2016 - dhughes - Modified the procedure to accept Application and Business objects.  Updated the inputs to accept type codes as
--					well as their unique identifiers.

-- SAMPLE CALL: 
/*
DECLARE
	-- Note properties
	@NoteID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	-- Interaction properties
	@InteractionID BIGINT = NULL OUTPUT,
	@InteractionTypeID INT = NULL,
	@InteractionTypeCode VARCHAR(50) = NULL,
	@DispositionTypeID INT = NULL,
	@DispositionTypeCode VARCHAR(50) = NULL,
	@InitiatedByID BIGINT = NULL,
	@InitiatedByString VARCHAR(256) = NULL,
	@InteractedWithID BIGINT = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeID INT = NULL,
	@ContactTypeCode VARCHAR(50) = NULL,
	@InteractionCreatedBy VARCHAR(256) = NULL,
	-- Interaction Comment Note properties
	@InteractionCommentNoteID BIGINT = NULL OUTPUT,
	@InteractionCommentScopeID INT = NULL,
	@InteractionCommentScopeCode VARCHAR(50) = NULL,
	@InteractionCommentApplicationID INT = NULL,
	@InteractionCommentApplicationCode VARCHAR(50) = NULL,
	@InteractionCommentBusinessID BIGINT = NULL,
	@InteractionCommentBusinessKey VARCHAR(50) = NULL,
	@InteractionCommentBusinessKeyType VARCHAR(50) = NULL,
	@InteractionCommentBusinessEntityID BIGINT = NULL,
	@InteractionCommentNoteTypeID INT = NULL,
	@InteractionCommentNoteTypeCode VARCHAR(50) = NULL,
	@InteractionCommentNotebookID INT = NULL,
	@InteractionCommentNotebookCode VARCHAR(50) = NULL,
	@InteractionCommentTitle VARCHAR(1000) = NULL,
	@InteractionCommentMemo VARCHAR(MAX) = NULL,
	@InteractionCommentPriorityID INT = NULL,
	@InteractionCommentPriorityCode VARCHAR(50) = NULL,
	@InteractionCommentTags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@InteractionCommentOriginID INT = NULL,
	@InteractionCommentOriginCode VARCHAR(50) = NULL,
	@InteractionCommentOriginDataKey VARCHAR(256) = NULL,
	@InteractionCommentAdditionalData XML = NULL,
	@InteractionCommentCorrelationKey VARCHAR(256) = NULL,
	@InteractionCommentCreatedBy VARCHAR(256) = NULL,
	-- Comment Note properties
	@CommentNoteID BIGINT = NULL OUTPUT,
	@CommentScopeID INT = NULL,
	@CommentScopeCode VARCHAR(50) = NULL,
	@CommentApplicationID INT = NULL,
	@CommentApplicationCode VARCHAR(50) = NULL,
	@CommentBusinessID BIGINT = NULL,
	@CommentBusinessKey VARCHAR(50) = NULL,
	@CommentBusinessKeyType VARCHAR(50) = NULL,
	@CommentBusinessEntityID BIGINT = NULL,
	@CommentNoteTypeID INT = NULL,
	@CommentNoteTypeCode VARCHAR(50) = NULL,
	@CommentNotebookID INT = NULL,
	@CommentNotebookCode VARCHAR(50) = NULL,
	@CommentTitle VARCHAR(1000) = NULL,
	@CommentMemo VARCHAR(MAX) = NULL,
	@CommentPriorityID INT = NULL,
	@CommentPriorityCode vARCHAR(50) = NULL,
	@CommentTags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@CommentOriginID INT = NULL,
	@CommentOriginCode VARCHAR(50) = NULL,
	@CommentOriginDataKey VARCHAR(256) = NULL,
	@CommentAdditionalData XML = NULL,
	@CommentCorrelationKey VARCHAR(256) = NULL,
	@CommentCreatedBy VARCHAR(256) = NULL,
	-- Common properties
	@CreatedBy VARCHAR(256) = NULL,
	-- Output properties
	@NoteInteractionID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL

EXEC dbo.spNoteWithOptionalInteraction_Create
	@NoteID = @NoteID,
	@ApplicationID = @ApplicationID,
	@ApplicationCode = @ApplicationCode,
	@BusinessID = @BusinessID,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@ScopeID = @ScopeID,
	@ScopeCode = @ScopeCode,
	@BusinessEntityID = @BusinessEntityID,
	@NoteTypeID = @NoteTypeID,
	@NoteTypeCode = @NoteTypeCode,
	@NotebookID = @NotebookID,
	@NotebookCode = @NotebookCode,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityID = @PriorityID,
	@PriorityCode = @PriorityCode,
	@Tags = @Tags,
	@OriginID = @OriginID,
	@OriginCode = @OriginCode,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@InteractionID = @InteractionID,
	@InteractionTypeID = @InteractionTypeID,
	@InteractionTypeCode = @InteractionTypeCode,
	@DispositionTypeID = @DispositionTypeID,
	@DispositionTypeCode = @DispositionTypeCode,
	@InitiatedByID = @InitiatedByID,
	@InitiatedByString = @InitiatedByString,
	@InteractedWithID = @InteractedWithID,
	@InteractedWithString = @InteractedWithString,
	@ContactTypeID = @ContactTypeID,
	@ContactTypeCode = @ContactTypeCode,
	@InteractionCreatedBy = @InteractionCreatedBy,
	@InteractionCommentNoteID = @InteractionCommentNoteID,
	@InteractionCommentScopeID = @InteractionCommentScopeID,
	@InteractionCommentScopeCode = @InteractionCommentScopeCode,
	@InteractionCommentApplicationID = @InteractionCommentApplicationID,
	@InteractionCommentApplicationCode = @InteractionCommentApplicationCode,
	@InteractionCommentBusinessID = @InteractionCommentBusinessID,
	@InteractionCommentBusinessKey = @InteractionCommentBusinessKey,
	@InteractionCommentBusinessKeyType = @InteractionCommentBusinessKeyType,
	@InteractionCommentBusinessEntityID = @InteractionCommentBusinessEntityID,
	@InteractionCommentNoteTypeID = @InteractionCommentNoteTypeID,
	@InteractionCommentNoteTypeCode = @InteractionCommentNoteTypeCode,
	@InteractionCommentNotebookID = @InteractionCommentNotebookID,
	@InteractionCommentNotebookCode = @InteractionCommentNotebookCode,
	@InteractionCommentTitle = @InteractionCommentTitle,
	@InteractionCommentMemo = @InteractionCommentMemo,
	@InteractionCommentPriorityID = @InteractionCommentPriorityID,
	@InteractionCommentPriorityCode = @InteractionCommentPriorityCode,
	@InteractionCommentTags = @InteractionCommentTags,
	@InteractionCommentOriginID = @InteractionCommentOriginID,
	@InteractionCommentOriginCode = @InteractionCommentOriginCode,
	@InteractionCommentOriginDataKey = @InteractionCommentOriginDataKey,
	@InteractionCommentAdditionalData = @InteractionCommentAdditionalData,
	@InteractionCommentCorrelationKey = @InteractionCommentCorrelationKey,
	@InteractionCommentCreatedBy = @InteractionCommentCreatedBy,
	@CommentNoteID = @CommentNoteID,
	@CommentScopeID = @CommentScopeID,
	@CommentScopeCode = @CommentScopeCode,
	@CommentApplicationID = @CommentApplicationID,
	@CommentApplicationCode = @CommentApplicationCode,
	@CommentBusinessID = @CommentBusinessID,
	@CommentBusinessKey = @CommentBusinessKey,
	@CommentBusinessKeyType = @CommentBusinessKeyType,
	@CommentBusinessEntityID = @CommentBusinessEntityID,
	@CommentNoteTypeID = @CommentNoteTypeID,
	@CommentNoteTypeCode = @CommentNoteTypeCode,
	@CommentNotebookID = @CommentNotebookID,
	@CommentNotebookCode = @CommentNotebookCode,
	@CommentTitle = @CommentTitle,
	@CommentMemo = @CommentMemo,
	@CommentPriorityID = @CommentPriorityID,
	@CommentPriorityCode = @CommentPriorityCode,
	@CommentTags = @CommentTags,
	@CommentOriginID = @CommentOriginID,
	@CommentOriginCode = @CommentOriginCode,
	@CommentOriginDataKey = @CommentOriginDataKey,
	@CommentAdditionalData = @CommentAdditionalData,
	@CommentCorrelationKey = @CommentCorrelationKey,
	@CommentCreatedBy = @CommentCreatedBy,
	@CreatedBy = @CreatedBy,
	@NoteInteractionID = @NoteInteractionID,
	@Debug = @Debug



SELECT @NoteID AS NoteID, @InteractionID AS InteractionID, 
	@InteractionCommentNoteID AS InteractionCommentNoteID,
	@CommentNoteID AS CommentNoteID, @NoteInteractionID AS NoteInteractionID

*/

-- SELECT * FROM dbo.NoteInteraction
-- SELECT * FROM dbo.vwTask
-- SELECT * FROM dbo.Interaction
-- SELECT * FROM dbo.InteractionType
-- SELECT * FROM dbo.DispositionType
-- SELECT * FROM dbo.Note


-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spNoteWithOptionalInteraction_Create]
	-- Note properties
	@NoteID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	-- Interaction properties
	@InteractionID BIGINT = NULL OUTPUT,
	@InteractionTypeID INT = NULL,
	@InteractionTypeCode VARCHAR(50) = NULL,
	@DispositionTypeID INT = NULL,
	@DispositionTypeCode VARCHAR(50) = NULL,
	@InitiatedByID BIGINT = NULL,
	@InitiatedByString VARCHAR(256) = NULL,
	@InteractedWithID BIGINT = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeID INT = NULL,
	@ContactTypeCode VARCHAR(50) = NULL,
	@InteractionCreatedBy VARCHAR(256) = NULL,
	-- Interaction Comment Note properties
	@InteractionCommentNoteID BIGINT = NULL OUTPUT,
	@InteractionCommentScopeID INT = NULL,
	@InteractionCommentScopeCode VARCHAR(50) = NULL,
	@InteractionCommentApplicationID INT = NULL,
	@InteractionCommentApplicationCode VARCHAR(50) = NULL,
	@InteractionCommentBusinessID BIGINT = NULL,
	@InteractionCommentBusinessKey VARCHAR(50) = NULL,
	@InteractionCommentBusinessKeyType VARCHAR(50) = NULL,
	@InteractionCommentBusinessEntityID BIGINT = NULL,
	@InteractionCommentNoteTypeID INT = NULL,
	@InteractionCommentNoteTypeCode VARCHAR(50) = NULL,
	@InteractionCommentNotebookID INT = NULL,
	@InteractionCommentNotebookCode VARCHAR(50) = NULL,
	@InteractionCommentTitle VARCHAR(1000) = NULL,
	@InteractionCommentMemo VARCHAR(MAX) = NULL,
	@InteractionCommentPriorityID INT = NULL,
	@InteractionCommentPriorityCode VARCHAR(50) = NULL,
	@InteractionCommentTags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@InteractionCommentOriginID INT = NULL,
	@InteractionCommentOriginCode VARCHAR(50) = NULL,
	@InteractionCommentOriginDataKey VARCHAR(256) = NULL,
	@InteractionCommentAdditionalData XML = NULL,
	@InteractionCommentCorrelationKey VARCHAR(256) = NULL,
	@InteractionCommentCreatedBy VARCHAR(256) = NULL,
	-- Comment Note properties
	@CommentNoteID BIGINT = NULL OUTPUT,
	@CommentScopeID INT = NULL,
	@CommentScopeCode VARCHAR(50) = NULL,
	@CommentApplicationID INT = NULL,
	@CommentApplicationCode VARCHAR(50) = NULL,
	@CommentBusinessID BIGINT = NULL,
	@CommentBusinessKey VARCHAR(50) = NULL,
	@CommentBusinessKeyType VARCHAR(50) = NULL,
	@CommentBusinessEntityID BIGINT = NULL,
	@CommentNoteTypeID INT = NULL,
	@CommentNoteTypeCode VARCHAR(50) = NULL,
	@CommentNotebookID INT = NULL,
	@CommentNotebookCode VARCHAR(50) = NULL,
	@CommentTitle VARCHAR(1000) = NULL,
	@CommentMemo VARCHAR(MAX) = NULL,
	@CommentPriorityID INT = NULL,
	@CommentPriorityCode vARCHAR(50) = NULL,
	@CommentTags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@CommentOriginID INT = NULL,
	@CommentOriginCode VARCHAR(50) = NULL,
	@CommentOriginDataKey VARCHAR(256) = NULL,
	@CommentAdditionalData XML = NULL,
	@CommentCorrelationKey VARCHAR(256) = NULL,
	@CommentCreatedBy VARCHAR(256) = NULL,
	-- Common properties
	@CreatedBy VARCHAR(256) = NULL,
	-- Output properties
	@NoteInteractionID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
	
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@NoteTypeID=' + dbo.fnToStringOrEmpty(@NoteTypeID) + ';' +
		'@NoteTypeCode=' + dbo.fnToStringOrEmpty(@NoteTypeCode) + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID) + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID) + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID) + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@InteractionID=' + dbo.fnToStringOrEmpty(@InteractionID) + ';' +
		'@InteractionTypeID=' + dbo.fnToStringOrEmpty(@InteractionTypeID) + ';' +
		'@InteractionTypeCode=' + dbo.fnToStringOrEmpty(@InteractionTypeCode) + ';' +
		'@DispositionTypeID=' + dbo.fnToStringOrEmpty(@DispositionTypeID) + ';' +
		'@DispositionTypeCode=' + dbo.fnToStringOrEmpty(@DispositionTypeCode) + ';' +
		'@InitiatedByID=' + dbo.fnToStringOrEmpty(@InitiatedByID) + ';' +
		'@InitiatedByString=' + dbo.fnToStringOrEmpty(@InitiatedByString) + ';' +
		'@InteractedWithID=' + dbo.fnToStringOrEmpty(@InteractedWithID) + ';' +
		'@InteractedWithString=' + dbo.fnToStringOrEmpty(@InteractedWithString) + ';' +
		'@ContactTypeID=' + dbo.fnToStringOrEmpty(@ContactTypeID) + ';' +
		'@ContactTypeCode=' + dbo.fnToStringOrEmpty(@ContactTypeCode) + ';' +
		'@InteractionCreatedBy=' + dbo.fnToStringOrEmpty(@InteractionCreatedBy) + ';' +
		'@InteractionCommentNoteID=' + dbo.fnToStringOrEmpty(@InteractionCommentNoteID) + ';' +
		'@InteractionCommentScopeID=' + dbo.fnToStringOrEmpty(@InteractionCommentScopeID) + ';' +
		'@InteractionCommentScopeCode=' + dbo.fnToStringOrEmpty(@InteractionCommentScopeCode) + ';' +
		'@InteractionCommentApplicationID=' + dbo.fnToStringOrEmpty(@InteractionCommentApplicationID) + ';' +
		'@InteractionCommentApplicationCode=' + dbo.fnToStringOrEmpty(@InteractionCommentApplicationCode) + ';' +
		'@InteractionCommentBusinessID=' + dbo.fnToStringOrEmpty(@InteractionCommentBusinessID) + ';' +
		'@InteractionCommentBusinessKey=' + dbo.fnToStringOrEmpty(@InteractionCommentBusinessKey) + ';' +
		'@InteractionCommentBusinessKeyType=' + dbo.fnToStringOrEmpty(@InteractionCommentBusinessKeyType) + ';' +
		'@InteractionCommentBusinessEntityID=' + dbo.fnToStringOrEmpty(@InteractionCommentBusinessEntityID) + ';' +
		'@InteractionCommentNoteTypeID=' + dbo.fnToStringOrEmpty(@InteractionCommentNoteTypeID) + ';' +
		'@InteractionCommentNoteTypeCode=' + dbo.fnToStringOrEmpty(@InteractionCommentNoteTypeCode) + ';' +
		'@InteractionCommentNotebookID=' + dbo.fnToStringOrEmpty(@InteractionCommentNotebookID) + ';' +
		'@InteractionCommentNotebookCode=' + dbo.fnToStringOrEmpty(@InteractionCommentNotebookCode) + ';' +
		'@InteractionCommentTitle=' + dbo.fnToStringOrEmpty(@InteractionCommentTitle) + ';' +
		'@InteractionCommentMemo=' + dbo.fnToStringOrEmpty(@InteractionCommentMemo) + ';' +
		'@InteractionCommentPriorityID=' + dbo.fnToStringOrEmpty(@InteractionCommentPriorityID) + ';' +
		'@InteractionCommentPriorityCode=' + dbo.fnToStringOrEmpty(@InteractionCommentPriorityCode) + ';' +
		'@InteractionCommentTags=' + dbo.fnToStringOrEmpty(@InteractionCommentTags) + ';' +
		'@InteractionCommentOriginID=' + dbo.fnToStringOrEmpty(@InteractionCommentOriginID) + ';' +
		'@InteractionCommentOriginCode=' + dbo.fnToStringOrEmpty(@InteractionCommentOriginCode) + ';' +
		'@InteractionCommentOriginDataKey=' + dbo.fnToStringOrEmpty(@InteractionCommentOriginDataKey) + ';' +
		'@InteractionCommentAdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @InteractionCommentAdditionalData)) + ';' +
		'@InteractionCommentCorrelationKey=' + dbo.fnToStringOrEmpty(@InteractionCommentCorrelationKey) + ';' +
		'@InteractionCommentCreatedBy=' + dbo.fnToStringOrEmpty(@InteractionCommentCreatedBy) + ';' +
		'@CommentNoteID=' + dbo.fnToStringOrEmpty(@CommentNoteID) + ';' +
		'@CommentScopeID=' + dbo.fnToStringOrEmpty(@CommentScopeID) + ';' +
		'@CommentScopeCode=' + dbo.fnToStringOrEmpty(@CommentScopeCode) + ';' +
		'@CommentApplicationID=' + dbo.fnToStringOrEmpty(@CommentApplicationID) + ';' +
		'@CommentApplicationCode=' + dbo.fnToStringOrEmpty(@CommentApplicationCode) + ';' +
		'@CommentBusinessID=' + dbo.fnToStringOrEmpty(@CommentBusinessID) + ';' +
		'@CommentBusinessKey=' + dbo.fnToStringOrEmpty(@CommentBusinessKey) + ';' +
		'@CommentBusinessKeyType=' + dbo.fnToStringOrEmpty(@CommentBusinessKeyType) + ';' +
		'@CommentBusinessEntityID=' + dbo.fnToStringOrEmpty(@CommentBusinessEntityID) + ';' +
		'@CommentNoteTypeID=' + dbo.fnToStringOrEmpty(@CommentNoteTypeID) + ';' +
		'@CommentNoteTypeCode=' + dbo.fnToStringOrEmpty(@CommentNoteTypeCode) + ';' +
		'@CommentNotebookID=' + dbo.fnToStringOrEmpty(@CommentNotebookID) + ';' +
		'@CommentNotebookCode=' + dbo.fnToStringOrEmpty(@CommentNotebookCode) + ';' +
		'@CommentTitle=' + dbo.fnToStringOrEmpty(@CommentTitle) + ';' +
		'@CommentMemo=' + dbo.fnToStringOrEmpty(@CommentMemo) + ';' +
		'@CommentPriorityID=' + dbo.fnToStringOrEmpty(@CommentPriorityID) + ';' +
		'@CommentPriorityCode=' + dbo.fnToStringOrEmpty(@CommentPriorityCode) + ';' +
		'@CommentTags=' + dbo.fnToStringOrEmpty(@CommentTags) + ';' +
		'@CommentOriginID=' + dbo.fnToStringOrEmpty(@CommentOriginID) + ';' +
		'@CommentOriginCode=' + dbo.fnToStringOrEmpty(@CommentOriginCode) + ';' +
		'@CommentOriginDataKey=' + dbo.fnToStringOrEmpty(@CommentOriginDataKey) + ';' +
		'@CommentAdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @CommentAdditionalData)) + ';' +
		'@CommentCorrelationKey=' + dbo.fnToStringOrEmpty(@CommentCorrelationKey) + ';' +
		'@CommentCreatedBy=' + dbo.fnToStringOrEmpty(@CommentCreatedBy) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@NoteInteractionID=' + dbo.fnToStringOrEmpty(@NoteInteractionID) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*		
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = NULL,
		@Arguments = @Args
	*/
	

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------------------------------------------------------
	SET @NoteInteractionID = NULL;
	SET @ContactTypeID = ISNULL(@ContactTypeID, dbo.fnGetContactTypeID('UNKN'));
	SET @InteractionCreatedBy = ISNULL(@InteractionCreatedBy, @CreatedBy);
	SET @InteractionCommentCreatedBy = ISNULL(@InteractionCommentCreatedBy, @CreatedBy);
	SET @CommentCreatedBy = ISNULL(@CommentCreatedBy, @CreatedBy);

	------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------	

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod, 
			@ApplicationID AS ApplicationID, @BusinessID AS BusinessID, @BusinessKey AS BusinessKey, @BusinessKeyType AS BusinessKeyType,
			@Args AS Arguments
	END	


	------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. If the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	------------------------------------------------------------------------------------------------------------	
	BEGIN TRY
	
		------------------------------------------------------------------------------------------------------------
		-- Null argument validation
		-- <Summary>
		-- Validates if any of the necessary arguments were provided with
		-- data.
		-- </Summary>
		------------------------------------------------------------------------------------------------------------
		-- @Memo 
		IF @NoteID IS NULL AND dbo.fnIsNullOrWhiteSpace(@Memo) = 1
		BEGIN
			SET @Memo = 'Unable to create Note object. Object reference (@Memo) is not set to an instance of an object. ' +
				'The @Memo parameter cannot be null or empty.';
			
			RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			  
			 RETURN;
		END	
	
	------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
		
		------------------------------------------------------------------------------------------------------------
		-- Create note object.
		-- <Summary>
		-- If a Note object is provided then do not create a new note;
		-- otherwise create a new Note object. 
		-- </Summary>
		------------------------------------------------------------------------------------------------------------
		IF @NoteID IS NULL
		BEGIN
		
			EXEC dbo.spNote_Create
				@ScopeID = @ScopeID,
				@ScopeCode = @ScopeCode,
				@ApplicationID = @ApplicationID,
				@ApplicationCode = @ApplicationCode,
				@BusinessID = @BusinessID,
				@BusinessKey = @BusinessKey,
				@BusinessKeyType = @BusinessKeyType,
				@BusinessEntityID = @BusinessEntityID,
				@NoteTypeID = @NoteTypeID,
				@NoteTypeCode = @NoteTypeCode,
				@NotebookID = @NotebookID,
				@NotebookCode = @NotebookCode,
				@Title = @Title,
				@Memo = @Memo,
				@PriorityID = @PriorityID,
				@PriorityCode = @PriorityCode,
				@Tags = @Tags,
				@OriginID = @OriginID,
				@OriginCode = @OriginCode,
				@OriginDataKey = @OriginDataKey,
				@AdditionalData = @AdditionalData,
				@CorrelationKey = @CorrelationKey,
				@CreatedBy = @CreatedBy,
				@NoteID = @NoteID OUTPUT
				
		END
		
		------------------------------------------------------------------------------------------------------------
		-- Create supplemental note to the interaction (if applicable).
		-- <Summary>
		-- If a supplemental Note object was not provided, but it looks
		-- like the user wants to comment on the interaction, then try and
		-- create a new Note object.
		-- </Summary>
		------------------------------------------------------------------------------------------------------------		
		IF @InteractionCommentNoteID IS NULL
		BEGIN
		
			IF @BusinessEntityID IS NOT NULL AND
				(dbo.fnIsNullOrWhiteSpace(@InteractionCommentTitle) = 0 OR dbo.fnIsNullOrWhiteSpace(@InteractionCommentMemo) = 0)
			BEGIN
				
				EXEC dbo.spNote_Create
					@ScopeID = @InteractionCommentScopeID,
					@ScopeCode = @InteractionCommentScopeCode,
					@ApplicationID = @InteractionCommentApplicationID,
					@ApplicationCode = @InteractionCommentApplicationCode,
					@BusinessID = @InteractionCommentBusinessID,
					@BusinessKey = @InteractionCommentBusinessKey,
					@BusinessKeyType = @InteractionCommentBusinessKeyType,
					@BusinessEntityID = @InteractionCommentBusinessEntityID,
					@NoteTypeID = @InteractionCommentNoteTypeID,
					@NoteTypeCode = @InteractionCommentNoteTypeCode,
					@NotebookID = @InteractionCommentNotebookID,
					@NotebookCode = @InteractionCommentNotebookCode,
					@Title = @InteractionCommentTitle,
					@Memo = @InteractionCommentMemo,
					@PriorityID = @InteractionCommentPriorityID,
					@PriorityCode = @InteractionCommentPriorityCode,
					@Tags = @InteractionCommentTags,
					@OriginID = @InteractionCommentOriginID,
					@OriginCode = @InteractionCommentOriginCode,
					@OriginDataKey = @InteractionCommentOriginDataKey,
					@AdditionalData = @InteractionCommentAdditionalData,
					@CorrelationKey = @InteractionCommentCorrelationKey,
					@CreatedBy = @InteractionCommentCreatedBy,
					@NoteID = @InteractionCommentNoteID OUTPUT
			
			END
		
		END
					
		------------------------------------------------------------------------------------------------------------
		-- Create interaction
		-- <Summary>
		-- If an Interaction object was not provided, then create a new
		-- Interaction object (if applicable).
		-- </Summary>
		------------------------------------------------------------------------------------------------------------
		IF @InteractionID IS NULL 
			AND @InteractionTypeID IS NOT NULL 
			AND @DispositionTypeID IS NOT NULL 
		BEGIN
		
			EXEC dbo.spInteraction_Create
				@InteractionTypeID = @InteractionTypeID,
				@InteractionTypeCode = @InteractionTypeCode,
				@DispositionTypeID = @DispositionTypeID,
				@DispositionTypeCode = @DispositionTypeCode,
				@InitiatedByID = @InitiatedByID,
				@InitiatedByString = @InitiatedByString,
				@InteractedWithID = @InteractedWithID,
				@InteractedWithString = @InteractedWithString,
				@ContactTypeID = @ContactTypeID,
				@ContactTypeCode = @ContactTypeCode,
				@CommentNoteID = @InteractionCommentNoteID,
				@CreatedBy = @InteractionCreatedBy,
				@InteractionID = @InteractionID OUTPUT
		
		END	
		
		------------------------------------------------------------------------------------------------------------
		-- Create a comment note that describes the note and the interaction objects.
		-- <Summary>
		-- If a supplemental Note object was not provided, but it looks
		-- like the user wants to comment on the note and interaction binding, then try and
		-- create a new Note object.
		-- </Summary>
		------------------------------------------------------------------------------------------------------------		
		IF @CommentNoteID IS NULL
		BEGIN
		
			IF @BusinessEntityID IS NOT NULL AND
				(dbo.fnIsNullOrWhiteSpace(@CommentTitle) = 0 OR dbo.fnIsNullOrWhiteSpace(@CommentMemo) = 0)
			BEGIN
				
				EXEC dbo.spNote_Create
					@ScopeID = @CommentScopeID,
					@ScopeCode = @CommentScopeCode,
					@BusinessID = @CommentBusinessID,
					@BusinessKey = @CommentBusinessKey,
					@BusinessKeyType = @CommentBusinessKeyType,
					@BusinessEntityID = @CommentBusinessEntityID,
					@NoteTypeID = @CommentNoteTypeID,
					@NoteTypeCode = @CommentNoteTypeCode,
					@NotebookID = @CommentNotebookID,
					@NotebookCode = @CommentNotebookCode,
					@Title = @CommentTitle,
					@Memo = @CommentMemo,
					@PriorityID = @CommentPriorityID,
					@PriorityCode = @CommentPriorityCode,
					@Tags = @CommentTags,
					@OriginID = @CommentOriginID,
					@OriginCode = @CommentOriginCode,
					@OriginDataKey = @CommentOriginDataKey,
					@AdditionalData = @CommentAdditionalData,
					@CorrelationKey = @CommentCorrelationKey,
					@CreatedBy = @CommentCreatedBy,
					@NoteID = @CommentNoteID OUTPUT
			
			END
		
		END
		
		------------------------------------------------------------------------------------------------------------
		-- Create new NoteInteraction object (if applicable)
		-- <Summary>
		-- Creates a new NoteInteraction object if all the applicable objects
		-- are available.
		-- </Summary>
		------------------------------------------------------------------------------------------------------------
		IF @NoteID IS NOT NULL
			AND @InteractionID IS NOT NULL
		BEGIN
			EXEC dbo.spNoteInteraction_Create
				@NoteID = @NoteID,
				@InteractionID = @InteractionID,
				@CommentNoteID = @CommentNoteID,
				@CreatedBy = @CreatedBy,
				@NoteInteractionID = @NoteInteractionID OUTPUT
		END
			
	
	------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);
		
	END CATCH	
		
END
