﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Creates a new note tag item.
-- SAMPLE CALL:
/*
DECLARE @NoteTagIDs VARCHAR(MAX)

EXEC dbo.spNoteTag_MergeRange
	@NoteID = 1,
	--@TagIDs = 1,
	@TagNames = 'Test,Test Note',
	@NoteTagIDs = @NoteTagIDs OUTPUT

SELECT @NoteTagIDs AS NoteTagIDs

*/
-- SELECT * FROM dbo.NoteTag
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Tag
-- =============================================
CREATE PROCEDURE [dbo].[spNoteTag_MergeRange]
	@NoteID BIGINT,
	@TagIDs VARCHAR(MAX) = NULL,
	@TagNames VARCHAR(MAX) = NULL,
	@NoteTagIDs VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- debug
	--SELECT @NoteID AS NoteID, @TagIDs AS TagIDs, @TagNames AS TagNames
	
	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @NoteTagIDs = NULL;

	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@UseIdentifiers BIT = 0
		
	----------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	----------------------------------------------------------------------
	-- A NoteTag object is required to be provided.
	IF ISNULL(@NoteID, 0) = 0
	BEGIN
		SET @ErrorMessage = 'Unable to create NoteTag object.  Object reference (@NoteID) is not set to an instance of an object. ' +
			'The @NoteID parameter cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	
	-- A TagID list or TagName list is required to be provided.
	IF dbo.fnIsNullOrWhiteSpace(@TagIDs) = 1 AND dbo.fnIsNullOrWhiteSpace(@TagNames) = 1
	BEGIN
	
		SET @ErrorMessage = 'Unable to create NoteTag object.  Object references (@TagIDs or @TagNames) are not set to an instance of an object. ' +
			'The @TagIDs or @TagNames parameters cannot be null or empty.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	
	----------------------------------------------------------------------
	-- Create temporary resources
	----------------------------------------------------------------------
	-- Tag objects
	IF OBJECT_ID('tempdb..#tmpNoteTag_Tags') IS NOT NULL
	BEGIN
		DROP TABLE #tmpNoteTag_Tags;
	END
	
	CREATE TABLE #tmpNoteTag_Tags (
		TagID BIGINT
	);
		

	--------------------------------------------------------------
	-- Parse TagID range.
	-- <Summary>
	-- Splits the comma delimited string of TagIDs into a table list.
	-- </Summary>
	--------------------------------------------------------------
	IF dbo.fnIsNullOrWhiteSpace(@TagIDs) = 0
	BEGIN
	
		INSERT INTO #tmpNoteTag_Tags(
			TagID
		)
		SELECT DISTINCT 
			Value
		FROM dbo.fnSplit(@TagIDs, ',')
		WHERE LTRIM(RTRIM(ISNULL(Value, ''))) <> ''
			AND ISNUMERIC(LTRIM(RTRIM(ISNULL(Value, '')))) = 1
		
		-- Validate list
		IF NOT EXISTS(SELECT TOP 1 * FROM #tmpNoteTag_Tags)
		BEGIN
			SET @ErrorMessage = 'Unable to create NoteTag entries.  Object reference (@TagIDs) is not set to an instance of an object. ' +
				'The @TagIDs list object does not contain a list of items or is not formatted correctly.';
				
			RAISERROR (@ErrorMessage, -- Message text.
					   16, -- Severity.
					   1 -- State.
					   );
			
			RETURN;		
		END	
		
		SET @UseIdentifiers = 1;
	END

	--------------------------------------------------------------
	-- Parse TagName range (If applicable).
	-- <Summary>
	-- Splits the comma delimited string of TagNames into a table list.
	-- </Summary>
	--------------------------------------------------------------	
	IF @UseIdentifiers = 0 AND dbo.fnIsNullOrWhiteSpace(@TagNames) = 0
	BEGIN

		-- Create Tag objects and retrieve object identifiers.
		EXEC dbo.spTag_MergeRange
			@Names = @TagNames,
			@TagIDs = @TagIDs OUTPUT
					
		-- Create TagID listing.
		INSERT INTO #tmpNoteTag_Tags(
			TagID
		)
		SELECT DISTINCT 
			Value
		FROM dbo.fnSplit(@TagIDs, ',')
		WHERE LTRIM(RTRIM(ISNULL(Value, ''))) <> ''
			AND ISNUMERIC(LTRIM(RTRIM(ISNULL(Value, '')))) = 1
		
		-- Validate list
		IF NOT EXISTS(SELECT TOP 1 * FROM #tmpNoteTag_Tags)
		BEGIN
			SET @ErrorMessage = 'Unable to create NoteTag entries.  Object reference (@TagNames) is not set to an instance of an object. ' +
				'The @TagNames list object does not contain a list of items or is not formatted correctly.';
				
			RAISERROR (@ErrorMessage, -- Message text.
					   16, -- Severity.
					   1 -- State.
					   );
			
			RETURN;		
		END	

	END	

	--------------------------------------------------------------
	-- Create NoteTag entries.
	-- <Summary>
	-- Adds the range of NoteTags into the NoteTag object collection.
	-- </Summary>
	--------------------------------------------------------------	
	-- Create new NoteTag objects.
	INSERT INTO dbo.NoteTag (
		NoteID,
		TagID
	)
	SELECT
		@NoteID AS NoteID,
		tmp.TagID
	FROM #tmpNoteTag_Tags tmp
		LEFT JOIN dbo.NoteTag nt
			ON nt.NoteID = @NoteID
				AND tmp.TagID = nt.TagID
	WHERE nt.NoteTagID IS NULL

	--------------------------------------------------------------
	-- Build NoteTag output string.
	-- <Summary>
	-- Creates a comma delimited string of existing and created
	-- NoteTagIDs.
	-- </Summary>
	--------------------------------------------------------------
	SET @NoteTagIDs = (
		SELECT CONVERT(VARCHAR(MAX), NoteTagID) + ','
		FROM #tmpNoteTag_Tags tag
			JOIN dbo.NoteTag nt
				ON tag.TagID = nt.TagID
		WHERE nt.NoteID = @NoteID
		FOR XML PATH('')
	);
	
	-- Remove trailing comma.
	SET @NoteTagIDs = LEFT(@NoteTagIDs, LEN(@NoteTagIDs) - 1);
			

	----------------------------------------------------------------------
	-- Dispose of temporary resources
	----------------------------------------------------------------------
	DROP TABLE #tmpNoteTag_Tags;

	
END
