﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Creates a new Tag/TagType object.
-- SAMPLE CALL:
/*
DECLARE @TagTagTypeID BIGINT

EXEC dbo.spTagTagType_Merge
	@TagTypeID = 1,
	@TagID = 11,
	@CreatedBy = 'dhughes',
	@TagTagTypeID = @TagTagTypeID OUTPUT

SELECT @TagTagTypeID AS TagTagTypeID

*/
-- SELECT * FROM dbo.TagTagType
-- SELECT * FROM dbo.TagType
-- SELECT * FROM dbo.Tag
-- =============================================
CREATE PROCEDURE [dbo].[spTagTagType_Merge]
	@TagTypeID INT = NULL,
	@TagID BIGINT = NULL,
	@TagName VARCHAR(500) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@TagTagTypeID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @TagTagTypeID = NULL;

	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
		
	----------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	----------------------------------------------------------------------
	-- A Tag object was required to be provided.
	IF @TagID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create TagTagType object.  Object reference (@TagID) is not set to an instance of an object. ' +
			'The @TagID parameter cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	
	--------------------------------------------------------------
	-- Determine TagTagType object existence.
	-- <Summary>
	-- Determine if the TagTagType object exists.  If the TagTagType object already exists,
	-- then exit the merge process and return the found TagTagTypeID.
	-- <Summary>
	-- <Remarks>
	-- TagTagType are always adds.  They are NEVER updates.
	-- </Summary>
	--------------------------------------------------------------
	EXEC dbo.spTagTagType_Exists
		@TagTypeID = @TagTypeID,
		@TagID = @TagID,
		@TagName = @TagName,
		@TagTagTypeID = @TagTagTypeID OUTPUT
	
	-- debug
	--SELECT @TagTagTypeID AS TagTagTypeID
	
	-- If a TagTagType object was found, then exit the code.
	IF @TagTagTypeID IS NOT NULL
	BEGIN
		RETURN;
	END	
	
	--------------------------------------------------------------
	-- Retrieve Tag object.
	-- <Summary>
	-- Determines the Tag object existence and retrieves the Tag object ID.
	-- </Summary>
	--------------------------------------------------------------
	EXEC dbo.spTag_Exists
		@TagID = @TagID OUTPUT,
		@Name = @TagName
	
	-- debug
	--SELECT @TagTypeID AS TagTypeID, @TagID AS TagID
		
	--------------------------------------------------------------
	-- Create new NoteTag object.
	-- <Summary>
	-- Creates a new NoteTag entry and returns its unique row identifier.
	-- </Summary>
	--------------------------------------------------------------
	EXEC dbo.spTagTagType_Create
		@TagTypeID = @TagTypeID,
		@TagID = @TagID,
		@CreatedBy = @CreatedBy,
		@TagTagTypeID = @TagTagTypeID OUTPUT
	
END
