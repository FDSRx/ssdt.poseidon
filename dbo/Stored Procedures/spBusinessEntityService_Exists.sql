﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/14/2014
-- Description:	Determines if a business entity service record exists.
-- SAMPLE CALL:
/*
DECLARE 
	@Exists BIT = 0, 
	@BusinessEntityServiceID BIGINT = NULL

---- Surrogate validation
--EXEC [spBusinessEntityService_Exists]
--	@BusinessEntityServiceID = @BusinessEntityServiceID OUTPUT,
--	@BusinessEntityID = -1,
--	@ServiceID = -1,
--	@Exists = @Exists OUTPUT

-- Unique key validation
EXEC [spBusinessEntityService_Exists]
	@BusinessEntityServiceID = @BusinessEntityServiceID OUTPUT,
	@BusinessEntityID = 6225,
	@ServiceID = 1,
	@Exists = @Exists OUTPUT
	
SELECT @Exists AS IsFound, @BusinessEntityServiceID AS BusinessEntityServiceID
	
		
*/
-- SELECT * FROM dbo.BusinessEntityService
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityService_Exists]
	@BusinessEntityServiceID BIGINT = NULL OUTPUT,
	@BusinessEntityID INT = NULL,
	@ServiceID INT = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidateSurrogateKey BIT = 0
		

	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------	
	IF @BusinessEntityServiceID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @ServiceID = NULL;
		
		SET @ValidateSurrogateKey = 1;
	END
	
	----------------------------------------------------------------------
	-- Argument Null Exceptions
	----------------------------------------------------------------------
	----------------------------------------------------------------------
	-- Surrogate key validation.
	-- <Summary>
	-- If a surrogate key was provided, then that will be the only lookup
	-- performed.
	-- <Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityServiceID, 0) = 0 AND @ValidateSurrogateKey = 1
	BEGIN			
		
		SET @ErrorMessage = 'Object reference not set to an instance of an object. The object, @BusinessEntityServiceID, cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	----------------------------------------------------------------------
	-- Unique row key validation.
	-- <Summary>
	-- If a surrogate key was not provided then we will try and perform the
	-- lookup based on the records unique key.
	-- </Summary>
	----------------------------------------------------------------------
	IF ISNULL(@ServiceID, 0) = 0 AND ISNULL(@BusinessEntityID, 0) = 0 AND @ValidateSurrogateKey = 0
	BEGIN
		
		SET @ErrorMessage = 'Object references not set to an instance of an object. The objects, @BusinessEntityID and @ServiceID cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END
	
	
	--------------------------------------------------------------------
	-- Determine if business entity service exists
	--------------------------------------------------------------------
	SET @BusinessEntityServiceID = 
		CASE 
			WHEN @BusinessEntityServiceID IS NOT NULL AND @ValidateSurrogateKey = 1 THEN (
				SELECT TOP 1 BusinessEntityServiceID 
				FROM dbo.BusinessEntityService
				WHERE BusinessEntityServiceID = @BusinessEntityServiceID
			)
			ELSE (
				SELECT TOP 1 BusinessEntityServiceID
				FROM dbo.BusinessEntityService
				WHERE BusinessEntityID = @BusinessEntityID
					AND ServiceID = @ServiceID
			)
		END;
	
	-- Set the exists flag to properly represent the finding.
	SET @Exists = CASE WHEN @BusinessEntityServiceID IS NOT NULL THEN 1 ELSE 0 END;
	
	
	
	
	
	
	
	
	
	
	
	
END
