﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/28/2014
-- Description:	Creates a new chain store entry.
-- =============================================
CREATE PROCEDURE spChainStore_Create
	@ChainID INT,
	@StoreID INT,
	@ChainStoreID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------
	SET @ChainStoreID = NULL;
	

	-----------------------------------------------------------
	-- Create chain store entry.
	-----------------------------------------------------------
	INSERT INTO dbo.ChainStore (
		ChainID,
		StoreID
	)
	SELECT
		@ChainID AS ChainID,
		@StoreID AS StoreID
	
	
	SET @ChainStoreID = SCOPE_IDENTITY();
	
	
		
END
