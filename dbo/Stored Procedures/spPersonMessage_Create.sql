﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/10/2103
-- Description:	Adds a message to the person's internal inbox
-- SELECT * FROM dbo.PersonMessage
-- =============================================
CREATE PROCEDURE [dbo].[spPersonMessage_Create]
	@PersonID BIGINT,
	@ServiceID INT,
	@FolderID INT,
	@MessageID BIGINT,
	@IsRead BIT = 0,
	@CanRespond BIT = 0,
	@IsConversation BIT = 0,
	@RoutingData XML = NULL,
	@ThreadKey UNIQUEIDENTIFIER = NULL OUTPUT,
	@PersonMessageID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------
	SET @CanRespond = ISNULL(@CanRespond, 0);
	SET @IsConversation = ISNULL(@IsConversation, 0);
	SET @ThreadKey = ISNULL(@ThreadKey, NEWID());
	SET @IsRead = ISNULL(@IsRead, 0);
	SET @PersonMessageID = NULL;
	

	---------------------------------------------------------------
	-- Create new person message record.
	---------------------------------------------------------------	
	INSERT INTO dbo.PersonMessage (
		PersonID,
		ServiceID,
		FolderID,
		MessageID,
		IsRead,
		CanRespond,
		IsConversation,
		ThreadKey,
		RoutingData
	)
	SELECT
		@PersonID,
		@ServiceID,
		@FolderID,
		@MessageID,
		@IsRead,
		@CanRespond,
		@IsConversation,
		@ThreadKey,
		@RoutingData
	
	-- Retrieve record identity	
	SET @PersonMessageID = SCOPE_IDENTITY();
	
END
