﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/12/2013
-- Description:	Updates a person's demographic records
-- SAMPLE CALL: 
/*

EXEC dbo.spPerson_Update
	@PersonID = 135624,
	@FirstName = 'Danny',
	@LastName = 'Hughes'

*/

-- SELECT * FROM dbo.Person WHERE BusinessEntityID = '135624'
-- =============================================
CREATE PROCEDURE [dbo].[spPerson_Update]
	@PersonID BIGINT,
	@PersonTypeID INT = NULL,
	@Title NVARCHAR(10) = NULL,
	@FirstName NVARCHAR(75) = NULL,
	@MiddleName NVARCHAR(75) = NULL,
	@LastName NVARCHAR(75) = NULL,
	@Suffix NVARCHAR(10) = NULL,
	@BirthDate DATETIME = NULL,
	@GenderID INT = NULL,
	@LanguageID INT = NULL,
	@AdditionalInfo XML = NULL,
	@Demographics XML = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@HashKey VARCHAR(256)

	
	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------
	-- Create hash key.
	SET @HashKey = dbo.fnCreateRecordHashKey( 
		dbo.fnToHashableString(@PersonTypeID) +
		dbo.fnToHashableString(@Title) +
		dbo.fnToHashableString(@FirstName) +
		dbo.fnToHashableString(@LastName) +
		dbo.fnToHashableString(@MiddleName) +
		dbo.fnToHashableString(@Suffix) +
		dbo.fnToHashableString(@BirthDate) +
		dbo.fnToHashableString(@GenderID) +
		dbo.fnToHashableString(@LanguageID)
	);


	----------------------------------------------------------------------
	-- Update person data.
	-- <Summary.
	-- Determines if there is a change in the person data and updates
	-- accordingly.
	-- </Summary>
	----------------------------------------------------------------------	
	UPDATE psn
	SET 
		psn.PersonTypeID = CASE WHEN @PersonTypeID IS NOT NULL THEN @PersonTypeID ELSE psn.PersonTypeID END,
		psn.Title = CASE WHEN @Title IS NOT NULL THEN @Title ELSE psn.Title END,
		psn.FirstName = CASE WHEN @FirstName IS NOT NULL THEN @FirstName ELSE psn.FirstName END,
		psn.MiddleName = CASE WHEN @MiddleName IS NOT NULL THEN @MiddleName ELSE psn.MiddleName END,
		psn.LastName = CASE WHEN @LastName IS NOT NULL THEN @LastName ELSE psn.LastName END,
		psn.Suffix = CASE WHEN @Suffix IS NOT NULL THEN @Suffix ELSE psn.Suffix END,
		psn.BirthDate = CASE WHEN @BirthDate IS NOT NULL THEN @BirthDate ELSE psn.BirthDate END,
		psn.GenderID = CASE WHEN @GenderID IS NOT NULL THEN @GenderID ELSE psn.GenderID END,
		psn.LanguageID = CASE WHEN @LanguageID IS NOT NULL THEN @LanguageID ELSE psn.LanguageID END,
		psn.AdditionalInfo = CASE WHEN @AdditionalInfo IS NOT NULL THEN @AdditionalInfo ELSE psn.AdditionalInfo END,
		psn.Demographics = CASE WHEN @Demographics IS NOT NULL THEN @Demographics ELSE psn.Demographics END,
		psn.HashKey = @HashKey,
		psn.ModifiedBy = CASE WHEN @ModifiedBy IS NOT NULL THEN @ModifiedBy ELSE psn.ModifiedBy END,
		psn.DateModified = GETDATE()
	--SELECT *
	FROM dbo.Person psn
	WHERE BusinessEntityID = @PersonID
		AND ISNULL(HashKey, '') <> @HashKey
	
END
