﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/12/1982
-- Description:	Updates an address
-- Change log:
-- 8/2/2016 - dhughes - Modified the procedure to only update the object if the Address object has changed.

-- SAMPLE CALL:
/*
DECLARE
	@BusinessEntityAddressID BIGINT = NULL,
	@BusinessEntityID BIGINT = 1,
	@AddressTypeID INT = 5,
	@AddressID INT = 257,
	@ModifiedBy VARCHAR(256) = NULL

EXEC dbo.spBusinessEntity_Address_Update
	@BusinessEntityAddressID = @BusinessEntityAddressID,
	@BusinessEntityID = @BusinessEntityID,
	@AddressTypeID = @AddressTypeID,
	@AddressID = @AddressID,
	@ModifiedBy = @ModifiedBy
*/

-- SELECT * FROM dbo.BusinessEntityAddress

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Address_Update]
	@BusinessEntityAddressID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@AddressTypeID INT = NULL,
	@AddressID INT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------
	-- Instance variables
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityAddressID=' + dbo.fnToStringOrEmpty(@BusinessEntityAddressID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@AddressTypeID=' + dbo.fnToStringOrEmpty(@AddressTypeID) + ';' +
		'@AddressID=' + dbo.fnToStringOrEmpty(@AddressID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' ;


	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------------
	SET @ModifiedBy = ISNULL(@ModifiedBy, SUSER_NAME());

	-----------------------------------------------------------------------------------------------------------------------
	-- Temporary resources
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE
		@tblOutput AS TABLE (BusinessEntityAddressID BIGINT)


	-----------------------------------------------------------------------------------------------------------------------
	-- Update record
	-----------------------------------------------------------------------------------------------------------------------
	-- If a person address was specified then directly update the specified address
	IF @BusinessEntityAddressID IS NOT NULL
		AND @AddressID IS NOT NULL
	BEGIN
	
		UPDATE TOP(1) addr
		SET 
			addr.AddressID = @AddressID,
			addr.AddressTypeID = CASE WHEN @AddressTypeID IS NOT NULL THEN @AddressTypeID ELSE addr.AddressTypeID END,
			addr.ModifiedBy = @ModifiedBy,
			addr.DateModified = GETDATE()
		OUTPUT inserted.BusinessEntityAddressID INTO @tblOutput(BusinessEntityAddressID)
		--SELECT *
		FROM dbo.BusinessEntityAddress addr
		WHERE BusinessEntityAddressID = @BusinessEntityAddressID	
			AND AddressID <> @AddressID
	
	END
	ELSE -- Let's go ahead and try and update the combination
	BEGIN
		
		-- Check if an address for the specified type already exists
		IF @BusinessEntityID IS NOT NULL 
			AND @AddressTypeID IS NOT NULL 
			AND @AddressID IS NOT NULL
			AND (SELECT COUNT(*) FROM BusinessEntityAddress WHERE BusinessEntityID = @BusinessEntityID AND AddressTypeID = @AddressTypeID) = 1
		BEGIN
		
			UPDATE TOP(1) addr
			SET 
				addr.AddressID = @AddressID,
				addr.ModifiedBy = @ModifiedBy,
				addr.DateModified = GETDATE()
			OUTPUT inserted.BusinessEntityAddressID INTO @tblOutput(BusinessEntityAddressID)
			--SELECT *
			FROM dbo.BusinessEntityAddress addr
			WHERE BusinessEntityID = @BusinessEntityID
				AND AddressTypeID = @AddressTypeID
				AND AddressID <> @AddressID		
				
		END
	
	END


	-----------------------------------------------------------------------------------------------------------------------
	-- Retrieve record information
	-----------------------------------------------------------------------------------------------------------------------
	SET @BusinessEntityAddressID = (SELECT TOP 1 BusinessEntityAddressID FROM @tblOutput);
	
END
