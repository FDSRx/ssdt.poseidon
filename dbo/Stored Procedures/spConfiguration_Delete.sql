﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/06/2015
-- Description:	Deletes a Configuration object.
-- SAMPLE CALL:

/*

DECLARE
	@ConfigurationID BIGINT = 93,
	@ApplicationID INT = 10,
	@BusinessEntityID BIGINT = 38,
	@KeyName VARCHAR(256) = 'IsEngageMessagingEnabled',
	@Exists BIT = NULL

EXEC dbo.spConfiguration_Delete
	@ApplicationID = @ApplicationID, -- "ENG" (Engage/Store management tool)
	@BusinessEntityID = @BusinessEntityID,
	@KeyName = @KeyName,
	@ConfigurationID = @ConfigurationID OUTPUT,
	@Exists = @Exists OUTPUT

SELECT @ConfigurationID AS ConfigurationID, @Exists AS IsFound

*/
-- SELECT * FROM dbo.Configuration
-- =============================================
CREATE PROCEDURE [dbo].[spConfiguration_Delete]
	@ConfigurationID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@KeyName VARCHAR(256) = NULL OUTPUT,
	@ModifiedBy VARCHAR(256) = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE	
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ConfigurationID=' + dbo.fnToStringOrEmpty(@ConfigurationID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@KeyName=' + dbo.fnToStringOrEmpty(@KeyName) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' 
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);

	-----------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------------------------
	-- No declarations.	

	-----------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------------
	
	-- Debug
	--SELECT @ConfigurationID AS ConfigurationID, @ApplicationID AS ApplicationID, @BusinessEntityID AS BusinessEntityID,
	--	@KeyName AS KeyName, @Exists AS IsFound

	-----------------------------------------------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-----------------------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy
	

	-----------------------------------------------------------------------------------------------------------------------
	-- Delete object.
	-----------------------------------------------------------------------------------------------------------------------
	DELETE TOP (1) trgt
	FROM dbo.Configuration trgt
	WHERE trgt.ConfigurationID = @ConfigurationID

	IF @@ROWCOUNT > 0
	BEGIN
		SET @Exists = 1;
	END
	
	
END
