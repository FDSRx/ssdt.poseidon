﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/14/2014
-- Description:	Creates a business entity service umbrella record.
-- SELECT * FROM dbo.BusinessEntityServiceUmbrella
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityServiceUmbrella_Create]
	@BusinessEntityID BIGINT,
	@ServiceID INT,
	@BusinessEntityUmbrellaID BIGINT,
	@CreatedBy VARCHAR(256),
	@BusinessEntityServiceUmbrellaID INT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------
	SET @BusinessEntityServiceUmbrellaID = NULL;
	
	--------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
	
	--------------------------------------------------------------------
	-- Argument Null Exceptions
	--------------------------------------------------------------------
	--------------------------------------------------------------------
	-- Business Entity Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null business entity ID
	-- was passed
	--------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@BusinessEntityID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;	
	END
	
	--------------------------------------------------------------------
	-- Business Entity Umbrella Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null business entity umbrella ID
	-- was passed
	--------------------------------------------------------------------
	IF ISNULL(@BusinessEntityUmbrellaID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@BusinessEntityUmbrellaID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;	
	END
	
	--------------------------------------------------------------------
	-- Service Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null service ID
	-- was passed
	--------------------------------------------------------------------
	IF ISNULL(@ServiceID, 0) = 0
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@ServiceID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END
	
	
	--------------------------------------------------------------------
	-- Create new business service umbrella record.
	--------------------------------------------------------------------
	INSERT INTO dbo.BusinessEntityServiceUmbrella (
		BusinessEntityID,
		ServiceID,
		BusinessEntityUmbrellaID,
		CreatedBy
	)
	SELECT
		@BusinessEntityID AS BusinessEntityID,
		@ServiceID AS ServiceID,
		@BusinessEntityUmbrellaID AS BusinessEntityUmbrellaID,
		@CreatedBy AS CreatedBy
		
	
	-- Return record identity.
	SET @BusinessEntityServiceUmbrellaID = SCOPE_IDENTITY();
	
	
	
	
	
	
	
	
	
	
	
	
END
