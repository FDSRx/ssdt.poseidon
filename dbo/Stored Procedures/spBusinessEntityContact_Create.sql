﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/24/2016
-- Description:	Creates a new BusinessEntityContact object.
-- SAMPLE CALL:
/*

DECLARE
	-- BusinessEntityContact object properties.
	@BusinessEntityID BIGINT = '10684',
	@PersonID BIGINT = NULL,
	@ContactTypeID INT = NULL,
	@ContactTypeCode VARCHAR(50) = NULL,
	-- Person object properties.
	@PersonTypeID INT = NULL,
	@PersonTypeCode VARCHAR(50) = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = 'Daniel',
	@LastName VARCHAR(75) = 'Hughes',
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderID INT = NULL,
	@GenderCode VARCHAR(50) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(50) = NULL,
	--Person Address.
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone.
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email.
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	-- Caller properties.
	@CreatedBy VARCHAR(256) = NULL,
	-- Output
	@BusinessEntityContactID BIGINT = NULL,
	-- Debug properties.
	@Debug BIT = 1


EXEC dbo.spBusinessEntityContact_Create
	@BusinessEntityID = @BusinessEntityID,
	@PersonID = @PersonID,
	@ContactTypeID = @ContactTypeID,
	@ContactTypeCode = @ContactTypeCode,
	@PersonTypeID = @PersonTypeID,
	@PersonTypeCode = @PersonTypeCode,
	@Title = @Title,
	@FirstName = @FirstName,
	@LastName = @LastName,
	@MiddleName = @MiddleName,
	@Suffix = @Suffix,
	@BirthDate = @BirthDate,
	@GenderID = @GenderID,
	@GenderCode = @GenderCode,
	@LanguageID = @LanguageID,
	@LanguageCode = @LanguageCode,
	@AddressLine1 = @AddressLine1,
	@AddressLine2 = @AddressLine2,
	@City = @City,
	@State = @State,
	@PostalCode = @PostalCode,
	@PhoneNumber_Home = @PhoneNumber_Home,
	@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
	@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
	@PhoneNumber_Home_IsPreferred = @PhoneNumber_Home_IsPreferred,
	@PhoneNumber_Mobile = @PhoneNumber_Mobile,
	@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
	@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
	@PhoneNumber_Mobile_IsPreferred = @PhoneNumber_Mobile_IsPreferred,
	@EmailAddress_Primary = @EmailAddress_Primary,
	@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
	@EmailAddress_Primary_IsPreferred = @EmailAddress_Primary_IsPreferred,
	@EmailAddress_Alternate = @EmailAddress_Alternate,
	@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
	@EmailAddress_Alternate_IsPreferred = @EmailAddress_Alternate_IsPreferred,
	@CreatedBy = @CreatedBy,
	@BusinessEntityContactID = @BusinessEntityContactID OUTPUT,
	@Debug = @Debug

SELECT @BusinessEntityContactID AS BusinessEntityContactID, @PersonID AS PersonID

*/

-- SELECT * FROM dbo.BusinessEntityContact
-- SELECT * FROM dbo.Person
-- SELECT * FROM dbo.Store WHERE Name LIKE '%Tommy''s Corner%'
-- =============================================
CREATE PROCEDURE dbo.spBusinessEntityContact_Create
	-- BusinessEntityContact object properties.
	@BusinessEntityID BIGINT,
	@PersonID BIGINT = NULL OUTPUT,
	@ContactTypeID INT = NULL,
	@ContactTypeCode VARCHAR(50) = NULL,
	-- Person object properties.
	@PersonTypeID INT = NULL,
	@PersonTypeCode VARCHAR(50) = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderID INT = NULL,
	@GenderCode VARCHAR(50) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(50) = NULL,
	--Person Address.
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone.
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	@PhoneNumber_Home_IsPreferred BIT = NULL,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	@PhoneNumber_Mobile_IsPreferred BIT = NULL,
	--Person Email.
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	@EmailAddress_Primary_IsPreferred BIT = NULL,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	@EmailAddress_Alternate_IsPreferred BIT = NULL,
	-- Caller properties.
	@CreatedBy VARCHAR(256) = NULL,
	-- Output
	@BusinessEntityContactID BIGINT = NULL OUTPUT,
	-- Debug properties.
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@DebugMessage VARCHAR(4000) = NULL,
		@ErrorMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()



	------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@PersonID=' + dbo.fnToStringOrEmpty(@PersonID) + ';' +
		'@ContactTypeID=' + dbo.fnToStringOrEmpty(@ContactTypeID) + ';' +
		'@ContactTypeCode=' + dbo.fnToStringOrEmpty(@ContactTypeCode) + ';' +
		'@PersonTypeID=' + dbo.fnToStringOrEmpty(@PersonTypeID) + ';' +
		'@PersonTypeCode=' + dbo.fnToStringOrEmpty(@PersonTypeCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@FirstName=' + dbo.fnToStringOrEmpty(@FirstName) + ';' +
		'@LastName=' + dbo.fnToStringOrEmpty(@LastName) + ';' +
		'@MiddleName=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @MiddleName)) + ';' +
		'@Suffix=' + dbo.fnToStringOrEmpty(@Suffix) + ';' +
		'@BirthDate=' + dbo.fnToStringOrEmpty(@BirthDate) + ';' +
		'@GenderID=' + dbo.fnToStringOrEmpty(@GenderID) + ';' +
		'@GenderCode=' + dbo.fnToStringOrEmpty(@GenderCode) + ';' +
		'@LanguageID=' + dbo.fnToStringOrEmpty(@LanguageID) + ';' +
		'@LanguageCode=' + dbo.fnToStringOrEmpty(@LanguageCode) + ';' +
		'@AddressLine1=' + dbo.fnToStringOrEmpty(@AddressLine1) + ';' +
		'@AddressLine2=' + dbo.fnToStringOrEmpty(@AddressLine2) + ';' +
		'@City=' + dbo.fnToStringOrEmpty(@City) + ';' +
		'@State=' + dbo.fnToStringOrEmpty(@State) + ';' +
		'@PostalCode=' + dbo.fnToStringOrEmpty(@PostalCode) + ';' +
		'@PhoneNumber_Home=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home) + ';' +
		'@PhoneNumber_Home_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsCallAllowed) + ';' +
		'@PhoneNumber_Home_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsTextAllowed) + ';' +
		'@PhoneNumber_Home_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Home_IsPreferred) + ';' +
		'@PhoneNumber_Mobile=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile) + ';' +
		'@PhoneNumber_Mobile_IsCallAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsCallAllowed) + ';' +
		'@PhoneNumber_Mobile_IsTextAllowed=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsTextAllowed) + ';' +
		'@PhoneNumber_Mobile_IsPreferred=' + dbo.fnToStringOrEmpty(@PhoneNumber_Mobile_IsPreferred) + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary) + ';'  +
		'@EmailAddress_Primary_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsEmailAllowed) + ';' +
		'@EmailAddress_Primary_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary_IsPreferred) + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate) + ';' +
		'@EmailAddress_Alternate_IsEmailAllowed=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsEmailAllowed) + ';' +
		'@EmailAddress_Alternate_IsPreferred=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate_IsPreferred) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@BusinessEntityContactID=' + dbo.fnToStringOrEmpty(@BusinessEntityContactID) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------------------
	SET @BusinessEntityContactID = NULL;


	------------------------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	------------------------------------------------------------------------------------------------------------------------
	-- @BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null or empty.';
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		  
		RETURN;
	END	

	------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------------------
	-- No local variables.

	------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------------------
	-- Data review
	-- SELECT * FROM dbo.ContactType

	SET @ContactTypeID = COALESCE(dbo.fnGetContactTypeID(@ContactTypeID), dbo.fnGetContactTypeID(@ContactTypeCode), dbo.fnGetContactTypeID('UNKN'));

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables.' AS ActionMethod,
			@BusinessEntityID AS BusinessEntityID, @PersonID AS PersonID, @ContactTypeID AS ContactTypeID,
			@ContactTypeCode AS ContactTypeCode, @PersonTypeID AS PersonTypeID, @PersonTypeCode AS PersonTypeCode, @Title AS Title, 
			@FirstName AS FirstName, @LastName AS LastName, @MiddleName AS MiddleName, @Suffix AS Suffix, @BirthDate AS BirthDate, 
			@GenderID AS GenderID, @GenderCode AS GenderCode, @LanguageID AS LanguageID, @LanguageCode AS LanguageCode, 
			@AddressLine1 AS AddressLine1, @AddressLine2 AS AddressLine2, @City AS City, @State AS State, 
			@PostalCode AS PostalCode, @PhoneNumber_Home AS PhoneNumber_Home, @PhoneNumber_Home_IsCallAllowed AS PhoneNumber_Home_IsCallAllowed, 
			@PhoneNumber_Home_IsTextAllowed AS PhoneNumber_Home_IsTextAllowed, @PhoneNumber_Home_IsPreferred AS PhoneNumber_Home_IsPreferred, 
			@PhoneNumber_Mobile AS PhoneNumber_Mobile, @PhoneNumber_Mobile_IsCallAllowed AS PhoneNumber_Mobile_IsCallAllowed, 
			@PhoneNumber_Mobile_IsTextAllowed AS PhoneNumber_Mobile_IsTextAllowed, @PhoneNumber_Mobile_IsPreferred AS PhoneNumber_Mobile_IsPreferred, 
			@EmailAddress_Primary AS EmailAddress_Primary, @EmailAddress_Primary_IsEmailAllowed AS EmailAddress_Primary_IsEmailAllowed, 
			@EmailAddress_Primary_IsPreferred AS EmailAddress_Primary_IsPreferred, @EmailAddress_Alternate AS EmailAddress_Alternate, 
			@EmailAddress_Alternate_IsEmailAllowed AS EmailAddress_Alternate_IsEmailAllowed, 
			@EmailAddress_Alternate_IsPreferred AS EmailAddress_Alternate_IsPreferred, 
			@CreatedBy AS CreatedBy
	END

	------------------------------------------------------------------------------------------------------------------------
	-- Create Person object.
	-- <Summary>
	-- If a person object identifier was not provided, then try and create a person object based on the provided
	-- person properties (if applicable).
	-- </Summary>
	------------------------------------------------------------------------------------------------------------------------
	IF @PersonID IS NULL
	BEGIN
		EXEC dbo.spPerson_Create
			@PersonTypeID = @PersonTypeID,
			@PersonTypeCode = @PersonTypeCode,
			@Title = @Title,
			@FirstName = @FirstName,
			@LastName = @LastName,
			@MiddleName = @MiddleName,
			@Suffix = @Suffix,
			@BirthDate = @BirthDate,
			@GenderID = @GenderID,
			@GenderCode = @GenderCode,
			@LanguageID = @LanguageID,
			@LanguageCode = @LanguageCode,
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			@PhoneNumber_Home = @PhoneNumber_Home,
			@PhoneNumber_Home_IsCallAllowed = @PhoneNumber_Home_IsCallAllowed,
			@PhoneNumber_Home_IsTextAllowed = @PhoneNumber_Home_IsTextAllowed,
			@PhoneNumber_Home_IsPreferred = @PhoneNumber_Home_IsPreferred,
			@PhoneNumber_Mobile = @PhoneNumber_Mobile,
			@PhoneNumber_Mobile_IsCallAllowed = @PhoneNumber_Mobile_IsCallAllowed,
			@PhoneNumber_Mobile_IsTextAllowed = @PhoneNumber_Mobile_IsTextAllowed,
			@PhoneNumber_Mobile_IsPreferred = @PhoneNumber_Mobile_IsPreferred,
			@EmailAddress_Primary = @EmailAddress_Primary,
			@EmailAddress_Primary_IsEmailAllowed = @EmailAddress_Primary_IsEmailAllowed,
			@EmailAddress_Primary_IsPreferred = @EmailAddress_Primary_IsPreferred,
			@EmailAddress_Alternate = @EmailAddress_Alternate,
			@EmailAddress_Alternate_IsEmailAllowed = @EmailAddress_Alternate_IsEmailAllowed,
			@EmailAddress_Alternate_IsPreferred = @EmailAddress_Alternate_IsPreferred,
			@CreatedBy = @CreatedBy,
			@IgnorePersonValidation = 1,
			@ThrowCaughtException = 1,
			@BusinessEntityID = @PersonID OUTPUT,
			@Debug = @Debug
	END


	------------------------------------------------------------------------------------------------------------------------
	-- Create object.
	------------------------------------------------------------------------------------------------------------------------
	-- Data reivew.
	-- SELECT TOP 1 * FROM dbo.BusinessEntityContact

	INSERT INTO dbo.BusinessEntityContact (
		BusinessEntityID,
		PersonID,
		ContactTypeID,
		CreatedBy
	)
	SELECT
		@BusinessEntityID AS BusinessEntityID,
		@PersonID AS PersonID,
		@ContactTypeID AS ContactTypeID,
		@CreatedBy AS CreatedBy

	-- Retrieve object identifier.
	SET @BusinessEntityContactID = SCOPE_IDENTITY();
		

END
