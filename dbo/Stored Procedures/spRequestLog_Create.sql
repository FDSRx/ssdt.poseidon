﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 4/8/2014
-- Description:	Creates a new request entry.
-- SAMPLE CALL: 
/*
DECLARE @RequestLogID BIGINT = NULL

EXEC dbo.spRequestLog_Create
	@RequestTypeID = 1,
	@SourceName = 'dbo.spRequestLog_Create',
	@ApplicationKey = 'MPC',
	@BusinessKey = '000000',
	@RequesterKey = 'dhughes',
	@RequestData = 'Test Call',
	@CreatedBy = 'dhughes',
	@RequestLogID = @RequestLogID OUTPUT
	
SELECT @RequestLogID AS RequestLogID


*/

-- SELECT * FROM dbo.RequestLog ORDER BY RequestLogID DESC
-- =============================================
CREATE PROCEDURE [dbo].[spRequestLog_Create]
	@RequestTypeID INT,
	@SourceName VARCHAR(256) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@RequesterKey VARCHAR(256) = NULL,
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@RequesterID BIGINT = NULL,
	@RequestData VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@RequestLogID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------
	SET @RequestLogID = NULL;
	
	------------------------------------------------------------
	-- Create request entry
	------------------------------------------------------------
	INSERT INTO dbo.RequestLog (
		RequestTypeID,
		SourceName,
		ApplicationKey,
		BusinessKey,
		RequesterKey,
		ApplicationID,
		BusinessEntityID,
		RequesterID,
		RequestData,
		Arguments,
		CreatedBy
	)
	SELECT
		@RequestTypeID,
		@SourceName,
		@ApplicationKey,
		@BusinessKey,
		@RequesterKey,
		@ApplicationID,
		@BusinessEntityID,
		@RequesterID,		
		@RequestData,
		@Arguments,
		@CreatedBy
		
	-- Retrieve record identity.
	SET @RequestLogID = SCOPE_IDENTITY();	
	
END
