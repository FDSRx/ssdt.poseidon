﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/24/2016
-- Description:	Deletes a BusinessEntityContact object(s).
-- SAMPLE CALL:
/*

DECLARE
	@BusinessEntityContactID VARCHAR(MAX) = 1,
	@ModifiedBy VARCHAR(256) = 'dughes',
	-- Debug properties.
	@Debug BIT = 1


EXEC dbo.spBusinessEntityContact_Delete
	@BusinessEntityContactID = @BusinessEntityContactID,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

SELECT @BusinessEntityContactID AS BusinessEntityContactID

*/

-- SELECT * FROM dbo.BusinessEntityContact
-- SELECT * FROM dbo.Person
-- SELECT * FROM dbo.Store WHERE Name LIKE '%Tommy''s Corner%'
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityContact_Delete]
	-- BusinessEntityContact object properties.
	@BusinessEntityContactID BIGINT = NULL OUTPUT,
	@ModifiedBy VARCHAR(256) = NULL,
	-- Debug properties.
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@DebugMessage VARCHAR(4000) = NULL,
		@ErrorMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()



	------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityContactID=' + dbo.fnToStringOrEmpty(@BusinessEntityContactID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------------------
	SET @ModifiedBy = ISNULL(@ModifiedBy, SUSER_NAME());


	/*
	------------------------------------------------------------------------------------------------------------------------
	-- Argument Validation.
	-- <Summary>
	-- Inspects the incoming arguments and determines if they are valid for processing.
	-- </Summary>
	------------------------------------------------------------------------------------------------------------------------
	-- @BusinessEntityContactID
	IF @BusinessEntityContactID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference not set to an instance of an object. ' +
			'The parameter, @BusinessEntityContactID, cannot be null or empty.';
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		  
		RETURN;
	END	
	*/

	------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------------------
	-- No local variables.

	------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------------------
	-- No instantiation necessary.

	------------------------------------------------------------------------------------------------------------------------
	-- Delete object.
	------------------------------------------------------------------------------------------------------------------------
	DELETE trgt
	FROM dbo.BusinessEntityContact trgt
	WHERE trgt.BusinessEntityContactID IN (SELECT Value FROM dbo.fnSplit(@BusinessEntityContactID, ',') WHERE ISNUMERIC(Value) = 1)


		

END
