﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/2/2015
-- Description:	Creates/Updates a BusinessEntityApplication object.
-- SAMPLE CALL:
/*

DECLARE 
	@BusinessEntityApplicationID BIGINT = NULL,
	@BusinessEntityID BIGINT = 10684,
	@ApplicationID INT = 10,
	@ApplicationPath VARCHAR(512) = NULL,
	@IsApplicationPathNullable BIT = NULL,
	@CreatedBy VARCHAR(256) = 'dhughes',
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@Exists BIT = NULL OUTPUT,
	@Debug BIT = 1
	

EXEC dbo.spBusinessEntityApplication_Merge
	@BusinessEntityApplicationID = @BusinessEntityApplicationID OUTPUT,
	@BusinessEntityID = @BusinessEntityID,
	@ApplicationID = @ApplicationID,
	@ApplicationPath = @ApplicationPath,
	@IsApplicationPathNullable = @IsApplicationPathNullable,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@Exists = @Exists OUTPUT

SELECT @BusinessEntityApplicationID AS BusinessEntityApplicationID, @Exists AS IsFound

*/

-- SElECT * FROM dbo.Application
-- SELECT * FROM dbo.BusinessEntity
-- SELECT * FROM dbo.BusinessEntityApplication
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityApplication_Merge]
	@BusinessEntityApplicationID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@ApplicationPath VARCHAR(512) = NULL,
	@IsApplicationPathNullable BIT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000)

	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityApplicationID=' + dbo.fnToStringOrEmpty(@BusinessEntityApplicationID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationPath=' + dbo.fnToStringOrEmpty(@ApplicationPath) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Exists=' + dbo.fnToStringOrEmpty(@Exists) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;
	
	-------------------------------------------------------------------------------------------------------
	-- Record request.
	-------------------------------------------------------------------------------------------------------
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	-------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------
	SET @Exists = 0;
	SET @Debug = ISNULL(@Debug, 0);
	SET @CreatedBy = COALESCE(@CreatedBy, @ModifiedBy, SUSER_NAME());
	SET @ModifiedBy = COALESCE(@ModifiedBy, @CreatedBy);
	SET @IsApplicationPathNullable = ISNULL(@IsApplicationPathNullable, 0);
	
	-------------------------------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------------------------------
		

	/*
	-------------------------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------------------------------
	-- BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplication object. Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @ApplicationID
	IF @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplication object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/
		
	-------------------------------------------------------------------------------------------------------
	-- Determine if the object exists.
	-------------------------------------------------------------------------------------------------------
	EXEC dbo.spBusinessEntityApplication_Exists
		@BusinessEntityID = @BusinessEntityID,
		@ApplicationID = @ApplicationID,
		@BusinessEntityApplicationID = @BusinessEntityApplicationID OUTPUT,
		@Exists = @Exists OUTPUT
	
	-------------------------------------------------------------------------------------------------------
	-- Merge record.
	-- <Summary>
	-- If the record does not exist, then create a new
	-- object; otherwise, do nothing.
	-- </Summary>
	-------------------------------------------------------------------------------------------------------
	-- If an object was not discovered, then create a new
	-- record.
	IF @BusinessEntityApplicationID IS NULL
	BEGIN
	
		-- Create a new object.
		EXEC dbo.spBusinessEntityApplication_Create
			@BusinessEntityID = @BusinessEntityID,
			@ApplicationID = @ApplicationID,
			@ApplicationPath = @ApplicationPath,
			@CreatedBy = @CreatedBy,
			@BusinessEntityApplicationID = @BusinessEntityApplicationID OUTPUT,
			@Debug = @Debug
	
	END
	-- If a record was discovered then do nothing.
	ELSE
	BEGIN
	
		-- Update object.
		EXEC dbo.spBusinessEntityApplication_Update
			@BusinessEntityApplicationID = @BusinessEntityApplicationID,
			@BusinessEntityID = @BusinessEntityID,
			@ApplicationID = @ApplicationID,
			@ApplicationPath = @ApplicationPath,
			@IsApplicationPathNullable = @IsApplicationPathNullable,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug
		
	END
	
	
		
		

		
END
