﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/24/2014
-- Description:	Updates a BusinessEntityServiceUmbrella object.
/*

EXEC dbo.spBusinessEntityServiceUmbrella_Update
	@BusinessEntityServiceUmbrellaID = NULL,
	@BusinessEntityID = 1,
	@ServiceID = 4,
	@BusinessEntityUmbrellaID = 5032,
	@ModifiedBy = 'dhughes'

*/

-- SELECT * FROM BusinessEntityServiceUmbrella
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityServiceUmbrella_Update]
	@BusinessEntityServiceUmbrellaID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@ServiceID INT = NULL,
	@BusinessEntityUmbrellaID BIGINT,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);

	--------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidateUniqueRowKey BIT = 1
	
	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------	
	IF @BusinessEntityServiceUmbrellaID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @ServiceID = NULL;
		
		SET @ValidateUniqueRowKey = 0;
	END
	
	--------------------------------------------------------------------
	-- Argument Null Exceptions
	--------------------------------------------------------------------
	
	IF @BusinessEntityServiceUmbrellaID IS NULL AND
		@BusinessEntityID IS NULL AND
		@ServiceID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object references (@BusinessEntityServiceUmbrellaID, @BusinessEntityID, @ServiceID) are not set to an instance ' +
			'of an object. The objects (@BusinessEntityServiceUmbrellaID, @BusinessEntityID, @ServiceID) cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;	
	END
	
	--------------------------------------------------------------------
	-- Business Entity Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null business entity ID
	-- was passed
	--------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0 AND @ValidateUniqueRowKey = 1
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@BusinessEntityID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;	
	END	
	
	--------------------------------------------------------------------
	-- Service Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null service ID
	-- was passed
	--------------------------------------------------------------------
	IF ISNULL(@ServiceID, 0) = 0 AND @ValidateUniqueRowKey = 1
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@ServiceID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	-- Debug
	--SELECT @BusinessEntityServiceUmbrellaID AS BusinessEntityServiceUmbrellaID, @BusinessEntityID AS BusinessEntityID, @ServiceID AS ServiceID

	--------------------------------------------------------------------
	-- Update the record.
	-- <Summary>
	-- Clone the @BusinessEntityServiceUmbrellaID and destroy the original.
	-- We want to ensure the caller retrieves the correct results when
	-- fetching the data. If the Id is found, the it will be recreated during
	-- assignment.
	-- </Summary>
	--------------------------------------------------------------------
	DECLARE
		@BusinessEntityServiceUmbrellaID_Clone BIGINT = @BusinessEntityServiceUmbrellaID
	
	SET @BusinessEntityServiceUmbrellaID = NULL;
	
	DECLARE @tblOutput AS TABLE (
		BusinessEntityServiceUmbrellaID BIGINT
	);
	
	UPDATE umb
	SET
		umb.BusinessEntityUmbrellaID = @BusinessEntityUmbrellaID,
		umb.DateModified = GETDATE(),
		umb.ModifiedBy = @ModifiedBy
	OUTPUT inserted.BusinessEntityServiceUmbrellaID INTO @tblOutput(BusinessEntityServiceUmbrellaID)
	--SELECT *
	FROM dbo.BusinessEntityServiceUmbrella umb
	WHERE (BusinessEntityServiceUmbrellaID = @BusinessEntityServiceUmbrellaID_Clone)
		OR ( BusinessEntityID = @BusinessEntityID AND ServiceID = @ServiceID )
	
	-- Debug
	--SELECT * FROM @tblOutput
	
	-- Set unique identifier and exists flag.
	SET @BusinessEntityServiceUmbrellaID = (SELECT TOP 1 BusinessEntityServiceUmbrellaID FROM @tblOutput);
	SET @Exists = CASE WHEN @BusinessEntityServiceUmbrellaID IS NOT NULL THEN 1 ELSE 0 END;




END
