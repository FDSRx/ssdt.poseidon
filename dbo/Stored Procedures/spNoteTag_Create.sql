﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Creates a new note tag item.
-- SAMPLE CALL:
/*
DECLARE @NoteTagID BIGINT

EXEC dbo.spNoteTag_Create
	@NoteID = 1,
	@TagID = 1,
	@NoteTagID = @NoteTagID OUTPUT

SELECT @NoteTagID AS NoteTagID
*/
-- SELECT * FROM dbo.NoteTag
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Tag
-- =============================================
CREATE PROCEDURE [dbo].[spNoteTag_Create]
	@NoteID BIGINT,
	@TagID BIGINT,
	@NoteTagID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @NoteTagID = NULL;
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000) = NULL
	
	----------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	----------------------------------------------------------------------
	-- A Note object and Tag object are required to create a NoteTag object.
	IF @NoteID IS NULL OR @TagID IS NULL
	BEGIN
	
		SET @ErrorMessage = 'Unable to create a NoteTag object.  Object references (@NoteID or @TagID) are not set to an instance of an object. ' +
			'The @NoteID and @TagID cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	
	----------------------------------------------------------------------
	-- Create new NoteTag object.
	----------------------------------------------------------------------	
	INSERT INTO dbo.NoteTag (
		NoteID,
		TagID
	)
	SELECT
		@NoteID,
		@TagID
	
	-- Retrieve record identity
	SET @NoteTagID = SCOPE_IDENTITY();
	
END
