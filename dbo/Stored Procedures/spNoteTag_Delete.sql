﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/19/2015
-- Description:	Deletes a NoteTag item(s).
-- Change Log:
-- 11/12/2015 - dhughes - Modified the delete statement by removing the "OR" clause and wrapping the identifier deletes in their
--							own "IF" statements to improve speed and performance.
-- SAMPLE CALL:
/*

DECLARE
	@NoteTagID VARCHAR(MAX) = NULL,
	@NoteID VARCHAR(MAX) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = 1

EXEC dbo.spNoteTag_Delete
	@NoteTagID = @NoteTagID,
	@NoteID = @NoteID,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/


-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.NoteTag
-- SELECT * FROM dbo.vwNoteTag ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spNoteTag_Delete]
	@NoteTagID VARCHAR(MAX) = NULL,
	@NoteID VARCHAR(MAX) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	----------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) = 
		'@NoteTagID=' + dbo.fnToStringOrEmpty(@NoteTagID)  + ';' +
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID)  + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;
	

	----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------------------------------------------------
	SET @NoteTagID = NULLIF(@NoteTagID, '');
	SET @NoteID = NULLIF(@NoteID, '');

	----------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	----------------------------------------------------------------------------------------------------------------------------
			
	----------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	----------------------------------------------------------------------------------------------------------------------------	
	SET @NoteID = 
		CASE 
			WHEN @NoteTagID IS NOT NULL THEN NULL 
			ELSE @NoteID 
		END;

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS procedureName, 'Get/Set variables.' AS ActionMethod,
			@NoteTagID AS NoteTagID, @NoteID AS NoteID, @ModifiedBy AS ModifiedBy
	END


	----------------------------------------------------------------------------------------------------------------------------
	-- Short circuit.
	----------------------------------------------------------------------------------------------------------------------------
	IF @NoteTagID IS NULL AND @NoteID IS NULL
	BEGIN
		RETURN;
	END
		

	----------------------------------------------------------------------------------------------------------------------------
	-- Remove note item record.
	----------------------------------------------------------------------------------------------------------------------------
	-- Use primary identifier to remove item.
	IF @NoteTagID IS NOT NULL
	BEGIN
		DELETE trgt
		--SELECT *
		FROM dbo.NoteTag trgt
		WHERE trgt.NoteTagID IN (SELECT Value FROM dbo.fnSplit(@NoteTagID, ',') WHERE ISNUMERIC(Value) = 1)
	END

	-- If a primary identifier is not supplied, then remove all items associated with the note identifier.
	IF @NoteTagID IS NULL AND @NoteID IS NOT NULL
	BEGIN
		DELETE trgt
		--SELECT *
		FROM dbo.NoteTag trgt
		WHERE trgt.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteID, ',') WHERE ISNUMERIC(Value) = 1)
	END




END
