﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Compare password
-- SAMPLE CALL:
/*
DECLARE @IsValid BIT
EXEC spPassword_Compare 1, 1, 'Password1', @IsValid OUTPUT
SELECT @IsValid
*/
-- =============================================
CREATE PROCEDURE spPassword_Compare
	@PersonID INT,
	@ServiceID INT,
	@Password VARCHAR(128),
	@IsValid BIT = 0 OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------
	-- Local variables
	-----------------------------------------------------
	DECLARE
		@PasswordHash VARBINARY(128),
		@PasswordSalt VARBINARY(10)
	
	
	-----------------------------------------------------
	-- Retrieve current password
	-----------------------------------------------------
	EXEC spPassword_Get	
		@PersonID = @PersonID,
		@ServiceID = @ServiceID,
		@PasswordHash = @PasswordHash OUTPUT,
		@PasswordSalt = @PasswordSalt OUTPUT
	
	-----------------------------------------------------
	-- COMPARE
	-----------------------------------------------------
	SET @IsValid = dbo.fnPasswordCompare(@Password + CONVERT(VARCHAR(MAX), @PasswordSalt), @PasswordHash);
	
END
