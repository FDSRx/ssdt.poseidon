﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/15/2016
-- Description:	Updates a Banner object.
-- SAMPLE CALL:
/*

DECLARE
	@BannerID BIGINT = NULL,
	@BannerTypeID INT = NULL,
	@BannerTypeCode VARCHAR(50) = NULL,
	@Name VARCHAR(256) = NULL,
	@Header VARCHAR(MAX) = NULL,
	@Body VARCHAR(MAX) = NULL,
	@Footer VARCHAR(MAX) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL

EXEC dbo.spBanner_Update
	@BannerID = @BannerID,
	@BannerTypeID = @BannerTypeID,
	@BannerTypeCode = @BannerTypeCode,
	@Name = @Name,
	@Header = @Header,
	@Body = @Body,
	@Footer = @Footer,
	@LanguageID = @LanguageID,
	@LanguageCode = @LanguageCode,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

SELECT @BannerID AS BannerID


*/

-- SELECT * FROM dbo.Banner
-- =============================================
CREATE PROCEDURE [dbo].[spBanner_Update]
	@BannerID BIGINT = NULL,
	@BannerTypeID INT = NULL,
	@BannerTypeCode VARCHAR(50) = NULL,
	@Name VARCHAR(256) = NULL,
	@Header VARCHAR(MAX) = NULL,
	@Body VARCHAR(MAX) = NULL,
	@Footer VARCHAR(MAX) = NULL,
	@LanguageID INT = NULL,
	@LanguageCode VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	----------------------------------------------------------------------------------------------------
	-- Instance variables.
	----------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@BannerTypeID=' + dbo.fnToStringOrEmpty(@BannerTypeID) + ';' +
		'@BannerTypeCode=' + dbo.fnToStringOrEmpty(@BannerTypeCode) + ';' +
		'@Name=' + dbo.fnToStringOrEmpty(@Name) + ';' +
		'@Header=' + dbo.fnToStringOrEmpty(@Header) + ';' +
		'@Body=' + dbo.fnToStringOrEmpty(@Body) + ';' +
		'@Footer=' + dbo.fnToStringOrEmpty(@Footer) + ';' +
		'@LanguageID=' + dbo.fnToStringOrEmpty(@LanguageID) + ';' +
		'@LanguageCode=' + dbo.fnToStringOrEmpty(@LanguageCode) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	----------------------------------------------------------------------------------------------------
	-- Log request.
	----------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	----------------------------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @ModifiedBy = COALESCE(@ModifiedBy, SUSER_NAME());

	----------------------------------------------------------------------------------------------------
	-- Set variables.
	----------------------------------------------------------------------------------------------------
	SET @BannerTypeID = ISNULL(dbo.fnGetBannerTypeID(@BannerTypeID), dbo.fnGetBannerTypeID(@BannerTypeCode));
	SET @LanguageID = ISNULL(dbo.fnGetLanguageID(@LanguageID), dbo.fnGetLanguageID(@LanguageCode));

	----------------------------------------------------------------------------------------------------
	-- Create object.
	----------------------------------------------------------------------------------------------------
	BEGIN TRY

		UPDATE trgt
		SET
			trgt.BannerTypeID = ISNULL(@BannerTypeID, trgt.BannerTypeID),
			trgt.Name = ISNULL(@Name, trgt.Name),
			trgt.Header = ISNULL(@Header, trgt.Header),
			trgt.Body = ISNULL(@Body, trgt.Body),
			trgt.Footer = ISNULL(@Footer, trgt.Footer),
			trgt.LanguageID = ISNULL(@LanguageID, trgt.LanguageID),
			trgt.DateModified = GETDATE(),
			trgt.ModifiedBy = @ModifiedBy
		FROM dbo.Banner trgt
		WHERE BannerID = @BannerID


	END TRY
	BEGIN CATCH

		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args

		SET @ErrorMessage = ERROR_MESSAGE();
	
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);		

	END CATCH

END