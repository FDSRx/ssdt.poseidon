﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Indicates whether the provided note tag reference exists.
-- SAMPLE CALL:
/*
DECLARE 
	@NoteTagID BIGINT = 1,
	@Exists BIT
	

EXEC dbo.spNoteTag_Exists
	@NoteTagID = @NoteTagID OUTPUT,
	@NoteID = 1,
	@TagID = 1,
	@Exists = @Exists OUTPUT

SELECT @NoteTagID AS NoteTagID, @Exists AS IsFound
*/
-- SELECT * FROM dbo.NoteTag
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Tag
-- =============================================
CREATE PROCEDURE [dbo].[spNoteTag_Exists]
	@NoteTagID BIGINT = NULL OUTPUT,
	@NoteID BIGINT = NULL,
	@TagID BIGINT = NULL,
	@TagName VARCHAR(500) = NULL,
	@Exists BIT = NULL OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @Exists = 0;
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000) = NULL,
		@ValidateRowKey BIT = 1
	
	----------------------------------------------------------------------
	-- Interogate arguments and determine validation.
	-- <Summary>
	-- Inspects the argument collection and determines which arguments
	-- should be used to validate and used for the lookup.
	-- </Summary>
	----------------------------------------------------------------------
	-- @NoteTagID will be used if provided.
	IF ISNULL(@NoteTagID, 0) <> 0
	BEGIN
		SET @NoteID = NULL;
		SET @TagID = NULL;
		SET @TagName = NULL;
		
		SET @ValidateRowKey = 0;
	END
	
	----------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	----------------------------------------------------------------------	
	IF @NoteTagID IS NULL
	BEGIN
	
		-- A Note object and Tag object are required to create a NoteTag object.
		EXEC dbo.spTag_Exists
			@TagID = @TagID OUTPUT,
			@Name = @TagName
			
	END
		
	IF ISNULL(@NoteID, 0) = 0 AND ISNULL(@TagID, 0) = 0 AND @ValidateRowKey = 1
	BEGIN
	
		SET @ErrorMessage = 'Unable to verify NoteTag object existence.  Object references (@NoteID and ( @TagID or @TagName )) are not set to an instance of an object. ' +
			'The @NoteID and ( @TagID or @TagName ) cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	
	--------------------------------------------------------------
	-- Determine if tag exists
	--------------------------------------------------------------
	SET @NoteTagID = (
		SELECT TOP 1
			NoteTagID
		FROM dbo.NoteTag
		WHERE (
			NoteTagID = @NoteTagID OR 
			( NoteID = @NoteID AND TagID = @TagID) 
		)
	);
	
	-- Set the exists flag based on the lookup.
	SET @Exists = CASE WHEN @NoteTagID IS NOT NULL THEN 1 ELSE 0 END;
	
END
