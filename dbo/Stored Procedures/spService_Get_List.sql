﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/14/2013
-- Description:	Returns a list of all services.
-- SAMPLE CALL: spService_Get_List
-- =============================================
CREATE PROCEDURE spService_Get_List
	@ServiceID INT = NULL,
	@ServiceCode VARCHAR(25) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		ServiceID,
		Code,
		Name,
		Description,
		DateCreated,
		DateModified
	FROM dbo.Service
	WHERE ServiceID = ISNULL(@ServiceID, ServiceID)
	
END
