﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/29/2016
-- Description:	Updates a CalendarItem object and its attached schedule (if applicable).
-- Change log:


-- SAMPLE CALL:
/*

DECLARE 
	@NoteID BIGINT = 144,
	@ScopeID INT,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = 'Test Calendar Item On ' + CONVERT(VARCHAR, GETDATE()),
	@Memo VARCHAR(MAX) = 'This is a test calendar item from the stored procedure #: ' + CONVERT(VARCHAR, RAND()),
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(59) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@CorrelationKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@Location VARCHAR(500) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@IsAllDayEvent BIT = NULL,
	@FrequencyTypeID INT = NULL,
	@FrequencyTypeCode VARCHAR(50) = NULL,
	@DateEffectiveStart DATETIME = NULL,
	@DateEffectiveEnd DATETIME = NULL,
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@Debug BIT = 1


EXEC dbo.spCalendarItem_Schedule_Update
	@NoteID = @NoteID,
	@ScopeID = @ScopeID,
	@ScopeCode = @ScopeCode,
	@BusinessEntityID = @BusinessEntityID,
	@NotebookID = @NotebookID,
	@NotebookCode = @NotebookCode,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityID = @PriorityID,
	@PriorityCode = @PriorityCode,
	@Tags = @Tags,
	@CorrelationKey = @CorrelationKey,
	@AdditionalData = @AdditionalData,
	@Location = @Location,
	@OriginID = @OriginID,
	@OriginCode = @OriginCode,
	@OriginDataKey = @OriginDataKey,
	@DateStart = @DateStart,
	@DateEnd = @DateEnd,
	@IsAllDayEvent = @IsAllDayEvent,
	@FrequencyTypeID = @FrequencyTypeID,
	@FrequencyTypeCode = @FrequencyTypeCode,
	@DateEffectiveStart = @DateEffectiveStart,
	@DateEffectiveEnd = @DateEffectiveEnd,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug


SELECT @NoteID AS NoteID

*/

-- SELECT * FROM dbo.CalendarItem
-- SELECT * FROM dbo.CalendarItem_History
-- SELECT * FROM schedule.NoteSchedule
-- SELECT * FROM schedule.NoteSchedule_History
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Tag
-- SELECT * FROM dbo.NoteTag
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook
-- SELECT * FROM dbo.Priority
-- SELECT TOP 10 * FROM phrm.vwPatient WHERE LastName LIKE '%Simmons%'

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spCalendarItem_Schedule_Update]
	@NoteID VARCHAR(MAX) = NULL,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(59) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@CorrelationKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@Location VARCHAR(500) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@IsAllDayEvent BIT = NULL,
	@FrequencyTypeID INT = NULL,
	@FrequencyTypeCode VARCHAR(50) = NULL,
	@DateEffectiveStart DATETIME = NULL,
	@DateEffectiveEnd DATETIME = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------------------
	-- Instance variables
	--------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000)
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode) + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID) + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID) + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@Location=' + dbo.fnToStringOrEmpty(@Location) + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID) + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart) + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd) + ';' +
		'@IsAllDayEvent=' + dbo.fnToStringOrEmpty(@IsAllDayEvent) + ';' +
		'@FrequencyTypeID=' + dbo.fnToStringOrEmpty(@FrequencyTypeID) + ';' +
		'@FrequencyTypeCode=' + dbo.fnToStringOrEmpty(@FrequencyTypeCode) + ';' +
		'@DateEffectiveStart=' + dbo.fnToStringOrEmpty(@DateEffectiveStart) + ';' +
		'@DateEffectiveEnd=' + dbo.fnToStringOrEmpty(@DateEffectiveEnd) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	
		
	--------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);

	--------------------------------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------------------------------
	DECLARE
		@NoteScheduleID VARCHAR(MAX) = NULL	
	
	--------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	--------------------------------------------------------------------------------------------------------------------
	SET @NoteScheduleID = (
		SELECT
			CONVERT(VARCHAR, NoteScheduleID) + ','
		FROM schedule.NoteSchedule ns
			JOIN (
				SELECT 
					Value AS NoteID
				FROM dbo.fnSplit(@NoteID, ',')
				WHERE ISNUMERIC(Value) = 1
			) tmp
				ON tmp.NoteID = ns.NoteID
		FOR XML PATH('')
	);

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ScopeID AS ScopeID, @ScopeCode AS ScopeCode, @NotebookID AS NotebookID, @NotebookCode AS NotebookCode, @Title AS Title, 
			@Memo AS Memo, @PriorityID AS PriorityID, @PriorityCode AS PriorityCode, @Tags AS Tags, @CorrelationKey AS CorrelationKey, 
			@AdditionalData AS AdditionalData, @Location AS Location, @OriginID AS OriginID, @OriginCode AS OriginCode, 
			@OriginDataKey AS OriginDataKey, @DateStart AS DateStart, @DateEnd AS DateEnd, @IsAllDayEvent AS IsAllDayEvent, 
			@FrequencyTypeID AS FrequencyTypeID, @FrequencyTypeCode AS FrequencyTypeCode, @DateEffectiveStart AS DateEffectiveStart, 
			@DateEffectiveEnd AS DateEffectiveEnd, @ModifiedBy AS ModifiedBy, @NoteScheduleID AS NoteScheduleID
	END		
	
	
	BEGIN TRY
	
		--------------------------------------------------------------------------------------------------------------------
		-- Null argument validation
		-- <Summary>
		-- Validates if any of the necessary arguments were provided with
		-- data.
		-- </Summary>
		--------------------------------------------------------------------------------------------------------------------
		-- No argument validation.
		
	--------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
		
		--------------------------------------------------------------------------------------------------------------------
		-- Update NoteSchedule object.
		--------------------------------------------------------------------------------------------------------------------		
		EXEC schedule.spNoteSchedule_Update
			@NoteScheduleID = @NoteScheduleID,
			@DateStart = @DateStart,
			@DateEnd = @DateEnd,
			@IsAllDayEvent = @IsAllDayEvent,
			@FrequencyTypeID = @FrequencyTypeID,
			@FrequencyTypeCode = @FrequencyTypeCode,
			@DateEffectiveStart = @DateEffectiveStart,
			@DateEffectiveEnd = @DateEffectiveEnd,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug


		--------------------------------------------------------------------------------------------------------------------
		-- Update CalendarItem object.
		--------------------------------------------------------------------------------------------------------------------		
		EXEC dbo.spCalendarItem_Update
			@NoteID = @NoteID,
			@ScopeID = @ScopeID,
			@ScopeCode = @ScopeCode,
			@BusinessEntityID = @BusinessEntityID,
			@NotebookID = @NotebookID,
			@NotebookCode = @NotebookCode,
			@PriorityID = @PriorityID,
			@PriorityCode = @PriorityCode,
			@Title = @Title,
			@Memo = @Memo,
			@Tags = @Tags,
			@CorrelationKey = @CorrelationKey,
			@AdditionalData = @AdditionalData,
			@OriginID = @OriginID,
			@OriginCode = @OriginCode,
			@OriginDataKey = @OriginDataKey,
			@Location = @Location,
			@ModifiedBy = @ModifiedBy
			
	
	--------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		SET @ErrorMessage = ERROR_MESSAGE();
	
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		
	END CATCH		

END
