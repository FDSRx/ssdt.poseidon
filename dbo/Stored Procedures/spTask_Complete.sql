﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/7/2014
-- Description:	Marks a Task object(s) as being completed.
-- Change Log:
-- 11/9/2015 - dhughes - Modified the procedure to receive a list of Note objects vs. a single object so that it is able
--					to perform batch updates (when applicable).
-- 6/10/2016 - dhughes - Modified the procedure to accept the Application and Business objects as parameters.

-- SAMPLE CALL:
/*

DECLARE 
	-- Tier properties.
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	-- Task properties
	@NoteID VARCHAR(MAX),
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusID INT = NULL,
	@TaskStatusCode VARCHAR(50) = NULL,
	-- Note interaction properties
	@NoteInteractionID BIGINT = NULL OUTPUT,
	@InteractionID BIGINT = NULL OUTPUT,
	@InteractedWithID BIGINT = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeID INT = NULL,
	@ContactTypeCode VARCHAR(50) = NULL,
	-- Interaction properties
	@InteractionTypeID INT = NULL,
	@InteractionTypeCode VARCHAR(50) = NULL,
	@DispositionTypeID INT = NULL,
	@DispositionTypeCode VARCHAR(50) = NULL,
	@InitiatedByID BIGINT = NULL,
	@InitiatedByString VARCHAR(256) = NULL,
	-- Comment Note
	@CommentNoteID BIGINT = NULL OUTPUT,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(50) = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	-- Entry properties
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL

EXEC dbo.spTask_Complete
	@ApplicationID = @ApplicationID,
	@ApplicationCode = @ApplicationCode,
	@BusinessID = @BusinessID,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@BusinessEntityID = @BusinessEntityID,
	@NoteID = @NoteID,
	@CompletedByID = @CompletedByID,
	@CompletedByString = @CompletedByString,
	@DateCompleted = @DateCompleted,
	@TaskStatusID = @TaskStatusID,
	@TaskStatusCode = @TaskStatusCode,
	@NoteInteractionID = @NoteInteractionID OUTPUT,
	@InteractionID = @InteractionID OUTPUT,
	@InteractedWithID = @InteractedWithID,
	@InteractedWithString = @InteractedWithString,
	@ContactTypeID = @ContactTypeID,
	@ContactTypeCode = @ContactTypeCode,
	@InteractionTypeID = @InteractionTypeID,
	@InteractionTypeCode = @InteractionTypeCode,
	@DispositionTypeID = @DispositionTypeID,
	@DispositionTypeCode = @DispositionTypeCode,
	@InitiatedByID = @InitiatedByID,
	@InitiatedByString = @InitiatedByString,
	@CommentNoteID = @CommentNoteID OUTPUT,
	@ScopeID = @ScopeID,
	@ScopeCode = @ScopeCode,
	@NoteTypeID = @NoteTypeID,
	@NoteTypeCode = @NoteTypeCode,
	@NotebookID = @NotebookID,
	@NotebookCode = @NotebookCode,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityID = @PriorityID,
	@PriorityCode = @PriorityCode,
	@Tags = @Tags,
	@OriginID = @OriginID,
	@OriginCode = @OriginCode,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug


SELECT @NoteID AS NoteID, @InteractionID AS InteractionID, @NoteInteractionID AS NoteInteractionID

*/


-- SELECT * FROM dbo.vwTask
-- SELECT * FROM dbo.vwNote
-- SELECT * FROM dbo.NoteInteraction

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spTask_Complete]
	-- Tier properties.
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	-- Task properties
	@NoteID VARCHAR(MAX),
	@CompletedByID BIGINT = NULL,
	@CompletedByString VARCHAR(256) = NULL,
	@DateCompleted DATETIME = NULL,
	@TaskStatusID INT = NULL,
	@TaskStatusCode VARCHAR(50) = NULL,
	-- Note interaction properties
	@NoteInteractionID BIGINT = NULL OUTPUT,
	@InteractionID BIGINT = NULL OUTPUT,
	@InteractedWithID BIGINT = NULL,
	@InteractedWithString VARCHAR(256) = NULL,
	@ContactTypeID INT = NULL,
	@ContactTypeCode VARCHAR(50) = NULL,
	-- Interaction properties
	@InteractionTypeID INT = NULL,
	@InteractionTypeCode VARCHAR(50) = NULL,
	@DispositionTypeID INT = NULL,
	@DispositionTypeCode VARCHAR(50) = NULL,
	@InitiatedByID BIGINT = NULL,
	@InitiatedByString VARCHAR(256) = NULL,
	-- Comment Note
	@CommentNoteID BIGINT = NULL OUTPUT,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(50) = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	-- Entry properties
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
	
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------------------------------------------------------------
	-- Instance variables
	----------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@Trancount INT = @@TRANCOUNT,
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL

	DECLARE @Args VARCHAR(MAX) =
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@CompletedByID=' + dbo.fnToStringOrEmpty(@CompletedByID) + ';' +
		'@CompletedByString=' + dbo.fnToStringOrEmpty(@CompletedByString) + ';' +
		'@DateCompleted=' + dbo.fnToStringOrEmpty(@DateCompleted) + ';' +
		'@TaskStatusID=' + dbo.fnToStringOrEmpty(@TaskStatusID) + ';' +
		'@TaskStatusCode=' + dbo.fnToStringOrEmpty(@TaskStatusCode) + ';' +
		'@NoteInteractionID=' + dbo.fnToStringOrEmpty(@NoteInteractionID) + ';' +
		'@InteractionID=' + dbo.fnToStringOrEmpty(@InteractionID) + ';' +
		'@InteractedWithID=' + dbo.fnToStringOrEmpty(@InteractedWithID) + ';' +
		'@InteractedWithString=' + dbo.fnToStringOrEmpty(@InteractedWithString) + ';' +
		'@ContactTypeID=' + dbo.fnToStringOrEmpty(@ContactTypeID) + ';' +
		'@ContactTypeCode=' + dbo.fnToStringOrEmpty(@ContactTypeCode) + ';' +
		'@InteractionTypeID=' + dbo.fnToStringOrEmpty(@InteractionTypeID) + ';' +
		'@InteractionTypeCode=' + dbo.fnToStringOrEmpty(@InteractionTypeCode) + ';' +
		'@DispositionTypeID=' + dbo.fnToStringOrEmpty(@DispositionTypeID) + ';' +
		'@DispositionTypeCode=' + dbo.fnToStringOrEmpty(@DispositionTypeCode) + ';' +
		'@InitiatedByID=' + dbo.fnToStringOrEmpty(@InitiatedByID) + ';' +
		'@InitiatedByString=' + dbo.fnToStringOrEmpty(@InitiatedByString) + ';' +
		'@CommentNoteID=' + dbo.fnToStringOrEmpty(@CommentNoteID) + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode) + ';' +
		'@NoteTypeID=' + dbo.fnToStringOrEmpty(@NoteTypeID) + ';' +
		'@NoteTypeCode=' + dbo.fnToStringOrEmpty(@NoteTypeCode) + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID) + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID) + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID) + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;



	----------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	----------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request	
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = NULL,
		@Arguments = @Args
	*/
		
	----------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------------------------------------------------------------
	SET @DateCompleted = ISNULL(@DateCompleted, GETDATE());
	SET @ModifiedBy = ISNULL(@ModifiedBy, @CreatedBy);
	SET @CreatedBy = ISNULL(@CreatedBy, @ModifiedBy);
	SET @CompletedByString = ISNULL(@CompletedByString, @ModifiedBy);
	SET @Debug = ISNULL(@Debug, 0);
	
	
	----------------------------------------------------------------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------------------------------------------------------------
	-- No declarations required.

	----------------------------------------------------------------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------------------------------------------------------------
	SET @TaskStatusID = ISNULL(dbo.fnGetTaskStatusID(@TaskStatusID), dbo.fnGetTaskStatusID('CMPL')); -- default to complete if not specified.


	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set method.' AS ActionMethod,
			@TaskStatusID AS TaskStatusID, @Args AS Arguments
	END


	----------------------------------------------------------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	----------------------------------------------------------------------------------------------------------------------------
	-- @NoteID 
	IF @NoteID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to complete the Task object(s). Object reference (@NoteID) is not set to an instance of an object. ' +
			'The @NoteID parameter cannot be null or empty.';
			
		RAISERROR (@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
			);	
			  
			RETURN;
	END	
			
	
	BEGIN TRY	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	----------------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	----------------------------------------------------------------------------------------------------------------------------	

		----------------------------------------------------------------------------------------------------------------------------
		-- Mark (close) the Task object(s) as being completed.
		-- <Summary>
		-- Closes the task(s).
		-- </Summary>
		----------------------------------------------------------------------------------------------------------------------------
		UPDATE tsk
		SET
			tsk.CompletedByID = CASE WHEN @CompletedByID IS NULL THEN tsk.CompletedByID ELSE @CompletedByID END,
			tsk.CompletedByString = CASE WHEN @CompletedByString IS NULL THEN tsk.CompletedByString ELSE @CompletedByString END,
			tsk.DateCompleted = @DateCompleted,
			tsk.TaskStatusID = @TaskStatusID,
			tsk.DateModified = GETDATE(),
			tsk.ModifiedBy = CASE WHEN @ModifiedBy IS NULL THEN tsk.ModifiedBy ELSE @ModifiedBy END
		FROM dbo.Task tsk
		WHERE tsk.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteID, ',') WHERE ISNUMERIC(Value) = 1)
		
		----------------------------------------------------------------------------------------------------------------------------
		-- If no records were updated, then go to the end of the function (EOF).
		-- There is no need to try and process an interaction.  There is no record
		-- to bind with the interaction.
		----------------------------------------------------------------------------------------------------------------------------
		IF @@ROWCOUNT = 0
		BEGIN
			GOTO EOF;
		END
				
		----------------------------------------------------------------------------------------------------------------------------
		-- Create Interaction object.
		-- <Summary>
		-- If an Interaction object was not provided, then create a new
		-- Interaction object (if applicable).
		-- </Summary>
		----------------------------------------------------------------------------------------------------------------------------
		IF @InteractionID IS NULL
		BEGIN
		
			IF @InteractionTypeID IS NOT NULL
			BEGIN
			
				EXEC dbo.spInteraction_Create
					@ApplicationID = @ApplicationID,
					@ApplicationCode = @ApplicationCode,
					@BusinessID = @BusinessID,
					@BusinessKey = @BusinessKey,
					@BusinessKeyType = @BusinessKeyType,
					@InteractionTypeID = @InteractionTypeID,
					@InteractionTypeCode = @InteractionTypeCode,
					@DispositionTypeID = @DispositionTypeID,
					@DispositionTypeCode = @DispositionTypeCode,
					@InitiatedByID = @InitiatedByID,
					@InitiatedByString = @InitiatedByString,
					@InteractedWithID = @InteractedWithID,
					@InteractedWithString = @InteractedWithString,
					@ContactTypeID = @ContactTypeID,
					@ContactTypeCode = @ContactTypeCode,
					@CreatedBy = @CreatedBy,
					@InteractionID = @InteractionID OUTPUT
					
			END
		
		END	
		
		----------------------------------------------------------------------------------------------------------------------------
		-- Create NoteInteraction object.
		-- <Summary>
		-- If an Interaction object was provided, then create a
		-- NoteInteration object.
		-- </Summary>
		----------------------------------------------------------------------------------------------------------------------------
		IF @InteractionID IS NOT NULL
		BEGIN
		
			EXEC dbo.spNoteInteraction_Create
				@NoteID = @NoteID,
				@ApplicationID = @ApplicationID,
				@ApplicationCode = @ApplicationCode,
				@BusinessID = @BusinessID,
				@BusinessKey = @BusinessKey,
				@BusinessKeyType = @BusinessKeyType,
				@InteractionID = @InteractionID,
				@CommentNoteID = @CommentNoteID,
				@ScopeID = @ScopeID,
				@ScopeCode = @ScopeCode,
				@BusinessEntityID = @BusinessEntityID,
				@NoteTypeID = @NoteTypeID,
				@NoteTypeCode = @NoteTypeCode,
				@NotebookID = @NotebookID,
				@NotebookCode = @NotebookCode,
				@Title = @Title,
				@Memo = @Memo,
				@PriorityID = @PriorityID,
				@PriorityCode = @PriorityCode,
				@Tags = @Tags, -- A comma separated list of tags that identify a note.
				@OriginID = @OriginID,
				@OriginCode = @OriginCode,
				@OriginDataKey = @OriginDataKey,
				@AdditionalData = @AdditionalData,
				@CorrelationKey = @CorrelationKey,
				@CreatedBy = @CreatedBy,
				@NoteInteractionID = @NoteInteractionID OUTPUT
					
		END				
	
	----------------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	----------------------------------------------------------------------------------------------------------------------------
	EOF:	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args
		
		DECLARE
			@ErrorSeverity INT = NULL,
			@ErrorState INT = NULL,
			@ErrorNumber INT = NULL,
			@ErrorLine INT = NULL,
			@ErrorProcedure  NVARCHAR(200) = NULL

		----------------------------------------------------------------------------------------------------------------------------
		-- Set error variables
		----------------------------------------------------------------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>');

		
		SELECT @ErrorMessage = 
			N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
				'Message: '+ ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);
		
	END CATCH		
	
END
