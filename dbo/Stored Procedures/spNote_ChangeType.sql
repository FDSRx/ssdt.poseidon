﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/2/2014
-- Description:	Changes the type of the note (e.g. Sticky to general, general to task, etc.).
-- SAMPLE CALL:
/*

DECLARE
	@NoteID BIGINT,
	@ApplicationID INT = NULL,
	@BusinessID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@NoteTypeID INT,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL

EXEC [dbo].[spNote_ChangeType]
	@NoteID = @NoteID,
	@ApplicationID = @ApplicationID,
	@BusinessID = @BusinessID,
	@BusinessEntityID = @BusinessEntityID,
	@NoteTypeID = @NoteTypeID,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.NoteType
-- =============================================
CREATE PROCEDURE [dbo].[spNote_ChangeType]
	@NoteID BIGINT,
	@ApplicationID INT = NULL,
	@BusinessID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@NoteTypeID INT,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@NoteTypeID=' + dbo.fnToStringOrEmpty(@NoteTypeID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------
	-- Update object.
	------------------------------------------------------------------------------------------------------------
	UPDATE n
	SET
		n.NoteTypeID = @NoteTypeID,
		n.DateModified = GETDATE(),
		n.ModifiedBy = @ModifiedBy
	FROM dbo.Note n
	WHERE NoteID = @NoteID
		AND (@BusinessEntityID IS NULL OR (@BusinessEntityID IS NOT NULL AND BusinessEntityID = @BusinessEntityID))


END
