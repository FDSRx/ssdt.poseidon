﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/19/2015
-- Description:	Returns the preference value information for the provided criteria.
-- SAMPLE CALL:
/*
DECLARE 
	@Exists BIT = NULL,
	@ValueString VARCHAR(MAX) = NULL,
	@ValueBoolean BIT = NULL,
	@ValueNumeric DECIMAL(9,2) = NULL

EXEC dbo.spBusinessEntityPreference_Get
	@BusinessEntityID = 135629,
	@PreferenceID = 1,
	@ValueString = @ValueString OUTPUT,
	@ValueBoolean = @ValueBoolean OUTPUT,
	@Exists = @Exists OUTPUT

SELECT @ValueString AS ValueString, @ValueBoolean AS ValueBoolean, @Exists AS IsFound
*/

-- SELECT * FROM dbo.BusinessEntityPreference
-- =============================================
CREATE PROCEDURE dbo.spBusinessEntityPreference_Get
	@BusinessEntityPreferenceID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@PreferenceID INT = NULL OUTPUT,
	@ValueString VARCHAR(MAX) = NULL OUTPUT,
	@ValueBoolean BIT = NULL OUTPUT,
	@ValueNumeric DECIMAL(9,2) = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------
	SET @ValueString = NULL;
	SET @ValueBoolean = NULL;
	SET @ValueNumeric = NULL;
	SET @Exists = 0;

	-----------------------------------------------------------------------------------------------
	-- Determine if the BusinessEntityPreference object exists.
	-----------------------------------------------------------------------------------------------
	-- The record identity has primary search priority.
	IF @BusinessEntityPreferenceID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @PreferenceID = NULL;
	END
	
	-----------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------	
	SELECT TOP 1
		@BusinessEntityPreferenceID = BusinessEntityPreferenceID,
		@BusinessEntityID = BusinessEntityID,
		@PreferenceID = PreferenceID,
		@ValueString = ValueString,
		@ValueBoolean = ValueBoolean,
		@ValueNumeric = ValueNumeric
	FROM dbo.BusinessEntityPreference
	WHERE ( BusinessEntityPreferenceID = @BusinessEntityPreferenceID )
		OR ( BusinessEntityID = @BusinessEntityID
			AND PreferenceID = @PreferenceID )
	
	
	IF @@ROWCOUNT = 1
	BEGIN
		SET @Exists = 1;
	END
		
	
	
	
	
END
