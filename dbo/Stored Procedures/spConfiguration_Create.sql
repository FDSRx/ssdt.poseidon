﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/09/2014
-- Description:	Creates a new Configuration object.
-- SAMPLE CALL:
/*

EXEC dbo.spConfiguration_Create
	@ApplicationID = 2, -- "ESMT" (Engage/Store management tool)
	@BusinessEntityID = NULL,
	@KeyName = 'MedSyncDaysInPeriod',
	@KeyValue = 7,
	@CreatedBy = 'dhughes'

*/
-- SELECT * FROM dbo.Configuration
-- =============================================
CREATE PROCEDURE [dbo].[spConfiguration_Create]
	@ApplicationID INT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@KeyName VARCHAR(256),
	@KeyValue VARCHAR(MAX) = NULL,
	@DataTypeID INT= NULL,
	@DataSize INT = NULL,
	@Description VARCHAR(1000) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ConfigurationID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------
	SET @ConfigurationID = NULL;

	-------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)		

	-------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------
	SET @DataTypeID = ISNULL(@DataTypeID, dbo.fnGetDataTypeID('varchar'));
	
	-------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------------
	IF dbo.fnIsNullOrWhiteSpace(@KeyName) = 1
	BEGIN
	
		SET @ErrorMessage = 'Unable to create Configuration object. Object reference (@KeyName) is not set to an instance of an object. ' +
			'The parameter, @KeyName, cannot be null or empty.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
		 
	END

	-------------------------------------------------------------------------------------
	-- Create the configuration key.
	-------------------------------------------------------------------------------------		
	EXEC config.spConfigurationKey_Merge
		@Name = @KeyName,
		@Description = @Description,
		@CreatedBy = @CreatedBy

	-------------------------------------------------------------------------------------
	-- Create new Configuration object.
	-------------------------------------------------------------------------------------	
	INSERT INTO dbo.Configuration (
		ApplicationID,
		BusinessEntityID,
		KeyName,
		KeyValue,
		DataTypeID,
		DataSize,
		Description,
		CreatedBy
	)
	SELECT
		@ApplicationID,
		@BusinessEntityID,
		@KeyName,
		@KeyValue,
		@DataTypeID,
		@DataSize,
		@Description,
		@CreatedBy
	
	-- Retrieve record identity.
	SET @ConfigurationID = SCOPE_IDENTITY();
	
	
END
