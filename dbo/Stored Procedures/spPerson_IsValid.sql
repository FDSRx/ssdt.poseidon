﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/30/2013
-- Description:	Determines if a person object is valid for storage
-- SAMPLE CALL:
/*
DECLARE @IsValid BIT, @ValidationSummary XML
EXEC spPerson_IsValid
	@FirstName = 'daniel',
	@LastName = 'hughes',
	@BirthDate = '1/1/1900',
	@PersonID = -10,
	@IsValid = @IsValid OUTPUT,
	@ValidationSummary = @ValidationSummary OUTPUT
SELECT @IsValid AS IsValid, @ValidationSummary AS ValidationSummary
*/
-- =============================================
CREATE PROCEDURE [dbo].[spPerson_IsValid]
	--Person
	@PersonID INT = NULL,
	@Title VARCHAR(10) = NULL,
	@FirstName VARCHAR(75) = NULL,
	@LastName VARCHAR(75) = NULL,
	@MiddleName VARCHAR(75) = NULL,
	@Suffix VARCHAR(10) = NULL,
	@BirthDate DATE = NULL,
	@GenderID INT = NULL,
	@LanguageID INT = NULL,
	--Person Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(15) = NULL,
	--Person Phone
	--	Home
	@PhoneNumber_Home VARCHAR(25) = NULL,
	@PhoneNumber_Home_IsCallAllowed BIT = 0,
	@PhoneNumber_Home_IsTextAllowed BIT = 0,
	--	Mobile
	@PhoneNumber_Mobile VARCHAR(25) = NULL,
	@PhoneNumber_Mobile_IsCallAllowed BIT = 0,
	@PhoneNumber_Mobile_IsTextAllowed BIT = 0,
	--Person Email 
	--	Primary
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Primary_IsEmailAllowed BIT = 0,
	--	Altenrate
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	@EmailAddress_Alternate_IsEmailAllowed BIT = 0,
	-- Output
	@IsValid BIT = NULL OUTPUT,
	@ValidationSummary XML = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------
	-- Sanitize input
	-----------------------------------------------------------------------
	SET @IsValid = 1;
	SET @ValidationSummary = NULL;
	
	-----------------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidationSummaryXmlRaw VARCHAR(MAX) = NULL,
		@ExceptionSummaryXmlRaw VARCHAR(MAX) = '',
		@MissingFields VARCHAR(MAX),
		@InvalidPropertyCount INT,
		@RequiredPropertyCount INT,
		@IsAllRequiredPropertiesMissing BIT = 0
	

	-----------------------------------------------------------------------
	-- Inspect person.
	-----------------------------------------------------------------------	
	IF @PersonID IS NOT NULL
	BEGIN
	
		-----------------------------------------------------------------------
		-- If a PersonID was supplied then we only want to inspect that validity
		-- of the ID.
		-- <Remarks>
		-- PersonID always wins the heirarchy battle.
		-- </Remarks>
		-----------------------------------------------------------------------
		-- If the person exists then reset validator to true.
		IF EXISTS (
			SELECT TOP 1 *
			FROM dbo.Person
			WHERE BusinessEntityID = @PersonID
		)
		BEGIN
			SET @IsValid = 1;		
		END
		ELSE
		BEGIN
			-- Set validator to false.
			SET @IsValid = 0;
			
			SET @ErrorMessage = 'Person identifier does not match any known person in the person repository.'
			
			-----------------------------------------------------------------------
			-- Build exception object
			-----------------------------------------------------------------------
			SET @ExceptionSummaryXmlRaw =
				'<Exception>' +
					dbo.fnXmlWriteElementString('Message', @ErrorMessage) +
					dbo.fnXmlWriteElementString('InvalidProperties', 'PersonID') +
					dbo.fnXmlWriteElementString('IsAllRequiredPropertiesMissing', 0) +
					'<Arguments>' +
						dbo.fnXmlWriteElementString('PersonID', @PersonID) +
					'</Arguments>' +
				'</Exception>'
		END
		
		
	END
	ELSE
	BEGIN
		-----------------------------------------------------------------------
		-- Validate demographic data to determine if a person can be created
		-- <Summary>
		-- Determines if the minimum requirements to create a person have been
		-- met
		-- </Summary>
		-----------------------------------------------------------------------
		SET @RequiredPropertyCount = 3;
		
		-- Base criteria required to create a person
		IF dbo.fnIsNullOrWhiteSpace(@FirstName) = 1 OR dbo.fnIsNullOrWhiteSpace(@LastName) = 1 
			OR ISNULL(@BirthDate, '1/1/0001') = '1/1/0001' --OR ISNULL(@PostalCode, '') = ''
		BEGIN
			-- insufficient information to create a person. A first, last, and dob is the minimum amount of information necessary
			-- to create a person entity
			SET @ErrorMessage = 'Unable to create a PERSON entity.  A minimum of first and last name, and birth date ' +
				'are required to create a person entity. '
				+ 'Data = '
				+ 'First Name: ' + ISNULL(@FirstName, '<EMPTY>')
				+ '; Last Name: ' + ISNULL(@LastName, '<EMPTY>')
				+ '; Birth Date: ' + ISNULL(CONVERT(VARCHAR, @BirthDate, 101), '<EMPTY>')
				--+ '; Postal Code: ' + ISNULL(@PostalCode, '<EMPTY>');
			
			-- Set valid flag to false.	
			SET @IsValid = 0;
			
			-----------------------------------------------------------------------
			-- Determine missing elements
			-- <Summary>
			-- Builds a list of the missing fields that caused the exception to
			-- trigger.
			-- </Summary>
			-----------------------------------------------------------------------
			-- First name
			IF dbo.fnIsNullOrWhiteSpace(@FirstName) = 1
			BEGIN
				SET @MissingFields = ISNULL(@MissingFields, '') + 'FirstName,';
				SET @InvalidPropertyCount = ISNULL(@InvalidPropertyCount, 0) + 1;
			END
			
			-- Last name
			IF dbo.fnIsNullOrWhiteSpace(@LastName) = 1
			BEGIN
				SET @MissingFields = ISNULL(@MissingFields, '') + 'LastName,';
				SET @InvalidPropertyCount = ISNULL(@InvalidPropertyCount, 0) + 1;
			END
			
			-- Birth date
			IF ISNULL(@BirthDate, '1/1/0001') = '1/1/0001'
			BEGIN
				SET @MissingFields = ISNULL(@MissingFields, '') + 'BirthDate,';
				SET @InvalidPropertyCount = ISNULL(@InvalidPropertyCount, 0) + 1;
			END
			
			IF @InvalidPropertyCount = @RequiredPropertyCount
			BEGIN
				SET @IsAllRequiredPropertiesMissing = 1;
			END
			
			-----------------------------------------------------------------------
			-- Build exception object
			-----------------------------------------------------------------------
			SET @ExceptionSummaryXmlRaw =
				'<Exception>' +
					dbo.fnXmlWriteElementString('Message', @ErrorMessage) +
					dbo.fnXmlWriteElementString('InvalidProperties', SUBSTRING(@MissingFields, 1, LEN(@MissingFields) -1)) +
					dbo.fnXmlWriteElementString('IsAllRequiredPropertiesMissing', @IsAllRequiredPropertiesMissing) +
					'<Arguments>' +
						dbo.fnXmlWriteElementString('FirstName', @FirstName) +
						dbo.fnXmlWriteElementString('LastName', @LastName) +
						dbo.fnXmlWriteElementString('BirthDate', @BirthDate) +
					'</Arguments>' +
				'</Exception>'
						
							  
		END
	END	
	

	-----------------------------------------------------------------------
	-- Create xml output
	-----------------------------------------------------------------------
	SET @ValidationSummaryXmlRaw =
		'<ValidationSummary>' +
			dbo.fnXmlWriteElementString('IsValid', @IsValid) +
			@ExceptionSummaryXmlRaw +
		'</ValidationSummary>';
	
	SET @ValidationSummary = CONVERT(XML, @ValidationSummaryXmlRaw);
		
			
	
	



END
