﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/14/2014
-- Description:	Indicates whether the specified business and service already exist.
-- SAMPLE CALL:
/*

DECLARE
	@BusinessEntityServiceUmbrellaID BIGINT = NULL, --1
	@BusinessEntityUmbrellaID BIGINT = NULL,
	@Exists BIT
	
EXEC spBusinessEntityServiceUmbrella_Exists
	@BusinessEntityServiceUmbrellaID = @BusinessEntityServiceUmbrellaID OUTPUT,
	@BusinessEntityUmbrellaID = @BusinessEntityUmbrellaID OUTPUT,
	@BusinessEntityID = 2,
	@ServiceID = 4,
	@Exists = @Exists OUTPUT

SELECT @BusinessEntityServiceUmbrellaID AS BusinessEntityServiceUmbrellaID, 
	@BusinessEntityUmbrellaID AS BusinessEntityUmbrellaID, @Exists AS IsFound
	
*/

-- SELECT * FROM dbo.BusinessEntityServiceUmbrella
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityServiceUmbrella_Exists]
	@BusinessEntityServiceUmbrellaID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@ServiceID INT = NULL,
	@BusinessEntityUmbrellaID BIGINT = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------
	SET @BusinessEntityUmbrellaID = NULL;
	SET @Exists = ISNULL(@Exists, 0);
	
	--------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidateUniqueRowKey BIT = 1
	
	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------	
	IF @BusinessEntityServiceUmbrellaID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @ServiceID = NULL;
		
		SET @ValidateUniqueRowKey = 0;
	END
	
	--------------------------------------------------------------------
	-- Argument Null Exceptions
	--------------------------------------------------------------------
	
	IF @BusinessEntityServiceUmbrellaID IS NULL AND
		@BusinessEntityID IS NULL AND
		@ServiceID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object references (@BusinessEntityServiceUmbrellaID, @BusinessEntityID, @ServiceID) are not set to an instance ' +
			'of an object. The objects (@BusinessEntityServiceUmbrellaID, @BusinessEntityID, @ServiceID) cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;	
	END
	
	--------------------------------------------------------------------
	-- Business Entity Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null business entity ID
	-- was passed
	--------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0 AND @ValidateUniqueRowKey = 1
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@BusinessEntityID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );
		
		RETURN;	
	END	
	
	--------------------------------------------------------------------
	-- Service Validation
	-- Before we can even create a new service mapping, 
	-- make sure a non-null service ID
	-- was passed
	--------------------------------------------------------------------
	IF ISNULL(@ServiceID, 0) = 0 AND @ValidateUniqueRowKey = 1
	BEGIN
		
		SET @ErrorMessage = 'Object reference (@ServiceID) is not set to an instance of an object. The object cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	-- Debug
	--SELECT @BusinessEntityServiceUmbrellaID AS BusinessEntityServiceUmbrellaID, @BusinessEntityID AS BusinessEntityID, @ServiceID AS ServiceID
	
	--------------------------------------------------------------------
	-- Determine if business entity service umbrella record already
	-- exists in the data.
	-- <Summary>
	-- Clone the @BusinessEntityServiceUmbrellaID and destroy the original.
	-- We want to ensure the caller retrieves the correct results when
	-- fetching the data. If the Id is found, the it will be recreated during
	-- assignment.
	-- </Summary>
	--------------------------------------------------------------------
	DECLARE
		@BusinessEntityServiceUmbrellaID_Clone BIGINT = @BusinessEntityServiceUmbrellaID
	
	SET @BusinessEntityServiceUmbrellaID = NULL;
	
	SELECT
		@BusinessEntityServiceUmbrellaID = BusinessEntityServiceUmbrellaID,
		@BusinessEntityUmbrellaID = BusinessEntityUmbrellaID
	FROM dbo.BusinessEntityServiceUmbrella
	WHERE (BusinessEntityServiceUmbrellaID = @BusinessEntityServiceUmbrellaID_Clone)
		OR ( BusinessEntityID = @BusinessEntityID AND ServiceID = @ServiceID )
	
	-- Set exists flag based on our finding.
	SET @Exists = CASE WHEN @BusinessEntityServiceUmbrellaID IS NOT NULL THEN 1 ELSE 0 END;
	
	
	
	
	
	
	
	
	
	
	
END
