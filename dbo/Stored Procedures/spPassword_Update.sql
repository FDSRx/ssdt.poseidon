﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Update person password
-- SAMPLE CALL: spPassword_Update 1, 1, 'Password1'
-- =============================================
CREATE PROCEDURE [dbo].[spPassword_Update]
	@PersonID INT,
	@ServiceID INT,
	@Password VARCHAR(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	---------------------------------------------------
	-- Local variables
	---------------------------------------------------
	DECLARE
		@Hash VARBINARY(128),
		@Salt VARBINARY(10),
		@ErrorMessage VARCHAR(4000)

	---------------------------------------------------------
	-- Salt and Hash clear text
	---------------------------------------------------------
	SET @Salt = dbo.fnPasswordSalt(RAND());
	SET @Hash = dbo.fnPasswordHash(@Password + CONVERT(VARCHAR(MAX), @Salt));
	
	
	---------------------------------------------------------
	-- Update user password
	---------------------------------------------------------
	UPDATE pwd
	SET
		pwd.PasswordHash = @Hash,
		pwd.PasswordSalt = @Salt
	FROM dbo.Password pwd
	WHERE PersonID = @PersonID
		AND ServiceID = @ServiceID
		
	
	---------------------------------------------------------
	-- Determine if a row was found to update
	-- fail if we could not find person
	---------------------------------------------------------
	IF @@ROWCOUNT = 0
	BEGIN
	
		SET @ErrorMessage = 'Unable to update password.  Could not find person.';
		
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );	
	END

	
END
