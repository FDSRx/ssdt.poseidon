﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/21/2014
-- Description: Updates an image within the image repository.
-- =============================================
CREATE PROCEDURE [dbo].[spImage_Update]
	@ImageID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@PersonID BIGINT = NULL,
	@KeyName VARCHAR(256) = NULL,
	@Name VARCHAR(256) = NULL,
	@Caption VARCHAR(4000) = NULL,
	@Description VARCHAR(MAX) = NULL,
	@FileName VARCHAR(256) = NULL,
	@FilePath VARCHAR(1000) = NULL,
	@FileDataString VARCHAR(MAX) = NULL,
	@FileDataBinary VARBINARY(MAX) = NULL,
	@LanguageID INT = NULL,
	@IsDelete BIT = 0,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------
	SET @IsDelete = ISNULL(@IsDelete, 0);
		
	------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
		
	------------------------------------------------------------
	-- Set variables
	------------------------------------------------------------
	SET @LanguageID = ISNULL(@LanguageID, dbo.fnGetLanguageID('en')); -- default to english.
	
	------------------------------------------------------------
	-- Argument null exception
	------------------------------------------------------------
	IF ISNULL(@KeyName, '') = '' AND @ImageID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object references (@KeyName and @ImageID) are not set to an instance of an object. ' + 
			'One of the objects cannot be null or empty.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	------------------------------------------------------------
	-- Retrieve ImageID
	------------------------------------------------------------
	IF @ImageID IS NULL
	BEGIN
	
		SELECT TOP 1
			@ImageID = ImageID
		FROM dbo.Images
		WHERE ( @ApplicationID IS NULL OR ApplicationID = @ApplicationID )
			AND ( @BusinessID IS NULL OR BusinessID = @BusinessID )
			AND ( @PersonID IS NULL OR PersonID = @PersonID )
			AND KeyName = @KeyName
			AND ( @LanguageID IS NULL OR LanguageID = @LanguageID )
	
	END
	
	------------------------------------------------------------
	-- Update image record.
	-- <Summary>
	-- If the image is not considered deleted then update the
	-- necessary data elements. If the image is to be deleted,
	-- then void the data elements that are considered the image
	-- and leave the image key behind to reserve space for a replacing image.
	-- </Summary>
	------------------------------------------------------------
	IF ISNULL(@IsDelete, 0 ) = 0
	BEGIN
	
		UPDATE img
		SET	
			--img.ApplicationID,
			--img.BusinessEntityID,
			--img.UserEntityID,
			--img.KeyName,
			img.Name = CASE WHEN @Name IS NOT NULL THEN @Name ELSE Name END,
			img.Caption = CASE WHEN @Caption IS NOT NULL THEN @Caption ELSE Caption END,
			img.Description = CASE WHEN @Description IS NOT NULL THEN @Description ELSE Description END,
			img.FileName = CASE WHEN @FileName IS NOT NULL THEN @FileName ELSE FileName END,
			img.FilePath = CASE WHEN @FilePath IS NOT NULL THEN @FilePath ELSE FilePath END,
			img.FileDataString = CASE WHEN @FileDataString IS NOT NULL THEN @FileDataString ELSE FileDataString END,
			img.FileDataBinary = CASE WHEN @FileDataBinary IS NOT NULL THEN @FileDataBinary ELSE FileDataBinary END,
			img.DateModified = GETDATE(),
			img.ModifiedBy = @ModifiedBy
		FROM dbo.Images img
		WHERE ImageID = @ImageID
	
	END
	ELSE
	BEGIN
	
		UPDATE img
		SET	
			--img.ApplicationID,
			--img.BusinessEntityID,
			--img.UserEntityID,
			--img.KeyName,
			img.Name = NULL,
			img.Caption = NULL,
			img.Description = NULL,
			img.FileName = NULL,
			img.FilePath = NULL,
			img.FileDataString = NULL,
			img.FileDataBinary = NULL,
			img.DateModified = GETDATE(),
			img.ModifiedBy = @ModifiedBy
		FROM dbo.Images img
		WHERE ImageID = @ImageID
	
	END
	
	
	
END

