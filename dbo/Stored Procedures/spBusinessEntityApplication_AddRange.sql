﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/3/2015
-- Description:	Creates a new set of BusinessEntityApplication object(s).
-- SAMPLE CALL:
/*

DECLARE 
	@BusinessEntityApplicationIDList VARCHAR(MAX) = NULL
	
EXEC dbo.spBusinessEntityApplication_AddRange
	@BusinessEntityID = 10684,
	@ApplicationID = 10,
	@BusinessEntityApplicationIDList = @BusinessEntityApplicationIDList OUTPUT

SELECT @BusinessEntityApplicationIDList AS BusinessEntityApplicationIDList

*/

-- SElECT * FROM dbo.Application
-- SELECT * FROM dbo.BusinessEntity
-- SELECT * FROM dbo.BusinessEntityApplication
-- =============================================
CREATE PROCEDURE dbo.[spBusinessEntityApplication_AddRange]
	@BusinessEntityID BIGINT,
	@ApplicationID VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityApplicationIDList VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------
	-- Instance variables
	---------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ErrorMessage VARCHAR(4000),
		@ErrorSeverity INT = NULL,
		@ErrorState INT = NULL,
		@ErrorNumber INT = NULL,
		@ErrorLine INT = NULL,
		@ErrorProcedure  NVARCHAR(200) = NULL
	
	-------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------
	SET @BusinessEntityApplicationIDList = NULL;
	
	-------------------------------------------------------------------------------
	-- Local variables
	-------------------------------------------------------------------------------
		

	-------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------
	-- @BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplication object. Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @ApplicationID
	IF dbo.fnIsNullOrWhiteSpace(@ApplicationID) = 1
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplication object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	


	BEGIN TRY
	--------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------	
	SET @Trancount = @@TRANCOUNT;
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	
		-------------------------------------------------------------------------------
		-- Temporary objects.
		-------------------------------------------------------------------------------
		DECLARE @tblOutput AS TABLE (
			Idx INT IDENTITY(1,1),
			BusinessEntityApplicationID BIGINT
		);
		
				
		-------------------------------------------------------------------------------
		-- Create new object(s).
		-------------------------------------------------------------------------------
		INSERT INTO dbo.BusinessEntityApplication (			
			BusinessEntityID,
			ApplicationID,
			CreatedBy
		)
		OUTPUT inserted.BusinessEntityApplicationID INTO @tblOutput(BusinessEntityApplicationID)
		SELECT
			@BusinessEntityID AS BusinessEntityID,
			Value AS ApplicationID,
			@CreatedBy
		FROM dbo.fnSplit(@ApplicationID, ',')
		WHERE ISNUMERIC(Value) = 1
		
		-------------------------------------------------------------------------------
		-- Retrieve record identity value(s).
		-------------------------------------------------------------------------------
		SET @BusinessEntityApplicationIDList = (
			SELECT 
				CONVERT(VARCHAR(25), BusinessEntityApplicationID) + ','
			FROM @tblOutput
			FOR XML PATH('')
		);
		
		IF RIGHT(@BusinessEntityApplicationIDList, 1) = ','
		BEGIN
			SET @BusinessEntityApplicationIDList = LEFT(@BusinessEntityApplicationIDList, LEN(@BusinessEntityApplicationIDList) - 1);
		END

	--------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END		
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE [dbo].[spLogError];

		--------------------------------------------------------------------
		-- Set error variables
		--------------------------------------------------------------------
		SELECT 
			@ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()),
			@ErrorNumber = ISNULL(@ErrorNumber, ERROR_NUMBER()),
			@ErrorSeverity = COALESCE(@ErrorSeverity, ERROR_SEVERITY(), 16),
			@ErrorState = COALESCE(@ErrorState, ERROR_STATE(), 1),
			@ErrorLine = ISNULL(@ErrorLine, ERROR_LINE()),
			@ErrorProcedure = COALESCE(@ErrorProcedure, ERROR_PROCEDURE(), '<Unspecified>'),
			@ErrorMessage = ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState,               
			@ErrorNumber,    -- parameter: original error number.
			@ErrorSeverity,  -- parameter: original error severity.
			@ErrorState,     -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine       -- parameter: original error line number.
		);	
	
	END CATCH
	
		
END
