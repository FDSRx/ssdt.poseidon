﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/14/2013
-- Description:	Determines if the person's email address exists.
-- SAMPLE CALL:
/*
DECLARE 
	@Exists BIT = 0, 
	@BusinessEntityEmailAddressID BIGINT = NULL

---- Surrogate validation
--EXEC dbo.spBusinessEntity_Email_Exists
--	@BusinessEntityEmailAddressID = @BusinessEntityEmailAddressID OUTPUT,
--	@BusinessEntityID = -1,
--	@EmailAddressTypeID = -1,
--	@Exists = @Exists OUTPUT

-- Unique key validation
EXEC dbo.spBusinessEntity_Email_Exists
	@BusinessEntityEmailAddressID = @BusinessEntityEmailAddressID OUTPUT,
	@BusinessEntityID = 67707,
	@EmailAddressTypeID = 1,
	@Exists = @Exists OUTPUT
	
SELECT @Exists AS IsFound, @BusinessEntityEmailAddressID AS BusinessEntityEmailAddressID
		
*/
-- SELECT * FROM dbo.BusinessEntityEmailAddress
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Email_Exists] 
	@BusinessEntityEmailAddressID INT = NULL OUTPUT,
	@BusinessEntityID INT = NULL,
	@EmailAddressTypeID INT = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidateSurrogateKey BIT = 0
		

	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------	
	IF @BusinessEntityEmailAddressID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @EmailAddressTypeID = NULL;
		
		SET @ValidateSurrogateKey = 1;
	END
	
	----------------------------------------------------------------------
	-- Argument Null Exceptions
	----------------------------------------------------------------------
	----------------------------------------------------------------------
	-- Surrogate key validation.
	-- <Summary>
	-- If a surrogate key was provided, then that will be the only lookup
	-- performed.
	-- <Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityEmailAddressID, 0) = 0 AND @ValidateSurrogateKey = 1
	BEGIN			
		
		SET @ErrorMessage = 'Object reference not set to an instance of an object. The object, @BusinessEntityEmailAddressID, cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	----------------------------------------------------------------------
	-- Unique row key validation.
	-- <Summary>
	-- If a surrogate key was not provided then we will try and perform the
	-- lookup based on the records unique key.
	-- </Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0 AND ISNULL(@EmailAddressTypeID, 0) = 0 AND @ValidateSurrogateKey = 0
	BEGIN
		
		SET @ErrorMessage = 'Object references not set to an instance of an object. The objects, @BusinessEntityID and @EmailAddressTypeID cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END
	
	
	--------------------------------------------------------------------
	-- Determine if business entity service exists
	--------------------------------------------------------------------
	SET @BusinessEntityEmailAddressID = 
		CASE 
			WHEN @BusinessEntityEmailAddressID IS NOT NULL AND @ValidateSurrogateKey = 1 THEN (
				SELECT TOP 1 BusinessEntityEmailAddressID 
				FROM dbo.BusinessEntityEmailAddress
				WHERE BusinessEntityEmailAddressID = @BusinessEntityEmailAddressID
			)
			ELSE (
				SELECT TOP 1 BusinessEntityEmailAddressID
				FROM dbo.BusinessEntityEmailAddress
				WHERE BusinessEntityID = @BusinessEntityID
					AND EmailAddressTypeID = @EmailAddressTypeID
			)
		END;
	
	-- Set the exists flag to properly represent the finding.
	SET @Exists = CASE WHEN @BusinessEntityEmailAddressID IS NOT NULL THEN 1 ELSE 0 END;

   
END
