﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/25/2014
-- Description:	Sets a device address (i.e. phone number or email address) as a BusinessEntity's
--	preferred contact method.
-- SAMPLE CALL:
/*

EXEC dbo.spBusinessEntity_CommunicationMethod_PreferredDevice_Set
	@BusinessEntityID = 135590,
	@CommunicationMethodID = 2,
	@DeviceAddress = 'ssimmons@hcc-care.com',
	@IsPreferred = 1,
	@ModifiedBy = 'dhughes'
	


*/

-- SELECT * FROM dbo.BusinessEntityPhone WHERE BusinessEntityID = 135590
-- SELECT * FROM dbo.BusinessEntityEmailAddress WHERE BusinessEntityID = 135590
-- SELECT * FROM phrm.Person WHERE LastName LIKE '%Simmons%'
-- SELECT * FROM comm.CommunicationMethod
-- SELECT * FROM dbo.PhoneNumberType
-- SELECT * FROM dbo.EmailAddressType
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_CommunicationMethod_PreferredDevice_Set]
	@BusinessEntityID BIGINT,
	@CommunicationMethodID INT,
	@DeviceTypeID INT = NULL,
	@DeviceAddressID BIGINT = NULL,
	@DeviceAddress VARCHAR(256) = NULL,
	@IsPreferred BIT = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@CommunicationMethodID=' + dbo.fnToStringOrEmpty(@CommunicationMethodID) + ';' +
		'@DeviceAddressID=' + dbo.fnToStringOrEmpty(@DeviceAddressID) + ';' +
		'@DeviceAddress=' + dbo.fnToStringOrEmpty(@DeviceAddress) + ';' +
		'@IsPreferred=' + dbo.fnToStringOrEmpty(@IsPreferred) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@DeviceAddressID=' + dbo.fnToStringOrEmpty(@DeviceAddressID) + ';' 

	DECLARE @Procedure VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @Procedure,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	---------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------------------------------
	SET @IsPreferred = ISNULL(@IsPreferred, 0);
	
	---------------------------------------------------------------------------------------------------------------------
	-- Local variables
	---------------------------------------------------------------------------------------------------------------------
	DECLARE
		@CommunicationMethodCode VARCHAR(25) = comm.fnGetCommunicationMethodCode(@CommunicationMethodID),
		@ErrorMessage VARCHAR(4000),
		@IsSet BIT = 0


	---------------------------------------------------------------------------------------------------------------------
	-- Argument null exception
	---------------------------------------------------------------------------------------------------------------------
	-- @BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference (@BusinessEntityID) is not set to an instance of an object. ' + 
			'The object cannot be null or empty.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	-- @CommunicationMethodID
	IF @CommunicationMethodID IS NULL
	BEGIN
		SET @ErrorMessage = 'Object reference (@CommunicationMethodID) is not set to an instance of an object. ' + 
			'The object cannot be null or empty.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	-- @DeviceAddressID and @DeviceAddress
	IF @DeviceAddressID IS NULL AND dbo.fnIsNullOrWhiteSpace(@DeviceAddress) = 1
	BEGIN
		SET @ErrorMessage = 'Object references (@DeviceAddressID and @DeviceAddress) are not set to an instance of an object. ' + 
			'One of the objects cannot be null or empty.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	---------------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	---------------------------------------------------------------------------------------------------------------------
	DECLARE @tblOutput AS TABLE (
		Idx INT IDENTITY(1,1),
		CommunicationMethodID INT,
		DeviceAddressID BIGINT
	);

	---------------------------------------------------------------------------------------------------------------------
	-- Void "Unique Key Identifiers" if a standard identifier was provided.
	---------------------------------------------------------------------------------------------------------------------
	IF @DeviceAddressID IS NOT NULL
	BEGIN
		SET @DeviceAddress = NULL;
		SET @DeviceTypeID = NULL;
	END
	
	---------------------------------------------------------------------------------------------------------------------
	-- Set preferred method
	-- <Summary>
	-- Uses the CommunicationMethodCode to infer which type of destination address
	-- to update. 
	-- </Summary>
	---------------------------------------------------------------------------------------------------------------------
	-- If the communication method is email, then set the preferred email.
	IF @CommunicationMethodCode = 'EMAIL'
	BEGIN
	
		UPDATE TOP(1)com
		SET
			com.IsPreferred = @IsPreferred,
			com.DateModified = GETDATE(),
			com.ModifiedBy = @ModifiedBy
		OUTPUT @CommunicationMethodID, inserted.BusinessEntityEmailAddressID INTO @tblOutput(CommunicationMethodID, DeviceAddressID)
		--SELECT *
		FROM dbo.BusinessEntityEmailAddress com
		WHERE BusinessEntityID = @BusinessEntityID
			AND (
				BusinessEntityEmailAddressID = @DeviceAddressID
				OR (
					EmailAddress = @DeviceAddress
					AND ( @DeviceTypeID IS NULL OR com.EmailAddressTypeID = @DeviceTypeID )
				)
			)
		
		SET @IsSet = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END;
	END
	
	-- If the communication method is phone, then set the preferred phone.
	IF @CommunicationMethodCode IN ('PHONE', 'VOICE', 'SMS')
	BEGIN
		
		-- Strip all non-numeric characters from the device address.
		SET @DeviceAddress = dbo.fnRemoveNonNumericChars(@DeviceAddress);
		
		UPDATE TOP(1) com
		SET
			com.IsPreferred = @IsPreferred,
			com.DateModified = GETDATE(),
			com.ModifiedBy = @ModifiedBy
		OUTPUT @CommunicationMethodID, inserted.BusinessEntityPhoneNumberID INTO @tblOutput(CommunicationMethodID, DeviceAddressID)
		--SELECT *
		FROM dbo.BusinessEntityPhone com
		WHERE BusinessEntityID = @BusinessEntityID
			AND (BusinessEntityPhoneNumberID = @DeviceAddressID
				OR ( 
					PhoneNumber = @DeviceAddress
					AND ( @DeviceTypeID IS NULL OR com.PhoneNumberTypeID = @DeviceTypeID )
				)
			)
		
		SET @IsSet = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END;
	END
	
	---------------------------------------------------------------------------------------------------------------------
	-- Void all other preferred device is a preferred device was able to be set.
	-- <Summary>
	-- If a preferred device was able to be set, then void all other preferred
	-- devices.
	-- </Summary>
	---------------------------------------------------------------------------------------------------------------------
	IF @IsSet = 1 AND @IsPreferred = 1
	BEGIN
		
		-- Void all email addresses except for the preferred address.
		UPDATE com
		SET
			com.IsPreferred = 0,
			com.DateModified = GETDATE(),
			com.ModifiedBy = @ModifiedBy
		--SELECT *
		FROM dbo.BusinessEntityEmailAddress com
		WHERE BusinessEntityID = @BusinessEntityID
			AND BusinessEntityEmailAddressID NOT IN (SELECT DeviceAddressID FROM @tblOutput WHERE CommunicationMethodID = @CommunicationMethodID)
			AND (com.IsPreferred IS NULL OR com.IsPreferred = 1)
		
	
		-- Void all phone numbers except for the preferred number.
		UPDATE com
		SET
			com.IsPreferred = 0,
			com.DateModified = GETDATE(),
			com.ModifiedBy = @ModifiedBy
		--SELECT *
		FROM dbo.BusinessEntityPhone com
		WHERE BusinessEntityID = @BusinessEntityID
			AND BusinessEntityPhoneNumberID NOT IN (SELECT DeviceAddressID FROM @tblOutput WHERE CommunicationMethodID = @CommunicationMethodID)
			AND (com.IsPreferred IS NULL OR com.IsPreferred = 1)
				
	END
END
