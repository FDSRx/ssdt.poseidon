﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/19/2016
-- Description:	Updates a new BusinessEntityApplication object.
-- SAMPLE CALL:
/*

DECLARE 
	@BusinessEntityApplicationID BIGINT = NULL,
	@BusinessEntityID BIGINT,
	@ApplicationID INT,
	@ApplicationPath VARCHAR(512) = NULL,
	@IsApplicationPathNullable BIT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@Debug BIT = 1
	
EXEC dbo.spBusinessEntityApplication_Update
	@BusinessEntityApplicationID = @BusinessEntityApplicationID OUTPUT,
	@BusinessEntityID BIGINT = @BusinessEntityID,
	@ApplicationID INT = @ApplicationID,
	@ApplicationPath = @ApplicationPath,
	@IsApplicationPathNullable = @IsApplicationPathNullable,
	@CreatedBy = @CreatedBy,
	@Debug = @Debug

SELECT @BusinessEntityApplicationID AS BusinessEntityApplicationID

*/

-- SELECT * FROM dbo.BusinessEntityApplication
-- SElECT * FROM dbo.Application
-- SELECT * FROM dbo.BusinessEntity
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityApplication_Update]
	@BusinessEntityApplicationID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@ApplicationID INT = NULL,
	@ApplicationPath VARCHAR(512) = NULL,
	@IsApplicationPathNullable BIT = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000)

	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityApplicationID=' + dbo.fnToStringOrEmpty(@BusinessEntityApplicationID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationPath=' + dbo.fnToStringOrEmpty(@ApplicationPath) + ';' +
		'@IsApplicationPathNullable=' + dbo.fnToStringOrEmpty(@IsApplicationPathNullable) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	-------------------------------------------------------------------------------------------------------
	-- Record request.
	-------------------------------------------------------------------------------------------------------
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------
	SET @BusinessEntityApplicationID = NULL;
	SET @Debug = ISNULL(@Debug, 0);
	SET @ModifiedBy = ISNULL(@ModifiedBy, SUSER_NAME());
	SET @IsApplicationPathNullable = ISNULL(@IsApplicationPathNullable, 0);
	
	-------------------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------------------
	SET @BusinessEntityApplicationID =
		ISNULL(
			@BusinessEntityApplicationID,
			(
				SELECT TOP(1)
					BusinessEntityApplicationID
				FROM dbo.BusinessEntityApplication
				WHERE BusinessEntityID = @BusinessEntityID
					AND ApplicationID = @ApplicationID
			)
		);


	/*
	-------------------------------------------------------------------------------------------------------
	-- Argument null exceptions.
	-- <Summary>
	-- Inspects necessary arguments for null or empty values.
	-- </Summary>
	-------------------------------------------------------------------------------------------------------
	-- @BusinessEntityID
	IF @BusinessEntityID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplication object. Object reference (@BusinessEntityID) is not set to an instance of an object. ' +
			'The parameter, @BusinessEntityID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	
	-- @ApplicationID
	IF @ApplicationID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create BusinessEntityApplication object. Object reference (@ApplicationID) is not set to an instance of an object. ' +
			'The parameter, @ApplicationID, cannot be null.';
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END	
	*/
		
	-------------------------------------------------------------------------------------------------------
	-- Update object.
	-------------------------------------------------------------------------------------------------------
	UPDATE TOP(1) trgt
	SET
		trgt.ApplicationPath = 
			CASE 
				WHEN @IsApplicationPathNullable = 1 THEN @ApplicationPath 
				ELSE ISNULL(@ApplicationPath, trgt.ApplicationPath)
			END
	FROM dbo.BusinessEntityApplication trgt
	WHERE BusinessEntityApplicationID = @BusinessEntityApplicationID

		
END