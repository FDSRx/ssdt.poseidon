﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/12/1982
-- Description:	Adds or updates the business entity address record.
-- SAMPLE CALL:
/*

spBusinessEntity_Email_Merge
	@BusinessEntityEmailAddressID  = NULL,
	@BusinessEntityID  = 135769,
	@EmailAddressTypeID  = 2,
	@EmailAddress  = 'dhughes@hcc-care.com', -- ssimmons@hcc-care.com -- original
	@IsEmailAllowed = 0,
	@ModifiedBy  = 'dhughes@rxlps.com'

spBusinessEntity_Email_Merge
	@BusinessEntityEmailAddressID  = 491,
	@BusinessEntityID  = 135769,
	@EmailAddressTypeID  = 2,
	@EmailAddress  = 'ssimmons@hcc-care.com', -- ssimmons@hcc-care.com -- original
	@IsEmailAllowed = 0,
	@ModifiedBy  = 'dhughes@rxlps.com'
		
*/
-- SELECT * FROM dbo.BusinessEntityEmailAddress WHERE BusinessEntityEmailAddressID = 491
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Email_Merge]
	@BusinessEntityEmailAddressID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@EmailAddressTypeID INT = NULL,
	@EmailAddress VARCHAR(255) = NULL,
	@IsEmailAllowed BIT = 0,
	@IsPreferred BIT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);
	SET @IsEmailAllowed = ISNULL(@IsEmailAllowed, 0);
	SET @IsPreferred = ISNULL(@IsPreferred, 0);
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ValidateSurrogateKey BIT = 0
		

	----------------------------------------------------------------------
	-- Set variables
	----------------------------------------------------------------------	
	IF @BusinessEntityEmailAddressID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @EmailAddressTypeID = NULL;
		
		SET @ValidateSurrogateKey = 1;
	END
	
	----------------------------------------------------------------------
	-- Argument Null Exceptions
	----------------------------------------------------------------------
	----------------------------------------------------------------------
	-- Surrogate key validation.
	-- <Summary>
	-- If a surrogate key was provided, then that will be the only lookup
	-- performed.
	-- <Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityEmailAddressID, 0) = 0 AND @ValidateSurrogateKey = 1
	BEGIN			
		
		SET @ErrorMessage = 'Object reference not set to an instance of an object. The object, @BusinessEntityEmailAddressID, cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	----------------------------------------------------------------------
	-- Unique row key validation.
	-- <Summary>
	-- If a surrogate key was not provided then we will try and perform the
	-- lookup based on the records unique key.
	-- </Summary>
	----------------------------------------------------------------------
	IF ISNULL(@BusinessEntityID, 0) = 0 AND ISNULL(@EmailAddressTypeID, 0) = 0 AND @ValidateSurrogateKey = 0
	BEGIN
		
		SET @ErrorMessage = 'Object references not set to an instance of an object. The objects, @BusinessEntityID and @EmailAddressTypeID cannot be null or empty.'
			
		RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			   
		RETURN;	
		
	END	
	
	--------------------------------------------------------------------
	-- Determine if record exists
	--------------------------------------------------------------------
	EXEC dbo.spBusinessEntity_Email_Exists
		@BusinessEntityEmailAddressID = @BusinessEntityEmailAddressID OUTPUT,
		@BusinessEntityID = @BusinessEntityID,
		@EmailAddressTypeID = @EmailAddressTypeID,
		@Exists = @Exists OUTPUT
		
	----------------------------------------------------------------------
	-- Merge data
	-- <Summary>
	-- If the record does not exist, then add the record; otherwise,
	-- update the record
	-- <Summary>
	----------------------------------------------------------------------	
	IF ISNULL(@Exists, 0) = 0
	BEGIN
		-- Add record
		EXEC dbo.spBusinessEntity_Email_Create
			@BusinessEntityID = @BusinessEntityID,
			@EmailAddressTypeID = @EmailAddressTypeID,
			@EmailAddress = @EmailAddress,
			@IsEmailAllowed = @IsEmailAllowed,
			@IsPreferred = @IsPreferred,
			@CreatedBy = @CreatedBy,
			@BusinessEntityEmailAddressID = @BusinessEntityEmailAddressID OUTPUT
	END
	ELSE
	BEGIN
		-- Update record
		EXEC dbo.spBusinessEntity_Email_Update
			@BusinessEntityEmailAddressID = @BusinessEntityEmailAddressID OUTPUT,
			@BusinessEntityID = @BusinessEntityID,
			@EmailAddressTypeID = @EmailAddressTypeID,		
			@EmailAddress = @EmailAddress,
			@IsEmailAllowed = @IsEmailAllowed,
			@IsPreferred = @IsPreferred,
			@ModifiedBy = @ModifiedBy
	END


	
END
