﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/17/2013
-- Description:	Deletes a business entity's email address
-- SAMPLE CALL:
/*

	EXEC dbo.spBusinessEntity_Email_Delete
		@BusinessEntityID = 10684,
		@EmailAddressTypeID = 2,
		@ModifiedBy = 'dhughes'
*/

-- SELECT * FROM dbo.BusinessEntityEmailAddress
-- SELECT * FROM dbo.BusinessEntityEmailAddress_History
-- SELECT * FROM dbo.EmailAddressType
-- SELECT * FROM dbo.BusinessEntityEmailAddress WHERE BusinessEntityEmailAddressID = 10684
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Email_Delete]
	@BusinessEntityEmailAddressID BIGINT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@EmailAddressTypeID INT = NULL,
	@EmaillAddress VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID)

	-------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy

	-------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------		
	IF @BusinessEntityEmailAddressID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @EmailAddressTypeID = NULL;
		SET @EmaillAddress = NULL;
	END	
		
	-------------------------------------------------------------------------------
	-- Remove object.
	-------------------------------------------------------------------------------	
	DELETE TOP(1) obj
	--SELECT TOP 1 *
	--SELECT *
	FROM dbo.BusinessEntityEmailAddress obj
	WHERE ( obj.BusinessEntityEmailAddressID = @BusinessEntityEmailAddressID )
		OR (
			obj.BusinessEntityID = @BusinessEntityID
			AND obj.EmailAddressTypeID = @EmailAddressTypeID
			AND ( ( @EmaillAddress IS NULL OR @EmaillAddress = '' ) OR obj.EmailAddress = @EmaillAddress )
		)
		
			
END
