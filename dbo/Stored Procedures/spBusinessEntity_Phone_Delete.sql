﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/17/2013
-- Description:	Deletes a BusinessEntityPhone object.
-- SAMPLE CALL:
/*
	-- Delete work phone.
	EXEC dbo.spBusinessEntity_Phone_Delete
		--@BusinessEntityPhoneNumberID = 891793,
		@BusinessEntityID = 10684,
		@PhoneNumberTypeID = 3,
		@ModifiedBy = 'dhughes'
	
	-- Delete fax phone.
	EXEC dbo.spBusinessEntity_Phone_Delete
		--@BusinessEntityPhoneNumberID = 891792,
		@BusinessEntityID = 10684,
		@PhoneNumberTypeID = 4,
		@ModifiedBy = 'dhughes'
*/

-- SELECT * FROM dbo.BusinessEntityPhone WHERE BusinessEntityID = 10684
-- SELECT * FROM dbo.BusinessEntityPhone_History WHERE BusinessEntityID = 10684
-- SELECT * FROM dbo.PhoneNumberType
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Phone_Delete] 
	@BusinessEntityPhoneNumberID BIGINT = NULL,
	@BusinessEntityID BIGINT = NULL,
	@PhoneNumberTypeID INT = NULL,
	@PhoneNumber VARCHAR(25) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------------------------------------
	-- Instance variables.
	-------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000),
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + ',' + OBJECT_NAME(@@PROCID)
		
		
	-------------------------------------------------------------------------------
	-- Save ModifiedBy property in "session" like object.
	-------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy

	-------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------		
	IF @BusinessEntityPhoneNumberID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @PhoneNumberTypeID = NULL;
		SET @PhoneNumber = NULL;
	END	
		
	-------------------------------------------------------------------------------
	-- Remove object.
	-------------------------------------------------------------------------------	
	DELETE TOP(1) obj
	FROM dbo.BusinessEntityPhone obj
	WHERE ( obj.BusinessEntityPhoneNumberID = @BusinessEntityPhoneNumberID )
		OR (
			obj.BusinessEntityID = @BusinessEntityID
			AND obj.PhoneNumberTypeID = @PhoneNumberTypeID
			AND ( ( @PhoneNumber IS NULL OR @PhoneNumber = '' ) OR obj.PhoneNumber = @PhoneNumber )
		)
	
END
