﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 10/3/2014
-- Description:	Creates/Tracks an attempted request.
-- SAMPLE CALL:
/*

DECLARE @RequestTrackerID BIGINT

EXEC dbo.spRequestTracker_Create
	@RequestTypeID = 2,
	@SourceName = 'dbo.spRequestTracker_Create',
	@ApplicationKey = 'MPC',
	@BusinessKey = '000000',
	@RequesterKey = 'dhughes',
	@RequestKey = 'MPC-000000-dhughes',
	@MaxRequestsAllowed = 3,
	@RequestData = 'Test Call',
	@CreatedBy = 'dhughes',
	@RequestTrackerID = @RequestTrackerID OUTPUT

SELECT @RequestTrackerID AS RequestTrackerID

*/

-- SELECT * FROM dbo.RequestTracker ORDER BY RequestTrackerID DESC
-- TRUNCATE TABLE dbo.RequestTracker 
-- =============================================
CREATE PROCEDURE [dbo].[spRequestTracker_Create]
	@RequestTypeID INT,
	@SourceName VARCHAR(256) = NULL,
	@ApplicationKey VARCHAR(50) = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@RequesterKey VARCHAR(256) = NULL,
	@RequestKey VARCHAR(256),
	@RequestCount INT = NULL,
	@RequestsRemaining INT = NULL,
	@MaxRequestsAllowed INT = NULL,
	@IsLocked BIT = NULL,
	@DateExpires DATETIME = NULL,
	@ApplicationID INT = NULL,
	@BusinessEntityID INT = NULL,
	@RequesterID INT = NULL,
	@RequestData VARCHAR(MAX) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@RequestTrackerID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------
	SET @RequestTrackerID = NULL;
	SET @DateExpires = ISNULL(@DateExpires, '12/31/9999');
	SET @IsLocked = ISNULL(@IsLocked, 0);
	SET @RequestCount = ISNULL(@RequestCount, 1);
	SET @MaxRequestsAllowed = ISNULL(@MaxRequestsAllowed, 2147483647);
	
	-- @RequestCount and @MaxRequestsAllowed are required to be populated to determine how many requests
	-- are allowed to be attempted.
	SET @RequestsRemaining =
		CASE 
			WHEN @MaxRequestsAllowed IS NOT NULL THEN @MaxRequestsAllowed - @RequestCount
			ELSE ISNULL(@RequestsRemaining, 2147483647)
		END;
	

	
	
	---------------------------------------------------------------------------------------------
	-- Create/Track request.
	---------------------------------------------------------------------------------------------
	INSERT INTO dbo.RequestTracker (
		RequestTypeID,
		SourceName,
		ApplicationKey,
		BusinessKey,
		RequesterKey,
		RequestKey,
		RequestCount,
		RequestsRemaining,
		MaxRequestsAllowed,
		IsLocked,
		DateExpires,
		ApplicationID,
		BusinessEntityID,
		RequesterID,
		RequestData,
		Arguments,
		CreatedBy	
	)
	SELECT
		@RequestTypeID,
		@SourceName,
		@ApplicationKey,
		@BusinessKey,
		@RequesterKey,
		@RequestKey,
		@RequestCount,
		@RequestsRemaining,
		@MaxRequestsAllowed,
		@IsLocked,
		@DateExpires,
		@ApplicationID,
		@BusinessEntityID,
		@RequesterID,
		@RequestData,
		@Arguments,
		@CreatedBy
	
	
	-- Retrive record identity.
	SET @RequestTrackerID = SCOPE_IDENTITY();
	
	
END
