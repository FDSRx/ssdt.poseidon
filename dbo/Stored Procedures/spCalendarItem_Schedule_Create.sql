﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/6/2014
-- Description:	Creates a new CalendarItem object with attached schedule.
-- Change log:
-- 2/26/2016 - dhughes - Modified the procedure to accept more inputs. Modified the procedure to call the CaleanderItem object create method with
--				more inputs.
-- 6/15/2016 - dhughes - Modified the procedure to accept the Application and Business objects as input parameters.

-- SAMPLE CALL:
/*

DECLARE 
	@NoteID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(59) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@CorrelationKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@Location VARCHAR(500) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@IsAllDayEvent BIT = NULL,
	@FrequencyTypeID INT = NULL,
	@FrequencyTypeCode VARCHAR(50) = NULL,
	@DateEffectiveStart DATETIME = NULL,
	@DateEffectiveEnd DATETIME = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CalendarItemID BIGINT = NULL OUTPUT,
	@NoteScheduleID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL


EXEC dbo.spCalendarItem_Schedule_Create
	@NoteID = @NoteID OUTPUT,
	@ApplicationID = @ApplicationID,
	@ApplicationCode = @ApplicationCode,
	@BusinessID = @BusinessID,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@ScopeID = @ScopeID,
	@ScopeCode = @ScopeCode,
	@BusinessEntityID = @BusinessEntityID,
	@NotebookID = @NotebookID,
	@NotebookCode = @NotebookCode,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityID = @PriorityID,
	@PriorityCode = @PriorityCode,
	@Tags = @Tags,
	@CorrelationKey = @CorrelationKey,
	@AdditionalData = @AdditionalData,
	@Location = @Location,
	@OriginID = @OriginID,
	@OriginCode = @OriginCode,
	@OriginDataKey = @OriginDataKey,
	@DateStart = @DateStart,
	@DateEnd = @DateEnd,
	@IsAllDayEvent = @IsAllDayEvent,
	@FrequencyTypeID = @FrequencyTypeID,
	@FrequencyTypeCode = @FrequencyTypeCode,
	@DateEffectiveStart = @DateEffectiveStart,
	@DateEffectiveEnd = @DateEffectiveEnd,
	@CreatedBy = @CreatedBy,
	@CalendarItemID = @CalendarItemID OUTPUT,
	@NoteScheduleID = @NoteScheduleID OUTPUT,
	@Debug = @Debug



SELECT @CalendarItemID AS CalendarItemID, @NoteScheduleID AS NoteScheduleID, @NoteID AS NoteID

*/

-- SELECT * FROM dbo.CalendarItem
-- SELECT * FROM schedule.NoteSchedule
-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Tag
-- SELECT * FROM dbo.NoteTag
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.Notebook
-- SELECT * FROM dbo.Priority
-- SELECT TOP 10 * FROM phrm.vwPatient WHERE LastName LIKE '%Simmons%'

-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spCalendarItem_Schedule_Create]
	@NoteID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@ScopeID INT,
	@ScopeCode VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(59) = NULL,
	@Tags VARCHAR(MAX) = NULL, -- A comma separated list of tags that identify a note.
	@CorrelationKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@Location VARCHAR(500) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@IsAllDayEvent BIT = NULL,
	@FrequencyTypeID INT = NULL,
	@FrequencyTypeCode VARCHAR(50) = NULL,
	@DateEffectiveStart DATETIME = NULL,
	@DateEffectiveEnd DATETIME = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@CalendarItemID BIGINT = NULL OUTPUT,
	@NoteScheduleID BIGINT = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID)  + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID)  + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode)  + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID)  + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey)  + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType)  + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID)  + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode)  + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID)  + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID)  + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode)  + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title)  + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo)  + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID)  + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode)  + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags)  + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey)  + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData))  + ';' +
		'@Location=' + dbo.fnToStringOrEmpty(@Location)  + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID)  + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode)  + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey)  + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart)  + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd)  + ';' +
		'@IsAllDayEvent=' + dbo.fnToStringOrEmpty(@IsAllDayEvent)  + ';' +
		'@FrequencyTypeID=' + dbo.fnToStringOrEmpty(@FrequencyTypeID)  + ';' +
		'@FrequencyTypeCode=' + dbo.fnToStringOrEmpty(@FrequencyTypeCode)  + ';' +
		'@DateEffectiveStart=' + dbo.fnToStringOrEmpty(@DateEffectiveStart)  + ';' +
		'@DateEffectiveEnd=' + dbo.fnToStringOrEmpty(@DateEffectiveEnd)  + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy)  + ';' +
		'@CalendarItemID=' + dbo.fnToStringOrEmpty(@CalendarItemID)  + ';' +
		'@NoteScheduleID=' + dbo.fnToStringOrEmpty(@NoteScheduleID)  + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug)  + ';' ;


	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/
	
	
	--------------------------------------------------------------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------------------------------------------------------------
	SET @NotebookID = ISNULL(@NotebookID, dbo.fnGetNotebookID('MCIS'));	
	SET @DateEnd = ISNULL(@DateEnd, @DateStart);
	SET @IsAllDayEvent = ISNULL(@IsAllDayEvent, 0);

	--------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	--------------------------------------------------------------------------------------------------------------------



	------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. If the request is unable to be processed, all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	------------------------------------------------------------------------------------------------------------		
	
	BEGIN TRY
	
		--------------------------------------------------------------------------------------------------------------------
		-- Null argument validation
		-- <Summary>
		-- Validates if any of the necessary arguments were provided with
		-- data.
		-- </Summary>
		--------------------------------------------------------------------------------------------------------------------
		-- @Title 
		IF dbo.fnIsNullOrWhiteSpace(@Title) = 1
		BEGIN
			SET @ErrorMessage = 'Unable to create CalendarItem object. Object reference (@Title) is not set to an instance of an object. ' +
				'The @Title parameter cannot be null or empty.';
			
			RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			  
			 RETURN;
		END	
		
		-- @DateStart
		IF @DateStart IS NULL
		BEGIN
			SET @ErrorMessage = 'Unable to create CalendarItem object. Object reference (@DateStart) is not set to an instance of an object. ' +
				'The @DateStart parameter cannot be null or empty.';
			
			RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			  
			 RETURN;
		END	
		
		-- @DateEnd
		IF @DateEnd IS NULL
		BEGIN
			SET @ErrorMessage = 'Unable to create CalendarItem object. Object reference (@DateEnd) is not set to an instance of an object. ' +
				'The @DateEnd parameter cannot be null or empty.';
			
			RAISERROR (@ErrorMessage, -- Message text.
			   16, -- Severity.
			   1 -- State.
			   );	
			  
			 RETURN;
		END	
		
	--------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	--------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END

		--------------------------------------------------------------------------------------------------------------------
		-- Create CalendarItem object.
		--------------------------------------------------------------------------------------------------------------------		
			EXEC dbo.spCalendarItem_Create
				@ApplicationID = @ApplicationID,
				@ApplicationCode = @ApplicationCode,
				@BusinessID = @BusinessID,
				@BusinessKey = @BusinessKey,
				@BusinessKeyType = @BusinessKeyType,
				@ScopeID = @ScopeID,
				@ScopeCode = @ScopeCode,
				@BusinessEntityID = @BusinessEntityID,
				@NotebookID = @NotebookID,
				@NotebookCode = @NotebookCode,
				@PriorityID = @PriorityID,
				@PriorityCode = @PriorityCode,
				@Title = @Title,
				@Memo = @Memo,
				@Tags = @Tags,
				@CorrelationKey = @CorrelationKey,
				@AdditionalData = @AdditionalData,
				@OriginID = @OriginID,
				@OriginCode = @OriginCode,
				@OriginDataKey = @OriginDataKey,
				@Location = @Location,
				@CreatedBy = @CreatedBy,
				@CalendarItemID = @CalendarItemID OUTPUT		
	

		--------------------------------------------------------------------------------------------------------------------
		-- Create NoteSchedule object.
		--------------------------------------------------------------------------------------------------------------------		
		EXEC schedule.spNoteSchedule_Create
			@NoteID = @CalendarItemID,
			@DateStart = @DateStart,
			@DateEnd = @DateEnd,
			@IsAllDayEvent = @IsAllDayEvent,
			@FrequencyTypeID = @FrequencyTypeID,
			@FrequencyTypeCode = @FrequencyTypeCode,
			@DateEffectiveStart = @DateEffectiveStart,
			@DateEffectiveEnd = @DateEffectiveEnd,
			@CreatedBy = @CreatedBy,
			@NoteScheduleID = @NoteScheduleID OUTPUT
			
	
	--------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	--------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args
		
		SET @ErrorMessage = ERROR_MESSAGE();

		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		
	END CATCH		

END
