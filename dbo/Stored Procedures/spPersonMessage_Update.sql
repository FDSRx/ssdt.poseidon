﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/9/2013
-- Description:	Updates person message
-- SELECT * FROM dbo.PersonMessage
-- =============================================
CREATE PROCEDURE [dbo].[spPersonMessage_Update]
	@PersonMessageID VARCHAR(MAX),
	@PersonID INT,
	@FolderID INT = NULL,
	@IsRead BIT = NULL,
	@IsDeleted BIT = NULL,
	@IsStarred BIT = NULL,
	@IsConversation BIT = NULL,
	@CanRespond BIT = NULL,
	@RoutingData XML = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	------------------------------------------------------
	-- Short circut update a null was passed as the ID
	------------------------------------------------------
	IF @PersonMessageID IS NULL
	BEGIN
		RETURN;
	END	

	------------------------------------------------------
	-- Temporary resources
	------------------------------------------------------
	DECLARE @tblPersonMessageIDList AS TABLE (
		PersonMessageID BIGINT
	);
	
	DECLARE @tblOutput AS TABLE (
		IsConversation BIT,
		ThreadKey UNIQUEIDENTIFIER
	);
	
	------------------------------------------------------
	-- Create PersonMessageID List
	------------------------------------------------------
	INSERT INTO @tblPersonMessageIDList (
		PersonMessageID
	)
	SELECT
		LTRIM(RTRIM(Value)) AS Value
	FROM dbo.fnSplit(@PersonMessageID, ',')
	WHERE ISNUMERIC(LTRIM(RTRIM(Value))) = 1

	------------------------------------------------------
	-- Update main threads
	-- <Summary>
	-- Updates the main message threads with the 
	-- supplied information
	-- <Summary>
	------------------------------------------------------
	UPDATE msg
	SET
		msg.FolderID = CASE WHEN @FolderID IS NOT NULL THEN @FolderID ELSE FolderID END,
		msg.IsRead = CASE WHEN @IsRead IS NOT NULL THEN @IsRead ELSE IsRead END,
		msg.IsDeleted = CASE WHEN @IsDeleted IS NOT NULL THEN @IsDeleted ELSE IsDeleted END,
		msg.IsStarred = CASE WHEN @IsStarred IS NOT NULL THEN @IsStarred ELSE IsStarred END,
		msg.IsConversation = CASE WHEN @IsConversation IS NOT NULL THEN @IsConversation ELSE IsConversation END,
		msg.CanRespond = CASE WHEN @CanRespond IS NOT NULL THEN @CanRespond ELSE CanRespond END,
		msg.RoutingData = CASE WHEN @RoutingData IS NOT NULL THEN @RoutingData ELSE RoutingData END,
		msg.DateModified = GETDATE()
	--SELECT *
	OUTPUT inserted.IsConversation, inserted.ThreadKey INTO @tblOutput(IsConversation, ThreadKey)
	--SELECT *
	FROM dbo.PersonMessage msg
		JOIN @tblPersonMessageIDList tmp
			ON msg.PersonMessageID = tmp.PersonMessageID
	WHERE PersonID = @PersonID
	

	------------------------------------------------------
	-- Update conversation threads
	-- <Summary>
	-- If the supplied message id(s) ara a conversation thread
	-- then update all of the threads messages.
	-- </Summary>
	------------------------------------------------------
	IF EXISTS (SELECT TOP 1 ThreadKey FROM @tblOutput WHERE IsConversation = 1)
	BEGIN
	
		UPDATE msg
		SET
			msg.FolderID = CASE WHEN @FolderID IS NOT NULL THEN @FolderID ELSE FolderID END,
			msg.IsRead = CASE WHEN @IsRead IS NOT NULL THEN @IsRead ELSE IsRead END,
			msg.IsDeleted = CASE WHEN @IsDeleted IS NOT NULL THEN @IsDeleted ELSE IsDeleted END,
			msg.IsStarred = CASE WHEN @IsStarred IS NOT NULL THEN @IsStarred ELSE IsStarred END,
			-- ?? Not sure if routing packets should all be updated in this scenario.
			-- May need another flag to indicate these types of procedures.
			--msg.RoutingData = CASE WHEN @RoutingData IS NOT NULL THEN @RoutingData ELSE RoutingData END,
			msg.DateModified = GETDATE()
		--SELECT *
		OUTPUT inserted.IsConversation, inserted.ThreadKey INTO @tblOutput(IsConversation, ThreadKey)
		--SELECT *
		FROM dbo.PersonMessage msg
			JOIN @tblOutput tmp
				ON msg.ThreadKey = tmp.ThreadKey
		WHERE msg.PersonID = @PersonID
			AND tmp.IsConversation = 1
	
	END
		

	

END
