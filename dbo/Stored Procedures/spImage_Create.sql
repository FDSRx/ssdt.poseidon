﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/21/2014
-- Description:	Adds an image to the image repository
-- =============================================
CREATE PROCEDURE [dbo].[spImage_Create]
	@ApplicationID INT = NULL,
	@BusinessID BIGINT = NULL,
	@PersonID BIGINT = NULL,
	@KeyName VARCHAR(256),
	@Name VARCHAR(256) = NULL,
	@Caption VARCHAR(4000) = NULL,
	@Description VARCHAR(MAX) = NULL,
	@FileName VARCHAR(256) = NULL,
	@FilePath VARCHAR(1000) = NULL,
	@FileDataString VARCHAR(MAX) = NULL,
	@FileDataBinary VARBINARY(MAX) = NULL,
	@LanguageID INT = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ImageID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------
	SET @ImageID = NULL;
	
	------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)

	------------------------------------------------------------
	-- Set variables
	------------------------------------------------------------
	SET @LanguageID = ISNULL(@LanguageID, dbo.fnGetLanguageID('en')); -- default to english.
	SET @FileName = CASE 
						WHEN ISNULL(@FileName, '') = '' THEN 
							dbo.fnToStringOrEmpty(ISNULL(@ApplicationID, -999)) + '_' +
							dbo.fnToStringOrEmpty(ISNULL(@BusinessID, -999)) + '_' +
							dbo.fnToStringOrEmpty(ISNULL(@PersonID, -999))  + '_' +
							dbo.fnToStringOrEmpty(ISNULL(@LanguageID, -999))  + '_' +
							dbo.fnToStringOrEmpty(NEWID())
						ELSE @FileName END
				
		
	------------------------------------------------------------
	-- Argument null exception
	------------------------------------------------------------
	IF ISNULL(@KeyName, '') = ''
	BEGIN
		SET @ErrorMessage = 'Object reference (@KeyName) is not set to an instance of an object. The object cannot be null or empty.'
		
		RAISERROR (@ErrorMessage, -- Message text.
		   16, -- Severity.
		   1 -- State.
		   );	
		  
		 RETURN;
	END
	
	------------------------------------------------------------
	-- Create image record.
	------------------------------------------------------------
	INSERT INTO dbo.Images (
		ApplicationID,
		BusinessID,
		PersonID,
		KeyName,
		Name,
		Caption,
		Description,
		FileName,
		FilePath,
		FileDataString,
		FileDataBinary,
		LanguageID,
		CreatedBy
	)
	SELECT
		@ApplicationID,
		@BusinessID,
		@PersonID,
		@KeyName,
		@Name,
		@Caption,
		@Description,
		@FileName,
		@FilePath,
		@FileDataString,
		@FileDataBinary,
		@LanguageID,
		@CreatedBy
	
	
	-- Retrieve record identity
	SET @ImageID = SCOPE_IDENTITY();
	
	
	
	
END

