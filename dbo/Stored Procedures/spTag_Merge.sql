﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/13/2014
-- Description:	Creates a new Tag object.
-- SAMPLE CALL:
/*
DECLARE @TagID BIGINT

EXEC dbo.spTag_Merge
	@TagTypeID = 'UD,PHRM',
	@Name = 'UD - High Risk Medications',
	@CreatedBy = 'dhughes',
	@TagID = @TagID OUTPUT

SELECT @TagID AS TagID
*/
-- SELECT * FROM dbo.Tag
-- SELECT * FROM dbo.TagTagType
-- =============================================
CREATE PROCEDURE [dbo].[spTag_Merge]
	@TagTypeID VARCHAR(MAX) = NULL,
	@Name VARCHAR(256),
	@CreatedBy VARCHAR(256) = NULL,
	@TagID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------
	-- Sanitize input
	--------------------------------------------------------------
	SET @TagID = NULL;

	--------------------------------------------------------------
	-- Determine tag existence
	-- <Summary>
	-- Determine if the tag exists.  If the tag already exists,
	-- then exit the merge process and return the found TagID.
	-- <Summary>
	-- <Remarks>
	-- Tags are always adds.  They are NEVER updates.
	-- </Summary>
	--------------------------------------------------------------
	EXEC dbo.spTag_Exists
		@Name = @Name,
		@TagID = @TagID OUTPUT
	
	
	-- If a tag was found, then exit the code.
	IF @TagID IS NOT NULL
	BEGIN
		RETURN;
	END	
		
	--------------------------------------------------------------
	-- Create new tag object.
	-- <Summary>
	-- Creates a new tag entry and returns its unique row identifier.
	-- </Summary>
	--------------------------------------------------------------
	EXEC dbo.spTag_Create
		@TagTypeID = @TagTypeID,
		@Name = @Name,
		@CreatedBy = @CreatedBy,
		@TagID = @TagID OUTPUT
	
END
