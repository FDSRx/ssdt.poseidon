﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 11/14/2013
-- Description:	Returns a list of all languages.
-- SAMPLE CALL: spLanguage_Get_List
-- =============================================
CREATE PROCEDURE [dbo].[spLanguage_Get_List]
	@LanguageID INT = NULL,
	@Code VARCHAR(25) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		LanguageID,
		Code,
		Name,
		DateCreated,
		DateModified
	--SELECT *
	FROM dbo.Language
	WHERE LanguageID = ISNULL(@LanguageID, LanguageID)
	
END
