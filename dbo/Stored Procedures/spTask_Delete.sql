﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/26/2015
-- Description:	Deletes a task item.
-- Change log:
-- 7/6/2016 - dhughes - Modified the procedure to accept a VARCHAR note input vs. an integer so that multiple notes
--				could be sent.

-- SAMPLE CALL:
/*

DECLARE
	@NoteID BIGINT,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL

EXEC dbo.spTask_Delete
	@NoteID = @NoteID,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/

-- SELECT * FROM dbo.Task ORDER BY 1 DESC
-- SELECT TOP 50 * FROM dbo.vwTask ORDER BY 1 DESC
-- SELECT TOP 50 * FROM dbo.vwNote ORDER BY 1 DESC

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spTask_Delete]
	@NoteID VARCHAR(MAX),
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/


	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
			

	------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------
	
	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Deubber On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@NoteID AS NoteID
	END


	------------------------------------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	------------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy

	------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. If the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	------------------------------------------------------------------------------------------------------------		
	BEGIN TRY
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END	
	------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	------------------------------------------------------------------------------------------------------------
		
		------------------------------------------------------------------------------------------------------------
		-- Remove task item from schedule.
		------------------------------------------------------------------------------------------------------------	
		EXEC schedule.spNoteSchedule_Delete
			@NoteID = @NoteID,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug
			
		------------------------------------------------------------------------------------------------------------
		-- Remove task item record.
		------------------------------------------------------------------------------------------------------------
		DELETE trgt
		--SELECT *
		FROM dbo.Task trgt
		WHERE NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteID, ',') WHERE ISNUMERIC(Value) = 1)

		------------------------------------------------------------------------------------------------------------
		-- Remove note item.
		------------------------------------------------------------------------------------------------------------	
		EXEC dbo.spNote_Delete
			@NoteID = @NoteID,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug


	------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0
	BEGIN
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
	
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		-- Log transaction error.	
		EXECUTE dbo.spLogException
			@Arguments = @Args

		SET @ErrorMessage = ERROR_MESSAGE();
	
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		
	END CATCH


END
