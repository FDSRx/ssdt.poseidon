﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/15/2016
-- Description:	Updates a Store object.
-- Change log:

-- SAMPLE CALL: 
/*

DECLARE
	@BusinessEntityID BIGINT = NULL,
	-- Business properties.
	@SourceSystemKey VARCHAR(256) = NULL,
	-- Store properties.
	@SourceStoreID INT, -- Legacy operation.
	@StoreName VARCHAR(255) = 'Test Pharmacy',
	@NABP VARCHAR(50) = '0000240',
	@TimeZoneID INT = NULL,
	@TimeZoneCode VARCHAR(50) = NULL,
	@TimeZonePrefix VARCHAR(10) = NULL,
	@SupportDST BIT = NULL,
	@Demographics XML = NULL,
	@Website VARCHAR(256) = NULL,
	-- Provider properties
	@ProviderID INT = NULL,
	@ProviderCode VARCHAR(50) = NULL,
	@ProviderUsername VARCHAR(256) = NULL,
	@ProviderPasswordUnencrypted VARCHAR(256) = NULL,
	@ProviderPasswordEncrypted VARCHAR(256) = NULL,
	-- Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(75) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(25) = NULL,
	--Business Phone (Work & Fax)
	@PhoneNumber_Work VARCHAR(25) = NULL,
	@PhoneNumber_Fax VARCHAR(25) = NULL,
	--Business Email (Primary & alternate)
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	-- Output
	@Exists BIT = NULL,
	-- Error
	@ErrorLogID BIGINT = NULL,
	-- Debug
	@Debug BIT = 1

EXEC dbo.spStore_Update
	@BusinessEntityID = @BusinessEntityID OUTPUT,
	-- Business properties.
	@SourceSystemKey = @SourceSystemKey,
	-- Store properties.
	@SourceStoreID = @SourceStoreID, -- Legacy operation.
	@StoreName = @StoreName,
	@NABP = @NABP,
	@TimeZoneID = @TimeZoneID,
	@TimeZoneCode = @TimeZoneCode,
	@SupportDST = @SupportDST,
	@Demographics = @Demographics,
	@Website = @Website,
	-- Provider properties
	@ProviderID = @ProviderID,
	@ProviderCode = @ProviderCode,
	@ProviderUsername = @ProviderUsername,
	@ProviderPasswordUnencrypted = @ProviderPasswordUnencrypted,
	@ProviderPasswordEncrypted = @ProviderPasswordEncrypted,
	-- Address
	@AddressLine1 = @AddressLine1,
	@AddressLine2 = @AddressLine2,
	@City = @City,
	@State = @State,
	@PostalCode = @PostalCode,
	--Business Phone (Work & Fax)
	@PhoneNumber_Work = @PhoneNumber_Work,
	@PhoneNumber_Fax = @PhoneNumber_Fax,
	--Business Email (Primary & alternate)
	@EmailAddress_Primary = @EmailAddress_Primary,
	@EmailAddress_Alternate = @EmailAddress_Alternate,
	-- Caller
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy,
	-- Output
	@Exists = @Exists OUTPUT,
	-- Error
	@ErrorLogID = @ErrorLogID OUTPUT,
	-- Debug
	@Debug = @Debug

SELECT @BusinessEntityID AS BusinessEntityID, @Exists AS IsFound

*/

-- SELECT * FROM dbo.Business
-- SELECT * FROM dbo.Store
-- SELECT * FROM dbo.Store WHERE BusinessEntityID = 1
-- SELECT * FROM dbo.Store_History WHERE BusinessEntityID = 1
-- SELECT * FROM dbo.Store ORDER BY SourceStoreID ASC
-- SELECT * FROM dbo.Provider
-- SELECT * FROM dbo.BusinessEntityProvider
-- SELECT * FROM dbo.TimeZone

-- SELECT * FROM dbo.ErrorLog ORDER BY 1 DESC
-- SELECT * FROM dbo.InformationLog ORDER BY 1 DESC
-- =============================================
CREATE PROCEDURE [dbo].[spStore_Update]
	@BusinessEntityID BIGINT = NULL OUTPUT,
	-- Business properties.
	@SourceSystemKey VARCHAR(256) = NULL,
	-- Store properties.
	@SourceStoreID INT, -- Legacy operation.
	@StoreName VARCHAR(255),
	@NABP VARCHAR(50) = NULL,
	@TimeZoneID INT = NULL,
	@TimeZoneCode VARCHAR(50) = NULL,
	@TimeZonePrefix VARCHAR(10) = NULL,
	@SupportDST BIT = NULL,
	@Demographics XML = NULL,
	@Website VARCHAR(256) = NULL,
	-- Provider properties
	@ProviderID INT = NULL,
	@ProviderCode VARCHAR(50) = NULL,
	@ProviderUsername VARCHAR(256) = NULL,
	@ProviderPasswordUnencrypted VARCHAR(256) = NULL,
	@ProviderPasswordEncrypted VARCHAR(256) = NULL,
	-- Address
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(75) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(25) = NULL,
	--Business Phone (Work & Fax)
	@PhoneNumber_Work VARCHAR(25) = NULL,
	@PhoneNumber_Fax VARCHAR(25) = NULL,
	--Business Email (Primary & alternate)
	@EmailAddress_Primary VARCHAR(255) = NULL,
	@EmailAddress_Alternate VARCHAR(255) = NULL,
	-- Caller
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	-- Output
	@Exists BIT = NULL OUTPUT,
	-- Error
	@ErrorLogID BIGINT = NULL OUTPUT,
	-- Debug
	@Debug BIT = NULL
	
AS
BEGIN


	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	------------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@ErrorCode VARCHAR(50) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	------------------------------------------------------------------------------------------------------------------------
	-- Construct argument list.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE @Args VARCHAR(MAX) =
		'@SourceStoreID=' + dbo.fnToStringOrEmpty(@SourceStoreID) + ';' +
		'@SourceSystemKey=' + dbo.fnToStringOrEmpty(@SourceSystemKey) + ';' +
		'@StoreName=' + dbo.fnToStringOrEmpty(@StoreName) + ';' +
		'@NABP=' + dbo.fnToStringOrEmpty(@NABP) + ';' +
		'@TimeZoneID=' + dbo.fnToStringOrEmpty(@TimeZoneID) + ';' +
		'@TimeZoneCode=' + dbo.fnToStringOrEmpty(@TimeZoneCode) + ';' +
		'@TimeZonePrefix=' + dbo.fnToStringOrEmpty(@TimeZonePrefix) + ';' +
		'@SupportDST=' + dbo.fnToStringOrEmpty(@SupportDST) + ';' +
		'@Demographics=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @Demographics)) + ';' +
		'@Website=' + dbo.fnToStringOrEmpty(@Website) + ';' +
		'@ProviderID=' + dbo.fnToStringOrEmpty(@ProviderID) + ';' +
		'@ProviderCode=' + dbo.fnToStringOrEmpty(@ProviderCode) + ';' +
		'@ProviderUsername=' + dbo.fnToStringOrEmpty(@ProviderUsername) + ';' +
		'@ProviderPasswordUnencrypted=' + dbo.fnToStringOrEmpty(@ProviderPasswordUnencrypted) + ';' +
		'@ProviderPasswordEncrypted=' + dbo.fnToStringOrEmpty(@ProviderPasswordEncrypted) + ';' +
		'@AddressLine1=' + dbo.fnToStringOrEmpty(@AddressLine1) + ';' +
		'@AddressLine2=' + dbo.fnToStringOrEmpty(@AddressLine2) + ';' +
		'@City=' + dbo.fnToStringOrEmpty(@City) + ';' +
		'@State=' + dbo.fnToStringOrEmpty(@State) + ';' +
		'@PostalCode=' + dbo.fnToStringOrEmpty(@PostalCode) + ';' +
		'@PhoneNumber_Work=' + dbo.fnToStringOrEmpty(@PhoneNumber_Work) + ';' +
		'@PhoneNumber_Fax=' + dbo.fnToStringOrEmpty(@PhoneNumber_Fax) + ';' +
		'@EmailAddress_Primary=' + dbo.fnToStringOrEmpty(@EmailAddress_Primary) + ';' +
		'@EmailAddress_Alternate=' + dbo.fnToStringOrEmpty(@EmailAddress_Alternate) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@Exists=' + dbo.fnToStringOrEmpty(@Exists) + ';' +
		'@ErrorLogID=' + dbo.fnToStringOrEmpty(@ErrorLogID) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	------------------------------------------------------------------------------------------------------------------------
	-- Log request.
	------------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/	


	------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------------------
	SET @ErrorLogID = NULL;
	SET @Exists = 0;
	SET @Debug = ISNULL(@Debug, 0);
	SET @ModifiedBy = COALESCE(@ModifiedBy, @CreatedBy, SUSER_NAME());

	
	------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@BusinessEntityTypeID INT,
		@BusinessTypeID INT
		
	
	------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------------------
	SET @BusinessEntityTypeID = dbo.fnGetBusinessEntityTypeID('STRE');
	SET @BusinessTypeID = dbo.fnGetBusinessTypeID('STRE');
	SET @ProviderID = ISNULL(dbo.fnGetProviderID(@ProviderID), dbo.fnGetProviderID(@ProviderCode));
	SET @SourceSystemKey = ISNULL(@SourceSystemKey, @SourceStoreID);

	-- Retrieve BusinessEntityID object.
	SET @BusinessEntityID = ISNULL(@BusinessEntityID, (SELECT BusinessEntityID FROM dbo.Store WHERE Nabp = @Nabp));

	-- Debug
	IF @Debug = 1
	BEGIN 
		SELECT 'DebuggerOn' AS DebugMode, @ProcedureName AS procedureName, 'Getter/Setter methods.' AS ActionMethod,
			@BusinessEntityTypeID AS BusinessEntityTypeID, @BusinessTypeID AS BusinessTypeID, 
			@ProviderID AS ProviderID, @ProviderCode AS ProviderCode, @TimeZoneID AS TimeZoneID, @TimeZoneCode AS TimeZoneCode,
			@TimeZonePrefix AS TimeZonePrefix, @SourceSystemKey AS SourceSystemKey, @BusinessEntityID AS BusinessID, 
			@CreatedBy AS CreatedBy, @SupportDST AS SupportDST
	END



	------------------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request. If the request is unable to be processed all applicable pieces will be
	-- returned to their original state (i.e. a rollback will be performed if applicable).
	-- </Summary>
	------------------------------------------------------------------------------------------------------------------------
	BEGIN TRY	
	IF @Trancount = 0
	BEGIN
		BEGIN TRANSACTION;
	END
	------------------------------------------------------------------------------------------------------------------------
	-- Begin data transaction.
	------------------------------------------------------------------------------------------------------------------------
	
		------------------------------------------------------------------------------------------------------------------------
		-- Transaction scoope variables.
		------------------------------------------------------------------------------------------------------------------------	
		DECLARE 
			@AddressID BIGINT
			
		
		DECLARE @tblOutput AS TABLE (
			Idx INT,
			BusinessEntityID BIGINT,
			SourceSystemKey VARCHAR(256)
		);			
		
		------------------------------------------------------------------------------------------------------------------------
		-- Upate Store object.
		------------------------------------------------------------------------------------------------------------------------
		UPDATE TOP(1) trgt
		SET
			trgt.SourceStoreID = COALESCE(@SourceStoreID, trgt.SourceStoreID,
				CASE
					WHEN (SELECT MIN(SourceStoreID) FROM dbo.Store) < 0 THEN (SELECT MIN(SourceStoreID) FROM dbo.Store) + -1
					-- Create initial seed for unknown StoreStoreIDs
					ELSE -1000
				END
			),
			trgt.Name = ISNULL(@StoreName, trgt.Name),
			trgt.TimeZoneID = COALESCE(@TimeZoneID, trgt.TimeZoneID),
			trgt.SupportDST = ISNULL(@SupportDST, trgt.SupportDST),
			trgt.Demographics = ISNULL(@Demographics, trgt.Demographics),
			trgt.Website = ISNULL(@Website, trgt.Website),
			trgt.DateModified = GETDATE(),
			trgt.ModifiedBy = ISNULL(@ModifiedBy, trgt.ModifiedBy)
		OUTPUT inserted.BusinessEntityID, inserted.SourceStoreID INTO @tblOutput (BusinessEntityID, SourceSystemKey)
		FROM dbo.Store trgt
		WHERE trgt.BusinessEntityID = @BusinessEntityID
			AND (
				( @SourceStoreID IS NOT NULL AND ISNULL(trgt.SourceStoreID, 0) <> ISNULL(@SourceStoreID, 0) )
				OR ISNULL(trgt.Name, '') <> ISNULL(@StoreName, '')
				OR (@TimeZoneID IS NOT NULL AND trgt.TimeZoneID <> @TimeZoneID )
				OR ( @SupportDST IS NOT NULL AND ISNULL(trgt.SupportDST, 0) <> @SupportDST )
				OR ISNULL(CONVERT(VARCHAR(MAX), trgt.Demographics), '') <> ISNULL(CONVERT(VARCHAR(MAX), @Demographics), '')
				OR ISNULL(trgt.Website, '') <> ISNULL(@Website, '')
			)	

		------------------------------------------------------------------------------------------------------------------------
		-- If an object was discovered then perform additional operations.
		------------------------------------------------------------------------------------------------------------------------	
		IF @@ROWCOUNT = 1
		BEGIN
			SET @Exists = 1;

			SET @SourceSystemKey = (SELECT TOP 1 SourceSystemKey FROM @tblOutput);
			SET @BusinessEntityID = (SELECT TOP 1 BusinessEntityID FROM @tblOutput);

			------------------------------------------------------------------------------------------------------------------------
			-- Upate Business object.
			------------------------------------------------------------------------------------------------------------------------
			UPDATE TOP(1) trgt
			SET
				trgt.Name = @StoreName,
				trgt.SourceSystemKey = @SourceSystemKey,
				trgt.DateModified = GETDATE(),
				trgt.ModifiedBy = @ModifiedBy
			FROM dbo.Business trgt
			WHERE trgt.BusinessEntityID = @BusinessEntityID
				AND (
					ISNULL(trgt.Name, '') <> ISNULL(@StoreName, '')
					OR ISNULL(trgt.SourceSystemKey, '') <> ISNULL(@SourceSystemKey, '')
				) 
		END


		------------------------------------------------------------------------------------------------------------------------
		-- Create new BusinessEntityProvider object (if applicable).
		------------------------------------------------------------------------------------------------------------------------
		IF @ProviderID IS NOT NULL
		BEGIN
			EXEC dbo.spBusinessEntityProvider_Merge
				@BusinessEntityID = @BusinessEntityID,
				@ProviderID = @ProviderID,
				@ProviderCode = @ProviderCode,
				@Username = @ProviderUsername,
				@PasswordUnencrypted = @ProviderPasswordUnencrypted,
				@PasswordEncrypted = @ProviderPasswordEncrypted,
				@CreatedBy = @CreatedBy,
				@ModifiedBy = @CreatedBy,
				@Debug = @Debug		
		END		
		
		------------------------------------------------------------------------------------------------------------------------
		-- Create new Address & Store Address object (if applicable).
		------------------------------------------------------------------------------------------------------------------------	
		-- Create a new address entry (if applicable)
		EXEC spAddress_Create 
			@AddressLine1 = @AddressLine1,
			@AddressLine2 = @AddressLine2,
			@City = @City,
			@State = @State,
			@PostalCode = @PostalCode,
			@CreatedBy = @CreatedBy,
			@AddressID = @AddressID OUTPUT
		
		-- Get the address ID for home address
		DECLARE @AddressTypeID_Work INT = dbo.fnGetAddressTypeID('PRIM');
		
		-- Create new BusinessEntityAddress (store address) object.
		EXEC spBusinessEntity_Address_Merge
			@BusinessEntityID = @BusinessEntityID,
			@AddressTypeID = @AddressTypeID_Work,
			@AddressID = @AddressID,
			@CreatedBy = @CreatedBy
			
		------------------------------------------------------------------------------------------------------------------------
		-- Create new store email.
		------------------------------------------------------------------------------------------------------------------------
		DECLARE 
			@EmailAddressTypeID_Primary INT = dbo.fnGetEmailAddressTypeID('PRIM'),
			@EmailAddressTypeID_Alternate INT = dbo.fnGetEmailAddressTypeID('ALT')
		
		-- Create primary email address
		EXEC spBusinessEntity_Email_Merge
			@BusinessEntityID = @BusinessEntityID,
			@EmailAddressTypeID = @EmailAddressTypeID_Primary,
			@EmailAddress = @EmailAddress_Primary,
			@CreatedBy = @CreatedBy  


		-- Create alternate email address
		EXEC spBusinessEntity_Email_Merge
			@BusinessEntityID = @BusinessEntityID,
			@EmailAddressTypeID = @EmailAddressTypeID_Alternate,
			@EmailAddress = @EmailAddress_Alternate,
			@CreatedBy = @CreatedBy
		
		------------------------------------------------------------------------------------------------------------------------
		-- Create new store phone number
		------------------------------------------------------------------------------------------------------------------------
		DECLARE
			@PhoneNumberTypeID_Work INT = dbo.fnGetPhoneNumberTypeID('WRK'),
			@PhoneNumberTypeID_Fax INT = dbo.fnGetPhoneNumberTypeID('FAX')
			
		-- Create new work phone
		EXEC spBusinessEntity_Phone_Merge
			@BusinessEntityID = @BusinessEntityID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Work,
			@PhoneNumber = @PhoneNumber_Work,
			@CreatedBy = @CreatedBy
		
		-- Create new fax number
		EXEC spBusinessEntity_Phone_Merge
			@BusinessEntityID = @BusinessEntityID,
			@PhoneNumberTypeID = @PhoneNumberTypeID_Fax,
			@PhoneNumber = @PhoneNumber_Fax,
			@CreatedBy = @CreatedBy

	------------------------------------------------------------------------------------------------------------------------
	-- End data transaction.
	------------------------------------------------------------------------------------------------------------------------	
	IF @Trancount = 0 
	BEGIN	
		COMMIT TRANSACTION;
	END	
	END TRY
	BEGIN CATCH
		
		-- Rollback any active or uncommittable transactions before
		-- inserting information in the ErrorLog
		IF @Trancount = 0 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		
		EXECUTE dbo.spLogException
			@ErrorLogID = @ErrorLogID OUTPUT,
			@Arguments = @Args

		SET @ErrorMessage = 'Unable to update Store object. Exeception: ' + ERROR_MESSAGE();
				
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
	
	END CATCH;
	
	
END
