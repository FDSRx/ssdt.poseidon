﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/3/2013
-- Description:	Create a password for a specific service
-- SAMPLE CALL: spPassword_Create 1, 1, 'Password'
-- SELECT * FROM dbo.Person WHERE LastName = 'TESTPERSON'
-- SELECT * FROM dbo.Service
-- SELECT * FROM dbo.Password
-- =============================================
CREATE PROCEDURE spPassword_Create
	@PersonID INT,
	@ServiceID INT,
	@Password VARCHAR(128)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    
    ---------------------------------------------------------
    -- Local variables
    ---------------------------------------------------------
    DECLARE 
		@Salt VARBINARY(10),
		@Hash VARBINARY(128)
	
	
	---------------------------------------------------------
	-- Salt and Hash clear text
	---------------------------------------------------------
	SET @Salt = dbo.fnPasswordSalt(RAND());
	SET @Hash = dbo.fnPasswordHash(@Password + CONVERT(VARCHAR(MAX), @Salt));
	
	
	--PRINT @Hash
	---------------------------------------------------------
	-- Create new service password
	---------------------------------------------------------
	INSERT INTO dbo.Password(
		PersonID,
		ServiceID,
		PasswordHash,
		PasswordSalt
	)
	SELECT
		@PersonID,
		@ServiceID,
		@Hash,
		@Salt
		
    

END
