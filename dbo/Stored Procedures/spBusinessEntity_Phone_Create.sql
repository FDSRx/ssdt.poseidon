﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/25/2013
-- Description: Create BusinessEntityPhoneNumber object.
-- SAMPLE CALL:
/*
	-- Work phone.
	EXEC dbo.spBusinessEntity_Phone_Create
		@BusinessEntityID = 10684,
		@PhoneNumberTypeID = 3,
		@PhoneNumber = '8178675309',
		@CreatedBy = 'dhughes'

	-- Fax phone.
	EXEC dbo.spBusinessEntity_Phone_Create
		@BusinessEntityID = 10684,
		@PhoneNumberTypeID = 4,
		@PhoneNumber = '8175369663',
		@CreatedBy = 'dhughes'
		
*/

-- SELECT * FROM dbo.BusinessEntityPhone WHERE BusinessEntityID = 10684
-- SELECT * FROM dbo.PhoneNumberType
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntity_Phone_Create]
	@BusinessEntityID BIGINT = NULL,
	@PhoneNumberTypeID INT = NULL,
	@PhoneNumber VARCHAR(25) = NULL,
	@IsCallAllowed BIT = 0,
	@IsTextAllowed BIT = 0,
	@IsPreferred BIT = 0,
	@CreatedBy VARCHAR(256) = NULL,
	@BusinessEntityPhoneNumberID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-------------------------------------------------
	-- Clean data
	-------------------------------------------------
	SET @PhoneNumber = dbo.fnRemoveNonNumericChars(@PhoneNumber);
	SET @IsCallAllowed = ISNULL(@IsCallAllowed, 0);
	SET @IsTextAllowed = ISNULL(@IsTextAllowed, 0);
	SET @IsPreferred = ISNULL(@IsPreferred, 0);

	--------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------
	DECLARE @HashKey VARCHAR(256)

	--------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------	
	-- Create hash key.
	SET @HashKey = dbo.fnCreateRecordHashKey( 
		dbo.fnToHashableString(@BusinessEntityID) + 
		dbo.fnToHashableString(@PhoneNumberTypeID) +
		dbo.fnToHashableString(@PhoneNumber) +
		dbo.fnToHashableString(@IsCallAllowed) +
		dbo.fnToHashableString(@IsTextAllowed) +
		dbo.fnToHashableString(@IsPreferred)
	);	
	
    -- Check to see if the phone number does not already exist and ensure the phone number is valid. 
	IF @BusinessEntityID IS NOT NULL AND @PhoneNumberTypeID IS NOT NULL AND @PhoneNumber IS NOT NULL 
		AND (SELECT COUNT(*) FROM BusinessEntityPhone WHERE BusinessEntityID = @BusinessEntityID AND PhoneNumberTypeID = @PhoneNumberTypeID) = 0
		AND dbo.fnIsPhoneValid(@PhoneNumber) > 0
	BEGIN
		INSERT INTO dbo.BusinessEntityPhone (
			BusinessEntityID,
			PhoneNumberTypeID,
			PhoneNumber,
			IsCallAllowed,
			IsTextAllowed,
			IsPreferred,
			HashKey,
			CreatedBy
		)
		SELECT
			@BusinessEntityID,
			@PhoneNumberTypeID,
			@PhoneNumber,
			@IsCallAllowed,
			@IsTextAllowed,
			@IsPreferred,
			@HashKey,
			@CreatedBy
		
		SET @BusinessEntityPhoneNumberID = SCOPE_IDENTITY();
	END
	
END
