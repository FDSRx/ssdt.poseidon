﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/20/2015
-- Description:	Updates a BusinessEntityPreference object.
-- SAMPLE CALL;
/*

EXEC dbo.spBusinessEntityPreference_Update
	@BusinessEntityID = 135590,
	@PreferenceID = 1,
	@ValueBoolean = 0

*/

-- SELECT * FROM dbo.BusinessEntityPreference
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityPreference_Update]
	@BusinessEntityPreferenceID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL,
	@PreferenceID INT = NULL,
	@ValueString VARCHAR(MAX) = NULL,
	@ValueBoolean BIT = NULL,
	@ValueNumeric DECIMAL(19, 8) = NULL,
	@ModifiedBy VARCHAR(256) = NULL	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000) = NULL
	
	-----------------------------------------------------------------------------------------------
	-- Argument validation.
	-- <Summary>
	-- Validates critical arguments.
	-- </Summary>
	-----------------------------------------------------------------------------------------------		
	
	--@BusinessEntityPreferenceID
	IF @BusinessEntityPreferenceID IS NULL AND ( @BusinessEntityID IS NULL OR @PreferenceID IS NULL )
	BEGIN
			
		SET @ErrorMessage = 'Unable to create record. Object references ' + 
			'(@BusinessEntityPreferenceID or @BusinessEntityID and @PreferenceID) are not set to an instance ' +
			'of an object.'
			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);
		
		RETURN;		
			
	END		
	
	
	-----------------------------------------------------------------------------------------------
	-- Create BusinessEntityPreference object.
	-----------------------------------------------------------------------------------------------	
	-- The record identity has primary search priority.
	IF @BusinessEntityPreferenceID IS NOT NULL
	BEGIN
		SET @BusinessEntityID = NULL;
		SET @PreferenceID = NULL;
	END
	
	DECLARE @tblOutput AS TABLE (
		BusinessEntityPreferenceID BIGINT
	);
	
	UPDATE bp
	SET
		ValueString = @ValueString,
		ValueBoolean = @ValueBoolean,
		ValueNumeric = @ValueNumeric,
		DateModified = GETDATE(),
		ModifiedBy = @ModifiedBy
	OUTPUT inserted.BusinessEntityPreferenceID INTO @tblOutput(BusinessEntityPreferenceID)
	FROM dbo.BusinessEntityPreference bp
	WHERE BusinessEntityPreferenceID = @BusinessEntityPreferenceID
		OR ( 
			BusinessEntityID = @BusinessEntityID
			AND PreferenceID = @PreferenceID 
		)

	-- Retrieve record identity.
	SET @BusinessEntityPreferenceID = (SELECT BusinessEntityPreferenceID FROM @tblOutput);
	
END
