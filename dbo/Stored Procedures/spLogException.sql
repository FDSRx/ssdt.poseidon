﻿-- spLogException logs error information in the ErrorLog table about the 
-- error that caused execution to jump to the CATCH block of a 
-- TRY...CATCH construct. This should be executed from within the scope 
-- of a CATCH block otherwise it will return without inserting error 
-- information. 
CREATE PROCEDURE [dbo].[spLogException]
	@ErrorNumber INT = NULL,
	@ErrorSeverity INT = NULL,
	@ErrorState INT = NULL,
	@UserName NVARCHAR(128) = NULL,
	@ErrorProcedure NVARCHAR(126) = NULL,
	@ErrorLine INT = NULL,
	@ErrorMessage NVARCHAR(4000) = NULL,
	@Exception NVARCHAR(4000) = NULL,
	@InnerException NVARCHAR(4000) = NULL,
	@Arguments VARCHAR(MAX) = NULL,
    @ErrorLogID [int] = 0 OUTPUT -- contains the ErrorLogID of the row inserted
AS                               -- by uspLogError in the ErrorLog table
BEGIN
    SET NOCOUNT ON;

    -- Output parameter value of 0 indicates that error 
    -- information was not logged
    SET @ErrorLogID = 0;

    BEGIN TRY
        -- Return if there is no error information to log
        --IF ERROR_NUMBER() IS NULL
        --    RETURN;

        -- Return if inside an uncommittable transaction.
        -- Data insertion/modification is not allowed when 
        -- a transaction is in an uncommittable state.
        IF XACT_STATE() = -1
        BEGIN
            PRINT 'Cannot log error since the current transaction is in an uncommittable state. ' 
                + 'Rollback the transaction before executing spLogException in order to successfully log error information.';
            RETURN;
        END
        
        SET @ErrorNumber = COALESCE(@ErrorNumber, ERROR_NUMBER(), 5000);
		SET @ErrorSeverity = ISNULL(@ErrorSeverity, ERROR_SEVERITY());
		SET @ErrorState = ISNULL(@ErrorState, ERROR_STATE());
		SET @UserName = ISNULL(@UserName, CONVERT(sysname, CURRENT_USER));
		SET @ErrorProcedure = ISNULL(@ErrorProcedure,  ERROR_PROCEDURE());
		SET @ErrorLine = ISNULL(@ErrorLine, ERROR_LINE());
		SET @ErrorMessage = ISNULL(@ErrorMessage, ERROR_MESSAGE()); 

        INSERT [dbo].[ErrorLog] 
            (
            [UserName], 
            [ErrorNumber], 
            [ErrorSeverity], 
            [ErrorState], 
            [ErrorProcedure], 
            [ErrorLine], 
            [ErrorMessage],
            [Exception],
            [InnerException],
            [Arguments]
            ) 
        VALUES 
            (
            @UserName, 
            @ErrorNumber,
            @ErrorSeverity,
            @ErrorState,
            @ErrorProcedure,
            @ErrorLine,
            @ErrorMessage,
            @Exception,
            @InnerException,
            @Arguments
            );

        -- Pass back the ErrorLogID of the row inserted
        SET @ErrorLogID = SCOPE_IDENTITY();
        
    END TRY
    BEGIN CATCH
    
        PRINT 'An error occurred in stored procedure spLogException: ';
        
        EXECUTE [dbo].[spPrintError];
        
        RETURN -1;
        
    END CATCH
END;
