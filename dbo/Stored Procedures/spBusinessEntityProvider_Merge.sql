﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/22/2016
-- Description:	Merges (creates/updates) a BusienssEntityServiceProvider object.
-- SAMPLE CALL:
/*

DECLARE
	@BusinessEntityProviderID BIGINT = 20,
	@BusinessEntityID BIGINT = NULL,
	@ProviderID INT = NULL,
	@ProviderCode VARCHAR(25) = NULL,
	@Username VARCHAR(256) = '14723',
	@PasswordUnencrypted VARChAR(256) = NULL,
	@PasswordEncrypted NVARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@Debug BIT = 1

EXEC dbo.spBusinessEntityProvider_Merge
	@BusinessEntityProviderID = @BusinessEntityProviderID OUTPUT,
	@BusinessEntityID = @BusinessEntityID,
	@ProviderID = @ProviderID,
	@ProviderCode = @ProviderCode,
	@Username = @Username,
	@PasswordUnencrypted = @PasswordUnencrypted,
	@PasswordEncrypted = @PasswordEncrypted,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

SELECT @BusinessEntityProviderID AS BusinessEntityProviderID, @BusinessEntityID AS BusinessEntityID, @ProviderID AS ProviderID

*/

-- SELECT * FROM dbo.BusinessEntityProvider
-- SELECT * FROM dbo.vwBusinessEntityProvider
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessEntityProvider_Merge]
	@BusinessEntityProviderID BIGINT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@ProviderID INT = NULL OUTPUT,
	@ProviderCode VARCHAR(25) = NULL,
	@Username VARCHAR(256) = NULL,
	@PasswordUnencrypted VARChAR(256) = NULL,
	@PasswordEncrypted NVARCHAR(256) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	----------------------------------------------------------------------------------------------------------
	-- Instance variables.
	----------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	----------------------------------------------------------------------------------------------------------
	-- Log request.
	----------------------------------------------------------------------------------------------------------
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@ProviderID=' + dbo.fnToStringOrEmpty(@ProviderID) + ';' +
		'@ProviderCode=' + dbo.fnToStringOrEmpty(@ProviderCode) + ';' +
		'@Username=' + dbo.fnToStringOrEmpty(@Username) + ';' +
		'@PasswordUnencrypted=' + dbo.fnToStringOrEmpty(@PasswordUnencrypted) + ';' +
		'@PasswordEncrypted=' + dbo.fnToStringOrEmpty(@PasswordEncrypted) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@PasswordEncrypted) + ';' +
		'@BusinessEntityProviderID=' + dbo.fnToStringOrEmpty(@BusinessEntityProviderID) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	----------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	----------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
	SET @CreatedBy = COALESCE(@CreatedBy, @ModifiedBy, SUSER_NAME());
	SET @ModifiedBy = COALESCE(@ModifiedBy, @CreatedBy, SUSER_NAME());

	----------------------------------------------------------------------------------------------------------
	-- Set variables.
	----------------------------------------------------------------------------------------------------------
	-- ProviderID
	SET @ProviderID = ISNULL(dbo.fnGetProviderID(@ProviderID), dbo.fnGetProviderID(@ProviderCode));

	-- Debug
	IF @Debug = 1
	BEGIN 
		SELECT 'DebuggerOn' AS DebugMode, @ProcedureName AS procedureName, 'Getter/Setter methods.' AS ActionMethod,
			@BusinessEntityProviderID AS BusinessEntityProviderID, @BusinessEntityID AS BusinessEntityID,
			@ProviderID AS ProviderID, @ProviderCode AS ProviderCode,
			@Username AS Username, @PasswordUnencrypted AS PasswordUnencrypted, @PasswordEncrypted AS PasswordEncrypted
	END



	----------------------------------------------------------------------------------------------------------
	-- Determine object existance.
	----------------------------------------------------------------------------------------------------------
	SET @BusinessEntityProviderID = (
		SELECT TOP 1
			BusinessEntityProviderID
		FROM dbo.BusinessEntityProvider trgt
		WHERE (@BusinessEntityProviderID IS NOT NULL AND trgt.BusinessEntityProviderID = @BusinessEntityProviderID)
			OR (@BusinessEntityProviderID IS NULL AND (trgt.BusinessEntityID = @BusinessEntityID AND trgt.ProviderID = @ProviderID))
	);

	-- Debug
	IF @Debug = 1
	BEGIN 
		SELECT 'DebuggerOn' AS DebugMode, @ProcedureName AS procedureName, 'BusinessEntityProviderID Lookup.' AS ActionMethod,
			@BusinessEntityProviderID AS BusinessEntityProviderID
	END

	----------------------------------------------------------------------------------------------------------
	-- Merge object.
	----------------------------------------------------------------------------------------------------------	
	IF @BusinessEntityProviderID IS NULL
	BEGIN
		-- Create a new object.
		EXEC dbo.spBusinessEntityProvider_Create
			@BusinessEntityID = @BusinessEntityID,
			@ProviderID = @ProviderID,
			@ProviderCode = @ProviderCode,
			@Username = @Username,
			@PasswordUnencrypted = @PasswordUnencrypted,
			@PasswordEncrypted = @PasswordEncrypted,
			@CreatedBy = @CreatedBy,
			@BusinessEntityProviderID = @BusinessEntityProviderID OUTPUT
	END
	ELSE
	BEGIN
		-- Update an existing object.
		EXEC dbo.spBusinessEntityProvider_Update
			@BusinessEntityProviderID = @BusinessEntityProviderID,
			@BusinessEntityID = @BusinessEntityID,
			@ProviderID = @ProviderID,
			@ProviderCode = @ProviderCode,
			@Username = @Username,
			@PasswordUnencrypted = @PasswordUnencrypted,
			@PasswordEncrypted = @PasswordEncrypted,
			@ModifiedBy = @ModifiedBy,
			@Debug = @Debug			
	END

END
