﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/23/2014
-- Description:	Updates a Note object.
-- Change Log:
-- 11/9/2015 - dhughes - Modified the procedure to receive a list of Note objects vs. a single object so that it is able
--					to perform batch updates (when applicable).
-- 11/13/15 - dhughes/ssimmons - Modified the update to have better speed and performance. Moved the parsing on the notes into its
--								own object for reuse and speed.
-- 2/29/2016 - dhughes - Modified the procedure to include additional inputs.

-- SAMPLE CALL:
/*

DECLARE 
	@NoteID VARCHAR(MAX) = 21094,
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(50) = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = '<div>MedSync fill for <a href="javascript:urlRedirect(''MED_SYNC'',30466 )">VIRGINIA SIMS</a> on 01-07-2015</div><div>CARB/LEVO    TAB 25-250MG (2/6.00).</div>',
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@Debug BIT = 1

	
EXEC dbo.spNote_Update
	@NoteID = @NoteID,
	@ApplicationID = @ApplicationID,
	@ApplicationCode = @ApplicationCode,
	@BusinessID = @BusinessID,
	@BusinessKey = @BusinessKey,
	@BusinessKeyType = @BusinessKeyType,
	@BusinessEntityID = @BusinessEntityID,
	@ScopeID = @ScopeID,
	@ScopeCode = @ScopeCode,
	@NoteTypeID = @NoteTypeID,
	@NoteTypeCode = @NoteTypeCode,
	@NotebookID = @NotebookID,
	@NotebookCode = @NotebookCode,
	@Title = @Title,
	@Memo = @Memo,
	@PriorityID = @PriorityID,
	@PriorityCode = @PriorityCode,
	@Tags = @Tags,
	@OriginID = @OriginID,
	@OriginCode = @OriginCode,
	@OriginDataKey = @OriginDataKey,
	@AdditionalData = @AdditionalData,
	@CorrelationKey = @CorrelationKey,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/


-- SELECT * FROM dbo.Note
-- SELECT * FROM dbo.Scope
-- SELECT * FROM dbo.NoteType
-- SELECT * FROM dbo.NoteBook
-- SELECT * FROM dbo.Note WHERE NoteID = 21094
-- =============================================
CREATE PROCEDURE [dbo].[spNote_Update]
	@NoteID VARCHAR(MAX),
	@ApplicationID INT = NULL,
	@ApplicationCode VARCHAR(50) = NULL,
	@BusinessID BIGINT = NULL,
	@BusinessKey VARCHAR(50) = NULL,
	@BusinessKeyType VARCHAR(50) = NULL,
	@BusinessEntityID BIGINT = NULL,
	@ScopeID INT = NULL,
	@ScopeCode VARCHAR(50) = NULL,
	@NoteTypeID INT = NULL,
	@NoteTypeCode VARCHAR(50) = NULL,
	@NotebookID INT = NULL,
	@NotebookCode VARCHAR(50) = NULL,
	@Title VARCHAR(1000) = NULL,
	@Memo VARCHAR(MAX) = NULL,
	@PriorityID INT = NULL,
	@PriorityCode VARCHAR(50) = NULL,
	@Tags VARCHAR(MAX) = NULL,
	@OriginID INT = NULL,
	@OriginCode VARCHAR(50) = NULL,
	@OriginDataKey VARCHAR(256) = NULL,
	@AdditionalData XML = NULL,
	@CorrelationKey VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------------------------------------------------------------------------------------------
	-- Instance variables
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL

	DECLARE @Args VARCHAR(MAX) =
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@ApplicationCode=' + dbo.fnToStringOrEmpty(@ApplicationCode) + ';' +
		'@BusinessID=' + dbo.fnToStringOrEmpty(@BusinessID) + ';' +
		'@BusinessKey=' + dbo.fnToStringOrEmpty(@BusinessKey) + ';' +
		'@BusinessKeyType=' + dbo.fnToStringOrEmpty(@BusinessKeyType) + ';' +
		'@ScopeID=' + dbo.fnToStringOrEmpty(@ScopeID) + ';' +
		'@ScopeCode=' + dbo.fnToStringOrEmpty(@ScopeCode) + ';' +
		'@NotebookID=' + dbo.fnToStringOrEmpty(@NotebookID) + ';' +
		'@NotebookCode=' + dbo.fnToStringOrEmpty(@NotebookCode) + ';' +
		'@Title=' + dbo.fnToStringOrEmpty(@Title) + ';' +
		'@Memo=' + dbo.fnToStringOrEmpty(@Memo) + ';' +
		'@PriorityID=' + dbo.fnToStringOrEmpty(@PriorityID) + ';' +
		'@PriorityCode=' + dbo.fnToStringOrEmpty(@PriorityCode) + ';' +
		'@Tags=' + dbo.fnToStringOrEmpty(@Tags) + ';' +
		'@CorrelationKey=' + dbo.fnToStringOrEmpty(@CorrelationKey) + ';' +
		'@AdditionalData=' + dbo.fnToStringOrEmpty(CONVERT(VARCHAR(MAX), @AdditionalData)) + ';' +
		'@OriginID=' + dbo.fnToStringOrEmpty(@OriginID) + ';' +
		'@OriginCode=' + dbo.fnToStringOrEmpty(@OriginCode) + ';' +
		'@OriginDataKey=' + dbo.fnToStringOrEmpty(@OriginDataKey) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	------------------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	------------------------------------------------------------------------------------------------------------------------
	DECLARE @tblNotes AS TABLE (
		Idx INT IDENTITY(1,1),
		NoteID BIGINT
	);

	------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------------------
	SET @ScopeID = ISNULL(dbo.fnGetScopeID(@ScopeID), dbo.fnGetScopeID(@ScopeCode));
	SET @NoteBookID = ISNULL(dbo.fnGetNotebookID(@NoteBookID), dbo.fnGetNotebookID(@NoteBookCode));
	SET @PriorityID = ISNULL(dbo.fnGetPriorityID(@PriorityID), dbo.fnGetPriorityID(@PriorityCode));
	SET @OriginID = ISNULL(dbo.fnGetOriginID(@OriginID), dbo.fnGetOriginID(@OriginCode));
	SET @NoteTypeID = ISNULL(dbo.fnGetNoteTypeID(@NoteTypeID), dbo.fnGetNoteTypeID(@NoteTypeCode));

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@ScopeID AS ScopeID, @ScopeCode AS ScopeCode, @NotebookID AS NotebookID, @NotebookCode AS NotebookCode, @Title AS Title, 
			@Memo AS Memo, @PriorityID AS PriorityID, @PriorityCode AS PriorityCode, @Tags AS Tags, @CorrelationKey AS CorrelationKey, 
			@AdditionalData AS AdditionalData, @OriginID AS OriginID, @OriginCode AS OriginCode, 
			@OriginDataKey AS OriginDataKey, @BusinessEntityID AS BusinessEntityID, @Args AS Arguments
	END	

	------------------------------------------------------------------------------------------------------------------------
	-- Parse notes.
	------------------------------------------------------------------------------------------------------------------------
	INSERT INTO @tblNotes (
		NoteID
	)
	SELECT 
		Value 
	FROM dbo.fnSplit(@NoteID, ',') 
	WHERE ISNUMERIC(Value) = 1


	------------------------------------------------------------------------------------------------------------------------
	-- Update Note object.
	-- <Remarks>
	-- If the values supplied for string objects are null then they will not be used in the update.
	-- </Remarks>
	------------------------------------------------------------------------------------------------------------------------
	UPDATE n
	SET 
		n.NoteBookID = CASE WHEN @NoteBookID IS NULL THEN n.NotebookID ELSE @NoteBookID END,
		n.Title = CASE WHEN @Title IS NULL THEN n.Title ELSE @Title END,
		n.Memo = CASE WHEN @Memo IS NULL THEN n.Memo ELSE @Memo END,
		n.PriorityID = CASE WHEN @PriorityID IS NULL THEN n.PriorityID ELSE @PriorityID END,
		n.OriginID = CASE WHEN @OriginID IS NULL THEN n.OriginID ELSE @OriginID END,
		n.OriginDataKey = CASE WHEN @OriginDataKey IS NULL THEN n.OriginDataKey ELSE @OriginDataKey END,
		n.AdditionalData = CASE WHEN @AdditionalData IS NULL THEN n.AdditionalData ELSE @AdditionalData END,
		n.CorrelationKey = CASE WHEN @CorrelationKey IS NULL THEN n.CorrelationKey ELSE @CorrelationKey END,
		n.ModifiedBy = CASE WHEN @ModifiedBy IS NULL THEN n.ModifiedBy ELSE @ModifiedBy END,
		n.DateModified = GETDATE()
	--SELECT *
	FROM @tblNotes tmp
		JOIN dbo.Note n
			ON tmp.NoteID = n.NoteID

	-- 11/13/2015 - dhughes - Deprecated code. Removed to help increase performance.  
	--WHERE ( @BusinessEntityID IS NULL AND n.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteID, ',') WHERE ISNUMERIC(Value) = 1) )
	--		OR ( @BusinessEntityID IS NOT NULL AND ( n.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteID, ',') WHERE ISNUMERIC(Value) = 1)  
	--			AND n.BusinessEntityID = @BusinessEntityID ) )
		
		
END
