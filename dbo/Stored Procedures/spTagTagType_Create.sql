﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/24/2014
-- Description:	Creates a new Tag/TagType combination
-- SAMPLE CALL:
/*
DECLARE @TagTagTypeID BIGINT

EXEC dbo.spTagTagType_Create
	@TagID = 11,
	@TagTypeID = 2,
	@CreatedBy = 'dhughes',
	@TagTagTypeID = @TagTagTypeID OUTPUT

SELECT @TagTagTypeID AS TagTagTypeID
*/
-- SELECT * FROM dbo.TagTagType
-- SELECT * FROM dbo.Tag
-- SELECT * FROM dbo.TagType
-- =============================================
CREATE PROCEDURE [dbo].[spTagTagType_Create]
	@TagID BIGINT,
	@TagTypeID INT,
	@CreatedBy VARCHAR(256) = NULL,
	@TagTagTypeID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @TagTagTypeID = NULL;

	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)
	
	----------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	----------------------------------------------------------------------
	-- A Note object and Tag object is required to create a NoteTag object.
	IF @TagID IS NULL OR @TagTypeID IS NULL
	BEGIN
	
		SET @ErrorMessage = 'Unable to create a TagTagType object.  Object references (@TagID or @TagTypeID) are not set to an instance of an object. ' +
			'The @TagID and @TagTypeID cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
		
	----------------------------------------------------------------------
	-- Create Tag/TagType object.
	----------------------------------------------------------------------
	INSERT INTO dbo.TagTagType (
		TagID,
		TagTypeID,
		CreatedBy
	)
	SELECT
		@TagID AS TagID,
		@TagTypeID AS TagTypeID,
		@CreatedBy AS CreatedBy
	
	-- Retrieve record identity
	SET @TagTagTypeID = SCOPE_IDENTITY();
	
	
	
	
	
	
END
