﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/15/2014
-- Description:	Indicates whether the provided Tag/TagType reference exists.
-- SAMPLE CALL:
/*
DECLARE 
	@TagTagTypeID BIGINT = NULL,
	@Exists BIT
	

EXEC dbo.spTagTagType_Exists
	@TagTagTypeID = @TagTagTypeID OUTPUT,
	@TagID = 10,
	@TagTypeID = 2,
	@Exists = @Exists OUTPUT

SELECT @TagTagTypeID AS TagTagTypeID, @Exists AS IsFound
*/
-- SELECT * FROM dbo.TagTagType
-- =============================================
CREATE PROCEDURE [dbo].[spTagTagType_Exists]
	@TagTagTypeID BIGINT = NULL OUTPUT,
	@TagTypeID BIGINT = NULL,
	@TagID BIGINT = NULL,
	@TagName VARCHAR(500) = NULL,
	@Exists BIT = NULL OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	----------------------------------------------------------------------
	-- Sanitize input
	----------------------------------------------------------------------
	SET @Exists = 0;
	
	----------------------------------------------------------------------
	-- Local variables
	----------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000) = NULL,
		@ValidateRowKey BIT = 1
	
	----------------------------------------------------------------------
	-- Interogate arguments and determine validation.
	-- <Summary>
	-- Inspects the argument collection and determines which arguments
	-- should be used to validate and used for the lookup.
	-- </Summary>
	----------------------------------------------------------------------
	-- @TagTagTypeID will be used if provided.
	IF @TagTagTypeID IS NOT NULL
	BEGIN
		SET @TagID = NULL;
		SET @TagTypeID = NULL;
		SET @TagName = NULL;
		
		SET @ValidateRowKey = 0;
	END
	
	----------------------------------------------------------------------
	-- Argument validation
	-- <Summary>
	-- Validates incoming arguments and ensures they adhere
	-- to the dedicated business rules
	-- </Summary>
	----------------------------------------------------------------------	
	IF @TagTagTypeID IS NULL
	BEGIN
	
		-- A Tag object and TagType object are required to create a TagTagType object.
		EXEC dbo.spTag_Exists
			@TagID = @TagID OUTPUT,
			@Name = @TagName
			
	END
		
	IF @TagID IS NULL AND @TagTypeID IS NULL AND @ValidateRowKey = 1
	BEGIN
	
		SET @ErrorMessage = 'Unable to verify TagTagType object existence.  Object references (@TagTypeID and ( @TagID or @TagName )) are not set to an instance of an object. ' +
			'The @TagTypeID and ( @TagID or @TagName ) cannot be null.';
			
		RAISERROR (@ErrorMessage, -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		
		RETURN;
	END
	
	--------------------------------------------------------------
	-- Determine if tag exists
	--------------------------------------------------------------
	SET @TagTagTypeID = (
		SELECT TOP 1
			TagTagTypeID
		FROM dbo.TagTagType
		WHERE (
			TagTagTypeID = @TagTagTypeID OR 
			( TagID = @TagID AND TagTypeID = @TagTypeID) 
		)
	);
	
	-- Set the exists flag based on the lookup.
	SET @Exists = CASE WHEN @TagTagTypeID IS NOT NULL THEN 1 ELSE 0 END;
	
END
