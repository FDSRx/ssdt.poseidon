﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/19/2013
-- Description:	Creates Address
-- 9/19/2014 - DRH - Broke out/upgraded the address validation/lookup logic into its own stored procedure
--		for future reuse.

/*

DECLARE @AddressID BIGINT

EXEC dbo.spAddress_Create
	@AddressLine1 = '8850 Stanford Blvd.',
	@City = 'Columbia',
	@State = 'MD',
	@PostalCode = '21045',
	@CreatedBy = 'dhughes',
	@AddressID = @AddressID OUTPUT

SELECT @AddressID AS AddressID

*/

-- SELECT TOP 10 * FROM dbo.Address ORDER BY AddressID DESC
-- SELECT TOP 10 * FROM dbo.Address ORDER BY AddressID ASC
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- =============================================
CREATE PROCEDURE [dbo].[spAddress_Create]
	@AddressLine1 VARCHAR(100) = NULL,
	@AddressLine2 VARCHAR(100) = NULL,
	@City VARCHAR(50) = NULL,
	@State VARCHAR(10) = NULL,
	@PostalCode VARCHAR(25) = NULL,
	@SpatialLocation GEOGRAPHY = NULL,
	@Latitude DECIMAL(9,6) = NULL,
	@Longitude DECIMAL(9,6) = NULL,
	@CreatedBy	VARCHAR(256) = NULL,
	@AddressID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT * FROM Address
	
	-----------------------------------------------------------------
	-- Local variables
	-----------------------------------------------------------------
	DECLARE
		@StateProvinceID INT,
		@HashKey VARCHAR(256)
		
	-----------------------------------------------------------------
	-- Set variables
	-----------------------------------------------------------------
	SELECT @StateProvinceID = StateProvinceID FROM StateProvince WHERE StateProvinceCode = @State;
	SET @AddressID = NULL;
	
	-- Create hash key.
	SET @HashKey = dbo.fnCreateRecordHashKey( 
		dbo.fnToHashableString(@AddressLine1) + 
		dbo.fnToHashableString(@AddressLine2) +
		dbo.fnToHashableString(@City) +
		dbo.fnToHashableString(@StateProvinceID) +
		dbo.fnToHashableString(@PostalCode)
	);
	
	-----------------------------------------------------------------
	-- Clean variables
	-----------------------------------------------------------------
	SET @AddressLine1 = CASE WHEN LTRIM(RTRIM(ISNULL(@AddressLine1, ''))) = '' THEN NULL ELSE LTRIM(RTRIM(@AddressLine1)) END;
	SET @AddressLine2 = CASE WHEN LTRIM(RTRIM(ISNULL(@AddressLine2, ''))) = '' THEN NULL ELSE LTRIM(RTRIM(@AddressLine2)) END;
	SET @City = CASE WHEN LTRIM(RTRIM(ISNULL(@City, ''))) = '' THEN NULL ELSE LTRIM(RTRIM(@City)) END;
	SET @State = CASE WHEN LTRIM(RTRIM(ISNULL(@State, ''))) = '' THEN NULL ELSE LTRIM(RTRIM(@State)) END;
	SET @PostalCode = CASE WHEN LTRIM(RTRIM(ISNULL(@PostalCode, ''))) = '' THEN NULL ELSE LTRIM(RTRIM(@PostalCode)) END;
	
	
	
	-----------------------------------------------------------------
	-- Check if an existing address exists
	-----------------------------------------------------------------
	EXEC dbo.spAddress_Exists
		@AddressLine1 = @AddressLine1,
		@AddressLine2 = @AddressLine2,
		@City = @City,
		@StateProvinceID = @StateProvinceID,
		@PostalCode = @PostalCode,
		@AddressID = @AddressID OUTPUT
		
	/*
	SELECT @AddressID = AddressID
	FROM dbo.Address
	WHERE ISNULL(AddressLine1, '') = ISNULL(@AddressLine1, '')
			AND ISNULL(AddressLine2, '') = ISNULL(@AddressLine2, '')
			AND ISNULL(City, '') = ISNULL(@City, '')
			AND ISNULL(StateProvinceID, 0) = ISNULL(@StateProvinceID, 0)
			AND ISNULL(PostalCode, '') = ISNULL(@PostalCode, '')
	*/
		
			
	-----------------------------------------------------------------
	-- Add address if it does not exist
	-----------------------------------------------------------------	
	IF @AddressID IS NULL 
		--AND (ISNULL(@AddressLine1, '') <> '' AND ISNULL(@City, '') <> '' AND @StateProvinceID IS NOT NULL AND ISNULL(@PostalCode, '') <> '')
	BEGIN
		INSERT INTO dbo.Address (
			AddressLine1,
			AddressLine2,
			City,
			StateProvinceID,
			PostalCode,
			SpatialLocation,
			Latitude,
			Longitude,
			HashKey,
			CreatedBy
		)
		SELECT
			@AddressLine1,
			@AddressLine2,
			@City,
			@StateProvinceID,
			@PostalCode,
			@SpatialLocation,
			@Latitude,
			@Longitude,
			@HashKey,
			@CreatedBy
			
		SET @AddressID = SCOPE_IDENTITY();	

	END
			
			
END
