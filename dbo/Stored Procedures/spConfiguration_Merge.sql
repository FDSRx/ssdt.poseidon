﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/04/2015
-- Description:	Creates/Updates a Configuration object.
-- SAMPLE CALL:

/*

DECLARE
	@ConfigurationID BIGINT = NULL,
	@ApplicationID INT = 10,
	@BusinessEntityID BIGINT = 38,
	@KeyName VARCHAR(256) = 'IsEngageMessagingEnabled',
	@KeyValue VARCHAR(MAX) = 'true',
	@Exists BIT = NULL

EXEC dbo.spConfiguration_Merge
	@ApplicationID = @ApplicationID, -- "ENG" (Engage/Store management tool)
	@BusinessEntityID = @BusinessEntityID,
	@KeyName = @KeyName,
	@KeyValue = @KeyValue,
	@ConfigurationID = @ConfigurationID OUTPUT,
	@Exists = @Exists OUTPUT

SELECT @ConfigurationID AS ConfigurationID, @Exists AS IsFound

*/
-- SELECT * FROM dbo.Configuration
-- =============================================
CREATE PROCEDURE [dbo].[spConfiguration_Merge]
	@ConfigurationID BIGINT = NULL OUTPUT,
	@ApplicationID INT = NULL OUTPUT,
	@BusinessEntityID BIGINT = NULL OUTPUT,
	@KeyName VARCHAR(256) = NULL OUTPUT,
	@KeyValue VARCHAR(MAX) = NULL,
	@DataTypeID INT= NULL OUTPUT,
	@DataSize INT = NULL OUTPUT,
	@Description VARCHAR(1000) = NULL OUTPUT,
	@CreatedBy VARCHAR(256) = NULL OUTPUT,
	@ModifiedBy VARCHAR(256) = NULL OUTPUT,
	@Exists BIT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-----------------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE	
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	-- Log incoming arguments
	DECLARE @Args VARCHAR(MAX) =
		'@ConfigurationID=' + dbo.fnToStringOrEmpty(@ConfigurationID) + ';' +
		'@ApplicationID=' + dbo.fnToStringOrEmpty(@ApplicationID) + ';' +
		'@BusinessEntityID=' + dbo.fnToStringOrEmpty(@BusinessEntityID) + ';' +
		'@KeyName=' + dbo.fnToStringOrEmpty(@KeyName) + ';' +
		'@KeyValue=' + dbo.fnToStringOrEmpty(@KeyValue) + ';' +
		'@DataTypeID=' + dbo.fnToStringOrEmpty(@DataTypeID) + ';' +
		'@DataSize=' + dbo.fnToStringOrEmpty(@DataSize) + ';' +
		'@Description=' + dbo.fnToStringOrEmpty(@Description) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' 
		
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	-----------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-----------------------------------------------------------------------------------------------------------------------
	SET @Exists = ISNULL(@Exists, 0);

	-----------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-----------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)		

	-----------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-----------------------------------------------------------------------------------------------------------------------
	
	-- Debug
	--SELECT @ConfigurationID AS ConfigurationID, @ApplicationID AS ApplicationID, @BusinessEntityID AS BusinessEntityID,
	--	@KeyName AS KeyName, @Exists AS IsFound

	-----------------------------------------------------------------------------------------------------------------------
	-- Find object.
	-----------------------------------------------------------------------------------------------------------------------
	EXEC dbo.spConfiguration_Exists
		@ConfigurationID = @ConfigurationID OUTPUT,
		@ApplicationID = @ApplicationID,
		@BusinessEntityID = @BusinessEntityID,
		@KeyName = @KeyName,
		@Exists = @Exists OUTPUT

	-- Debug
	-- SELECT @ConfigurationID AS ConfigurationID, @Exists AS IsFound

	-----------------------------------------------------------------------------------------------------------------------
	-- Merge object.
	-----------------------------------------------------------------------------------------------------------------------
	IF ISNULL(@Exists, 0) = 0
	BEGIN
		EXEC dbo.spConfiguration_Create
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessEntityID,
			@KeyName = @KeyName,
			@KeyValue = @KeyValue,
			@DataTypeID = @DataTypeID,
			@DataSize = @DataSize,
			@Description = @Description,
			@CreatedBy = @CreatedBy,
			@ConfigurationID = @ConfigurationID OUTPUT
	END
	ELSE
	BEGIN
		EXEC dbo.spConfiguration_Update
			@ConfigurationID = @ConfigurationID,
			@ApplicationID = @ApplicationID,
			@BusinessEntityID = @BusinessEntityID,
			@KeyName = @KeyName,
			@KeyValue = @KeyValue,
			@DataTypeID = @DataTypeID,
			@DataSize = @DataSize,
			@Description = @Description,
			@ModifiedBy = @ModifiedBy
	END
	

	
	
END
