﻿CREATE TABLE [dbo].[Store_History] (
    [StoreHistoryID]   BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID] BIGINT           NULL,
    [SourceStoreID]    INT              NULL,
    [Name]             VARCHAR (255)    NULL,
    [NABP]             VARCHAR (50)     NULL,
    [TimeZoneID]       INT              NULL,
    [SupportDST]       BIT              NULL,
    [Demographics]     XML              NULL,
    [Website]          VARCHAR (255)    NULL,
    [rowguid]          UNIQUEIDENTIFIER NULL,
    [DateCreated]      DATETIME         NULL,
    [DateModified]     DATETIME         NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    [AuditGuid]        UNIQUEIDENTIFIER CONSTRAINT [DF_Store_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]      VARCHAR (10)     NULL,
    [DateAudited]      DATETIME         CONSTRAINT [DF_Store_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Store_History] PRIMARY KEY CLUSTERED ([StoreHistoryID] ASC)
);

