﻿CREATE TABLE [dbo].[SocialMedia] (
    [SocialMediaID] INT           IDENTITY (1, 1) NOT NULL,
    [Code]          NVARCHAR (10) NOT NULL,
    [Name]          NVARCHAR (50) NOT NULL,
    [Url]           VARCHAR (255) NOT NULL,
    [IconUrl]       VARCHAR (300) NULL,
    [DateCreated]   DATETIME      CONSTRAINT [DF_SocialMedia_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME      CONSTRAINT [DF_SocialMedia_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SocialMedia] PRIMARY KEY CLUSTERED ([SocialMediaID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_SocialMedia_Code]
    ON [dbo].[SocialMedia]([SocialMediaID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_SocialMedia_Name]
    ON [dbo].[SocialMedia]([Name] ASC);

