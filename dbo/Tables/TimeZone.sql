﻿CREATE TABLE [dbo].[TimeZone] (
    [TimeZoneID] INT            IDENTITY (1, 1) NOT NULL,
    [Code]       VARCHAR (10)   NULL,
    [Prefix]     VARCHAR (5)    NULL,
    [Location]   NVARCHAR (255) NOT NULL,
    [GMT]        NVARCHAR (25)  NOT NULL,
    [Offset]     INT            CONSTRAINT [DF_TimeZone_Offset] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TimeZone] PRIMARY KEY CLUSTERED ([TimeZoneID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TimeZone_LocationOffset]
    ON [dbo].[TimeZone]([Location] ASC, [Offset] ASC);

