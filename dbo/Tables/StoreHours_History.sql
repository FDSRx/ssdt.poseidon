﻿CREATE TABLE [dbo].[StoreHours_History] (
    [StoreHourHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [StoreHourID]        INT              NULL,
    [StoreID]            BIGINT           NULL,
    [HourTypeID]         INT              NULL,
    [DayID]              INT              NULL,
    [TimeStart]          TIME (7)         NULL,
    [TimeEnd]            TIME (7)         NULL,
    [TimeText]           VARCHAR (255)    NULL,
    [rowguid]            UNIQUEIDENTIFIER NULL,
    [DateCreated]        DATETIME         NULL,
    [DateModified]       DATETIME         NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    [AuditGuid]          UNIQUEIDENTIFIER CONSTRAINT [DF_StoreHours_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]        VARCHAR (10)     NULL,
    [DateAudited]        DATETIME         CONSTRAINT [DF_StoreHours_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_StoreHours_History] PRIMARY KEY CLUSTERED ([StoreHourHistoryID] ASC)
);

