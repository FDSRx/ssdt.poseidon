﻿CREATE TABLE [dbo].[Banner] (
    [BannerID]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [BannerTypeID] INT              NOT NULL,
    [Name]         VARCHAR (256)    NULL,
    [Header]       VARCHAR (MAX)    NULL,
    [Body]         VARCHAR (MAX)    NULL,
    [Footer]       VARCHAR (MAX)    NULL,
    [LanguageID]   INT              NOT NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Banner_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Banner_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Banner_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED ([BannerID] ASC),
    CONSTRAINT [FK_Banner_BannerType] FOREIGN KEY ([BannerTypeID]) REFERENCES [dbo].[BannerType] ([BannerTypeID]),
    CONSTRAINT [FK_Banner_Business] FOREIGN KEY ([BannerID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_Banner_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Banner_rowguid]
    ON [dbo].[Banner]([rowguid] ASC);


GO


