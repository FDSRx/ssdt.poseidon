﻿CREATE TABLE [dbo].[RandomNames] (
    [NameID]    INT          IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL
);

