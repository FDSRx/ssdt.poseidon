﻿CREATE TABLE [dbo].[BannerSchedule] (
    [BannerScheduleID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]    INT              NULL,
    [BusinessID]       BIGINT           NULL,
    [PersonID]         BIGINT           NULL,
    [BannerID]         BIGINT           NOT NULL,
    [DateStart]        DATETIME         CONSTRAINT [DF_BannerSchedule_DateStart] DEFAULT (getdate()) NOT NULL,
    [DateEnd]          DATETIME         CONSTRAINT [DF_BannerSchedule_DateEnd] DEFAULT (CONVERT([datetime],'12/31/9999',(0))) NOT NULL,
    [SortOrder]        DECIMAL (9, 2)   CONSTRAINT [DF_BannerSchedule_SortOrder] DEFAULT ((0)) NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_BannerSchedule_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_BannerSchedule_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_BannerSchedule_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_BannerSchedule] PRIMARY KEY CLUSTERED ([BannerScheduleID] ASC),
    CONSTRAINT [FK_BannerSchedule_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_BannerSchedule_Banner] FOREIGN KEY ([BannerID]) REFERENCES [dbo].[Banner] ([BannerID]),
    CONSTRAINT [FK_BannerSchedule_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_BannerSchedule_Person] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BannerSchedule_rowguid]
    ON [dbo].[BannerSchedule]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BannerSchedule_AppBizPersonBanner]
    ON [dbo].[BannerSchedule]([ApplicationID] ASC, [BusinessID] ASC, [PersonID] ASC, [BannerID] ASC);

