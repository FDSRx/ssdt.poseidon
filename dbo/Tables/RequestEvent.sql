﻿CREATE TABLE [dbo].[RequestEvent] (
    [EventID]      INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_RequestEvent_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_RequestEvent_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_RequestEvent_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_RequestEvent] PRIMARY KEY CLUSTERED ([EventID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_RequestEvent_Event] FOREIGN KEY ([EventID]) REFERENCES [dbo].[Event] ([EventID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_RequestEvent_Code]
    ON [dbo].[RequestEvent]([Code] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_RequestEvent_Name]
    ON [dbo].[RequestEvent]([Name] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_RequestEvent_rowguid]
    ON [dbo].[RequestEvent]([rowguid] ASC) WITH (FILLFACTOR = 90);

