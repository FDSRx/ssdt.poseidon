﻿CREATE TABLE [dbo].[Client] (
    [ClientID]     INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (128)    NOT NULL,
    [Description]  VARCHAR (256)    NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Client_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Client_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Client_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED ([ClientID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Client_Code]
    ON [dbo].[Client]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Client_Name]
    ON [dbo].[Client]([Name] ASC);

