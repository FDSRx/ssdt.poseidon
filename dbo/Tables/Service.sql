﻿CREATE TABLE [dbo].[Service] (
    [ServiceID]    INT              IDENTITY (1, 1) NOT NULL,
    [Code]         NVARCHAR (10)    NOT NULL,
    [Name]         NVARCHAR (50)    NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Service_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Service_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Service_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED ([ServiceID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Service_Code]
    ON [dbo].[Service]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Service_Name]
    ON [dbo].[Service]([Name] ASC);

