﻿CREATE TABLE [dbo].[Customer] (
    [CustomerID]       BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID] BIGINT           NOT NULL,
    [PersonID]         BIGINT           NOT NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Customer_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_Customer_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_Customer_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([CustomerID] ASC),
    CONSTRAINT [FK_Customer_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Customer_Person] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Customer_rowguid]
    ON [dbo].[Customer]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Customer_BizPerson]
    ON [dbo].[Customer]([BusinessEntityID] ASC, [PersonID] ASC);

