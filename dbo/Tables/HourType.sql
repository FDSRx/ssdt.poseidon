﻿CREATE TABLE [dbo].[HourType] (
    [HourTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (10)   NOT NULL,
    [Name]         VARCHAR (50)   NOT NULL,
    [Description]  VARCHAR (1000) NULL,
    [DateModified] DATETIME       CONSTRAINT [DF_HourType_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_HourType] PRIMARY KEY CLUSTERED ([HourTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_HourType_Code]
    ON [dbo].[HourType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_HourType_Name]
    ON [dbo].[HourType]([Name] ASC);

