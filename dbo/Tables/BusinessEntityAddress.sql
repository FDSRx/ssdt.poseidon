﻿CREATE TABLE [dbo].[BusinessEntityAddress] (
    [BusinessEntityAddressID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]        BIGINT           NOT NULL,
    [AddressID]               BIGINT           NOT NULL,
    [AddressTypeID]           INT              NOT NULL,
    [rowguid]                 UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityAddress_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]             DATETIME         CONSTRAINT [DF_BusinessEntityAddress_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]            DATETIME         CONSTRAINT [DF_BusinessEntityAddress_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]               VARCHAR (256)    NULL,
    [ModifiedBy]              VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessEntityAddress] PRIMARY KEY CLUSTERED ([BusinessEntityAddressID] ASC),
    CONSTRAINT [FK_BusinessEntityAddress_Address] FOREIGN KEY ([AddressID]) REFERENCES [dbo].[Address] ([AddressID]),
    CONSTRAINT [FK_BusinessEntityAddress_AddressType] FOREIGN KEY ([AddressTypeID]) REFERENCES [dbo].[AddressType] ([AddressTypeID]),
    CONSTRAINT [FK_BusinessEntityAddress_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityAddress_BizAddressType]
    ON [dbo].[BusinessEntityAddress]([BusinessEntityID] ASC, [AddressTypeID] ASC, [AddressID] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/2/2013
-- Description:	Audits a record change of the person address table
-- SELECT * FROM dbo.BusinessEntityAddress_History
-- SELECT * FROM dbo.BusinessEntityAddress WHERE BusinessEntityID = 59201
-- TRUNCATE TABLE dbo.BusinessEntityAddress_History
-- =============================================
CREATE TRIGGER [dbo].[trigBusinessEntityAddress_Audit]
   ON  dbo.BusinessEntityAddress
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM deleted) = 0 -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-- Audit inserted records (updated or newly created)
	INSERT INTO dbo.BusinessEntityAddress_History (
		BusinessEntityAddressID,	
		BusinessEntityID,	
		AddressID,	
		AddressTypeID,	
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityAddressID,	
		BusinessEntityID,	
		AddressID,	
		AddressTypeID,	
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	--SELECT * FROM dbo.PersonAddress
	
	

END
