﻿CREATE TABLE [dbo].[NoteType] (
    [NoteTypeID]   INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_NoteType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_NoteType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_NoteType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_NoteType] PRIMARY KEY CLUSTERED ([NoteTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_NoteType_Code]
    ON [dbo].[NoteType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_NoteType_Name]
    ON [dbo].[NoteType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_NoteType_rowguid]
    ON [dbo].[NoteType]([rowguid] ASC);

