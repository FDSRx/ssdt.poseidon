﻿CREATE TABLE [dbo].[Person] (
    [BusinessEntityID] BIGINT           NOT NULL,
    [PersonTypeID]     INT              NULL,
    [Title]            NVARCHAR (10)    NULL,
    [FirstName]        NVARCHAR (75)    NULL,
    [MiddleName]       NVARCHAR (75)    NULL,
    [LastName]         NVARCHAR (75)    NULL,
    [Suffix]           NVARCHAR (10)    NULL,
    [BirthDate]        DATE             NULL,
    [GenderID]         INT              NULL,
    [LanguageID]       INT              NULL,
    [AdditionalInfo]   XML              NULL,
    [Demographics]     XML              NULL,
    [HashKey]          VARCHAR (256)    NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Person_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_Person_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_Person_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED ([BusinessEntityID] ASC),
    CONSTRAINT [FK_Person_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Person_Gender] FOREIGN KEY ([GenderID]) REFERENCES [dbo].[Gender] ([GenderID]),
    CONSTRAINT [FK_Person_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_Person_PersonType] FOREIGN KEY ([PersonTypeID]) REFERENCES [dbo].[PersonType] ([PersonTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Person_rowguid]
    ON [dbo].[Person]([rowguid] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person_FirstLastBirth]
    ON [dbo].[Person]([FirstName] ASC, [LastName] ASC, [BirthDate] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/2/2013
-- Description:	Audits a record change of the person table
-- SELECT * FROM dbo.Person_History
-- SELECT * FROM dbo.Person WHERE BusinessEntityID = 59201
-- TRUNCATE TABLE dbo.Person_History
-- =============================================
CREATE TRIGGER [dbo].[trigPerson_Audit]
   ON  dbo.Person
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM deleted) = 0 -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-- Audit inserted records (updated or newly created)
	INSERT INTO dbo.Person_History (
		BusinessEntityID,	
		PersonTypeID,	
		Title,	
		FirstName,	
		MiddleName,	
		LastName,	
		Suffix,	
		BirthDate,	
		GenderID,	
		LanguageID,	
		AdditionalInfo,	
		Demographics,
		HashKey,	
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityID,	
		PersonTypeID,	
		Title,	
		FirstName,	
		MiddleName,	
		LastName,	
		Suffix,	
		BirthDate,	
		GenderID,	
		LanguageID,	
		AdditionalInfo,	
		Demographics,
		HashKey,	
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,
		ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	--SELECT * FROM dbo.Person
	
	

END
