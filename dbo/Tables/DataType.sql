﻿CREATE TABLE [dbo].[DataType] (
    [DataTypeID]   INT              IDENTITY (1, 1) NOT NULL,
    [TypeOf]       VARCHAR (50)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [Storage]      VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_DataType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_DataType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_DataType_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_DataType] PRIMARY KEY CLUSTERED ([DataTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DataType_Name]
    ON [dbo].[DataType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DataType_TypeOf]
    ON [dbo].[DataType]([TypeOf] ASC);


GO
CREATE NONCLUSTERED INDEX [UIX_DataType_rowguid]
    ON [dbo].[DataType]([rowguid] ASC);

