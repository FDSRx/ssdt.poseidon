﻿CREATE TABLE [dbo].[Task] (
    [NoteID]            BIGINT           NOT NULL,
    [ApplicationID]     INT              NULL,
    [BusinessID]        BIGINT           NULL,
    [BusinessEntityID]  BIGINT           NULL,
    [DateDue]           DATETIME         NULL,
    [AssignedByID]      BIGINT           NULL,
    [AssignedByString]  VARCHAR (256)    NULL,
    [AssignedToID]      BIGINT           NULL,
    [AssignedToString]  VARCHAR (256)    NULL,
    [CompletedByID]     BIGINT           NULL,
    [CompletedByString] VARCHAR (256)    NULL,
    [DateCompleted]     DATETIME         NULL,
    [TaskStatusID]      INT              NULL,
    [ParentTaskID]      BIGINT           NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_Task_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_Task_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_Task_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED ([NoteID] ASC),
    CONSTRAINT [FK_Task_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_Task_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_Task_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Task_BusinessEntity_AssignedBy] FOREIGN KEY ([AssignedByID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Task_BusinessEntity_AssignedTo] FOREIGN KEY ([AssignedToID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Task_BusinessEntity_CompletedBy] FOREIGN KEY ([CompletedByID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Task_Note] FOREIGN KEY ([NoteID]) REFERENCES [dbo].[Note] ([NoteID]),
    CONSTRAINT [FK_Task_Task] FOREIGN KEY ([ParentTaskID]) REFERENCES [dbo].[Task] ([NoteID]),
    CONSTRAINT [FK_Task_TaskStatus] FOREIGN KEY ([TaskStatusID]) REFERENCES [dbo].[TaskStatus] ([TaskStatusID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Task_rowguid]
    ON [dbo].[Task]([rowguid] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Task_DateDue]
    ON [dbo].[Task]([DateDue] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Task_DateCompleted]
    ON [dbo].[Task]([DateCompleted] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Task_BusinessDateDue]
    ON [dbo].[Task]([BusinessID] ASC, [DateDue] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Task_BusinessDateCompleted]
    ON [dbo].[Task]([BusinessID] ASC, [DateCompleted] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/25/2014
-- Description:	Audits a record change of the Task table

-- SELECT * FROM dbo.Task_History
-- TRUNCATE TABLE dbo.Task_History
-- =============================================
CREATE TRIGGER [dbo].[trigTask_Audit]
   ON  [dbo].[Task]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------------------
	-- Exit trigger when zero records affected
	--------------------------------------------------------------------------------------------------------------------
	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) 
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	--------------------------------------------------------------------------------------------------------------------
	-- Retrieve session data.
	--------------------------------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT

	--------------------------------------------------------------------------------------------------------------------			
	-- Audit inserted records (updated or deleted)
	--------------------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.Task_History (
		NoteID,
		ApplicationID,
		BusinessID,
		BusinessEntityID,	
		DateDue,	
		AssignedByID,
		AssignedByString,
		AssignedToID,
		AssignedToString,	
		CompletedByID,	
		CompletedByString,
		DateCompleted,	
		TaskStatusID,
		ParentTaskID,
		rowguid,	
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		NoteID,	
		ApplicationID,
		BusinessID,
		BusinessEntityID,	
		DateDue,	
		AssignedByID,
		AssignedByString,
		AssignedToID,
		AssignedToString,	
		CompletedByID,	
		CompletedByString,
		DateCompleted,	
		TaskStatusID,
		ParentTaskID,
		rowguid,	
		DateCreated,
		DateModified,
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_NAME()
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	--SELECT * FROM dbo.Task
	--SELECT * FROM dbo.Task_History
	
	

END
