﻿CREATE TABLE [dbo].[Event] (
    [EventID]     INT              IDENTITY (1, 1) NOT NULL,
    [EventTypeID] INT              NOT NULL,
    [rowguid]     UNIQUEIDENTIFIER CONSTRAINT [DF_Event_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated] DATETIME         CONSTRAINT [DF_Event_DateCreated] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED ([EventID] ASC),
    CONSTRAINT [FK_Event_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Event_rowguid]
    ON [dbo].[Event]([rowguid] ASC);

