﻿CREATE TABLE [dbo].[DataTable] (
    [DataTableID]   INT              IDENTITY (1, 1) NOT NULL,
    [DataCatalogID] INT              NOT NULL,
    [DataSchemaID]  INT              NOT NULL,
    [Name]          VARCHAR (256)    NOT NULL,
    [Description]   VARCHAR (1000)   NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_DataTable_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_DataTable_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_DataTable_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_DataTable] PRIMARY KEY CLUSTERED ([DataTableID] ASC),
    CONSTRAINT [FK_DataTable_DataCatalog] FOREIGN KEY ([DataCatalogID]) REFERENCES [dbo].[DataCatalog] ([DataCatalogID]),
    CONSTRAINT [FK_DataTable_DataSchema] FOREIGN KEY ([DataSchemaID]) REFERENCES [dbo].[DataSchema] ([DataSchemaID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataTable_SchemaTable]
    ON [dbo].[DataTable]([DataSchemaID] ASC, [Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataTable_rowguid]
    ON [dbo].[DataTable]([rowguid] ASC);

