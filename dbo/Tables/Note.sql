﻿CREATE TABLE [dbo].[Note] (
    [NoteID]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]    INT              NULL,
    [BusinessID]       BIGINT           NULL,
    [BusinessEntityID] BIGINT           NOT NULL,
    [ScopeID]          INT              NOT NULL,
    [NoteTypeID]       INT              NOT NULL,
    [NotebookID]       INT              NOT NULL,
    [Title]            VARCHAR (1000)   NULL,
    [Memo]             VARCHAR (MAX)    NULL,
    [PriorityID]       INT              NOT NULL,
    [OriginID]         INT              NULL,
    [OriginDataKey]    VARCHAR (256)    NULL,
    [AdditionalData]   XML              NULL,
    [CorrelationKey]   VARCHAR (256)    NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Note_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_Note_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_Note_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Note] PRIMARY KEY CLUSTERED ([NoteID] ASC),
    CONSTRAINT [FK_Note_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_Note_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_Note_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Note_DataSource] FOREIGN KEY ([OriginID]) REFERENCES [dbo].[Origin] ([OriginID]),
    CONSTRAINT [FK_Note_Notebook] FOREIGN KEY ([NotebookID]) REFERENCES [dbo].[Notebook] ([NotebookID]),
    CONSTRAINT [FK_Note_NoteType] FOREIGN KEY ([NoteTypeID]) REFERENCES [dbo].[NoteType] ([NoteTypeID]),
    CONSTRAINT [FK_Note_Priority] FOREIGN KEY ([PriorityID]) REFERENCES [dbo].[Priority] ([PriorityID]),
    CONSTRAINT [FK_Note_Scope] FOREIGN KEY ([ScopeID]) REFERENCES [dbo].[Scope] ([ScopeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Note_rowguid]
    ON [dbo].[Note]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/25/2014
-- Description:	Audits a record change of the Note table
-- SELECT * FROM dbo.Note_History
-- TRUNCATE TABLE dbo.Note_History
-- =============================================
CREATE TRIGGER [dbo].[trigNote_Audit]
   ON  [dbo].[Note]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	-----------------------------------------------------------------------------
	-- Retrieve session data.
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT

	-----------------------------------------------------------------------------			
	-- Audit inserted records (updated or newly created)
	-----------------------------------------------------------------------------
	INSERT INTO dbo.Note_History (
		NoteID,	
		ApplicationID,
		BusinessID,
		BusinessEntityID,	
		ScopeID,	
		NoteTypeID,	
		NotebookID,	
		Title,
		Memo,
		PriorityID,	
		OriginID,	
		OriginDataKey,
		AdditionalData,
		CorrelationKey,
		rowguid,	
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		NoteID,	
		ApplicationID,
		BusinessID,
		BusinessEntityID,	
		ScopeID,	
		NoteTypeID,	
		NotebookID,	
		Title,
		Memo,
		PriorityID,	
		OriginID,	
		OriginDataKey,
		AdditionalData,
		CorrelationKey,
		rowguid,	
		DateCreated,
		DateModified,
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_NAME()
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	--SELECT * FROM dbo.Note
	
	

END
