﻿CREATE TABLE [dbo].[CodeQualifier] (
    [CodeQualifierID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]            VARCHAR (25)     NOT NULL,
    [Name]            VARCHAR (50)     NOT NULL,
    [Description]     VARCHAR (1000)   NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_CodeQualifier_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_CodeQualifier_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_CodeQualifier_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)    NULL,
    [ModifiedBy]      VARCHAR (256)    NULL,
    CONSTRAINT [PK_CodeQualifier] PRIMARY KEY CLUSTERED ([CodeQualifierID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CodeQualifier_Code]
    ON [dbo].[CodeQualifier]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CodeQualifier_Name]
    ON [dbo].[CodeQualifier]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CodeQualifier_rowguid]
    ON [dbo].[CodeQualifier]([rowguid] ASC);

