﻿CREATE TABLE [dbo].[BusinessType] (
    [BusinessTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]           VARCHAR (25)     NOT NULL,
    [Name]           VARCHAR (25)     NOT NULL,
    [Description]    VARCHAR (1000)   NULL,
    [rowguid]        UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]    DATETIME         CONSTRAINT [DF_BusinessType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]   DATETIME         CONSTRAINT [DF_BusinessType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      VARCHAR (256)    NULL,
    [ModifiedBy]     VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessType] PRIMARY KEY CLUSTERED ([BusinessTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessType_Code]
    ON [dbo].[BusinessType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessType_Name]
    ON [dbo].[BusinessType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessType_rowguid]
    ON [dbo].[BusinessType]([rowguid] ASC);

