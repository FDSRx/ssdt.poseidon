﻿CREATE TABLE [dbo].[EventLogType] (
    [EventLogTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]           VARCHAR (25)     NOT NULL,
    [Name]           VARCHAR (50)     NOT NULL,
    [Description]    VARCHAR (1000)   NULL,
    [rowguid]        UNIQUEIDENTIFIER CONSTRAINT [DF_EventLogType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]    DATETIME         CONSTRAINT [DF_EventLogType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]   DATETIME         CONSTRAINT [DF_EventLogType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      VARCHAR (256)    NULL,
    [ModifiedBy]     VARCHAR (256)    NULL,
    CONSTRAINT [PK_EventLogType] PRIMARY KEY CLUSTERED ([EventLogTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EventLogType_Code]
    ON [dbo].[EventLogType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EventLogType_Name]
    ON [dbo].[EventLogType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EventLogType_rowguid]
    ON [dbo].[EventLogType]([rowguid] ASC);

