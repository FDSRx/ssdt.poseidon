﻿CREATE TABLE [dbo].[Chain] (
    [BusinessEntityID] BIGINT           NOT NULL,
    [SourceChainID]    INT              NOT NULL,
    [Name]             VARCHAR (255)    NOT NULL,
    [Demographics]     XML              NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Chain_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_Chain_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_Chain_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Chain] PRIMARY KEY CLUSTERED ([BusinessEntityID] ASC),
    CONSTRAINT [FK_Chain_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Chain_rowguid]
    ON [dbo].[Chain]([BusinessEntityID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Chain_SourceChainID]
    ON [dbo].[Chain]([SourceChainID] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/23/2016
-- Description:	Audits a record change of the Chain table.

-- SELECT * FROM dbo.Chain
-- SELECT * FROM dbo.Chain_History

-- TRUNCATE TABLE dbo.Chain_History
-- =============================================
CREATE TRIGGER [dbo].[trigChain_Audit]
   ON  [dbo].Chain
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------------
	-- Short circut audit if no records.
	------------------------------------------------------------------------------------------------------------------
	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;

	------------------------------------------------------------------------------------------------------------------	
	-- Determine type of operation being performed (i.e. create, update, delete, etc.).
	------------------------------------------------------------------------------------------------------------------
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	------------------------------------------------------------------------------------------------------------------
	-- Retrieve session data
	------------------------------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
			
	------------------------------------------------------------------------------------------------------------------	
	-- Audit inserted records (updated or newly created)
	------------------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.Chain_History (
		BusinessEntityID,
		SourceChainID,
		Name,
		Demographics,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityID,
		SourceChainID,
		Name,
		Demographics,
		rowguid,
		DateCreated,
		DateModified,	
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	-- Data review.
	-- SELECT * FROM dbo.Chain
	-- SELECT * FROM dbo.Chain_History
	
	

END
