﻿CREATE TABLE [dbo].[Store] (
    [BusinessEntityID] BIGINT           NOT NULL,
    [SourceStoreID]    INT              NOT NULL,
    [Name]             VARCHAR (255)    NOT NULL,
    [NABP]             VARCHAR (50)     NULL,
    [TimeZoneID]       INT              NOT NULL,
    [SupportDST]       BIT              CONSTRAINT [DF_Store_SupportDST] DEFAULT ((0)) NOT NULL,
    [Demographics]     XML              NULL,
    [Website]          VARCHAR (255)    NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Store_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_Store_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_Store_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Store] PRIMARY KEY CLUSTERED ([BusinessEntityID] ASC),
    CONSTRAINT [FK_Store_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Store_TimeZone] FOREIGN KEY ([TimeZoneID]) REFERENCES [dbo].[TimeZone] ([TimeZoneID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Store_SourceStoreID]
    ON [dbo].[Store]([SourceStoreID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Store_rowguid]
    ON [dbo].[Store]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Store_Nabp]
    ON [dbo].[Store]([NABP] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/23/2016
-- Description:	Audits a record change of the Store table.

-- SELECT * FROM dbo.Store
-- SELECT * FROM dbo.Store_History

-- TRUNCATE TABLE dbo.Store_History
-- =============================================
CREATE TRIGGER [dbo].[trigStore_Audit]
   ON  [dbo].Store
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------------
	-- Short circut audit if no records.
	------------------------------------------------------------------------------------------------------------------
	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;

	------------------------------------------------------------------------------------------------------------------	
	-- Determine type of operation being performed (i.e. create, update, delete, etc.).
	------------------------------------------------------------------------------------------------------------------
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	------------------------------------------------------------------------------------------------------------------
	-- Retrieve session data
	------------------------------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
			
	------------------------------------------------------------------------------------------------------------------	
	-- Audit inserted records (updated or newly created)
	------------------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.Store_History (
		BusinessEntityID,
		SourceStoreID,
		Name,
		NABP,
		TimeZoneID,
		SupportDST,
		Demographics,
		Website,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityID,
		SourceStoreID,
		Name,
		NABP,
		TimeZoneID,
		SupportDST,
		Demographics,
		Website,
		rowguid,
		DateCreated,
		DateModified,	
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	-- Data review.
	-- SELECT * FROM dbo.Store
	-- SELECT * FROM dbo.Store_History
	
	

END
