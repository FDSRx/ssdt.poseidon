﻿CREATE TABLE [dbo].[Business_History] (
    [BusinessHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]  BIGINT           NULL,
    [BusinessTypeID]    INT              NULL,
    [BusinessToken]     UNIQUEIDENTIFIER NULL,
    [BusinessNumber]    VARCHAR (10)     NULL,
    [Name]              VARCHAR (256)    NULL,
    [SourceSystemKey]   VARCHAR (256)    NULL,
    [rowguid]           UNIQUEIDENTIFIER NULL,
    [DateCreated]       DATETIME         NULL,
    [DateModified]      DATETIME         NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    [AuditGuid]         UNIQUEIDENTIFIER CONSTRAINT [DF_Business_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]       VARCHAR (10)     NULL,
    [DateAudited]       DATETIME         CONSTRAINT [DF_Business_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Business_History] PRIMARY KEY CLUSTERED ([BusinessHistoryID] ASC)
);

