﻿CREATE TABLE [dbo].[PhoneNumberType] (
    [PhoneNumberTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [Code]              NVARCHAR (10) NOT NULL,
    [Name]              NVARCHAR (50) NOT NULL,
    [DateCreated]       DATETIME      CONSTRAINT [DF_PhoneNumberType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME      CONSTRAINT [DF_PhoneNumberType_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PhoneNumberType] PRIMARY KEY CLUSTERED ([PhoneNumberTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PhoneNumberType_Code]
    ON [dbo].[PhoneNumberType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PhoneNumberType_Name]
    ON [dbo].[PhoneNumberType]([Name] ASC);

