﻿CREATE TABLE [dbo].[DataSchema] (
    [DataSchemaID] INT              IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (10)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_DataSchema_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_DataSchema_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_DataSchema_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_DataSchema] PRIMARY KEY CLUSTERED ([DataSchemaID] ASC)
);

