﻿CREATE TABLE [dbo].[ProviderKeyring] (
    [ProviderKeyringID]  INT              IDENTITY (1, 1) NOT NULL,
    [ProviderID]         INT              NOT NULL,
    [KeyringDirectionID] INT              NOT NULL,
    [AsymmetricKeyID]    BIGINT           NULL,
    [SymmetricKeyID]     BIGINT           NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_ProviderKeyring_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_ProviderKeyring_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_ProviderKeyring_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_ProviderKeyring] PRIMARY KEY CLUSTERED ([ProviderKeyringID] ASC),
    CONSTRAINT [FK_ProviderKeyring_AsymmetricKey] FOREIGN KEY ([AsymmetricKeyID]) REFERENCES [secur].[AsymmetricKey] ([AsymmetricKeyID]),
    CONSTRAINT [FK_ProviderKeyring_KeyringDirection] FOREIGN KEY ([KeyringDirectionID]) REFERENCES [secur].[KeyringDirection] ([KeyringDirectionID]),
    CONSTRAINT [FK_ProviderKeyring_Provider] FOREIGN KEY ([ProviderID]) REFERENCES [dbo].[Provider] ([ProviderID]),
    CONSTRAINT [FK_ProviderKeyring_SymmetricKey] FOREIGN KEY ([SymmetricKeyID]) REFERENCES [secur].[SymmetricKey] ([SymmetricKeyID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ProviderKeyring_ProviderDirection]
    ON [dbo].[ProviderKeyring]([ProviderID] ASC, [KeyringDirectionID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ProviderKeyring_rowguid]
    ON [dbo].[ProviderKeyring]([rowguid] ASC);

