﻿CREATE TABLE [dbo].[BusinessEntityPrivate] (
    [BusinessEntityPrivateID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]           INT              NULL,
    [BusinessID]              BIGINT           NULL,
    [ServiceID]               INT              NULL,
    [BusinessEntityID]        BIGINT           NOT NULL,
    [rowguid]                 UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityPrivate_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]             DATETIME         CONSTRAINT [DF_BusinessEntityPrivate_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]            DATETIME         CONSTRAINT [DF_BusinessEntityPrivate_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]               VARCHAR (256)    NULL,
    [ModifiedBy]              VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessEntityPrivate] PRIMARY KEY CLUSTERED ([BusinessEntityPrivateID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_BusinessEntityPrivate_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntityPrivate_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntityPrivate_BusinessEntityPrivate] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_BusinessEntityPrivate_Service] FOREIGN KEY ([ServiceID]) REFERENCES [dbo].[Service] ([ServiceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BusinessEntityPrivate_AppBizServEntity]
    ON [dbo].[BusinessEntityPrivate]([ApplicationID] ASC, [BusinessID] ASC, [ServiceID] ASC, [BusinessEntityID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityPrivate_rowguid]
    ON [dbo].[BusinessEntityPrivate]([rowguid] ASC) WITH (FILLFACTOR = 90);

