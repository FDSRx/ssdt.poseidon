﻿CREATE TABLE [dbo].[DataTableMetaData] (
    [DataTableMetaDataID] BIGINT           NOT NULL,
    [DataTableID]         INT              NOT NULL,
    [ColumnName]          VARCHAR (256)    NOT NULL,
    [DataType]            VARCHAR (50)     NOT NULL,
    [AllowNulls]          BIT              CONSTRAINT [DF_DataTableMetaData_AllowNull] DEFAULT ((1)) NOT NULL,
    [Description]         VARCHAR (1000)   NULL,
    [rowguid]             UNIQUEIDENTIFIER CONSTRAINT [DF_DataTableMetaData_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]         DATETIME         CONSTRAINT [DF_DataTableMetaData_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]        DATETIME         CONSTRAINT [DF_DataTableMetaData_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           VARCHAR (256)    NULL,
    [ModifiedBy]          VARCHAR (256)    NULL,
    CONSTRAINT [PK_DataTableMetaData] PRIMARY KEY CLUSTERED ([DataTableMetaDataID] ASC),
    CONSTRAINT [FK_DataTableMetaData_DataTable] FOREIGN KEY ([DataTableID]) REFERENCES [dbo].[DataTable] ([DataTableID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataTableMetaData_TableColumn]
    ON [dbo].[DataTableMetaData]([DataTableID] ASC, [ColumnName] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataTableMetaData_rowguid]
    ON [dbo].[DataTableMetaData]([rowguid] ASC);

