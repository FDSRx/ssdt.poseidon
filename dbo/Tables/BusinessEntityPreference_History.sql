﻿CREATE TABLE [dbo].[BusinessEntityPreference_History] (
    [BusinessEntityPreferenceHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityPreferenceID]        BIGINT           NOT NULL,
    [BusinessEntityID]                  BIGINT           NOT NULL,
    [PreferenceID]                      INT              NOT NULL,
    [ValueString]                       VARCHAR (MAX)    NULL,
    [ValueBoolean]                      BIT              NULL,
    [ValueNumeric]                      DECIMAL (18)     NULL,
    [rowguid]                           UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]                       DATETIME         NOT NULL,
    [DateModified]                      DATETIME         NOT NULL,
    [CreatedBy]                         VARCHAR (256)    NULL,
    [ModifiedBy]                        VARCHAR (256)    NULL,
    [AuditGuid]                         UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [AuditAction]                       VARCHAR (10)     NULL,
    [DateAudited]                       DATETIME         DEFAULT (getdate()) NULL
);

