﻿CREATE TABLE [dbo].[EventLog] (
    [EventLogID]      BIGINT           IDENTITY (1, 1) NOT NULL,
    [EventLogTypeID]  INT              NOT NULL,
    [EventID]         INT              NOT NULL,
    [EventSource]     VARCHAR (256)    NOT NULL,
    [EventDataXml]    XML              NULL,
    [EventDataString] VARCHAR (MAX)    NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_EventLog_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_EventLog_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_EventLog_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)    NULL,
    [ModifiedBy]      VARCHAR (256)    NULL,
    CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED ([EventLogID] ASC),
    CONSTRAINT [FK_EventLog_Event] FOREIGN KEY ([EventID]) REFERENCES [dbo].[Event] ([EventID]),
    CONSTRAINT [FK_EventLog_EventLogType] FOREIGN KEY ([EventLogTypeID]) REFERENCES [dbo].[EventLogType] ([EventLogTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EventLog_rowguid]
    ON [dbo].[EventLog]([rowguid] ASC);

