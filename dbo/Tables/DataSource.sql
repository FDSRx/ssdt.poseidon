﻿CREATE TABLE [dbo].[DataSource] (
    [DataSourceID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (50)     NOT NULL,
    [Name]         VARCHAR (256)    NOT NULL,
    [Namespace]    VARCHAR (256)    NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_DataSource_rowguid_1] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_DataSource_DateCreated_1] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_DataSource_DateModified_1] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_DataSource] PRIMARY KEY CLUSTERED ([DataSourceID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DataSource_Code]
    ON [dbo].[DataSource]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DataSource_Name]
    ON [dbo].[DataSource]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DataSource_rowguid]
    ON [dbo].[DataSource]([rowguid] ASC);

