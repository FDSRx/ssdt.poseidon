﻿CREATE TABLE [dbo].[DataServer] (
    [DataServerID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_DataServer_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_DataServer_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_DataServer_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_DataServer] PRIMARY KEY CLUSTERED ([DataServerID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataServer_Code]
    ON [dbo].[DataServer]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataServer_Name]
    ON [dbo].[DataServer]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataServer_rowguid]
    ON [dbo].[DataServer]([rowguid] ASC);

