﻿CREATE TABLE [dbo].[Scope] (
    [ScopeID]      INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Scope_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Scope_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Scope_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Scope] PRIMARY KEY CLUSTERED ([ScopeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Scope_Code]
    ON [dbo].[Scope]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Scope_Name]
    ON [dbo].[Scope]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Scope_rowguid]
    ON [dbo].[Scope]([rowguid] ASC);

