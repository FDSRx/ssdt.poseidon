﻿CREATE TABLE [dbo].[RoutingConfiguration] (
    [RoutingConfigurationID] INT          IDENTITY (1, 1) NOT NULL,
    [ClientID]               INT          NULL,
    [ConfigurationXml]       XML          NOT NULL,
    [CreatedOn]              DATETIME     NOT NULL,
    [CreatedBy]              VARCHAR (50) NOT NULL,
    [ModifiedOn]             DATETIME     NOT NULL,
    [ModifiedBy]             VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_RoutingConfiguration_1] PRIMARY KEY CLUSTERED ([RoutingConfigurationID] ASC),
    CONSTRAINT [FK_RoutingConfiguration_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
);

