﻿CREATE TABLE [dbo].[Tag] (
    [TagID]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (500)    NOT NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Tag_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Tag_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Tag_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    [Code]         VARCHAR (25)     NULL,
    CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED ([TagID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Tag_rowguid]
    ON [dbo].[Tag]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Tag_Name]
    ON [dbo].[Tag]([Name] ASC);

