﻿CREATE TABLE [dbo].[Holiday] (
    [HolidayID]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [HolidayTypeID] INT              NOT NULL,
    [DayID]         INT              NULL,
    [DateObserved]  DATE             NOT NULL,
    [Name]          VARCHAR (256)    NOT NULL,
    [WhereObserved] VARCHAR (1000)   NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_Holiday_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_Holiday_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_Holiday_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_Holiday] PRIMARY KEY CLUSTERED ([HolidayID] ASC),
    CONSTRAINT [FK_Holiday_Days] FOREIGN KEY ([DayID]) REFERENCES [dbo].[Days] ([DayID]),
    CONSTRAINT [FK_Holiday_HolidayType] FOREIGN KEY ([HolidayTypeID]) REFERENCES [dbo].[HolidayType] ([HolidayTypeID])
);

