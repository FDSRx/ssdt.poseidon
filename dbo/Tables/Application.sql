﻿CREATE TABLE [dbo].[Application] (
    [ApplicationID]     INT              IDENTITY (1, 1) NOT NULL,
    [ApplicationTypeID] INT              NOT NULL,
    [Code]              VARCHAR (15)     NOT NULL,
    [Name]              VARCHAR (50)     NOT NULL,
    [Description]       VARCHAR (255)    NULL,
    [LicenseName]       VARCHAR (256)    NULL,
    [ApplicationPath]   VARCHAR (512)    NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_Application_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_Application_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_Application_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED ([ApplicationID] ASC),
    CONSTRAINT [FK_Application_ApplicationType] FOREIGN KEY ([ApplicationTypeID]) REFERENCES [dbo].[ApplicationType] ([ApplicationTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Application_Code]
    ON [dbo].[Application]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Application_Name]
    ON [dbo].[Application]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Application_rowguid]
    ON [dbo].[Application]([rowguid] ASC);

