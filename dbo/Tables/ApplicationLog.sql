﻿CREATE TABLE [dbo].[ApplicationLog] (
    [ApplicationLogID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]    INT              NOT NULL,
    [LogEntryTypeID]   INT              NOT NULL,
    [MachineName]      VARCHAR (256)    NULL,
    [ServiceName]      VARCHAR (256)    NULL,
    [SourceName]       VARCHAR (256)    NULL,
    [EventName]        VARCHAR (128)    NULL,
    [UserName]         VARCHAR (256)    NULL,
    [StartTime]        DATETIME         CONSTRAINT [DF_ApplicationLog_StartTime] DEFAULT (getdate()) NULL,
    [EndTime]          DATETIME         CONSTRAINT [DF_ApplicationLog_EndTime] DEFAULT (getdate()) NULL,
    [Message]          VARCHAR (MAX)    NULL,
    [Arguments]        VARCHAR (MAX)    NULL,
    [StackTrace]       VARCHAR (MAX)    NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_ApplicationLog_rowguid] DEFAULT (newid()) NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_ApplicationLog_DateCreated] DEFAULT (getdate()) NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_ApplicationLog_DateModified] DEFAULT (getdate()) NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_ApplicationLog] PRIMARY KEY CLUSTERED ([ApplicationLogID] ASC),
    CONSTRAINT [FK_ApplicationLog_ApplicationLog] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_ApplicationLog_LogEntryType] FOREIGN KEY ([LogEntryTypeID]) REFERENCES [dbo].[LogEntryType] ([LogEntryTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ApplicationLog_rowguid]
    ON [dbo].[ApplicationLog]([rowguid] ASC);

