﻿CREATE TABLE [dbo].[TagType] (
    [TagTypeID]    INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_TagType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_TagType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_TagType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_TagType] PRIMARY KEY CLUSTERED ([TagTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TagType_Code]
    ON [dbo].[TagType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TagType_Name]
    ON [dbo].[TagType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TagType_rowguid]
    ON [dbo].[TagType]([rowguid] ASC);

