﻿CREATE TABLE [dbo].[Password] (
    [PasswordID]   INT             IDENTITY (1, 1) NOT NULL,
    [PersonID]     INT             NOT NULL,
    [ServiceID]    INT             NOT NULL,
    [PasswordHash] VARBINARY (128) NOT NULL,
    [PasswordSalt] VARBINARY (10)  NOT NULL,
    [DateCreated]  DATETIME        CONSTRAINT [DF_Password_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME        CONSTRAINT [DF_Password_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Password] PRIMARY KEY CLUSTERED ([PasswordID] ASC),
    CONSTRAINT [FK_Password_Service] FOREIGN KEY ([ServiceID]) REFERENCES [dbo].[Service] ([ServiceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Password_PersonService]
    ON [dbo].[Password]([PersonID] ASC, [ServiceID] ASC);

