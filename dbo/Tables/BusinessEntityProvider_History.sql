﻿CREATE TABLE [dbo].[BusinessEntityProvider_History] (
    [BusinessEntityProviderHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityProviderID]        BIGINT           NULL,
    [BusinessEntityID]                BIGINT           NULL,
    [ProviderID]                      INT              NULL,
    [Username]                        VARCHAR (256)    NULL,
    [PasswordUnencrypted]             NVARCHAR (256)   NULL,
    [PasswordEncrypted]               NVARCHAR (256)   NULL,
    [rowguid]                         UNIQUEIDENTIFIER NULL,
    [DateCreated]                     DATETIME         NULL,
    [DateModified]                    DATETIME         NULL,
    [CreatedBy]                       VARCHAR (256)    NULL,
    [ModifiedBy]                      VARCHAR (256)    NULL,
    [AuditGuid]                       UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityProvider_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                     VARCHAR (10)     NULL,
    [DateAudited]                     DATETIME         CONSTRAINT [DF_BusinessEntityProvider_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BusinessEntityProvider_History] PRIMARY KEY CLUSTERED ([BusinessEntityProviderHistoryID] ASC)
);

