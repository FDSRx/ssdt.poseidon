﻿CREATE TABLE [dbo].[InteractionType] (
    [InteractionTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]              VARCHAR (25)     NOT NULL,
    [Name]              VARCHAR (50)     NOT NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_InteractionType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_InteractionType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_InteractionType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_InteractionType] PRIMARY KEY CLUSTERED ([InteractionTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_InteractionType_Code]
    ON [dbo].[InteractionType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_InteractionType_Name]
    ON [dbo].[InteractionType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_InteractionType_rowguid]
    ON [dbo].[InteractionType]([rowguid] ASC);

