﻿CREATE TABLE [dbo].[BusinessEntityProvider] (
    [BusinessEntityProviderID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]         BIGINT           NOT NULL,
    [ProviderID]               INT              NOT NULL,
    [Username]                 VARCHAR (256)    NULL,
    [PasswordUnencrypted]      NVARCHAR (256)   NULL,
    [PasswordEncrypted]        NVARCHAR (256)   NULL,
    [rowguid]                  UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityProvider_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]              DATETIME         CONSTRAINT [DF_BusinessEntityProvider_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]             DATETIME         CONSTRAINT [DF_BusinessEntityProvider_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                VARCHAR (256)    NULL,
    [ModifiedBy]               VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessEntityProvider] PRIMARY KEY CLUSTERED ([BusinessEntityProviderID] ASC),
    CONSTRAINT [FK_BusinessEntityProvider_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntityProvider_Provider] FOREIGN KEY ([ProviderID]) REFERENCES [dbo].[Provider] ([ProviderID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityProvider_BizProvider]
    ON [dbo].[BusinessEntityProvider]([BusinessEntityID] ASC, [ProviderID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityProvider_rowguid]
    ON [dbo].[BusinessEntityProvider]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/29/2016
-- Description:	Audits a record change of the BusinessEntityProvider table.

-- SELECT * FROM dbo.BusinessEntityProvider
-- SELECT * FROM dbo.BusinessEntityProvider_History

-- TRUNCATE TABLE dbo.BusinessEntityProvider_History
-- =============================================
CREATE TRIGGER [dbo].[trigBusinessEntityProvider_Audit]
   ON  [dbo].[BusinessEntityProvider]
   AFTER DELETE, UPDATE
AS 
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------------
	-- Short circut audit if no records.
	------------------------------------------------------------------------------------------------------------------
	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;

	------------------------------------------------------------------------------------------------------------------	
	-- Determine type of operation being performed (i.e. create, update, delete, etc.).
	------------------------------------------------------------------------------------------------------------------
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	------------------------------------------------------------------------------------------------------------------
	-- Retrieve session data.
	------------------------------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
			
	------------------------------------------------------------------------------------------------------------------	
	-- Audit records (updated or deleted).
	------------------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.BusinessEntityProvider_History (
		BusinessEntityProviderID,
		BusinessEntityID,
		ProviderID,
		Username,
		PasswordUnencrypted,
		PasswordEncrypted,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityProviderID,
		BusinessEntityID,
		ProviderID,
		Username,
		PasswordUnencrypted,
		PasswordEncrypted,
		rowguid,
		DateCreated,
		DateModified,	
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_NAME()
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	-- Data review.
	-- SELECT * FROM dbo.BusinessEntityProvider
	-- SELECT * FROM dbo.BusinessEntityProvider_History
	
	

END
