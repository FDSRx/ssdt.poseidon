﻿CREATE TABLE [dbo].[LastNames] (
    [NameID]   INT          IDENTITY (1, 1) NOT NULL,
    [LastName] VARCHAR (50) NULL
);

