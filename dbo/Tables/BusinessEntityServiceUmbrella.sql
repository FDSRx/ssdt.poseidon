﻿CREATE TABLE [dbo].[BusinessEntityServiceUmbrella] (
    [BusinessEntityServiceUmbrellaID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]                BIGINT           NOT NULL,
    [ServiceID]                       INT              NOT NULL,
    [BusinessEntityUmbrellaID]        BIGINT           NOT NULL,
    [rowguid]                         UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityServiceUmbrella_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                     DATETIME         CONSTRAINT [DF_BusinessEntityServiceUmbrella_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                    DATETIME         CONSTRAINT [DF_StoreService_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                       VARCHAR (256)    NULL,
    [ModifiedBy]                      VARCHAR (256)    NULL,
    CONSTRAINT [PK_StoreService] PRIMARY KEY CLUSTERED ([BusinessEntityServiceUmbrellaID] ASC),
    CONSTRAINT [FK_BusinessServiceUmbrella_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessServiceUmbrella_BusinessEntity1] FOREIGN KEY ([BusinessEntityUmbrellaID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessServiceUmbrella_Service] FOREIGN KEY ([ServiceID]) REFERENCES [dbo].[Service] ([ServiceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_StoreService_BusinessService]
    ON [dbo].[BusinessEntityServiceUmbrella]([BusinessEntityID] ASC, [ServiceID] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The store business entity ID.  Not to be confused with the SourceStoreID that is derived from the third party data.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BusinessEntityServiceUmbrella', @level2type = N'COLUMN', @level2name = N'BusinessEntityID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Business level at which the store operates (i.e. as itself, a chain, a network, etc.).', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BusinessEntityServiceUmbrella', @level2type = N'COLUMN', @level2name = N'BusinessEntityUmbrellaID';

