﻿CREATE TABLE [dbo].[BusinessEntityFile] (
    [BusinessEntityFileID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]     BIGINT           NOT NULL,
    [FileID]               BIGINT           NOT NULL,
    [rowguid]              UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityFile_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]          DATETIME         CONSTRAINT [DF_BusinessEntityFile_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]         DATETIME         CONSTRAINT [DF_BusinessEntityFile_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BusinessEntityFile] PRIMARY KEY CLUSTERED ([BusinessEntityFileID] ASC),
    CONSTRAINT [FK_BusinessEntityFile_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntityFile_Files] FOREIGN KEY ([FileID]) REFERENCES [doc].[Files] ([FileID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityFile_rowguid]
    ON [dbo].[BusinessEntityFile]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityFile_BusinessFile]
    ON [dbo].[BusinessEntityFile]([BusinessEntityID] ASC, [FileID] ASC);

