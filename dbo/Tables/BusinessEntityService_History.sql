﻿CREATE TABLE [dbo].[BusinessEntityService_History] (
    [BusinessEntityServiceHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityServiceID]        BIGINT           NULL,
    [BusinessEntityID]               BIGINT           NULL,
    [ServiceID]                      INT              NULL,
    [AccountKey]                     VARCHAR (256)    NULL,
    [ConnectionStatusID]             INT              NULL,
    [GoLiveDate]                     DATETIME         NULL,
    [DateTermed]                     DATETIME         NULL,
    [HashKey]                        VARCHAR (256)    NULL,
    [rowguid]                        UNIQUEIDENTIFIER NULL,
    [DateCreated]                    DATETIME         NULL,
    [DateModified]                   DATETIME         NULL,
    [CreatedBy]                      VARCHAR (256)    NULL,
    [ModifiedBy]                     VARCHAR (256)    NULL,
    [AuditGuid]                      UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityService_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                    VARCHAR (10)     NULL,
    [DateAudited]                    DATETIME         CONSTRAINT [DF_BusinessEntityService_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BusinessEntityService_History] PRIMARY KEY CLUSTERED ([BusinessEntityServiceHistoryID] ASC)
);



