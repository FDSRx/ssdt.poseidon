﻿CREATE TABLE [dbo].[ApplicationType] (
    [ApplicationTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]              NVARCHAR (10)    NOT NULL,
    [Name]              NVARCHAR (50)    NOT NULL,
    [Description]       VARCHAR (1000)   NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_ApplicationType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_ApplicationType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_ApplicationType_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ApplicationType] PRIMARY KEY CLUSTERED ([ApplicationTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ApplicationType_Code]
    ON [dbo].[ApplicationType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ApplicationType_Name]
    ON [dbo].[ApplicationType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ApplicationType_rowguid]
    ON [dbo].[ApplicationType]([rowguid] ASC);

