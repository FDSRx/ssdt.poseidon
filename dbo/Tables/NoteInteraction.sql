﻿CREATE TABLE [dbo].[NoteInteraction] (
    [NoteInteractionID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [NoteID]            BIGINT           NOT NULL,
    [InteractionID]     BIGINT           NOT NULL,
    [CommentNoteID]     BIGINT           NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_NoteInteraction_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_NoteInteraction_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_NoteInteraction_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_NoteInteraction] PRIMARY KEY CLUSTERED ([NoteInteractionID] ASC),
    CONSTRAINT [FK_NoteInteraction_Comment] FOREIGN KEY ([CommentNoteID]) REFERENCES [dbo].[Note] ([NoteID]),
    CONSTRAINT [FK_NoteInteraction_Interaction] FOREIGN KEY ([InteractionID]) REFERENCES [dbo].[Interaction] ([InteractionID]),
    CONSTRAINT [FK_NoteInteraction_Note] FOREIGN KEY ([NoteID]) REFERENCES [dbo].[Note] ([NoteID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_NoteInteraction_rowguid]
    ON [dbo].[NoteInteraction]([rowguid] ASC);

