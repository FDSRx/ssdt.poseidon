﻿CREATE TABLE [dbo].[Origin] (
    [OriginID]     INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Namespace]    VARCHAR (256)    NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_DataSource_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_DataSource_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_DataSource_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Origin] PRIMARY KEY CLUSTERED ([OriginID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Origin_Code]
    ON [dbo].[Origin]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Origin_Name]
    ON [dbo].[Origin]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Origin_rowguid]
    ON [dbo].[Origin]([rowguid] ASC);

