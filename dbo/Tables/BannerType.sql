﻿CREATE TABLE [dbo].[BannerType] (
    [BannerTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_BannerType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_BannerType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_BannerType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_BannerType] PRIMARY KEY CLUSTERED ([BannerTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BannerType_Code]
    ON [dbo].[BannerType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BannerType_Name]
    ON [dbo].[BannerType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BannerType_rowguid]
    ON [dbo].[BannerType]([rowguid] ASC);

