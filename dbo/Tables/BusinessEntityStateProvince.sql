﻿CREATE TABLE [dbo].[BusinessEntityStateProvince] (
    [BusinessEntityStateProvinceID] INT              IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]              BIGINT           NOT NULL,
    [StateProvinceID]               INT              NOT NULL,
    [rowguid]                       UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityStateProvince_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                   DATETIME         CONSTRAINT [DF_BusinessEntityStateProvince_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                  DATETIME         CONSTRAINT [DF_BusinessEntityStateProvince_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BusinessEntityStateProvince] PRIMARY KEY CLUSTERED ([BusinessEntityStateProvinceID] ASC),
    CONSTRAINT [FK_BusinessEntityStateProvince_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntityStateProvince_StateProvince] FOREIGN KEY ([StateProvinceID]) REFERENCES [dbo].[StateProvince] ([StateProvinceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityStateProvince_rowguid]
    ON [dbo].[BusinessEntityStateProvince]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityStateProvince_BizState]
    ON [dbo].[BusinessEntityStateProvince]([BusinessEntityID] ASC, [StateProvinceID] ASC);

