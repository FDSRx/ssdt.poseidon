﻿CREATE TABLE [dbo].[NoteTag] (
    [NoteTagID]   BIGINT           IDENTITY (1, 1) NOT NULL,
    [NoteID]      BIGINT           NOT NULL,
    [TagID]       BIGINT           NOT NULL,
    [rowguid]     UNIQUEIDENTIFIER CONSTRAINT [DF_NoteTag_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated] DATETIME         CONSTRAINT [DF_NoteTag_DateCreated] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_NoteTag] PRIMARY KEY CLUSTERED ([NoteTagID] ASC),
    CONSTRAINT [FK_NoteTag_Note] FOREIGN KEY ([NoteID]) REFERENCES [dbo].[Note] ([NoteID]),
    CONSTRAINT [FK_NoteTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_NoteTag_NoteTag]
    ON [dbo].[NoteTag]([NoteID] ASC, [TagID] ASC);

