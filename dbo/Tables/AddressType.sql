﻿CREATE TABLE [dbo].[AddressType] (
    [AddressTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]          NVARCHAR (10)    NOT NULL,
    [Name]          NVARCHAR (50)    NOT NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_AddressType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_AddressType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_AddressType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_AddressType] PRIMARY KEY CLUSTERED ([AddressTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AddressType_Code]
    ON [dbo].[AddressType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AddressType_rowguid]
    ON [dbo].[AddressType]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AddressType_Name]
    ON [dbo].[AddressType]([Name] ASC);

