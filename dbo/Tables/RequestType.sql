﻿CREATE TABLE [dbo].[RequestType] (
    [RequestTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]          VARCHAR (25)     NULL,
    [Name]          VARCHAR (50)     NULL,
    [Description]   VARCHAR (1000)   NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_RequestType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_RequestType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_RequestType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_RequestType] PRIMARY KEY CLUSTERED ([RequestTypeID] ASC)
);

