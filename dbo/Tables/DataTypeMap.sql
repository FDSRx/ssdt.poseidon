﻿CREATE TABLE [dbo].[DataTypeMap] (
    [DataTypeMapID] INT              IDENTITY (1, 1) NOT NULL,
    [DataTypeID]    INT              NOT NULL,
    [DataSourceID]  INT              NOT NULL,
    [MapKey]        VARCHAR (256)    NOT NULL,
    [rowguid]       UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]   DATETIME         NOT NULL,
    [DateModified]  DATETIME         NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_DataTypeMap] PRIMARY KEY CLUSTERED ([DataTypeMapID] ASC),
    CONSTRAINT [FK_DataTypeMap_DataSource] FOREIGN KEY ([DataSourceID]) REFERENCES [dbo].[DataSource] ([DataSourceID]),
    CONSTRAINT [FK_DataTypeMap_DataType] FOREIGN KEY ([DataTypeID]) REFERENCES [dbo].[DataType] ([DataTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DataTypeMap_DataSourceMapKey]
    ON [dbo].[DataTypeMap]([DataSourceID] ASC, [MapKey] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DataTypeMap_rowguid]
    ON [dbo].[DataTypeMap]([rowguid] ASC);

