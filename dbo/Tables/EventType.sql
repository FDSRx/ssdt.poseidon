﻿CREATE TABLE [dbo].[EventType] (
    [EventTypeID]  INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_EventType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_EventType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_EventType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_EventType] PRIMARY KEY CLUSTERED ([EventTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EventType_Code]
    ON [dbo].[EventType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EventType_Name]
    ON [dbo].[EventType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EventType_rowguid]
    ON [dbo].[EventType]([rowguid] ASC);

