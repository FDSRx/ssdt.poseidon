﻿CREATE TABLE [dbo].[Days] (
    [DayID]        INT          NOT NULL,
    [Code]         VARCHAR (10) NOT NULL,
    [Name]         VARCHAR (10) NOT NULL,
    [Abbreviation] VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_Days] PRIMARY KEY CLUSTERED ([DayID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Days_Code]
    ON [dbo].[Days]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Days_Name]
    ON [dbo].[Days]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Days_Abbreviation]
    ON [dbo].[Days]([Abbreviation] ASC);

