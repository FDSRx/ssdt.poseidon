﻿CREATE TABLE [dbo].[BusinessEntityType] (
    [BusinessEntityTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]                 VARCHAR (10)     NOT NULL,
    [Name]                 VARCHAR (50)     NOT NULL,
    [rowguid]              UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]          DATETIME         CONSTRAINT [DF_BusinessEntityType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]         DATETIME         CONSTRAINT [DF_BusinessEntityType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]            VARCHAR (256)    NULL,
    [ModifiedBy]           VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessEntityType] PRIMARY KEY CLUSTERED ([BusinessEntityTypeID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityType_rowguid]
    ON [dbo].[BusinessEntityType]([rowguid] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityType_Code]
    ON [dbo].[BusinessEntityType]([Code] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityType_Name]
    ON [dbo].[BusinessEntityType]([Name] ASC) WITH (FILLFACTOR = 90);

