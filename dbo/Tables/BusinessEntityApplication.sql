﻿CREATE TABLE [dbo].[BusinessEntityApplication] (
    [BusinessEntityApplicationID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]            BIGINT           NOT NULL,
    [ApplicationID]               INT              NOT NULL,
    [ApplicationPath]             VARCHAR (512)    NULL,
    [rowguid]                     UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityApplication_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                 DATETIME         CONSTRAINT [DF_BusinessEntityApplication_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                DATETIME         CONSTRAINT [DF_BusinessEntityApplication_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                   VARCHAR (256)    NULL,
    [ModifiedBy]                  VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessEntityApplication] PRIMARY KEY CLUSTERED ([BusinessEntityApplicationID] ASC),
    CONSTRAINT [FK_BusinessEntityApplication_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_BusinessEntityApplication_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityApplication_BizApp]
    ON [dbo].[BusinessEntityApplication]([BusinessEntityID] ASC, [ApplicationID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityApplication_rowguid]
    ON [dbo].[BusinessEntityApplication]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/25/2016
-- Description:	Audits a record change of the BusinessEntityApplication table.

-- SELECT * FROM dbo.BusinessEntityApplication
-- SELECT * FROM dbo.BusinessEntityApplication_History

-- TRUNCATE TABLE dbo.BusinessEntityApplication_History
-- =============================================
CREATE TRIGGER [dbo].[trigBusinessEntityApplication_Audit]
   ON  [dbo].BusinessEntityApplication
   AFTER DELETE, UPDATE
AS 
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------------
	-- Short circut audit if no records.
	------------------------------------------------------------------------------------------------------------------
	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;

	------------------------------------------------------------------------------------------------------------------	
	-- Determine type of operation being performed (i.e. create, update, delete, etc.).
	------------------------------------------------------------------------------------------------------------------
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	------------------------------------------------------------------------------------------------------------------
	-- Retrieve session data.
	------------------------------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
			
	------------------------------------------------------------------------------------------------------------------	
	-- Audit object.
	------------------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.BusinessEntityApplication_History (
		BusinessEntityApplicationID,
		BusinessEntityID,
		ApplicationID,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityApplicationID,
		BusinessEntityID,
		ApplicationID,
		rowguid,
		DateCreated,
		DateModified,	
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	-- Data review.
	-- SELECT * FROM dbo.BusinessEntityApplication
	-- SELECT * FROM dbo.BusinessEntityApplication_History
	
	

END
