﻿CREATE TABLE [dbo].[BusinessEntityApplication_History] (
    [BusinessEntityApplicationHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityApplicationID]        BIGINT           NULL,
    [BusinessEntityID]                   BIGINT           NULL,
    [ApplicationID]                      INT              NULL,
    [rowguid]                            UNIQUEIDENTIFIER NULL,
    [DateCreated]                        DATETIME         NULL,
    [DateModified]                       DATETIME         NULL,
    [CreatedBy]                          VARCHAR (256)    NULL,
    [ModifiedBy]                         VARCHAR (256)    NULL,
    [AuditGuid]                          UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityApplication_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                        VARCHAR (10)     NULL,
    [DateAudited]                        DATETIME         CONSTRAINT [DF_BusinessEntityApplication_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BusinessEntityApplication_History] PRIMARY KEY CLUSTERED ([BusinessEntityApplicationHistoryID] ASC)
);

