﻿CREATE TABLE [dbo].[PersonType] (
    [PersonTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]         NVARCHAR (10)    NOT NULL,
    [Name]         NVARCHAR (50)    NOT NULL,
    [Description]  NVARCHAR (255)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_PersonType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Table_1_DateModified] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_PersonType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_PersonType] PRIMARY KEY CLUSTERED ([PersonTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PersonType_Code]
    ON [dbo].[PersonType]([Code] ASC);


GO
CREATE NONCLUSTERED INDEX [UIX_PersonType_Name]
    ON [dbo].[PersonType]([Name] ASC);

