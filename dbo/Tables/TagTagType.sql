﻿CREATE TABLE [dbo].[TagTagType] (
    [TagTagTypeID]   BIGINT           IDENTITY (1, 1) NOT NULL,
    [TagID]          BIGINT           NOT NULL,
    [TagTypeID]      INT              NOT NULL,
    [CorrelationKey] VARCHAR (256)    NULL,
    [rowguid]        UNIQUEIDENTIFIER CONSTRAINT [DF_TagTagType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]    DATETIME         CONSTRAINT [DF_TagTagType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]   DATETIME         CONSTRAINT [DF_TagTagType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      VARCHAR (256)    NULL,
    [ModifiedBy]     VARCHAR (256)    NULL,
    CONSTRAINT [PK_TagTagType] PRIMARY KEY CLUSTERED ([TagTagTypeID] ASC),
    CONSTRAINT [FK_TagTagType_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]),
    CONSTRAINT [FK_TagTagType_TagType] FOREIGN KEY ([TagTypeID]) REFERENCES [dbo].[TagType] ([TagTypeID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TagTagType_UniqueRow]
    ON [dbo].[TagTagType]([TagID] ASC, [TagTypeID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TagTagType_rowguid]
    ON [dbo].[TagTagType]([rowguid] ASC);

