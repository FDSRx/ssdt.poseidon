﻿CREATE TABLE [dbo].[Interaction] (
    [InteractionID]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [InteractionTypeID]    INT              NOT NULL,
    [DispositionTypeID]    INT              NOT NULL,
    [InitiatedByID]        BIGINT           NULL,
    [InitiatedByString]    VARCHAR (256)    NULL,
    [InteractedWithID]     BIGINT           NULL,
    [InteractedWithString] VARCHAR (256)    NULL,
    [ContactTypeID]        INT              NULL,
    [CommentNoteID]        BIGINT           NULL,
    [rowguid]              UNIQUEIDENTIFIER CONSTRAINT [DF_Interaction_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]          DATETIME         CONSTRAINT [DF_Interaction_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]         DATETIME         CONSTRAINT [DF_Interaction_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]            VARCHAR (256)    NULL,
    [ModifiedBy]           VARCHAR (256)    NULL,
    CONSTRAINT [PK_Interaction] PRIMARY KEY CLUSTERED ([InteractionID] ASC),
    CONSTRAINT [FK_Interaction_ByID] FOREIGN KEY ([InitiatedByID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Interaction_ContactType] FOREIGN KEY ([ContactTypeID]) REFERENCES [dbo].[ContactType] ([ContactTypeID]),
    CONSTRAINT [FK_Interaction_DispositionType] FOREIGN KEY ([DispositionTypeID]) REFERENCES [dbo].[DispositionType] ([DispositionTypeID]),
    CONSTRAINT [FK_Interaction_InteractionType] FOREIGN KEY ([InteractionTypeID]) REFERENCES [dbo].[InteractionType] ([InteractionTypeID]),
    CONSTRAINT [FK_Interaction_WithID] FOREIGN KEY ([InteractedWithID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Interaction_rowguid]
    ON [dbo].[Interaction]([rowguid] ASC);

