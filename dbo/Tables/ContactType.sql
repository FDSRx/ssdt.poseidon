﻿CREATE TABLE [dbo].[ContactType] (
    [ContactTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]          VARCHAR (25)     NOT NULL,
    [Name]          VARCHAR (50)     NOT NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_ContactType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_ContactType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_ContactType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_ContactType] PRIMARY KEY CLUSTERED ([ContactTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ContactType_Code]
    ON [dbo].[ContactType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ContactType_Name]
    ON [dbo].[ContactType]([Name] ASC);

