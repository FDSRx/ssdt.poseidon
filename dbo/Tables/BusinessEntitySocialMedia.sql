﻿CREATE TABLE [dbo].[BusinessEntitySocialMedia] (
    [BusinessEntitySocialMediaID] INT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]            BIGINT        NOT NULL,
    [SocialMediaID]               INT           NOT NULL,
    [Url]                         VARCHAR (255) NOT NULL,
    [MediaKey]                    VARCHAR (255) NULL,
    [DateCreated]                 DATETIME      CONSTRAINT [DF_BusinessEntitySocialMedia_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                DATETIME      CONSTRAINT [DF_BusinessEntitySocialMedia_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BusinessEntitySocialMedia] PRIMARY KEY CLUSTERED ([BusinessEntitySocialMediaID] ASC),
    CONSTRAINT [FK_BusinessEntitySocialMedia_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntitySocialMedia_SocialMedia] FOREIGN KEY ([SocialMediaID]) REFERENCES [dbo].[SocialMedia] ([SocialMediaID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntitySocialMedia_BusinessSocial]
    ON [dbo].[BusinessEntitySocialMedia]([BusinessEntityID] ASC, [SocialMediaID] ASC);

