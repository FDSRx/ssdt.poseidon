﻿CREATE TABLE [dbo].[PersonMessage] (
    [PersonMessageID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [PersonID]        BIGINT           NOT NULL,
    [ServiceID]       INT              NOT NULL,
    [FolderID]        INT              NOT NULL,
    [MessageID]       BIGINT           NOT NULL,
    [IsRead]          BIT              CONSTRAINT [DF_PersonMessage_IsRead] DEFAULT ((0)) NOT NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_PersonMessage_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsStarred]       BIT              CONSTRAINT [DF_PersonMessage_IsStarred] DEFAULT ((0)) NOT NULL,
    [CanRespond]      BIT              CONSTRAINT [DF_PersonMessage_CanRespond] DEFAULT ((0)) NOT NULL,
    [IsConversation]  BIT              CONSTRAINT [DF_PersonMessage_IsConversation] DEFAULT ((0)) NOT NULL,
    [ThreadKey]       UNIQUEIDENTIFIER CONSTRAINT [DF_PersonMessage_ConversationID] DEFAULT (newid()) NOT NULL,
    [RoutingData]     XML              NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_PersonMessage_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_PersonMessage_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_PersonMessage_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PersonMessage] PRIMARY KEY CLUSTERED ([PersonMessageID] ASC),
    CONSTRAINT [FK_PersonMessage_Folder] FOREIGN KEY ([FolderID]) REFERENCES [msg].[Folder] ([FolderID]),
    CONSTRAINT [FK_PersonMessage_Message] FOREIGN KEY ([MessageID]) REFERENCES [msg].[Message] ([MessageID]),
    CONSTRAINT [FK_PersonMessage_Person] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([BusinessEntityID]),
    CONSTRAINT [FK_PersonMessage_Service] FOREIGN KEY ([ServiceID]) REFERENCES [dbo].[Service] ([ServiceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PersonMessage_rowguid]
    ON [dbo].[PersonMessage]([rowguid] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PersonMessage_Person]
    ON [dbo].[PersonMessage]([PersonID] ASC);


GO
CREATE NONCLUSTERED INDEX [UIX_PersonMessage_TheadKey]
    ON [dbo].[PersonMessage]([ThreadKey] ASC);

