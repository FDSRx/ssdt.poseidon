﻿CREATE TABLE [dbo].[Language] (
    [LanguageID]   INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (5)      NULL,
    [Name]         NVARCHAR (50)    NULL,
    [ISO_639]      NVARCHAR (5)     NULL,
    [ISO_639_1]    NVARCHAR (5)     NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Language_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Language_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Language_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED ([LanguageID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Language_Code]
    ON [dbo].[Language]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Language_Name]
    ON [dbo].[Language]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Language_rowguid]
    ON [dbo].[Language]([rowguid] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Only using neutral (2 character) codes.  Names should be unique.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Language', @level2type = N'INDEX', @level2name = N'UIX_Language_Name';

