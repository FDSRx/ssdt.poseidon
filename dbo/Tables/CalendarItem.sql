﻿CREATE TABLE [dbo].[CalendarItem] (
    [NoteID]           BIGINT           NOT NULL,
    [ApplicationID]    INT              NULL,
    [BusinessID]       BIGINT           NULL,
    [BusinessEntityID] BIGINT           NULL,
    [Location]         VARCHAR (500)    NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_CalendarItem_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_CalendarItem_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_CalendarItem_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_CalendarItem] PRIMARY KEY CLUSTERED ([NoteID] ASC),
    CONSTRAINT [FK_CalendarItem_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_CalendarItem_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_CalendarItem_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_CalendarItem_Note] FOREIGN KEY ([NoteID]) REFERENCES [dbo].[Note] ([NoteID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CalendarItem_rowguid]
    ON [dbo].[CalendarItem]([rowguid] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CalendarItem_Business]
    ON [dbo].[CalendarItem]([BusinessID] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/25/2014
-- Description:	Audits a record change of the CalendarItem table
-- SELECT * FROM dbo.CalendarItem_History
-- TRUNCATE TABLE dbo.CalendarItem_History
-- =============================================
CREATE TRIGGER [dbo].[trigCalendarItem_Audit]
   ON  [dbo].[CalendarItem]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
	
	-----------------------------------------------------------------------------	
	-- Audit inserted records (updated or newly created)
	-----------------------------------------------------------------------------
	INSERT INTO dbo.CalendarItem_History (
		NoteID,	
		ApplicationID,
		BusinessID,
		BusinessEntityID,
		Location,	
		rowguid,	
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		NoteID,	
		ApplicationID,
		BusinessID,
		BusinessEntityID,
		Location,
		rowguid,	
		DateCreated,
		DateModified,
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_NAME()
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	--SELECT * FROM dbo.CalendarItem
	
	

END
