﻿CREATE TABLE [dbo].[BusinessEntityEmailAddress] (
    [BusinessEntityEmailAddressID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]             BIGINT           NOT NULL,
    [EmailAddress]                 NVARCHAR (256)   NOT NULL,
    [EmailAddressTypeID]           INT              NOT NULL,
    [IsEmailAllowed]               BIT              CONSTRAINT [DF_BusinessEntityEmailAddress_IsEmailAllowed] DEFAULT ((0)) NOT NULL,
    [HashKey]                      VARCHAR (256)    NULL,
    [rowguid]                      UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityEmailAddress_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                  DATETIME         CONSTRAINT [DF_BusinessEntityEmailAddress_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                 DATETIME         CONSTRAINT [DF_BusinessEntityEmailAddress_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                    VARCHAR (256)    NULL,
    [ModifiedBy]                   VARCHAR (256)    NULL,
    [IsPreferred]                  BIT              CONSTRAINT [DF_BusinessEntityEmailAddress_IsPreferred] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_BusinessEntityEmailAddress] PRIMARY KEY CLUSTERED ([BusinessEntityEmailAddressID] ASC),
    CONSTRAINT [FK_BusinessEntityEmailAddress_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntityEmailAddress_EmailAddressType] FOREIGN KEY ([EmailAddressTypeID]) REFERENCES [dbo].[EmailAddressType] ([EmailAddressTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityEmailAddress_BizEmailType]
    ON [dbo].[BusinessEntityEmailAddress]([BusinessEntityID] ASC, [EmailAddress] ASC, [EmailAddressTypeID] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/2/2013
-- Description:	Audits a record change of the business entity email address table
-- SELECT * FROM dbo.BusinessEntityEmailAddress_History
-- SELECT * FROM dbo.BusinessEntityEmailAddress_History WHERE BusinessEntityID = 59201
-- TRUNCATE TABLE dbo.BusinessEntityEmailAddress_History
-- =============================================
CREATE TRIGGER [dbo].[trigBusinessEntityEmailAddress_Audit]
   ON  [dbo].[BusinessEntityEmailAddress]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM deleted) = 0 -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
		

	-----------------------------------------------------------------------------
	-- Audit transaction
	-----------------------------------------------------------------------------				
	-- Audit inserted records (updated or newly created)
	INSERT INTO dbo.BusinessEntityEmailAddress_History (
		BusinessEntityEmailAddressID,	
		BusinessEntityID,	
		EmailAddress,	
		EmailAddressTypeID,	
		IsEmailAllowed,	
		IsPreferred,
		HashKey,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,	
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityEmailAddressID,	
		BusinessEntityID,	
		EmailAddress,	
		EmailAddressTypeID,	
		IsEmailAllowed,	
		IsPreferred,
		HashKey,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,		
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d
	
	--SELECT * FROM dbo.BusinessEntityEmailAddress
	
	

END
