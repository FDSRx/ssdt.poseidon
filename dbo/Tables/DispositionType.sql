﻿CREATE TABLE [dbo].[DispositionType] (
    [DispositionTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]              VARCHAR (25)     NOT NULL,
    [Name]              VARCHAR (25)     NOT NULL,
    [rowguid]           UNIQUEIDENTIFIER CONSTRAINT [DF_DispositionType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]       DATETIME         CONSTRAINT [DF_DispositionType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME         CONSTRAINT [DF_DispositionType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         VARCHAR (256)    NULL,
    [ModifiedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_DispositionType] PRIMARY KEY CLUSTERED ([DispositionTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DispositionType_Code]
    ON [dbo].[DispositionType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DispositionType_Name]
    ON [dbo].[DispositionType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DispositionType_rowguid]
    ON [dbo].[DispositionType]([rowguid] ASC);

