﻿CREATE TABLE [dbo].[BusinessEntityContact_History] (
    [BusinessEntityContactHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityContactID]        BIGINT           NULL,
    [BusinessEntityID]               BIGINT           NULL,
    [PersonID]                       BIGINT           NULL,
    [ContactTypeID]                  INT              NULL,
    [rowguid]                        UNIQUEIDENTIFIER NULL,
    [DateCreated]                    DATETIME         NULL,
    [DateModified]                   DATETIME         NULL,
    [CreatedBy]                      VARCHAR (256)    NULL,
    [ModifiedBy]                     VARCHAR (256)    NULL,
    [AuditGuid]                      UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityContact_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                    VARCHAR (10)     NULL,
    [DateAudited]                    DATETIME         CONSTRAINT [DF_BusinessEntityContact_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BusinessEntityContact_History] PRIMARY KEY CLUSTERED ([BusinessEntityContactHistoryID] ASC)
);

