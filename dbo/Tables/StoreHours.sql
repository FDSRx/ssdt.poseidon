﻿CREATE TABLE [dbo].[StoreHours] (
    [StoreHourID]  INT              IDENTITY (1, 1) NOT NULL,
    [StoreID]      BIGINT           NOT NULL,
    [HourTypeID]   INT              NOT NULL,
    [DayID]        INT              NOT NULL,
    [TimeStart]    TIME (7)         NULL,
    [TimeEnd]      TIME (7)         NULL,
    [TimeText]     VARCHAR (255)    NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_StoreHours_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_StoreHours_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_StoreHours_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_StoreHours] PRIMARY KEY CLUSTERED ([StoreHourID] ASC),
    CONSTRAINT [FK_StoreHours_Days] FOREIGN KEY ([DayID]) REFERENCES [dbo].[Days] ([DayID]),
    CONSTRAINT [FK_StoreHours_HourType] FOREIGN KEY ([HourTypeID]) REFERENCES [dbo].[HourType] ([HourTypeID]),
    CONSTRAINT [FK_StoreHours_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_StoreHours_rowguid]
    ON [dbo].[StoreHours]([rowguid] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_StoreHours_StoreHourType]
    ON [dbo].[StoreHours]([StoreID] ASC, [HourTypeID] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 3/1/2016
-- Description:	Audits a record change of the StoreHours table.

-- SELECT * FROM dbo.StoreHours
-- SELECT * FROM dbo.StoreHours_History

-- TRUNCATE TABLE dbo.StoreHours_History
-- =============================================
CREATE TRIGGER [dbo].[trigStoreHours_Audit]
   ON  [dbo].StoreHours
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------------
	-- Short circut audit if no records.
	------------------------------------------------------------------------------------------------------------------
	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;

	------------------------------------------------------------------------------------------------------------------	
	-- Determine type of operation being performed (i.e. create, update, delete, etc.).
	------------------------------------------------------------------------------------------------------------------
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	------------------------------------------------------------------------------------------------------------------
	-- Retrieve session data
	------------------------------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
			
	------------------------------------------------------------------------------------------------------------------	
	-- Audit inserted records (updated or newly created)
	------------------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.StoreHours_History (
		StoreHourID,
		StoreID,
		HourTypeID,
		DayID,
		TimeStart,
		TimeEnd,
		TimeText,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		StoreHourID,
		StoreID,
		HourTypeID,
		DayID,
		TimeStart,
		TimeEnd,
		TimeText,
		rowguid,
		DateCreated,
		DateModified,	
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	-- Data review.
	-- SELECT * FROM dbo.StoreHours
	-- SELECT * FROM dbo.StoreHours_History
	
	

END
