﻿CREATE TABLE [dbo].[RequestLog] (
    [RequestLogID]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [RequestTypeID]    INT              NOT NULL,
    [SourceName]       VARCHAR (256)    NULL,
    [ApplicationKey]   VARCHAR (50)     NULL,
    [BusinessKey]      VARCHAR (50)     NULL,
    [RequesterKey]     VARCHAR (256)    NULL,
    [ApplicationID]    INT              NULL,
    [BusinessEntityID] BIGINT           NULL,
    [RequesterID]      BIGINT           NULL,
    [RequestData]      VARCHAR (MAX)    NULL,
    [Arguments]        VARCHAR (MAX)    NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_RequestLog_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_RequestLog_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_RequestLog_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    CONSTRAINT [PK_RequestLog] PRIMARY KEY CLUSTERED ([RequestLogID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_RequestLog_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_RequestLog_Business] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_RequestLog_BusinessEntity] FOREIGN KEY ([RequesterID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_RequestLog_RequestType] FOREIGN KEY ([RequestTypeID]) REFERENCES [dbo].[RequestType] ([RequestTypeID])
);

