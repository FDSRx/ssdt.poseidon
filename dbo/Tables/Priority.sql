﻿CREATE TABLE [dbo].[Priority] (
    [PriorityID]   INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Priority_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Priority_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Priority_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Priority] PRIMARY KEY CLUSTERED ([PriorityID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Priority_Code]
    ON [dbo].[Priority]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Priority_Name]
    ON [dbo].[Priority]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Priority_rowguid]
    ON [dbo].[Priority]([rowguid] ASC);

