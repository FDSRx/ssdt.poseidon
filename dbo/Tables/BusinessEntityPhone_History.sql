﻿CREATE TABLE [dbo].[BusinessEntityPhone_History] (
    [BusinessEntityPhoneNumberHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityPhoneNumberID]        BIGINT           NOT NULL,
    [BusinessEntityID]                   BIGINT           NOT NULL,
    [PhoneNumber]                        NVARCHAR (25)    NULL,
    [PhoneNumberTypeID]                  INT              NOT NULL,
    [IsCallAllowed]                      BIT              NULL,
    [IsTextAllowed]                      BIT              NULL,
    [HashKey]                            VARCHAR (256)    NULL,
    [rowguid]                            UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]                        DATETIME         NOT NULL,
    [DateModified]                       DATETIME         NOT NULL,
    [CreatedBy]                          VARCHAR (256)    NULL,
    [ModifiedBy]                         VARCHAR (256)    NULL,
    [AuditGuid]                          UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityPhone_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                        VARCHAR (10)     NOT NULL,
    [DateAudited]                        DATETIME         CONSTRAINT [DF_BusinessEntityPhone_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    [IsPreferred]                        BIT              CONSTRAINT [DF_BusinessEntityPhone_History_IsPreferred] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_BusinessEntityPhone_History] PRIMARY KEY CLUSTERED ([BusinessEntityPhoneNumberHistoryID] ASC)
);

