﻿CREATE TABLE [dbo].[Notebook] (
    [NotebookID]  INT              IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (25)     NULL,
    [Name]        VARCHAR (500)    NOT NULL,
    [rowguid]     UNIQUEIDENTIFIER CONSTRAINT [DF_Notebook_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated] DATETIME         CONSTRAINT [DF_Notebook_DateCreated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Notebook] PRIMARY KEY CLUSTERED ([NotebookID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Notebook_rowguid]
    ON [dbo].[Notebook]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Notebook_Name]
    ON [dbo].[Notebook]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Notebook_Code]
    ON [dbo].[Notebook]([Code] ASC);

