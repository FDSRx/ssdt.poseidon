﻿CREATE TABLE [dbo].[TokenType] (
    [TokenTypeID]  INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_TokenType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_TokenType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_TokenType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_TokenType] PRIMARY KEY CLUSTERED ([TokenTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TokenType_Code]
    ON [dbo].[TokenType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TokenType_Name]
    ON [dbo].[TokenType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TokenType_rowguid]
    ON [dbo].[TokenType]([rowguid] ASC);

