﻿CREATE TABLE [dbo].[Preference] (
    [PreferenceID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Preference_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Preference_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Preference_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Preference] PRIMARY KEY CLUSTERED ([PreferenceID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Preference_Code]
    ON [dbo].[Preference]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Preference_Name]
    ON [dbo].[Preference]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Preference_rowguid]
    ON [dbo].[Preference]([rowguid] ASC);

