﻿CREATE TABLE [dbo].[BusinessEntityAddress_History] (
    [BusinessEntityAddressHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityAddressID]        BIGINT           NOT NULL,
    [BusinessEntityID]               BIGINT           NOT NULL,
    [AddressID]                      BIGINT           NOT NULL,
    [AddressTypeID]                  INT              NOT NULL,
    [rowguid]                        UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]                    DATETIME         NOT NULL,
    [DateModified]                   DATETIME         NOT NULL,
    [CreatedBy]                      VARCHAR (256)    NULL,
    [ModifiedBy]                     VARCHAR (256)    NULL,
    [AuditGuid]                      UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityAddress_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                    VARCHAR (10)     NOT NULL,
    [DateAudited]                    DATETIME         CONSTRAINT [DF_BusinessEntityAddress_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BusinessEntityAddress_History] PRIMARY KEY CLUSTERED ([BusinessEntityAddressHistoryID] ASC)
);

