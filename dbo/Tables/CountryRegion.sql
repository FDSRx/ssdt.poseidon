﻿CREATE TABLE [dbo].[CountryRegion] (
    [CountryRegionCode] NVARCHAR (5)  NOT NULL,
    [Name]              NVARCHAR (50) NOT NULL,
    [DateCreated]       DATETIME      CONSTRAINT [DF_CountryRegion_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]      DATETIME      CONSTRAINT [DF_CountryRegion_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CountryRegion] PRIMARY KEY CLUSTERED ([CountryRegionCode] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_CountryRegion_Name]
    ON [dbo].[CountryRegion]([Name] ASC);

