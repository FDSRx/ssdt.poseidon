﻿CREATE TABLE [dbo].[ChainStore] (
    [ChainStoreID] INT              IDENTITY (1, 1) NOT NULL,
    [ChainID]      BIGINT           NOT NULL,
    [StoreID]      BIGINT           NOT NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_ChainStore_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_ChainStore_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_ChainStore_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_ChainStore] PRIMARY KEY CLUSTERED ([ChainStoreID] ASC),
    CONSTRAINT [FK_ChainStore_Chain] FOREIGN KEY ([ChainID]) REFERENCES [dbo].[Chain] ([BusinessEntityID]),
    CONSTRAINT [FK_ChainStore_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ChainStore_Store]
    ON [dbo].[ChainStore]([StoreID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ChainStore_rowguid]
    ON [dbo].[ChainStore]([rowguid] ASC);

