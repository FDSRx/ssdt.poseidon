﻿CREATE TABLE [dbo].[TaskStatus] (
    [TaskStatusID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_TaskStatus_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_TaskStatus_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_TaskStatus_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_TaskStatus] PRIMARY KEY CLUSTERED ([TaskStatusID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TaskStatus_Code]
    ON [dbo].[TaskStatus]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TaskStatus_Name]
    ON [dbo].[TaskStatus]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TaskStatus_rowguid]
    ON [dbo].[TaskStatus]([rowguid] ASC);

