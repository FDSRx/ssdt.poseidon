﻿CREATE TABLE [dbo].[Address] (
    [AddressID]       BIGINT            IDENTITY (1, 1) NOT NULL,
    [AddressLine1]    NVARCHAR (100)    NULL,
    [AddressLine2]    NVARCHAR (100)    NULL,
    [City]            NVARCHAR (50)     NULL,
    [StateProvinceID] INT               NULL,
    [PostalCode]      NVARCHAR (15)     NULL,
    [SpatialLocation] [sys].[geography] NULL,
    [Latitude]        DECIMAL (9, 6)    NULL,
    [Longitude]       DECIMAL (9, 6)    NULL,
    [HashKey]         VARCHAR (256)     NULL,
    [rowguid]         UNIQUEIDENTIFIER  CONSTRAINT [DF_Address_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME          CONSTRAINT [DF_Address_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME          CONSTRAINT [DF_Address_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)     NULL,
    [ModifiedBy]      VARCHAR (256)     NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED ([AddressID] ASC),
    CONSTRAINT [FK_Address_StateProvince] FOREIGN KEY ([StateProvinceID]) REFERENCES [dbo].[StateProvince] ([StateProvinceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Address]
    ON [dbo].[Address]([AddressLine1] ASC, [AddressLine2] ASC, [City] ASC, [StateProvinceID] ASC, [PostalCode] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Address_rowguid]
    ON [dbo].[Address]([rowguid] ASC);

