﻿CREATE TABLE [dbo].[DataCatalog] (
    [DataCatalogID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]          NVARCHAR (10)    NOT NULL,
    [Name]          NVARCHAR (50)    NOT NULL,
    [Description]   NVARCHAR (255)   NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_DataCatalog_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_DataCatalog_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_DataCatalog_DateModified] DEFAULT (getdate()) NOT NULL,
    [DataServerID]  INT              NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_Database] PRIMARY KEY CLUSTERED ([DataCatalogID] ASC),
    CONSTRAINT [FK_DataCatalog_DataServer] FOREIGN KEY ([DataServerID]) REFERENCES [dbo].[DataServer] ([DataServerID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DataCatalog_Code]
    ON [dbo].[DataCatalog]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DataCatalog_Name]
    ON [dbo].[DataCatalog]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DataCatalog_rowguid]
    ON [dbo].[DataCatalog]([rowguid] ASC);

