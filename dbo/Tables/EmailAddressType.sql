﻿CREATE TABLE [dbo].[EmailAddressType] (
    [EmailAddressTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [Code]               NVARCHAR (10) NOT NULL,
    [Name]               NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_EmailAddressType] PRIMARY KEY CLUSTERED ([EmailAddressTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EmailAddressType_Code]
    ON [dbo].[EmailAddressType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EmailAddressType_Name]
    ON [dbo].[EmailAddressType]([Name] ASC);

