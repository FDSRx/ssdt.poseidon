﻿CREATE TABLE [dbo].[BusinessEntityContact] (
    [BusinessEntityContactID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]        BIGINT           NOT NULL,
    [PersonID]                BIGINT           NOT NULL,
    [ContactTypeID]           INT              NOT NULL,
    [rowguid]                 UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityContact_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]             DATETIME         CONSTRAINT [DF_BusinessEntityContact_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]            DATETIME         CONSTRAINT [DF_BusinessEntityContact_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]               VARCHAR (256)    NULL,
    [ModifiedBy]              VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessEntityContact] PRIMARY KEY CLUSTERED ([BusinessEntityContactID] ASC),
    CONSTRAINT [FK_BusinessEntityContact_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntityContact_ContactType] FOREIGN KEY ([ContactTypeID]) REFERENCES [dbo].[ContactType] ([ContactTypeID]),
    CONSTRAINT [FK_BusinessEntityContact_Person] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityContact_BizPersonType]
    ON [dbo].[BusinessEntityContact]([BusinessEntityID] ASC, [PersonID] ASC, [ContactTypeID] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/24/2016
-- Description:	Audits a record change of the BusinessEntityContact table.

-- SELECT * FROM dbo.BusinessEntityContact
-- SELECT * FROM dbo.BusinessEntityContact_History

-- TRUNCATE TABLE dbo.BusinessEntityContact_History
-- =============================================
CREATE TRIGGER [dbo].[trigBusinessEntityContact_Audit]
   ON  [dbo].BusinessEntityContact
   AFTER DELETE, UPDATE
AS 
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------------
	-- Short circut audit if no records.
	------------------------------------------------------------------------------------------------------------------
	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;

	------------------------------------------------------------------------------------------------------------------	
	-- Determine type of operation being performed (i.e. create, update, delete, etc.).
	------------------------------------------------------------------------------------------------------------------
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	------------------------------------------------------------------------------------------------------------------
	-- Retrieve session data.
	------------------------------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
			
	------------------------------------------------------------------------------------------------------------------	
	-- Audit inserted records (updated or deleted).
	------------------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.BusinessEntityContact_History (
		BusinessEntityContactID,
		BusinessEntityID,
		PersonID,
		ContactTypeID,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityContactID,
		BusinessEntityID,
		PersonID,
		ContactTypeID,
		rowguid,
		DateCreated,
		DateModified,	
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	-- Data review.
	-- SELECT * FROM dbo.BusinessEntityContact
	-- SELECT * FROM dbo.BusinessEntityContact_History
	
	

END
