﻿CREATE TABLE [dbo].[Images] (
    [ImageID]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]  INT              NULL,
    [BusinessID]     BIGINT           NULL,
    [PersonID]       BIGINT           NULL,
    [KeyName]        VARCHAR (256)    NOT NULL,
    [Name]           VARCHAR (256)    NULL,
    [Caption]        VARCHAR (4000)   NULL,
    [Description]    VARCHAR (MAX)    NULL,
    [FileName]       VARCHAR (256)    NULL,
    [FilePath]       VARCHAR (1000)   NULL,
    [FileDataString] VARCHAR (MAX)    NULL,
    [FileDataBinary] VARBINARY (MAX)  NULL,
    [LanguageID]     INT              NOT NULL,
    [rowguid]        UNIQUEIDENTIFIER CONSTRAINT [DF_Images_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]    DATETIME         CONSTRAINT [DF_Images_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]   DATETIME         CONSTRAINT [DF_Images_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      VARCHAR (256)    NULL,
    [ModifiedBy]     VARCHAR (256)    NULL,
    CONSTRAINT [PK_Images] PRIMARY KEY CLUSTERED ([ImageID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Images_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_Images_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_Images_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_Images_Person] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([BusinessEntityID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Images_AppBizPrsnImgKey]
    ON [dbo].[Images]([ApplicationID] ASC, [BusinessID] ASC, [PersonID] ASC, [KeyName] ASC, [LanguageID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Images_rowguid]
    ON [dbo].[Images]([rowguid] ASC) WITH (FILLFACTOR = 90);

