﻿CREATE TABLE [dbo].[BusinessEntity] (
    [BusinessEntityID]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityTypeID] INT              NOT NULL,
    [rowguid]              UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntity_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]          DATETIME         CONSTRAINT [DF_BusinessEntity_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]         DATETIME         CONSTRAINT [DF_BusinessEntity_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]            VARCHAR (256)    NULL,
    [ModifiedBy]           VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessEntity] PRIMARY KEY CLUSTERED ([BusinessEntityID] ASC),
    CONSTRAINT [FK_BusinessEntity_BusinessEntityType] FOREIGN KEY ([BusinessEntityTypeID]) REFERENCES [dbo].[BusinessEntityType] ([BusinessEntityTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntity_rowguid]
    ON [dbo].[BusinessEntity]([rowguid] ASC);

