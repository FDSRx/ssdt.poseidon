﻿CREATE TABLE [dbo].[FinancialClass] (
    [FinancialClassID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]             VARCHAR (25)     NOT NULL,
    [Description]      VARCHAR (256)    NOT NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_FinancialClass_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_FinancialClass_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_FinancialClass_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_FinancialClass] PRIMARY KEY CLUSTERED ([FinancialClassID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FinancialClass_Code]
    ON [dbo].[FinancialClass]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FinancialClass_rowguid]
    ON [dbo].[FinancialClass]([rowguid] ASC);

