﻿CREATE TABLE [dbo].[Configuration_History] (
    [ConfigurationHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ConfigurationID]        BIGINT           NOT NULL,
    [ApplicationID]          INT              NULL,
    [BusinessEntityID]       BIGINT           NULL,
    [KeyName]                VARCHAR (255)    NOT NULL,
    [KeyValue]               VARCHAR (MAX)    NOT NULL,
    [DataTypeID]             INT              NOT NULL,
    [DataSize]               INT              NULL,
    [Description]            VARCHAR (1000)   NULL,
    [rowguid]                UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]            DATETIME         NOT NULL,
    [DateModified]           DATETIME         NOT NULL,
    [CreatedBy]              VARCHAR (256)    NULL,
    [ModifiedBy]             VARCHAR (256)    NULL,
    [AuditGuid]              UNIQUEIDENTIFIER CONSTRAINT [DF_Configuration_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]            VARCHAR (10)     NULL,
    [DateAudited]            DATETIME         CONSTRAINT [DF_Configuration_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Configuration_History] PRIMARY KEY CLUSTERED ([ConfigurationHistoryID] ASC)
);

