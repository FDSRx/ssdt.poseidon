﻿CREATE TABLE [dbo].[LogEntryType] (
    [LogEntryTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]           VARCHAR (25)     NOT NULL,
    [Name]           VARCHAR (50)     NOT NULL,
    [Description]    VARCHAR (1000)   NULL,
    [rowguid]        UNIQUEIDENTIFIER CONSTRAINT [DF_LogEntryType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]    DATETIME         CONSTRAINT [DF_LogEntryType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]   DATETIME         CONSTRAINT [DF_LogEntryType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      VARCHAR (256)    NULL,
    [ModifiedBy]     VARCHAR (256)    NULL,
    CONSTRAINT [PK_LogEntryType] PRIMARY KEY CLUSTERED ([LogEntryTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LogEntryType_Code]
    ON [dbo].[LogEntryType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LogEntryType_Name]
    ON [dbo].[LogEntryType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LogEntryType_rowguid]
    ON [dbo].[LogEntryType]([rowguid] ASC);

