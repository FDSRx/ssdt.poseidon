﻿CREATE TABLE [dbo].[HolidayType] (
    [HolidayTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]          VARCHAR (25)     NOT NULL,
    [Name]          VARCHAR (50)     NOT NULL,
    [Description]   VARCHAR (1000)   NULL,
    [rowguid]       UNIQUEIDENTIFIER CONSTRAINT [DF_HolidayType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]   DATETIME         CONSTRAINT [DF_HolidayType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]  DATETIME         CONSTRAINT [DF_HolidayType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     VARCHAR (256)    NULL,
    [ModifiedBy]    VARCHAR (256)    NULL,
    CONSTRAINT [PK_HolidayType] PRIMARY KEY CLUSTERED ([HolidayTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_HolidayType_Code]
    ON [dbo].[HolidayType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_HolidayType_Name]
    ON [dbo].[HolidayType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_HolidayType_rowguid]
    ON [dbo].[HolidayType]([rowguid] ASC);

