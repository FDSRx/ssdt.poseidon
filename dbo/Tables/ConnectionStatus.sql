﻿CREATE TABLE [dbo].[ConnectionStatus] (
    [ConnectionStatusID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]               VARCHAR (25)     NOT NULL,
    [Name]               VARCHAR (50)     NOT NULL,
    [IsActive]           BIT              CONSTRAINT [DF_ConnectionStatus_IsActive] DEFAULT ((0)) NOT NULL,
    [IsSelfOperated]     BIT              CONSTRAINT [DF_ConnectionStatus_IsSelfConnected] DEFAULT ((0)) NOT NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_ConnectionStatus_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_ConnectionStatus_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_ConnectionStatus_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_ConnectionStatus] PRIMARY KEY CLUSTERED ([ConnectionStatusID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ConnectionStatus_Code]
    ON [dbo].[ConnectionStatus]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ConnectionStatus_Name]
    ON [dbo].[ConnectionStatus]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_ConnectionStatus_rowguid]
    ON [dbo].[ConnectionStatus]([rowguid] ASC);

