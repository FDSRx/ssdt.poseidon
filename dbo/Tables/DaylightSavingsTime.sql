﻿CREATE TABLE [dbo].[DaylightSavingsTime] (
    [DaylightSavingsTimeID] INT              IDENTITY (1, 1) NOT NULL,
    [DateStart]             DATETIME         NOT NULL,
    [DateEnd]               DATETIME         NOT NULL,
    [rowguid]               UNIQUEIDENTIFIER CONSTRAINT [DF_DaylightSavingsTime_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]           DATETIME         CONSTRAINT [DF_DaylightSavingsTime_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]          DATETIME         CONSTRAINT [DF_DaylightSavingsTime_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_DaylightSavingsTime] PRIMARY KEY CLUSTERED ([DaylightSavingsTimeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DaylightSavingsTime_rowguid]
    ON [dbo].[DaylightSavingsTime]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_DaylightSavingsTime_DateStart]
    ON [dbo].[DaylightSavingsTime]([DateStart] ASC);

