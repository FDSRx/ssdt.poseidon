﻿CREATE TABLE [dbo].[BusinessEntityEmailAddress_History] (
    [BusinessEntityEmailAddressHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityEmailAddressID]        BIGINT           NOT NULL,
    [BusinessEntityID]                    BIGINT           NOT NULL,
    [EmailAddress]                        NVARCHAR (256)   NULL,
    [EmailAddressTypeID]                  INT              NOT NULL,
    [IsEmailAllowed]                      BIT              NULL,
    [rowguid]                             UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityEmailAddress_History_rowguid] DEFAULT (newid()) NOT NULL,
    [HashKey]                             VARCHAR (256)    NULL,
    [DateCreated]                         DATETIME         NOT NULL,
    [DateModified]                        DATETIME         NOT NULL,
    [CreatedBy]                           VARCHAR (256)    NULL,
    [ModifiedBy]                          VARCHAR (256)    NULL,
    [AuditGuid]                           UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityEmailAddress_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]                         VARCHAR (10)     NOT NULL,
    [DateAudited]                         DATETIME         CONSTRAINT [DF_BusinessEntityEmailAddress_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    [IsPreferred]                         BIT              CONSTRAINT [DF_BusinessEntityEmailAddress_History_IsPreferred] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_BusinessEntityEmailAddress_History] PRIMARY KEY CLUSTERED ([BusinessEntityEmailAddressHistoryID] ASC)
);

