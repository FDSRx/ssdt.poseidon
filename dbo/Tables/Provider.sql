﻿CREATE TABLE [dbo].[Provider] (
    [ProviderID]   INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (1000)   NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Provider_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Provider_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Provider_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Provider] PRIMARY KEY CLUSTERED ([ProviderID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Provider_Code]
    ON [dbo].[Provider]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Provider_Name]
    ON [dbo].[Provider]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Provider_rowguid]
    ON [dbo].[Provider]([rowguid] ASC);

