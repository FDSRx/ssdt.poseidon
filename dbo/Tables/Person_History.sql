﻿CREATE TABLE [dbo].[Person_History] (
    [PersonHistoryID]  BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID] BIGINT           NOT NULL,
    [PersonTypeID]     INT              NULL,
    [Title]            NVARCHAR (10)    NULL,
    [FirstName]        NVARCHAR (75)    NULL,
    [MiddleName]       NVARCHAR (75)    NULL,
    [LastName]         NVARCHAR (75)    NULL,
    [Suffix]           NVARCHAR (10)    NULL,
    [BirthDate]        DATE             NULL,
    [GenderID]         INT              NULL,
    [LanguageID]       INT              NULL,
    [AdditionalInfo]   XML              NULL,
    [Demographics]     XML              NULL,
    [HashKey]          VARCHAR (256)    NULL,
    [rowguid]          UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]      DATETIME         NOT NULL,
    [DateModified]     DATETIME         NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    [AuditGuid]        UNIQUEIDENTIFIER CONSTRAINT [DF_Person_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]      VARCHAR (10)     NOT NULL,
    [DateAudited]      DATETIME         CONSTRAINT [DF_Person_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Person_History] PRIMARY KEY CLUSTERED ([PersonHistoryID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Person_History_PersonID]
    ON [dbo].[Person_History]([BusinessEntityID] ASC);

