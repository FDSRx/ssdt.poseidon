﻿CREATE TABLE [dbo].[InformationLog] (
    [InformationLogID]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserName]             VARCHAR (255)    NULL,
    [InformationProcedure] VARCHAR (255)    NULL,
    [Message]              VARCHAR (MAX)    NULL,
    [Arguments]            VARCHAR (MAX)    NULL,
    [rowguid]              UNIQUEIDENTIFIER CONSTRAINT [DF_InformationLog_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]          DATETIME         CONSTRAINT [DF_InformationLog_DateCreated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_InformationLog] PRIMARY KEY CLUSTERED ([InformationLogID] ASC)
);

