﻿CREATE TABLE [dbo].[BusinessEntityPhone] (
    [BusinessEntityPhoneNumberID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]            BIGINT           NOT NULL,
    [PhoneNumber]                 NVARCHAR (25)    NOT NULL,
    [PhoneNumberTypeID]           INT              NOT NULL,
    [IsCallAllowed]               BIT              CONSTRAINT [DF_BusinessEntityPhone_IsCallAllowed] DEFAULT ((0)) NOT NULL,
    [IsTextAllowed]               BIT              CONSTRAINT [DF_BusinessEntityPhone_IsTextAllowed] DEFAULT ((0)) NOT NULL,
    [HashKey]                     VARCHAR (256)    NULL,
    [rowguid]                     UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityPhone_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                 DATETIME         CONSTRAINT [DF_BusinessEntityPhoneNumber_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]                DATETIME         CONSTRAINT [DF_BusinessEntityPhoneNumber_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                   VARCHAR (256)    NULL,
    [ModifiedBy]                  VARCHAR (256)    NULL,
    [IsPreferred]                 BIT              CONSTRAINT [DF_BusinessEntityPhone_IsPreferred] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_BusinessEntityPhoneNumber] PRIMARY KEY CLUSTERED ([BusinessEntityPhoneNumberID] ASC),
    CONSTRAINT [FK_BusinessEntityPhoneNumber_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntityPhoneNumber_PhoneNumberType] FOREIGN KEY ([PhoneNumberTypeID]) REFERENCES [dbo].[PhoneNumberType] ([PhoneNumberTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityPhone_BizPhoneType]
    ON [dbo].[BusinessEntityPhone]([BusinessEntityID] ASC, [PhoneNumber] ASC, [PhoneNumberTypeID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityPhone_rowguid]
    ON [dbo].[BusinessEntityPhone]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/2/2013
-- Description:	Audits a record change of the business entity phone table
-- SELECT * FROM dbo.BusinessEntityPhone_History
-- TRUNCATE TABLE dbo.BusinessEntityPhone_History
-- =============================================
CREATE TRIGGER [dbo].[trigBusinessEntityPhone_Audit]
   ON  [dbo].[BusinessEntityPhone]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	-----------------------------------------------------------------------------
	-- Retrieve session data
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT


	-----------------------------------------------------------------------------
	-- Audit transaction
	-----------------------------------------------------------------------------			
	-- Audit inserted records (updated or newly created)
	INSERT INTO dbo.BusinessEntityPhone_History (
		BusinessEntityPhoneNumberID,	
		BusinessEntityID,	
		PhoneNumber,	
		PhoneNumberTypeID,	
		IsCallAllowed,	
		IsTextAllowed,
		IsPreferred,
		HashKey,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,	
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityPhoneNumberID,	
		BusinessEntityID,	
		PhoneNumber,	
		PhoneNumberTypeID,	
		IsCallAllowed,	
		IsTextAllowed,
		IsPreferred,
		HashKey,
		rowguid,	
		DateCreated,	
		DateModified,
		CreatedBy,		
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE d.ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted d
	
	--SELECT * FROM dbo.BusinessEntityPhone
	
	

END
