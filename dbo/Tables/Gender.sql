﻿CREATE TABLE [dbo].[Gender] (
    [GenderID]     INT           IDENTITY (1, 1) NOT NULL,
    [Code]         NVARCHAR (10) NULL,
    [Name]         NVARCHAR (20) NULL,
    [DateCreated]  DATETIME      CONSTRAINT [DF_Gender_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME      CONSTRAINT [DF_Gender_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Gender] PRIMARY KEY CLUSTERED ([GenderID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Gender_Code]
    ON [dbo].[Gender]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Gender_Name]
    ON [dbo].[Gender]([Name] ASC);

