﻿CREATE TABLE [dbo].[CalendarItem_History] (
    [CalendarItemHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [NoteID]                BIGINT           NOT NULL,
    [ApplicationID]         INT              NULL,
    [BusinessID]            BIGINT           NULL,
    [BusinessEntityID]      BIGINT           NULL,
    [Location]              VARCHAR (500)    NULL,
    [rowguid]               UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]           DATETIME         NOT NULL,
    [DateModified]          DATETIME         NOT NULL,
    [CreatedBy]             VARCHAR (256)    NULL,
    [ModifiedBy]            VARCHAR (256)    NULL,
    [AuditGuid]             UNIQUEIDENTIFIER CONSTRAINT [DF_CalendarItem_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]           VARCHAR (10)     NULL,
    [DateAudited]           DATETIME         CONSTRAINT [DF_CalendarItem_History_DateAudited] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_CalendarItem_History] PRIMARY KEY CLUSTERED ([CalendarItemHistoryID] ASC)
);

