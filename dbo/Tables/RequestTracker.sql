﻿CREATE TABLE [dbo].[RequestTracker] (
    [RequestTrackerID]   BIGINT           IDENTITY (1, 1) NOT NULL,
    [RequestTypeID]      INT              NOT NULL,
    [SourceName]         VARCHAR (256)    NULL,
    [ApplicationKey]     VARCHAR (50)     NULL,
    [BusinessKey]        VARCHAR (50)     NULL,
    [RequesterKey]       VARCHAR (50)     NULL,
    [RequestKey]         VARCHAR (256)    NULL,
    [RequestCount]       INT              NULL,
    [RequestsRemaining]  INT              NULL,
    [MaxRequestsAllowed] INT              NULL,
    [IsLocked]           BIT              DEFAULT ((0)) NULL,
    [DateExpires]        DATETIME         NULL,
    [ApplicationID]      INT              NULL,
    [BusinessEntityID]   BIGINT           NULL,
    [RequesterID]        BIGINT           NULL,
    [RequestData]        VARCHAR (MAX)    NULL,
    [Arguments]          VARCHAR (MAX)    NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_RequestTracker_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_RequestTracker_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_RequestTracker_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_RequestTracker] PRIMARY KEY CLUSTERED ([RequestTrackerID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_RequestTracker_RequestTracker] FOREIGN KEY ([RequestTrackerID]) REFERENCES [dbo].[RequestTracker] ([RequestTrackerID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_RequestTracker_AppBizReqtorReq]
    ON [dbo].[RequestTracker]([ApplicationKey] ASC, [BusinessKey] ASC, [RequesterKey] ASC, [RequestKey] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_RequestTracker_rowguid]
    ON [dbo].[RequestTracker]([rowguid] ASC) WITH (FILLFACTOR = 90);

