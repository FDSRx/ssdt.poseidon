﻿CREATE TABLE [dbo].[Business] (
    [BusinessEntityID] BIGINT           NOT NULL,
    [BusinessTypeID]   INT              NOT NULL,
    [BusinessToken]    UNIQUEIDENTIFIER CONSTRAINT [DF_Business_BusinessToken] DEFAULT (newid()) NOT NULL,
    [BusinessNumber]   VARCHAR (10)     NOT NULL,
    [Name]             VARCHAR (256)    NULL,
    [SourceSystemKey]  VARCHAR (256)    NULL,
    [LicenseNumber]    UNIQUEIDENTIFIER CONSTRAINT [DF_Business_LicenseNumber] DEFAULT (newid()) NOT NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Business_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_Business_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_Business_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Business] PRIMARY KEY CLUSTERED ([BusinessEntityID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Business_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Business_BusinessType] FOREIGN KEY ([BusinessTypeID]) REFERENCES [dbo].[BusinessType] ([BusinessTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Business_BusinessToken]
    ON [dbo].[Business]([BusinessToken] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Business_BusinessNumber]
    ON [dbo].[Business]([BusinessNumber] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Business_rowguid]
    ON [dbo].[Business]([rowguid] ASC) WITH (FILLFACTOR = 90);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Business_LicenseNumber]
    ON [dbo].[Business]([LicenseNumber] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/22/2016
-- Description:	Audits a record change of the Business table

-- SELECT * FROM dbo.Business
-- SELECT * FROM dbo.Business_History

-- TRUNCATE TABLE dbo.Business_History
-- =============================================
CREATE TRIGGER dbo.[trigBusiness_Audit]
   ON  dbo.Business
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------------
	-- Short circut audit if no records.
	------------------------------------------------------------------------------------------------------------------
	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;

	------------------------------------------------------------------------------------------------------------------	
	-- Determine type of operation being performed (i.e. create, update, delete, etc.).
	------------------------------------------------------------------------------------------------------------------
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	------------------------------------------------------------------------------------------------------------------
	-- Retrieve session data
	------------------------------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
			
	------------------------------------------------------------------------------------------------------------------	
	-- Audit inserted records (updated or newly created)
	------------------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.Business_History (
		BusinessEntityID,
		BusinessTypeID,
		BusinessToken,
		BusinessNumber,
		Name,
		SourceSystemKey,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityID,
		BusinessTypeID,
		BusinessToken,
		BusinessNumber,
		Name,
		SourceSystemKey,
		rowguid,
		DateCreated,
		DateModified,	
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	-- Data review.
	-- SELECT * FROM dbo.Business
	-- SELECT * FROM dbo.Business_History
	
	

END
