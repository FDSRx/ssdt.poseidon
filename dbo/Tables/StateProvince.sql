﻿CREATE TABLE [dbo].[StateProvince] (
    [StateProvinceID]     INT              IDENTITY (1, 1) NOT NULL,
    [StateProvinceCode]   NVARCHAR (5)     NULL,
    [CountryRegionCode]   NVARCHAR (5)     NULL,
    [IsOnlyStateProvince] BIT              CONSTRAINT [DF_StateProvince_IsOnlyStateProvince] DEFAULT ((0)) NULL,
    [Name]                NVARCHAR (50)    NULL,
    [TimeZoneID]          INT              NULL,
    [rowguid]             UNIQUEIDENTIFIER CONSTRAINT [DF_StateProvince_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]         DATETIME         CONSTRAINT [DF_StateProvince_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]        DATETIME         CONSTRAINT [DF_StateProvince_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_StateProvince] PRIMARY KEY CLUSTERED ([StateProvinceID] ASC),
    CONSTRAINT [FK_StateProvince_CountryRegion] FOREIGN KEY ([CountryRegionCode]) REFERENCES [dbo].[CountryRegion] ([CountryRegionCode])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_StateProvince_rowguid]
    ON [dbo].[StateProvince]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_StateProvince_ProvCodeCountryCode]
    ON [dbo].[StateProvince]([StateProvinceCode] ASC, [CountryRegionCode] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_StateProvince_Name]
    ON [dbo].[StateProvince]([Name] ASC);

