﻿CREATE TABLE [dbo].[Chain_History] (
    [ChainHistoryID]   BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID] BIGINT           NULL,
    [SourceChainID]    INT              NULL,
    [Name]             VARCHAR (255)    NULL,
    [Demographics]     XML              NULL,
    [rowguid]          UNIQUEIDENTIFIER NULL,
    [DateCreated]      DATETIME         NULL,
    [DateModified]     DATETIME         NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    [AuditGuid]        UNIQUEIDENTIFIER CONSTRAINT [DF_Chain_History_AuditGuid] DEFAULT (newid()) NOT NULL,
    [AuditAction]      VARCHAR (10)     NULL,
    [DateAudited]      DATETIME         CONSTRAINT [DF_Chain_History_DateAudited] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Chain_History] PRIMARY KEY CLUSTERED ([ChainHistoryID] ASC)
);

