﻿CREATE TABLE [dbo].[BusinessEntityService] (
    [BusinessEntityServiceID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]        BIGINT           NOT NULL,
    [ServiceID]               INT              NOT NULL,
    [AccountKey]              VARCHAR (256)    NULL,
    [ConnectionStatusID]      INT              NULL,
    [GoLiveDate]              DATETIME         NULL,
    [DateTermed]              DATETIME         NULL,
    [HashKey]                 VARCHAR (256)    NULL,
    [rowguid]                 UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityService_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]             DATETIME         CONSTRAINT [DF_BusinessEntityService_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]            DATETIME         CONSTRAINT [DF_BusinessEntityService_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]               VARCHAR (256)    NULL,
    [ModifiedBy]              VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessEntityService] PRIMARY KEY CLUSTERED ([BusinessEntityServiceID] ASC),
    CONSTRAINT [FK_BusinessEntityService_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntityService_ConnectionStatus] FOREIGN KEY ([ConnectionStatusID]) REFERENCES [dbo].[ConnectionStatus] ([ConnectionStatusID]),
    CONSTRAINT [FK_BusinessEntityService_Service] FOREIGN KEY ([ServiceID]) REFERENCES [dbo].[Service] ([ServiceID])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityService_BusinessService]
    ON [dbo].[BusinessEntityService]([BusinessEntityID] ASC, [ServiceID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityService_rowguid]
    ON [dbo].[BusinessEntityService]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/25/2016
-- Description:	Audits a record change of the BusinessEntityService table

-- SELECT * FROM dbo.BusinessEntityService
-- SELECT * FROM dbo.BusinessEntityService_History

-- TRUNCATE TABLE dbo.BusinessEntityService_History
-- =============================================
CREATE TRIGGER [dbo].[trigBusinessEntityService_Audit]
   ON  [dbo].[BusinessEntityService]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------------------------------
	-- Short circut audit if no records.
	------------------------------------------------------------------------------------------------------------------
	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;

	------------------------------------------------------------------------------------------------------------------	
	-- Determine type of operation being performed (i.e. create, update, delete, etc.).
	------------------------------------------------------------------------------------------------------------------
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	

	------------------------------------------------------------------------------------------------------------------
	-- Retrieve session data
	------------------------------------------------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
			
	------------------------------------------------------------------------------------------------------------------	
	-- Audit object(s).
	------------------------------------------------------------------------------------------------------------------
	INSERT INTO dbo.BusinessEntityService_History (
		BusinessEntityServiceID,
		BusinessEntityID,
		ServiceID,
		AccountKey,
		ConnectionStatusID,
		GoLiveDate,
		DateTermed,
		HashKey,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityServiceID,
		BusinessEntityID,
		ServiceID,
		AccountKey,
		ConnectionStatusID,
		GoLiveDate,
		DateTermed,
		HashKey,
		rowguid,
		DateCreated,
		DateModified,	
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	-- Data review.
	-- SELECT * FROM dbo.BusinessEntityService
	-- SELECT * FROM dbo.BusinessEntityService_History
	
	

END
