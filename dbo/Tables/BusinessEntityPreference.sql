﻿CREATE TABLE [dbo].[BusinessEntityPreference] (
    [BusinessEntityPreferenceID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]           BIGINT           NOT NULL,
    [PreferenceID]               INT              NOT NULL,
    [ValueString]                VARCHAR (MAX)    NULL,
    [ValueBoolean]               BIT              NULL,
    [ValueNumeric]               DECIMAL (18)     NULL,
    [rowguid]                    UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityPreference_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]                DATETIME         CONSTRAINT [DF_BusinessEntityPreference_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]               DATETIME         CONSTRAINT [DF_BusinessEntityPreference_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                  VARCHAR (256)    NULL,
    [ModifiedBy]                 VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessEntityPreference] PRIMARY KEY CLUSTERED ([BusinessEntityPreferenceID] ASC),
    CONSTRAINT [FK_BusinessEntityPreference_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntityPreference_Preference] FOREIGN KEY ([PreferenceID]) REFERENCES [dbo].[Preference] ([PreferenceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityPreference_BizEntityPreference]
    ON [dbo].[BusinessEntityPreference]([BusinessEntityID] ASC, [PreferenceID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityPreference_rowguid]
    ON [dbo].[BusinessEntityPreference]([rowguid] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/23/2015
-- Description:	Audits a record change of the BusinessEntityPreference table
-- SELECT * FROM dbo.BusinessEntityPreference_History
-- TRUNCATE TABLE dbo.BusinessEntityPreference_History
-- =============================================
CREATE TRIGGER [dbo].[trigBusinessEntityPreference_Audit]
   ON  [dbo].[BusinessEntityPreference]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	
	
	-- Audit inserted records (updated or newly created)
	INSERT INTO dbo.BusinessEntityPreference_History (
		BusinessEntityPreferenceID,	
		BusinessEntityID,	
		PreferenceID,	
		ValueString,	
		ValueBoolean,	
		ValueNumeric,
		rowguid,
		DateCreated,
		DateModified,	
		CreatedBy,	
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited
	)
	SELECT
		BusinessEntityPreferenceID,	
		BusinessEntityID,	
		PreferenceID,	
		ValueString,	
		ValueBoolean,	
		ValueNumeric,
		rowguid,
		DateCreated,
		DateModified,	
		CreatedBy,	
		ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	--SELECT * FROM dbo.BusinessEntityPreference
	
	

END
