﻿CREATE TABLE [dbo].[BusinessEntityHoliday] (
    [BusinessEntityHolidayID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [BusinessEntityID]        BIGINT           NULL,
    [HolidayID]               BIGINT           NOT NULL,
    [rowguid]                 UNIQUEIDENTIFIER CONSTRAINT [DF_BusinessEntityHoliday_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]             DATETIME         CONSTRAINT [DF_BusinessEntityHoliday_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]            DATETIME         CONSTRAINT [DF_BusinessEntityHoliday_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]               VARCHAR (256)    NULL,
    [ModifiedBy]              VARCHAR (256)    NULL,
    CONSTRAINT [PK_BusinessEntityHoliday] PRIMARY KEY CLUSTERED ([BusinessEntityHolidayID] ASC),
    CONSTRAINT [FK_BusinessEntityHoliday_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_BusinessEntityHoliday_Holiday] FOREIGN KEY ([HolidayID]) REFERENCES [dbo].[Holiday] ([HolidayID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_BusinessEntityHoliday_BusinessEntityIDHolidayID]
    ON [dbo].[BusinessEntityHoliday]([BusinessEntityID] ASC, [HolidayID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BusinessEntityHoliday_rowguid]
    ON [dbo].[BusinessEntityHoliday]([rowguid] ASC);

