﻿CREATE TABLE [dbo].[Configuration] (
    [ConfigurationID]  BIGINT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID]    INT              NULL,
    [BusinessEntityID] BIGINT           NULL,
    [KeyName]          VARCHAR (256)    NOT NULL,
    [KeyValue]         VARCHAR (MAX)    NOT NULL,
    [DataTypeID]       INT              NOT NULL,
    [DataSize]         INT              NULL,
    [Description]      VARCHAR (1000)   NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Configuration_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_Configuration_DateModified] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_Configuration_DateModified_1] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED ([ConfigurationID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Configuration_Application] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Application] ([ApplicationID]),
    CONSTRAINT [FK_Configuration_BusinessEntity] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[BusinessEntity] ([BusinessEntityID]),
    CONSTRAINT [FK_Configuration_ConfigurationKey] FOREIGN KEY ([KeyName]) REFERENCES [config].[ConfigurationKey] ([Name]),
    CONSTRAINT [FK_Configuration_DataType] FOREIGN KEY ([DataTypeID]) REFERENCES [dbo].[DataType] ([DataTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Configuration_AppBizKey]
    ON [dbo].[Configuration]([ApplicationID] ASC, [BusinessEntityID] ASC, [KeyName] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [UIX_Configuration_rowguid]
    ON [dbo].[Configuration]([rowguid] ASC) WITH (FILLFACTOR = 90);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/25/2015
-- Description:	Audits a record change of the dbo.Configuration table.

-- SELECT * FROM dbo.Configuration_History
-- SELECT * FROM dbo.Configuration

-- TRUNCATE TABLE dbo.NoteSchedule_History
-- =============================================
CREATE TRIGGER dbo.[trigConfiguration_Audit]
   ON  dbo.Configuration
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	


	-----------------------------------------------------------------------------
	-- Retrieve session data.
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
		
	-------------------------------------------------------------------------------		
	-- Audit inserted records (updated or newly created)
	-------------------------------------------------------------------------------
	INSERT INTO dbo.Configuration_History (
		ConfigurationID,
		ApplicationID,
		BusinessEntityID,
		KeyName,
		KeyValue,
		DataTypeID,
		DataSize,
		Description,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited	
	)
	SELECT
		ConfigurationID,
		ApplicationID,
		BusinessEntityID,
		KeyName,
		KeyValue,
		DataTypeID,
		DataSize,
		Description,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	--SELECT * FROM dbo.Configuration
	
	

END
