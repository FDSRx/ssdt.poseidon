﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
IF NOT EXISTS(SELECT * FROM creds.CredentialEntityType)
BEGIN
	SET IDENTITY_INSERT creds.CredentialEntityType ON;
	INSERT INTO creds.CredentialEntityType
		(CredentialEntityTypeID, Code, Name, Description, rowguid, DateCreated, DateModified, CreatedBy, ModifiedBy)
	VALUES
		(1, 'EXTRANET', 'Extranet User', 'Intranet type applications that have been extended to our external clients.', '69B862D5-8415-479E-B14C-7B8386A09831', '2013-10-29 14:52:31.033', '2013-10-29 14:52:31.033', 'dhughes', NULL),
		(2, 'NETWORK', 'Network User', 'Internal applications that are only visible to employees.', 'E62CDFEB-37CE-440F-8FFA-116ADC8DE3F6', '2013-12-11 10:32:07.023', '2013-12-11 10:32:07.023', 'dhughes', NULL),
		(3, 'WORKGROUP', 'Workgroup', 'A singular or collection of credentials.', 'B68CD1CE-95E8-4A29-92B2-65999CF27E67', '2013-12-11 13:02:58.597', '2013-12-11 13:02:58.597', 'dhughes', NULL),
		(4, 'DATASERV', 'Data Services', 'A publicly exposed API or application.', '7A6C66FB-D6B1-40BD-B1A6-49B2A8841D30', '2015-09-04 10:01:55.923', '2015-09-04 10:01:55.923', 'dhughes', NULL)
	SET IDENTITY_INSERT creds.CredentialEntityType OFF;
END

IF NOT EXISTS(SELECT * FROM dbo.BusinessEntityType)
BEGIN
	SET IDENTITY_INSERT dbo.BusinessEntityType ON;
	INSERT INTO dbo.BusinessEntityType
		(BusinessEntityTypeID, Code, Name, rowguid, DateCreated, DateModified, CreatedBy, ModifiedBy)
	VALUES
		(1, 'STRE', 'Store', '873E2476-ADB1-4679-BBAA-1FBF021ED96A', '2014-09-25 09:18:45.170', '2013-01-14 13:13:19.730', NULL, NULL),
		(2, 'CHN', 'Chain', 'B0CC76F2-5772-4C75-87D9-F921C3423FFD', '2014-09-25 09:18:45.170', '2013-01-14 13:13:19.750', NULL, NULL),
		(3, 'NTWRK', 'Network', 'EB0073BD-44EA-4B4A-9C12-902156E1ED10', '2014-09-25 09:18:45.170', '2013-01-14 13:13:19.763', NULL, NULL),
		(4, 'PRSN', 'Person', 'BC62FED2-24CC-4BCC-B2AA-63E59CC6137C', '2014-09-25 09:18:45.170', '2013-01-16 09:57:27.890', NULL, NULL),
		(5, 'CRED', 'Credential', '305BE756-B56C-4FCD-91FB-967C54DC0423', '2014-09-25 09:18:45.170', '2014-06-12 07:19:15.547', NULL, NULL),
		(6, 'SNDR', 'Sender', '25F63169-B1F3-4B8E-9AB3-86355A07BC64', '2014-09-25 09:18:45.170', '2014-09-02 12:52:42.473', NULL, NULL),
		(7, 'UNKN', 'Unknown', 'A9EAEA9A-8017-418D-84A8-F28E5E907FE4', '2014-09-25 09:18:45.170', '2014-09-25 09:17:40.470', NULL, NULL)
	SET IDENTITY_INSERT dbo.BusinessEntityType OFF;
END

IF NOT EXISTS(SELECT * FROM dbo.BusinessType)
BEGIN
	SET IDENTITY_INSERT dbo.BusinessType ON;
	INSERT INTO dbo.BusinessType
		(BusinessTypeID, Code, Name, Description, rowguid)
	VALUES
		(1, 'STRE', 'Store', NULL, '5AE25627-80A2-4512-97DC-DED8440B6088'),
		(2, 'CHN', 'Chain', NULL, '07A6A70F-1028-4DBE-B9AB-9B3CAF3F9CDC'),
		(3, 'NTWRK', 'Network', NULL, '101EA9FD-5898-482F-95A5-D06BBD1C93D3'),
		(4, 'UNKN', 'Unknown', NULL, '0FFF7D13-5D2B-4464-AF65-6E38F571706D')
	SET IDENTITY_INSERT dbo.BusinessType OFF;
END

IF NOT EXISTS(SELECT * FROM config.ConfigurationKey)
BEGIN
	SET IDENTITY_INSERT config.ConfigurationKey ON;
	INSERT INTO config.ConfigurationKey
		(ConfigurationKeyID, Name, Description, rowguid, DateCreated, DateModified, CreatedBy, ModifiedBy)
	VALUES
		(1, 'CredentialLockOutMaxAttempts', 'Default number of times a credential is allowed to be tried before it is locked by the system.', '5F0C3AC1-6F0A-4F14-B43B-0F0B8A04F9C0', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(2, 'CredentialLockOutTimeMin', 'Default lock out time for a credential.', '005108C4-3544-48E8-AD11-1661343D11AB', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(3, 'CredTokenDurationMin', 'Default credential token duration.', 'B2C6541C-7E50-4133-9E4A-E3C527170606', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(4, 'DEFAULTLANG', 'Default system language.', 'F629C4D0-7DDD-4436-8270-81FEA1AE5504', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(5, 'DFLTSTREID', 'Default store.  Store is used when standard documents not owned by other stores are needed. (i.e. privacy policy, logos, etc.).', 'FC4F4AAD-9AC7-4179-8CA7-669B0F01CFAB', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(6, 'EnableContactCustomerContextMenuItem', 'True to enable the ability to contact customer via the context menu; otherwise, false to disable.', 'EB2BE3F5-5865-4F4A-8CD6-25D20C865F89', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(7, 'FetchAndShowBusinessTaskHomeWidget', 'True to fetch business task data and to show task widget; otherwise, false to not fetch and hide.', '976E3755-FD33-4D30-A302-F643FEFA340E', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(8, 'ForceNullPharmacyPlanKeyRequest', 'True to force the use of a Null pharmacy plan key request which triggers an ALL plans lookup; otherwise, false to use pharamcy plan key request from the UI.', '83AC9513-7620-4A16-867A-A75F495762DC', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(9, 'IsAdhocMessagingEnabled', 'True to show the adhoc messaging element link; otherwise, false to hide the adhoc messaging element link.', '3E426470-B5C0-4EF8-8AC7-02F66E0C895C', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(10, 'IsFiveStarBannerDrillThroughEnabled', 'True to have the ability to drill into 5star plan detail; otherwise, false to have the ability unavailable.', '9598EC15-45ED-40DF-8B52-AC1AB2F96EF6', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(11, 'IsFiveStarReportMeasuresExpandable', 'True to have the ability to expand five star measures; otherwise, false to exclude the ability.', '6A7D19D4-5E3A-4025-9FAD-D753BD5E17AB', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(12, 'IsMedSyncCustomModeEnabled', 'True to have Med. Sync custom mode enabled; otherwise, false to have the ability disabled.', '22C1C55B-7BAB-49E4-B7ED-474CB279DE6E', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(13, 'MedSyncDaysInPeriod', '# of days in period to qualify as candidate', '4BD8A405-049D-4F90-9993-C9AB44C3B0F7', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(14, 'MedSyncDaysPerPeriod', 'Times per came in the period to qualify as candidate', '7CDA8DF3-E0B0-40B1-82B0-A13B76CEF252', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(15, 'MedSyncQueueDays', 'Days early to place in pharmacy queue', 'C2BFFFBF-74E9-4D1C-BC3F-01F1326AEF46', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(16, 'MedSyncReminderDays', 'Number of days before the appointment to call the patient.', '039DEEC7-0CB3-42C0-90EE-57DB07B5166E', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(17, 'MedSyncScoreAlertMultiplier', 'Alert multiplier for determining score', 'A0B2C628-5974-4B05-9DC9-8A04C952C01E', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(18, 'MedSyncScoreDaysMultiplier', 'Day count Multiplier for determining score', '87A1E760-746C-4DB4-A8E6-916C61120030', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(19, 'MedSyncScoreScriptsMultiplier', 'Script count Multiplier for determining score', '737C51E2-ABC4-4E73-8DEE-47740CA38D26', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(20, 'MedSyncScriptsPerPeriod', '# of scripts in period to qualify as candidate', '60355BC4-38D5-4AB1-ABC6-4869CBF769F6', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(21, 'MedSyncZeroQtyMMLookback', 'Number of days to lookup back to include scripts in Recommended list for MM with no qty remaining', '18AA268E-D127-47E4-9C48-CD50A5662C89', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(22, 'RequestAttemptsMax', 'The default number of times a request is allowed to be tried before it is locked by the system.', '39DCD69E-80A8-458C-BA10-71151F5E5874', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(23, 'RequestKeyExpirationMin', 'The number of minutes the request is tracked before it expires and starts a new.', 'AB77E1A2-9ABE-4AD9-AAA8-FC808B1A4337', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(24, 'SERVERTIMEZONE', 'POSEIDON server records time in Central Standard Time.', '3CDE0C94-A5F5-4B6F-9CA8-7DDE5C87CA4F', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(25, 'ShowDoNotCall', 'True to have "do not call" feature available; otherwise, false to have the ability hidden.', '6ECD9C53-7319-4157-8ABA-4E88958F9114', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(26, 'ShowEngagePlanFiltering', 'True to show the engage plan filtering menu; otherwise, false to hide.', '11F09C14-B114-4C3A-B793-F82D5C392D9A', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(27, 'ShowProfileAdverseReactions', 'True to show the patient and medical profile adverse reactions widget; otherwise, false to hide.', 'E72663F7-9AA4-4883-9561-E5E5F8A8F8F2', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(28, 'ShowProfileAllergies', 'True to show the patient and medical profile allergies widget; otherwise, false to hide.', '16A99236-3593-4479-B346-5165084123E0', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(29, 'ShowProfileClinicalMeasures', 'True to show the patient and medical profile clinical measures widget; otherwise, false to hide.', '8099B9DA-B5B5-4B29-B6E5-7AB18B6F010A', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(30, 'ShowProfileConditions', 'True to show the patient and medical profile conditions widget; otherwise, false to hide.', '81398B12-F1B6-4A17-9D79-40A50035520E', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(31, 'ShowProfileDrugCompliance', 'True to show the profile drug compliance widget; otherwise, false to hide.', 'EF81DB04-DC27-411C-90C8-6A042727F551', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(32, 'ShowProfileImmunizations', 'True to show the patient and medical profile immunizations widget; otherwise, false to hide.', '64B53E9A-52B6-466D-BEE4-2112F452E099', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(33, 'ShowProfileRecentClaimsActivity', 'True to show the patient and medical profile recent claim activity; otherwise, false to hide.', '4D589AF2-DD6A-4E4B-9D1F-AE324AF5D1C7', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(34, 'ShowRestrictInsurerCommunications', 'True to have "restrict insurer communications" feature available; otherwise, false to have the ability hidden.', '62DCF715-03FE-4F74-AF35-6DF794DB279E', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(35, 'UnknownAuthorID', NULL, '66B8DB43-0EFB-4CFB-BD97-FF63A0C73CC9', '2015-06-10 13:09:55.000', '2015-06-10 13:09:55.000', NULL, NULL),
		(36, 'ShowEngageAdjudicationFiltering', 'True to show the engage adjudication filtering menu; otherwise, false to have the ability hidden.', '02841ABE-9B55-4781-AE72-5338137A55B1', '2015-06-16 12:18:41.953', '2015-06-16 12:18:41.953', 'dhughes', NULL),
		(37, 'InitialEngagePlanFilterTypeKey', 'The unique identifier used to determine the initial filtering panel to display (i.e. "ADJ" for adjudication or "PHRM" for pharmacy defined plans).', 'E5808EA0-BF0F-4ED3-93FE-0CEFB2CB485F', '2015-06-17 17:17:14.527', '2015-06-17 17:17:14.527', 'dhughes', NULL),
		(38, 'IsFiveStarBannerPatientSelectionEnabled', 'True to have patient selection available on the five star banner; otherwise, false to have patient selection disabled.', '42DD8969-B119-4379-AFA0-7363D0237A22', '2015-06-20 21:08:59.623', '2015-06-20 21:08:59.623', 'dhughes', NULL),
		(39, 'IsFiveStarBannerPatientCountsVisible', 'True to have the patient counts visible; otherwise, false to have the patient counts hidden.', '14AF1517-5F72-453F-9137-0910BF44041B', '2015-06-22 11:48:43.957', '2015-06-22 11:48:43.957', 'dhughes', NULL),
		(40, 'IsEngageMedSyncOnly', 'True to have the engage product be med sync only; otherwise, false to enable all features.', '26E81025-DB89-4833-8F19-7715B63E2208', '2015-07-17 16:22:15.423', '2015-07-17 16:22:15.423', 'dhughes', NULL),
		(41, 'IsEngageMessagingEnabled', 'True to have messaging enabled within the engage product; otherwise, false to have messaging disabled.', '5DA98777-5B46-4C12-8A6C-7D1B46C6365B', '2015-07-17 16:22:50.113', '2015-07-17 16:22:50.113', 'dhughes', NULL),
		(42, 'MedSyncPharmacyQueueEnabled', 'True to indicate the client allows the med sync pharmacy queue to be populated via the engage platform; otherwise, false to indicate no auto population.', '03A65990-2649-4331-88D4-F716285DBBAB', '2015-08-18 14:18:06.383', '2015-08-18 14:18:06.383', 'dhughes', NULL),
		(43, 'MedSyncAutoRxRenew', 'Whether to auto renew Rxs with a new fill for same GPI', '223DEA68-246B-42B8-8A38-2916259CC4FB', '2015-12-14 09:32:04.047', '2015-12-14 09:32:04.047', 'ssimmons', NULL),
		(44, 'MedSyncQueueDaysNRR', 'Days early to populate fills queue if no refills remaining', 'BBFF96CA-3966-4A61-A187-02CEAE31738B', '2015-12-28 12:56:49.873', '2015-12-28 12:56:49.873', 'ssimmons', NULL),
		(79, 'AK1', 'AK1 TEST KEY', 'A2BBCEDC-EE88-4178-B932-44A489F841D1', '2016-01-14 18:21:51.617', '2016-01-14 18:21:51.620', 'Alex Korolev', NULL),
		(80, 'AK2', 'AK2 MULTI APP KEY', '1AAED6F0-C76A-4760-943D-C243C8E7017E', '2016-01-14 18:33:50.100', '2016-01-14 18:33:50.100', 'Alex Korolev', NULL),
		(81, 'MedSyncDisplayPickupDate', 'Display Pickup Date on Med Sync Screen', 'EDCCCCF8-FB18-482D-BA57-52A1960C00D8', '2016-05-16 13:21:37.467', '2016-05-16 13:21:37.467', 'ssimmons', NULL),
		(82, 'IsMedSyncBinVerificationQueueEnabled', 'True to indicate the med sync bin verification queue can be utilized; otherwise, false to indicate the queue is not available for use.', 'E8B5EB8A-8F52-45FC-846A-4EA299300869', '2016-06-06 10:03:05.543', '2016-06-06 10:03:05.543', 'dhughes', NULL),
		(83, 'IsImmunizationsEnabled', 'True to indicate that immunizations are loadable and visible; otherwise, false to indicate immunizations are not to be loaded.', 'EFBEA0B2-3607-4E39-9FE7-29409B936F63', '2016-09-01 12:53:03.787', '2016-09-01 12:53:03.787', 'Daniel Hughes', NULL)
	SET IDENTITY_INSERT config.ConfigurationKey OFF;
END

IF NOT EXISTS(SELECT * FROM dbo.DataType)
BEGIN
	SET IDENTITY_INSERT dbo.DataType ON;
	INSERT INTO dbo.DataType
		(DataTypeID, TypeOf, Name, Description, Storage, rowguid, DateCreated, DateModified)
	VALUES
		(1, 'char', 'char(n)', 'Fixed width character string. Maximum 8,000 characters', 'Defined width', '7A1D42FB-75D9-4B17-9698-1CC04B4A9E51', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(2, 'varchar', 'varchar(n)', 'Variable width character string. Maximum 8,000 characters', '2 bytes + number of chars', 'C060D31F-6F74-47B4-99FB-AA9DD8CB3723', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(3, 'varchar(max)', 'varchar(max)', 'Variable width character string. Maximum 1,073,741,824 characters', '2 bytes + number of chars', 'F60D05B5-E8A8-4273-B208-C279D2E057E1', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(4, 'text', 'text', 'Variable width character string. Maximum 2GB of text data', '4 bytes + number of chars', '566500DC-5FD4-4346-A41E-AD5FD46752AF', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(5, 'nchar', 'nchar', 'Fixed width Unicode string. Maximum 4,000 characters', 'Defined width x 2', 'E7A119F0-B481-4BE0-8D0E-C0F3E515E350', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(6, 'nvarchar', 'nvarchar', 'Variable width Unicode string. Maximum 4,000 characters', '', 'A260EF65-E4B3-456E-ACDB-74C84F9F5690', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(7, 'nvarchar(max)', 'nvarchar(max)', 'Variable width Unicode string. Maximum 536,870,912 characters', '', 'AB340E73-FDF3-4FD7-9AF7-7D8BBD3E046C', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(8, 'ntext', 'ntext', 'Variable width Unicode string. Maximum 2GB of text data', '', '8D5B376C-2148-4A72-8F6C-B333311117DE', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(9, 'bit', 'bit', 'Allows 0, 1, or NULL', '', '89FA5CE2-2368-4ADD-94FA-49907E7163C2', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(10, 'binary', 'binary(n)', 'Fixed width binary string. Maximum 8,000 bytes', '', 'AD715C72-85CC-4620-AAE7-E2E8FB99C335', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(11, 'varbinary', 'varbinary', 'Variable width binary string. Maximum 8,000 bytes', '', 'C7C87F27-0016-4081-A842-F27108AC9847', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(12, 'varbinary(max)', 'varbinary(max)', 'Variable width binary string. Maximum 2GB', '', 'EF000B55-9193-4161-9A46-F2A1B4A19316', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(13, 'image', 'image', 'Variable width binary string. Maximum 2GB', '', '05E94F0E-A11A-449C-A96B-E2E1CA5A8789', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(14, 'tinyint', 'tinyint', 'Allows whole numbers from 0 to 255', '1 byte', 'C8ECDDAF-94A9-48B5-9E0E-725243D4352E', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(15, 'smallint', 'smallint', 'Allows whole numbers between -32,768 and 32,767', '2 bytes', '3FB1F66A-77DE-46FC-BEF9-3DE2191B6EA5', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(16, 'int', 'int', 'Allows whole numbers between -2,147,483,648 and 2,147,483,647', '4 bytes', '16CBD6A2-E313-4715-9697-784F42EB2868', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(17, 'bigint', 'bigint', 'Allows whole numbers between -9,223,372,036,854,775,808 and 9,223,372,036,854,775,807', '8 bytes', '787131F8-33A2-4B1A-BDB6-DC83DC6DB97D', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(18, 'decimal', 'decimal(p,s)', 'Fixed precision and scale numbers. Allows numbers from -10^38 +1 to 10^38 –1. The p parameter indicates the maximum total number of digits that can be stored (both to the left and to the right of the decimal point). p must be a value from 1 to 38. Default is 18. The s parameter indicates the maximum number of digits stored to the right of the decimal point. s must be a value from 0 to p. Default value is 0', '5-17 bytes', '2E2E778E-C8B5-4F08-9007-7C17F40AE778', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(19, 'numeric', 'numeric(p,s)', 'Fixed precision and scale numbers. Allows numbers from -10^38 +1 to 10^38 –1. The p parameter indicates the maximum total number of digits that can be stored (both to the left and to the right of the decimal point). p must be a value from 1 to 38. Default is 18. The s parameter indicates the maximum number of digits stored to the right of the decimal point. s must be a value from 0 to p. Default value is 0', '5-17 bytes', '77A990E8-D581-4D18-B222-05BD2D241727', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(20, 'smallmoney', 'smallmoney', 'Monetary data from -214,748.3648 to 214,748.3647', '4 bytes', 'A9F481F7-8392-49CA-8EF8-9EF32BF8CF53', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(21, 'money', 'money', 'Monetary data from -922,337,203,685,477.5808 to 922,337,203,685,477.5807', '8 bytes', '823F3BD1-D365-44AE-8EDD-6EDA4FF347EA', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(22, 'float', 'float(n)', 'Floating precision number data from -1.79E + 308 to 1.79E + 308. The n parameter indicates whether the field should hold 4 or 8 bytes. float(24) holds a 4-byte field and float(53) holds an 8-byte field. Default value of n is 53.', '4 or 8 bytes', '2AE32864-14E6-417B-98E4-69E2F1868DA7', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(23, 'real', 'real', 'Floating precision number data from -3.40E + 38 to 3.40E + 38', '4 bytes', '6F48DCDB-5073-40A9-8C42-65141E7D5403', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(24, 'datetime', 'datetime', 'From January 1, 1753 to December 31, 9999 with an accuracy of 3.33 milliseconds', '8 bytes', 'C276702E-0EEB-4715-B2A5-1C0E8BE0C16E', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(25, 'datetime2', 'datetime2', 'From January 1, 0001 to December 31, 9999 with an accuracy of 100 nanoseconds', '6-8 bytes', 'FAFFD519-4430-41BC-8763-439AC40F7407', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(26, 'smalldatetime', 'smalldatetime', 'From January 1, 1900 to June 6, 2079 with an accuracy of 1 minute', '4 bytes', '016E9490-B9CC-4E4C-BB06-56765D4F3616', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(27, 'date', 'date', 'Store a date only. From January 1, 0001 to December 31, 9999', '3 bytes', '757C2F05-F9A5-4899-BB74-9EA8AD91A338', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(28, 'time', 'time', 'Store a time only to an accuracy of 100 nanoseconds', '3-5 bytes', 'D78BBCED-C524-48AE-B45E-2EDB8C8E986A', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(29, 'datetimeoffset', 'datetimeoffset', 'The same as datetime2 with the addition of a time zone offset', '8-10 bytes', '5DDCDD6A-F8D3-4B75-A8DC-FF740CCBDC79', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(30, 'timestamp', 'timestamp', 'Stores a unique number that gets updated every time a row gets created or modified. The timestamp value is based upon an internal clock and does not correspond to real time. Each table may have only one timestamp variable', '', 'C398686D-3312-4714-98AC-135665CC454A', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(31, 'sql_variant', 'sql_variant', 'Stores up to 8,000 bytes of data of various data types, except text, ntext, and timestamp', '', 'F54D526C-5087-4861-8F1D-158BF84D48A0', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(32, 'uniqueidentifier', 'uniqueidentifier', 'Stores a globally unique identifier (GUID)', '', 'A7E1368C-9ECB-4B71-9F0F-638B44623A5F', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(33, 'xml', 'xml', 'Stores XML formatted data. Maximum 2GB', '', 'C584E8D0-B789-477E-91EA-41E73C0FA271', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(34, 'cursor', 'cursor', 'Stores a reference to a cursor used for database operations', '', 'DFAA08A3-1410-4F7A-B8B5-D6CBE8ACA386', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267'),
		(35, 'table', 'table', 'Stores a result-set for later processing', '', 'E5DE3D47-2170-44F1-BA12-EA153D1F2FEC', '2013-06-21 09:47:18.267', '2013-06-21 09:47:18.267')
	SET IDENTITY_INSERT dbo.DataType OFF;
END

IF NOT EXISTS(SELECT * FROM dbo.ApplicationType)
BEGIN
	SET IDENTITY_INSERT dbo.ApplicationType ON;
	INSERT INTO dbo.ApplicationType
		(ApplicationTypeID, Code, Name, Description, rowguid, DateCreated, DateModified)
	VALUES
		(1, 'WEBSITE', 'Web Site', 'Web based solutions that can be internal or external (public facing).', '3A8E1963-9727-4A53-934C-EFBB0B08598C', '2013-02-19 16:12:42.540', '2013-02-19 16:12:42.540'),
		(2, 'SOFT', 'Installable software', 'Software that can be installed on an internal or external machine.', '21A9606E-99AB-4EDE-9C52-DB66FB3A4797', '2013-02-19 16:12:42.540', '2013-02-19 16:12:42.540'),
		(3, 'UNKNOWN', 'Unknown Application Type', 'The type of application has not been defined in the system.', '9E81C5FE-DDEF-41AB-A65E-4A70B5843C53', '2013-12-19 07:58:35.123', '2013-12-19 07:58:35.123'),
		(6, 'CMD', 'Command', 'Command Line Program 1', '6E9AA7DA-7FAD-4D6A-90AE-C32BCB108018', '2016-02-08 19:30:30.973', '2016-02-10 20:25:33.913'),
		(7, 'cmd1', 'command1', 'Test Type', 'E7E55540-05C1-434E-A36C-95F76B02551B', '2016-02-10 20:26:06.480', '2016-02-10 20:26:06.480'),
		(8, 'SVC', 'Service', 'Service Application', 'FBD3CD29-8166-4184-A014-59E705DC1744', '2016-02-17 17:40:15.410', '2016-02-17 17:40:15.410')
	SET IDENTITY_INSERT dbo.ApplicationType OFF;
END

IF NOT EXISTS(SELECT * FROM dbo.Application)
BEGIN
	SET IDENTITY_INSERT dbo.Application ON;
	INSERT INTO dbo.Application
		(ApplicationID, ApplicationTypeID, Code, Name, Description, LicenseName, ApplicationPath, rowguid, DateCreated, DateModified, CreatedBy, ModifiedBy)
	VALUES
		(1, 1, 'MPC', 'myPharmacyConnect', 'Public facing web site designed for store customers to manage their prescriptions and loyalty program.', NULL, 'http://www.mypharmacyconnect.com:83/Guest/', 'FE0B7C59-8548-405A-8020-491AC2E9DF72', '2013-02-19 16:25:02.517', '2013-02-19 16:25:02.517', NULL, NULL),
		(2, 1, 'ESMT', 'External Store Management Tool', NULL, NULL, NULL, '1161A82B-9BB7-4FC3-9491-457A8197A48C', '2013-10-31 08:53:25.400', '2013-10-31 08:53:25.400', NULL, NULL),
		(3, 1, 'ISMT', 'Internal Support Management Tool', NULL, NULL, 'http://www.mypharmacyconnect.com:83/InternalSupport/Account/Login/', '6F011107-DA7D-4DA2-97E9-A6C18E7893EA', '2013-10-31 08:53:37.890', '2013-10-31 08:53:37.890', NULL, NULL),
		(4, 1, 'PHAT', 'Internal Pharmacy Admin Tool', 'Internal web site designed to allow store configuration administration ', NULL, NULL, 'E528B63B-6F78-4222-8E02-C2964FC3D3F0', '2013-12-12 08:47:48.303', '2013-12-12 08:47:48.303', NULL, NULL),
		(5, 3, 'UNKNOWN', 'Unkown Application', 'The application has not been defined in the system.', NULL, NULL, '55759F27-69A4-43A2-A78B-74FE7D2AA677', '2013-12-19 07:59:40.670', '2013-12-19 07:59:40.670', NULL, NULL),
		(6, 1, 'MDM', 'myDataMart', 'Public facing web site designed for stores to view/investigate their data.', NULL, 'http://www.mydatamart.net/', 'FF2F1D97-B9A6-4248-95FA-8BE98C0F3CEF', '2014-03-13 11:12:30.717', '2014-03-13 11:12:30.717', NULL, NULL),
		(7, 1, 'PCG', 'Product Control Group', 'Internal product monitoring and support site.', NULL, NULL, 'F4E1D99A-9714-4BD6-AA46-C195F0EA3938', '2014-08-27 15:52:46.947', '2014-08-27 15:52:46.947', NULL, NULL),
		(8, 1, 'PARX', 'Prior Authorization', NULL, NULL, NULL, 'F57F8D36-DE77-4132-B52A-7BF3B0070BB2', '2014-08-28 14:09:08.033', '2014-08-28 14:09:08.033', NULL, NULL),
		(9, 1, 'RECON', 'Reconciliation', NULL, NULL, NULL, 'D17C5411-201E-4F91-A32A-8B97D134C250', '2014-11-14 08:49:07.010', '2014-11-14 08:49:07.010', NULL, NULL),
		(10, 1, 'ENG', 'Engage', NULL, NULL, 'http://ec2-54-85-107-85.compute-1.amazonaws.com/Engage.WEB.UI.DEV/', '5A625FF5-9C14-44F0-8B6E-CA9EA866CD98', '2015-01-13 10:38:51.920', '2015-01-13 10:38:51.920', 'dhughes', NULL),
		(11, 1, 'EME', 'Enterprise Messaging Engine', 'The company''s enterprise messaging engine.', NULL, NULL, 'F3DA29FE-7EF3-4BF1-85FA-D77F12FEA91B', '2015-02-12 09:08:46.497', '2015-02-12 09:08:46.497', 'dhughes', NULL),
		(12, 1, 'MIT', 'Messaging Implementation Tool', NULL, NULL, NULL, '61E11473-8295-488D-B42D-731E1D130A51', '2015-04-22 13:29:18.210', '2015-04-22 13:29:18.210', 'dhughes', NULL),
		(13, 2, 'ENGALRT', 'Engage Alert', 'An installable application that mimics Engage WebUI functionality', NULL, NULL, '42245288-A52C-4913-8CB6-0ED14CE25979', '2015-09-04 10:13:45.350', '2015-09-04 10:13:45.350', 'dhughes', NULL),
		(16, 1, 'TST', 'TESTAPP', 'TEST Application 333', NULL, NULL, '83C77689-50AD-486B-9366-5760CAE76A3D', '2016-02-10 19:45:03.780', '2016-02-11 20:22:50.733', 'Alex Korolev', 'Alex Korolev'),
		(17, 8, 'Intake', 'Intake', 'Intake', NULL, NULL, '12FB8886-03CE-4971-B464-1090E4F14E8F', '2016-02-17 17:40:39.080', '2016-02-17 17:40:39.080', 'Alex Korolev', NULL),
		(18, 1, 'FCT', 'FDS Configuration Tools', 'FDS Configuration Tools', NULL, NULL, '7E1435E6-FFC2-43D1-BC6A-C904BCEA1811', '2016-03-07 15:05:19.073', '2016-03-07 15:05:19.073', 'Alex Korolev', NULL)
	SET IDENTITY_INSERT dbo.Application OFF;
END

IF NOT EXISTS(SELECT * FROM dbo.Configuration)
BEGIN
	SET IDENTITY_INSERT dbo.Configuration ON;
	INSERT INTO dbo.Configuration
		(ConfigurationID, ApplicationID, BusinessEntityID, KeyName, KeyValue, DataTypeID, DataSize, Description, rowguid, DateCreated, DateModified, CreatedBy, ModifiedBy)
	VALUES
		(1, NULL, NULL, 'SERVERTIMEZONE', 'CST', '2', NULL, 'POSEIDON server records time in Central Standard Time.', 'D40BE770-F7FD-474E-B76E-57B754360B2B', '2013-01-30 10:00:08.853', '2013-01-30 10:02:04.860', NULL, NULL),
		(2, NULL, NULL, 'DEFAULTLANG', 'en', '2', NULL, 'Default system language.', 'F01619A5-EDC5-4DE9-A937-BDE647DAEE35', '2013-02-07 14:04:12.883', '2013-02-07 14:04:12.883', NULL, NULL),
		(3, NULL, NULL, 'DFLTSTREID', '-1', 16, NULL, 'Default store.  Store is used when standard documents not owned by other stores are needed. (i.e. privacy policy, logos, etc.).', '01C99DB7-1DC6-4140-8722-7D3097AE9FCC', '2013-02-16 11:38:11.697', '2013-02-16 11:38:11.697', NULL, NULL),
		(4, NULL, NULL, 'CredTokenDurationMin', '30', 16, NULL, 'Default credential token duration.', '973A6960-3738-47A0-8507-A37503C3F069', '2013-10-31 08:14:28.087', '2013-10-31 08:14:28.087', NULL, NULL),
		(5, 2, NULL, 'CredTokenDurationMin', '30', 16, NULL, 'Default credential token duration.', 'D548A204-B56D-481D-8034-2D5F3437F23B', '2013-11-01 10:28:21.073', '2013-11-01 10:28:21.073', NULL, NULL),
		(6, NULL, NULL, 'UnknownAuthorID', '-5', 16, NULL, NULL, 'D8C15BB3-335D-4980-84E6-01180006729A', '2013-11-19 09:28:56.937', '2013-11-19 09:28:56.937', NULL, NULL),
		(7, NULL, NULL, 'CredentialLockOutTimeMin', '20', 16, NULL, 'Default lock out time for a credential.', 'B633BA14-4ED6-49DA-87C2-ACC0E3445025', '2014-09-08 14:05:07.470', '2014-09-08 14:05:07.470', NULL, NULL),
		(9, NULL, NULL, 'CredentialLockOutMaxAttempts', '3', 16, NULL, 'Default number of times a credential is allowed to be tried before it is locked by the system.', '0BD8EF36-5203-48E9-8E48-179A32ED2165', '2014-09-08 14:05:57.493', '2014-09-08 14:05:57.493', NULL, NULL),
		(10, NULL, NULL, 'RequestAttemptsMax', '3', 16, NULL, 'The default number of times a request is allowed to be tried before it is locked by the system.', '1BBCCEDF-02E4-4ADF-A914-D6BDCEEE61FB', '2014-10-03 11:47:31.667', '2014-10-03 11:47:31.667', 'dhughes', NULL),
		(11, NULL, NULL, 'RequestKeyExpirationMin', '20', 16, NULL, 'The number of minutes the request is tracked before it expires and starts a new.', '2BB9B4DA-C0F4-42D6-92C8-CFE0CD7D0CED', '2014-10-03 11:50:41.943', '2014-10-03 11:50:41.943', 'dhughes', NULL),
		(16, 10, NULL, 'MedSyncDaysInPeriod', '120', 16, NULL, '# of days in period to qualify as candidate', '06BDB08D-A9A4-4442-BA04-A576D53E71D6', '2014-10-09 09:27:26.667', '2014-10-09 09:27:26.667', 'ssimmons', NULL),
		(18, 10, NULL, 'MedSyncDaysPerPeriod', '2', 16, NULL, 'Times per came in the period to qualify as candidate', '2DB37880-D733-4267-85F4-D839B5AFF0DA', '2014-10-09 10:08:26.620', '2014-10-09 10:08:26.620', 'ssimmons', NULL),
		(20, 10, NULL, 'MedSyncScriptsPerPeriod', '3', 16, NULL, '# of scripts in period to qualify as candidate', '3C744491-3796-46C1-9435-FBA01322E9DE', '2014-10-09 10:08:53.647', '2014-10-09 10:08:53.647', 'ssimmons', NULL),
		(27, 10, NULL, 'MedSyncQueueDays', '3', 16, NULL, 'Days early to place in pharmacy queue', 'DF7AD6E4-43B2-4B14-84E5-41258B0E489E', '2014-11-19 15:44:35.127', '2014-11-19 15:44:35.127', 'dhughes', NULL),
		(28, 10, NULL, 'MedSyncReminderDays', '10', 16, NULL, 'Number of days before the appointment to call the patient.', 'C0893644-3BC7-4A83-B58F-4F2CB67B3B92', '2014-11-19 15:45:00.407', '2014-11-19 15:45:00.407', 'dhughes', NULL),
		(29, 10, NULL, 'MedSyncZeroQtyMMLookback', '30', 16, NULL, 'Number of days to lookup back to include scripts in Recommended list for MM with no qty remaining', 'E7F6C4CE-6144-4D86-BB75-51952E964ACF', '2014-11-21 15:45:03.213', '2014-11-21 15:45:03.213', NULL, NULL),
		(30, 10, NULL, 'MedSyncScoreScriptsMultiplier', '3', 16, NULL, 'Script count Multiplier for determining score', 'D829D252-110C-4ED8-98F6-66827325C5AE', '2014-12-10 10:53:07.040', '2014-12-10 10:53:07.040', 'ssimmons', NULL),
		(32, 10, NULL, 'MedSyncScoreDaysMultiplier', '10', 16, NULL, 'Day count Multiplier for determining score', 'B63B97E1-63EB-44F6-AE20-5DD7FB2084C6', '2014-12-10 10:54:29.250', '2014-12-10 10:54:29.250', 'ssimmons', NULL),
		(33, 10, NULL, 'MedSyncScoreAlertMultiplier', '0', 16, NULL, 'Alert multiplier for determining score', 'EAFF3A57-B3F8-42B2-9D8C-B5EE361BDF4A', '2014-12-10 10:58:50.893', '2014-12-10 10:58:50.893', 'ssimmons', NULL),
		(37, 10, NULL, 'ShowEngagePlanFiltering', 'true', 9, NULL, 'True to show the engage plan filtering menu; otherwise, false to hide.', 'F94620E0-5EC3-4CDD-99FA-B5EF7C68B136', '2015-01-16 11:06:59.980', '2015-01-16 11:06:59.980', 'dhughes', NULL),
		(39, 10, NULL, 'ShowProfileAllergies', 'false', 9, NULL, 'True to show the patient and medical profile allergies widget; otherwise, false to hide.', 'B81239B3-5D49-4EF2-A92C-B2D2F93B0F3F', '2015-01-16 11:28:05.673', '2015-01-16 11:28:05.673', 'dhughes', NULL),
		(40, 10, NULL, 'ShowProfileConditions', 'false', 9, NULL, 'True to show the patient and medical profile conditions widget; otherwise, false to hide.', '8B76FD29-33B9-4F32-87F3-5D80AC2C375E', '2015-01-16 11:28:47.163', '2015-01-16 11:28:47.163', 'dhughes', NULL),
		(44, 10, NULL, 'ShowProfileAdverseReactions', 'false', 9, NULL, 'True to show the patient and medical profile adverse reactions widget; otherwise, false to hide.', 'FAF884BE-1CEF-4792-A36B-58CC7DF1E7CE', '2015-01-16 11:30:04.813', '2015-01-16 11:30:04.813', 'dhughes', NULL),
		(45, 10, NULL, 'ShowProfileImmunizations', 'false', 9, NULL, 'True to show the patient and medical profile immunizations widget; otherwise, false to hide.', '794A455D-2FAD-49CC-8DCC-3308E46B8F82', '2015-01-16 16:22:22.917', '2015-01-16 16:22:22.917', 'dhughes', NULL),
		(46, 10, NULL, 'ShowProfileClinicalMeasures', 'false', 9, NULL, 'True to show the patient and medical profile clinical measures widget; otherwise, false to hide.', '9CB0E978-0187-4611-B9DC-4E0CBB6D6C8B', '2015-01-16 16:22:49.130', '2015-01-16 16:22:49.130', 'dhughes', NULL),
		(47, 10, NULL, 'IsFiveStarReportMeasuresExpandable', 'true', 9, NULL, 'True to have the ability to expand five star measures; otherwise, false to exclude the ability.', 'CDCFCACD-F675-4D5F-9007-CAC8C4C266DD', '2015-01-19 12:42:12.260', '2015-01-19 12:42:12.260', 'dhughes', NULL),
		(48, 10, NULL, 'IsMedSyncCustomModeEnabled', 'false', 9, NULL, 'True to have Med. Sync custom mode enabled; otherwise, false to have the ability disabled.', '1AB32F87-BB81-4892-8F17-1F369DC532A9', '2015-01-19 14:38:59.970', '2015-01-19 14:38:59.970', 'dhughes', NULL),
		(49, 10, NULL, 'ShowDoNotCall', 'true', 9, NULL, 'True to have "do not call" feature available; otherwise, false to have the ability hidden.', 'CCF8A773-A76E-458B-B45A-3135B51B259F', '2015-01-19 15:50:21.103', '2015-01-19 15:50:21.103', 'dhughes', NULL),
		(52, 10, NULL, 'ShowRestrictInsurerCommunications', 'false', 9, NULL, 'True to have "restrict insurer communications" feature available; otherwise, false to have the ability hidden.', 'ADA7D475-AAAD-466B-AA5D-12B091FD4C53', '2015-01-19 15:50:53.703', '2015-01-19 15:50:53.703', 'dhughes', NULL),
		(53, 10, NULL, 'IsFiveStarBannerDrillThroughEnabled', 'false', 9, NULL, 'True to have the ability to drill into 5star plan detail; otherwise, false to have the ability unavailable.', 'F01EFDBF-538B-4514-A83E-B1B8EBF3B783', '2015-01-19 16:08:42.310', '2015-01-19 16:08:42.310', 'dhughes', NULL),
		(54, 10, NULL, 'ShowProfileRecentClaimsActivity', 'false', 9, NULL, 'True to show the patient and medical profile recent claim activity; otherwise, false to hide.', '33E6E373-9D90-4967-A7F5-C0D50C10A11B', '2015-01-23 08:37:32.500', '2015-01-23 08:37:32.500', 'dhughes', NULL),
		(65, 10, NULL, 'ForceNullPharmacyPlanKeyRequest', 'false', 9, NULL, 'True to force the use of a Null pharmacy plan key request which triggers an ALL plans lookup; otherwise, false to use pharamcy plan key request from the UI.', '7B77F3D6-1749-4E34-BADA-2E5D205690D0', '2015-02-02 13:06:47.927', '2015-02-02 13:06:47.927', 'dhughes', NULL),
		(66, 10, NULL, 'ShowProfileDrugCompliance', 'true', 9, NULL, 'True to show the profile drug compliance widget; otherwise, false to hide.', 'DC345DED-942C-4CC7-85C5-7F8A3C6C7517', '2015-02-09 08:39:27.353', '2015-02-09 08:39:27.353', 'dhughes', NULL),
		(67, 10, NULL, 'EnableContactCustomerContextMenuItem', 'false', 9, NULL, 'True to enable the ability to contact customer via the context menu; otherwise, false to disable.', '5A48A653-2C0C-4CCB-914D-E13E2653CD8B', '2015-02-09 11:04:00.123', '2015-02-09 11:04:00.123', 'dhughes', NULL),
		(68, 10, NULL, 'FetchAndShowBusinessTaskHomeWidget', 'true', 9, NULL, 'True to fetch business task data and to show task widget; otherwise, false to not fetch and hide.', '2DF5D9BF-DA82-433B-BFB7-CE2F1360D28E', '2015-02-17 10:18:02.147', '2015-02-17 10:18:02.147', 'dhughes', NULL),
		(101, 10, NULL, 'IsAdhocMessagingEnabled', 'true', 9, NULL, 'True to show the adhoc messaging element link; otherwise, false to hide the adhoc messaging element link.', '2B8C2E05-1346-47F1-B048-9AA84D4A0A63', '2015-06-03 15:24:13.687', '2015-06-03 15:24:13.687', 'dhughes', NULL),
		(103, 10, NULL, 'ShowEngageAdjudicationFiltering', 'true', 9, NULL, NULL, 'DF67D883-85B8-4376-9CA4-2C2EE1EFA6DB', '2015-06-16 12:18:53.803', '2015-06-16 12:18:53.803', 'dhughes', NULL),
		(104, 10, NULL, 'InitialEngagePlanFilterTypeKey', 'phrm', 2, NULL, NULL, 'B9D0DFCD-6489-46A6-BDF8-991201EB7F26', '2015-06-17 17:17:54.700', '2015-06-17 17:17:54.700', 'dhughes', NULL),
		(105, 10, NULL, 'IsFiveStarBannerPatientSelectionEnabled', 'false', 9, NULL, NULL, 'F7275C03-615B-4A85-B067-57D41E1DD58B', '2015-06-20 21:10:27.047', '2015-06-20 21:10:27.047', 'dhughes', NULL),
		(108, 10, NULL, 'IsFiveStarBannerPatientCountsVisible', 'true', 9, NULL, NULL, 'CA0AFEB6-730C-4BEA-8656-127FA514D06D', '2015-06-22 11:49:31.920', '2015-06-22 11:49:31.920', 'dhughes', NULL),
		(109, 10, NULL, 'IsEngageMedSyncOnly', 'false', 9, NULL, NULL, '9B5B1701-5CA0-444E-A8DC-A150BF2A4388', '2015-07-17 16:33:08.483', '2015-07-17 16:33:08.483', 'dhughes', NULL),
		(110, 10, NULL, 'IsEngageMessagingEnabled', 'true', 9, NULL, NULL, 'F30C2DDE-BFB9-47CE-80ED-7CE62AFD43F9', '2015-07-17 16:38:07.263', '2015-07-17 16:38:07.263', 'dhughes', NULL),
		(154, 10, NULL, 'MedSyncAutoRxRenew', 'true', 9, NULL, NULL, 'CE2F59CE-A066-48DF-8811-49BF025D0B3A', '2015-12-14 09:32:12.040', '2015-12-14 09:32:12.040', 'ssimmons', NULL),
		(159, 10, NULL, 'MedSyncQueueDaysNRR', '7', 16, NULL, 'Days early to fill queue if no refills remain', '8828447B-72CA-4183-B996-A5F381A4EE72', '2015-12-28 12:57:16.387', '2015-12-28 12:57:16.387', 'ssimmons', NULL),
		(235, NULL, NULL, 'AK1', '7777', 16, NULL, 'AK1 TEST KEY', '6ACF58FD-90B0-487D-8727-61AD980ED187', '2016-01-14 18:21:43.260', '2016-01-14 18:21:43.260', 'Alex Korolev', NULL),
		(242, 10, NULL, 'AK2', '11', 16, NULL, 'GLOBAL AK2 MULTI APP KEY ENG 1', 'DC27E7CD-E508-4C2D-AC1A-2F6044B39D53', '2016-01-14 18:33:50.100', '2016-01-14 18:43:57.097', 'Alex Korolev', 'Alex Korolev'),
		(243, 13, NULL, 'AK2', '22', 16, NULL, 'GLOBAL AK2 MULTI APP KEY ANG ALERT 1', '810B90D7-98A0-4D59-A8EC-336F8396935D', '2016-01-14 18:33:50.100', '2016-01-14 18:43:57.107', 'Alex Korolev', 'Alex Korolev'),
		(253, 11, NULL, 'AK2', '33', 16, NULL, 'GLOBAL EME', '9DAFFFBE-7D3D-4AD5-BBE4-CB57A60E42DD', '2016-01-14 18:48:06.177', '2016-01-14 18:48:06.177', 'Alex Korolev', NULL),
		(30324, 10, NULL, 'MedSyncDisplayPickupDate', 'false', 9, NULL, NULL, 'D313C707-D751-4D72-B228-F932C4CE024D', '2016-05-16 13:23:32.337', '2016-05-16 13:23:32.337', 'ssimmons', NULL),
		(30328, 10, NULL, 'IsMedSyncBinVerificationQueueEnabled', 'false', 9, NULL, NULL, '2AE3D8B0-6040-4226-A6AD-B2C885C085BB', '2016-06-06 10:03:52.940', '2016-06-06 10:03:52.940', 'dhughes', NULL),
		(30403, 10, NULL, 'IsImmunizationsEnabled', 'false', 9, NULL, 'True to indicate that immunizations are loadable and visible; otherwise, false to indicate immunizations are not to be loaded.', 'EAAF5786-CFA0-43BD-9A23-36004F858430', '2016-09-01 12:53:03.787', '2016-09-01 12:53:03.787', 'Daniel Hughes', NULL),
		(30404, 13, NULL, 'IsImmunizationsEnabled', 'false', 9, NULL, 'True to indicate that immunizations are loadable and visible; otherwise, false to indicate immunizations are not to be loaded.', '559DB811-7E97-4CAE-8423-3F3BD2FBB3B3', '2016-09-01 12:53:03.787', '2016-09-01 12:53:03.787', 'Daniel Hughes', NULL)

	SET IDENTITY_INSERT dbo.Configuration OFF;
END

IF NOT EXISTS(SELECT * FROM acl.Roles)
BEGIN
	SET IDENTITY_INSERT acl.Roles ON;
	INSERT INTO acl.Roles
		(RoleID, Code, Name, Description, rowguid, DateCreated, DateModified, CreatedBy, ModifiedBy)
	VALUES
		(1, 'ADMIN', 'Administrator', 'Complete access to the application and all its settings', '732C970D-A47E-4B36-A2E3-496ED64774F6', '2013-11-07 14:28:31.827', '2013-11-07 14:28:31.827', NULL, NULL),
		(2, 'DEV', 'Developer', 'Can modify all technical settings but cannot delete application, or add additional users', '7CD6FB7C-11A9-4F70-B674-1B2BD7B7B1E0', '2013-11-07 14:29:16.203', '2013-11-07 14:29:16.203', NULL, NULL),
		(3, 'TESTER', 'Tester', 'Can test the application in sandbox mode but can''t modify the application.', '9E7FE4CA-AA85-4EC6-99B2-971AFF7B7A0E', '2013-11-07 14:29:37.907', '2016-03-28 19:28:25.507', NULL, 'Alex Korolev'),
		(4, 'EMP', 'Employee', 'Can access the application but may have limited functionality and settings.', 'A29BF367-5DEA-4099-8EB0-E039707C692D', '2013-11-07 14:30:49.090', '2013-11-07 14:30:49.090', NULL, NULL),
		(5, 'MPCS', 'mPC Support', 'User can access mPC specific functionality within the application', '28532438-4A87-4457-BB1B-1C3F7206C548', '2013-11-12 12:38:27.437', '2013-11-12 12:38:27.437', NULL, NULL),
		(6, 'TIXTECH', 'Ticket Technician', 'User can access ticketing support records as a tech within the application', 'C30481A5-CC5F-4D73-81E9-048F381D32F7', '2013-11-12 12:42:46.410', '2013-11-12 12:42:46.410', NULL, NULL),
		(7, 'TIXADM', 'Ticket Administrator', 'User can access ticketing support records as an admin within the application', 'F831E576-41F3-4EFC-8CE3-A1B1249DAD26', '2013-11-12 12:43:22.007', '2013-11-12 12:43:22.007', NULL, NULL),
		(8, 'LOYADM', 'Loyalty Administrator', 'User can access loyalty specific functionality within the application', '2739E46E-901C-4D74-86DC-D214BAFD01CA', '2013-11-12 12:46:35.887', '2013-11-12 12:46:35.887', NULL, NULL),
		(9, 'MPCR', 'mPC Reader', 'User can access mPC data with read only permissions', '7E59948F-18E5-4DC3-A9A1-59826FE56D31', '2013-11-15 15:03:47.223', '2013-11-15 15:03:47.223', NULL, NULL),
		(10, 'ADD', 'Add', 'User can add objects.', '52B8B06F-701E-4CC5-B9CF-089A9B0626F0', '2014-08-29 14:22:03.677', '2014-08-29 14:22:03.677', NULL, NULL),
		(11, 'DELETE', 'Delete', 'User can delete objects.', '80742BFA-EE59-4C1B-89BF-609DAAEA9DE0', '2014-08-29 14:22:20.563', '2014-08-29 14:22:20.563', NULL, NULL),
		(12, 'UPDATE', 'Update', 'User can update objects.', '8AC349B8-D849-4EBC-BC1E-4E4CFB738EF0', '2014-08-29 14:22:32.707', '2014-08-29 14:22:32.707', NULL, NULL),
		(13, 'READ', 'Read', 'User can read data.', 'B3410462-B7CC-4E26-B979-FDFC363A194F', '2014-08-29 14:25:44.747', '2014-08-29 14:25:44.747', NULL, NULL),
		(14, 'DEMO', 'Demo', 'Allows access to demo related controls and features.', 'D389CA3F-6CD5-47DA-9239-E8CFA5DC367E', '2014-09-08 09:04:27.783', '2014-09-08 09:04:27.783', NULL, NULL),
		(15, 'DRMNT', 'Doctor Maintenance', 'Allows the maintenance of  doctor information.', 'CBE12127-D10A-4ACB-96B4-C9D059143364', '2014-09-19 11:37:54.750', '2014-09-19 11:37:54.750', NULL, NULL),
		(16, 'STRMNT', 'Store Maintenance', 'Allows the maintenance of stores.', '5C759DFD-7304-4AD6-879A-6F8F3ED18EE2', '2014-09-19 11:39:00.007', '2014-09-19 11:39:00.007', NULL, NULL),
		(17, 'USRMNT', 'User Maintenance', 'Allos the maintenance of users.', '72447AD2-CAEB-4662-9EDA-A4D6A47252B8', '2014-09-19 11:39:17.253', '2014-09-19 11:39:17.253', NULL, NULL),
		(18, 'STATLKP', 'Status Lookup', 'Looks up statuses.', '247D16CD-2C69-4262-AAB4-83B5E931F75B', '2014-09-19 11:39:33.380', '2014-09-19 11:39:33.380', NULL, NULL),
		(19, 'DRGEX', 'Drug Exclusion', NULL, '5FF077C5-668E-4936-8921-C988CDE08D8F', '2014-10-09 10:57:04.460', '2014-10-09 10:57:04.460', 'dhughes', NULL),
		(20, 'ENGAGE', 'Engage', 'User can maintain patient opportunities and five star goals.', '19961D4E-B89B-49D3-9F5D-1C4D163D177B', '2014-10-10 15:14:37.783', '2014-10-10 15:14:37.783', 'dhughes', NULL),
		(21, 'RPTVWR', 'Report Viewer', 'User can view reports', '1A410C25-292A-4418-AD42-ED8829B0BFA6', '2015-01-08 16:05:04.080', '2015-01-08 16:05:04.080', 'dhughes', NULL),
		(22, 'SPRADM', 'Super Admin', 'Complete access to the applicaion and all of its setings', '3278AD81-87A2-43BD-8AF7-FC2DB19D1EF2', '2015-01-08 16:05:27.097', '2015-01-08 16:05:27.097', 'dhughes', NULL),
		(23, 'REPORT', 'Reporting', 'Complete access to all reports and their functionality.', 'FE07AB74-DCB4-4C3F-B0D9-240861C84B3E', '2015-01-08 16:05:46.437', '2015-01-08 16:05:46.437', 'dhughes', NULL),
		(24, 'ALL', 'All', NULL, '5350D7DB-867C-40AF-B89B-12340F46EB44', '2016-02-23 07:40:16.573', '2016-02-23 07:40:16.573', 'dhughes', NULL),
		(25, 'USR', 'User', 'Generic User', '478D2D09-95AB-4774-B0E7-9648A5DACD25', '2016-03-10 13:21:01.853', '2016-03-10 13:21:01.853', 'akorolev', NULL),
		(29, 'PLNHIST', 'Plan History', 'Ability to view the history of a plan.', '405A52D4-460E-4D6F-B750-C51B526A5E15', '2016-07-14 07:55:48.250', '2016-07-14 07:55:48.250', 'dhughes', NULL),
		(30, 'SUPADMIN', 'Support Administrator', 'Complete access to the application and all of its settings.', '620F8B2B-F4C3-4250-841C-23F074881629', '2016-07-14 07:56:28.637', '2016-07-14 07:56:28.637', 'dhughes', NULL),
		(32, 'AUDITVWR', 'Audit Viewer', 'User can view auditing information.', 'D15FB284-4083-4A99-96E9-C661FC1D34EA', '2016-07-14 11:41:55.063', '2016-07-14 11:41:55.063', 'dhughes', NULL)

	SET IDENTITY_INSERT acl.Roles OFF;
END

IF NOT EXISTS(SELECT * FROM news.ArticleFormat)
BEGIN
	SET IDENTITY_INSERT news.ArticleFormat ON;
	INSERT INTO news.ArticleFormat
		(ArticleFormatID, Code, Name)
	VALUES
		(1, 'UNKN', 'Unknown'),
		(2, 'TXT', 'Text'),
		(3, 'HTML', 'Html'),
		(4, 'VID', 'Video'),
		(5, 'AUD', 'Audio'),
		(6, 'DOC', 'Document'),
		(10, 'TST', 'TEST')
	SET IDENTITY_INSERT news.ArticleFormat OFF;
END

IF NOT EXISTS(SELECT * FROM news.ArticleType)
BEGIN
	SET IDENTITY_INSERT news.ArticleType ON;
	INSERT INTO news.ArticleType
		(ArticleTypeID, Code, Name)
	VALUES
		(1, 'INFO', 'Informational'),
		(2, 'INS', 'Instructional'),
		(3, 'UNKN', 'Unknown')
	SET IDENTITY_INSERT news.ArticleType OFF;
END

IF NOT EXISTS(SELECT * FROM news.Category)
BEGIN
	SET IDENTITY_INSERT news.Category ON;
	INSERT INTO news.Category
		(CategoryID, Code, Name)
	VALUES
		(1, 'GEN', 'General'),
		(2, 'OPP', 'Opportunities'),
		(3, 'MS', 'Med Sync'),
		(4, 'QUE', 'Queues'),
		(5, 'MSG', 'Messaging'),
		(6, 'RPT', 'Reporting'),
		(7, 'TRAIN', 'Training')
	SET IDENTITY_INSERT news.Category OFF;
END

IF NOT EXISTS(SELECT * FROM dbo.TimeZone)
BEGIN
	SET IDENTITY_INSERT dbo.TimeZone ON;
	INSERT INTO dbo.TimeZone
		(TimeZoneID, Code, Prefix, Location, GMT, Offset)
	VALUES
		(1, NULL, NULL, 'International Date Line West', '(GMT-12:00)', -12),
		(2, NULL, NULL, 'Midway Island', '(GMT-11:00)', -11),
		(3, NULL, NULL, 'Samoa', '(GMT-11:00)', -11),
		(4, 'HAST', 'HA', 'Hawaii', '(GMT-10:00)', -10),
		(5, 'AKST', 'AK', 'Alaska', '(GMT-09:00)', -9),
		(6, 'PST', 'P', 'Pacific Time (US & Canada)', '(GMT-08:00)', -8),
		(7, NULL, NULL, 'Tijuana', '(GMT-08:00)', -8),
		(8, NULL, NULL, 'Arizona', '(GMT-07:00)', -7),
		(9, 'MST', 'M', 'Mountain Time (US & Canada)', '(GMT-07:00)', -7),
		(10, NULL, NULL, 'Chihuahua', '(GMT-07:00)', -7),
		(11, NULL, NULL, 'La Paz', '(GMT-07:00)', -7),
		(12, NULL, NULL, 'Mazatlan', '(GMT-07:00)', -7),
		(13, 'CST', 'C', 'Central Time (US & Canada)', '(GMT-06:00)', -6),
		(14, NULL, NULL, 'Central America', '(GMT-06:00)', -6),
		(15, NULL, NULL, 'Guadalajara', '(GMT-06:00)', -6),
		(16, NULL, NULL, 'Mexico City', '(GMT-06:00)', -6),
		(17, NULL, NULL, 'Monterrey', '(GMT-06:00)', -6),
		(18, NULL, NULL, 'Saskatchewan', '(GMT-06:00)', -6),
		(19, 'EST', 'E', 'Eastern Time (US & Canada)', '(GMT-05:00)', -5),
		(20, NULL, NULL, 'Indiana (East)', '(GMT-05:00)', -5),
		(21, NULL, NULL, 'Bogota', '(GMT-05:00)', -5),
		(22, NULL, NULL, 'Lima', '(GMT-05:00)', -5),
		(23, NULL, NULL, 'Quito', '(GMT-05:00)', -5),
		(24, NULL, NULL, 'Atlantic Time (Canada)', '(GMT-04:00)', -4),
		(25, NULL, NULL, 'Caracas', '(GMT-04:00)', -4),
		(26, NULL, NULL, 'La Paz', '(GMT-04:00)', -4),
		(27, NULL, NULL, 'Santiago', '(GMT-04:00)', -4),
		(28, NULL, NULL, 'Newfoundland', '(GMT-03:30)', -3),
		(29, NULL, NULL, 'Brasilia', '(GMT-03:00)', -3),
		(30, NULL, NULL, 'Buenos Aires', '(GMT-03:00)', -3),
		(31, NULL, NULL, 'Georgetown', '(GMT-03:00)', -3),
		(32, NULL, NULL, 'Greenland', '(GMT-03:00)', -3),
		(33, NULL, NULL, 'Mid-Atlantic', '(GMT-02:00)', -2),
		(34, NULL, NULL, 'Azores', '(GMT-01:00)', -1),
		(35, NULL, NULL, 'Cape Verde Is.', '(GMT-01:00)', -1),
		(36, NULL, NULL, 'Casablanca', '(GMT)', 0),
		(37, NULL, NULL, 'Dublin', '(GMT)', 0),
		(38, NULL, NULL, 'Edinburgh', '(GMT)', 0),
		(39, NULL, NULL, 'Lisbon', '(GMT)', 0),
		(40, NULL, NULL, 'London', '(GMT)', 0),
		(41, NULL, NULL, 'Monrovia', '(GMT)', 0),
		(42, NULL, NULL, 'Amsterdam', '(GMT+01:00)', 1),
		(43, NULL, NULL, 'Belgrade', '(GMT+01:00)', 1),
		(44, NULL, NULL, 'Berlin', '(GMT+01:00)', 1),
		(45, NULL, NULL, 'Bern', '(GMT+01:00)', 1),
		(46, NULL, NULL, 'Bratislava', '(GMT+01:00)', 1),
		(47, NULL, NULL, 'Brussels', '(GMT+01:00)', 1),
		(48, NULL, NULL, 'Budapest', '(GMT+01:00)', 1),
		(49, NULL, NULL, 'Copenhagen', '(GMT+01:00)', 1),
		(50, NULL, NULL, 'Ljubljana', '(GMT+01:00)', 1),
		(51, NULL, NULL, 'Madrid', '(GMT+01:00)', 1),
		(52, NULL, NULL, 'Paris', '(GMT+01:00)', 1),
		(53, NULL, NULL, 'Prague', '(GMT+01:00)', 1),
		(54, NULL, NULL, 'Rome', '(GMT+01:00)', 1),
		(55, NULL, NULL, 'Sarajevo', '(GMT+01:00)', 1),
		(56, NULL, NULL, 'Skopje', '(GMT+01:00)', 1),
		(57, NULL, NULL, 'Stockholm', '(GMT+01:00)', 1),
		(58, NULL, NULL, 'Vienna', '(GMT+01:00)', 1),
		(59, NULL, NULL, 'Warsaw', '(GMT+01:00)', 1),
		(60, NULL, NULL, 'West Central Africa', '(GMT+01:00)', 1),
		(61, NULL, NULL, 'Zagreb', '(GMT+01:00)', 1),
		(62, NULL, NULL, 'Athens', '(GMT+02:00)', 2),
		(63, NULL, NULL, 'Bucharest', '(GMT+02:00)', 2),
		(64, NULL, NULL, 'Cairo', '(GMT+02:00)', 2),
		(65, NULL, NULL, 'Harare', '(GMT+02:00)', 2),
		(66, NULL, NULL, 'Helsinki', '(GMT+02:00)', 2),
		(67, NULL, NULL, 'Istanbul', '(GMT+02:00)', 2),
		(68, NULL, NULL, 'Jerusalem', '(GMT+02:00)', 2),
		(69, NULL, NULL, 'Kyev', '(GMT+02:00)', 2),
		(70, NULL, NULL, 'Minsk', '(GMT+02:00)', 2),
		(71, NULL, NULL, 'Pretoria', '(GMT+02:00)', 2),
		(72, NULL, NULL, 'Riga', '(GMT+02:00)', 2),
		(73, NULL, NULL, 'Sofia', '(GMT+02:00)', 2),
		(74, NULL, NULL, 'Tallinn', '(GMT+02:00)', 2),
		(75, NULL, NULL, 'Vilnius', '(GMT+02:00)', 2),
		(76, NULL, NULL, 'Baghdad', '(GMT+03:00)', 3),
		(77, NULL, NULL, 'Kuwait', '(GMT+03:00)', 3),
		(78, NULL, NULL, 'Moscow', '(GMT+03:00)', 3),
		(79, NULL, NULL, 'Nairobi', '(GMT+03:00)', 3),
		(80, NULL, NULL, 'Riyadh', '(GMT+03:00)', 3),
		(81, NULL, NULL, 'St. Petersburg', '(GMT+03:00)', 3),
		(82, NULL, NULL, 'Volgograd', '(GMT+03:00)', 3),
		(83, NULL, NULL, 'Tehran', '(GMT+03:30)', 3),
		(84, NULL, NULL, 'Abu Dhabi', '(GMT+04:00)', 4),
		(85, NULL, NULL, 'Baku', '(GMT+04:00)', 4),
		(86, NULL, NULL, 'Muscat', '(GMT+04:00)', 4),
		(87, NULL, NULL, 'Tbilisi', '(GMT+04:00)', 4),
		(88, NULL, NULL, 'Yerevan', '(GMT+04:00)', 4),
		(89, NULL, NULL, 'Kabul', '(GMT+04:30)', 4),
		(90, NULL, NULL, 'Ekaterinburg', '(GMT+05:00)', 5),
		(91, NULL, NULL, 'Islamabad', '(GMT+05:00)', 5),
		(92, NULL, NULL, 'Karachi', '(GMT+05:00)', 5),
		(93, NULL, NULL, 'Tashkent', '(GMT+05:00)', 5),
		(94, NULL, NULL, 'Chennai', '(GMT+05:30)', 5),
		(95, NULL, NULL, 'Kolkata', '(GMT+05:30)', 5),
		(96, NULL, NULL, 'Mumbai', '(GMT+05:30)', 5),
		(97, NULL, NULL, 'New Delhi', '(GMT+05:30)', 5),
		(98, NULL, NULL, 'Kathmandu', '(GMT+05:45)', 5),
		(99, NULL, NULL, 'Almaty', '(GMT+06:00)', 6),
		(100, NULL, NULL, 'Astana', '(GMT+06:00)', 6),
		(101, NULL, NULL, 'Dhaka', '(GMT+06:00)', 6),
		(102, NULL, NULL, 'Novosibirsk', '(GMT+06:00)', 6),
		(103, NULL, NULL, 'Sri Jayawardenepura', '(GMT+06:00)', 6),
		(104, NULL, NULL, 'Rangoon', '(GMT+06:30)', 6),
		(105, NULL, NULL, 'Bangkok', '(GMT+07:00)', 7),
		(106, NULL, NULL, 'Hanoi', '(GMT+07:00)', 7),
		(107, NULL, NULL, 'Jakarta', '(GMT+07:00)', 7),
		(108, NULL, NULL, 'Krasnoyarsk', '(GMT+07:00)', 7),
		(109, NULL, NULL, 'Beijing', '(GMT+08:00)', 8),
		(110, NULL, NULL, 'Chongqing', '(GMT+08:00)', 8),
		(111, NULL, NULL, 'Hong Kong', '(GMT+08:00)', 8),
		(112, NULL, NULL, 'Irkutsk', '(GMT+08:00)', 8),
		(113, NULL, NULL, 'Kuala Lumpur', '(GMT+08:00)', 8),
		(114, NULL, NULL, 'Perth', '(GMT+08:00)', 8),
		(115, NULL, NULL, 'Singapore', '(GMT+08:00)', 8),
		(116, NULL, NULL, 'Taipei', '(GMT+08:00)', 8),
		(117, NULL, NULL, 'Ulaan Bataar', '(GMT+08:00)', 8),
		(118, NULL, NULL, 'Urumqi', '(GMT+08:00)', 8),
		(119, NULL, NULL, 'Osaka', '(GMT+09:00)', 9),
		(120, NULL, NULL, 'Sapporo', '(GMT+09:00)', 9),
		(121, NULL, NULL, 'Seoul', '(GMT+09:00)', 9),
		(122, NULL, NULL, 'Tokyo', '(GMT+09:00)', 9),
		(123, NULL, NULL, 'Yakutsk', '(GMT+09:00)', 9),
		(124, NULL, NULL, 'Adelaide', '(GMT+09:30)', 9),
		(125, NULL, NULL, 'Darwin', '(GMT+09:30)', 9),
		(126, NULL, NULL, 'Brisbane', '(GMT+10:00)', 10),
		(127, NULL, NULL, 'Canberra', '(GMT+10:00)', 10),
		(128, NULL, NULL, 'Guam', '(GMT+10:00)', 10),
		(129, NULL, NULL, 'Hobart', '(GMT+10:00)', 10),
		(130, NULL, NULL, 'Melbourne', '(GMT+10:00)', 10),
		(131, NULL, NULL, 'Port Moresby', '(GMT+10:00)', 10),
		(132, NULL, NULL, 'Sydney', '(GMT+10:00)', 10),
		(133, NULL, NULL, 'Vladivostok', '(GMT+10:00)', 10),
		(134, NULL, NULL, 'Magadan', '(GMT+11:00)', 11),
		(135, NULL, NULL, 'New Caledonia', '(GMT+11:00)', 11),
		(136, NULL, NULL, 'Solomon Is.', '(GMT+11:00)', 11),
		(137, NULL, NULL, 'Auckland', '(GMT+12:00)', 12),
		(138, NULL, NULL, 'Fiji', '(GMT+12:00)', 12),
		(139, NULL, NULL, 'Kamchatka', '(GMT+12:00)', 12),
		(140, NULL, NULL, 'Marshall Is.', '(GMT+12:00)', 12),
		(141, NULL, NULL, 'Wellington', '(GMT+12:00)', 12),
		(142, NULL, NULL, 'Nuku''alofa', '(GMT+13:00)', 13)

	SET IDENTITY_INSERT dbo.TimeZone OFF;
END
