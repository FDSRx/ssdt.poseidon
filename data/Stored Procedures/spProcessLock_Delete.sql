﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/13/2014
-- Description:	Deletes a process lock.
-- SAMPLE CALL:
/*

DECLARE
	@LockToken UNIQUEIDENTIFIER = '32D25544-72AD-4D95-A2AC-DAC6709F1EC1'

EXEC data.spProcessLock_Delete
	@LockToken = @LockToken

*/

-- SELECT * FROM data.ProcessLock
-- =============================================
CREATE PROCEDURE [data].[spProcessLock_Delete]
	@LockToken UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------------------------------------------------------------------------------------------
	-- Short circut delete.
	------------------------------------------------------------------------------------------
	IF @LockToken IS NULL
	BEGIN
		RETURN;
	END

	------------------------------------------------------------------------------------------
	-- Delete lock object.
	------------------------------------------------------------------------------------------
	DELETE
	--SELECT *
	FROM data.ProcessLock
	WHERE LockToken = @LockToken
END