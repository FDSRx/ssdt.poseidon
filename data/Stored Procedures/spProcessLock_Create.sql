﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 6/13/2014
-- Description:	Creates a process lock.
-- SAMPLE CALL:
/*

DECLARE
	@ProcessName VARCHAR(256) = 'TEST_PROCESS',
	@ProcessKey VARCHAR(256) = '1234',
	@LockToken UNIQUEIDENTIFIER = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@Debug BIT = 1

EXEC data.spProcessLock_Create
	@ProcessName = @ProcessName,
	@ProcessKey = @ProcessKey,
	@LockToken = @LockToken OUTPUT,
	@CreatedBy = @CreatedBy,
	@Debug = @Debug

SELECT @LockToken AS LockToken

*/

-- SELECT * FROM data.ProcessLock
-- =============================================
CREATE PROCEDURE [data].[spProcessLock_Create] 
	@ProcessName VARCHAR(256),
	@ProcessKey VARCHAR(256),
	@CreatedBy VARCHAR(256) = NULL,
	@LockToken UNIQUEIDENTIFIER = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------
	-- Sanitize input
	---------------------------------------------------------------------------------------------------
	SET @LockToken = NULL;
	SET @Debug = ISNULL(@Debug, 0);
	SET @CreatedBy = ISNULL(@CreatedBy, SUSER_NAME());
	
	---------------------------------------------------------------------------------------------------
	-- Create lock token.
	-- <Summary>
	-- Creates a new token that is unique to the caller's session.
	-- </Summary>
	---------------------------------------------------------------------------------------------------
	SET @LockToken = NEWID();
	
	BEGIN TRY
		
		INSERT INTO data.ProcessLock (
			ProcessName,
			ProcessKey,
			LockToken,
			CreatedBy
		)
		SELECT
			@ProcessName,
			@ProcessKey,
			@LockToken,
			@CreatedBy
	
	END TRY
	BEGIN CATCH
	
		SET @LockToken = NULL;
	
	END CATCH	
	
END