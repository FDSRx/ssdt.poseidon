﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/14/2015
-- Description:	Returns the Parameter object identifier.

-- SAMPLE CALL: SELECT data.fnGetParameterID('DRLB')
-- SAMPLE CALL: SELECT data.fnGetParameterID(4)

-- SELECT * FROM data.Parameter
-- =============================================
CREATE FUNCTION data.fnGetParameterID 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT ParameterID FROM data.Parameter WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT ParameterID FROM data.Parameter WHERE ParameterID = @Key);
	END

	-- Return the result of the function
	RETURN @ID;

END

