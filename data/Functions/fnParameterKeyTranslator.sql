﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 9/14/2015
-- Description: Gets the translated Parameter key or keys.
-- References: dbo.fnSlice
-- References: dbo.fnSplit

-- SAMPLE CALL: SELECT data.fnParameterKeyTranslator(1, 'ParameterID')
-- SAMPLE CALL: SELECT data.fnParameterKeyTranslator('DRLB', DEFAULT)
-- SAMPLE CALL: SELECT data.fnParameterKeyTranslator(3, DEFAULT)
-- SAMPLE CALL: SELECT data.fnParameterKeyTranslator('1,2,3', 'ParameterIDList')
-- SAMPLE CALL: SELECT data.fnParameterKeyTranslator('DRLB,MINFILL,MINAGE', 'CodeList')
-- SAMPLE CALL: SELECT data.fnParameterKeyTranslator('DRLB,MINFILL,MINAGE', DEFAULT)
-- SAMPLE CALL: SELECT data.fnParameterKeyTranslator('1,2,3', DEFAULT)

-- SELECT * FROM data.Parameter
-- =============================================
CREATE FUNCTION data.[fnParameterKeyTranslator]
(
	@Key VARCHAR(MAX),
	@KeyType VARCHAR(50) = NULL
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE 
		@Ids VARCHAR(MAX)
	
	IF RIGHT(@Key, 1) = ','
	BEGIN
		SET @Key = NULLIF(LEFT(@Key, LEN(@Key) - 1), '');
	END	
	
	-- Determine if there is a value to evaluate.
	IF @Key IS NULL OR @Key = ''
	BEGIN
		RETURN @Ids;
	END	
	
	-- Evaluate input and translate accordingly.
	SET @Ids = 
		CASE
			WHEN @KeyType IN ('Id', 'ParameterID') AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ParameterID) 
				FROM data.Parameter
				WHERE ParameterID = CONVERT(INT, @Key)
			)
			WHEN @KeyType = 'Code' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ParameterID) 
				FROM data.Parameter
				WHERE Code = @Key
			)
			WHEN @KeyType = 'Name' THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ParameterID) 
				FROM data.Parameter
				WHERE Name = @Key
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 1 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ParameterID) 
				FROM data.Parameter
				WHERE ParameterID = CONVERT(INT, @Key)
			)
			WHEN ISNULL(@KeyType, '') = '' AND ISNUMERIC(@Key) = 0 AND CHARINDEX(',', @Key) <= 0 THEN (
				SELECT TOP 1 CONVERT(VARCHAR, ParameterID) 
				FROM data.Parameter
				WHERE Code = @Key
			)
			WHEN @KeyType IN ('IdList', 'ParameterIDList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ParameterID), '') + ','
				FROM data.Parameter
				WHERE ParameterID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('CodeList', 'ParameterCodeList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ParameterID), '') + ','
				FROM data.Parameter
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN @KeyType IN ('NameList', 'ParameterNameList') THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ParameterID), '') + ','
				FROM data.Parameter
				WHERE Name IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 1 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ParameterID), '') + ','
				FROM data.Parameter
				WHERE ParameterID IN (SELECT Value FROM dbo.fnSplit(@Key, ',') WHERE ISNUMERIC(Value) = 1)
				FOR XML PATH('')
			)
			WHEN ISNULL(@KeyType, '') = '' AND CHARINDEX(',', @Key) > 0 AND ISNUMERIC(dbo.fnSlice(@Key, ',', 0)) = 0 THEN (
				SELECT ISNULL(CONVERT(VARCHAR, ParameterID), '') + ','
				FROM data.Parameter
				WHERE Code IN (SELECT Value FROM dbo.fnSplit(@Key, ','))
				FOR XML PATH('')
			)			
			ELSE NULL
		END	

	IF RIGHT(@Ids, 1) = ','
	BEGIN
		SET @Ids = NULLIF(LEFT(@Ids, LEN(@Ids) - 1), '');
	END	
	
	-- Return the result of the function
	RETURN @Ids;

END
