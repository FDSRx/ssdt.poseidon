﻿CREATE TABLE [data].[Parameter] (
    [ParameterID]  INT              IDENTITY (1, 1) NOT NULL,
    [Code]         VARCHAR (25)     NOT NULL,
    [Name]         VARCHAR (256)    NOT NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Parameter_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Parameter_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Parameter_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Parameter] PRIMARY KEY CLUSTERED ([ParameterID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Parameter_Code]
    ON [data].[Parameter]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Parameter_Name]
    ON [data].[Parameter]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Parameter_rowguid]
    ON [data].[Parameter]([rowguid] ASC);

