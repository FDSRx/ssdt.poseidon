﻿CREATE TABLE [data].[ProcessLock] (
    [ProcessName] VARCHAR (256)    NOT NULL,
    [ProcessKey]  VARCHAR (256)    NOT NULL,
    [LockToken]   UNIQUEIDENTIFIER CONSTRAINT [DF_ProcessLock_LockToken] DEFAULT (newid()) NOT NULL,
    [DateCreated] DATETIME         CONSTRAINT [DF_ProcessLock_DateCreated] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_ProcessLock] PRIMARY KEY CLUSTERED ([ProcessName] ASC, [ProcessKey] ASC)
);

