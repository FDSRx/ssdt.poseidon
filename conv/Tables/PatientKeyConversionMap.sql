﻿CREATE TABLE [conv].[PatientKeyConversionMap] (
    [PatientKeyConversionMapID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [DataSourceID]              INT              NOT NULL,
    [BusinessID]                BIGINT           NOT NULL,
    [Nabp]                      VARCHAR (50)     NULL,
    [PatientID]                 BIGINT           NULL,
    [PatientKeyTypeID]          INT              NOT NULL,
    [PatientKeySource]          VARCHAR (50)     NOT NULL,
    [PatientKeyTarget]          VARCHAR (50)     NOT NULL,
    [Name]                      VARCHAR (256)    NULL,
    [DateEffective]             DATETIME         NULL,
    [DateTermed]                DATETIME         NULL,
    [TransactionGuid]           UNIQUEIDENTIFIER NULL,
    [rowguid]                   UNIQUEIDENTIFIER CONSTRAINT [DF_PatientKeyConversionMap_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]               DATETIME         CONSTRAINT [DF_PatientKeyConversionMap_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]              DATETIME         CONSTRAINT [DF_PatientKeyConversionMap_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                 VARCHAR (256)    NULL,
    [ModifiedBy]                VARCHAR (256)    NULL,
    CONSTRAINT [PK_PatientKeyConversionMap] PRIMARY KEY CLUSTERED ([PatientKeyConversionMapID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_PatientKeyConversionMap_Business] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[Business] ([BusinessEntityID]),
    CONSTRAINT [FK_PatientKeyConversionMap_DataSource] FOREIGN KEY ([DataSourceID]) REFERENCES [dbo].[DataSource] ([DataSourceID]),
    CONSTRAINT [FK_PatientKeyConversionMap_PatientKeyType] FOREIGN KEY ([PatientKeyTypeID]) REFERENCES [phrm].[PatientKeyType] ([PatientKeyTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PatientKeyConversionMap_DataSrcBizPatKeySourceTarget]
    ON [conv].[PatientKeyConversionMap]([DataSourceID] ASC, [BusinessID] ASC, [PatientKeyTypeID] ASC, [PatientKeySource] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_PatientKeyConversionMap_rowguid]
    ON [conv].[PatientKeyConversionMap]([rowguid] ASC);

