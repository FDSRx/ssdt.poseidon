﻿CREATE TABLE [hr].[Employee] (
    [BusinessEntityID] BIGINT           NOT NULL,
    [NationalIDNumber] NVARCHAR (15)    NULL,
    [LoginID]          NVARCHAR (256)   NULL,
    [JobTitle]         NVARCHAR (50)    NOT NULL,
    [BirthDate]        DATE             NOT NULL,
    [MaritalStatus]    NCHAR (1)        NOT NULL,
    [HireDate]         DATE             NOT NULL,
    [IsSalaried]       BIT              CONSTRAINT [DF_Employee_IsSalaried] DEFAULT ((0)) NOT NULL,
    [VacationHours]    SMALLINT         CONSTRAINT [DF_Employee_VacationHours] DEFAULT ((0)) NOT NULL,
    [SickLeaveHours]   SMALLINT         CONSTRAINT [DF_Employee_SickLeaveHours] DEFAULT ((0)) NOT NULL,
    [rowguid]          UNIQUEIDENTIFIER CONSTRAINT [DF_Employee_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]      DATETIME         CONSTRAINT [DF_Employee_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]     DATETIME         CONSTRAINT [DF_Employee_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (256)    NULL,
    [ModifiedBy]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([BusinessEntityID] ASC),
    CONSTRAINT [FK_Employee_Person] FOREIGN KEY ([BusinessEntityID]) REFERENCES [dbo].[Person] ([BusinessEntityID])
);

