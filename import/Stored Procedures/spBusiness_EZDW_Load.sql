﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 12/11/2015
-- Description:	Imports store and chain data from EZDW.
-- Change log :
-- 9/8/2016 - Nullified the code as it is currently not in a working state. The procedure has pointers
--		to LPS servers that are no longer within the FDS domain.

-- SAMPLE CALL:
/*

DECLARE
	@StoreID VARCHAR(MAX) = NULL,
	@ChainID VARCHAR(MAX) = NULL,
	@Nabp VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = 'dhughes',
	@ModifiedBy VARCHAR(256) = 'dhughes'

EXEC import.spBusiness_EZDW_Load
	@StoreID = @StoreID,
	@ChainID = @ChainID,
	@Nabp = @Nabp,
	@CreatedBy = @CreatedBy,
	@ModifiedBy = @ModifiedBy

*/


-- SELECT * FROM dbo.vwChain
-- SELECT * FROM dbo.vwStore
-- SELECT * FROM dbo.ChainStore
-- SELECT * FROM etl.vwBatchJobStep
-- SELECT * FROM etl.vwBatchJobExecution
-- SELECT * FROM etl.vwBatchStepExecution
-- SELECT * FROM dbo.ErrorLog
-- =============================================
CREATE PROCEDURE [import].[spBusiness_EZDW_Load]
	@StoreID VARCHAR(MAX) = NULL,
	@ChainID VARCHAR(MAX) = NULL,
	@Nabp VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*

	--------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------
	DECLARE
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL	
	
	
	--------------------------------------------------------------------------------------------------------
	-- Record request.
	--------------------------------------------------------------------------------------------------------
	-- Log incoming arguments
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@StoreID=' + dbo.fnToStringOrEmpty(@StoreID) + ';' +
		'@ChainID=' + dbo.fnToStringOrEmpty(@ChainID) + ';' +
		'@Nabp=' + dbo.fnToStringOrEmpty(@Nabp) + ';' +
		'@CreatedBy=' + dbo.fnToStringOrEmpty(@CreatedBy) + ';' ;

	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/	

	-------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	-------------------------------------------------------------------------------------------------------------------
	SET @CreatedBy = ISNULL(@CreatedBy, SUSER_NAME());
	SET @ModifiedBy = COALESCE(@ModifiedBy, @CreatedBy, SUSER_NAME());
	SET @Debug = ISNULL(@Debug, 0);


	-------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	-------------------------------------------------------------------------------------------------------------------
	DECLARE
		@BatchJobExecutionID BIGINT = NULL,
		@BatchStepExecutionID BIGINT = NULL,
		@TransactionDate DATETIME = GETDATE(),
		@Parameters VARCHAR(MAX) = NULL


	-------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	-------------------------------------------------------------------------------------------------------------------
	SET @Parameters = etl.fnAppendParameter(@Parameters, 'StoreID', @StoreID, 'VARCHAR(MAX)');
	SET @Parameters = etl.fnAppendParameter(@Parameters, 'ChainID', @ChainID, 'VARCHAR(MAX)');
	SET @Parameters = etl.fnAppendParameter(@Parameters, 'NABP', @Nabp, 'VARCHAR(MAX)');
	SET @Parameters = etl.fnAppendParameter(@Parameters, 'CreatedBy', @CreatedBy, 'VARCHAR(256)');
	SET @Parameters = etl.fnAppendParameter(@Parameters, 'ModifiedBy', @ModifiedBy, 'VARCHAR(256)');

					
	-------------------------------------------------------------------------------------------------------------------
	-- Unit of work.
	-- <Summary>
	-- Processes the request.
	-- </Summary>
	-------------------------------------------------------------------------------------------------------------------	
	BEGIN TRY

		-------------------------------------------------------------------------------------------------------------------
		-- Create parent batch object.
		-------------------------------------------------------------------------------------------------------------------	
		-- SELECT * FROM etl.BatchJobInstance
		EXEC etl.spBatchJobExecution_Create
			@BatchExecutionID = @BatchJobExecutionID OUTPUT,
			@BatchJobInstanceCode = 'LPSBIZDL',
			@DateStart = @TransactionDate,
			@ExecutionStatusCode = 'R',
			@Parameters = @Parameters,
			@CreatedBy = @CreatedBy,
			@Debug = @Debug

		-------------------------------------------------------------------------------------------------------------------
		-- Merge chain data.
		-- <Summary>
		-- Creates/Updates chain data.
		-- </Summary.
		-------------------------------------------------------------------------------------------------------------------
		SET @Parameters = NULL;
		SET @Parameters = etl.fnAppendParameter(@Parameters, 'ChainID', @ChainID, 'VARCHAR(MAX)');
		SET @Parameters = etl.fnAppendParameter(@Parameters, 'CreatedBy', @CreatedBy, 'VARCHAR(256)');

		-- Create and run an EZDW chain data load step.
		EXEC etl.spBatchStepExecution_Create
			@BatchJobExecutionID = @BatchJobExecutionID,
			@BatchStepInstanceCode = 'CHNDL',
			@DateStart = @TransactionDate,
			@ExecutionStatusCode = 'R',
			@Parameters = @Parameters,
			@CreatedBy = @CreatedBy,
			@Debug = @Debug,
			@BatchExecutionID = @BatchStepExecutionID OUTPUT

		-- Load EZDW chain data.
		EXEC import.spChain_EZDW_Merge
			@ChainID = @ChainID,
			@CreatedBy = @CreatedBy

		SET @TransactionDate = GETDATE();

		-- Reflect the step has finished.	
		EXEC etl.spBatchStepExecution_Update
			@BatchExecutionID = @BatchStepExecutionID,
			@DateEnd = @TransactionDate,
			@ExecutionStatusCode = 'S',
			@Message = 'The EZDW chain data load step has completed.',
			@ModifiedBy = @CreatedBy,
			@Debug = @Debug

		-------------------------------------------------------------------------------------------------------------------
		-- Merge store data.
		-- <Summary>
		-- Creates/Updates store data.
		-- </Summary.
		-------------------------------------------------------------------------------------------------------------------
		SET @Parameters = NULL;
		SET @Parameters = etl.fnAppendParameter(@Parameters, 'StoreID', @StoreID, 'VARCHAR(MAX)');
		SET @Parameters = etl.fnAppendParameter(@Parameters, 'ChainID', @ChainID, 'VARCHAR(MAX)');
		SET @Parameters = etl.fnAppendParameter(@Parameters, 'NABP', @Nabp, 'VARCHAR(MAX)');
		SET @Parameters = etl.fnAppendParameter(@Parameters, 'CreatedBy', @CreatedBy, 'VARCHAR(256)');

		SET @BatchStepExecutionID = NULL;
		SET @TransactionDate = GETDATE();

		-- Create and run an EZDW chain data load step.
		EXEC etl.spBatchStepExecution_Create
			@BatchJobExecutionID = @BatchJobExecutionID,
			@BatchStepInstanceCode = 'STREDL',
			@DateStart = @TransactionDate,
			@ExecutionStatusCode = 'R',
			@Parameters = @Parameters,
			@CreatedBy = @CreatedBy,
			@Debug = @Debug,
			@BatchExecutionID = @BatchStepExecutionID OUTPUT

		-- Load EZDW store data.
		EXEC import.spStore_EZDW_Merge
			@StoreID = @StoreID,
			@ChainID = @ChainID,
			@Nabp = @Nabp,
			@CreatedBy = @CreatedBy

		SET @TransactionDate = GETDATE();

		-- Reflect the step has finished.	
		EXEC etl.spBatchStepExecution_Update
			@BatchExecutionID = @BatchStepExecutionID,
			@DateEnd = @TransactionDate,
			@ExecutionStatusCode = 'S',
			@Message = 'The EZDW store data load step has completed.',
			@ModifiedBy = @CreatedBy,
			@Debug = @Debug

		-------------------------------------------------------------------------------------------------------------------
		-- Assign stores to chains.
		-- <Summary>
		-- Adds stores to their associated chains.
		-- </Summary.
		-------------------------------------------------------------------------------------------------------------------
		SET @Parameters = NULL;
		SET @Parameters = etl.fnAppendParameter(@Parameters, 'StoreID', @StoreID, 'VARCHAR(MAX)');
		SET @Parameters = etl.fnAppendParameter(@Parameters, 'ChainID', @ChainID, 'VARCHAR(MAX)');
		SET @Parameters = etl.fnAppendParameter(@Parameters, 'NABP', @Nabp, 'VARCHAR(MAX)');
		SET @Parameters = etl.fnAppendParameter(@Parameters, 'CreatedBy', @CreatedBy, 'VARCHAR(256)');
		SET @Parameters = etl.fnAppendParameter(@Parameters, 'ModifiedBy', @ModifiedBy, 'VARCHAR(256)');

		SET @BatchStepExecutionID = NULL;
		SET @TransactionDate = GETDATE();

		-- Create and run an EZDW chain data load step.
		EXEC etl.spBatchStepExecution_Create
			@BatchJobExecutionID = @BatchJobExecutionID,
			@BatchStepInstanceCode = 'CHNSTREMAP',
			@DateStart = @TransactionDate,
			@ExecutionStatusCode = 'R',
			@Parameters = @Parameters,
			@CreatedBy = @CreatedBy,
			@Debug = @Debug,
			@BatchExecutionID = @BatchStepExecutionID OUTPUT

		EXEC import.spChainStore_EZDW_Merge
			@SourceStoreID = @StoreID,
			@SourceChainID = @ChainID,
			@Nabp = @Nabp,
			@CreatedBy = @CreatedBy,
			@ModifiedBy = @ModifiedBy

		SET @TransactionDate = GETDATE();

		-- Reflect the step has finished.	
		EXEC etl.spBatchStepExecution_Update
			@BatchExecutionID = @BatchStepExecutionID,
			@DateEnd = @TransactionDate,
			@ExecutionStatusCode = 'S',
			@Message = 'The EZDW store to chain mapping step has completed.',
			@ModifiedBy = @CreatedBy,
			@Debug = @Debug


		-------------------------------------------------------------------------------------------------------------------
		-- Update parent batch object to reflect completion.
		-------------------------------------------------------------------------------------------------------------------	
		SET @TransactionDate = GETDATE();

		-- Reflect the job has finished.
		EXEC etl.spBatchJobExecution_Update
			@BatchExecutionID = @BatchJobExecutionID,
			@DateEnd = @TransactionDate,
			@ExecutionStatusCode = 'S', 
			@ModifiedBy = @CreatedBy,
			@Debug = @Debug


	END TRY
	BEGIN CATCH

		-- Log the error.
		EXEC dbo.spLogError;

		SET @TransactionDate = GETDATE();

		SET @ErrorMessage = 'Unable to load EZDW business data. Exeception: ' + ERROR_MESSAGE();

		-- Reflect the step has failed.	
		EXEC etl.spBatchStepExecution_Update
			@BatchExecutionID = @BatchStepExecutionID,
			@DateEnd = @TransactionDate,
			@ExecutionStatusCode = 'F',
			@ExitMessage = @ErrorMessage,
			@ModifiedBy = @CreatedBy,
			@Debug = @Debug

		-- Reflect the job has failed.
		EXEC etl.spBatchJobExecution_Update
			@BatchExecutionID = @BatchJobExecutionID,
			@DateEnd = @TransactionDate,
			@ExecutionStatusCode = 'F', 
			@ExitMessage = @ErrorMessage,
			@ModifiedBy = @CreatedBy,
			@Debug = @Debug

		-- Re-throw the error.			
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	

	END CATCH

*/


END
