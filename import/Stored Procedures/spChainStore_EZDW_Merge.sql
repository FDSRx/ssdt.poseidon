﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/28/2014
-- Description:	Investigate the temporary store data and attempts to merge stores to their
-- Change log:
-- 9/8/2016 - Nullified the code as it is currently not in a working state. The procedure has pointers
--		to LPS servers that are no longer within the FDS domain.

--	appropriate chains.
-- SAMPLE CALL: import.spChainStore_EZDW_Merge @CreatedBy = 'dhughes'
-- SAMPLE CALL: import.spChainStore_EZDW_Merge @SourceStoreID = 9737

-- SELECT * FROM dbo.ChainStore
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- EXEC utl.spReseed 'dbo.ChainStore'
-- =============================================
CREATE PROCEDURE [import].[spChainStore_EZDW_Merge]
	@SourceStoreID VARCHAR(MAX) = NULL,
	@SourceChainID VARCHAR(MAX) = NULL,
	@NABP VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@ModifiedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*

	------------------------------------------------------------
	-- Sanitize input
	------------------------------------------------------------
	SET @ModifiedBy = ISNULL(@ModifiedBy, @CreatedBy);
	SET @CreatedBy = ISNULL(@CreatedBy, @ModifiedBy);
	
	
	------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------
	DECLARE
		@DataCatalogCode VARCHAR(10) = 'EZDW',
		@ErrorLogID BIGINT,
		@ErrorMessage VARCHAR(4000)


	------------------------------------------------------------
	-- Temporary resources
	------------------------------------------------------------
	-- Store id list.
	IF OBJECT_ID('tempdb..#tmpStoreIDList') IS NOT NULL
	BEGIN
		DROP TABLE #tmpStoreIDList;
	END

	CREATE TABLE #tmpStoreIDList (
		StoreID INT
	);
	
	-- Chain id list.
	IF OBJECT_ID('tempdb..#tmpChainIDList') IS NOT NULL
	BEGIN
		DROP TABLE #tmpChainIDList;
	END

	CREATE TABLE #tmpChainIDList (
		ChainID INT
	);
	
	-- NABP list.
	IF OBJECT_ID('tempdb..#tmpNABPList') IS NOT NULL
	BEGIN
		DROP TABLE #tmpNABPList;
	END

	CREATE TABLE #tmpNABPList (
		NABP VARCHAR(25)
	);

	-- Chain/Store list.
	IF OBJECT_ID('tempdb..#tmpChainStore') IS NOT NULL
	BEGIN
		DROP TABLE #tmpChainStore;
	END
		
	CREATE TABLE #tmpChainStore (
		Idx INT IDENTITY(1,1),
		ChainID INT,
		StoreID INT
	);
	

	------------------------------------------------------------
	-- Parse specified lists.
	-- <Summary>
	-- If the caller has specified certain store criteria then
	-- we need to parse and use that for our lookup.
	-- If no criteria is specified then lets retrieve all stores.
	-- </Summary>
	------------------------------------------------------------
	
	----------------------------
	-- Parse store ids
	----------------------------
	INSERT INTO #tmpStoreIDList (
		StoreID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@SourceStoreID, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpStoreIDList (StoreID) VALUES (NULL);
	END
	
	----------------------------
	-- Parse chain ids
	----------------------------
	INSERT INTO #tmpChainIDList (
		ChainID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@SourceChainID, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpChainIDList (ChainID) VALUES (NULL);
	END
	
	-- Debug
	-- SELECT * FROM #tmpStoreIDList
	
	----------------------------
	-- Parse NABPs
	----------------------------
	INSERT INTO #tmpNABPList(
		NABP
	)
	SELECT
		Value
	FROM dbo.fnSplit(@NABP, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpNABPList(NABP) VALUES (NULL);
	END

	-- Debug
	-- SELECT * FROM #tmpNABPList
	
	------------------------------------------------------------
	-- Obtain Chain/Store combinations
	------------------------------------------------------------	
	INSERT INTO #tmpChainStore (
		ChainID,
		StoreID
	)
	SELECT
		chn.BusinessEntityID AS ChainID,
		store.BusinessEntityID AS StoreID
	--SELECT *
	--SELECT TOP 1 *
	FROM tmp.Store_Import sto
		JOIN #tmpNABPList nabp
			ON ISNULL(sto.NABP, '') = ISNULL(nabp.NABP, ISNULL(sto.NABP, ''))
		JOIN #tmpStoreIDList storeid
			ON sto.SourceStoreID = ISNULL(storeid.StoreID, sto.SourceStoreID)
		JOIN #tmpChainIDList chainid
			ON ISNULL(sto.SourceChainID, -1999999) = ISNULL(chainid.ChainID, ISNULL(sto.SourceChainID, -1999999))
		JOIN dbo.Chain chn
			ON sto.SourceChainID = chn.SourceChainID
		JOIN dbo.Store store
			ON sto.SourceStoreID = store.SourceStoreID
	WHERE DataCatalogCode = @DataCatalogCode

	-- Debug
	-- SELECT * FROM #tmpChainStore
	

	------------------------------------------------------------
	-- Merge (Add/Update) chain store data.
	-- <Summary>
	-- Determines if there are any pre-existing store records
	-- that do not have a matching chain ID; if the above is true
	-- then the chain ID will be overwritten with the new ID.
	-- If no matches are found then those records will be added
	-- to the data.
	-- </Summary>
	------------------------------------------------------------	
    MERGE dbo.ChainStore AS target
    USING #tmpChainStore AS source
    ON 
		target.StoreID = source.StoreID
    WHEN MATCHED THEN
        UPDATE 
			SET 
				target.ChainID = CASE WHEN source.ChainID = target.ChainID THEN target.ChainID ELSE source.ChainID END,
				target.DateModified = CASE WHEN source.ChainID = target.ChainID THEN target.DateModified ELSE GETDATE() END,
				target.ModifiedBy = CASE WHEN source.ChainID = target.ChainID THEN target.ModifiedBy ELSE @ModifiedBy END
	WHEN NOT MATCHED THEN	
	    INSERT (ChainID, StoreID, CreatedBy)
	    VALUES (source.ChainID, source.StoreID, @CreatedBy);
	    --OUTPUT deleted.*, $action, inserted.* INTO #tmpChainStoreMergeOutput;
	    
	
	-- Review data
	-- SELECT * FROM dbo.ChainStore	


	------------------------------------------------------------
	-- Log information
	-- <Summary>
	-- Logs arguments.
	-- </Summary>
	------------------------------------------------------------	
	DECLARE 
		@InfoMessage VARCHAR(4000),
		@SourceName VARCHAR(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@Arguments VARCHAR(MAX)
	
	SET @InfoMessage = 'EZDW Chain/Store matching and population process has completed successfully.';
		
	SET @Arguments = 
		'@SourceStoreID=' + dbo.fnToStringOrEmpty(@SourceStoreID) + '; ' +
		'@SourceChainID=' + dbo.fnToStringOrEmpty(@SourceChainID) + '; ' +
		'@NABP=' + dbo.fnToStringOrEmpty(@NABP);
    
	-- Log data.
	EXEC spLogInformation
		@InformationProcedure = @SourceName,
		@Message = @InfoMessage,
		@Arguments = @Arguments
		
		
	------------------------------------------------------------
	-- Release resources
	------------------------------------------------------------
	DROP TABLE #tmpStoreIDList;
	DROP TABLE #tmpNABPList;
	DROP TABLE #tmpChainIDList;
	DROP TABLE #tmpChainStore;
	
	
	*/
		
END
