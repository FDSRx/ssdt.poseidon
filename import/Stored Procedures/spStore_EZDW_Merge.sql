﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/7/2013
-- Description:	Imports store data from EZDW into Poseidon's store table.
-- Change log:
-- 9/8/2016 - Nullified the code as it is currently not in a working state. The procedure has pointers
--		to LPS servers that are no longer within the FDS domain.


-- SAMPLE CALL: import.spStore_EZDW_Merge @CreatedBy = 'dhughes'
-- SAMPLE CALL: import.spStore_EZDW_Merge @StoreID = '17275'
-- SAMPLE CALL: import.spStore_EZDW_Merge @NABP = '7871787'
-- SAMPLE CALL: import.spStore_EZDW_Merge @ChainID = '232'

-- SELECT * FROM tmp.Store_Import WHERE BusinessEntityID IS NULL
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- SELECT * FROM dbo.vwStore WHERE SourceStoreID = 17275
-- SELECT * FROM dbo.BusinessEntity ORDER BY BusinessEntityID DESC
-- SELECT * FROM dbo.Business ORDER BY BusinessEntityID DESC
-- =============================================
CREATE PROCEDURE [import].[spStore_EZDW_Merge] 
	@StoreID VARCHAR(MAX) = NULL,
	@ChainID VARCHAR(MAX) = NULL,
	@NABP VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*

	------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------
	DECLARE
		@DataCatalogCode VARCHAR(10) = 'EZDW',
		@ErrorLogID BIGINT,
		@ErrorMessage VARCHAR(4000)


	------------------------------------------------------------
	-- Temporary resources
	------------------------------------------------------------
	-- Store id list.
	CREATE TABLE #tmpStoreIDList (
		StoreID INT
	);
	
	-- Chain id list.
	CREATE TABLE #tmpChainIDList (
		ChainID INT
	);
	
	-- NABP list.
	CREATE TABLE #tmpNABPList (
		NABP VARCHAR(25)
	);
	
	------------------------------------------------------------
	-- Flush existing data.
	-- <Summary>
	-- Delete records that pertain only to the data (source system)
	-- in question.
	-- If the deleted data leaves "0" records in the table,
	-- then let's give it a clean slate by truncating the table.
	-- </Summary>
	------------------------------------------------------------	
	-- Delete existing records.
	DELETE
	FROM tmp.Store_Import
	WHERE DataCatalogCode = @DataCatalogCode

	-- If table contains "0" records after the delete, then let's truncate.
	IF (SELECT COUNT(*) FROM tmp.Store_Import) = 0
	BEGIN
		TRUNCATE TABLE tmp.Store_Import
	END
	
	------------------------------------------------------------
	-- Parse specified lists.
	-- <Summary>
	-- If the caller has specified certain store criteria then
	-- we need to parse and use that for our lookup.
	-- If no criteria is specified then lets retrieve all stores.
	-- </Summary>
	------------------------------------------------------------
	
	----------------------------
	-- Parse store ids
	----------------------------
	INSERT INTO #tmpStoreIDList (
		StoreID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@StoreID, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpStoreIDList (StoreID) VALUES (NULL);
	END
	
	----------------------------
	-- Parse chain ids
	----------------------------
	INSERT INTO #tmpChainIDList (
		ChainID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@ChainID, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpChainIDList (ChainID) VALUES (NULL);
	END
	
	-- Debug
	-- SELECT * FROM #tmpStoreIDList
	
	----------------------------
	-- Parse NABPs
	----------------------------
	INSERT INTO #tmpNABPList(
		NABP
	)
	SELECT
		Value
	FROM dbo.fnSplit(@NABP, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpNABPList(NABP) VALUES (NULL);
	END

	-- Debug
	-- SELECT * FROM #tmpNABPList
	
	------------------------------------------------------------
	-- Import EZDW data
	-- <Summary>
	-- Imports EZDW records.
	-- NOTE: Scrubs data as it is being migrated.
	-- </Summary>
	-- <Remarks>
	-- Data scrubbing comes at a cost of currently adding overhead
	-- to the query execution time (roughly 15 seconds) due to
	-- NULLing out whitespace and empty fields.
	-- </Remarks>
	------------------------------------------------------------	
	INSERT INTO tmp.Store_Import(
		SourceStoreID,
		SourceChainID,
		NABP,
		Name,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		PhoneNumber,
		FaxNumber,
		BusinessEntityID_Store,
		BusinessEntityID_Chain,
		DataCatalogCode
	)
	SELECT
		sto.pkStore AS SourceStoreID,
		sto.fkChain AS SourceChainID,
		dbo.fnToNullIfEmptyOrWhiteSpace(sto.vchrNABP) AS NABP,
		dbo.fnToNullIfEmptyOrWhiteSpace(ISNULL(graph.vchrStoreName, sto.vchrStoreName)) AS Name,
		dbo.fnToNullIfEmptyOrWhiteSpace(REPLACE(graph.vchrAddress1, '&nbsp;', '')) AS AddressLine1,
		dbo.fnToNullIfEmptyOrWhiteSpace(REPLACE(graph.vchrAddress2, '&nbsp;', '')) AS AddressLine2,
		dbo.fnToNullIfEmptyOrWhiteSpace(REPLACE(graph.vchrCity, '&nbsp;', '')) AS City,
		dbo.fnToNullIfEmptyOrWhiteSpace(REPLACE(graph.chrState, '&nbsp;', '')) AS State,
		dbo.fnToNullIfEmptyOrWhiteSpace(REPLACE(graph.vchrZip, '&nbsp;', '')) AS PostalCode,
		dbo.fnToNullIfEmptyOrWhiteSpace(REPLACE(graph.vchrPhone, '&nbsp;', '')) AS PhoneNumber,
		dbo.fnToNullIfEmptyOrWhiteSpace(REPLACE(graph.vchrFax, '&nbsp;', '')) AS FaxNumber,
		biz.BusinessEntityID,
		chn.BusinessEntityID,
		@DataCatalogCode
	--SELECT *
	--SELECT TOP 1 *
	FROM HCCEZDW.eConcile.dbo.tblStore sto
		JOIN HCCEZDW.eConcile.dbo.tbldemographics graph
			ON sto.pkStore = graph.fkGeneric
		LEFT JOIN Poseidon.dbo.Store biz
			ON biz.SourceStoreID = sto.pkStore
		LEFT JOIN Poseidon.dbo.Chain chn
			ON chn.SourceChainID = sto.fkChain
		JOIN #tmpNABPList nabp
			ON ISNULL(sto.vchrNABP, '') = ISNULL(nabp.NABP, ISNULL(sto.vchrNABP, ''))
		JOIN #tmpStoreIDList storeid
			ON sto.pkStore = ISNULL(storeid.StoreID, sto.pkStore)
		JOIN #tmpChainIDList chainid
			ON ISNULL(sto.fkChain, -1999999) = ISNULL(chainid.ChainID, ISNULL(sto.fkChain, -1999999))
	WHERE graph.fkLocation = 5
		AND dbo.fnIsNullOrEmpty(ISNULL(graph.vchrStoreName, sto.vchrStoreName)) = 0
	ORDER BY ISNULL(graph.vchrStoreName, sto.vchrStoreName) ASC


	
	------------------------------------------------------------
	-- Create/Update stores
	-- <Summary>
	-- Creates new store records for stores that were unable
	-- to be identified during the store import process above.
	-- </Summary>
	------------------------------------------------------------	
	-- Cursor vars
	DECLARE 
		@StoreImportID BIGINT = NULL,
		@SourceStoreID INT = NULL,
		@SourceChainID INT = NULL,
		@StoreNABP VARCHAR(25) = NULL,
		@Name VARCHAR(255) = NULL,
		@AddressLine1 VARCHAR(255) = NULL,
		@AddressLine2 VARCHAR(255) = NULL,
		@City VARCHAR(255) = NULL,
		@State VARCHAR(255) = NULL,
		@PostalCode VARCHAR(255) = NULL,
		@PhoneNumber VARCHAR(255) = NULL,
		@FaxNumber VARCHAR(255) = NULL,
		@BusinessEntityID_Store INT = NULL,
		@BusinessEntityID_Chain INT = NULL
		
		
	DECLARE curStores CURSOR FAST_FORWARD FOR
	
	-- Retrieve cursor data for processing.
	SELECT
		StoreImportID,
		SourceStoreID,
		SourceChainID,
		NABP,
		Name,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		PhoneNumber,
		FaxNumber,
		BusinessEntityID_Store,
		BusinessEntityID_Chain
	FROM tmp.Store_Import
	WHERE DataCatalogCode = @DataCatalogCode

	OPEN curStores

	FETCH NEXT FROM curStores INTO 
		@StoreImportID,
		@SourceStoreID,
		@SourceChainID,
		@StoreNABP,
		@Name,
		@AddressLine1,
		@AddressLine2,
		@City,
		@State,
		@PostalCode,
		@PhoneNumber,
		@FaxNumber,
		@BusinessEntityID_Store,
		@BusinessEntityID_Chain
		
	WHILE @@FETCH_STATUS = 0
	BEGIN

		BEGIN TRY
		
			DECLARE 
				@Msg VARCHAR(4000),
				@IsMerged BIT = 0
			
			-- If we could not identify a business then it is considered new and needs to be added.
			IF @BusinessEntityID_Store IS NULL
			BEGIN
				
				/*
				-- Debug
				SET @Msg = 'Processing Source Store: ' + dbo.fnToStringOrEmpty(@Name) + ' (' + dbo.fnToStringOrEmpty(@SourceStoreID) + ')'
				RAISERROR (@Msg, 0, 1) WITH NOWAIT
				*/
				
				EXEC spStore_Create
					@SourceStoreID = @SourceStoreID,
					@StoreName = @Name,
					@NABP = @StoreNABP,
					-- Address
					@AddressLine1 = @AddressLine1,
					@AddressLine2 = @AddressLine2,
					@City  = @City,
					@State  = @State,
					@PostalCode  = @PostalCode,
					--Business Phone (Work)
					@PhoneNumber_Work  = @PhoneNumber,
					@PhoneNumber_Fax  = @FaxNumber,	
					-- Output
					@BusinessEntityID = @BusinessEntityID_Store OUTPUT,
					-- Caller
					@CreatedBy = @CreatedBy,	
					-- Error
					@ErrorLogID  = @ErrorLogID OUTPUT
			
				SET @IsMerged = CASE WHEN @ErrorLogID IS NULL THEN 1 ELSE 0 END;
					
			END
			-- * ------------------------------------------------------------
			-- TODO: Create update script that updates information.
			-- * ------------------------------------------------------------
		
			-- Update import data to reflect merge.
			UPDATE i
			SET 
				i.IsMerged = @IsMerged,
				i.ErrorLogID = @ErrorLogID,
				i.DateModified = GETDATE()
			FROM tmp.Store_Import i
			WHERE i.StoreImportID = @StoreImportID
						
		END TRY
		BEGIN CATCH
			
			EXEC spLogError @ErrorLogID;
			
			-- Update import data to reflect error.		
			UPDATE i
			SET 
				i.IsMerged = 0,
				i.ErrorLogID = @ErrorLogID,
				i.DateModified = GETDATE()
			FROM tmp.Store_Import i
			WHERE i.StoreImportID = @StoreImportID				
			
		END CATCH	

		
		FETCH NEXT FROM curStores INTO
			@StoreImportID,
			@SourceStoreID,
			@SourceChainID,
			@StoreNABP,
			@Name,
			@AddressLine1,
			@AddressLine2,
			@City,
			@State,
			@PostalCode,
			@PhoneNumber,
			@FaxNumber,
			@BusinessEntityID_Store,
			@BusinessEntityID_Chain
			
	END

	CLOSE curStores
	DEALLOCATE curStores
	
		


	------------------------------------------------------------
	-- Log information
	-- <Summary>
	-- Logs processed counts for review.
	-- </Summary>
	------------------------------------------------------------
	DECLARE
		@TotalRecords INT = 0,
		@Records_ToCreate INT = 0,
		@Records_Created INT = 0,
		@Records_ToUpdate INT = 0,
		@Records_Updated INT = 0,
		@TotalErrorRecords INT = 0,
		@SourceName VARCHAR(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@InfoMessage VARCHAR(4000),
		@Arguments VARCHAR(MAX)
	
	-- Obtain record information.
	SELECT
		@TotalRecords = COUNT(*),
		@Records_ToCreate = SUM(CASE WHEN BusinessEntityID_Store IS NULL THEN 1 ELSE 0 END),
		@Records_ToUpdate = SUM(CASE WHEN BusinessEntityID_Store IS NOT NULL THEN 1 ELSE 0 END),
		@Records_Created = SUM(CASE WHEN BusinessEntityID_Store IS NULL AND IsMerged = 1 THEN 1 ELSE 0 END),	
		@Records_Updated = SUM(CASE WHEN BusinessEntityID_Store IS NOT NULL AND IsMerged = 1 THEN 1 ELSE 0 END),
		@TotalErrorRecords = SUM(CASE WHEN ErrorLogID IS NOT NULL THEN 1 ELSE 0 END)
	FROM tmp.Store_Import
	WHERE DataCatalogCode = @DataCatalogCode

	SET @InfoMessage = 'EZDW store import completed. ' +
		'Total records imported: ' + dbo.fnToStringOrEmpty(@TotalRecords) + '. ' +
		'Total records to create: ' + dbo.fnToStringOrEmpty(@Records_ToCreate) + '. ' +
		'Total records created: ' + dbo.fnToStringOrEmpty(@Records_Created) + '. ' +
		'Total records to update: ' + dbo.fnToStringOrEmpty(@Records_ToUpdate) + '. ' +
		'Total records updated: ' + dbo.fnToStringOrEmpty(@Records_Updated) + '. ' +
		'Total Errors Encountered: '+ dbo.fnToStringOrEmpty(@TotalErrorRecords)
		
	SET @Arguments = 
		'@StoreID=' + dbo.fnToStringOrEmpty(@StoreID) + '; ' +
		'@ChainID=' + dbo.fnToStringOrEmpty(@ChainID) + '; ' +
		'@NABP=' + dbo.fnToStringOrEmpty(@NABP);
    
	-- Log data.
	EXEC spLogInformation
		@InformationProcedure = @SourceName,
		@Message = @InfoMessage,
		@Arguments = @Arguments



	------------------------------------------------------------
	-- Drop resources
	------------------------------------------------------------
	DROP TABLE #tmpStoreIDList;
	DROP TABLE #tmpNABPList;
	DROP TABLE #tmpChainIDList;



	*/

END
