﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/7/2013
-- Description:	Imports store data from IPP into Poseidon's store table.
-- 9/8/2016 - Nullified the code as it is currently not in a working state. The procedure has pointers
--		to LPS servers that are no longer within the FDS domain.

-- SAMPLE CALL: import.spStore_IPP_Merge 
-- SAMPLE CALL: import.spStore_IPP_Merge @StoreID = '44'
-- SAMPLE CALL: import.spStore_IPP_Merge @NABP = '7871787'
-- SAMPLE CALL: import.spStore_IPP_Merge @ChainID = '232'
-- SELECT * FROM tmp.Store_Import WHERE BusinessEntityID IS NULL
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- SELECT * FROM dbo.vwStore WHERE SourceStoreID = 17275
-- SELECT * FROM IPP.dbo.Store
-- =============================================
CREATE PROCEDURE [import].[spStore_IPP_Merge] 
	@StoreID VARCHAR(MAX) = NULL,
	@ChainID VARCHAR(MAX) = NULL,
	@NABP VARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*

	------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------
	DECLARE
		@DataCatalogCode VARCHAR(10) = 'IPP',
		@ErrorLogID BIGINT,
		@ErrorMessage VARCHAR(4000),
		@Comments VARCHAR(MAX),
		@ServiceID_mPC INT = dbo.fnGetServiceID('MPC'),
		@ServiceID_Dir INT = dbo.fnGetServiceID('CUSDIR')
		


	------------------------------------------------------------
	-- Temporary resources
	------------------------------------------------------------
	-- Store id list.
	CREATE TABLE #tmpStoreIDList (
		StoreID INT
	);
	
	-- Chain id list.
	CREATE TABLE #tmpChainIDList (
		ChainID INT
	);
	
	-- NABP list.
	CREATE TABLE #tmpNABPList (
		NABP VARCHAR(25)
	);
	
	------------------------------------------------------------
	-- Flush existing data.
	-- <Summary>
	-- Delete records that pertain only to the data (source system)
	-- in question.
	-- If the deleted data leaves "0" records in the table,
	-- then let's give it a clean slate by truncating the table.
	-- </Summary>
	------------------------------------------------------------	
	-- Delete existing records.
	DELETE
	FROM tmp.Store_Import
	WHERE DataCatalogCode = @DataCatalogCode

	-- If table contains "0" records after the delete, then let's truncate.
	IF (SELECT COUNT(*) FROM tmp.Store_Import) = 0
	BEGIN
		TRUNCATE TABLE tmp.Store_Import
	END
	
	------------------------------------------------------------
	-- Parse specified lists.
	-- <Summary>
	-- If the caller has specified certain store criteria then
	-- we need to parse and use that for our lookup.
	-- If no criteria is specified then lets retrieve all stores.
	-- </Summary>
	------------------------------------------------------------
	
	----------------------------
	-- Parse store ids
	----------------------------
	INSERT INTO #tmpStoreIDList (
		StoreID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@StoreID, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpStoreIDList (StoreID) VALUES (NULL);
	END
	
	----------------------------
	-- Parse chain ids
	----------------------------
	INSERT INTO #tmpChainIDList (
		ChainID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@ChainID, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpChainIDList (ChainID) VALUES (NULL);
	END
	
	-- Debug
	-- SELECT * FROM #tmpStoreIDList
	
	----------------------------
	-- Parse NABPs
	----------------------------
	INSERT INTO #tmpNABPList(
		NABP
	)
	SELECT
		Value
	FROM dbo.fnSplit(@NABP, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpNABPList(NABP) VALUES (NULL);
	END

	-- Debug
	-- SELECT * FROM #tmpNABPList
	
	------------------------------------------------------------
	-- Import IPP data
	-- <Summary>
	-- Imports IPP records.
	-- NOTE: Scrubs data as it is being migrated.
	-- </Summary>
	-- <Remarks>
	-- Data scrubbing comes at a cost of currently adding overhead
	-- to the query execution time.
	-- </Remarks>
	------------------------------------------------------------	
	INSERT INTO tmp.Store_Import(
		SourceStoreID,
		SourceChainID,
		NABP,
		Token,
		Name,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		TimeZone,
		SupportDST,
		Latitude,
		Longitude,
		PhoneNumber,
		FaxNumber,
		BusinessEntityID_Store,
		BusinessEntityID_Chain,
		DataCatalogCode
	)
	SELECT
		CASE WHEN sto.StoreID = 0 THEN -1 ELSE sto.StoreID END AS SourceStoreID,
		sto.ChainID AS SourceChainID,
		dbo.fnToNullIfEmptyOrWhiteSpace(sto.NABP) AS NABP,
		sto.StoreToken,
		dbo.fnToNullIfEmptyOrWhiteSpace(sto.Name) AS Name,
		dbo.fnToNullIfEmptyOrWhiteSpace(sto.AddressLine1) AS AddressLine1,
		dbo.fnToNullIfEmptyOrWhiteSpace(sto.AddressLine2) AS AddressLine2,
		dbo.fnToNullIfEmptyOrWhiteSpace(sto.City) AS City,
		dbo.fnToNullIfEmptyOrWhiteSpace(sto.State) AS State,
		dbo.fnToNullIfEmptyOrWhiteSpace(sto.Zipcode) AS PostalCode,
		sto.TimeZone,
		sto.SupportDST,
		sto.Latitude,
		sto.Longtitude,
		dbo.fnToNullIfEmptyOrWhiteSpace(sto.PhoneNumber) AS PhoneNumber,
		NULL AS FaxNumber,
		biz.BusinessEntityID AS BusinessEntityID_Store,
		chn.BusinessEntityID AS BusinessEntityID_Chain,
		@DataCatalogCode
	--SELECT *
	--SELECT TOP 1 *
	FROM IPP.dbo.Store sto
		LEFT JOIN Poseidon.dbo.Store biz
			ON biz.SourceStoreID = sto.StoreID
		LEFT JOIN Poseidon.dbo.Chain chn
			ON chn.SourceChainID = sto.ChainID
		JOIN #tmpNABPList nabp
			ON ISNULL(sto.NABP, '') = ISNULL(nabp.NABP, ISNULL(sto.NABP, ''))
		JOIN #tmpStoreIDList storeid
			ON sto.StoreID = ISNULL(storeid.StoreID, sto.StoreID)
		JOIN #tmpChainIDList chainid
			ON ISNULL(sto.ChainID, -1999999) = ISNULL(chainid.ChainID, ISNULL(sto.ChainID, -1999999))
	ORDER BY Name ASC

	

	------------------------------------------------------------
	-- Create/Update stores
	-- <Summary>
	-- Creates new store records for stores that were unable
	-- to be identified during the store import process above.
	-- </Summary>
	------------------------------------------------------------
	-- Cursor vars
	DECLARE 
		@StoreImportID BIGINT = NULL,
		@SourceStoreID INT = NULL,
		@SourceChainID INT = NULL,
		@StoreNABP VARCHAR(25) = NULL,
		@Token VARCHAR(50) = NULL,
		@Name VARCHAR(255) = NULL,
		@AddressLine1 VARCHAR(255) = NULL,
		@AddressLine2 VARCHAR(255) = NULL,
		@City VARCHAR(255) = NULL,
		@State VARCHAR(255) = NULL,
		@PostalCode VARCHAR(255) = NULL,
		@TimeZone VARCHAR(25) = NULL,
		@SupportDST BIT = NULL,
		@Latitude DECIMAL(9,6) = NULL,
		@Longitude DECIMAL(9,6) = NULL,
		@PhoneNumber VARCHAR(255) = NULL,
		@FaxNumber VARCHAR(255) = NULL,
		@BusinessEntityID_Store INT = NULL,
		@BusinessEntityID_Chain INT = NULL

	
	DECLARE curStores CURSOR FAST_FORWARD FOR
	
	-- Retrieve cursor data for processing.
	SELECT
		StoreImportID,
		SourceStoreID,
		SourceChainID,
		NABP,
		Token,
		Name,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		TimeZone,
		SupportDST,
		Latitude,
		Longitude,
		PhoneNumber,
		FaxNumber,
		BusinessEntityID_Store,
		BusinessEntityID_Chain
	FROM tmp.Store_Import
	WHERE DataCatalogCode = @DataCatalogCode
	ORDER BY Name
	
	OPEN curStores

	FETCH NEXT FROM curStores INTO 
		@StoreImportID,
		@SourceStoreID,
		@SourceChainID,
		@StoreNABP,
		@Token,
		@Name,
		@AddressLine1,
		@AddressLine2,
		@City,
		@State,
		@PostalCode,
		@TimeZone,
		@SupportDST,
		@Latitude,
		@Longitude,
		@PhoneNumber,
		@FaxNumber,
		@BusinessEntityID_Store,
		@BusinessEntityID_Chain
		
	WHILE @@FETCH_STATUS = 0
	BEGIN

		BEGIN TRY
		
			SET @Comments = NULL;
			
			--------------------------------------------
			-- Local loop variables
			--------------------------------------------
			DECLARE 
				@BusinessToken VARCHAR(50) = NULL,
				@Exists BIT = NULL,
				@Msg VARCHAR(4000) = NULL,
				@IsMerged BIT = 0
			
			-- If we could not identify a business then it is considered new and needs to be added.
			IF @BusinessEntityID_Store IS NULL
			BEGIN
				
				/*
				-- Debug
				SET @Msg = 'Processing Source Store: ' + dbo.fnToStringOrEmpty(@Name) + ' (' + dbo.fnToStringOrEmpty(@SourceStoreID) + ')'
				RAISERROR (@Msg, 0, 1) WITH NOWAIT
				*/
				
				SET @IsMerged = 0;
				
			END
			ELSE
			BEGIN		
				
				-- Fetch associated business token.
				SET @BusinessToken = (SELECT BusinessToken FROM dbo.Business WHERE BusinessEntityID = @BusinessEntityID_Store);
				
				-- If the business token does not match the IPP standarized token, then we need to update.
				IF @BusinessToken <> @Token
				BEGIN
				
					/*
					-- Debug
					SELECT @Name AS StoreName, @StoreImportID AS StoreImportID, @SourceStoreID AS SourceStoreID, 
						@BusinessEntityID_Store AS BusinessEntityID, @BusinessToken AS BusinessToken, @Token AS StoreToken
					*/
					
					UPDATE biz
					SET
						biz.BusinessToken = @Token,
						biz.DateModified = GETDATE()
					FROM Business biz
					WHERE BusinessEntityID = @BusinessEntityID_Store
					
					SET @Comments = ISNULL(@Comments, '') + 'BusinessToken was updated from ' + dbo.fnToStringOrEmpty(@BusinessToken) +
						' to ' + dbo.fnToStringOrEmpty(@Token) + '. ** ';
					
				END
				
				--------------------------------------------------------
				-- Build service/umbrella records
				--------------------------------------------------------
				
				IF ISNULL(@BusinessEntityID_Store, 0) <> 0 AND ISNULL(@BusinessEntityID_Chain, 0) <> 0
				BEGIN
				
					-- Create store mPC service umbrella entry
					EXEC spBusinessEntityServiceUmbrella_Exists
						@BusinessEntityID = @BusinessEntityID_Store,
						@ServiceID = @ServiceID_mPC,
						@Exists = @Exists OUTPUT
					
					IF ISNULL(@Exists, 0) = 0
					BEGIN
						EXEC spBusinessEntityServiceUmbrella_Create
							@BusinessEntityID = @BusinessEntityID_Store,
							@ServiceID = @ServiceID_mPC,
							@BusinessEntityUmbrellaID = @BusinessEntityID_Chain
						
						SET @Comments = ISNULL(@Comments, '') + 'mPC service umbrella entry was created for store: ' + dbo.fnToStringOrEmpty(@BusinessEntityID_Store) +
						   '. ** ';
					END
					
					-- Create chain mPC service umbrella entry
					EXEC spBusinessEntityServiceUmbrella_Exists
						@BusinessEntityID = @BusinessEntityID_Chain,
						@ServiceID = @ServiceID_mPC,
						@Exists = @Exists OUTPUT
					
					IF ISNULL(@Exists, 0) = 0
					BEGIN
						EXEC spBusinessEntityServiceUmbrella_Create
							@BusinessEntityID = @BusinessEntityID_Chain,
							@ServiceID = @ServiceID_mPC,
							@BusinessEntityUmbrellaID = @BusinessEntityID_Chain
						
						SET @Comments = ISNULL(@Comments, '') + 'mPC service umbrella entry was created for chain: ' + dbo.fnToStringOrEmpty(@BusinessEntityID_Chain) +
						   '. ** ';
					END
					
					-- Create store customer directory service umbrella entry
					EXEC spBusinessEntityServiceUmbrella_Exists
						@BusinessEntityID = @BusinessEntityID_Store,
						@ServiceID = @ServiceID_dir,
						@Exists = @Exists OUTPUT
					
					IF ISNULL(@Exists, 0) = 0
					BEGIN
						EXEC spBusinessEntityServiceUmbrella_Create
							@BusinessEntityID = @BusinessEntityID_Store,
							@ServiceID = @ServiceID_dir,
							@BusinessEntityUmbrellaID = @BusinessEntityID_Chain
						
						SET @Comments = ISNULL(@Comments, '') + 'customer directory service umbrella entry was created for store: ' + dbo.fnToStringOrEmpty(@BusinessEntityID_Store) +
						   '. ** ';
					END
					
					-- Create chain customer directory service umbrella entry
					EXEC spBusinessEntityServiceUmbrella_Exists
						@BusinessEntityID = @BusinessEntityID_Chain,
						@ServiceID = @ServiceID_dir,
						@Exists = @Exists OUTPUT
					
					IF ISNULL(@Exists, 0) = 0
					BEGIN
						EXEC spBusinessEntityServiceUmbrella_Create
							@BusinessEntityID = @BusinessEntityID_Chain,
							@ServiceID = @ServiceID_dir,
							@BusinessEntityUmbrellaID = @BusinessEntityID_Chain
						
						SET @Comments = ISNULL(@Comments, '') + 'customer directory service umbrella entry was created for chain: ' + dbo.fnToStringOrEmpty(@BusinessEntityID_Chain) +
						   '. ** ';
					END
					
				END				
				
				--------------------------------------------------------
				-- Build standard service records
				--------------------------------------------------------
				
				IF ISNULL(@BusinessEntityID_Store, 0) <> 0 AND ISNULL(@BusinessEntityID_Chain, 0) <> 0
				BEGIN
				
					-- Create store mPC service entry
					EXEC spBusinessEntityService_Exists
						@BusinessEntityID = @BusinessEntityID_Store,
						@ServiceID = @ServiceID_mPC,
						@Exists = @Exists OUTPUT
					
					IF ISNULL(@Exists, 0) = 0
					BEGIN
						EXEC spBusinessEntityService_Create
							@BusinessEntityID = @BusinessEntityID_Store,
							@ServiceID = @ServiceID_mPC
						
						SET @Comments = ISNULL(@Comments, '') + 'mPC service entry was created for store: ' + dbo.fnToStringOrEmpty(@BusinessEntityID_Store) +
						   '. ** ';
					END
					
					-- Create chain mPC service entry
					EXEC spBusinessEntityService_Exists
						@BusinessEntityID = @BusinessEntityID_Chain,
						@ServiceID = @ServiceID_mPC,
						@Exists = @Exists OUTPUT
					
					IF ISNULL(@Exists, 0) = 0
					BEGIN
						EXEC spBusinessEntityService_Create
							@BusinessEntityID = @BusinessEntityID_Chain,
							@ServiceID = @ServiceID_mPC
						
						SET @Comments = ISNULL(@Comments, '') + 'mPC service entry was created for chain: ' + dbo.fnToStringOrEmpty(@BusinessEntityID_Chain) +
						   '. ** ';
					END
					
					-- Create store customer directory service entry
					EXEC spBusinessEntityService_Exists
						@BusinessEntityID = @BusinessEntityID_Store,
						@ServiceID = @ServiceID_dir,
						@Exists = @Exists OUTPUT
					
					IF ISNULL(@Exists, 0) = 0
					BEGIN
						EXEC spBusinessEntityService_Create
							@BusinessEntityID = @BusinessEntityID_Store,
							@ServiceID = @ServiceID_dir
						
						SET @Comments = ISNULL(@Comments, '') + 'customer directory service entry was created for store: ' + dbo.fnToStringOrEmpty(@BusinessEntityID_Store) +
						   '. ** ';
					END
					
					-- Create chain customer directory service entry
					EXEC spBusinessEntityService_Exists
						@BusinessEntityID = @BusinessEntityID_Chain,
						@ServiceID = @ServiceID_dir,
						@Exists = @Exists OUTPUT
					
					IF ISNULL(@Exists, 0) = 0
					BEGIN
						EXEC spBusinessEntityService_Create
							@BusinessEntityID = @BusinessEntityID_Chain,
							@ServiceID = @ServiceID_dir
						
						SET @Comments = ISNULL(@Comments, '') + 'customer directory service entry was created for chain: ' + dbo.fnToStringOrEmpty(@BusinessEntityID_Chain) +
						   '. ** ';
					END
					
				END
			
				SET @IsMerged = 1;
				
			END
				
				
			------------------------------------------------------------
			-- Update import data to reflect merge.
			------------------------------------------------------------
			UPDATE i
			SET 
				i.IsMerged = @IsMerged,
				i.ErrorLogID = @ErrorLogID,
				i.Comments = @Comments,
				i.DateModified = GETDATE()
			FROM tmp.Store_Import i
			WHERE i.StoreImportID = @StoreImportID
			
		END TRY
		BEGIN CATCH
			
			EXEC spLogError @ErrorLogID;
			
			------------------------------------------------------------
			-- Update import data to reflect merge.
			------------------------------------------------------------
			UPDATE i
			SET 
				i.IsMerged = 0,
				i.ErrorLogID = @ErrorLogID,
				i.Comments = @Comments,
				i.DateModified = GETDATE()
			FROM tmp.Store_Import i
			WHERE i.StoreImportID = @StoreImportID				
			
		END CATCH	

		
		FETCH NEXT FROM curStores INTO
			@StoreImportID,
			@SourceStoreID,
			@SourceChainID,
			@StoreNABP,
			@Token,
			@Name,
			@AddressLine1,
			@AddressLine2,
			@City,
			@State,
			@PostalCode,
			@TimeZone,
			@SupportDST,
			@Latitude,
			@Longitude,
			@PhoneNumber,
			@FaxNumber,
			@BusinessEntityID_Store,
			@BusinessEntityID_Chain
			
	END

	CLOSE curStores
	DEALLOCATE curStores
	
		


	------------------------------------------------------------
	-- Log information
	-- <Summary>
	-- Logs processed counts for review.
	-- </Summary>
	------------------------------------------------------------
	DECLARE
		@TotalRecords INT = 0,
		@Records_ToCreate INT = 0,
		@Records_Created INT = 0,
		@Records_ToUpdate INT = 0,
		@Records_Updated INT = 0,
		@TotalErrorRecords INT = 0,
		@SourceName VARCHAR(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@InfoMessage VARCHAR(4000),
		@Arguments VARCHAR(MAX)
	
	-- Obtain record information.
	SELECT
		@TotalRecords = COUNT(*),
		@Records_ToCreate = SUM(CASE WHEN BusinessEntityID_Store IS NULL THEN 1 ELSE 0 END),
		@Records_ToUpdate = SUM(CASE WHEN BusinessEntityID_Store IS NOT NULL THEN 1 ELSE 0 END),
		@Records_Created = SUM(CASE WHEN BusinessEntityID_Store IS NULL AND IsMerged = 1 THEN 1 ELSE 0 END),	
		@Records_Updated = SUM(CASE WHEN BusinessEntityID_Store IS NOT NULL AND IsMerged = 1 THEN 1 ELSE 0 END),
		@TotalErrorRecords = SUM(CASE WHEN ErrorLogID IS NOT NULL THEN 1 ELSE 0 END)
	FROM tmp.Store_Import
	WHERE DataCatalogCode = @DataCatalogCode

	SET @InfoMessage = 'IPP store import completed. ' +
		'Total records imported: ' + dbo.fnToStringOrEmpty(@TotalRecords) + '. ' +
		'Total records to create: ' + dbo.fnToStringOrEmpty(@Records_ToCreate) + '. ' +
		'Total records created: ' + dbo.fnToStringOrEmpty(@Records_Created) + '. ' +
		'Total records to update: ' + dbo.fnToStringOrEmpty(@Records_ToUpdate) + '. ' +
		'Total records updated: ' + dbo.fnToStringOrEmpty(@Records_Updated) + '. ' +
		'Total Errors Encountered: '+ dbo.fnToStringOrEmpty(@TotalErrorRecords)
		
	SET @Arguments = 
		'@StoreID=' + dbo.fnToStringOrEmpty(@StoreID) + '; ' +
		'@ChainID=' + dbo.fnToStringOrEmpty(@ChainID) + '; ' +
		'@NABP=' + dbo.fnToStringOrEmpty(@NABP);
    
	-- Log data.
	EXEC spLogInformation
		@InformationProcedure = @SourceName,
		@Message = @InfoMessage,
		@Arguments = @Arguments



	------------------------------------------------------------
	-- Drop resources
	------------------------------------------------------------
	DROP TABLE #tmpStoreIDList;
	DROP TABLE #tmpNABPList;
	DROP TABLE #tmpChainIDList;


	*/

END
