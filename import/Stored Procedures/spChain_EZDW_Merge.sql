﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 1/8/2014
-- Description:	Imports chain data from EZDW into Poseidon's chain table.
-- Change Log:
-- 03/15/2015 - DRH - Added the ability to update chains.
-- 9/8/2016 - Nullified the code as it is currently not in a working state. The procedure has pointers
--		to LPS servers that are no longer within the FDS domain.

-- SAMPLE CALL: import.spChain_EZDW_Merge @CreatedBy = 'dhughes'
-- SAMPLE CALL: import.spChain_EZDW_Merge @ChainID = '7'

-- SELECT * FROM tmp.Chain_Import WHERE BusinessEntityID IS NULL
-- SELECT * FROM dbo.InformationLog ORDER BY InformationLogID DESC
-- SELECT * FROM dbo.ErrorLog ORDER BY ErrorLogID DESC
-- SELECT * FROM dbo.vwChain
-- SELECT * FROM dbo.Business ORDER BY BusinessEntityID DESC
-- SELECT * FROM dbo.BusinessType
-- =============================================
CREATE PROCEDURE [import].[spChain_EZDW_Merge]
	@ChainID VARCHAR(MAX) = NULL,
	@CreatedBy VARCHAR(256) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*


	------------------------------------------------------------
	-- Local variables
	------------------------------------------------------------
	DECLARE
		@DataCatalogCode VARCHAR(10) = 'EZDW',
		@ErrorLogID BIGINT,
		@ErrorMessage VARCHAR(4000)
		
	------------------------------------------------------------
	-- Temporary resources
	------------------------------------------------------------
	-- Chain id list.
	IF OBJECT_ID('tempdb..#tmpChainIDList') IS NOT NULL
	BEGIN
		DROP TABLE #tmpChainIDList;
	END
	
	CREATE TABLE #tmpChainIDList (
		ChainID INT
	);
	
	------------------------------------------------------------
	-- Flush existing data.
	-- <Summary>
	-- Delete records that pertain only to the data (source system)
	-- in question.
	-- If the deleted data leaves "0" records in the table,
	-- then let's give it a clean slate by truncating the table.
	-- </Summary>
	------------------------------------------------------------	
	-- Delete existing records.
	DELETE
	FROM tmp.Chain_Import
	WHERE DataCatalogCode = @DataCatalogCode

	-- If table contains "0" records after the delete, then let's truncate.
	IF (SELECT COUNT(*) FROM tmp.Chain_Import) = 0
	BEGIN
		TRUNCATE TABLE tmp.Chain_Import
	END
	
	------------------------------------------------------------
	-- Parse specified lists.
	-- <Summary>
	-- If the caller has specified certain store criteria then
	-- we need to parse and use that for our lookup.
	-- If no criteria is specified then lets retrieve all stores.
	-- </Summary>
	------------------------------------------------------------
	
	----------------------------
	-- Parse chain ids
	----------------------------
	INSERT INTO #tmpChainIDList (
		ChainID
	)
	SELECT
		Value
	FROM dbo.fnSplit(@ChainID, ',')
	WHERE dbo.fnIsNullOrEmpty(Value) = 0
		AND ISNUMERIC(Value) = 1
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO #tmpChainIDList (ChainID) VALUES (NULL);
	END
	
	
	------------------------------------------------------------
	-- Import EZDW data
	-- <Summary>
	-- Imports EZDW records.
	-- NOTE: Scrubs data as it is being migrated.
	-- </Summary>
	------------------------------------------------------------	
	--INSERT INTO tmp.Chain_Import(
	--	SourceChainID,
	--	Name,
	--	BusinessEntityID,
	--	DataCatalogCode
	--)
	--SELECT
	--	pkChain AS SourceChainID, 
	--	vchrName AS Name,
	--	chn.BusinessEntityID,
	--	@DataCatalogCode
	----SELECT *
	----SELECT TOP 1 *
	--FROM hccezdw.econcile.dbo.tblChain ez
	--	LEFT JOIN dbo.Chain chn
	--		ON ez.pkChain = chn.SourceChainID
	--	JOIN #tmpChainIDList chainid
	--		ON ISNULL(ez.pkChain, -1999999) = ISNULL(chainid.ChainID, ISNULL(ez.pkChain, -1999999))

	

	------------------------------------------------------------
	-- Create/Update chains
	-- <Summary>
	-- Creates new chain records for chains that were unable
	-- to be identified during the chain import process above.
	-- </Summary>
	------------------------------------------------------------	
	-- Cursor vars
	DECLARE 
		@ChainImportID BIGINT = NULL,
		@SourceChainID INT = NULL,
		@Name VARCHAR(255) = NULL,
		@AddressLine1 VARCHAR(255) = NULL,
		@AddressLine2 VARCHAR(255) = NULL,
		@City VARCHAR(255) = NULL,
		@State VARCHAR(255) = NULL,
		@PostalCode VARCHAR(255) = NULL,
		@PhoneNumber VARCHAR(255) = NULL,
		@FaxNumber VARCHAR(255) = NULL,
		@BusinessEntityID INT = NULL
		
		
	DECLARE curChains CURSOR FAST_FORWARD FOR
	
	-- Retrieve cursor data for processing.
	SELECT
		ChainImportID,
		SourceChainID,
		Name,
		AddressLine1,
		AddressLine2,
		City,
		State,
		PostalCode,
		PhoneNumber,
		FaxNumber,
		BusinessEntityID
	FROM tmp.Chain_Import
	WHERE DataCatalogCode = @DataCatalogCode

	OPEN curChains

	FETCH NEXT FROM curChains INTO 
		@ChainImportID,
		@SourceChainID,
		@Name,
		@AddressLine1,
		@AddressLine2,
		@City,
		@State,
		@PostalCode,
		@PhoneNumber,
		@FaxNumber,
		@BusinessEntityID
		
	WHILE @@FETCH_STATUS = 0
	BEGIN

		BEGIN TRY
		
			DECLARE 
				@Msg VARCHAR(4000),
				@IsMerged BIT = 0
			
			-- If we could not identify a business then it is considered new and needs to be added.
			IF @BusinessEntityID IS NULL
			BEGIN
				
				/*
				-- Debug
				SET @Msg = 'Processing Source Store: ' + dbo.fnToStringOrEmpty(@Name) + ' (' + dbo.fnToStringOrEmpty(@SourceStoreID) + ')'
				RAISERROR (@Msg, 0, 1) WITH NOWAIT
				*/
				
				EXEC spChain_Create
					@SourceChainID = @SourceChainID,
					@Name = @Name,
					-- Address
					@AddressLine1 = @AddressLine1,
					@AddressLine2 = @AddressLine2,
					@City  = @City,
					@State  = @State,
					@PostalCode  = @PostalCode,
					--Business Phone (Work)
					@PhoneNumber_Work  = @PhoneNumber,
					@PhoneNumber_Fax  = @FaxNumber,	
					-- Caller
					@CreatedBy = @CreatedBy,
					-- Output
					@BusinessEntityID = @BusinessEntityID OUTPUT,	
					-- Error
					@ErrorLogID  = @ErrorLogID OUTPUT			
				
								
			END
			BEGIN
			
				EXEC spChain_Update
					@BusinessEntityID = @BusinessEntityID,
					@SourceChainID = @SourceChainID,
					@Name = @Name,
					-- Address
					@AddressLine1 = @AddressLine1,
					@AddressLine2 = @AddressLine2,
					@City  = @City,
					@State  = @State,
					@PostalCode  = @PostalCode,
					--Business Phone (Work)
					@PhoneNumber_Work  = @PhoneNumber,
					@PhoneNumber_Fax  = @FaxNumber,	
					-- Caller
					@ModifiedBy = @CreatedBy,
					-- Output						
					-- Error
					@ErrorLogID  = @ErrorLogID OUTPUT
					
			END

			SET @IsMerged = CASE WHEN @ErrorLogID IS NULL THEN 1 ELSE 0 END;
		
			-- Update import data to reflect merge.
			UPDATE i
			SET 
				i.IsMerged = @IsMerged,
				i.ErrorLogID = @ErrorLogID,
				i.DateModified = GETDATE()
			FROM tmp.Chain_Import i
			WHERE i.ChainImportID = @ChainImportID
						
		END TRY
		BEGIN CATCH
			
			EXEC spLogError @ErrorLogID;
			
			-- Update import data to reflect error.		
			UPDATE i
			SET 
				i.IsMerged = 0,
				i.ErrorLogID = @ErrorLogID,
				i.DateModified = GETDATE()
			FROM tmp.Chain_Import i
			WHERE i.ChainImportID = @ChainImportID				
			
		END CATCH	

		
		FETCH NEXT FROM curChains INTO
			@ChainImportID,
			@SourceChainID,
			@Name,
			@AddressLine1,
			@AddressLine2,
			@City,
			@State,
			@PostalCode,
			@PhoneNumber,
			@FaxNumber,
			@BusinessEntityID
			
	END

	CLOSE curChains
	DEALLOCATE curChains
	
	------------------------------------------------------------
	-- Log information
	-- <Summary>
	-- Logs processed counts for review.
	-- </Summary>
	------------------------------------------------------------
	DECLARE
		@TotalRecords INT = 0,
		@Records_ToCreate INT = 0,
		@Records_Created INT = 0,
		@Records_ToUpdate INT = 0,
		@Records_Updated INT = 0,
		@TotalErrorRecords INT = 0,
		@SourceName VARCHAR(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@InfoMessage VARCHAR(4000),
		@Arguments VARCHAR(MAX)
	
	-- Obtain record information.
	SELECT
		@TotalRecords = COUNT(*),
		@Records_ToCreate = SUM(CASE WHEN BusinessEntityID IS NULL THEN 1 ELSE 0 END),
		@Records_ToUpdate = SUM(CASE WHEN BusinessEntityID IS NOT NULL THEN 1 ELSE 0 END),
		@Records_Created = SUM(CASE WHEN BusinessEntityID IS NULL AND IsMerged = 1 THEN 1 ELSE 0 END),	
		@Records_Updated = SUM(CASE WHEN BusinessEntityID IS NOT NULL AND IsMerged = 1 THEN 1 ELSE 0 END),
		@TotalErrorRecords = SUM(CASE WHEN ErrorLogID IS NOT NULL THEN 1 ELSE 0 END)
	--SELECT *
	--SELECT TOP 1 *
	FROM tmp.Chain_Import
	WHERE DataCatalogCode = @DataCatalogCode

	SET @InfoMessage = 'EZDW chain import completed. ' +
		'Total records imported: ' + dbo.fnToStringOrEmpty(@TotalRecords) + '. ' +
		'Total records to create: ' + dbo.fnToStringOrEmpty(@Records_ToCreate) + '. ' +
		'Total records created: ' + dbo.fnToStringOrEmpty(@Records_Created) + '. ' +
		'Total records to update: ' + dbo.fnToStringOrEmpty(@Records_ToUpdate) + '. ' +
		'Total records updated: ' + dbo.fnToStringOrEmpty(@Records_Updated) + '. ' +
		'Total Errors Encountered: '+ dbo.fnToStringOrEmpty(@TotalErrorRecords)
		
	SET @Arguments = 
		'@ChainID=' + dbo.fnToStringOrEmpty(@ChainID)
    
	-- Log data.
	EXEC spLogInformation
		@InformationProcedure = @SourceName,
		@Message = @InfoMessage,
		@Arguments = @Arguments


	------------------------------------------------------------
	-- Drop resources
	------------------------------------------------------------
	DROP TABLE #tmpChainIDList;
	

	*/

END
