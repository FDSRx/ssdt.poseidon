﻿CREATE TABLE [template].[Template] (
    [TemplateID]   INT              NOT NULL,
    [Code]         VARCHAR (50)     NOT NULL,
    [Name]         VARCHAR (256)    NOT NULL,
    [Header]       VARCHAR (MAX)    NULL,
    [Body]         VARCHAR (MAX)    NULL,
    [Footer]       VARCHAR (MAX)    NULL,
    [rowguid]      UNIQUEIDENTIFIER CONSTRAINT [DF_Template_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]  DATETIME         CONSTRAINT [DF_Template_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME         CONSTRAINT [DF_Template_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    VARCHAR (256)    NULL,
    [ModifiedBy]   VARCHAR (256)    NULL,
    CONSTRAINT [PK_Template] PRIMARY KEY CLUSTERED ([TemplateID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Template_Code]
    ON [template].[Template]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Template_rowguid]
    ON [template].[Template]([rowguid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Template_Name]
    ON [template].[Template]([Name] ASC);

