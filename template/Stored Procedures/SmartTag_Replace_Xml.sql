﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/19/2016
-- Description:	Replaces smart tags.
-- SAMPLE CALL:
/*

DECLARE
	@SmartTagData XML = '<KeyValueList><KeyValue><Key>{{FirstName}}</Key><Value>Daniel</Value></KeyValue></KeyValueList>',
	@Data VARCHAR(MAX) = 'Hello, {{FirstName}}',
	@Result VARCHAR(MAX) = NULL,
	@Debug BIT = 1

EXEC template.SmartTag_Replace_Xml
	@SmartTagData = @SmartTagData,
	@Data = @Data,
	@Result = @Result OUTPUT,
	@Debug = @Debug

SELECT @Result AS Result


*/
-- =============================================
CREATE PROCEDURE template.[SmartTag_Replace_Xml]
	@SmartTagData XML,
	@Data VARCHAR(MAX),
	@Result VARCHAR(MAX) = NULL OUTPUT,
	@Debug BIT = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	---------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	---------------------------------------------------------------------------------------------------------------
	SET @Result = NULL;
	SET @Debug = ISNULL(@Debug, 0);
	SET @Data = NULLIF(@Data, '');
	SET @SmartTagData = NULLIF(CONVERT(VARCHAR(MAX), @SmartTagData), '');

	---------------------------------------------------------------------------------------------------------------
	-- Short Circuit.
	---------------------------------------------------------------------------------------------------------------
	IF @SmartTagData IS NULL OR @Data IS NULL
	BEGIN
		RETURN;
	END;

	---------------------------------------------------------------------------------------------------------------
	-- Temporary resources.
	---------------------------------------------------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#tmpSmartFields') IS NOT NULL
	BEGIN
		DROP TABLE #tmpSmartFields;
	END

	CREATE TABLE  #tmpSmartFields (
		Idx BIGINT IDENTITY(1,1),
		KeyName VARCHAR(256),
		KeyValue VARCHAR(MAX)
	)

	---------------------------------------------------------------------------------------------------------------
	-- Parse key/value (smart field/value) pairs.
	---------------------------------------------------------------------------------------------------------------	
	INSERT INTO #tmpSmartFields (
		KeyName,
		KeyValue
	)
	SELECT 
		KeyName,
		Value
	FROM dbo.fnXmlKeyValueList(@SmartTagData) 
	
	---------------------------------------------------------------------------------------------------------------
	-- Update data to have smart tags inserted.
	---------------------------------------------------------------------------------------------------------------
	DECLARE 
		@KeyName VARCHAR(256),
		@KeyValue VARCHAR(MAX)  
		
	DECLARE curSmartFields CURSOR FAST_FORWARD FOR

	SELECT 
		KeyName, 
		KeyValue
	FROM #tmpSmartFields

	OPEN curSmartFields

	FETCH NEXT FROM curSmartFields INTO @KeyName, @KeyValue
	WHILE @@FETCH_STATUS = 0
	BEGIN

		IF @Data IS NOT NULL 
		BEGIN
			SET @Data = REPLACE(@Data, @KeyName, @KeyValue);
		END
		
		FETCH NEXT FROM curSmartFields INTO @KeyName, @KeyValue
	END

	CLOSE curSmartFields
	DEALLOCATE curSmartFields	

	-- Output the updated data
	SET @Result = @Data;
	
	---------------------------------------------------------------------------------------------------------------
	-- Dispose of resources.
	---------------------------------------------------------------------------------------------------------------
	DROP TABLE #tmpSmartFields;
	
END
