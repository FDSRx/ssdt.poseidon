﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 2/26/2016
-- Description:	Updates a NoteSchedule object.
-- Change log:
-- 2/26/2016 - dhughes - Modified the procedure to except more inputs. 

-- SAMPLE CALL:
/*

DECLARE 
	@NoteScheduleID BIGINT = 144,
	@NoteID BIGINT = NULL,
	@DateStart DATETIME,
	@DateEnd DATETIME = NULL,
	@IsAllDayEvent BIT = NULL,
	@FrequencyTypeID INT = NULL,
	@FrequencyTypeCode VARCHAR(50) = NULL,
	@DateEffectiveStart DATETIME = NULL,
	@DateEffectiveEnd DATETIME = NULL,
	@ModifiedBy VARCHAR(256) = 'dhughes',
	@Debug BIT = 1

EXEC schedule.spNoteSchedule_Update
	@NoteScheduleID = @NoteScheduleID,
	@NoteID = @NoteID,
	@DateStart = @DateStart,
	@DateEnd = @DateEnd,
	@IsAllDayEvent = @IsAllDayEvent,
	@FrequencyTypeID = @FrequencyTypeID,
	@FrequencyTypeCode = @FrequencyTypeCode,
	@DateEffectiveStart = @DateEffectiveStart,
	@DateEffectiveEnd = @DateEffectiveEnd,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

SELECT @NoteScheduleID AS NoteScheduleID, @NoteID AS NoteID

*/

-- SELECT * FROM schedule.NoteSchedule
-- SELECT * FROM schedule.vwNoteSchedule
-- SELECT * FROM dbo.vwNote
-- SELECT * FROM dbo.vwTask
-- SELECT * FROM dbo.vwCalendarItem
-- =============================================
CREATE PROCEDURE [schedule].[spNoteSchedule_Update]
	@NoteScheduleID VARCHAR(MAX),
	@NoteID BIGINT = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL,
	@IsAllDayEvent BIT = NULL,
	@FrequencyTypeID INT = NULL,
	@FrequencyTypeCode VARCHAR(50) = NULL,
	@DateEffectiveStart DATETIME = NULL,
	@DateEffectiveEnd DATETIME = NULL,
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------------------
	-- Instance variables
	--------------------------------------------------------------------------------------------------------------------
	DECLARE 
		@Trancount INT = @@TRANCOUNT,
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000)
	
	-----------------------------------------------------------------------------------------------------------------------
	-- Log request.
	-----------------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	DECLARE @Args VARCHAR(MAX) =
		'@NoteScheduleID=' + dbo.fnToStringOrEmpty(@NoteScheduleID) + ';' +
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@DateStart=' + dbo.fnToStringOrEmpty(@DateStart) + ';' +
		'@DateEnd=' + dbo.fnToStringOrEmpty(@DateEnd) + ';' +
		'@IsAllDayEvent=' + dbo.fnToStringOrEmpty(@IsAllDayEvent) + ';' +
		'@FrequencyTypeID=' + dbo.fnToStringOrEmpty(@FrequencyTypeID) + ';' +
		'@FrequencyTypeCode=' + dbo.fnToStringOrEmpty(@FrequencyTypeCode) + ';' +
		'@DateEffectiveStart=' + dbo.fnToStringOrEmpty(@DateEffectiveStart) + ';' +
		'@DateEffectiveEnd=' + dbo.fnToStringOrEmpty(@DateEffectiveEnd) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;
	
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/

	
	--------------------------------------------------------------------------------------------------------------------
	-- Local variables
	--------------------------------------------------------------------------------------------------------------------
	-- No local declarations.
	
	--------------------------------------------------------------------------------------------------------------------
	-- Set variables
	--------------------------------------------------------------------------------------------------------------------
	SET @FrequencyTypeID = COALESCE(schedule.fnGetFrequencyTypeID(@FrequencyTypeID), schedule.fnGetFrequencyTypeID(@FrequencyTypeCode));

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Debugger On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@DateStart AS DateStart, @DateEnd AS DateEnd, @IsAllDayEvent AS IsAllDayEvent, 
			@FrequencyTypeID AS FrequencyTypeID, @FrequencyTypeCode AS FrequencyTypeCode, @DateEffectiveStart AS DateEffectiveStart, 
			@DateEffectiveEnd AS DateEffectiveEnd, @ModifiedBy AS ModifiedBy
	END	

	--------------------------------------------------------------------------------------------------------------------
	-- Update object.
	--------------------------------------------------------------------------------------------------------------------
	UPDATE trgt
	SET
		trgt.DateStart = ISNULL(@DateStart, trgt.DateStart),
		trgt.DateEnd = ISNULL(@DateEnd, trgt.DateEnd),
		trgt.IsAllDayEvent = ISNULL(@IsAllDayEvent, trgt.IsAllDayEvent),
		trgt.FrequencyTypeID = ISNULL(@FrequencyTypeID, trgt.FrequencyTypeID),
		trgt.DateEffectiveStart = ISNULL(@DateEffectiveStart, trgt.DateEffectiveStart),
		trgt.DateEffectiveEnd = ISNULL(@DateEffectiveEnd, trgt.DateEffectiveEnd),
		trgt.DateModified = GETDATE(),
		trgt.ModifiedBy = ISNULL(@ModifiedBy, trgt.ModifiedBy)
	--SELECT *
	FROM schedule.NoteSchedule trgt
		JOIN (
			SELECT
				Value AS NoteScheduleID
			FROM dbo.fnSplit(@NoteScheduleID, ',')
			WHERE ISNUMERIC(Value) = 1
		) src
			ON src.NoteScheduleID = trgt.NoteScheduleID

		
END
