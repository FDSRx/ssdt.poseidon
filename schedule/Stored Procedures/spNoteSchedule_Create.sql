﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/8/2014
-- Description:	Creates a new NoteSchedule object.
-- Change log:
-- 2/26/2016 - dhughes - Modified the procedure to except more inputs. 

-- SAMPLE CALL:
/*
DECLARE 
	@NoteScheduleID BIGINT,
	@NoteID BIGINT = NULL,
	@DateStart DATETIME,
	@DateEnd DATETIME = NULL,
	@IsAllDayEvent BIT = NULL,
	@FrequencyTypeID INT = NULL,
	@FrequencyTypeCode VARCHAR(50) = NULL,
	@DateEffectiveStart DATETIME = NULL,
	@DateEffectiveEnd DATETIME = NULL,
	@ModifiedBy VARCHAR(256) = NULL

EXEC schedule.spNoteSchedule_Create
	@NoteID = @NoteID,
	@DateStart = @DateStart,
	@DateEnd = @DateEnd,
	@IsAllDayEvent = @IsAllDayEvent,
	@FrequencyTypeID = @FrequencyTypeID,
	@FrequencyTypeCode = @FrequencyTypeCode,
	@DateEffectiveStart = @DateEffectiveStart,
	@DateEffectiveEnd = @DateEffectiveEnd,
	@CreatedBy = @CreatedBy,
	@NoteScheduleID = @NoteScheduleID OUTPUT

SELECT @NoteScheduleID AS NoteScheduleID, @NoteID AS NoteID
*/

-- SELECT * FROM schedule.NoteSchedule
-- SELECT * FROM dbo.vwNote
-- SELECT * FROM dbo.vwTask
-- SELECT * FROM dbo.vwCalendarItem
-- =============================================
CREATE PROCEDURE [schedule].[spNoteSchedule_Create]
	@NoteID BIGINT,
	@DateStart DATETIME,
	@DateEnd DATETIME = NULL,
	@IsAllDayEvent BIT = NULL,
	@FrequencyTypeID INT = NULL,
	@FrequencyTypeCode VARCHAR(50) = NULL,
	@DateEffectiveStart DATETIME = NULL,
	@DateEffectiveEnd DATETIME = NULL,
	@CreatedBy VARCHAR(256) = NULL,
	@NoteScheduleID BIGINT = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------------------
	-- Instance variables
	--------------------------------------------------------------------------------------------------------------------
	DECLARE
		@ErrorMessage VARCHAR(4000)


	------------------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------------------
	SET @NoteScheduleID = NULL;
	SET @DateEffectiveStart = ISNULL(@DateEffectiveStart, '1/1/1900');
	SET @DateEffectiveEnd = ISNULL(@DateEffectiveEnd, '12/31/9999');
	SET @DateEnd = ISNULL(@DateEnd, @DateStart);
	SET @IsAllDayEvent = ISNULL(@IsAllDayEvent, 0);
	SET @FrequencyTypeID = COALESCE(@FrequencyTypeID, schedule.fnGetFrequencyTypeID(@FrequencyTypeCode), schedule.fnGetFrequencyTypeID('ONCE'));
	
	------------------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------------------
	-- No local declarations.
	
	------------------------------------------------------------------------------------------------------------------------
	-- Set variables.
	------------------------------------------------------------------------------------------------------------------------
	
	------------------------------------------------------------------------------------------------------------------------
	-- Null argument validation
	-- <Summary>
	-- Validates if any of the necessary arguments were provided with
	-- data.
	-- </Summary>
	------------------------------------------------------------------------------------------------------------------------
	-- @NoteID 
	IF @NoteID IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create NoteSchedule object. Object reference (@NoteID) is not set to an instance of an object. ' +
			'The @NoteID parameter cannot be null or empty.';
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		  
		RETURN;
	END	
	
	-- @DateStart
	IF @DateStart IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create NoteSchedule object. Object reference (@DateStart) is not set to an instance of an object. ' +
			'The @DateStart parameter cannot be null or empty.';
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		  
		RETURN;
	END	
	
	-- @DateEnd
	IF @DateEnd IS NULL
	BEGIN
		SET @ErrorMessage = 'Unable to create NoteSchedule object. Object reference (@DateEnd) is not set to an instance of an object. ' +
			'The @DateEnd parameter cannot be null or empty.';
		
		RAISERROR (
			@ErrorMessage, -- Message text.
			16, -- Severity.
			1 -- State.
		);	
		  
		RETURN;
	END		


	------------------------------------------------------------------------------------------------------------------------
	-- Create NoteSchedule object.
	------------------------------------------------------------------------------------------------------------------------
	INSERT INTO schedule.NoteSchedule (
		NoteID,
		DateStart,
		DateEnd,
		IsAllDayEvent,
		FrequencyTypeID,
		DateEffectiveStart,
		DateEffectiveEnd,
		CreatedBy
	)
	SELECT
		@NoteID AS NoteID,
		@DateStart AS DateStart,
		@DateEnd AS DateEnd,
		@IsAllDayEvent AS IsAllDayEvent,
		@FrequencyTypeID AS FrequencyTypeID,
		@DateEffectiveStart AS DateEffectiveStart,
		@DateEffectiveEnd AS DateEffectiveEnd,
		@CreatedBy
			
	-- Retrieve record identity
	SET @NoteScheduleID = SCOPE_IDENTITY();
		
END
