﻿-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/18/2104
-- Description:	Deletes a NoteSchedule object.
-- Change Log:
-- 11/12/2015 - dhughes - Modified the delete statement by removing the "OR" clause and wrapping the identifier deletes in their
--							own "IF" statements to improve speed and performance.
-- 7/6/2016 - dhughes - Modified the procedure to accept a VARCHAR note/schedule input vs. an integer so that multiple notes/schedules
--				could be sent.

-- SAMPLE CALL: 
/*

DECLARE
	@NoteScheduleID VARCHAR(MAX) = NULL, 
	@NoteID VARCHAR(MAX) = NULL, 
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = 1

EXEC schedule.spNoteSchedule_Delete 
	@NoteScheduleID = @NoteScheduleID,
	@NoteID = @NoteID,
	@ModifiedBy = @ModifiedBy,
	@Debug = @Debug

*/

-- SELECT * FROM schedule.NoteSchedule
-- =============================================
CREATE PROCEDURE [schedule].[spNoteSchedule_Delete]
	@NoteScheduleID VARCHAR(MAX) = NULL, 
	@NoteID VARCHAR(MAX) = NULL, 
	@ModifiedBy VARCHAR(256) = NULL,
	@Debug BIT = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--------------------------------------------------------------------------------------------------------------
	-- Instance variables.
	--------------------------------------------------------------------------------------------------------------
	DECLARE 
		@ProcedureName VARCHAR(256) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID),
		@ErrorMessage VARCHAR(4000) = NULL,
		@DebugMessage VARCHAR(4000) = NULL,
		@Trancount INT = @@TRANCOUNT,
		@TransactionDate DATETIME = GETDATE()

	DECLARE @Args VARCHAR(MAX) =
		'@NoteScheduleID=' + dbo.fnToStringOrEmpty(@NoteScheduleID) + ';' +
		'@NoteID=' + dbo.fnToStringOrEmpty(@NoteID) + ';' +
		'@ModifiedBy=' + dbo.fnToStringOrEmpty(@ModifiedBy) + ';' +
		'@Debug=' + dbo.fnToStringOrEmpty(@Debug) + ';' ;

	--------------------------------------------------------------------------------------------------------------
	-- Log request.
	--------------------------------------------------------------------------------------------------------------
	-- Debug: Log request
	/*
	EXEC dbo.spLogInformation
		@InformationProcedure = @ProcedureName,
		@Message = 'The request was fired.',
		@Arguments = @Args
	*/		

	------------------------------------------------------------------------------------------------------------
	-- Sanitize input.
	------------------------------------------------------------------------------------------------------------
	SET @Debug = ISNULL(@Debug, 0);
			

	------------------------------------------------------------------------------------------------------------
	-- Local variables.
	------------------------------------------------------------------------------------------------------------

		
	--------------------------------------------------------------------------------------------------------------
	-- Set variables.
	--------------------------------------------------------------------------------------------------------------
	SET @NoteID = 
		CASE 
			WHEN @NoteScheduleID IS NULL THEN @NoteID 
			ELSE NULL 
		END;

	-- Debug
	IF @Debug = 1
	BEGIN
		SELECT 'Deubber On' AS DebugMode, @ProcedureName AS ProcedureName, 'Get/Set variables' AS ActionMethod,
			@NoteScheduleID AS NoteScheduleID, @NoteID AS NoteID
	END
	
	--------------------------------------------------------------------------------------------------------------
	-- Store ModifiedBy property in "session" like object.
	--------------------------------------------------------------------------------------------------------------
	EXEC memory.spContextInfo_TriggerData_Set
		@ObjectName = @ProcedureName,
		@DataString = @ModifiedBy
	
	--------------------------------------------------------------------------------------------------------------
	-- Delete NoteSchedule object(s).
	--------------------------------------------------------------------------------------------------------------
	-- Delete via the primary index.
	IF @NoteScheduleID IS NOT NULL
	BEGIN
		DELETE ns
		--SELECT *
		FROM schedule.NoteSchedule ns
		WHERE ns.NoteScheduleID IN (SELECT Value FROM dbo.fnSplit(@NoteScheduleID, ',') WHERE ISNUMERIC(Value) = 1)
	END

	-- If a primary identifier was not supplied, but we did receive a note identifier, then delete using the note
	-- identifier.
	IF @NoteScheduleID IS NULL AND @NoteID IS NOT NULL
	BEGIN
		DELETE ns
		--SELECT *
		FROM schedule.NoteSchedule ns
		WHERE ns.NoteID IN (SELECT Value FROM dbo.fnSplit(@NoteID, ',') WHERE ISNUMERIC(Value) = 1)
	END
	
END
