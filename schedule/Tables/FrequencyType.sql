﻿CREATE TABLE [schedule].[FrequencyType] (
    [FrequencyTypeID] INT              IDENTITY (1, 1) NOT NULL,
    [Code]            VARCHAR (25)     NOT NULL,
    [Name]            VARCHAR (25)     NOT NULL,
    [Description]     VARCHAR (1000)   NULL,
    [rowguid]         UNIQUEIDENTIFIER CONSTRAINT [DF_FrequencyType_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]     DATETIME         CONSTRAINT [DF_FrequencyType_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]    DATETIME         CONSTRAINT [DF_FrequencyType_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       VARCHAR (256)    NULL,
    [ModifiedBy]      VARCHAR (256)    NULL,
    CONSTRAINT [PK_FrequencyType] PRIMARY KEY CLUSTERED ([FrequencyTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FrequencyType_Code]
    ON [schedule].[FrequencyType]([Code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FrequencyType_Name]
    ON [schedule].[FrequencyType]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_FrequencyType_rowguid]
    ON [schedule].[FrequencyType]([rowguid] ASC);

