﻿CREATE TABLE [schedule].[NoteSchedule_History] (
    [NoteScheduleHistoryID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [NoteScheduleID]        BIGINT           NOT NULL,
    [NoteID]                BIGINT           NOT NULL,
    [DateStart]             DATETIME         NOT NULL,
    [DateEnd]               DATETIME         NOT NULL,
    [IsAllDayEvent]         BIT              NOT NULL,
    [FrequencyTypeID]       INT              NOT NULL,
    [DateEffectiveStart]    DATETIME         NOT NULL,
    [DateEffectiveEnd]      DATETIME         NOT NULL,
    [rowguid]               UNIQUEIDENTIFIER NOT NULL,
    [DateCreated]           DATETIME         NOT NULL,
    [DateModified]          DATETIME         NOT NULL,
    [CreatedBy]             VARCHAR (256)    NULL,
    [ModifiedBy]            VARCHAR (256)    NULL,
    [AuditGuid]             UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [AuditAction]           VARCHAR (10)     NOT NULL,
    [DateAudited]           DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_NoteSchedule_History] PRIMARY KEY CLUSTERED ([NoteScheduleHistoryID] ASC)
);

