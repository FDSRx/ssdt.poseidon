﻿CREATE TABLE [schedule].[NoteSchedule] (
    [NoteScheduleID]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [NoteID]             BIGINT           NOT NULL,
    [DateStart]          DATETIME         NOT NULL,
    [DateEnd]            DATETIME         NOT NULL,
    [IsAllDayEvent]      BIT              CONSTRAINT [DF_NoteSchedule_IsAllDayEvent] DEFAULT ((0)) NOT NULL,
    [FrequencyTypeID]    INT              NOT NULL,
    [DateEffectiveStart] DATETIME         CONSTRAINT [DF_NoteSchedule_DateEffectiveStart] DEFAULT (CONVERT([datetime],'1/1/1900',(0))) NOT NULL,
    [DateEffectiveEnd]   DATETIME         CONSTRAINT [DF_NoteSchedule_DateTerminated] DEFAULT (CONVERT([datetime],'12/31/9999',(0))) NOT NULL,
    [rowguid]            UNIQUEIDENTIFIER CONSTRAINT [DF_NoteSchedule_rowguid] DEFAULT (newid()) NOT NULL,
    [DateCreated]        DATETIME         CONSTRAINT [DF_NoteSchedule_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateModified]       DATETIME         CONSTRAINT [DF_NoteSchedule_DateModified] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          VARCHAR (256)    NULL,
    [ModifiedBy]         VARCHAR (256)    NULL,
    CONSTRAINT [PK_NoteSchedule] PRIMARY KEY CLUSTERED ([NoteScheduleID] ASC),
    CONSTRAINT [FK_NoteSchedule_FrequencyType] FOREIGN KEY ([FrequencyTypeID]) REFERENCES [schedule].[FrequencyType] ([FrequencyTypeID]),
    CONSTRAINT [FK_NoteSchedule_Note] FOREIGN KEY ([NoteID]) REFERENCES [dbo].[Note] ([NoteID])
);


GO
CREATE NONCLUSTERED INDEX [IX_NoteSchedule_NoteID]
    ON [schedule].[NoteSchedule]([NoteID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_NoteSchedule_rowguid]
    ON [schedule].[NoteSchedule]([rowguid] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_NoteSchedule_StartEndDate]
    ON [schedule].[NoteSchedule]([DateStart] ASC, [DateEnd] ASC);


GO
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 8/5/2014
-- Description:	Audits a record change of the schedule.NoteSchedule table
-- SELECT * FROM schedule.NoteSchedule_History
-- TRUNCATE TABLE schedule.NoteSchedule_History
-- =============================================
CREATE TRIGGER [schedule].[trigNoteSchedule_Audit]
   ON  [schedule].[NoteSchedule]
   AFTER DELETE, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS(SELECT TOP 1 * FROM deleted) -- exit trigger when zero records affected
	BEGIN
	   RETURN;
	END;
	
	DECLARE @AuditAction VARCHAR(10);-- Update/Insert/Delete
	DECLARE @AuditActionCode CHAR(1);
	DECLARE @AuditGuid UNIQUEIDENTIFIER = NEWID();
	DECLARE @DateAudited DATETIME = GETDATE();
	
	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
	  IF EXISTS(SELECT TOP 1 * FROM deleted)
	  BEGIN
		 SET @AuditAction ='Update';
		 SET @AuditActionCode = 'U';
	  END
	  ELSE
	  BEGIN
		 SET @AuditAction ='Insert';
		 SET @AuditActionCode = 'I';
	  END
	END
	ELSE
	BEGIN
	  SET @AuditAction = 'Delete';
	  SET @AuditActionCode = 'D';
	END;	


	-----------------------------------------------------------------------------
	-- Retrieve session data.
	-----------------------------------------------------------------------------
	DECLARE
		@DataString VARCHAR(MAX)
	
	EXEC memory.spContextInfo_TriggerData_Get
		@DataString = @DataString OUTPUT
		
	-------------------------------------------------------------------------------		
	-- Audit inserted records (updated or newly created)
	-------------------------------------------------------------------------------
	INSERT INTO schedule.NoteSchedule_History (
		NoteScheduleID,
		NoteID,
		DateStart,
		DateEnd,
		IsAllDayEvent,
		FrequencyTypeID,
		DateEffectiveStart,
		DateEffectiveEnd,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		ModifiedBy,
		AuditGuid,
		AuditAction,
		DateAudited	
	)
	SELECT
		NoteScheduleID,
		NoteID,
		DateStart,
		DateEnd,
		IsAllDayEvent,
		FrequencyTypeID,
		DateEffectiveStart,
		DateEffectiveEnd,
		rowguid,
		DateCreated,
		DateModified,
		CreatedBy,
		CASE 
			WHEN @DataString IS NOT NULL AND @AuditActionCode = 'D' THEN @DataString 
			WHEN @DataString IS NULL AND @AuditActionCode = 'D' THEN SUSER_NAME()
			ELSE ModifiedBy 
		END AS ModifiedBy,
		@AuditGuid,
		@AuditAction,
		@DateAudited
	FROM deleted
	
	-- Debug
	-- SELECT * FROM schedule.NoteSchedule
	-- SELECT * FROM schedule.NoteSchedule_History
	
	

END
