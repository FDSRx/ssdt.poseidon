﻿













CREATE VIEW [schedule].[vwCalendarItemSchedule] AS
SELECT
	ns.NoteScheduleID,
	ns.NoteID,
	nte.BusinessEntityID,
	enttyp.Code AS BusinessEntityTypeCode,
	enttyp.Name AS BusinessEntityTypeName,
	ci.Location,
	nte.ScopeID,
	scp.Code AS ScopeCode,
	scp.Name AS ScopeName,
	nte.NoteTypeID AS NoteTypeID,
	nt.Code AS NoteTypeCode,
	nt.Name AS NoteTypeName,
	nte.NotebookID,
	nb.Code AS NotebookCode,
	nb.Name AS NotebookName,
	nte.Title,
	nte.Memo,
	nte.PriorityID,
	pri.Code AS PriorityCode,
	pri.Name AS PriorityName,
	nte.OriginID,
	org.Code AS OriginCode,
	org.Name AS OriginName,
	nte.OriginDataKey,
	nte.AdditionalData,
	nte.CorrelationKey,
	ns.DateStart,
	ns.DateEnd,
	ns.IsAllDayEvent,
	ns.FrequencyTypeID,
	ft.Code AS FrequencyTypeCode,
	ft.Name AS FrequencyTypeName,
	ns.DateEffectiveStart,
	ns.DateEffectiveEnd,
	ns.rowguid,
	ns.DateCreated,
	ns.DateModified,
	ns.CreatedBy,
	ns.ModifiedBy
--SELECT *
FROM schedule.NoteSchedule ns
	JOIN dbo.Note nte
		ON nte.NoteID = ns.NoteID
	JOIN dbo.CalendarItem ci -- SELECT * FROM dbo.CalendarItem
		ON ci.NoteID = ns.NoteID
    JOIN dbo.BusinessEntity ent 
		ON ent.BusinessEntityID = nte.BusinessEntityID
    LEFT JOIN dbo.BusinessEntityType enttyp 
		ON enttyp.BusinessEntityTypeID = ent.BusinessEntityTypeID
	LEFT JOIN schedule.FrequencyType ft
		ON ft.FrequencyTypeID = ns.FrequencyTypeID
	LEFT JOIN dbo.Scope scp
		ON scp.ScopeID = nte.ScopeID
	LEFT JOIN dbo.NoteType nt
		ON nt.NoteTypeID = nte.NoteTypeID
	LEFT JOIN dbo.Notebook nb
		On nb.NotebookID = nte.NotebookID
	LEFT JOIN dbo.Priority pri
		ON pri.PriorityID = nte.PriorityID
	LEFT JOIN dbo.Origin org
		ON org.OriginID = nte.OriginID
