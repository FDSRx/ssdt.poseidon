﻿
-- =============================================
-- Author:		Daniel Hughes
-- Create date: 7/7/2013
-- Description:	Gets the FequencyTypeID from the FrequencyType object.
-- SAMPLE CALL: SELECT schedule.fnGetFrequencyTypeID('ONCE')
-- SAMPLE CALL: SELECT schedule.fnGetFrequencyTypeID(3)
-- =============================================
CREATE FUNCTION schedule.[fnGetFrequencyTypeID] 
(
	@Key VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ID INT

	IF ISNUMERIC(@Key) = 0
	BEGIN
		SET @ID = (SELECT FrequencyTypeID FROM schedule.FrequencyType WHERE Code = @Key);	
	END
	ELSE
	BEGIN
		SET @ID = (SELECT FrequencyTypeID FROM schedule.FrequencyType WHERE FrequencyTypeID = CONVERT(INT, @Key));
	END

	-- Return the result of the function
	RETURN @ID;

END

